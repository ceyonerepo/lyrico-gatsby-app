---
title: "anthimaalai neram song lyrics"
album: "Monster"
artist: "Justin Prabhakaran"
lyricist: "Karthik Netha"
director: "Nelson Venkatesan"
path: "/albums/monster-lyrics"
song: "Anthimaalai Neram"
image: ../../images/albumart/monster.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wN0kZTC6gsA"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andhimaalai Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimaalai Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatrangarai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrangarai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nila Vandhathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi Pesi Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Pesi Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Pokka Thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Pokka Thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavizhuthu Vaarththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavizhuthu Vaarththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Porththi Kondathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porththi Kondathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Neerin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Neerin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhaaye Viraindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhaaye Viraindhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Urundodinaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urundodinaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimaalai Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimaalai Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatrangarai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrangarai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nila Vandhathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottai Maadi Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottai Maadi Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai Mazhaiyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai Mazhaiyaagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottadaiyin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottadaiyin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattampochchi Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampochchi Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaraadha Edhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraadha Edhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thaalaattuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaalaattuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamdhorum Adhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamdhorum Adhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Thaan Ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Thaan Ketkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saambal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovin Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam Aagirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam Aagirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimaalai Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimaalai Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatrangarai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrangarai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nila Vandhathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarin Mazhai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mazhai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mazhai Serndhatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mazhai Serndhatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Kudai Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Kudai Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Manam Pogutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Manam Pogutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirakkaadha Kadhavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkaadha Kadhavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naal Ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naal Ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavilla Veliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavilla Veliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Naal Seruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Naal Seruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattam Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndhen Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndhen Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal Vaikkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Vaikkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimaalai Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimaalai Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatrangarai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrangarai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nila Vandhathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi Pesi Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Pesi Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi Pesi Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Pesi Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Pokka Thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Pokka Thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Pokka Thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Pokka Thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavizhuthu Vaarththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavizhuthu Vaarththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Porththi Kondathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porththi Kondathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhi Neerin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Neerin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhaaye Viraindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhaaye Viraindhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Urundodinaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urundodinaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhimaalai Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhimaalai Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatrangarai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatrangarai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nila Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nila Vandhathe"/>
</div>
</pre>
