---
title: "thandalkaaran song lyrics"
album: "NGK"
artist: "Yuvan Shankar Raja"
lyricist: "Kabilan"
director: "Selvaraghavan"
path: "/albums/ngk-lyrics"
song: "Thandalkaaran"
image: ../../images/albumart/ngk.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ln6XfxGRhd4"
type: "mass"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm Mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thandalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thandalkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanda Soru Kekkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanda Soru Kekkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Vechu Pesuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Vechu Pesuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandapadi Yesuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi Yesuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattampoochi Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachonthiya Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachonthiya Aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattamaiyin Kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattamaiyin Kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu Kettu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu Kettu Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahunnnnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahunnnnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandalkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ungalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ungalkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ungalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ungalkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadadadaa Naan Ungalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadadadaa Naan Ungalkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahunnnnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahunnnnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indianin Panpaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indianin Panpaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anniyano Vangitaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anniyano Vangitaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhar Atta Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhar Atta Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchi Seiya Vanthutaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchi Seiya Vanthutaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Indianin Panpaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indianin Panpaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anniyano Vangitaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anniyano Vangitaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhar Atta Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhar Atta Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchi Seiya Vanthutaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchi Seiya Vanthutaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Cheri Onna Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Cheri Onna Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Naattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Naattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Senjavana Vetturaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Senjavana Vetturaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Roattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Roattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Setha Pinbum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setha Pinbum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Vacha Sudukaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Vacha Sudukaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambu Kooda Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambu Kooda Kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumae Oru Koottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumae Oru Koottil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahunnnnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahunnnnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thandalkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thandalkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanda Soru Kekkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanda Soru Kekkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Vechu Pesuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Vechu Pesuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandapadi Yesuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi Yesuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattampoochi Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachonthiya Aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachonthiya Aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattamaiyin Kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattamaiyin Kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadu Kettu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu Kettu Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahuun Aahuun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuun Aahuun"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahunnnnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahunnnnn"/>
</div>
</pre>
