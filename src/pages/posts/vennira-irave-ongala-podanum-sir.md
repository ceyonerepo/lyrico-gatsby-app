---
title: "vennira irave song lyrics"
album: "Ongala Podanum Sir"
artist: "Rejimon"
lyricist: "Murugan Manthiram"
director: "Sreejith Vijayan"
path: "/albums/ongala-podanum-sir-lyrics"
song: "Vennira Irave"
image: ../../images/albumart/ongala-podanum-sir.jpg
date: 2019-09-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fo_Ggm9k3Mk"
type: "love"
singers:
  - Naresh Iyer
  - Reji Mon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vennira irave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennira irave"/>
</div>
<div class="lyrico-lyrics-wrapper">vinveli pagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinveli pagale"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mozhi nee sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mozhi nee sol"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">minmini thugale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minmini thugale"/>
</div>
<div class="lyrico-lyrics-wrapper">minnalin magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnalin magale"/>
</div>
<div class="lyrico-lyrics-wrapper">en vizhi pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vizhi pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">un azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan maarbil saainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan maarbil saainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaazhveane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaazhveane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaa inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaa inge"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir mazhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir mazhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">yennai naane thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai naane thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam oyntheane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam oyntheane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaa anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaa anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanmaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjil yaasagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil yaasagam"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan vaasagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan vaasagam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu podhum podhum kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu podhum podhum kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan gnaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan gnaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil veesidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil veesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvaaga kollum kannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvaaga kollum kannee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakkena naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkena naan"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyindri en uyirillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyindri en uyirillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhaliye uyir tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhaliye uyir tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir tharavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir tharavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pesum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pesum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">pesaamal keatpean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesaamal keatpean"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paarkkum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paarkkum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkaamal paarpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkaamal paarpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh kannadi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh kannadi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">yenai maatri kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenai maatri kolven"/>
</div>
<div class="lyrico-lyrics-wrapper">unai mattum thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai mattum thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kaati nirpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kaati nirpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kopam neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kopam neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kondalum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondalum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">mannippaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannippaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">naan keatpeanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan keatpeanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thooram neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sendralum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendralum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">naan selveane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan selveane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aagayam pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru megamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru megamai"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaatril poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaatril poven"/>
</div>
<div class="lyrico-lyrics-wrapper">naan koodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan koodave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aarambam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarambam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai thooralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai thooralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">un boomi serven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un boomi serven"/>
</div>
<div class="lyrico-lyrics-wrapper">naal thorume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal thorume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unthan ullangai pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan ullangai pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaazhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaazhave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjil yaasagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil yaasagam"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan vaasagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan vaasagam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu podhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu podhum "/>
</div>
<div class="lyrico-lyrics-wrapper">podhum kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan gnaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan gnaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil veesidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil veesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvaaga kollum kanneee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvaaga kollum kanneee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakkena naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkena naan"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyindri en uyirillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyindri en uyirillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhaliye uyir tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhaliye uyir tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir tharavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir tharavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">imaiyodu yeano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaiyodu yeano"/>
</div>
<div class="lyrico-lyrics-wrapper">yerumboora kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerumboora kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">yethiraali pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethiraali pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unai enninen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai enninen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viral pinni kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral pinni kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">vilagaamal nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagaamal nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">ee pogum pokkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee pogum pokkil"/>
</div>
<div class="lyrico-lyrics-wrapper">yenai maatrinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenai maatrinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maayam seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kan jaadai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaadai indru"/>
</div>
<div class="lyrico-lyrics-wrapper">adaam eval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaam eval"/>
</div>
<div class="lyrico-lyrics-wrapper">kathai solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathai solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavam seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">athuve niyaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuve niyaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">manam solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thegangal soozhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegangal soozhum"/>
</div>
<div class="lyrico-lyrics-wrapper">veppangalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veppangalil "/>
</div>
<div class="lyrico-lyrics-wrapper">kaayangal aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayangal aarum"/>
</div>
<div class="lyrico-lyrics-wrapper">neer oorudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer oorudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoorangal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoorangal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">thoolaagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoolaagida"/>
</div>
<div class="lyrico-lyrics-wrapper">thozhodu thozhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozhodu thozhum"/>
</div>
<div class="lyrico-lyrics-wrapper">uravaaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravaaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenthan thaabangal meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenthan thaabangal meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">ozhi pookuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ozhi pookuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjil yaasagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil yaasagam"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan vaasagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan vaasagam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu podhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu podhum "/>
</div>
<div class="lyrico-lyrics-wrapper">podhum kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan gnaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan gnaabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil veesidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil veesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">methuvaaga kollum kanneee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuvaaga kollum kanneee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakkena naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkena naan"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyindri en uyirillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyindri en uyirillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhaliye uyir tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhaliye uyir tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir tharavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir tharavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal penne"/>
</div>
</pre>
