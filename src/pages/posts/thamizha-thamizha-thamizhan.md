---
title: "tamizha tamizha song lyrics"
album: "Thamizhan"
artist: "D. Imman"
lyricist: "Vairamuthu"
director: "Majith"
path: "/albums/thamizhan-lyrics"
song: "Tamizha Tamizha"
image: ../../images/albumart/thamizhan.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fIr-N1jJd0k"
type: "motivational"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thamizha Hey Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizha Hey Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizha Hey Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizha Hey Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Velvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Velvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Velvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Velvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imayamalaiyai Iduppil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imayamalaiyai Iduppil Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhuthu Varuvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Varuvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Velvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Velvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Velvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Velvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imayamalaiyai Iduppil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imayamalaiyai Iduppil Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhuthu Varuvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Varuvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Vayalgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Vayalgalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Nadhigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Nadhigalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Naattil Vara Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Naattil Vara Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrum Mazhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Mazhaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Sonnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal Katti Thozha Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Katti Thozha Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvai Graham Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvai Graham Varaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Selvaakellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Selvaakellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pera Vendum Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera Vendum Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaayai Katti Vayathai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayai Katti Vayathai Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Variyai Kattugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Variyai Kattugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Varigal Engey Valigirathennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Varigal Engey Valigirathennum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigal Arivaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Arivaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu Vettigal Vaangitharavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Vettigal Vaangitharavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthahthiram Vanthathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthahthiram Vanthathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaal Kattirundha Kovanam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Kattirundha Kovanam Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavu Ponadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavu Ponadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu Kozhi Kaanom Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Kozhi Kaanom Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaval Nilayam Pona Magal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval Nilayam Pona Magal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karpai Kaanoom Kaanom Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpai Kaanoom Kaanom Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhari Kondu Thirumbuvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhari Kondu Thirumbuvatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janaathipathiyum Rickshakaaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaathipathiyum Rickshakaaranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattathin Munn Oruvarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattathin Munn Oruvarada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomai Janagal Urimai Pattri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Janagal Urimai Pattri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unara Seivathu Kadamaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unara Seivathu Kadamaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthanthiram Kaaka Poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthanthiram Kaaka Poradu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattam Terinthu Vaathaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Terinthu Vaathaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitri Suzhalum Soozhchi Udaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitri Suzhalum Soozhchi Udaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetri Kulathil Neeraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Kulathil Neeraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizha Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizha Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizha Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizha Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Velvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Velvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Velavaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Velavaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imayamalaiyai Iduppil Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imayamalaiyai Iduppil Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhuthu Varuvaai Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Varuvaai Thamizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanga Vayalgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Vayalgalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Nadhigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Nadhigalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Naattil Vara Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Naattil Vara Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrum Mazhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Mazhaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Sonnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal Katti Thozha Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Katti Thozha Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevvai Graham Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvai Graham Varaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Selvaakellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Selvaakellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pera Vendum Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera Vendum Vendum"/>
</div>
</pre>
