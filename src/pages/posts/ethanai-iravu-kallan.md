---
title: "ethanai iravu song lyrics"
album: "Kallan"
artist: "K"
lyricist: "Na Muthukumar"
director: "Chandra Thangaraj"
path: "/albums/kallan-lyrics"
song: "Ethanai Iravu"
image: ../../images/albumart/kallan.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/H_XNiJYifzk"
type: "love"
singers:
  - Deepak Blue
  - Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ethanai iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai nilavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai nilavai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pol oliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pol oliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">veesa villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesa villai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennudan thaniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennudan thaniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">pesa villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesa villai"/>
</div>
<div class="lyrico-lyrics-wrapper">anbenum malaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbenum malaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">nanainthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanainthidave"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram paalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram paalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unniru paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unniru paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">koodu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">siragugal murinthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragugal murinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">paranthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">piranthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piranthu vanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethanai iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai nilavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai nilavai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu irunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boomiyil pirantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil pirantha"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarume inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarume inge"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaai endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaai endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">iruppathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruppathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">pullukum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullukum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">uravugal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravugal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">man konda ver
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man konda ver"/>
</div>
<div class="lyrico-lyrics-wrapper">athai marapathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai marapathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">malaigalil thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaigalil thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">nathigalin payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathigalin payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal vanthu sernthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal vanthu sernthum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">mugil ena maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugil ena maari"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyaai thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyaai thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbukku endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbukku endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ondraai kalanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondraai kalanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">pirivu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivu illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethanai iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai nilavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai nilavai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu irunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethirinil irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirinil irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">nerukkangal piranthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerukkangal piranthum"/>
</div>
<div class="lyrico-lyrics-wrapper">iru manam kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru manam kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">erimalai ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erimalai ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">panimalai ondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panimalai ondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">sadugudu aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadugudu aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">sila ganam thotrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila ganam thotrom"/>
</div>
<div class="lyrico-lyrics-wrapper">sila ganam vendrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila ganam vendrom"/>
</div>
<div class="lyrico-lyrics-wrapper">sillena eriyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillena eriyum "/>
</div>
<div class="lyrico-lyrics-wrapper">neruppu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee naan naamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee naan naamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">maari vittome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari vittome"/>
</div>
</pre>
