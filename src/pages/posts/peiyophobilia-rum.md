---
title: "peiyophobilia song lyrics"
album: "Rum"
artist: "Anirudh Ravichander"
lyricist: "Vivek"
director: "Sai Bharath"
path: "/albums/rum-lyrics"
song: "Peiyophobilia"
image: ../../images/albumart/rum.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SSfdCq8LMGQ"
type: "mass"
singers:
  - Silambarasan
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Shabba Pothumda Saami Nu Uyira Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shabba Pothumda Saami Nu Uyira Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Neeturavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Neeturavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi Thigilaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Thigilaana "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathil Peiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathil Peiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Vaikka Maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vaikka Maataan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Death Aana Peiyaanu Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Death Aana Peiyaanu Maatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Kothoda Nammaalu Thookiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Kothoda Nammaalu Thookiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Fresh Aana Peiyaanu Post Adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fresh Aana Peiyaanu Post Adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Saithana Elathil Potudivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Saithana Elathil Potudivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peiyophobilia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyophobilia"/>
</div>
<div class="lyrico-lyrics-wrapper">Beethi Aavuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beethi Aavuriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayanthe Saavuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayanthe Saavuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambatha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambu Naanu Pongunithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambu Naanu Pongunithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mango Manguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mango Manguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrongu Ranguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrongu Ranguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothumda Saami Nu Uyira Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothumda Saami Nu Uyira Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Neeturavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Neeturavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi Thigilaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Thigilaana "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathil Peiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathil Peiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Vaikka Maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vaikka Maataan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Oru Bayangara Kaateri Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Oru Bayangara Kaateri Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Purinjita Biskothuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Purinjita Biskothuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Irutula Asanjathu Pei Illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutula Asanjathu Pei Illada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Engira Kathiresan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Engira Kathiresan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peiyaadi Aathaadi Loosaana Mel Maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyaadi Aathaadi Loosaana Mel Maadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhatha Komaali Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhatha Komaali Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyama Nogatha Paala Nee Pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyama Nogatha Paala Nee Pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagama Saathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagama Saathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nallathaan Pothu Un Lifeu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nallathaan Pothu Un Lifeu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Illadha Boothatha Thediduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Illadha Boothatha Thediduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaavi Vittaalum Shock Adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaavi Vittaalum Shock Adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kettaavi Vittaanu Oodiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kettaavi Vittaanu Oodiduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peiyophobilia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyophobilia"/>
</div>
<div class="lyrico-lyrics-wrapper">Beethi Aavuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beethi Aavuriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayanthe Saavuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayanthe Saavuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambatha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambu Naanu Pongunithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambu Naanu Pongunithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mango Manguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mango Manguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrongu Ranguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrongu Ranguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Peiyophobilia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Peiyophobilia"/>
</div>
<div class="lyrico-lyrics-wrapper">Beethi Aavuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beethi Aavuriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayanthe Saavuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayanthe Saavuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambatha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambu Naanu Pongunithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambu Naanu Pongunithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mango Manguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mango Manguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrongu Ranguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrongu Ranguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothumda Saami Nu Uyira Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothumda Saami Nu Uyira Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Neeturavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Neeturavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi Thigilaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Thigilaana "/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathil Peiya Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathil Peiya Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Vaikka Maataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vaikka Maataan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Death Aana Peiyaanu Maatikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Death Aana Peiyaanu Maatikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Kothoda Nammaalu Thookiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Kothoda Nammaalu Thookiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Fresh Aana Peiyaanu Post Adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fresh Aana Peiyaanu Post Adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Saithana Elathil Potudivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Saithana Elathil Potudivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Peiyophobilia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Peiyophobilia"/>
</div>
<div class="lyrico-lyrics-wrapper">Beethi Aavuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beethi Aavuriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayanthe Saavuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayanthe Saavuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambatha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambu Naanu Pongunithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambu Naanu Pongunithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mango Manguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mango Manguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrongu Ranguni Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrongu Ranguni Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambathada"/>
</div>
</pre>
