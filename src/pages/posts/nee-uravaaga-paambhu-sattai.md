---
title: "nee uravaaga song lyrics"
album: "Paambhu Sattai"
artist: "Ajeesh"
lyricist: "Yugabharathi"
director: "Adam Dasan"
path: "/albums/paambhu-sattai-lyrics"
song: "Nee Uravaaga"
image: ../../images/albumart/paambhu-sattai.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QHrwk1B0x50"
type: "love"
singers:
  - Shreya Ghoshal
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yelae yelaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelae yelaee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren kaadhal naalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poren kaadhal naalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee uravaagha aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uravaagha aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan verenna pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan verenna pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal oyaama veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal oyaama veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal thaanaaga koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal thaanaaga koosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketten kaadhal oosaa oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketten kaadhal oosaa oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee uravaagha aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uravaagha aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan verenna pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan verenna pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oraa kann paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oraa kann paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhedho pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhaari paiyan naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhaari paiyan naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen nallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen nallaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallum un vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallum un vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enmela veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enmela veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanenae kora pullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanenae kora pullum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala nee killa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala nee killa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaatha naan sollaoh hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaatha naan sollaoh hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamalae tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamalae tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaa kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaa kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Allaaduren pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allaaduren pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho oohooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho oohooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppa koolam ponnaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppa koolam ponnaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttiaana thaeraachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttiaana thaeraachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaadi unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorrum chellaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorrum chellaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen pulla enna neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen pulla enna neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aati vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aati vechaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkum munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkum munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaagha vaazha podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaagha vaazha podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal pichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal pichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu nee vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu nee vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum ponnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illa-nu naan sonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa-nu naan sonnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selaa kattum poongothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaa kattum poongothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda ninnaa dhaan gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda ninnaa dhaan gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaamae veredhum sothu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaamae veredhum sothu pathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee uravaagha aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uravaagha aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan verenna pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan verenna pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal oyaama veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal oyaama veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal thaanaaga koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal thaanaaga koosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketten kaadhal oosaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketten kaadhal oosaa aa"/>
</div>
</pre>
