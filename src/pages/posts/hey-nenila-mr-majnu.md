---
title: "hey nenila song lyrics"
album: "Mr. Majnu"
artist: "S. Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/mr-majnu-lyrics"
song: "Hey Nenila"
image: ../../images/albumart/mr-majnu.jpg
date: 2019-01-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JoPZMfF3xsk"
type: "love"
singers:
  - Sruthiranjani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey nenilaa neeto nedilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nenilaa neeto nedilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey choodilaa ento vintalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey choodilaa ento vintalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni rojullo eroju leninta ledi pillalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni rojullo eroju leninta ledi pillalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola bantalle naa gunde andamga gantulesilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola bantalle naa gunde andamga gantulesilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheti geetallo geesundi bahusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheti geetallo geesundi bahusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu kaliseti ee kotta varusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu kaliseti ee kotta varusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru vecchamga nee choopu naa gundelona gucchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru vecchamga nee choopu naa gundelona gucchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enta giliginta pedutondi nee allaredo mattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enta giliginta pedutondi nee allaredo mattugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nenilaa neeto nedilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nenilaa neeto nedilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey choodilaa ento vintalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey choodilaa ento vintalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu ento ishtamannaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu ento ishtamannaa "/>
</div>
<div class="lyrico-lyrics-wrapper">paatale nenilaa paadutunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatale nenilaa paadutunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku ento ishtamainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku ento ishtamainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">aatalaa nenilaa maarutunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatalaa nenilaa maarutunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu pakka enta mandi nannu patti aapinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu pakka enta mandi nannu patti aapinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu batti parugupetti ninnu cheranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu batti parugupetti ninnu cheranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku nenu daaramesi inta katti choosinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku nenu daaramesi inta katti choosinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishamaina neeku dooramavvalenanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishamaina neeku dooramavvalenanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu pattinchukonattu unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu pattinchukonattu unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nannenta tippinchukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nannenta tippinchukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru vecchamga nee choopu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru vecchamga nee choopu "/>
</div>
<div class="lyrico-lyrics-wrapper">naa gundelona gucchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa gundelona gucchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni maimarachipotunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni maimarachipotunna "/>
</div>
<div class="lyrico-lyrics-wrapper">nee maayalona mattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee maayalona mattugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rojuloni velalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojuloni velalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">rammani okkasaari pilavalemugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rammani okkasaari pilavalemugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuloni maatalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuloni maatalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">vinamani mootagatti ivvalemugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinamani mootagatti ivvalemugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karigipovu nimishamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigipovu nimishamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">cherigipovu lopale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherigipovu lopale"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeloni oohalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeloni oohalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku cheppanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku cheppanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chetiloni neeru laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chetiloni neeru laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">jaaripoka mundare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaaripoka mundare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasuloni aasalanni vinnavinchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasuloni aasalanni vinnavinchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni samayaalu nee pakkanunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni samayaalu nee pakkanunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka kshanamega naakivvamannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka kshanamega naakivvamannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru vecchamga nee choopu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru vecchamga nee choopu "/>
</div>
<div class="lyrico-lyrics-wrapper">naa gundelona gucchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa gundelona gucchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni maimarachipotunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni maimarachipotunna "/>
</div>
<div class="lyrico-lyrics-wrapper">nee maayalona mattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee maayalona mattugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nenilaa neeto nedilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nenilaa neeto nedilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey choodilaa ento vintalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey choodilaa ento vintalaa"/>
</div>
</pre>
