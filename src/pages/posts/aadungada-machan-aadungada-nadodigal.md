---
title: "aadungada machan aadungada song lyrics"
album: "Nadodigal"
artist: "Sundar C Babu"
lyricist: "Kabilan"
director: "Samuthrakani"
path: "/albums/nadodigal-lyrics"
song: "Aadungada Machan Aadungada"
image: ../../images/albumart/nadodigal.jpg
date: 2009-06-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dH7tWyfZWvQ"
type: "Celebration"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aadungada Machaan Aadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadungada Machaan Aadungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana Ponna Paathu Thedungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Ponna Paathu Thedungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadungadaa Machan Paadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadungadaa Machan Paadungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaadai Pinnaalathaan Odungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaadai Pinnaalathaan Odungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththa Vacha Ponnu Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa Vacha Ponnu Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththai Ponnu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Ponnu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththa Ponnu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththa Ponnu Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Maaman Ponnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maaman Ponnuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithatti Kooppududhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithatti Kooppududhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendukkannuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendukkannuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendaannukketka Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaannukketka Ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendaannu Cholla Cholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaannu Cholla Cholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarume Illa Illa Engalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarume Illa Illa Engalathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Engappaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Engappaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Engappaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Engappaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singaari Naaththana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaari Naaththana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Tea Aaththuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Tea Aaththuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koduppotta Glassula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppotta Glassula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Ooththunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Ooththunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjaavooru Kacheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjaavooru Kacheri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappaatta Oiyaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaatta Oiyaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalyaanam Pannikkannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam Pannikkannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhakkillunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhakkillunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paambu Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu Pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magudi Magudithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magudi Magudithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnappudikka Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnappudikka Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabadithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Paambu Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Paambu Pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magudi Magudithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magudi Magudithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnappudikka Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnappudikka Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabadithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam Melappaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam Melappaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaana Vedikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana Vedikkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanoda Ponnu Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanoda Ponnu Vandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Moodikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oororam Kallukkadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oororam Kallukkadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ododivaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ododivaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pangaali Onnaa Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangaali Onnaa Serndhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhaadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siththappan Paakkettula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siththappan Paakkettula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Slillairaiya Eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slillairaiya Eduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattaanmai Thinnaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaanmai Thinnaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seettaadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seettaadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangammaa Maaraappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangammaa Maaraappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malyuthththa Veeraappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malyuthththa Veeraappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkunnu Sirichaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkunnu Sirichaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sindhum Maththaappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhum Maththaappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyo Aandaalu Iduppula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Aandaalu Iduppula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaaru Madippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaaru Madippula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththaattam Aadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaattam Aadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu Saavithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu Saavithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallappudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallappudunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya Kaattudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya Kaattudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponna Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallakkaattudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakkaattudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Pallappudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Pallappudunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaiya Kaattudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaiya Kaattudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponna Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Pudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallakkaattudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakkaattudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaadai Katti Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaadai Katti Vandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serndhukkittu Aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhukkittu Aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda Vaadi Edhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Vaadi Edhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aankozhi Engaloda Aattaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankozhi Engaloda Aattaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaru Vaankozhi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Vaankozhi Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Jodi Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Jodi Seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaanpulla Aanaakkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanpulla Aanaakkooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanpulla Naan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanpulla Naan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paaththu Odippora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paaththu Odippora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaanpulla Aanaakkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaanpulla Aanaakkooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanpulla Naan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanpulla Naan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paaththu Odippora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paaththu Odippora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadungada Machaan Aadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadungada Machaan Aadungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana Ponna Paathu Thedungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Ponna Paathu Thedungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadungadaa Machan Paadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadungadaa Machan Paadungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaadai Pinnaalathaan Odungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaadai Pinnaalathaan Odungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththa Vacha Ponnu Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa Vacha Ponnu Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththai Ponnu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Ponnu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththa Ponnu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththa Ponnu Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Maaman Ponnuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maaman Ponnuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithatti Kooppududhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithatti Kooppududhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendukkannuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendukkannuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendaannukketka Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaannukketka Ketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendaannu Cholla Cholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaannu Cholla Cholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarume Illa Illa Engalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarume Illa Illa Engalathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Engappaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Engappaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Engappaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Engappaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalandhaan"/>
</div>
</pre>
