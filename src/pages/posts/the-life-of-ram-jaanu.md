---
title: "the life of ram song lyrics"
album: "Jaanu"
artist: "Govind Vasantha"
lyricist: "Sirivennela Sitaramasastri"
director: "C. Prem Kumar"
path: "/albums/jaanu-lyrics"
song: "The Life Of Ram"
image: ../../images/albumart/jaanu.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2a34XyiZO14"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Dhaaredhurainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dhaaredhurainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Etuveluthundho Adigaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etuveluthundho Adigaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Thochani Parugai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Thochani Parugai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravahisthoo Pothunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravahisthoo Pothunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Chusthu Unnaa Ne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Chusthu Unnaa Ne "/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikaanaa Edhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikaanaa Edhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorikane Chuttu Evevo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorikane Chuttu Evevo "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipisthu Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipisthu Unnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalani O Shilane Aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalani O Shilane Aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thrutilo Karige Kalane Aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thrutilo Karige Kalane Aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Theda Undhata Nuvvevarantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Theda Undhata Nuvvevarantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adigithe Nannevaraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigithe Nannevaraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaage Kadadhaaka O 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaage Kadadhaaka O "/>
</div>
<div class="lyrico-lyrics-wrapper">Prashnai Untaanantunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prashnai Untaanantunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Oka Badhulai Nanu Cherapoddhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Oka Badhulai Nanu Cherapoddhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaannaduguthu Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaannaduguthu Unnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Ventapadi Nuvventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ventapadi Nuvventha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontarivanavaddhu Anoddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontarivanavaddhu Anoddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhayunchi Evaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayunchi Evaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkonni Janmaalaki Saripadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkonni Janmaalaki Saripadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aneka Sruthulni Itharulu Eragaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aneka Sruthulni Itharulu Eragaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Oopirini Innalluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Oopirini Innalluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Vennanti Nadipina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Vennanti Nadipina"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyutha Evaridhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyutha Evaridhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Edha Layanu Kusalamu Adigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Edha Layanu Kusalamu Adigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gusa Gusa Kabhurula Guma Guma Levarivee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gusa Gusa Kabhurula Guma Guma Levarivee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhayam Kaagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhayam Kaagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaajaaga Puduthu Untaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaajaaga Puduthu Untaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Ipude Nanu Kanadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ipude Nanu Kanadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaganaga Antoo Ne Unta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaga Antoo Ne Unta"/>
</div>
<div class="lyrico-lyrics-wrapper">Epudu Poorthavane Avaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudu Poorthavane Avaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhi Leni Kadha Nenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhi Leni Kadha Nenugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Vaatam Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Vaatam Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aage Alavaate Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aage Alavaate Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Nilavadhu Ye Chotaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Nilavadhu Ye Chotaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilakadaga Ye Chirunaama Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilakadaga Ye Chirunaama Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Badhulu Pondhani Lekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Badhulu Pondhani Lekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuku Vesthundho Kekaa Mounamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuku Vesthundho Kekaa Mounamgaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Ventapadi Nuvventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ventapadi Nuvventha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontarivanavaddhu Anoddhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontarivanavaddhu Anoddhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhayunchi Evaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayunchi Evaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkonni Janmaalaki Saripadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkonni Janmaalaki Saripadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aneka Sruthulni Itharulu Eragaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aneka Sruthulni Itharulu Eragaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Oopirini Innalluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Oopirini Innalluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Vennanti Nadipina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Vennanti Nadipina"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyutha Evaridhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyutha Evaridhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Edha Layanu Kusalamu Adigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Edha Layanu Kusalamu Adigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gusa Gusa Kabhurula Guma Guma Levarivee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gusa Gusa Kabhurula Guma Guma Levarivee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lolo Ekantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolo Ekantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Chuttu Allina Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Chuttu Allina Lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Sontham Antunnaa Vinnaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Sontham Antunnaa Vinnaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Naa Needa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Naa Needa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharame Chaalantunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharame Chaalantunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakoodadhu Inkevarainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakoodadhu Inkevarainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Odilo Monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Odilo Monna"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhani Aashalatho Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhani Aashalatho Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho Ooristhu Undhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho Ooristhu Undhee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaabilli Antha Dhooraanunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaabilli Antha Dhooraanunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelega Chanthane Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelega Chanthane Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Antoo Ooyalaloopindhi Jolaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antoo Ooyalaloopindhi Jolaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Naane Naanine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Naane Naanine"/>
</div>
</pre>
