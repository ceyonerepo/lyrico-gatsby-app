---
title: "sakkara barabara song lyrics"
album: "Yeidhavan"
artist: "Paartav Barggo"
lyricist: "Ghana Vinoth"
director: "Sakthi Rajasekaran"
path: "/albums/yeidhavan-lyrics"
song: "Sakkara Barabara"
image: ../../images/albumart/yeidhavan.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CoJLF5dbV4k"
type: "gana"
singers:
  -	Mukesh
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pecha podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha podum"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kamaila kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamaila kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">gulanjaka salpa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gulanjaka salpa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">gabalnu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gabalnu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">sillaytta barkili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillaytta barkili"/>
</div>
<div class="lyrico-lyrics-wrapper">sinakoni mellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinakoni mellu"/>
</div>
<div class="lyrico-lyrics-wrapper">silputni nadaya pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silputni nadaya pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">silpikinu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silpikinu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">puringitha puringitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puringitha puringitha"/>
</div>
<div class="lyrico-lyrics-wrapper">puringitha puringitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puringitha puringitha"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyala puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyala puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyala mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyala mamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siththiragupthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siththiragupthan"/>
</div>
<div class="lyrico-lyrics-wrapper">face booku open
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="face booku open"/>
</div>
<div class="lyrico-lyrics-wrapper">panni pakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panni pakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">balance sheetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balance sheetu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanakku panni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakku panni "/>
</div>
<div class="lyrico-lyrics-wrapper">comment onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="comment onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">poduran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduran "/>
</div>
<div class="lyrico-lyrics-wrapper">yaru potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru potta"/>
</div>
<div class="lyrico-lyrics-wrapper">puchikidho avana dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puchikidho avana dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">katturan dharman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katturan dharman"/>
</div>
<div class="lyrico-lyrics-wrapper">yemadharman like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemadharman like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu avana mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu avana mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">thukkuran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thukkuran "/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pecha podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha podum"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kamaila kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamaila kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">gulanjaka salpa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gulanjaka salpa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">gabalnu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gabalnu thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siluki kaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siluki kaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">simbili unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simbili unda"/>
</div>
<div class="lyrico-lyrics-wrapper">sidaila kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sidaila kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pasappu thalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasappu thalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">botti koiyambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="botti koiyambu"/>
</div>
<div class="lyrico-lyrics-wrapper">sojala kothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sojala kothu"/>
</div>
<div class="lyrico-lyrics-wrapper">dubaksu dubaksu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dubaksu dubaksu"/>
</div>
<div class="lyrico-lyrics-wrapper">peggula kickku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peggula kickku"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuchi neechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuchi neechi"/>
</div>
<div class="lyrico-lyrics-wrapper">kallaichi yeravuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallaichi yeravuda"/>
</div>
<div class="lyrico-lyrics-wrapper">thokku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thokku "/>
</div>
<div class="lyrico-lyrics-wrapper">puringitha puringitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puringitha puringitha"/>
</div>
<div class="lyrico-lyrics-wrapper">puringitha puringitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puringitha puringitha"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyala puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyala puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">puriyala mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyala mamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">appartment vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appartment vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">vanghi ikklasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanghi ikklasa"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">audi cara ottikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="audi cara ottikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">altikinae ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="altikinae ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">irukumbothu peethikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukumbothu peethikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">thembula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembula than"/>
</div>
<div class="lyrico-lyrics-wrapper">kedabthiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedabthiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">sethaporam mchinela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaporam mchinela"/>
</div>
<div class="lyrico-lyrics-wrapper">vuttu sombula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vuttu sombula than"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhuduva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhuduva "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pecha podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha podum"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kamaila kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamaila kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">gulanjaka salpa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gulanjaka salpa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">gabalnu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gabalnu thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethalum savu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethalum savu"/>
</div>
<div class="lyrico-lyrics-wrapper">sanikilama savu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanikilama savu"/>
</div>
<div class="lyrico-lyrics-wrapper">niyaithikelama leave vu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyaithikelama leave vu"/>
</div>
<div class="lyrico-lyrics-wrapper">majava povomada savu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majava povomada savu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiroda irukkum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiroda irukkum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vangi tharala tea yi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangi tharala tea yi"/>
</div>
<div class="lyrico-lyrics-wrapper">sethapuram kushitu vandhy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethapuram kushitu vandhy"/>
</div>
<div class="lyrico-lyrics-wrapper">kathinukiraya naya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathinukiraya naya"/>
</div>
<div class="lyrico-lyrics-wrapper">adatha aatam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adatha aatam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">bumiyila aaduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bumiyila aaduva"/>
</div>
<div class="lyrico-lyrics-wrapper">melandhu ponu vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melandhu ponu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">automatika oduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="automatika oduva"/>
</div>
<div class="lyrico-lyrics-wrapper">adupuku than uoodakalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adupuku than uoodakalu"/>
</div>
<div class="lyrico-lyrics-wrapper">sarpukudhan sanakallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarpukudhan sanakallu"/>
</div>
<div class="lyrico-lyrics-wrapper">sappakadhaya orandallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sappakadhaya orandallu"/>
</div>
<div class="lyrico-lyrics-wrapper">keatukada gana sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keatukada gana sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkara barabara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkara barabara "/>
</div>
<div class="lyrico-lyrics-wrapper">garabara under basha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="garabara under basha"/>
</div>
<div class="lyrico-lyrics-wrapper">malu tholu vagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malu tholu vagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vekkatha aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vekkatha aasa"/>
</div>
</pre>
