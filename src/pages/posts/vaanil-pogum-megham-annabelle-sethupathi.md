---
title: "vaanil pogum megham song lyrics"
album: "Annabelle Sethupathi"
artist: "Krishna Kishor"
lyricist: "Uma Devi"
director: "Deepak Sundarrajan"
path: "/albums/annabelle-sethupathi-lyrics"
song: "Vaanil Pogum Megham"
image: ../../images/albumart/annabelle-sethupathi.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IHlo6oAAlE4"
type: "love"
singers:
  - Armaan Malik
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanil Pogum Megam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil Pogum Megam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Theendi Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Theendi Poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhi Veesi Paadal Vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhi Veesi Paadal Vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul Yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Yenguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kadal Neelame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kadal Neelame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Desangal Thaandi Enai Eerkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Desangal Thaandi Enai Eerkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannin Paarvaiyil Kudai Saaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannin Paarvaiyil Kudai Saaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Theendum Bodhu Penne Uyir Kaakiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Theendum Bodhu Penne Uyir Kaakiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkum Dheva Vizhiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkum Dheva Vizhiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Aasai Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Aasai Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa En Kooda Thunaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa En Kooda Thunaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum Dhooram Varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Dhooram Varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Undhan Kaatchi Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Undhan Kaatchi Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kadal Neelame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kadal Neelame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Mazhai Thuliyanaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Mazhai Thuliyanaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi Vandhu Unai Chergiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi Vandhu Unai Chergiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorellaam Paarkiren Uravaaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorellaam Paarkiren Uravaaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaaga Ketkiren Niraiveruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaaga Ketkiren Niraiveruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palithidum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palithidum Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee En Thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ival Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ival Ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkum Dheva Vizhiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkum Dheva Vizhiyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Aasai Mozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Aasai Mozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa En Kooda Thunaiaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa En Kooda Thunaiaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram Yedhu Inimele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Yedhu Inimele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Neelam Varaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Neelam Varaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee En Paadhi Uyire Veerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Paadhi Uyire Veerane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkum Dheva Vizhiyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkum Dheva Vizhiyale"/>
</div>
</pre>
