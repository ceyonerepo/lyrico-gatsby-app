---
title: "ulagam adhira vada song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "Thiyaru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Ulagam Adhira Vada"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6psISenET00"
type: "happy"
singers:
  - MM Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ulagam athira vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam athira vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">unna urasa evanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna urasa evanum"/>
</div>
<div class="lyrico-lyrics-wrapper">unda unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unda unda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">elunthu nimirnthu vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthu nimirnthu vada"/>
</div>
<div class="lyrico-lyrics-wrapper">un ethiri usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ethiri usura"/>
</div>
<div class="lyrico-lyrics-wrapper">konda konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konda konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athiradi puyalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athiradi puyalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">pagai kodu padava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagai kodu padava"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai mothum soolai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai mothum soolai"/>
</div>
<div class="lyrico-lyrics-wrapper">elu veera veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elu veera veera"/>
</div>
<div class="lyrico-lyrics-wrapper">arogara sivaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arogara sivaaaaa"/>
</div>
</pre>
