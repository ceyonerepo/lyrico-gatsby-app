---
title: "jai hanumantha song lyrics"
album: "Sollividava"
artist: "Jassie Gift"
lyricist: "Vijaya Narasimha - Gotturi"
director: "Arjun Sarja"
path: "/albums/sollividava-lyrics"
song: "Jai Hanumantha"
image: ../../images/albumart/sollividava.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-1rBP7jUqkg"
type: "devotional"
singers:
  - S.P. Balasubrahmanyam
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mano-javam maarutha tulya vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mano-javam maarutha tulya vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jitendriyam buddhi mataam varissttam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitendriyam buddhi mataam varissttam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaataatmajam vaanara yuutha mukhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaataatmajam vaanara yuutha mukhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shrii raama duutam shirasa namami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shrii raama duutam shirasa namami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hohoho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hohoho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram jai ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram jai ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram ram ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram ram ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai ram ram ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai ram ram ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram jai ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram jai ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram jai ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram jai ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai hanumantha kesarinandana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai hanumantha kesarinandana"/>
</div>
<div class="lyrico-lyrics-wrapper">Marutiraaya vaanara yodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marutiraaya vaanara yodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayu putra vajrakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayu putra vajrakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheena bandhuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheena bandhuvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera jai hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera jai hanuman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai hanumantha kesarinandana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai hanumantha kesarinandana"/>
</div>
<div class="lyrico-lyrics-wrapper">Marutiraaya vaanara yodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marutiraaya vaanara yodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayu putra vajrakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayu putra vajrakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheena bandhuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheena bandhuvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera jai hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera jai hanuman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raghupathi raghava rajaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghupathi raghava rajaraama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enavae thuthippan engal hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enavae thuthippan engal hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Namoo raama bhaktha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namoo raama bhaktha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninae sarva saktha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninae sarva saktha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramanin uyir thanae hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramanin uyir thanae hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumanin uyir rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumanin uyir rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramanin uyir thanae hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramanin uyir thanae hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumanin uyir rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumanin uyir rama rama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai hanumantha kesarinandana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai hanumantha kesarinandana"/>
</div>
<div class="lyrico-lyrics-wrapper">Marutiraaya vaanara yodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marutiraaya vaanara yodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayu putra vajrakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayu putra vajrakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheena bandhuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheena bandhuvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera jai Hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera jai Hanuman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai jai jai seetha ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai jai seetha ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai jai jai janaki ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai jai jai janaki ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayodhya rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayodhya rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dasharatha rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dasharatha rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarvabhouma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarvabhouma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhandarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhandarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhandarama kodhandarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhandarama kodhandarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhandarama kodhandarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhandarama kodhandarama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhakthinalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakthinalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakthi kandavan hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakthi kandavan hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramanaiyae vendru nindravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramanaiyae vendru nindravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanjeevanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjeevanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumanthu thanthavan hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumanthu thanthavan hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshmananin uyirai kaththavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshmananin uyirai kaththavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu raama manthirathin saaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu raama manthirathin saaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumanin sakthi abaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumanin sakthi abaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha bhakthiyin porulae aanjaneyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha bhakthiyin porulae aanjaneyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakthiyin kadalae aanjaneyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakthiyin kadalae aanjaneyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai hanumanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai hanumanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kapiraayanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapiraayanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam nee thanappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam nee thanappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan thiranthu karunaiyalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan thiranthu karunaiyalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaraiyum kappathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaraiyum kappathappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama dhoodhanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama dhoodhanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendravan hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendravan hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Lankaiyai azhithu vanthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lankaiyai azhithu vanthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seethaiyin patham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seethaiyin patham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paninthavan maaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paninthavan maaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Janakiyin arul petravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janakiyin arul petravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Than idhayathil raaman roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than idhayathil raaman roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatti nindran aanjaneyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatti nindran aanjaneyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum deivam nee thaanaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum deivam nee thaanaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal pechum moochum nee thaanaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal pechum moochum nee thaanaiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai hanumanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai hanumanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kapiraayanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kapiraayanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam nee thanappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam nee thanappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan thiranthu karunaiyalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan thiranthu karunaiyalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaraiyum kappathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaraiyum kappathappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thaa thagida thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagida thaa thagida thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai hanumantha kesarinandana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai hanumantha kesarinandana"/>
</div>
<div class="lyrico-lyrics-wrapper">Marutiraaya vaanara yodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marutiraaya vaanara yodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayu putra vajrakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayu putra vajrakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheena bandhuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheena bandhuvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera jai Hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera jai Hanuman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raghupathi raghava rajaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghupathi raghava rajaraama"/>
</div>
<div class="lyrico-lyrics-wrapper">Enavae thuthippan engal hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enavae thuthippan engal hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Namoo raama bhaktha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namoo raama bhaktha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninae sarva saktha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninae sarva saktha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramanin uyir thanae hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramanin uyir thanae hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumanin uyir rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumanin uyir rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramanin uyir thanae hanuman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramanin uyir thanae hanuman"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanumanin uyir rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumanin uyir rama rama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
</pre>
