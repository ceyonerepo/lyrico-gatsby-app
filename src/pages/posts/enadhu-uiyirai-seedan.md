---
title: "enadhu uiyirai song lyrics"
album: "Seedan"
artist: "Dhina"
lyricist: "Yugabharathi"
director: "Subramaniam Siva"
path: "/albums/seedan-lyrics"
song: "Enadhu Uiyirai"
image: ../../images/albumart/seedan.jpg
date: 2011-02-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Dmrhr7ht4GY"
type: "love"
singers:
  - V.V. Prasanna
  - Aalap Raju
  - Janaki Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sa sa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ma ma ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ma ma ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri ga ri ri ga ma pa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri ga ri ri ga ma pa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa sa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ma ma ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ma ma ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri ga ri ri ga ma pa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri ga ri ri ga ma pa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga anuppavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga anuppavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaigalai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaigalai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaippaduthavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaippaduthavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagazhagaai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagazhagaai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhandhu parakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhu parakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhumedhuvaai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhumedhuvaai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba pirakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba pirakkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga anuppavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga anuppavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa ni sa ri ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa ni sa ri ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ma ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ma ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa ni sa ri ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa ni sa ri ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ma ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ma ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha ma dha paa ri ga pa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha ma dha paa ri ga pa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri ri ma ga ri ne ne saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri ri ma ga ri ne ne saa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyai thurantha thuravipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyai thurantha thuravipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai seeraagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai seeraagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo uranga bayandha tharumipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo uranga bayandha tharumipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu poraadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu poraadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana kugaiyilae anbaana vedhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana kugaiyilae anbaana vedhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alarathuvangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alarathuvangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu alaiyum isaiyenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu alaiyum isaiyenavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam unarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam unarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurudhiyilae sa ri ga ma paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurudhiyilae sa ri ga ma paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanam puriyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanam puriyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa sa ni pa ri ga pa ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa sa ni pa ri ga pa ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ma dha ri ga ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ma dha ri ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ni da pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ni da pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhaiyum kadandha nilaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum kadandha nilaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum neeyaagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum neeyaagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa urugi vazhindha kodhinilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa urugi vazhindha kodhinilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagai aaraayudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagai aaraayudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala vagaiyilae pollaadha kaadhalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vagaiyilae pollaadha kaadhalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodumai thodarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodumai thodarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nimida kodumai idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nimida kodumai idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai varududhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai varududhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaividavum sugam edhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaividavum sugam edhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugi vazhiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugi vazhiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga anuppavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga anuppavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaigalai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaigalai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaippaduthavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaippaduthavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagazhagaai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagazhagaai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhandhu parakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhu parakkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhumedhuvaai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhumedhuvaai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba pirakkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba pirakkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirai mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga anuppavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga anuppavaa"/>
</div>
</pre>
