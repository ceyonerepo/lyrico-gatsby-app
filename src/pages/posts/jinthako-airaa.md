---
title: 'jinthako lyrics'
album: 'Airaa'
artist: 'Sundaramurthy K.S.'
lyricist: 'Ku. Karthik'
director: 'Sarjun KM'
path: '/albums/airaa-song-lyrics'
song: 'Jinthako'
image: ../../images/albumart/airaa.jpg
date: 2019-03-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_ksQfR-fzEU"
type: 'happy'
singers: 
- Sundaramurthy KS
- Sri Radha Bharath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Kaathadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Kaathadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Pakkam Paathadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pakkam Paathadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Uchchathula Irukkuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Uchchathula Irukkuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Gallavulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Gallavulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu Kattaa Dhuddu Serra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattaa Dhuddu Serra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poli Gammu Onnu Thodanguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poli Gammu Onnu Thodanguthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Theriyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Theriyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soundu Koraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu Koraiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanaa Maraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa Maraiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenaa Alaiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaa Alaiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Athiruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Athiruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Viriyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Viriyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartu Patharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Patharuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lite ta Otharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lite ta Otharuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evannukkul Yethu Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evannukkul Yethu Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Therunjavan Evanum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Therunjavan Evanum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meiyila Poiyiruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyila Poiyiruntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadikkul Therivathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadikkul Therivathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Ulagathula Oru Kanakkirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Ulagathula Oru Kanakkirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kadavulukkum Athil Koottirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kadavulukkum Athil Koottirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sari Samamaa Avan Padaikkavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sari Samamaa Avan Padaikkavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Unmaikku Velai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Unmaikku Velai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetho Theriyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho Theriyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soundu Koraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu Koraiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana Maraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Maraiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenaa Alaiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaa Alaiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Athiruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Athiruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Viriyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Viriyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartu Patharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Patharuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lite ta Otharuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lite ta Otharuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthako"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Kaathadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Kaathadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Pakkam Paathadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Pakkam Paathadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Uchchathula Irukkuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Uchchathula Irukkuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Gallavulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Gallavulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu Kattaa Dhuddu Serra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattaa Dhuddu Serra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poli Gammu Onnu Thodanguthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poli Gammu Onnu Thodanguthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Theriyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Theriyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soundu Koraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu Koraiyuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanaa Maraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa Maraiyuthey"/>
</div>
</pre>