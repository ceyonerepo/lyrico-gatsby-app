---
title: "neengalum oorum song lyrics"
album: "Genius"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Suseenthiran"
path: "/albums/genius-lyrics"
song: "Neengalum Oorum"
image: ../../images/albumart/genius.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wiFdCdSaLH4"
type: "happy"
singers:
  - Sreekanth Hariharan
  - Priya Mali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naanum paathuttae irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum paathuttae irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu perum anga inganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu perum anga inganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu pesikittae irukinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu pesikittae irukinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna rendu perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna rendu perum"/>
</div>
<div class="lyrico-lyrics-wrapper">Love pandringala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love pandringala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hmmmmm.mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hmmmmm.mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengalum oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalum oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaippathu madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaippathu madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalum illai "/>
</div>
<div class="lyrico-lyrics-wrapper">karumamum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumamum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalillaamal arivil irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalillaamal arivil irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengalum oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalum oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaippathu madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaippathu madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalum illai "/>
</div>
<div class="lyrico-lyrics-wrapper">karumamum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumamum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalillaamal arivil irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalillaamal arivil irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyil irunthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyil irunthu "/>
</div>
<div class="lyrico-lyrics-wrapper">baaram iranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baaram iranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam nazhuvi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam nazhuvi "/>
</div>
<div class="lyrico-lyrics-wrapper">vayittril urula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayittril urula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathimaar ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathimaar ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">dhevathaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhevathaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagupparai ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagupparai ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">pookkal pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookkal pookka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasanthu pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasanthu pona "/>
</div>
<div class="lyrico-lyrics-wrapper">kanitham kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanitham kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasantha kaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha kaala "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasanai veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasanai veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo nadakkuthu vazhkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo nadakkuthu vazhkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla theriyala varthaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla theriyala varthaiyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengalum oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalum oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaippathu madhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaippathu madhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalum illai "/>
</div>
<div class="lyrico-lyrics-wrapper">karumamum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumamum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalillaamal arivil irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalillaamal arivil irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivalae enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalae enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham aaginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham aaginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalae enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalae enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai aaginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai aaginaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu karu karuvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu karu karuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesayil mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesayil mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeruthae yeruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeruthae yeruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mada mada madavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mada mada madavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathil azhutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathil azhutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaruthae maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaruthae maaruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengalai purintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalai purintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithargal mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithargal mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengalai kadanthu sellungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalai kadanthu sellungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengalai puriya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalai puriya "/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyaa vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyaa vittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai moodi kollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai moodi kollungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennathu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennathu puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengalai purintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalai purintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithargal mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithargal mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengalai kadanthu sellungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalai kadanthu sellungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengalai puriya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengalai puriya "/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyaa vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyaa vittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai moodi kollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai moodi kollungal"/>
</div>
</pre>
