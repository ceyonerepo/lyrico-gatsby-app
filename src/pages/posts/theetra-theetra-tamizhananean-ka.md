---
title: "theetra theetra song lyrics"
album: "Tamizhananean Ka"
artist: "Vynod Subramaniam"
lyricist: "Vynod Subramaniam"
director: "Sathish Ramakrishnan"
path: "/albums/tamizhananean-ka-song-lyrics"
song: "Theetra Theetra"
image: ../../images/albumart/tamizhananean-ka.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u-ja9TPkFqE"
type: "mass"
singers:
  - Unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pen thantha boomiye paarellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen thantha boomiye paarellam "/>
</div>
<div class="lyrico-lyrics-wrapper">poo manam unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo manam unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">kan munne theivam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan munne theivam than"/>
</div>
<div class="lyrico-lyrics-wrapper">avale nan konji vilaiyadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avale nan konji vilaiyadum"/>
</div>
<div class="lyrico-lyrics-wrapper">thangaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangaiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">magalum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magalum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">nathiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">naadum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">nam mozhiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam mozhiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">imaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">nam uyirum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam uyirum avale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaniya pona van kodumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya pona van kodumai"/>
</div>
<div class="lyrico-lyrics-wrapper">pani kondran manam konnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani kondran manam konnu"/>
</div>
<div class="lyrico-lyrics-wrapper">panam thinnu uyir illa otta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam thinnu uyir illa otta"/>
</div>
<div class="lyrico-lyrics-wrapper">udal kondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal kondu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">setholiyum udaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setholiyum udaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">suga maayai thoonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suga maayai thoonda"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamam thevai ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamam thevai ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">athai vilai porul aaki vikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai vilai porul aaki vikiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannukulla anal adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla anal adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla parai adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla parai adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">thotathellam thoolagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotathellam thoolagum"/>
</div>
<div class="lyrico-lyrics-wrapper">paaraiyilum payir mulaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaraiyilum payir mulaikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pen thantha boomiye paarellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen thantha boomiye paarellam "/>
</div>
<div class="lyrico-lyrics-wrapper">poo manam unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo manam unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">kan munne theivam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan munne theivam than"/>
</div>
<div class="lyrico-lyrics-wrapper">avale nan konji vilaiyadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avale nan konji vilaiyadum"/>
</div>
<div class="lyrico-lyrics-wrapper">thangaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangaiyum avale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puli poranth boomila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli poranth boomila"/>
</div>
<div class="lyrico-lyrics-wrapper">pulli maana oonai theenduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulli maana oonai theenduma"/>
</div>
<div class="lyrico-lyrics-wrapper">maanum puliya maaruminge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanum puliya maaruminge"/>
</div>
<div class="lyrico-lyrics-wrapper">por gunam tamilan la saguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="por gunam tamilan la saguma"/>
</div>
<div class="lyrico-lyrics-wrapper">sandaila muthuga kaatra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandaila muthuga kaatra "/>
</div>
<div class="lyrico-lyrics-wrapper">palakam engaluku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palakam engaluku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ponna thottavana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ponna thottavana "/>
</div>
<div class="lyrico-lyrics-wrapper">ennaikum nanga vittathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaikum nanga vittathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">pothanja kathaya thirupi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothanja kathaya thirupi"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthu murasu adida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthu murasu adida"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhanu thamizhananen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhanu thamizhananen"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhananen thamizhananen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhananen thamizhananen"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhananen thamizhananen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhananen thamizhananen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">penne penne payanthathu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne penne payanthathu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">un paasam vaithathu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paasam vaithathu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam mattum patathu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam mattum patathu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini illai pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini illai pothum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">manase manase ennachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase manase ennachu"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan manasu kallachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan manasu kallachu"/>
</div>
<div class="lyrico-lyrics-wrapper">penne penne ennachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne penne ennachu"/>
</div>
<div class="lyrico-lyrics-wrapper">nimirnthu nillu payam pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirnthu nillu payam pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">paathi ulagam pennachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathi ulagam pennachu"/>
</div>
<div class="lyrico-lyrics-wrapper">tamizha ipo ennachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamizha ipo ennachu"/>
</div>
<div class="lyrico-lyrics-wrapper">un veeram ellam mannachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un veeram ellam mannachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnuga ponnuga uyir pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnuga ponnuga uyir pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">penne nee kaali kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nee kaali kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thotavan gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thotavan gaali gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">tamizhan yarunu kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamizhan yarunu kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ponnu saami saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ponnu saami saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theetra theetra vaala theetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetra theetra vaala theetra"/>
</div>
<div class="lyrico-lyrics-wrapper">oothra paala oothra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothra paala oothra"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatra kaatra thirupi kaatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatra kaatra thirupi kaatra"/>
</div>
<div class="lyrico-lyrics-wrapper">kanthanin vela thookra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanthanin vela thookra"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaka kaaka kanaga vel kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaka kaaka kanaga vel kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">noka noka nodiyil noka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noka noka nodiyil noka"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaka thaaka thadayara thaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaka thaaka thadayara thaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">paarka paarka paavam podi pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarka paarka paavam podi pada"/>
</div>
</pre>
