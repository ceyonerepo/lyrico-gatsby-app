---
title: "putukku jara jara song lyrics"
album: "Guvva Gorinka"
artist: "Bobbili Suresh"
lyricist: "Abhinaya Srinivas"
director: "Mohan Bammidi"
path: "/albums/guvva-gorinka-lyrics"
song: "Putukku Jara Jara"
image: ../../images/albumart/guvva-gorinka.jpg
date: 2020-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/GecOeDh5Hm4"
type: "happy"
singers:
  - Rahul Ramakrishna
  - Suresh Bobbili
  - Priyadarshi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Are dhaggarivaallani dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are dhaggarivaallani dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesi aataadisthadhi kaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesi aataadisthadhi kaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi android majaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi android majaakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadavani mucchata racchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadavani mucchata racchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchi pichhekkisthadhi kaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchi pichhekkisthadhi kaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu andhulona dhiginaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu andhulona dhiginaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are faceu booklo pattesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are faceu booklo pattesi"/>
</div>
<div class="lyrico-lyrics-wrapper">You tubel tho chuttesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You tubel tho chuttesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatsaplo nettesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsaplo nettesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Twitter tho ninu kattesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitter tho ninu kattesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Like lu share lu commentlatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like lu share lu commentlatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee lifae lockai poindhi ra bhay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee lifae lockai poindhi ra bhay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putuk putuk putuk putuk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putuk putuk putuk putuk"/>
</div>
<div class="lyrico-lyrics-wrapper">Putuk jara jara dupukme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putuk jara jara dupukme"/>
</div>
<div class="lyrico-lyrics-wrapper">Social media dhumaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Social media dhumaarame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penchuthundhi raa virodhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penchuthundhi raa virodhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sruthi minchuthondhiraa vinodhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sruthi minchuthondhiraa vinodhame"/>
</div>
</pre>
