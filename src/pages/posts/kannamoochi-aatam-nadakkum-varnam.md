---
title: "kannamoochi aatam nadakkum song lyrics"
album: "Varnam"
artist: "Isaac Thomas Kottukapally"
lyricist: "Na. Muthukumar"
director: "SM Raju"
path: "/albums/varnam-lyrics"
song: "Kannamoochi Aatam Nadakkum"
image: ../../images/albumart/varnam.jpg
date: 2011-10-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bh55N5NjH5w"
type: "sad"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannamoochchi Aattam Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamoochchi Aattam Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna Moodi Kaatulae Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna Moodi Kaatulae Irukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">thaedi Thaedi Tholainjae Ponom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaedi Thaedi Tholainjae Ponom"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapadhu Ellaam Nesam Dhaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapadhu Ellaam Nesam Dhaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjilae Aasa Vandhaa Vali Dhaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjilae Aasa Vandhaa Vali Dhaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannamoochchi Aattam Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamoochchi Aattam Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna Moodi Kaatulae Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna Moodi Kaatulae Irukkom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan Thirakkum Munnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan Thirakkum Munnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal Selai Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Selai Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">odainju Ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odainju Ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan Varenja Oviyaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan Varenja Oviyaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">vannam Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannam Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">saayam Ponadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saayam Ponadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thannan Thanniyaa Naethu Irundhaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannan Thanniyaa Naethu Irundhaen"/>
</div>
<div class="lyrico-lyrics-wrapper">onnai Kanda Peragu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnai Kanda Peragu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam Enakku Pidichchadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam Enakku Pidichchadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum Ippo Velagittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum Ippo Velagittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">engae Poiyi Azhuvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engae Poiyi Azhuvadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannamoochchi Aattam Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamoochchi Aattam Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna Moodi Kaatulae Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna Moodi Kaatulae Irukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">thaedi Thaedi Tholainjae Ponom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaedi Thaedi Tholainjae Ponom"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapadhu Ellaam Nesam Dhaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapadhu Ellaam Nesam Dhaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjilae Aasa Vandhaa Vali Dhaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjilae Aasa Vandhaa Vali Dhaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aala Mara Kelai Maelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala Mara Kelai Maelae"/>
</div>
<div class="lyrico-lyrics-wrapper">vaeppam Chedi Molaichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaeppam Chedi Molaichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">poopadhilaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poopadhilaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai Paarththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai Paarththum"/>
</div>
<div class="lyrico-lyrics-wrapper">thirundhalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirundhalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo Ooru Sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo Ooru Sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">maara Villaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara Villaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna Nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna Nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">enna Kanavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna Kanavo"/>
</div>
<div class="lyrico-lyrics-wrapper">aaththukulla Vizhindha Seragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaththukulla Vizhindha Seragu"/>
</div>
<div class="lyrico-lyrics-wrapper">karaikku Meendum Thirumbumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaikku Meendum Thirumbumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillum Karainjutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillum Karainjutta"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumba Varaiya Mudiyumaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumba Varaiya Mudiyumaa?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannamoochchi Aattam Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamoochchi Aattam Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna Moodi Kaatulae Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna Moodi Kaatulae Irukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">thaedi Thaedi Tholainjae Ponom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaedi Thaedi Tholainjae Ponom"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapadhu Ellaam Nesam Dhaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapadhu Ellaam Nesam Dhaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjilae Aasa Vandhaa Vali Dhaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjilae Aasa Vandhaa Vali Dhaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannamoochchi Aattam Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamoochchi Aattam Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna Moodi Kaatulae Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna Moodi Kaatulae Irukkom"/>
</div>
</pre>
