---
title: "bathike haayiga song lyrics"
album: "Anubhavinchu Raja"
artist: "Gopi Sundar"
lyricist: "Bhaskarabhatla"
director: "Sreenu Gavireddy"
path: "/albums/anubhavinchu-raja-lyrics"
song: "Bathike Haayiga"
image: ../../images/albumart/anubhavinchu-raja.jpg
date: 2021-11-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6AfXJY43IK8"
type: "happy"
singers:
  - Deepu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bathike Haayiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathike Haayiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Malli Malli Raadhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Malli Malli Raadhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathidhi Anthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathidhi Anthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoothaddhamlonchi Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoothaddhamlonchi Choodaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathikeyy Haayiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathikeyy Haayiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Malli Malli Raadhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Malli Malli Raadhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathidhi Anthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathidhi Anthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoothaddhamlonchi Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoothaddhamlonchi Choodaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachhithe Kalipesukuporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhithe Kalipesukuporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulukoku Ye Okkarini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulukoku Ye Okkarini"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeyy Nuv Sardhukuporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeyy Nuv Sardhukuporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhakunnaa Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhakunnaa Gaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manase Padi Hatthukuporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Padi Hatthukuporaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Ella Ee Bahumaanaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Ella Ee Bahumaanaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gola Godavalatho Nimpeyyakura Daanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gola Godavalatho Nimpeyyakura Daanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tellaari Levagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tellaari Levagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Gajibijigaa Paruguleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gajibijigaa Paruguleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Jaanedu Pottakosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Jaanedu Pottakosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinadhina Gandamraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinadhina Gandamraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu O Saari Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu O Saari Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadu Sukhapaduthu Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadu Sukhapaduthu Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaage Vaadu Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaage Vaadu Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabadu thunnaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabadu thunnaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanuke Epudaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke Epudaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manasuni Noppisthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manasuni Noppisthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Porapaate Chesthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Porapaate Chesthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponle Ani Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponle Ani Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Anukunte Vaadu Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Anukunte Vaadu Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manavaade Ayipothaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manavaade Ayipothaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathikeyy Haayiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathikeyy Haayiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Malli Malli Raadhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Malli Malli Raadhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathidhi Anthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathidhi Anthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoothaddhamlonchi Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoothaddhamlonchi Choodaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kopaale Penchukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopaale Penchukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavesham Anchununte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavesham Anchununte"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Kantiki Buddhudaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Kantiki Buddhudaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Shatruvu Ayipodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shatruvu Ayipodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradagaa Palakaristhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradagaa Palakaristhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavve Chilakaristhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavve Chilakaristhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhantu Evvadaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhantu Evvadaina "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooranguntaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooranguntaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Oka Lopam Unnode Manishavuthaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Oka Lopam Unnode Manishavuthaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledho Ayipodaa Devudilaa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledho Ayipodaa Devudilaa Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Epudoo Edhutollo Thappulne Vethiketappudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudoo Edhutollo Thappulne Vethiketappudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Manishe Ani Gurthu Chesukovaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Manishe Ani Gurthu Chesukovaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haayigaa Raadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayigaa Raadhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthalaa Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthalaa Choodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayigaa Raadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayigaa Raadhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthalaa Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthalaa Choodaka"/>
</div>
</pre>
