---
title: "kaavu ulla kalludi song lyrics"
album: "Parris Jeyaraj"
artist: "Santhosh Narayanan"
lyricist: "Asal Kolaar, Rokesh"
director: "Johnson K"
path: "/albums/parris-jeyaraj-lyrics"
song: "Kaavu Ulla Kalludi"
image: ../../images/albumart/parris-jeyaraj.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2cWqgCH_gcE"
type: "Enjoy"
singers:
  - Asal Kolaar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anttaa aakittaa oru girl-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anttaa aakittaa oru girl-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambelnu illadha love feel-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambelnu illadha love feel-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantaa macha adi dhool-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantaa macha adi dhool-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Inmeltu ava thaan da en aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inmeltu ava thaan da en aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taanadi turn adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taanadi turn adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula marnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula marnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakilla sornadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakilla sornadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatriya karnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatriya karnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla kalludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla kalludi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala odi vandhu sollu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala odi vandhu sollu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kamarkatta vaayila mellu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kamarkatta vaayila mellu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganaava like panna bell adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganaava like panna bell adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava pagalu pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava pagalu pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilava pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilava pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolippa jolippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolippa jolippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolippa jolippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolippa jolippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal nuni-larndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal nuni-larndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai mudi varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai mudi varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakke thelippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakke thelippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum thelippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum thelippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelippa thelippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelippa thelippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava raatuna gairu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava raatuna gairu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava paathuta thevalla raathiri soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava paathuta thevalla raathiri soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava road-tula thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava road-tula thaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">En goindhamma pasanga paarvaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En goindhamma pasanga paarvaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ac bar-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ac bar-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnanda puttappu kudukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanda puttappu kudukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">En little idhayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En little idhayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottalam madichi tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottalam madichi tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee koopta husband-u sthaanathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee koopta husband-u sthaanathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthava sevai seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthava sevai seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dress-ah ellam thovikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dress-ah ellam thovikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni odla adaipula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni odla adaipula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla kalludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla kalludi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala odi vandhu sollu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala odi vandhu sollu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kamarkatta vaayila mellu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kamarkatta vaayila mellu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganaava like panna bell adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganaava like panna bell adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ganaava like panna bella adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ganaava like panna bella adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ganaava like panna….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ganaava like panna…."/>
</div>
<div class="lyrico-lyrics-wrapper">Bella adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bella adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava artic kadalula uruvaana suzhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava artic kadalula uruvaana suzhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikinnu adikkuren saaigandha peralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikinnu adikkuren saaigandha peralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittunaalum inikkura kuyiloda kuralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittunaalum inikkura kuyiloda kuralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta maadiyila ava nenappula porandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta maadiyila ava nenappula porandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalu full-ah thechikichchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu full-ah thechikichchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaila naan enchimutchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaila naan enchimutchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala naan solla vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala naan solla vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalan-naa moochi putchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalan-naa moochi putchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhura illama oda mudiyuma jockey-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhura illama oda mudiyuma jockey-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey figure-u usaarna dhowlathada ookayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey figure-u usaarna dhowlathada ookayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaiya maruthada ava sonna vaakkiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaiya maruthada ava sonna vaakkiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh manaiviya ava vandha adhu dhaan en baakiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh manaiviya ava vandha adhu dhaan en baakiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnanda puttappu kudukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanda puttappu kudukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">En little idhayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En little idhayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottalam madichi tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottalam madichi tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee koopta husband-u sthaanathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee koopta husband-u sthaanathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthava sevai seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthava sevai seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dress-ah ellam thovikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dress-ah ellam thovikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni odla adaipula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni odla adaipula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaava Kaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava Kaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla kalludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla kalludi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala odiyandhu sollu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala odiyandhu sollu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kamarkatta vaayila mellu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kamarkatta vaayila mellu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganaava like panna bell adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganaava like panna bell adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anttaa aakittaa oru girl-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anttaa aakittaa oru girl-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambelnu illadha love feel-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambelnu illadha love feel-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantaa macha adi dhool-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantaa macha adi dhool-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Inmeltu ava thaan da en aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inmeltu ava thaan da en aalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taanadi turn adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taanadi turn adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula marnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula marnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakilla sornadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakilla sornadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatriya karnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatriya karnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaava ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaava ulla kalludi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaava ulla kalludi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala odi vandhu sollu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala odi vandhu sollu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kamarkatta vaayila mellu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kamarkatta vaayila mellu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganaava like panna bell adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganaava like panna bell adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ganaava like panna bella adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ganaava like panna bella adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ganaava like panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ganaava like panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Bella adi….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bella adi…."/>
</div>
</pre>
