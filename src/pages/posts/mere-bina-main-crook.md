---
title: "mere bina song lyrics"
album: "Crook"
artist: "Pritam - Babbu Maan"
lyricist: "Kumaar"
director: "Mohit Suri"
path: "/albums/crook-lyrics"
song: "Mere Bina Main"
image: ../../images/albumart/crook.jpg
date: 2010-10-08
lang: hindi
youtubeLink: "https://www.youtube.com/embed/XvUSsh3gdO4"
type: "love"
singers:
  - Nikhil D'Souza
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mere bina main rehne laga hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere bina main rehne laga hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri hawa mein behne laga hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri hawa mein behne laga hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane main kaise tera hua hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane main kaise tera hua hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe to lagta hai main shayad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe to lagta hai main shayad"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere dil ki dua hoon haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere dil ki dua hoon haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhko jo paya aaha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko jo paya aaha!"/>
</div>
<div class="lyrico-lyrics-wrapper">To jeena aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To jeena aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab yeh lamha thehar jaaye tham jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab yeh lamha thehar jaaye tham jaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bas jaaye hum dono ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas jaaye hum dono ke darmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas jaaye hum dono ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas jaaye hum dono ke darmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehle se zyada main jee raha hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle se zyada main jee raha hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Jabse main tere dil se juda hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse main tere dil se juda hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Raahon pe teri main to chala hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raahon pe teri main to chala hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu meri manzil hai tere kadmon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri manzil hai tere kadmon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas rukne laga hoon haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas rukne laga hoon haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhko jo paya aaha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko jo paya aaha!"/>
</div>
<div class="lyrico-lyrics-wrapper">To jeena aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To jeena aaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ab yeh lamha thehar jaaye tham jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab yeh lamha thehar jaaye tham jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas jaaye hum dono ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas jaaye hum dono ke darmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas jaaye hum dono ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas jaaye hum dono ke darmiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri nazar mein nayi si adaa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri nazar mein nayi si adaa hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naya sa nasha bhi ghula hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naya sa nasha bhi ghula hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayi dinon se bandha tha baadal jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayi dinon se bandha tha baadal jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere hi baalon mein khula hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere hi baalon mein khula hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri hadon mein meri basar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri hadon mein meri basar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab tujhe bhi jaana kidhar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab tujhe bhi jaana kidhar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan rahe tu main woh jahan hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan rahe tu main woh jahan hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Jise jiye tu main woh sama hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jise jiye tu main woh sama hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri wajah se naya naya hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri wajah se naya naya hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehle kaha na maine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle kaha na maine"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab yeh tumse kehne laga hoon haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab yeh tumse kehne laga hoon haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhko jo paya aaha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhko jo paya aaha!"/>
</div>
<div class="lyrico-lyrics-wrapper">To jeena aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To jeena aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab yeh lamha thehar jaaye tham jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab yeh lamha thehar jaaye tham jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas jaaye hum dono ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas jaaye hum dono ke darmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas jaaye hum dono ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas jaaye hum dono ke darmiyan"/>
</div>
</pre>
