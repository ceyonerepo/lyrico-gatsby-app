---
title: "awesome song lyrics"
album: "Aadavallu Meeku Johaarlu"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Kishore Tirumala"
path: "/albums/aadavallu-meeku-johaarlu-lyrics"
song: "Awesome"
image: ../../images/albumart/aadavallu-meeku-johaarlu.jpg
date: 2022-03-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9v0yAdxgFA4"
type: "happy"
singers:
  - Sagar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enni Enni Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Enni Enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Maatalaadukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Maatalaadukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Konni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Konni"/>
</div>
<div class="lyrico-lyrics-wrapper">Migilipovaram Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migilipovaram Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Entha Entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Entha Entha"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Dhooramunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Dhooramunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Pakkanunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Pakkanunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Kalagadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Kalagadam Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baagunnaava Ani Nuvvadigaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagunnaava Ani Nuvvadigaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Baadhalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Baadhalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaripovadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaripovadam Awesome"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhocheshaava Ani O Maatannaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhocheshaava Ani O Maatannaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Aakale Maayamavvadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Aakale Maayamavvadam Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enni Enni Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Enni Enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Maatalaadukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Maatalaadukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Konni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Konni"/>
</div>
<div class="lyrico-lyrics-wrapper">Migilipovaram Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migilipovaram Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Entha Entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Entha Entha"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Dhooramunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Dhooramunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Pakkanunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Pakkanunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Kalagadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Kalagadam Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Kaalamu Inni Raathrulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kaalamu Inni Raathrulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaaga Nuvvalle Kaburle Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaaga Nuvvalle Kaburle Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Vyarthamaayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Vyarthamaayane"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni Rojulu Rendu Kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Rojulu Rendu Kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaaga Kalalne Kathalne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaaga Kalalne Kathalne"/>
</div>
<div class="lyrico-lyrics-wrapper">Choose Veele Lekapoyene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choose Veele Lekapoyene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Nannu Kalavamanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nannu Kalavamanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chotu Ekkadunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotu Ekkadunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O Ganta Mundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ganta Mundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Raavadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Raavadam Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inti Varaku Saaganampi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Varaku Saaganampi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedukolu Anni Ventane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedukolu Anni Ventane"/>
</div>
<div class="lyrico-lyrics-wrapper">Phone Lo Kalavadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phone Lo Kalavadam Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakentha Nachhina Nee Intha Nachhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakentha Nachhina Nee Intha Nachhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhennainaa Chee Antu Chaa Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhennainaa Chee Antu Chaa Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti Evoti Thitlu Kalpanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti Evoti Thitlu Kalpanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Panochhinaa Maa Amme Cheppina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Panochhinaa Maa Amme Cheppina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathoti Neekedho Panundhi Annaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathoti Neekedho Panundhi Annaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevaipe Parugu Thiyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevaipe Parugu Thiyyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku Ishtamaindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Ishtamaindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Nuvvu Cheppagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Nuvvu Cheppagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Ishtame Maaripovadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ishtame Maaripovadam Awesome"/>
</div>
<div class="lyrico-lyrics-wrapper">Taj Mahal Andam Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taj Mahal Andam Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Poguduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Poguduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Shah Jahan Ni Nene Avvadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shah Jahan Ni Nene Avvadam Awesome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melkonnaava Ani Nuvvu Adigaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melkonnaava Ani Nuvvu Adigaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Niddhare Sorry Cheppadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Niddhare Sorry Cheppadam Awesome"/>
</div>
<div class="lyrico-lyrics-wrapper">Tellaaripoyindha Ani Phone Ye Pettaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tellaaripoyindha Ani Phone Ye Pettaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Sooryudante Ollu Mandadam Awesome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Sooryudante Ollu Mandadam Awesome"/>
</div>
</pre>
