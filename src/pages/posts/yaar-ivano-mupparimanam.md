---
title: "yaar ivano song lyrics"
album: "Mupparimanam"
artist: "G V Prakash Kumar"
lyricist: "Na Muthu Kumar"
director: "Adhiroopan"
path: "/albums/mupparimanam-lyrics"
song: "Yaar Ivano"
image: ../../images/albumart/mupparimanam.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wG682j2FAUk"
type: "mass"
singers:
  - Santhosh Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaro ivano 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro ivano "/>
</div>
<div class="lyrico-lyrics-wrapper">enghirundhu vandha puyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enghirundhu vandha puyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivano kannil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivano kannil "/>
</div>
<div class="lyrico-lyrics-wrapper">rendum kodai veyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum kodai veyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivano 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivano "/>
</div>
<div class="lyrico-lyrics-wrapper">theriyavillaiiieeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyavillaiiieeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupai pola seeri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupai pola seeri "/>
</div>
<div class="lyrico-lyrics-wrapper">paindhu vandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paindhu vandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaar ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Panniyai pola eeram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panniyai pola eeram "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil kondavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil kondavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaar ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrin paadhai yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin paadhai yenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaral solla koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaral solla koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idamvalam sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idamvalam sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai thandi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai thandi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai meedhu nindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai meedhu nindraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroyaarooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroyaarooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovai pola ivan nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai pola ivan nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari ponadhu indru konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari ponadhu indru konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullei pola ivan keeri paikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullei pola ivan keeri paikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Aayachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Aayachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavai eera vizhi anjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai eera vizhi anjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai enghum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai enghum "/>
</div>
<div class="lyrico-lyrics-wrapper">illai thanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai thanjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna aaghum ini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna aaghum ini "/>
</div>
<div class="lyrico-lyrics-wrapper">kangal kaangiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal kaangiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanal neer katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanal neer katchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhithunaiya varum paghaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhithunaiya varum paghaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna uravoo ini yenna mudivoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna uravoo ini yenna mudivoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal illa vidukathaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal illa vidukathaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroyaarooyaaroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroyaarooyaaroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro ivano enghirundhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro ivano enghirundhu "/>
</div>
<div class="lyrico-lyrics-wrapper">vandha puyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandha puyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivano kannil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivano kannil "/>
</div>
<div class="lyrico-lyrics-wrapper">rendum kodai veyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum kodai veyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivano 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivano "/>
</div>
<div class="lyrico-lyrics-wrapper">theriyavillaiiieeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyavillaiiieeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupai pola seeri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupai pola seeri "/>
</div>
<div class="lyrico-lyrics-wrapper">paindhu vandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paindhu vandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaar ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Panniyai pola eeram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panniyai pola eeram "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil kondavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil kondavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yaar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaar ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrin paadhai yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin paadhai yenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaral solla koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaral solla koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idamvalam sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idamvalam sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai thandi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai thandi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai meedhu nindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai meedhu nindraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroyaarooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroyaarooo"/>
</div>
</pre>
