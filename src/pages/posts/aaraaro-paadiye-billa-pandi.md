---
title: "aaraaro paadiye song lyrics"
album: "Billa Pandi"
artist: "Ilayavan"
lyricist: "Kalaikumar"
director: "Raj Sethupathy"
path: "/albums/billa-pandi-lyrics"
song: "Aaraaro Paadiye"
image: ../../images/albumart/billa-pandi.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/p9bL0nk1Vl4"
type: "melody"
singers:
  - Madhu Balakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aaraaro paadiye nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaraaro paadiye nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaya maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaya maara"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar yaaro pesiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar yaaro pesiya"/>
</div>
<div class="lyrico-lyrics-wrapper">vaartha kaatha poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaartha kaatha poga"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagame othukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagame othukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">usurula pathukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurula pathukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">manasara thanga yenguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasara thanga yenguven"/>
</div>
<div class="lyrico-lyrics-wrapper">nan maru senmam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan maru senmam "/>
</div>
<div class="lyrico-lyrics-wrapper">vaagi thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaagi thaanguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaraaro paadiye nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaraaro paadiye nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaya maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaya maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saathi sanam athanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi sanam athanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaaga maranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaaga maranthene"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pola vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pola vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">enakulla pooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakulla pooka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">satham illa saamathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham illa saamathula"/>
</div>
<div class="lyrico-lyrics-wrapper">urangaama kedanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaama kedanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">kula saamy pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kula saamy pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thala saamy kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala saamy kaakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inime nithamume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime nithamume"/>
</div>
<div class="lyrico-lyrics-wrapper">sugame suthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugame suthidume"/>
</div>
<div class="lyrico-lyrics-wrapper">nesame valum varaikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesame valum varaikume"/>
</div>
<div class="lyrico-lyrics-wrapper">en paasam nelaikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paasam nelaikume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaraaro paadiye nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaraaro paadiye nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaya maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaya maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee sonangi nikaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonangi nikaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">thaviya thavichene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaviya thavichene"/>
</div>
<div class="lyrico-lyrics-wrapper">adiyaathi naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiyaathi naane"/>
</div>
<div class="lyrico-lyrics-wrapper">marunthaagi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marunthaagi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paasam ulla pacha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasam ulla pacha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">pasi thaagam arinjene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi thaagam arinjene"/>
</div>
<div class="lyrico-lyrics-wrapper">athu theera naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu theera naane"/>
</div>
<div class="lyrico-lyrics-wrapper">aan thayum aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aan thayum aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadippa nee alutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadippa nee alutha"/>
</div>
<div class="lyrico-lyrics-wrapper">nesamaa naan aluthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesamaa naan aluthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thudipen mega thiriyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudipen mega thiriyila"/>
</div>
<div class="lyrico-lyrics-wrapper">naan yaagam valakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan yaagam valakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">saaga varathaye naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaga varathaye naan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanga uruguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanga uruguren"/>
</div>
</pre>
