---
title: "pilla naa song lyrics"
album: "Utthara"
artist: "Suresh bobbili"
lyricist: "Raju"
director: "Thirupathi Sr"
path: "/albums/utthara-lyrics"
song: "Pilla Naa"
image: ../../images/albumart/utthara.jpg
date: 2020-01-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/aEFle8PY2XE"
type: "love"
singers:
  - Suresh Bobbili
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pilla naa gundenu patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla naa gundenu patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Laagake dhaaram katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laagake dhaaram katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee choopey cheemai kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee choopey cheemai kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mante repenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mante repenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee soke maagani matti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee soke maagani matti"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvake puvvula thotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvake puvvula thotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvellaa nanne chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvellaa nanne chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Usure thiyakule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usure thiyakule"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharamlo thene pattuni motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharamlo thene pattuni motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarruna laagey paruvamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarruna laagey paruvamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogo Sotthunu Dhochey ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogo Sotthunu Dhochey ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi paduthu muni maapullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi paduthu muni maapullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhaata yeddham cheyro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhaata yeddham cheyro"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabaduthu thadi thaapam penchey ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabaduthu thadi thaapam penchey ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Jara jara jara jara jara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara jara jara jara jara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gichchinaave gunde kanne pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gichchinaave gunde kanne pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavalane gandipetakaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavalane gandipetakaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu kottinaave vannelaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu kottinaave vannelaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara kara kara kara kara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara kara kara kara kara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammukunte nuvvu midha padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammukunte nuvvu midha padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atthiregi aada eeda nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atthiregi aada eeda nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chetulesthey siggu podha mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chetulesthey siggu podha mari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla naa gundenu patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla naa gundenu patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Laagake dhaaram katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laagake dhaaram katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee choopey cheemai kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee choopey cheemai kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mante repenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mante repenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Champa paina muddhu pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champa paina muddhu pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chekkerelu oo retattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chekkerelu oo retattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikedho penchinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikedho penchinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondalu pindi chesetattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondalu pindi chesetattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soyagaalu thene pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soyagaalu thene pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattinaave kanchi pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattinaave kanchi pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manthramedho vesinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthramedho vesinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde jaari poyetattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde jaari poyetattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuko yennadu leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuko yennadu leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaanthaalai naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaanthaalai naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeragaa pichchaga nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeragaa pichchaga nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chichhe pettakuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chichhe pettakuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Sootigaa soodhai guchhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sootigaa soodhai guchhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyyani dhaaham penchakuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyyani dhaaham penchakuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruga neeruga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruga neeruga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thadapaku ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thadapaku ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Bira bira bira bira bira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bira bira bira bira bira"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukunta ninne nangaanaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukunta ninne nangaanaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti ganta kottagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti ganta kottagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu vachhi pothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu vachhi pothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari pichhi pichhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari pichhi pichhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira gira gira gira gira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira gira gira gira gira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiragaddhayya na venta padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiragaddhayya na venta padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada gaali thaakindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada gaali thaakindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari jaarukuntavayya komaloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari jaarukuntavayya komaloki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunji naave gende guttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunji naave gende guttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaaripoke thene pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaripoke thene pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo unna rasapattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo unna rasapattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopisthaane okka naitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopisthaane okka naitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvamantha panchi pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvamantha panchi pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu bottu jaare tattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu bottu jaare tattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamu vela jaathara chestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamu vela jaathara chestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvula dhaaham theere tattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvula dhaaham theere tattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoraga nanne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoraga nanne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammani kaaraa repakuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammani kaaraa repakuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannela vampulu dhochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannela vampulu dhochi"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne champakuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne champakuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula kolaataala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula kolaataala"/>
</div>
<div class="lyrico-lyrics-wrapper">maatuku nanne laagakuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatuku nanne laagakuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayyaram okey ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayyaram okey ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Temptayyi pothaav ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Temptayyi pothaav ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Jara jara jara jara jara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara jara jara jara jara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachchi pove osi kanne pori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachchi pove osi kanne pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu ekamayyi ika lokamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu ekamayyi ika lokamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheddham dhaadhaariri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheddham dhaadhaariri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sara sara sara sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara sara sara sara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaaruthundhi onti koka mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaruthundhi onti koka mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami ranga sandhe vela soku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami ranga sandhe vela soku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaaviloni nuvvu dhooku mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaviloni nuvvu dhooku mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaa na pillaa na ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaa na pillaa na ohhh"/>
</div>
</pre>
