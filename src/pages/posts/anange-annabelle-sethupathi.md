---
title: "anange song lyrics"
album: "Annabelle Sethupathi"
artist: "Krishna Kishor"
lyricist: "Vivek"
director: "Deepak Sundarrajan"
path: "/albums/annabelle-sethupathi-lyrics"
song: "Anange"
image: ../../images/albumart/annabelle-sethupathi.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FtbdUaoCJ9I"
type: "love"
singers:
  - Pradeep Kumar
  - Asees Kaur
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endru sirumugam yendhum sugam varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru sirumugam yendhum sugam varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengi thavam kidappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengi thavam kidappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai asaivayum thandhai rasipaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai asaivayum thandhai rasipaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli uyir niraipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli uyir niraipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrai thoosu thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrai thoosu thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai oppam vaangi kondu ulle anumadhippene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai oppam vaangi kondu ulle anumadhippene"/>
</div>
<div class="lyrico-lyrics-wrapper">Netril koodu katti nee thandha nyabagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netril koodu katti nee thandha nyabagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum varavazhaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum varavazhaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viragendralum nee koduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragendralum nee koduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">En udal theeyai aagave vizhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En udal theeyai aagave vizhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivendralum un arugil kai viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivendralum un arugil kai viral"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai theendave mudiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai theendave mudiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illa edhu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa edhu naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
<div class="lyrico-lyrics-wrapper">Anange anange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anange anange"/>
</div>
</pre>
