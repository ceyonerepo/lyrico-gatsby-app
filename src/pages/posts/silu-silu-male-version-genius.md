---
title: "silu silu male version song lyrics"
album: "Genius"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Suseenthiran"
path: "/albums/genius-lyrics"
song: "Silu Silu Male Version"
image: ../../images/albumart/genius.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EjAeO7RTNJ4"
type: "happu"
singers:
  - Al-Rufian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Silu silu silu sil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu silu silu sil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala sala sala sal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala sala sala sal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu pesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuga yuga yugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuga yuga yugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu paaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu paaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam paathavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam paathavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Panamthin pinnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panamthin pinnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam kothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam kothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam oodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam oodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanal neeril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal neeril "/>
</div>
<div class="lyrico-lyrics-wrapper">meengal thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meengal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam muzhuthum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam muzhuthum "/>
</div>
<div class="lyrico-lyrics-wrapper">kavaliyil adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavaliyil adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkai eriyum velaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai eriyum velaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadigaal vendum moolaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadigaal vendum moolaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai eriyum velaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai eriyum velaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadigaal vendum moolaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadigaal vendum moolaiyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu silu silu sil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu silu silu sil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala sala sala sal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala sala sala sal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu pesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu pesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuga yuga yugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuga yuga yugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu paaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu paaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyy eh eh eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy eh eh eyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalil udaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalil udaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai panithuzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai panithuzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyil sikkiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyil sikkiya "/>
</div>
<div class="lyrico-lyrics-wrapper">iravin mazhaithuzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravin mazhaithuzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu kuruviyin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu kuruviyin "/>
</div>
<div class="lyrico-lyrics-wrapper">kalavi satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavi satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetti muditha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetti muditha "/>
</div>
<div class="lyrico-lyrics-wrapper">veenaiyin mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenaiyin mounam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyaal ezhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyaal ezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyil mithanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyil mithanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyaal udainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyaal udainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraiyum kumazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiyum kumazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">vadigaal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadigaal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thudipathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thudipathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ithanaal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithanaal thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkai eriyum velaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai eriyum velaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadigaal vendum moolaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadigaal vendum moolaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai eriyum velaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai eriyum velaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadigaal vendum moolaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadigaal vendum moolaiyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkai eriyum velaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai eriyum velaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadigaal vendum moolaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadigaal vendum moolaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai eriyum velaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai eriyum velaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadigaal vendum moolaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadigaal vendum moolaiyilae"/>
</div>
</pre>
