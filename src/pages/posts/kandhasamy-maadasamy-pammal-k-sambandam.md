---
title: "kandhasamy maadasamy song lyrics"
album: "Pammal K Sambandam"
artist: "Deva"
lyricist: "Kamal Haasan"
director: "Moulee"
path: "/albums/pammal-k-sambandam-lyrics"
song: "Kandhasamy Maadasamy"
image: ../../images/albumart/pammal-k-sambandam.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3ATaslvlE4c"
type: "happy"
singers:
  - Kamal Haasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kandasamy maadasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandasamy maadasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppusamy ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppusamy ramasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kattikinango oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kattikinango oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakkiyellam onji poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkiyellam onji poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmuzhichi pakkasolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmuzhichi pakkasolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappunu othukinango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappunu othukinango"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei thambida thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei thambida thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir kathila echi muzhiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir kathila echi muzhiyathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathula pattu thirumbi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathula pattu thirumbi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonjila ottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonjila ottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupaanda kupaiya kottathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupaanda kupaiya kottathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh aah..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh aah.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir kathila echi muzhiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir kathila echi muzhiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei thambida thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei thambida thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambiyam thambida thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambiyam thambida thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooh ohhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooh ohhhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupaanda kupaiya kottathae yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupaanda kupaiya kottathae yeh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinaikkum sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinaikkum sonnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanda dhinaikkum sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanda dhinaikkum sonnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annikkae sonnaru annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annikkae sonnaru annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hanumar bodiya paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanumar bodiya paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaaru buthiya paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaaru buthiya paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyappan amsama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyappan amsama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunthinikkira styley-um paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunthinikkira styley-um paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annikkae sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annikkae sonnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Annikkae sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annikkae sonnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Venamnu sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venamnu sonnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam venamnu sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam venamnu sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Super bodyaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super bodyaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam surukkidumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam surukkidumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum kaalaiyaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum kaalaiyaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soopu pottu kuchidumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soopu pottu kuchidumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarupadai veedu katinavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarupadai veedu katinavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam natti anaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam natti anaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Double duty senjaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double duty senjaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ana budget thaalatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ana budget thaalatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Palani malaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palani malaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandiya ninnaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandiya ninnaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Komana aandiya ninnaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komana aandiya ninnaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi ezhumalaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi ezhumalaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathiya paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathiya paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekka chakkama kadanu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekka chakkama kadanu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavanukkae indha gathina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavanukkae indha gathina"/>
</div>
<div class="lyrico-lyrics-wrapper">Alpanga gathiya nenachu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alpanga gathiya nenachu paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apple attam pona paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apple attam pona paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhigi poi vanthirukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhigi poi vanthirukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marriageunaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriageunaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan kaaran sonna maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan kaaran sonna maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekamalae molla maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekamalae molla maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazha pootanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazha pootanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiythalakka aiythalakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiythalakka aiythalakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiythalakka hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiythalakka hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondikattai ambalaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondikattai ambalaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalu koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalu koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeans potta valai edukkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeans potta valai edukkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaikku aavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikku aavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethira traffikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethira traffikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Illatha rodu idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illatha rodu idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guthirai onnu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guthirai onnu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi jeikkum race idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi jeikkum race idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arthama sonnango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arthama sonnango"/>
</div>
<div class="lyrico-lyrics-wrapper">Periyavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhama sonnango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhama sonnango"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaiya annatha thanni pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaiya annatha thanni pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lighta sonnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lighta sonnaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathila thothadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathila thothadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathadi aavathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadi aavathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalila thathalikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalila thathalikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattamaram aavathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattamaram aavathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasthiku onnu asaiku onnunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasthiku onnu asaiku onnunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathu eduthukooyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathu eduthukooyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai nee pekka thevai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai nee pekka thevai illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna nasthi pannura kostigalalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna nasthi pannura kostigalalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nova thevaiyillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nova thevaiyillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu nova thevaiyillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu nova thevaiyillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thambi summa iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi summa iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanga nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanga nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Narasama oothittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narasama oothittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettiyil vutta onannai nee ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettiyil vutta onannai nee ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vonna vonnanaa vituduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vonna vonnanaa vituduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melam kotti malai maatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melam kotti malai maatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingirunthu pona paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingirunthu pona paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosam ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosam ponaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bachelora iruntha paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bachelora iruntha paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudichavikki velai panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudichavikki velai panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolu pattanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolu pattanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandasamy maadasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandasamy maadasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppusamy ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppusamy ramasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam kattikinango oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam kattikinango oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakkiyellam onji poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakkiyellam onji poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmuzhichi pakkasolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmuzhichi pakkasolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappunu othukinango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappunu othukinango"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei thambida thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei thambida thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir kathila echi muzhiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir kathila echi muzhiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerupaanda kupaiya kottathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupaanda kupaiya kottathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh aah..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh aah.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiythalakka aiythalakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiythalakka aiythalakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiythalakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiythalakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavila thatti thaali kattina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavila thatti thaali kattina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikilu than meetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikilu than meetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sevila pathu jollu vittina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sevila pathu jollu vittina"/>
</div>
<div class="lyrico-lyrics-wrapper">Selavoda pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selavoda pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandasamy madasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandasamy madasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">De dei innoru charamra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="De dei innoru charamra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennamda kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennamda kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkaru ukkaru ukkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaru ukkaru ukkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkaru appadi appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaru appadi appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu samy vellai samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu samy vellai samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei dei yerikkuchira unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei dei yerikkuchira unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha samsa kadichikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha samsa kadichikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Venava sari po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venava sari po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadura samy paadu hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadura samy paadu hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu samy vellai samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu samy vellai samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela samy manja samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela samy manja samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Multicolour mayilu samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Multicolour mayilu samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerikkuchu samy irankichu samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerikkuchu samy irankichu samy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerikkuchu samy irankichu samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerikkuchu samy irankichu samy"/>
</div>
</pre>
