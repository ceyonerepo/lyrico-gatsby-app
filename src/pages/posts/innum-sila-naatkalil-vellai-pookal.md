---
title: "potraamarai song lyrics"
album: "Vellai Pookal"
artist: "Ramgopal Krishnaraju"
lyricist: "Madhan Karky"
director: "Vivek Elangovan"
path: "/albums/vellai-pookal-lyrics"
song: "Potraamarai"
image: ../../images/albumart/vellai-pookal.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N8OSWkjC8xg"
type: "happy"
singers:
  - S. P. Balasubrahmanyam
  - Ravi Gopinath
  - Priyanka NK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Potraamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potraamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrum Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrum Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttrum Punal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttrum Punal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattrai Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattrai Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Settram Ura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settram Ura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram Sila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram Sila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattrum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattrum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeriyum Thazhal Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeriyum Thazhal Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal Virippaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Virippaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Karugum Siragodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karugum Siragodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathil Parappaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil Parappaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potraamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potraamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrum Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrum Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttrum Punal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttrum Punal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattrai Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattrai Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Settram Ura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settram Ura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram Sila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram Sila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattrum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattrum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhirkka Mudiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirkka Mudiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Ival Idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Ival Idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaikka Mudiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaikka Mudiyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uram Ival Manadhinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uram Ival Manadhinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potraamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potraamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrum Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrum Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttrum Punal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttrum Punal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattrai Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattrai Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Settram Ura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settram Ura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram Sila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram Sila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattrum Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattrum Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatile Oar Poovaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatile Oar Poovaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyile Oar Kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyile Oar Kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalum Seivaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalum Seivaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalum Koivaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalum Koivaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalaa Nee Imaikaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaa Nee Imaikaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendralaai Selvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendralaai Selvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindru Thaan Kolvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindru Thaan Kolvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhagaa Nee Sirikkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhagaa Nee Sirikkayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei Yaadhenin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Yaadhenin"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyin Nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyin Nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhve Suzhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhve Suzhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoolaai Thadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoolaai Thadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane Padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam Vidai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthirgal Ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthirgal Ovvondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avizhndhida Avizhndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avizhndhida Avizhndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Irugum Nenjangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irugum Nenjangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Negizhndhida Negizhndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Negizhndhida Negizhndhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttram Yenil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram Yenil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam Ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam Ethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam Yenil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Yenil"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharmam Ethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharmam Ethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verroor Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verroor Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Verroor Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verroor Kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verroor Irul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verroor Irul"/>
</div>
<div class="lyrico-lyrics-wrapper">Verroor Oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verroor Oli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irunda Vaanodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunda Vaanodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Mazhai Pozhinthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Mazhai Pozhinthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurudhi Karai Yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurudhi Karai Yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyil Azhinthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil Azhinthida"/>
</div>
</pre>
