---
title: "thappara song lyrics"
album: "Thirupathi Samy Kudumbam"
artist: "Sam D Raj"
lyricist: "Kabilan"
director: "Suresh Shanmugam"
path: "/albums/thirupathi-samy-kudumbam-lyrics"
song: "Thappara"
image: ../../images/albumart/thirupathi-samy-kudumbam.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zJJAAuJvL14"
type: "happy"
singers:
  - Vijayalakshmi
  - Shravan
  - Uday
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thappara thappara thappara thara thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappara thappara thappara thara thara"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pothum ne unna tholaikathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pothum ne unna tholaikathada"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara pappara pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara pappara pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyadha iravuku vedi poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyadha iravuku vedi poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malaiya theyagave neyaga irunthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiya theyagave neyaga irunthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">anaiyathada thinam madangathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaiyathada thinam madangathada"/>
</div>
<div class="lyrico-lyrics-wrapper">kavala illamale katraga paranthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavala illamale katraga paranthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">siragethada oru thinaiyethada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragethada oru thinaiyethada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thappara thappara thappara thara thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappara thappara thappara thara thara"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pothum ne unna tholaikathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pothum ne unna tholaikathada"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara pappara pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara pappara pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyadha iravuku vedi poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyadha iravuku vedi poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aagayam pol oru thooram illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam pol oru thooram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">thotu vitta unna pola evanum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotu vitta unna pola evanum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">aanantham than enga kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanantham than enga kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">andavanin kaiyila nan kadaisi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andavanin kaiyila nan kadaisi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalaiya oram kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaiya oram kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavinai eara kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavinai eara kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponatha moota kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponatha moota kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu puthu paatu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu puthu paatu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">aanathu aachuda aarambam iruku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanathu aachuda aarambam iruku da"/>
</div>
<div class="lyrico-lyrics-wrapper">elu naal varathil ellame namaku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elu naal varathil ellame namaku da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thappara thappara thappara thara thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappara thappara thappara thara thara"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pothum ne unna tholaikathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pothum ne unna tholaikathada"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara pappara pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara pappara pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyadha iravuku vedi poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyadha iravuku vedi poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yarukume naanga tholla illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukume naanga tholla illa"/>
</div>
<div class="lyrico-lyrics-wrapper">thai pala pola nanga vella pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai pala pola nanga vella pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">naal ellame enga naale illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal ellame enga naale illa"/>
</div>
<div class="lyrico-lyrics-wrapper">naam veru endru sollu vela illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam veru endru sollu vela illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamaiyin kelviki ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamaiyin kelviki ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu varai tholvi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu varai tholvi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">elunthathu romba illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthathu romba illa"/>
</div>
<div class="lyrico-lyrics-wrapper">iruku da nooru ella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruku da nooru ella"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneera sinthuna un kangal selikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneera sinthuna un kangal selikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam than thoongina kathadi parakuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam than thoongina kathadi parakuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thappara thappara thappara thara thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappara thappara thappara thara thara"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pothum ne unna tholaikathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pothum ne unna tholaikathada"/>
</div>
<div class="lyrico-lyrics-wrapper">pappara pappara pappara para para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pappara pappara pappara para para"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyadha iravuku vedi poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyadha iravuku vedi poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malaiya theyagave neyaga irunthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiya theyagave neyaga irunthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">anaiyathada thinam madangathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaiyathada thinam madangathada"/>
</div>
<div class="lyrico-lyrics-wrapper">kavala illamale katraga paranthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavala illamale katraga paranthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">siragethada oru thinaiyethada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragethada oru thinaiyethada"/>
</div>
</pre>
