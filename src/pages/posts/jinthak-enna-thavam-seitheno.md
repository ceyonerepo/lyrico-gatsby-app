---
title: "jinthak song lyrics"
album: "Enna Thavam Seitheno"
artist: "Dev Guru"
lyricist: "Venkatesh Prabhakar"
director: "Murabasalan"
path: "/albums/enna-thavam-seitheno-lyrics"
song: "Jinthak"
image: ../../images/albumart/enna-thavam-seitheno.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Jqpnox0HxjE"
type: "happy"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh pulla atha allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh pulla atha allu"/>
</div>
<div class="lyrico-lyrics-wrapper">itha jalli kattu kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha jalli kattu kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">eh miss enna thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh miss enna thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu majava nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu majava nee"/>
</div>
<div class="lyrico-lyrics-wrapper">muthatha kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthatha kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">summa seenu podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa seenu podatha"/>
</div>
<div class="lyrico-lyrics-wrapper">maams ah round katathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maams ah round katathe"/>
</div>
<div class="lyrico-lyrics-wrapper">katting potu kaaliya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katting potu kaaliya "/>
</div>
<div class="lyrico-lyrics-wrapper">nee sound vidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sound vidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">neka kannu vudatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neka kannu vudatha"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka sottu vudatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka sottu vudatha"/>
</div>
<div class="lyrico-lyrics-wrapper">idupu alaga kaatee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idupu alaga kaatee"/>
</div>
<div class="lyrico-lyrics-wrapper">enna mersal aakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna mersal aakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">santhu cape ula ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhu cape ula ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">sinthu paadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthu paadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthu kaata theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthu kaata theva"/>
</div>
<div class="lyrico-lyrics-wrapper">illa sothu pathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa sothu pathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">history un pera solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="history un pera solla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalthu kaatu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalthu kaatu da"/>
</div>
<div class="lyrico-lyrics-wrapper">illa saraku adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa saraku adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">off oil potu kaatuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="off oil potu kaatuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh pulla atha allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh pulla atha allu"/>
</div>
<div class="lyrico-lyrics-wrapper">itha jalli kattu kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha jalli kattu kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">eh miss enna thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh miss enna thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu majava nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu majava nee"/>
</div>
<div class="lyrico-lyrics-wrapper">muthatha kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthatha kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">summa seenu podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa seenu podatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">erangi kuthu erangu kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erangi kuthu erangu kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu vitha eh jaami panitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu vitha eh jaami panitu"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu pulladichu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu pulladichu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<div class="lyrico-lyrics-wrapper">miss ah vutta ballu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miss ah vutta ballu"/>
</div>
<div class="lyrico-lyrics-wrapper">bajjar aana aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bajjar aana aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak thak thak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak thak thak "/>
</div>
<div class="lyrico-lyrics-wrapper">thak thak thak thak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thak thak thak thak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatu poda jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu poda jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">eh vettu poda jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh vettu poda jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">eh erangi kutha jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh erangi kutha jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">eh mersal aana jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh mersal aana jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">kirukalaagi jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirukalaagi jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">thassaana jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thassaana jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaraana jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaraana jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">danger aana jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danger aana jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">kattu pottu vettu pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu pottu vettu pota"/>
</div>
<div class="lyrico-lyrics-wrapper">erangi kuthi mersalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erangi kuthi mersalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kirukalaga jaaru vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirukalaga jaaru vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">dasaagi thillana danger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dasaagi thillana danger"/>
</div>
<div class="lyrico-lyrics-wrapper">aana jinthak 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana jinthak "/>
</div>
<div class="lyrico-lyrics-wrapper">seetu ithu illa jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seetu ithu illa jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak jinthak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodi kodiya note 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodiya note "/>
</div>
<div class="lyrico-lyrics-wrapper">irukuthu da aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukuthu da aana"/>
</div>
<div class="lyrico-lyrics-wrapper">paadaiyila pogum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadaiyila pogum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">otha roova da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha roova da"/>
</div>
<div class="lyrico-lyrics-wrapper">maadi maadiya kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadi maadiya kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">irupan da nama mastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupan da nama mastha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thookam pota 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thookam pota "/>
</div>
<div class="lyrico-lyrics-wrapper">payapaduvan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payapaduvan da"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai oru vattam polada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai oru vattam polada"/>
</div>
<div class="lyrico-lyrics-wrapper">athula colour coloru ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athula colour coloru ra"/>
</div>
<div class="lyrico-lyrics-wrapper">raja nee bit ah otuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raja nee bit ah otuda"/>
</div>
<div class="lyrico-lyrics-wrapper">mallaka nee paduthu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallaka nee paduthu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">mela vaanathula natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela vaanathula natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">sirukum paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirukum paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">soodana idly ku satiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodana idly ku satiya"/>
</div>
<div class="lyrico-lyrics-wrapper">thotuka kannana ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotuka kannana ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee life fulla sethuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee life fulla sethuka"/>
</div>
<div class="lyrico-lyrics-wrapper">kastapattu paru avurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastapattu paru avurum"/>
</div>
<div class="lyrico-lyrics-wrapper">un towser kaasa kuduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un towser kaasa kuduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paaru happy un mother
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaru happy un mother"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jinthak jinthak jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh jillu enna allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh jillu enna allu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya thorantha ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya thorantha ka"/>
</div>
<div class="lyrico-lyrics-wrapper">jolikuthu un pallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jolikuthu un pallu"/>
</div>
<div class="lyrico-lyrics-wrapper">eh mayilu its my deal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh mayilu its my deal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan oturen un track la 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan oturen un track la "/>
</div>
<div class="lyrico-lyrics-wrapper">en rail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en rail"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boss jinthak na enna boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boss jinthak na enna boss"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak ah"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga thana sonnenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga thana sonnenga"/>
</div>
<div class="lyrico-lyrics-wrapper">ungaluke theriyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ungaluke theriyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak na ennanu theriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak na ennanu theriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jinthak jinthak tha tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak tha tha "/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak tha tha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak tha tha "/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak jinthak"/>
</div>
<div class="lyrico-lyrics-wrapper">jinthak eh jinthak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinthak eh jinthak"/>
</div>
</pre>
