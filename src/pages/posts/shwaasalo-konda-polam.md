---
title: "shwaasalo song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "M. M. Keeravani"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Shwaasalo"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/A8DC_pc6WfE"
type: "love"
singers:
  - Yamini Ghantasala
  - PVNS Rohit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neelo Naalo Neelo Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Naalo Neelo Naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Naalo Neelo Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Naalo Neelo Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shwaasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulni Daataalanna Aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulni Daataalanna Aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Naalo Neelo Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Naalo Neelo Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasha Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhulni Marche Haayi Mosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhulni Marche Haayi Mosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunde Loyal’lo Pongu Vaagullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Loyal’lo Pongu Vaagullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Saagullo Baagu Vogullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Saagullo Baagu Vogullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Menu Marachelaa Paina Padtunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menu Marachelaa Paina Padtunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Koona Degallo Thene Teegallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koona Degallo Thene Teegallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shwasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwasalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulni Daatalanna Aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulni Daatalanna Aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Naalo Neelo Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Naalo Neelo Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvamulo Anuvu Anuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvamulo Anuvu Anuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasamondaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasamondaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasame Aalalu Alalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasame Aalalu Alalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alajadi Repaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alajadi Repaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eetitho Aatale Theta Thellamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eetitho Aatale Theta Thellamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Ra Amme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Ra Amme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shwaasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulni Daataalanna Aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulni Daataalanna Aasha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasha Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasha Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhulni Marche Haayi Mosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhulni Marche Haayi Mosaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunde Loyal’lo Pongu Vaagullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Loyal’lo Pongu Vaagullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Saagullo Baagu Vogullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Saagullo Baagu Vogullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Menu Marachelaa Paina Padtunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menu Marachelaa Paina Padtunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Koona Degallo Thene Teegallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koona Degallo Thene Teegallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shwaasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulni Daataalanna Aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulni Daataalanna Aasha"/>
</div>
</pre>
