---
title: "adada kadhal vanthathe song lyrics"
album: "Enga Kattula Mazhai"
artist: "Srivijay"
lyricist: "Na Muthukumar"
director: "Sri Balaji"
path: "/albums/enga-kattula-mazhai-lyrics"
song: "Adada Kadhal Vanthathe"
image: ../../images/albumart/enga-kattula-mazhai.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8zFcyB93tQI"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adada Kadhal Vanthathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Kadhal Vanthathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yennai azhagaai mothi sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai azhagaai mothi sendrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">alaimel aadum padakay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaimel aadum padakay "/>
</div>
<div class="lyrico-lyrics-wrapper">en manathai matri sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manathai matri sendrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai oru dhevathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai oru dhevathai "/>
</div>
<div class="lyrico-lyrics-wrapper">naan kandeney pala murai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kandeney pala murai "/>
</div>
<div class="lyrico-lyrics-wrapper">en kankalai saripartheney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kankalai saripartheney "/>
</div>
<div class="lyrico-lyrics-wrapper">adi antharathiley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi antharathiley "/>
</div>
<div class="lyrico-lyrics-wrapper">thulli thulli kuthithen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thulli kuthithen "/>
</div>
<div class="lyrico-lyrics-wrapper">rekkai indriye kaatril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekkai indriye kaatril "/>
</div>
<div class="lyrico-lyrics-wrapper">yeri parandhen kannai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeri parandhen kannai "/>
</div>
<div class="lyrico-lyrics-wrapper">mutinala unnai mattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutinala unnai mattum "/>
</div>
<div class="lyrico-lyrics-wrapper">nenaithen yennai yennavo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaithen yennai yennavo "/>
</div>
<div class="lyrico-lyrics-wrapper">seiyuthey kadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seiyuthey kadhal "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Kadhal Vanthathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Kadhal Vanthathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yennai azhagaai mothi sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai azhagaai mothi sendrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">alaimel aadum padakay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaimel aadum padakay "/>
</div>
<div class="lyrico-lyrics-wrapper">en manathai matri sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manathai matri sendrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">idhaiyathin kathavaruke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaiyathin kathavaruke "/>
</div>
<div class="lyrico-lyrics-wrapper">ne varum olui ketkinrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne varum olui ketkinrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna imai aruke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna imai aruke "/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam mattum pookkindrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam mattum pookkindrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">iruvili athu yennidam irukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruvili athu yennidam irukum "/>
</div>
<div class="lyrico-lyrics-wrapper">marunodi athu unnidam parakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marunodi athu unnidam parakum "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna penne yeen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna penne yeen "/>
</div>
<div class="lyrico-lyrics-wrapper">intha mayakam un vaasham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha mayakam un vaasham "/>
</div>
<div class="lyrico-lyrics-wrapper">nan kaatril thedi parakkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kaatril thedi parakkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">muchil serkkiren munne munne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muchil serkkiren munne munne "/>
</div>
<div class="lyrico-lyrics-wrapper">unkaaladi pinne varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unkaaladi pinne varum "/>
</div>
<div class="lyrico-lyrics-wrapper">yenthan nezhal naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenthan nezhal naan "/>
</div>
<div class="lyrico-lyrics-wrapper">mannodu vazhanthal unnodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannodu vazhanthal unnodu "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhven uyire en uyirukul neyadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhven uyire en uyirukul neyadi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Kadhal Vanthathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Kadhal Vanthathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yennai azhagaai mothi sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai azhagaai mothi sendrathey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithuvarai thotravan naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuvarai thotravan naan "/>
</div>
<div class="lyrico-lyrics-wrapper">indru unmanam jeithuviten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru unmanam jeithuviten "/>
</div>
<div class="lyrico-lyrics-wrapper">kanaviley vazhanthavan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaviley vazhanthavan "/>
</div>
<div class="lyrico-lyrics-wrapper">naan indru nejathinil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan indru nejathinil "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhnthu viten adikadi idhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhnthu viten adikadi idhal "/>
</div>
<div class="lyrico-lyrics-wrapper">un peyar uraikkum antha nodikalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un peyar uraikkum antha nodikalil "/>
</div>
<div class="lyrico-lyrics-wrapper">en idhal inikkum paravasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idhal inikkum paravasam "/>
</div>
<div class="lyrico-lyrics-wrapper">ondru nenjukul perakkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondru nenjukul perakkum "/>
</div>
<div class="lyrico-lyrics-wrapper">en tholkal saaiyathan ullathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en tholkal saaiyathan ullathu "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai theduthu aiyo aiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai theduthu aiyo aiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">indha kadhalthan tharum vali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kadhalthan tharum vali "/>
</div>
<div class="lyrico-lyrics-wrapper">oru sugam un parvai podhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sugam un parvai podhum "/>
</div>
<div class="lyrico-lyrics-wrapper">en jeevan vazhum kanave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en jeevan vazhum kanave "/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavilum neyadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavilum neyadi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Kadhal Vanthathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Kadhal Vanthathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yennai azhagaai mothi sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai azhagaai mothi sendrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">alaimel aadum padakay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaimel aadum padakay "/>
</div>
<div class="lyrico-lyrics-wrapper">en manathai matri sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manathai matri sendrathey "/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal murai oru dhevathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal murai oru dhevathai "/>
</div>
<div class="lyrico-lyrics-wrapper">naan kandeney pala murai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kandeney pala murai "/>
</div>
<div class="lyrico-lyrics-wrapper">en kankalai saripartheney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kankalai saripartheney "/>
</div>
<div class="lyrico-lyrics-wrapper">adi antharathiley 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi antharathiley "/>
</div>
<div class="lyrico-lyrics-wrapper">thulli thulli kuthithen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thulli kuthithen "/>
</div>
<div class="lyrico-lyrics-wrapper">rekkai indriye kaatril 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekkai indriye kaatril "/>
</div>
<div class="lyrico-lyrics-wrapper">yeri parandhen kannai mutinala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeri parandhen kannai mutinala "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai mattum nenaithen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai mattum nenaithen "/>
</div>
<div class="lyrico-lyrics-wrapper">yennai yennavo seiyuthey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai yennavo seiyuthey "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal Adada Kadhal Vanthathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal Adada Kadhal Vanthathe "/>
</div>
<div class="lyrico-lyrics-wrapper">yennai azhagaai mothi sendrathey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennai azhagaai mothi sendrathey "/>
</div>
</pre>
