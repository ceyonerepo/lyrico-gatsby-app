---
title: "ja ve sajjna song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Japjeet Dhillon"
path: "/albums/above-all-lyrics"
song: "Ja Ve Sajjna"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/2mjz0NRXdcI"
type: "Love"
singers:
  - Jassa Dhillon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mainu pehla hi dass dayin jadon chadd jaavenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu pehla hi dass dayin jadon chadd jaavenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maithon ruleya jaana nahi te tu pachtavenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maithon ruleya jaana nahi te tu pachtavenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir dil te laggiyan sattan nu dovein seeh laange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir dil te laggiyan sattan nu dovein seeh laange"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jarr lange tere taane vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jarr lange tere taane vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu pyaar jo kitaa ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu pyaar jo kitaa ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Darr lagan hatt gaya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darr lagan hatt gaya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera ishq jo peeta ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera ishq jo peeta ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jarr lange tere taane vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jarr lange tere taane vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu pyaar jo kitaa ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu pyaar jo kitaa ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Darr lagan hatt gaya ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darr lagan hatt gaya ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera ishq jo peeta ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera ishq jo peeta ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu bhull gaya saukha ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu bhull gaya saukha ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera har saah aukha ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera har saah aukha ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo rehde baaki ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo rehde baaki ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hanju vi pee laange haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hanju vi pee laange haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bade khwaab si mere ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bade khwaab si mere ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas khwaab hi ban reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas khwaab hi ban reh gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dass de dil kamle nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass de dil kamle nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assi kithe ghatt reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assi kithe ghatt reh gaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bade khwaab si mere ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bade khwaab si mere ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas khwaab hi ban reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas khwaab hi ban reh gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dass de dil kamle nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass de dil kamle nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assi kithe ghatt reh gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assi kithe ghatt reh gaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere mere yaaraneyan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere mere yaaraneyan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dass naam ki deyange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass naam ki deyange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange haaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal ja ve sajjna ve tere bin jee laange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal ja ve sajjna ve tere bin jee laange"/>
</div>
</pre>
