---
title: "manothodu pesum song lyrics"
album: "Kapalikaram"
artist: "Karthik Krishnan"
lyricist: "Karthik"
director: "Dhakshan Vijay"
path: "/albums/kapalikaram-lyrics"
song: "Manothodu Pesum"
image: ../../images/albumart/kapalikaram.jpg
date: 2022-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o60l5voyBuw"
type: "love"
singers:
  - Madhu Balakrishna
  - Amritha
  - Ramya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manathodu pesum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathodu pesum "/>
</div>
<div class="lyrico-lyrics-wrapper">isai yaaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isai yaaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvai malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvai malare"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nedun thoora payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedun thoora payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvaai varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvaai varuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">urave urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urave urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manathil ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum malarum malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum malarum malare"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum malarum malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum malarum malare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru murai unai parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru murai unai parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">pala murai ennil verthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala murai ennil verthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum thinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">enakul siripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakul siripen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru murai unnai parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru murai unnai parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">pala murai ennil verthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala murai ennil verthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum thinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">enakul siripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakul siripen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuru thuru vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuru thuru vena"/>
</div>
<div class="lyrico-lyrics-wrapper">iru pathangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru pathangal "/>
</div>
<div class="lyrico-lyrics-wrapper">tharaiyil nadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaiyil nadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pada vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pada vena"/>
</div>
<div class="lyrico-lyrics-wrapper">en manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">siragondru mulaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragondru mulaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ilai mel viluntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilai mel viluntha"/>
</div>
<div class="lyrico-lyrics-wrapper">pani thuli pol sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani thuli pol sitharum"/>
</div>
<div class="lyrico-lyrics-wrapper">malare malare malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malare malare malare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iru kai viral korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru kai viral korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">iruvarum mella nadapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruvarum mella nadapom"/>
</div>
<div class="lyrico-lyrics-wrapper">nedun salai engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedun salai engum"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyil nanaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyil nanaivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadal karai manal meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal karai manal meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">iru pathangal sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru pathangal sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paravai pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravai pola "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril parapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ilanthendran ithamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilanthendran ithamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">manathinai varuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathinai varuda"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai ellam namai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai ellam namai "/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu vaalthukal koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu vaalthukal koora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelelu jenmam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelelu jenmam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu vaalvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu vaalvom"/>
</div>
<div class="lyrico-lyrics-wrapper">malare malare malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malare malare malare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manathodu pesum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathodu pesum "/>
</div>
<div class="lyrico-lyrics-wrapper">isai yaaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isai yaaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvai malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvai malare"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nedun thoora payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedun thoora payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvaai varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvaai varuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">urave urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urave urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manathil ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum malarum malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum malarum malare"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum malarum malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum malarum malare"/>
</div>
</pre>
