---
title: "sammathame song lyrics"
album: "Raja Vikramarka"
artist: "Prasanth R Vihari"
lyricist: "Ramajogayya Sastry"
director: "Sri Saripalli"
path: "/albums/raja-vikramarka-lyrics"
song: "Sammathame"
image: ../../images/albumart/raja-vikramarka.jpg
date: 2021-11-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ClOxi-OHaFg?"
type: "love"
singers:
  - Karthik
  - Shashaa Tirupati
  - Chaitra Ambadipudi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ee tholipremaanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee tholipremaanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Varnichalenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varnichalenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa jathaloni andham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa jathaloni andham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhellapaatu vendi vennele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhellapaatu vendi vennele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa heart beatulo dhvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa heart beatulo dhvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaalilaaga undhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaalilaaga undhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozart chethi vellu kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozart chethi vellu kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopinchane levule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopinchane levule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kshanaana naalo kaanthini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kshanaana naalo kaanthini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye meter aina inthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye meter aina inthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkinchi cheppalenu asale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkinchi cheppalenu asale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sammathamey sambaramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathamey sambaramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammathamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathamey"/>
</div>
<div class="lyrico-lyrics-wrapper">O varamai varamai nannu kalisaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O varamai varamai nannu kalisaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammathamey sambaramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathamey sambaramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammathamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathamey"/>
</div>
<div class="lyrico-lyrics-wrapper">O varamai varamai nannu kalisaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O varamai varamai nannu kalisaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Cheliya you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Cheliya you are my love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa chirunavvulevi thaarajuvvalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa chirunavvulevi thaarajuvvalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rivvantunnavi kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rivvantunnavi kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sirimuvvalevi gaallo guvvalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa sirimuvvalevi gaallo guvvalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aade savvadi vintunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aade savvadi vintunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerani swapnaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerani swapnaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerchina velugu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerchina velugu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Merisenura kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merisenura kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamani rangulanu manasuna nimpaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamani rangulanu manasuna nimpaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalaku ra nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalaku ra nannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Parichayam jarigeno ledho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichayam jarigeno ledho"/>
</div>
<div class="lyrico-lyrics-wrapper">Marukshanam premalo thelene pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukshanam premalo thelene pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini yerugani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini yerugani"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasam nannu kammaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasam nannu kammaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammene ee tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammene ee tharunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munde munde nuvvannaava naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munde munde nuvvannaava naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemo unnavemo oopirilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemo unnavemo oopirilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nede ninnu choosthunnaana naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nede ninnu choosthunnaana naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanaya vaasthvamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanaya vaasthvamlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sammathamey sambaramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathamey sambaramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammathamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathamey"/>
</div>
<div class="lyrico-lyrics-wrapper">O varamai varamai nannu kalisaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O varamai varamai nannu kalisaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Cheliya you are my love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Cheliya you are my love"/>
</div>
</pre>
