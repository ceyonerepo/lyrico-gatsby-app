---
title: "yar mogatha paathalum song lyrics"
album: "Kapalikaram"
artist: "Karthik Krishnan"
lyricist: "Karthik"
director: "Dhakshan Vijay"
path: "/albums/kapalikaram-lyrics"
song: "Yar Mogatha Paathalum"
image: ../../images/albumart/kapalikaram.jpg
date: 2022-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5nGm9asZgKQ"
type: "sad"
singers:
  - Srikanth Harichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yar mogatha pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar mogatha pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam than theriyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam than theriyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yar pecha ketalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar pecha ketalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kural pol thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kural pol thonuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">usure usure ne enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usure usure ne enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">manase manase ne enna aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase manase ne enna aana"/>
</div>
<div class="lyrico-lyrics-wrapper">usure usure ne enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usure usure ne enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">alage alage nee enna aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage alage nee enna aana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yar mogatha pathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar mogatha pathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam than theriyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam than theriyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yar pecha ketalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar pecha ketalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kural pol thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kural pol thonuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannadi mun ninna kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi mun ninna kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam than theriyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam than theriyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">katrilum en moochilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katrilum en moochilum"/>
</div>
<div class="lyrico-lyrics-wrapper">adi un mugam theriyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi un mugam theriyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">suvar illa oviyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvar illa oviyama"/>
</div>
<div class="lyrico-lyrics-wrapper">naan irunthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan irunthen "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillin nee kalantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillin nee kalantha"/>
</div>
<div class="lyrico-lyrics-wrapper">pasi ariya siru kulanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi ariya siru kulanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol aanen unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol aanen unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">thisai ariya oru paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisai ariya oru paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol paranthen unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol paranthen unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">en madi meethu malalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en madi meethu malalai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol sainthida vendumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol sainthida vendumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en maru thaye maru thaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en maru thaye maru thaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nimmathi nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimmathi nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">en maru thaye maru thaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en maru thaye maru thaye"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir nee thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathal vaasam veesum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vaasam veesum "/>
</div>
<div class="lyrico-lyrics-wrapper">thisaiyil yengi yengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisaiyil yengi yengi"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikindrene vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikindrene vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">un vaasanai en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vaasanai en "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvil veesiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvil veesiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">en siripai nan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en siripai nan "/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu  naal aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu  naal aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">un nenapil en manasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nenapil en manasum"/>
</div>
<div class="lyrico-lyrics-wrapper">udanjachu nathiyodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udanjachu nathiyodi"/>
</div>
<div class="lyrico-lyrics-wrapper">inaiyatha thani theevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaiyatha thani theevai"/>
</div>
<div class="lyrico-lyrics-wrapper">pirinthene thisai ariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirinthene thisai ariya"/>
</div>
<div class="lyrico-lyrics-wrapper">idam engum nan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam engum nan "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai tholaithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai tholaithene"/>
</div>
<div class="lyrico-lyrics-wrapper">en madi meethu malalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en madi meethu malalai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol sainthida vendumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol sainthida vendumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en maru thaye maru thaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en maru thaye maru thaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nimmathi nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimmathi nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">en maru thaye maru thaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en maru thaye maru thaye"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir nee thane"/>
</div>
</pre>
