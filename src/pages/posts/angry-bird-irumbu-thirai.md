---
title: "angry bird song lyrics"
album: "Irumbu Thirai"
artist: "Yuvan Shankar Raja"
lyricist: "Vignesh Shivan"
director: "P.S. Mithran"
path: "/albums/irumbu-thirai-song-lyrics"
song: "Angry Bird"
image: ../../images/albumart/irumbu-thirai.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ocJ0fwwHx0A"
type: "love"
singers:
  - Jithin Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ohooo ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Angry bird aana aala ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angry bird aana aala ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Love bird aakittaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love bird aakittaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovakkaara aaladhaan iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovakkaara aaladhaan iva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhai aakittaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhai aakittaa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratchakan pola irundhavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratchakan pola irundhavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Rupture aakittaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rupture aakittaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandakozhiya ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakozhiya ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Broiler kozhiya aakkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Broiler kozhiya aakkitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby nee podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby nee podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly jolly baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly jolly baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona Un baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona Un baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali gaali gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby nee podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby nee podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly jolly baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly jolly baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona Un baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona Un baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali gaali gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anniyanaaga irundhavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anniyanaaga irundhavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambi aakittaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambi aakittaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Batcha va pesi pesiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batcha va pesi pesiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanik aakittaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanik aakittaa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoni pola six adichavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoni pola six adichavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dravid aakittaa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dravid aakittaa daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kung fu class ku ponavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kung fu class ku ponavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yoga class-ku anupitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yoga class-ku anupitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby nee podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby nee podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly jolly baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly jolly baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona Un baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona Un baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali gaali gaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby nee podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby nee podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly jolly jolly baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly jolly jolly baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona Un baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona Un baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali gaali gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali gaali gaali"/>
</div>
</pre>
