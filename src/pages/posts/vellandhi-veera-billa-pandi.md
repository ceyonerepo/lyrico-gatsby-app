---
title: "vellandhi veera song lyrics"
album: "Billa Pandi"
artist: "Ilayavan"
lyricist: "Thanikkodi"
director: "Raj Sethupathy"
path: "/albums/billa-pandi-lyrics"
song: "Vellandhi Veera"
image: ../../images/albumart/billa-pandi.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QwL5vWW-GDw"
type: "love"
singers:
  - Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vellanthi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">veeraapu kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeraapu kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vellanthi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">veeraapu kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeraapu kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">sithalaa un sirukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithalaa un sirukki"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thangura"/>
</div>
<div class="lyrico-lyrics-wrapper">mathaalam muzhunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathaalam muzhunga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalikki iva yengura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalikki iva yengura"/>
</div>
<div class="lyrico-lyrics-wrapper">manjala theikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjala theikira"/>
</div>
<div class="lyrico-lyrics-wrapper">manakkathan poosura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manakkathan poosura"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjula kaathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjula kaathala"/>
</div>
<div class="lyrico-lyrics-wrapper">mana poosa poduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana poosa poduraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellanthi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">veeraapu kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeraapu kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">sithalaa un sirukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithalaa un sirukki"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thangura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paaradaa paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaradaa paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kaadhal sinnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kaadhal sinnam"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam poochukarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam poochukarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">pudicha thoonumedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudicha thoonumedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">therkethu vadakkethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therkethu vadakkethu"/>
</div>
<div class="lyrico-lyrics-wrapper">thikku than thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thikku than thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">tholaichenea enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholaichenea enna"/>
</div>
<div class="lyrico-lyrics-wrapper">eduthenea unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthenea unna"/>
</div>
<div class="lyrico-lyrics-wrapper">adadaa adadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adadaa adadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adadaa adi nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adadaa adi nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram aasadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram aasadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellanthi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">veeraapu kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeraapu kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">sithalaa un sirukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithalaa un sirukki"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thangura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koyilea neeyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyilea neeyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thenan theebam yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenan theebam yenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">tholuven nooru jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholuven nooru jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiya venduvenea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiya venduvenea"/>
</div>
<div class="lyrico-lyrics-wrapper">mothamaa onna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothamaa onna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">ururula thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ururula thaanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vaartha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaartha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaiya kedapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaiya kedapen"/>
</div>
<div class="lyrico-lyrics-wrapper">oan nenjaan goodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oan nenjaan goodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaan pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan pothume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellanthi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellanthi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">veeraapu kaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeraapu kaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">sithalaa un sirukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithalaa un sirukki"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thangura"/>
</div>
<div class="lyrico-lyrics-wrapper">mathaalam muzhunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathaalam muzhunga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalikki iva yengura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalikki iva yengura"/>
</div>
<div class="lyrico-lyrics-wrapper">manjala theikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjala theikira"/>
</div>
<div class="lyrico-lyrics-wrapper">manakkathan poosura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manakkathan poosura"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjula kaathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjula kaathala"/>
</div>
<div class="lyrico-lyrics-wrapper">mana poosa poduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana poosa poduraa"/>
</div>
</pre>
