---
title: "golimaare song lyrics"
album: "Rowthiram"
artist: "Prakash Nikki"
lyricist: "Gokul"
director: "Gokul"
path: "/albums/rowthiram-lyrics"
song: "Golimaare"
image: ../../images/albumart/rowthiram.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GpTlSdTn6bE"
type: "happy"
singers:
  - Vallavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maama Kanji Pona Boomi Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Kanji Pona Boomi Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattraadha Nadhiya Parthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattraadha Nadhiya Parthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarudhal Adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarudhal Adaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nadhiyae Kaanji Poitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nadhiyae Kaanji Poitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Golimaare Gangsteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golimaare Gangsteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli Soda Boosteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli Soda Boosteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaraumaara Basteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaraumaara Basteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy Daddy Manthra Bedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy Daddy Manthra Bedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Maari Youngstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Youngstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthu Mathi Vastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthu Mathi Vastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam Maama 5 Star-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam Maama 5 Star-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunty Aunty One More 90
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunty Aunty One More 90"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allu Allu Asaalta Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allu Allu Asaalta Allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Fullu Fullu Ollumukku Fullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fullu Fullu Ollumukku Fullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirmirudhu Thimiridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirmirudhu Thimiridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Ooru Thimiridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ooru Thimiridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguruthu Eguruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguruthu Eguruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Ooru Eguruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ooru Eguruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatadha Style-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatadha Style-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm-Ah Polalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm-Ah Polalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingada Visilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingada Visilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliyatum Sevulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliyatum Sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhapazham Gapla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhapazham Gapla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhapazham Vipparaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhapazham Vipparaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varasolla Posolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varasolla Posolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivar Vandhu Nottuvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivar Vandhu Nottuvaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Golimaare Gangsteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golimaare Gangsteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli Soda Boosteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli Soda Boosteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaraumaara Basteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaraumaara Basteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy Daddy Manthra Bedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy Daddy Manthra Bedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Maari Youngstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Youngstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthu Mathi Vastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthu Mathi Vastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam Maama 5 Star-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam Maama 5 Star-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunty Aunty One More 90
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunty Aunty One More 90"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Duuuraaahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duuuraaahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egatu Velayula Dhigula Kaatithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egatu Velayula Dhigula Kaatithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagula Pethutan Nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagula Pethutan Nammaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarumarula Yerumaaraa Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarumarula Yerumaaraa Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeruvaadiyila Settillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeruvaadiyila Settillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toll Gate-Ula Maasa Kaatithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toll Gate-Ula Maasa Kaatithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Kattala Appette-Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Kattala Appette-Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Basin Bridgela Base Voicela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basin Bridgela Base Voicela"/>
</div>
<div class="lyrico-lyrics-wrapper">Haanadha Eppdika En Vaayala Soluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haanadha Eppdika En Vaayala Soluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Labathiba Labathiba North Madraasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labathiba Labathiba North Madraasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tappu Tippnu Silence Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappu Tippnu Silence Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathivaakathula Semma Twistu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathivaakathula Semma Twistu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaven Namma Guesttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaven Namma Guesttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Omlette Vaayil Alludhu Taste-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omlette Vaayil Alludhu Taste-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aari Pochuna Adhuvum Waste-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aari Pochuna Adhuvum Waste-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Alari Pochuda East-U West-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alari Pochuda East-U West-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu Areaya Annan Annan Annan Bestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu Areaya Annan Annan Annan Bestu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Golimaare Gangsteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golimaare Gangsteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli Soda Boosteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli Soda Boosteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaraumaara Basteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaraumaara Basteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy Daddy Manthra Bedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy Daddy Manthra Bedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Maari Youngstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Youngstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthu Mathi Vastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthu Mathi Vastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam Maama 5 Star-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam Maama 5 Star-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunty Aunty One More 90
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunty Aunty One More 90"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roundu Rounda Yethikinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roundu Rounda Yethikinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soundu Sounda Pesikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu Sounda Pesikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanda Geenda Kilapikkinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanda Geenda Kilapikkinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udrra Udrra Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udrra Udrra Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Case-U Case-A Kudichikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Case-U Case-A Kudichikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Keta Kalandukinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Keta Kalandukinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odra Odra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odra Odra"/>
</div>
<div class="lyrico-lyrics-wrapper">Deividurra Vidurra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deividurra Vidurra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paati Sutta Vada Kaaka Thookala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paati Sutta Vada Kaaka Thookala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nekka Thukitom Kaakava Kaakava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekka Thukitom Kaakava Kaakava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiril Nikkkum Podhu Ethirae Varaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiril Nikkkum Podhu Ethirae Varaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Aeroplane Otraaru Otraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Aeroplane Otraaru Otraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goli Soda Goli Soda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli Soda Goli Soda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumuritaanda Kumuritaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumuritaanda Kumuritaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichavan Namma Aaluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichavan Namma Aaluda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanga Maatom Naangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga Maatom Naangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bossu Kalasu Ellorayum Velasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bossu Kalasu Ellorayum Velasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaru Style-A Ninnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaru Style-A Ninnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Under Ware-A Kilichitaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under Ware-A Kilichitaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Under Wolrd Don
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under Wolrd Don"/>
</div>
<div class="lyrico-lyrics-wrapper">Under Ware Gone Yo Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under Ware Gone Yo Yo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Golimaare Gangsteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golimaare Gangsteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli Soda Boosteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli Soda Boosteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaraumaara Basteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaraumaara Basteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy Daddy Manthra Bedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy Daddy Manthra Bedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Maari Youngstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Youngstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthu Mathi Vastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthu Mathi Vastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam Maama 5 Star-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam Maama 5 Star-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunty Aunty One More 90
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunty Aunty One More 90"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">French Beard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French Beard"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu Figure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu Figure"/>
</div>
<div class="lyrico-lyrics-wrapper">Ac Bus Tickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ac Bus Tickettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Decent Theatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Decent Theatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu Overu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu Overu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aii Bengaloru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Bengaloru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">French Beard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French Beard"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu Figure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu Figure"/>
</div>
<div class="lyrico-lyrics-wrapper">Ac Bus Tickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ac Bus Tickettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Decent Theatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Decent Theatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu Overu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu Overu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aii Hyderabad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Hyderabad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">French Beard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French Beard"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu Figure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu Figure"/>
</div>
<div class="lyrico-lyrics-wrapper">Ac Bus Tickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ac Bus Tickettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Decent Theatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Decent Theatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu Overu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu Overu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Next Mumbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Next Mumbai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">French Beard
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="French Beard"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu Figure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu Figure"/>
</div>
<div class="lyrico-lyrics-wrapper">Ac Bus Tickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ac Bus Tickettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Decent Theatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Decent Theatre"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu Overu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu Overu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Under Wolrd Don
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under Wolrd Don"/>
</div>
<div class="lyrico-lyrics-wrapper">Under Ware Gone Yo Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Under Ware Gone Yo Yo"/>
</div>
</pre>
