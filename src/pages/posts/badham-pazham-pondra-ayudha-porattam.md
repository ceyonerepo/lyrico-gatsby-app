---
title: "badham pazham pondra song lyrics"
album: "Ayudha Porattam"
artist: "Nandhan Raj - S.S. Athreya"
lyricist: "Andal Priyadarshini"
director: "Jai Akash"
path: "/albums/ayudha-porattam-lyrics"
song: "Badham Pazham Pondra"
image: ../../images/albumart/ayudha-porattam.jpg
date: 2011-09-09
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Madhumitha
  - Jessie Gift
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan laari vandi yeripoana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan laari vandi yeripoana"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kooda vaadaa aasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kooda vaadaa aasakkaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan laari vandi yeripoana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan laari vandi yeripoana"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kooda vaadaa aasakkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kooda vaadaa aasakkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dootti dootti naan pannapoaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dootti dootti naan pannapoaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei poatti poattu naan aadavaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei poatti poattu naan aadavaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai thottu paarkka kannam katti cherkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thottu paarkka kannam katti cherkka"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai ennam muttudhedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai ennam muttudhedi"/>
</div>
<div class="lyrico-lyrics-wrapper">innum yendi thalli poara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum yendi thalli poara"/>
</div>
<div class="lyrico-lyrics-wrapper">haa kitta vaadi enna sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haa kitta vaadi enna sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee solliputta naan seiyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee solliputta naan seiyavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">uchikotti poagum munney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchikotti poagum munney"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli poayaa ippo venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli poayaa ippo venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyo thalli poayaa ippo venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo thalli poayaa ippo venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey venum venum endrey nee jaadai seiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey venum venum endrey nee jaadai seiraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">HO kannaal kannaal nee thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HO kannaal kannaal nee thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondil poattaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondil poattaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey konjam konjam thodavenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey konjam konjam thodavenaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvai pattaaley karpamaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai pattaaley karpamaaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey konjam konjam thodavenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey konjam konjam thodavenaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvai pattaaley karpamaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai pattaaley karpamaaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">HO mutham thaaney en soththaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HO mutham thaaney en soththaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">andha vetkam thookki poadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha vetkam thookki poadu"/>
</div>
<div class="lyrico-lyrics-wrapper">soodaa mutham aakkipoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodaa mutham aakkipoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">en pakkam vandhu macham thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pakkam vandhu macham thedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thottuputtaa naan sokki poaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thottuputtaa naan sokki poaven"/>
</div>
<div class="lyrico-lyrics-wrapper">thaali katta nee rediyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali katta nee rediyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaali katta nee rediyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali katta nee rediyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaali katta nee rediyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali katta nee rediyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey naanaa venaam soldrendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naanaa venaam soldrendi"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo katrendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo katrendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa kaaththu kaaththu kedandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa kaaththu kaaththu kedandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">naachaan kedaichaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naachaan kedaichaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaa serndhadhey joadi onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaa serndhadhey joadi onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi paadudhey aasa ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paadudhey aasa ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaa serndhadhey joadi onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaa serndhadhey joadi onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi paadudhey aasa ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paadudhey aasa ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhaam pazham poandra ponnuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhaam pazham poandra ponnuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kunjithaa kanna killi poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunjithaa kanna killi poanaa"/>
</div>
</pre>
