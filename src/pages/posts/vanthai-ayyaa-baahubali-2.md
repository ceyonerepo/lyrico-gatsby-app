---
title: "vanthai ayyaa song lyrics"
album: "Baahubali 2"
artist: "M.M. Keeravani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli"
path: "/albums/baahubali-2-song-lyrics"
song: "Vanthai Ayyaa"
image: ../../images/albumart/baahubali-2.jpg
date: 2017-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PBxcAmsaLqY"
type: "Motivational"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">merkkai erkkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merkkai erkkadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhum sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhum sooriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">dharmam thorkaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharmam thorkaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aalum kaavalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalum kaavalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">merkkai erkkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merkkai erkkadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhum sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhum sooriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">dharmam thorkaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharmam thorkaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aalum kaavalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalum kaavalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasindhidum kanneerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasindhidum kanneerai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbida chey ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbida chey ayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varandidum nenjathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varandidum nenjathil"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhayena pey ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhayena pey ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aazh manathinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aazh manathinil"/>
</div>
<div class="lyrico-lyrics-wrapper">soozhum irulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soozhum irulai"/>
</div>
<div class="lyrico-lyrics-wrapper">neelum thuyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelum thuyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">paazhum vidhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paazhum vidhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neekkum theeye nee ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekkum theeye nee ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vandhai ayyaa vandhai ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhai ayyaa vandhai ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvai meendum thandhaai ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvai meendum thandhaai ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhai ayyaa vandhai ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhai ayyaa vandhai ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvai meendum thandhaai ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvai meendum thandhaai ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee veetridum thoaranai yaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veetridum thoaranai yaale"/>
</div>
<div class="lyrico-lyrics-wrapper">paaraigalum ariyaasanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaraigalum ariyaasanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">un perai thammil thaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un perai thammil thaame"/>
</div>
<div class="lyrico-lyrics-wrapper">sedhukidum kalvettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sedhukidum kalvettai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrodu un kural kettaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrodu un kural kettaal"/>
</div>
<div class="lyrico-lyrics-wrapper">pottal kaadum arasavaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottal kaadum arasavaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">un vervai oru thuli pattaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vervai oru thuli pattaal"/>
</div>
<div class="lyrico-lyrics-wrapper">olirudhu nelpattaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olirudhu nelpattaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un solle saatam ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un solle saatam ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai saasanam ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai saasanam ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en sindhai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sindhai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhai neeye seyum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhai neeye seyum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">engal aayul nee kol ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal aayul nee kol ayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vandhai ayyaa vandhai ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhai ayyaa vandhai ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvai meendum thandhaai ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvai meendum thandhaai ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhai ayyaa vandhai ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhai ayyaa vandhai ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvai meendum thandhaai ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvai meendum thandhaai ayya"/>
</div>
</pre>
