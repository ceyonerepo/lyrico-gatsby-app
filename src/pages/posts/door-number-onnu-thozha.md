---
title: "door number onnu song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Door Number Onnu"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zpcQe61Gg20"
type: "Party Song"
singers:
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Door Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Daashu Aimbathi Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Daashu Aimbathi Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhukku Keela Saiber Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Keela Saiber Rendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku Nagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku Nagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaa Sandhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaa Sandhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thar Roadu Sevappu Gate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thar Roadu Sevappu Gate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vandhu Soppa Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vandhu Soppa Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukandhu Nee Kaathirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukandhu Nee Kaathirundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Varuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Varuvendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Door Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Door Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Daashu Ambaththi Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Daashu Ambaththi Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhukku Keela Saiber Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Keela Saiber Rendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku Nagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku Nagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaa Sandhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaa Sandhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaar Roadu Sevappu Gate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaar Roadu Sevappu Gate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vandhu Soppa Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vandhu Soppa Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukkandhu Nee Kaathirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkandhu Nee Kaathirundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Varuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Varuvendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodumbodhu Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodumbodhu Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Kai Kulikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Kai Kulikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaluthil Mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaluthil Mutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vecha Necklace Maatikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vecha Necklace Maatikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa Yaaru Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa Yaaru Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathaa Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaa Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">TVyil Potta Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TVyil Potta Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaamal Konji Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamal Konji Konji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enjoy Panniko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Panniko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisu Kisu Kisu Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisu Kisu Kisu Vaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandaane Milli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandaane Milli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Busu Busu Busuvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Busu Busu Busuvaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavadha Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavadha Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaara Kaara Kaara Mikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaara Kaara Kaara Mikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillunu Nemili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillunu Nemili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaraa Thaakiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaraa Thaakiduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalta Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalta Alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Face Bookkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Face Bookkula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Photo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta Liku Buttonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Liku Buttonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Poora Theinjuduchaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Poora Theinjuduchaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Twitterla Ennoda Karutha Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitterla Ennoda Karutha Sonnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti Pasanga Sandaiyelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Pasanga Sandaiyelaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oinjuduchaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oinjuduchaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Instagramla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Instagramla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Nolanju Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Nolanju Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selfie Selvinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Selvinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Chella Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Chella Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whattsupla Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whattsupla Enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasigar Mandram Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasigar Mandram Nooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Video Vaa Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Video Vaa Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkatha Aal Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkatha Aal Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Social Networkil Elam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Social Networkil Elam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kodithaan Parakkuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kodithaan Parakkuthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ending Illa Trending
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ending Illa Trending"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Naanthanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Naanthanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisu Kisu Kiss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisu Kisu Kiss"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikka Vandaane Milli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikka Vandaane Milli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Busu Busu Busuvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Busu Busu Busuvaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavadha Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavadha Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaara Kaara Kaara Mikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaara Kaara Kaara Mikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillunu Nemili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillunu Nemili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru Maaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakiduva Asalta Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakiduva Asalta Alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Door Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Daashu Aimbathi Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Daashu Aimbathi Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhukku Keela Saiber Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Keela Saiber Rendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku Nagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku Nagaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaa Sandhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaa Sandhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thar Roadu Sevappu Gate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thar Roadu Sevappu Gate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vandhu Soppa Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vandhu Soppa Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukandhu Nee Kaathirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukandhu Nee Kaathirundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Varuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Varuvendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Door Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Daashu Aimbathi Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Daashu Aimbathi Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhukku Keela Saiber Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Keela Saiber Rendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku Nagaru Munnaa Sandhudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku Nagaru Munnaa Sandhudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thar Roadu Sevappu Gate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thar Roadu Sevappu Gate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vandhu Sofa Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vandhu Sofa Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukandhu Nee Varra Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukandhu Nee Varra Varaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathiruppom di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathiruppom di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Door Number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Door Number"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Daashu Aimbathi Aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Daashu Aimbathi Aaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhukku Keela Saiber Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukku Keela Saiber Rendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukku Nagaru Munnaa Sandhudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku Nagaru Munnaa Sandhudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thar Roadu Sevappu Gate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thar Roadu Sevappu Gate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vandhu Sofa Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vandhu Sofa Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukandhu Nee Varra Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukandhu Nee Varra Varaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathiruppom di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathiruppom di"/>
</div>
</pre>
