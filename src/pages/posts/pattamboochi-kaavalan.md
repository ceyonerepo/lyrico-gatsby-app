---
title: "pattamboochi song lyrics"
album: "Kaavalan"
artist: "Vidyasagar"
lyricist: "Kabilan"
director: "Siddique"
path: "/albums/kaavalan-lyrics"
song: "Pattamboochi"
image: ../../images/albumart/kaavalan.jpg
date: 2011-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Cjf6kIcarOc"
type: "love"
singers:
  - KK
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pattampoochi Kupidum Bothu Poove Odathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi Kupidum Bothu Poove Odathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Thaenai Saapidum Bothu Pesa Kudadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thaenai Saapidum Bothu Pesa Kudadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pataampoochi Kupidum Bothu Poovey Oodadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pataampoochi Kupidum Bothu Poovey Oodadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Thenai Saapidum Bothu Pesa Kudadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thenai Saapidum Bothu Pesa Kudadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaanai Thanthathin Silai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanai Thanthathin Silai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Yerum Thangathin Vilai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Yerum Thangathin Vilai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Veesiya Valai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Veesiya Valai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Katti Ilukaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Katti Ilukaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pataampoochi Kupidum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pataampoochi Kupidum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovey Odadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovey Odadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Thenai Sapidum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thenai Sapidum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesa Koodathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Koodathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethai Tharuvathu Naan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethai Tharuvathu Naan Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethai Peruvathu Thaan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethai Peruvathu Thaan Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurukum Nedukum Kuzhandhai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurukum Nedukum Kuzhandhai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Kudhithoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Kudhithoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Asaikuthu Un Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Asaikuthu Un Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavi Thavikuthu En Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavi Thavikuthu En Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruthi Pola Oruthi Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthi Pola Oruthi Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Panthaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Panthaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyaabagam Un Niyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaabagam Un Niyaabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Mudiyaadha Mudhalaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Mudiyaadha Mudhalaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poomugam Un Poomugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomugam Un Poomugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Mudiyaadha Mudhalpaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Mudiyaadha Mudhalpaagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Kavidhai Ival Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Kavidhai Ival Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Idhazhaal Padippaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhazhaal Padippaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Imaiyaal Ennai Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Imaiyaal Ennai Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Thirappaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thirappaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaampoochi Kupidum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaampoochi Kupidum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poove Oodadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Oodadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Kaadhal Thaenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Kaadhal Thaenai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saapidum Pothu Pesa Kudathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapidum Pothu Pesa Kudathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Varisaiyil Nee Sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Varisaiyil Nee Sirikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholai Thodarbinil Naan Iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Thodarbinil Naan Iruka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthadum Uthadum Pesum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthadum Uthadum Pesum Pozhudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Maranthaene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Maranthaene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhauginil Naan Iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhauginil Naan Iruka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Kuzhathinil Poo Mulaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Kuzhathinil Poo Mulaika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandaam Muraiyaai Idhayam Thudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandaam Muraiyaai Idhayam Thudika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthithaai Piranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaai Piranthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalaiyil Pon Malaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyil Pon Malaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Madi Meedhu Vizhuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madi Meedhu Vizhuveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbinil Un Maarbinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbinil Un Maarbinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Maruthaani Mazhai Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Maruthaani Mazhai Thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavo Nedunthooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavo Nedunthooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Nilavo Thodum Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Nilavo Thodum Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mazhayil Nanainthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mazhayil Nanainthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaichal Parandhodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaichal Parandhodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaamboochi Kupidum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaamboochi Kupidum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poove Odathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Odathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Thenai Sapidum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thenai Sapidum Bothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesa Koodadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Koodadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaanai Thanthathin Silai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanai Thanthathin Silai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Yerum Thangathin Vilai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Yerum Thangathin Vilai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Veesiya Valai Neeyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Veesiya Valai Neeyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Katti Ilukaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Katti Ilukaadhey"/>
</div>
</pre>
