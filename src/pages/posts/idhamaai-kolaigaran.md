---
title: "idhamaai song lyrics"
album: "Kolaigaran"
artist: "Simon K. King"
lyricist: "Dhamayanthi"
director: "Andrew Louis"
path: "/albums/kolaigaran-song-lyrics"
song: "Idhamaai"
image: ../../images/albumart/kolaigaran.jpg
date: 2019-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gFmDJrsNNuo"
type: "love"
singers:
  - Karthik
  - Keerthana Vaidyanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Anaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Anaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vathaipaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vathaipaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Neliyum Viralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Neliyum Viralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai Padipaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai Padipaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Anaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Anaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Sugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Sugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Padarvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Padarvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Endhan Udalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Endhan Udalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Padhipaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Padhipaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruvaaipe Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvaaipe Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muthathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirai Yedhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai Yedhum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Vetkathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Vetkathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaadum Bothu Unpeyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaadum Bothu Unpeyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Sollavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Anaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Anaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Sugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Sugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Padarvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Padarvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Endhan Udalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Endhan Udalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Padhipaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Padhipaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Thuli Neruppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thuli Neruppena"/>
</div>
<div class="lyrico-lyrics-wrapper">Idai Vazhi Padaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idai Vazhi Padaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai Ena Virinthu Seraaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Ena Virinthu Seraaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nodi Urangidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Urangidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugal Puthiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Puthiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinthida Thadaigal Sollaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinthida Thadaigal Sollaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillidum Udalinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillidum Udalinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pradhiyeduthaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pradhiyeduthaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manam Udaigal Thurakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Udaigal Thurakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oru Murai Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Murai Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Eraingidum Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraingidum Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Mounam Ketkkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Mounam Ketkkirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavinil Kadal Ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavinil Kadal Ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Vizhi Thavikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhi Thavikkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayalalul Valaigal Veesaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayalalul Valaigal Veesaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Sollidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Sollidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaguppugal Puthiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaguppugal Puthiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravinil Uraigal Sollaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravinil Uraigal Sollaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Udal Ondrena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Udal Ondrena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkugal Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkugal Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinthe Naanum Mayangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinthe Naanum Mayangugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirai Nilaa Pol Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirai Nilaa Pol Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadugal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadugal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral Mazhaiyaai Thoovugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral Mazhaiyaai Thoovugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhamaai Idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Anaipaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Anaipaaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Sugamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Sugamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Padarvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Padarvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Endhan Udalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Endhan Udalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Padhipaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Padhipaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruvaaipe Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvaaipe Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muthathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirai Yedhum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai Yedhum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Vetkathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Vetkathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavaadum Bothu Unpeyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaadum Bothu Unpeyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Sollavaa"/>
</div>
</pre>
