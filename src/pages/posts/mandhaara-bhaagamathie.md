---
title: "mandhaara song lyrics"
album: "Bhaagamathie"
artist: "S. Thaman"
lyricist: "Vivek"
director: "G. Ashok"
path: "/albums/bhaagamathie-lyrics"
song: "Mandhaara"
image: ../../images/albumart/bhaagamathie.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GMxsJjvtSTQ"
type: "happy"
singers:
  - Jyotsna Radhakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilaadum mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilaadum mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalodu thooral thandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madalodu thooral thandhaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraloodhum mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraloodhum mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhaleri kaadhil sendrara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhaleri kaadhil sendrara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilayinil thaavum kaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilayinil thaavum kaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyinai neeril podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyinai neeril podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai ariyamal odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai ariyamal odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai unarvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai unarvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai ariyamal neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai ariyamal neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival mana odai melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival mana odai melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ilai veesi ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ilai veesi ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir padarvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir padarvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh pollaadha megam pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh pollaadha megam pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu porida paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu porida paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ennagumo malar mannaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ennagumo malar mannaagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yendhi kolvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai yendhi kolvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilaadum mandhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilaadum mandhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalin melae thooral thandhaara aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madalin melae thooral thandhaara aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalaai pesum mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalaai pesum mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalileri kaadhil sendrara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalileri kaadhil sendrara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Singara aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singara aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallara aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallara aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Reengara aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reengara aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilaadum mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilaadum mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalodu thooral thandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madalodu thooral thandhaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhavidhamaga poigalai alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhavidhamaga poigalai alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinil poosi maraithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinil poosi maraithenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijangalai kaatum vilambaramaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijangalai kaatum vilambaramaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripinai idhazhil inaithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripinai idhazhil inaithenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai vandhu serum aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai vandhu serum aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil yerum osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil yerum osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal mudhalaaga keten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal mudhalaaga keten"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai adaivenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai adaivenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennodu yeno modhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennodu yeno modhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaakudhae un kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaakudhae un kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhinamtharum uyir kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhinamtharum uyir kalavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaikaamal nindrena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaikaamal nindrena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilaadum mandhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilaadum mandhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalin melae thooral thandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madalin melae thooral thandhaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalaai pesum mandhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalaai pesum mandhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalileri kaadhil sendrara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalileri kaadhil sendrara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhaara mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhaara mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilaadum mandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilaadum mandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalodu thooral thandhaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madalodu thooral thandhaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aaa haaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aaa haaa aa"/>
</div>
</pre>
