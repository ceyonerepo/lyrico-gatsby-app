---
title: "thamizhan pattu lyrics"
album: "Eeswaran"
artist: "Thaman S"
lyricist: "Yugabharathi"
director: "Susienthiran"
path: "/albums/eeswaran-song-lyrics"
song: "Thamizhan Pattu"
image: ../../images/albumart/eeswaran.jpg
date: 2021-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fWlJxwkfjnk"
type: "mass"
singers:
  - Ananthu
  - Deepak
  - Thaman S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ooraana oorayellam oonga vaikkum kolasaamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraana oorayellam oonga vaikkum kolasaamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera cholli vaarom munnera vazhi kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pera cholli vaarom munnera vazhi kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavilakku pottudarom maasumaru pokkidaiya….aa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavilakku pottudarom maasumaru pokkidaiya….aa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavadiyum thookkidurom kadaisi varai kaathidaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavadiyum thookkidurom kadaisi varai kaathidaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Varaan varaan…thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan…thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaan varaan…thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan…thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaan varaan adi dhill-ah thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan adi dhill-ah thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaan varaan adi dhill-ah thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan adi dhill-ah thamizhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naade kidu kidukka nattathuthaan poothiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naade kidu kidukka nattathuthaan poothiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum paraiyadukka nalla kaalam vanthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum paraiyadukka nalla kaalam vanthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumamum santhanamum neththi yengum ottiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumamum santhanamum neththi yengum ottiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttappatta koottam idhu kotaiyayum thottuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappatta koottam idhu kotaiyayum thottuduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thananthani naadu illai tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananthani naadu illai tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thattukkettu porathenna veli nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thattukkettu porathenna veli nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennai vaazha vaikkum tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ennai vaazha vaikkum tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee velai seiyya alli tharum vayakkaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velai seiyya alli tharum vayakkaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hei uthatula namakku thamizh irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei uthatula namakku thamizh irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraiyum kodukka uravirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiyum kodukka uravirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttula kedanthae thavikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttula kedanthae thavikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adakkura manushana madhikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adakkura manushana madhikkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naade kidu kidukka nattathuthaan poothiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naade kidu kidukka nattathuthaan poothiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum paraiyadukka nalla kaalam vanthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum paraiyadukka nalla kaalam vanthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumamum santhanamum neththi yengum ottiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumamum santhanamum neththi yengum ottiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttappatta koottam idhu kotaiyayum thottuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappatta koottam idhu kotaiyayum thottuduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thananthani naadu illai tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananthani naadu illai tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thattukkettu porathenna veli nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thattukkettu porathenna veli nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennai vaazha vaikkum tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ennai vaazha vaikkum tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee velai seiyya alli tharum vayakkaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velai seiyya alli tharum vayakkaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mannaathi mannarellam manna nambi ninnaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaathi mannarellam manna nambi ninnaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanathoda veeram kaakka sandai seiyya sonnanga…..aa….ae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanathoda veeram kaakka sandai seiyya sonnanga…..aa….ae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Porantha oora maranthu neeyum pozhappa paakka poniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porantha oora maranthu neeyum pozhappa paakka poniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthikkaatta maladiyakki naragam vaazhvu vaazhnthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthikkaatta maladiyakki naragam vaazhvu vaazhnthiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nilatha nee madhicha osanthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilatha nee madhicha osanthidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkura varaiyil sirichidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkura varaiyil sirichidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arupadai muruganum thunai iruppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arupadai muruganum thunai iruppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nella aruthida maniyena velanjiruppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nella aruthida maniyena velanjiruppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey varaan varaan intha thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey varaan varaan intha thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalatra thiruththi ezhudhum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalatra thiruththi ezhudhum thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naade kidu kidukka nattathuthaan poothiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naade kidu kidukka nattathuthaan poothiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum paraiyadukka nalla kaalam vanthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum paraiyadukka nalla kaalam vanthuduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumamum santhanamum neththi yengum ottiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumamum santhanamum neththi yengum ottiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttappatta koottam idhu kotaiyayum thottuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappatta koottam idhu kotaiyayum thottuduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thananthani naadu illai tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananthani naadu illai tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thattukkettu porathenna veli nadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thattukkettu porathenna veli nadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennai vaazha vaikkum tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ennai vaazha vaikkum tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee velai seiyya alli tharum vayakkaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velai seiyya alli tharum vayakkaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooraana oorayellam oonga vaikkum kolasaamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraana oorayellam oonga vaikkum kolasaamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
</pre>
