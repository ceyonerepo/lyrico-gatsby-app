---
title: "inthena song lyrics"
album: "Jaanu"
artist: "Govind Vasantha"
lyricist: "Sri Mani"
director: "C. Prem Kumar"
path: "/albums/jaanu-lyrics"
song: "Inthena"
image: ../../images/albumart/jaanu.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7i3M9RRId2c"
type: "sad"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inthena Inthena Oka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena Inthena Oka "/>
</div>
<div class="lyrico-lyrics-wrapper">Maataina Maataadavedhainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataina Maataadavedhainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthena Ika Inthenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena Ika Inthenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Aashalatho Alaa Nuvvu Nee Chenthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Aashalatho Alaa Nuvvu Nee Chenthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalame Maarinaa Dhoorame Cherinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Maarinaa Dhoorame Cherinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanthamegire Edaari Edurainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanthamegire Edaari Edurainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju Kosam Vechindi Naa Praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju Kosam Vechindi Naa Praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju Kooda Gelichindile Nee Mouname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju Kooda Gelichindile Nee Mouname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sootigaa Choopadhe Nee Gunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sootigaa Choopadhe Nee Gunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Chaatu Bhaavaala Bhaadhane Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaatu Bhaavaala Bhaadhane Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaa Cheppaali Elaa Adagaali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Cheppaali Elaa Adagaali "/>
</div>
<div class="lyrico-lyrics-wrapper">Aatalaadeti Raathala Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatalaadeti Raathala Nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paataalu Chadivina Kaalam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paataalu Chadivina Kaalam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Paataalu Nerpina Kaalam Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paataalu Nerpina Kaalam Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham Avvanee Paatamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham Avvanee Paatamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Kshanam Naa Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Kshanam Naa Nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhraalu Dhaatenu Naa Rekkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhraalu Dhaatenu Naa Rekkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraalu Thaakenu Naa Parugule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraalu Thaakenu Naa Parugule"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Maatram Nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Maatram Nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidichina Chotune Aagene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidichina Chotune Aagene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repati Oohalu Ninnati Aashale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati Oohalu Ninnati Aashale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeti Paatalu Ninnu Daatanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeti Paatalu Ninnu Daatanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju Kosam Veechindi Naa Praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju Kosam Veechindi Naa Praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju Kooda Ninnu Ave Ponivvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju Kooda Ninnu Ave Ponivvane"/>
</div>
</pre>
