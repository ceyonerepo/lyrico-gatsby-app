---
title: "theera oru song lyrics"
album: "Kadikara Manithargal"
artist: "Sam CS"
lyricist: "Na Muthukumar"
director: "Vaigarai Balan"
path: "/albums/kadikara-manithargal-lyrics"
song: "Theera Oru"
image: ../../images/albumart/kadikara-manithargal.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7Su8o6pXfPI"
type: "sad"
singers:
  - Sam CS
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theeraa oru aanandhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraa oru aanandhamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann munnilae therigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann munnilae therigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ul nenjilae inbangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul nenjilae inbangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaipattu yeno tholaigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaipattu yeno tholaigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinjondru koottil adaipadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinjondru koottil adaipadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanjendra vaazhkkai nagargiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjendra vaazhkkai nagargiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkaedhum theriya paravaiya pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkaedhum theriya paravaiya pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhoda manamum thudikkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhoda manamum thudikkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Or naaliley vin meengalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or naaliley vin meengalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vaazhkkai vaanil pookkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vaazhkkai vaanil pookkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam kanavugalai yaar thiruduvadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam kanavugalai yaar thiruduvadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vidhivalaigal yaar ezhudhuvaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vidhivalaigal yaar ezhudhuvaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam idam peyarum or paravaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam idam peyarum or paravaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam kudi peyara sondha koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kudi peyara sondha koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkillaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkillaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadoodi polae vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadoodi polae vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarudhae nagarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarudhae nagarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbennum koottukkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbennum koottukkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vidha sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vidha sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudhae varudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudhae varudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pirandhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirandhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu veridam illaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu veridam illaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen naragam idhil vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen naragam idhil vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pirandhanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pirandhanaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner vazhigalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner vazhigalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellum oor idhu illaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellum oor idhu illaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan enum orumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan enum orumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inguthaan olithidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inguthaan olithidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor vittu vandhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor vittu vandhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhathudikkudhae or inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhathudikkudhae or inam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu silandhi valaiyin koottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu silandhi valaiyin koottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhingu nelindhidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhingu nelindhidudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum"/>
</div>
</pre>
