---
title: "vaaran unna vechu song lyrics"
album: "Kennedy Club"
artist: "D. Imman"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/kennedy-club-lyrics"
song: "Vaaran Unna Vechu"
image: ../../images/albumart/kennedy-club.jpg
date: 2019-08-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JT3jLzfAB8w"
type: "mass"
singers:
  - Santhosh Hariharan
  - Shenbagaraj
  - Vignesh Narayanan
  - Deepak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaaraan Unna Vechu Seiya Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Unna Vechu Seiya Poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kattabomman Ooraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kattabomman Ooraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kottam Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kottam Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae Mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Pinna Kuththu Uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Pinna Kuththu Uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda Suda Suda Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda Suda Suda Suda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda Suda Suda Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda Suda Suda Suda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraan Un Moonji Mela Pooraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Un Moonji Mela Pooraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththa Oda Uda Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Oda Uda Poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Attam Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Attam Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae Mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaru Ivan Ninnaa Thaarumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Ivan Ninnaa Thaarumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenju Manja Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenju Manja Soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththa Alla Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Alla Poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae Mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Pinna Kuththu Uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Pinna Kuththu Uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda Suda Suda Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda Suda Suda Suda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda Suda Suda Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda Suda Suda Suda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suda Suda Suda Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda Suda Suda Suda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda Suda Suda Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda Suda Suda Suda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraan Un Moonji Mela Pooraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Un Moonji Mela Pooraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththa Oda Uda Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Oda Uda Poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Attam Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Attam Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae Mavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaru Ivan Ninnaa Thaarumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Ivan Ninnaa Thaarumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenju Manja Soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenju Manja Soru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththa Alla Poraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa Alla Poraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavanae Mavanae Mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavanae Mavanae Mavanae"/>
</div>
</pre>
