---
title: 'kannazhaga song lyrics'
album: '3'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Soundharya Rajinikanth'
path: '/albums/3-song-lyrics'
song: 'Kannazhaga'
image: ../../images/albumart/3.jpg
date: 2012-03-30
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/ptB_6oJ3vGY'
type: 'duet'
singers: 
- Dhanush
- Shruti Haasan
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Kannazhaga.. kaalazhaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannazhaga.. kaalazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnazhaga.. penn azhaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnazhaga.. penn azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaeyo thaedi sellum.. viral azhaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Engaeyo thaedi sellum.. viral azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaigal korthu kollum vidham azhaga
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaigal korthu kollum vidham azhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirae uyirae unaivida edhuvum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirae uyirae unaivida edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyril peridhaai illayadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyril peridhaai illayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae azhagae unaivida edhuvum
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagae azhagae unaivida edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagil azhagaai illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagil azhagaai illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Engaeyo paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Engaeyo paarkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna solgiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennenna solgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaigal thaandida
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaigal thaandida"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayangam seikiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Maayangam seikiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnakul paarkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnakul paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulladhai solgiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulladhai solgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnuyir serndhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnuyir serndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhi paarkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vazhi paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhazhum idhazhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhazhum idhazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiyattumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaiyattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhidhaai vazhigal illai
<input type="checkbox" class="lyrico-select-lyric-line" value="pudhidhaai vazhigal illai"/>
</div>
  <div class="lyrico-lyrics-wrapper">Immaigal moodi aruginil vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Immaigal moodi aruginil vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol edhuvum illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhupol edhuvum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnakul paarkava
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnakul paarkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulladhai ketkkava
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulladhai ketkkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennuyir serndhida
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennuyir serndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhi sollava
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vazhi sollava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannazhagae perazhagae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannazhagae perazhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Penn azhagae ennazhagae
<input type="checkbox" class="lyrico-select-lyric-line" value="Penn azhagae ennazhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirae uyirae unaivida edhuvum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirae uyirae unaivida edhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyril peridhaai illayadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyril peridhaai illayadi"/>
</div>
</pre>