---
title: "karuvinil ennai sumanthu song lyrics"
album: "penguin"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Eashvar Karthic"
path: "/albums/penguin-lyrics"
song: "Karuvinil Ennai Sumanthu"
image: ../../images/albumart/penguin.jpg
date: 2020-06-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0W81Ks7UvWY"
type: "emotional"
singers:
  - Ananya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">karuvinil ennai sumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvinil ennai sumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvinil nee nadanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvinil nee nadanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">therinil oorvalame amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinil oorvalame amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poochandi varum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poochandi varum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">munthanai thirai porthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthanai thirai porthi"/>
</div>
<div class="lyrico-lyrics-wrapper">mana payam theerthaye amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana payam theerthaye amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaanatha kadavuluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanatha kadavuluku"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaigal vanangathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaigal vanangathe"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku en uyire aarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku en uyire aarathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellam antha oorinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellam antha oorinile"/>
</div>
<div class="lyrico-lyrics-wrapper">sirai patta oomaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirai patta oomaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannai geli seiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannai geli seiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ratha kaneer sinthi manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha kaneer sinthi manam"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam kalanguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam kalanguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerai un kaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerai un kaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">thudaithu pogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudaithu pogume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirulla kadavulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirulla kadavulai"/>
</div>
<div class="lyrico-lyrics-wrapper">un uruvil parkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un uruvil parkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">ne than nambikai endrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne than nambikai endrume"/>
</div>
</pre>
