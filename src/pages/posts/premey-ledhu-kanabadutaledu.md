---
title: "premey ledhu song lyrics"
album: "Kanabadutaledu"
artist: "Madhu Ponnas"
lyricist: "Madhunandan"
director: "Balaraju M"
path: "/albums/kanabadutaledu-lyrics"
song: "Premey Ledhu"
image: ../../images/albumart/kanabadutaledu.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/A7y58cfEIf4"
type: "sad"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye premey ledhu baadhey ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye premey ledhu baadhey ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeke chappudu ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeke chappudu ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey car-ey ledhu bangala ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey car-ey ledhu bangala ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhukey ammai ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhukey ammai ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartuney ichaney aavirai poyaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartuney ichaney aavirai poyaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartuney ichaney aavirai poyaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartuney ichaney aavirai poyaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Inninallu nuvvu nenu okkatantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inninallu nuvvu nenu okkatantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentha baga cheyppeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentha baga cheyppeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaloney ayya oppukodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaloney ayya oppukodu "/>
</div>
<div class="lyrico-lyrics-wrapper">antu ontarodni chesanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antu ontarodni chesanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammo nammodhu ammayila premalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo nammodhu ammayila premalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakinchestharu mana life ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakinchestharu mana life ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyammo nammodhu make up musugulnee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyammo nammodhu make up musugulnee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam chesthare magavalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam chesthare magavalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye premey ledhu baadhey ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye premey ledhu baadhey ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeke chappudu ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeke chappudu ledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvey nuvvey arey naatho unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvey nuvvey arey naatho unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dasha thirigindhile anukunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dasha thirigindhile anukunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke neeke na jindhagi mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke neeke na jindhagi mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika raasichay yela anukunnaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika raasichay yela anukunnaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem ayina jaali leni dhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem ayina jaali leni dhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu oochakotha kosinavu lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu oochakotha kosinavu lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulenno marchinavu jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulenno marchinavu jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu raayilaga chesinavu lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu raayilaga chesinavu lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanta neerey pettakunda ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanta neerey pettakunda ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">nenu chusukunta nantiney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu chusukunta nantiney"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa kanti neerey naku nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa kanti neerey naku nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">mutta jeppi chudakunda pothive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutta jeppi chudakunda pothive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammo nammodhu ammayila premalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo nammodhu ammayila premalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakinchestharu mana life ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakinchestharu mana life ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyammo nammodhu make up musugulnee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyammo nammodhu make up musugulnee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam chesthare magavalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam chesthare magavalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachi poye aa varsham laage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachi poye aa varsham laage"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka muddhe petti muripisthaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka muddhe petti muripisthaarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkarleni aa phojey kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkarleni aa phojey kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu konchem konchem karigisthare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu konchem konchem karigisthare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugguloki dinchutharu maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugguloki dinchutharu maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu mentalonni chestharu maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu mentalonni chestharu maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunga muthi pedatharu maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bunga muthi pedatharu maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathukey narakam chestharey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathukey narakam chestharey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Istamaithey eppudaithey ekkadaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istamaithey eppudaithey ekkadaina "/>
</div>
<div class="lyrico-lyrics-wrapper">rechipothu untarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rechipothu untarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekapothey kopamanatha kummarinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekapothey kopamanatha kummarinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoola therche pothare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoola therche pothare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammo nammodhu ammayila premalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammo nammodhu ammayila premalni"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakinchestharu mana life ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakinchestharu mana life ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyammo nammodhu make up musugulnee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyammo nammodhu make up musugulnee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam chesthare magavalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam chesthare magavalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam agam agam chestharey magavalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam agam agam chestharey magavalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam chestharey magavalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam chestharey magavalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam agam agam chestharey magavalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam agam agam chestharey magavalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam chestharey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam chestharey"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam agam agam chestharey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam agam agam chestharey"/>
</div>
</pre>
