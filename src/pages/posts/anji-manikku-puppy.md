---
title: "anji manikku song lyrics"
album: "Puppy"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Nattu Dev"
path: "/albums/puppy-lyrics"
song: "Anji Manikku"
image: ../../images/albumart/puppy.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OdWK_ChKkus"
type: "love"
singers:
  - Yuvan Shankar Raja
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andha Saalai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Saalai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhi Saayum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhi Saayum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Saelai Aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Saelai Aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nejam Vaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nejam Vaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pesaa Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesaa Nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kollumpadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kollumpadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Vesham Poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vesham Poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Katti Aaluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Katti Aaluthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enbathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enbathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Nee Thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nee Thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendhu Dhaan Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendhu Dhaan Yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Kaadhal Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Kaadhal Kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peasama Nee Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peasama Nee Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Dhaan Naam Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Dhaan Naam Serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichipom Di Pudichipom Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichipom Di Pudichipom Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5 <div class="lyrico-lyrics-wrapper">Maniku Un Kaiya Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Un Kaiya Pudichen"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Unna Katti Anachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Unna Katti Anachen"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Koduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Koduthen"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniku Naan Thookathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Naan Thookathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Mulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Mulichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey 5 Maniku Un Kaiya Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey 5 Maniku Un Kaiya Pudichen"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Unna Katti Anachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Unna Katti Anachen"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Koduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Koduthen"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniku Naan Thookathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Naan Thookathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Mulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Mulichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Rendu Meiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendu Meiyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vandhu Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vandhu Paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Poruthathu Pothumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Poruthathu Pothumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi Vadi Odi Odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Vadi Odi Odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Seiya Naanum Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Seiya Naanum Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey 5 Maniku Un Kaiya Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey 5 Maniku Un Kaiya Pudichen"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Unna Katti Anachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Unna Katti Anachen"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Koduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Koduthen"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniku Naan Thookathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Naan Thookathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Mulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Mulichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Saalai Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Saalai Oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaathiruntha Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaathiruntha Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mounamaaga Rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mounamaaga Rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulla Naanum Sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulla Naanum Sirichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paarvai Dhinusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarvai Dhinusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enakku Romba Pudhusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enakku Romba Pudhusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Enakkae Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Enakkae Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannu Kaatuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannu Kaatuthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Listu Podu Un Aasai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Listu Podu Un Aasai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nalla Naal Varum Varai Kaathirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nalla Naal Varum Varai Kaathirudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yekamulla Oonjal Aaduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekamulla Oonjal Aaduthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kalyaanam Pannitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kalyaanam Pannitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anachikadaa Katti Pudichikadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anachikadaa Katti Pudichikadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5 <div class="lyrico-lyrics-wrapper">Maniku En Kaiya Pudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku En Kaiya Pudicha"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Enna Katti Anacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Enna Katti Anacha"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Kodutha"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniyila Edhukudaa Kanna Mulicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniyila Edhukudaa Kanna Mulicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5 <div class="lyrico-lyrics-wrapper">Maniku En Kaiya Pudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku En Kaiya Pudicha"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Enna Katti Anacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Enna Katti Anacha"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Kodutha"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniyila Edhukudaa Kanna Mulicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniyila Edhukudaa Kanna Mulicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Rendu Meiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendu Meiyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vandhu Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vandhu Paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Poruthathu Pothumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Poruthathu Pothumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi Vadi Odi Odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Vadi Odi Odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Seiya Naanum Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Seiya Naanum Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Sollaal En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Sollaal En Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla Vaarthai Ingu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Vaarthai Ingu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swaasam Unnai Theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swaasam Unnai Theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kanna Adika Nee Katti Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanna Adika Nee Katti Pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Enakkae Killi Paaka Thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Enakkae Killi Paaka Thonuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5 <div class="lyrico-lyrics-wrapper">Maniku Un Kaiya Pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Un Kaiya Pudichen"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Unna Katti Anachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Unna Katti Anachen"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Koduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Koduthen"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Porudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Porudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaathu Iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaathu Iruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanavellaam Palika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavellaam Palika"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Varumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Varumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Ellaam Nitham Serthu Vaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Ellaam Nitham Serthu Vaiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Mutta Mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Mutta Mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Thara Kathirukendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Thara Kathirukendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Rendu Meiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendu Meiyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vandhu Paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vandhu Paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Poruthathu Pothumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Poruthathu Pothumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadi Vadi Vadi Vadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Vadi Vadi Vadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Readyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Readyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Readyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Readyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
5 <div class="lyrico-lyrics-wrapper">Maniku En Kaiya Pudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku En Kaiya Pudicha"/>
</div>
6 <div class="lyrico-lyrics-wrapper">Maniku Enna Katti Anacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Enna Katti Anacha"/>
</div>
7 <div class="lyrico-lyrics-wrapper">Maniku Oru Mutham Kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniku Oru Mutham Kodutha"/>
</div>
8 <div class="lyrico-lyrics-wrapper">Maniyila Edhukudaa Kanna Mulicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maniyila Edhukudaa Kanna Mulicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathiruppen"/>
</div>
</pre>
