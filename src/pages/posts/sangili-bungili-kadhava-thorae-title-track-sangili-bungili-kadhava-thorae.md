---
title: "sangili bungili kadhava thorae title track song lyrics"
album: "Sangili Bungili Kadhava Thorae"
artist: "Vishal Chandrasekhar"
lyricist: "Na Muthu Kumar"
director: "Ike"
path: "/albums/sangili-bungili-kadhava-thorae-lyrics"
song: "Sangili Bungili Kadhava Thorae Title Track"
image: ../../images/albumart/sangili-bungili-kadhava-thorae.jpg
date: 2017-05-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RmFdkU-LPcU"
type: "title track"
singers:
  -	Arunraja Kamaraj
  - Premgi Amaren
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thunindhu sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu sel"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai udaithidu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai udaithidu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppangal thaandi nee poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppangal thaandi nee poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodarum ovvoru adiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum ovvoru adiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini nee un peyar kurithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini nee un peyar kurithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithu norukkida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithu norukkida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaindhu sel imaiyathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindhu sel imaiyathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Eralam vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eralam vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru karam koththu nee poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru karam koththu nee poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannikkum ovvoru uyirgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannikkum ovvoru uyirgalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil bayathai marandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil bayathai marandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu vendridum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu vendridum vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum ovvoru alaigal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum ovvoru alaigal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerin siruthuli thaanae nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerin siruthuli thaanae nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasum ovvoru neruppin thuligalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasum ovvoru neruppin thuligalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai erimalai aagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai erimalai aagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum ovvoru payanam endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum ovvoru payanam endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorai sernthida thaanae nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai sernthida thaanae nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil nooraayiram peigalum varalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil nooraayiram peigalum varalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhal vilagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhal vilagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Open the door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open the door"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Open the door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open the door"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga vanthom nu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga vanthom nu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba vanthutom nu (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba vanthutom nu (Dialogue)"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil namaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil namaku "/>
</div>
<div class="lyrico-lyrics-wrapper">inbathai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbathai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbamum vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbamum vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil vizhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil vizhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhundhal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhundhal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivugal thondrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivugal thondrumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oru thisaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru thisaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru thisaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru thisaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaithadhu kaalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaithadhu kaalamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam endra vaarthaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam endra vaarthaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam ondru sergaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam ondru sergaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithathu palikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathu palikkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili open the door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili open the door"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili sangili bungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili sangili bungil sangili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili sangili bungil sangili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranthu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranthu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranthu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranthu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranthu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranthu paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili kadhava thorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili kadhava thorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Open the door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open the door"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili bungili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili bungili"/>
</div>
<div class="lyrico-lyrics-wrapper">Open the door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open the door"/>
</div>
</pre>
