---
title: "edho edho song lyrics"
album: "Ninnu Thalachi"
artist: "Yellender mahaveera"
lyricist: "poornachary"
director: "Anil thota"
path: "/albums/ninnu-thalachi-lyrics"
song: "Edho Edho"
image: ../../images/albumart/ninnu-thalachi.jpg
date: 2019-09-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TrhQxsfJELg"
type: "love"
singers:
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">edho ededho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edho ededho"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyenanta naakila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyenanta naakila"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve chudakunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve chudakunte"/>
</div>
<div class="lyrico-lyrics-wrapper">unnadanta mayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnadanta mayala"/>
</div>
<div class="lyrico-lyrics-wrapper">ila rendu kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ila rendu kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chuse aashalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chuse aashalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ninne dhachukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne dhachukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">cheppaleni oohalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppaleni oohalo"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi peru"/>
</div>
<div class="lyrico-lyrics-wrapper">needhe telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhe telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuna theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuna theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">neela naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neela naala"/>
</div>
<div class="lyrico-lyrics-wrapper">yevaru leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevaru leru"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhe veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhe veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andham ante ardham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andham ante ardham "/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve navvadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve navvadam"/>
</div>
<div class="lyrico-lyrics-wrapper">navve ninnu chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navve ninnu chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">gallo theladam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gallo theladam"/>
</div>
<div class="lyrico-lyrics-wrapper">chuse rendu kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuse rendu kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">yedho cheppene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedho cheppene"/>
</div>
<div class="lyrico-lyrics-wrapper">bhavam yemitina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhavam yemitina"/>
</div>
<div class="lyrico-lyrics-wrapper">anthe chalune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthe chalune"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi peru"/>
</div>
<div class="lyrico-lyrics-wrapper">needhe telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhe telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuna theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuna theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">neela naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neela naala"/>
</div>
<div class="lyrico-lyrics-wrapper">yevaru leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevaru leru"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhe veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhe veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raase unnadhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raase unnadhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku naakila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku naakila"/>
</div>
<div class="lyrico-lyrics-wrapper">chuse manasunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuse manasunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chudosarila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chudosarila"/>
</div>
<div class="lyrico-lyrics-wrapper">neetho undaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho undaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">roje yendhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roje yendhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">ninna nedu repu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninna nedu repu"/>
</div>
<div class="lyrico-lyrics-wrapper">naatho undi po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatho undi po"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo kalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo kalige"/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi peru"/>
</div>
<div class="lyrico-lyrics-wrapper">needhe telusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhe telusa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuna theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuna theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">neela naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neela naala"/>
</div>
<div class="lyrico-lyrics-wrapper">yevaru leru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevaru leru"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhe veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhe veru"/>
</div>
</pre>
