---
title: "ponga vechom song lyrics"
album: "Udhayan"
artist: "Manikanth Kadri"
lyricist: "Vaali - Yugabharathi - Annamalai - Surya - Muthamil"
director: "Chaplin"
path: "/albums/udhayan-lyrics"
song: "Ponga Vechom"
image: ../../images/albumart/udhayan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Vivek Narayan
  - Divya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pongavachom poova vachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongavachom poova vachom"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu naanga poosa vachoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu naanga poosa vachoam"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavarukkum nallavazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavarukkum nallavazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaattida vaa aiyanaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattida vaa aiyanaarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edudaa edudaa malaiya adidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edudaa edudaa malaiya adidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adidaa vediya vediya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adidaa vediya vediya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru ulagam kaanaadhadhaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru ulagam kaanaadhadhaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga pudhusaa aadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga pudhusaa aadavandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanaanguyilu paadaadha paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanaanguyilu paadaadha paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru dhinusaa paadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru dhinusaa paadavandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya kaiya katti kondaadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya kaiya katti kondaadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">etti solla mattum ennaadheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etti solla mattum ennaadheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">poagaama ninnudunga joaraaga kumbidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poagaama ninnudunga joaraaga kumbidunga"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyanaaru sannidhinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyanaaru sannidhinga"/>
</div>
<div class="lyrico-lyrics-wrapper">idha naama thozhudhaa nimmadhinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idha naama thozhudhaa nimmadhinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru ulagam kaanaadhadhaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru ulagam kaanaadhadhaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga pudhusaa aadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga pudhusaa aadavandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanaanguyilu paadaadha paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanaanguyilu paadaadha paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru dhinusaa paadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru dhinusaa paadavandhoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaroada neeravachi thaadhaiyil ooravachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaroada neeravachi thaadhaiyil ooravachi"/>
</div>
<div class="lyrico-lyrics-wrapper">peroada vaazhavaikkum aiyanaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peroada vaazhavaikkum aiyanaare"/>
</div>
<div class="lyrico-lyrics-wrapper">noagaama paasam vachi noayellaam theeravachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noagaama paasam vachi noayellaam theeravachi"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraaga oora kaakka vakkiraarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraaga oora kaakka vakkiraarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadu medu pookkavachi kaasu naalu serthuvachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu medu pookkavachi kaasu naalu serthuvachi"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu maadu kooda sera poosa vachoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu maadu kooda sera poosa vachoam"/>
</div>
<div class="lyrico-lyrics-wrapper">veedu vaasal aarambichi veedhiyoada nesam vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedu vaasal aarambichi veedhiyoada nesam vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">meendu vaazhathaaney naanga aasa vachoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendu vaazhathaaney naanga aasa vachoam"/>
</div>
<div class="lyrico-lyrics-wrapper">illa illa enna illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa illa enna illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">illa illa enna illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa illa enna illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivarappoala oruvarilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivarappoala oruvarilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivarappoala oruvarilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivarappoala oruvarilla"/>
</div>
<div class="lyrico-lyrics-wrapper">solla solla enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla solla enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">varuma koattil eththana tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuma koattil eththana tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">vellai vellai enna vellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellai vellai enna vellai"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuppooraa madhura vellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuppooraa madhura vellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru ulagam kaanaadhadhaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru ulagam kaanaadhadhaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga pudhusaa aadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga pudhusaa aadavandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanaanguyilu paadaadha paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanaanguyilu paadaadha paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru dhinusaa paadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru dhinusaa paadavandhoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serndhu odhukkivittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu odhukkivittu "/>
</div>
<div class="lyrico-lyrics-wrapper">sendhooradha pottuvachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendhooradha pottuvachi"/>
</div>
<div class="lyrico-lyrics-wrapper">nannaa nannaa naanannaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannaa nannaa naanannaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sendhooradha pottu vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendhooradha pottu vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">nannaa nannaa naanannaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannaa nannaa naanannaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">soakku nadai nadandhaa sokkudhaiyaa ungamela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soakku nadai nadandhaa sokkudhaiyaa ungamela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaadha Oviyatha kandeney poo mugatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaadha Oviyatha kandeney poo mugatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadaadhey neeyirukka vannathoada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadaadhey neeyirukka vannathoada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaadha kaariyatha aaraadha vaalibatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaadha kaariyatha aaraadha vaalibatha"/>
</div>
<div class="lyrico-lyrics-wrapper">maariyaa aadharikkum ennathoada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maariyaa aadharikkum ennathoada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamanoada seedhaipoala maaridaadha vaasathoada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamanoada seedhaipoala maaridaadha vaasathoada"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavendum nooru aandu vaadibulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavendum nooru aandu vaadibulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhaiyoada saadham poala yengum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaiyoada saadham poala yengum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhampoala kaadhalodu neenga venum sedhi solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhampoala kaadhalodu neenga venum sedhi solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla nalla vandhu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla nalla vandhu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">thodanguveney thavarupanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodanguveney thavarupanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illa illa thandhu enna mayangureney urasithinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa illa thandhu enna mayangureney urasithinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholla tholla rumba tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholla tholla rumba tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhaguvoamey manasu thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhaguvoamey manasu thinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru ulagam kaanaadhadhaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru ulagam kaanaadhadhaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga pudhusaa aadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga pudhusaa aadavandhoam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanaanguyilu paadaadha paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanaanguyilu paadaadha paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru dhinusaa paadavandhoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru dhinusaa paadavandhoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pongavachom poova vachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongavachom poova vachom"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu naanga poosa vachoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu naanga poosa vachoam"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavarukkum nallavazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavarukkum nallavazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaattida vaa aiyanaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattida vaa aiyanaarey"/>
</div>
</pre>
