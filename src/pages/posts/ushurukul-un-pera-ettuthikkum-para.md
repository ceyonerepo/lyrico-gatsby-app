---
title: "ushurukul un pera song lyrics"
album: "Ettuthikkum Para"
artist: "M. S. Sreekanth"
lyricist: "Snehan"
director: "Keera"
path: "/albums/ettuthikkum-para-song-lyrics"
song: "Ushurukul un pera"
image: ../../images/albumart/ettuthikkum-para.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yOsvT7_flQk"
type: "love"
singers:
  - Anuradha Sriram
  - Keshav Vinod
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nana naana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana naana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana naana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana naana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana naana naa naa naa naa naaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana naana naa naa naa naa naaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurukkul unpera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurukkul unpera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezuthi vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezuthi vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moochil en moocha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochil en moocha"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu thechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu thechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmela mattum dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmela mattum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkitta enna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta enna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvai patta neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai patta neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ver arunthu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ver arunthu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un verva pattathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un verva pattathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor arunthu ponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor arunthu ponenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurukkul unpera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurukkul unpera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezuthi vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezuthi vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moochil en moocha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochil en moocha"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu thechen hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu thechen hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enakku podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enakku podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan neranju valiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan neranju valiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi pantha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi pantha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaana sozhaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaana sozhaluren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naththa kootta pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naththa kootta pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakkul adanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakkul adanguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththamindri unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththamindri unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mulusa mulunguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mulusa mulunguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulasaami polathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulasaami polathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En munnaala nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En munnaala nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Marankoththi polathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marankoththi polathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna ne koththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna ne koththura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kozhi kunja pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kozhi kunja pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna poththi veikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna poththi veikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha paavi nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha paavi nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruvaada sekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruvaada sekkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellanthi paasatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellanthi paasatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela pesa yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela pesa yaarumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurukkul unpera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurukkul unpera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezuthi vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezuthi vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moochil en moocha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochil en moocha"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu thechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu thechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hoo oo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hoo oo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangai regai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangai regai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera solluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pera solluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla vekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla vekkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kuththi kolluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kuththi kolluthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangkaalu nizhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangkaalu nizhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna otta vechuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna otta vechuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura mattum pichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura mattum pichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee veliyae nattutaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee veliyae nattutaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaagayam pola nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaagayam pola nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaeyum nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaeyum nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Appappo enna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appappo enna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopookka vaikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopookka vaikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi neeyum naanum vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi neeyum naanum vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kovil kattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kovil kattuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna yaarum theendinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna yaarum theendinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ninnu vettuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ninnu vettuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En jaami nee dhaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jaami nee dhaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna vittu poga matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna vittu poga matten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurukkul unpera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurukkul unpera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezuthi vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezuthi vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moochil en moocha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochil en moocha"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu thechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu thechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmela mattum dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmela mattum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkitta enna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta enna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa vechen"/>
</div>
</pre>
