---
title: "thaila thaila song lyrics"
album: "Vaagai Sooda Vaa"
artist: "Ghibran"
lyricist: "Ve. Ramasamy"
director: "A. Sarkunam"
path: "/albums/vaagai-sooda-vaa-lyrics"
song: "Thaila Thaila"
image: ../../images/albumart/vaagai-sooda-vaa.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cDVS9QMJYt0"
type: "happy"
singers:
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh ohhoo oh oh oh hoo ohhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhoo oh oh oh hoo ohhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohhoo oh oh oh hoo ohhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhoo oh oh oh hoo ohhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaila thaila thaila thaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaila thaila thaila thaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaila thaila thaila kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaila thaila thaila kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaila thaila thaila thaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaila thaila thaila thaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthan thaiyaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthan thaiyaloo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah thaila thaila thaila thaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah thaila thaila thaila thaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaila thaila thaila kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaila thaila thaila kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaila thaila thaila thaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaila thaila thaila thaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthan thaiyaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthan thaiyaloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam poochi rekkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam poochi rekkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi thaan pakka vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi thaan pakka vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Malakka thaan parakka vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malakka thaan parakka vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veta veli pottalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veta veli pottalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nondi mazhai paiyaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondi mazhai paiyaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Senga soola poothiruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga soola poothiruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seventhiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seventhiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ohhoo oh oh oh hoo ohhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhoo oh oh oh hoo ohhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohhoo oh oh oh hoo ohhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhoo oh oh oh hoo ohhoo"/>
</div>
</pre>
