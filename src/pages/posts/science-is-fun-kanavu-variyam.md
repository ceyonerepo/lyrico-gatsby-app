---
title: "science is fun song lyrics"
album: "Kanavu Variyam"
artist: "Shyam Benjamin"
lyricist: "Arun Chidambaram"
director: "Arun Chidambaram"
path: "/albums/kanavu-variyam-lyrics"
song: "Science Is Fun"
image: ../../images/albumart/kanavu-variyam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-e2q6i-QjsI"
type: "mass"
singers:
  - Sharmila
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<div class="lyrico-lyrics-wrapper">fun fun fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fun fun fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Science is fun so easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science is fun so easy"/>
</div>
<div class="lyrico-lyrics-wrapper">Science is fun so easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science is fun so easy"/>
</div>
<div class="lyrico-lyrics-wrapper">Very easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very easy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Science science
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science science"/>
</div>
<div class="lyrico-lyrics-wrapper">Science science
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science science"/>
</div>
<div class="lyrico-lyrics-wrapper">Science is so much fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science is so much fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Experimentation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Experimentation"/>
</div>
<div class="lyrico-lyrics-wrapper">Gives you motivation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gives you motivation"/>
</div>
<div class="lyrico-lyrics-wrapper">Add some extra passion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Add some extra passion"/>
</div>
<div class="lyrico-lyrics-wrapper">Brings in transformation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brings in transformation"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Scientist in the making
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scientist in the making"/>
</div>
<div class="lyrico-lyrics-wrapper">Play and learn with him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play and learn with him"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on come on here we goHey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on here we goHey"/>
</div>
<div class="lyrico-lyrics-wrapper">bring it up O boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bring it up O boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Time for exploration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time for exploration"/>
</div>
<div class="lyrico-lyrics-wrapper">Some vacuum some pressure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Some vacuum some pressure"/>
</div>
<div class="lyrico-lyrics-wrapper">Some ideas to treasure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Some ideas to treasure"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Light travels faster than sound
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light travels faster than sound"/>
</div>
<div class="lyrico-lyrics-wrapper">E = MC square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E = MC square"/>
</div>
<div class="lyrico-lyrics-wrapper">A religion that’s 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A religion that’s "/>
</div>
<div class="lyrico-lyrics-wrapper">universally bound
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="universally bound"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Science is fun so easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science is fun so easy"/>
</div>
<div class="lyrico-lyrics-wrapper">Science is fun so easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science is fun so easy"/>
</div>
<div class="lyrico-lyrics-wrapper">Very easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very easy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Science science
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science science"/>
</div>
<div class="lyrico-lyrics-wrapper">Science science
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science science"/>
</div>
<div class="lyrico-lyrics-wrapper">Science is so much fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science is so much fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laws And theories
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laws And theories"/>
</div>
<div class="lyrico-lyrics-wrapper">Makes up some density
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makes up some density"/>
</div>
<div class="lyrico-lyrics-wrapper">Physics and chemistry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Physics and chemistry"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake hands for gravity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake hands for gravity"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah! That’s science
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah! That’s science"/>
</div>
</pre>
