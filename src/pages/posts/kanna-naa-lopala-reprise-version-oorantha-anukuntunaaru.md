---
title: "kanna naa lopala reprise version song lyrics"
album: "Oorantha Anukuntunnaru"
artist: "	KM Radha Krishnan"
lyricist: "Vanamali"
director: "Balaji Sanala"
path: "/albums/oorantha-anukuntunnaru-lyrics"
song: "Kanna Naa Lopala Reprise Version"
image: ../../images/albumart/oorantha-anukuntunaaru.jpg
date: 2019-10-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dFh0NZtPgVE"
type: "love"
singers:
  - Raashi Khanna
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannaa Naa Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Naa Lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Naa Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Naa Lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Eenaadilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Eenaadilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhalona Needhe Gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalona Needhe Gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuraithe Yedhoo Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuraithe Yedhoo Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Naalo Kalipesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Naalo Kalipesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvellaa Needhe Swapnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvellaa Needhe Swapnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Naa Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Naa Lopala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasuna Unnadhi Neeve Ayinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuna Unnadhi Neeve Ayinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polchane Ledhu Okasaarainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polchane Ledhu Okasaarainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raamuni Seethala Yedhute Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamuni Seethala Yedhute Unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuwuna Dhaati Thanale Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuwuna Dhaati Thanale Praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaka Neetho Nadiche Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaka Neetho Nadiche Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Naa Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Naa Lopala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Eenaadilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Eenaadilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalona Needhe Gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalona Needhe Gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuraithe Yedhoo Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuraithe Yedhoo Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Naalo Kalipesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Naalo Kalipesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvellaa Needhe Swapnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvellaa Needhe Swapnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Naa Lopala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Naa Lopala "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adugula Vembadi Adugesthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugula Vembadi Adugesthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagane Ledhu Nanu Nenainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagane Ledhu Nanu Nenainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Jatha Neevani Kalalonainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Jatha Neevani Kalalonainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvuri Dhooram Kariginchelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvuri Dhooram Kariginchelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Antuku Thirige Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Antuku Thirige Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Naa Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Naa Lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Eenaadilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Eenaadilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhalona Needhe Gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalona Needhe Gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuraithe Yedhoo Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuraithe Yedhoo Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Naalo Kalipesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Naalo Kalipesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvellaa Needhe Swapnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvellaa Needhe Swapnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa  Naa Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa  Naa Lopala"/>
</div>
</pre>
