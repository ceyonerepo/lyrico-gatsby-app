---
title: "viliyal pesum kiliye song lyrics"
album: "Pen Vilai Verum 999 Rubai Mattume"
artist: "Judah Sandhy"
lyricist: "Jawar Raj"
director: "Varadha Raj G"
path: "/albums/pen-vilai-verum-999-rubai-mattume-lyrics"
song: "Viliyal Pesum Kiliye"
image: ../../images/albumart/pen-vilai-verum-999-rubai-mattume.jpg
date: 2022-01-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jPbRASSlheU"
type: "love"
singers:
  - Tony Thamos
  - Shreya Sundar Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">viliyaal pesum kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyaal pesum kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai thaluvum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai thaluvum paniye"/>
</div>
<div class="lyrico-lyrics-wrapper">viliyaal pesum kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyaal pesum kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai thaluvum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai thaluvum paniye"/>
</div>
<div class="lyrico-lyrics-wrapper">un alagai kollai kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un alagai kollai kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum ingu kalvan aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum ingu kalvan aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">en viliyaal unnai paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en viliyaal unnai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum indru aengi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum indru aengi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viliyaal pesum kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyaal pesum kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai thaluvum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai thaluvum paniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poomalaiyil naan nanaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poomalaiyil naan nanaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">un arugil irukaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un arugil irukaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">un swasam en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un swasam en "/>
</div>
<div class="lyrico-lyrics-wrapper">penmai thoonduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penmai thoonduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en ragasiya sinegithiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ragasiya sinegithiye"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyiril kalanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyiril kalanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">un arugil naan irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un arugil naan irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnaiyum thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnaiyum thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thenai parugum theniyai pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenai parugum theniyai pol "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai paruga thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai paruga thudithen"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalelu thandi varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalelu thandi varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">un kadhalai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kadhalai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">endro venvel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endro venvel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viliyaal pesum kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyaal pesum kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai thaluvum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai thaluvum paniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tamilil eluthukal noora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamilil eluthukal noora"/>
</div>
<div class="lyrico-lyrics-wrapper">naan pesa vanthen joraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan pesa vanthen joraa"/>
</div>
<div class="lyrico-lyrics-wrapper">adi unnai kanden nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unnai kanden nera"/>
</div>
<div class="lyrico-lyrics-wrapper">tholanthenee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholanthenee"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavilum ninaivilum neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavilum ninaivilum neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai niraithu kondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai niraithu kondu "/>
</div>
<div class="lyrico-lyrics-wrapper">irupathu naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupathu naane"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalilum iravilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalilum iravilum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than varuvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than varuvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megangal karaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megangal karaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayangal maraigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayangal maraigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aiaiyayo enakenna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiaiyayo enakenna "/>
</div>
<div class="lyrico-lyrics-wrapper">aachu puriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aachu puriyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">valaiyin viluntha vinmeenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyin viluntha vinmeenai"/>
</div>
<div class="lyrico-lyrics-wrapper">pol un kadhalil vilunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol un kadhalil vilunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">en viligal unnai paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en viligal unnai paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum indru aengi ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum indru aengi ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viliyaal pesum kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyaal pesum kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai thaluvum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai thaluvum paniye"/>
</div>
</pre>
