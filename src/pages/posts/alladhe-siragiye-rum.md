---
title: "alladhe siragiye song lyrics"
album: "Rum"
artist: "Anirudh Ravichander"
lyricist: "Vivek"
director: "Sai Bharath"
path: "/albums/rum-lyrics"
song: "Alladhe Siragiye"
image: ../../images/albumart/rum.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HY1gmSrixlM"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alladhe Siragiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alladhe Siragiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolladhe Kalavara Azhagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolladhe Kalavara Azhagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Polladha Asaivile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha Asaivile"/>
</div>
<div class="lyrico-lyrics-wrapper">Melladhe Thuru Thuru Kuruviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melladhe Thuru Thuru Kuruviye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unai Anaithu Uyir Paripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unai Anaithu Uyir Paripen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaneere Vendam Kaathu Nirpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaneere Vendam Kaathu Nirpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavayum Thaduthu Vazhi Maripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavayum Thaduthu Vazhi Maripen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kettaal Naan Enai Koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kettaal Naan Enai Koduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Itta Pimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Itta Pimbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhala Nilava Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhala Nilava Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann Thotta Kaiyil Oliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Thotta Kaiyil Oliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mouna Satham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mouna Satham "/>
</div>
<div class="lyrico-lyrics-wrapper">Asayaa Isayaa Ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asayaa Isayaa Ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Men Kokki Podum Visaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Men Kokki Podum Visaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Vaanavil Siripinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vaanavil Siripinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niram Piripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niram Piripen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kaagitha Idhazhgalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kaagitha Idhazhgalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagal Edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagal Edupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Nyabaga Kumizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Nyabaga Kumizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Adaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Adaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru Vaazhndhida Adhisaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendru Vaazhndhida Adhisaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Padaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Padaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Niram Piripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Niram Piripen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagal Edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagal Edupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Adaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Adaipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhisaya Idam Padaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisaya Idam Padaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Niram Piripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Niram Piripen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagal Edupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagal Edupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Adaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Adaipen"/>
</div>
</pre>
