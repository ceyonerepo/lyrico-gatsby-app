---
title: "naan yaar song lyrics"
album: "Pariyerum Perumal"
artist: "Santhosh Narayanan"
lyricist: "Mari Selvaraj"
director: "Mari Selvaraj"
path: "/albums/pariyerum-perumal-lyrics"
song: "Naan Yaar"
image: ../../images/albumart/pariyerum-perumal.jpg
date: 2018-09-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fxYdH58_0P0"
type: "mass"
singers:
  - Santhosh Narayanan
  - Vijay Narain
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaipadum kadhavukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaipadum kadhavukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaipadum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaipadum uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidigindra pozhuthilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigindra pozhuthilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Posungidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posungidum uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthidum paravaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthidum paravaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurathidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathidum uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothidum nilathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothidum nilathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasangidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasangidum uyir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peigindra mazhaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peigindra mazhaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Erinthidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erinthidum uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkindra manitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkindra manitharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruthidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruthidum uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkindra kadavulum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkindra kadavulum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthidum uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Varukindra saavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varukindra saavaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poruthidum uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruthidum uyir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rayil thedi vanthu kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayil thedi vanthu kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkum maramengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkum maramengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkil thongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkil thongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyil seththaMeenaai mithakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyil seththaMeenaai mithakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudusaikkul kathari erintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudusaikkul kathari erintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee eratha saamyingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee eratha saamyingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kai padamal thanneer parugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kai padamal thanneer parugum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor suvarkatti thooram vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor suvarkatti thooram vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Malakkuzhikkul moochaiyadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malakkuzhikkul moochaiyadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan yaaarrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaaarrrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaaaan yaaaarrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaaaan yaaaarrrr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arasan endru solvorumundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasan endru solvorumundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai endru ninaipporumundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai endru ninaipporumundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yer piditha vazhvum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yer piditha vazhvum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Por seitha kadhaiyum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por seitha kadhaiyum undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marithapin udal engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marithapin udal engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthaithapin neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthaithapin neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil neenthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil neenthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marithapin udal engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marithapin udal engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthaithapin neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthaithapin neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil neenthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil neenthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaaannnnyaaarrrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaaannnnyaaarrrrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Marithapin udal engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marithapin udal engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthaithapin neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthaithapin neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil neenthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil neenthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaaannnnyaaarrrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaaannnnyaaarrrrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Marithapin udal engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marithapin udal engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthaithapin neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthaithapin neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil neenthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil neenthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo hoo ooo ooo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo hoo ooo ooo hoo ooo"/>
</div>
</pre>
