---
title: "malaiyiru song lyrics"
album: "Mambattiyan"
artist: "S Thaman"
lyricist: "Na. Muthukumar"
director: "Thiagarajan"
path: "/albums/mambattiyan-lyrics"
song: "Malaiyiru"
image: ../../images/albumart/mambattiyan.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zrkVz1zN4bY"
type: "love"
singers:
  - S. Thaman
  - Srivardhini
  - Megha
  - Janani
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ada Maamoiye Ye Ye Ye Yee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maamoiye Ye Ye Ye Yee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dada Dada Dadadada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dada Dada Dadadada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambatiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambatiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dada Dada Dadadada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dada Dada Dadadada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambatiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambatiyan Ada Mambattiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyuru Nattaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyuru Nattaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Kaatu Poottaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Kaatu Poottaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Pola Yaarum Illa Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pola Yaarum Illa Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanjaavuru Raasatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjaavuru Raasatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaralama Thanthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaralama Thanthaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkulla Evannum Illa Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkulla Evannum Illa Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Minnala Pidikka Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Minnala Pidikka Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Valaiya Kondu Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Valaiya Kondu Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Meen Pudikka Maan Pudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Meen Pudikka Maan Pudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Illa Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Illa Podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vettai Yaada Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vettai Yaada Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vela Kondu Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vela Kondu Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Poo Parikka Thaen Edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Poo Parikka Thaen Edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhuthu Illa Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthu Illa Podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathellam Dhool Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathellam Dhool Parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikkum Kodi Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikkum Kodi Parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketavudan Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketavudan Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthavudan Padappadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthavudan Padappadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyuru Nattaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyuru Nattaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Kaatu Poottaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Kaatu Poottaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Pola Yaarum Illa Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pola Yaarum Illa Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanjaavuru Raasatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjaavuru Raasatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaralama Thanthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaralama Thanthaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkulla Evannum Illa Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkulla Evannum Illa Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Maamoiye Ye Ye Ye Yee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maamoiye Ye Ye Ye Yee"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadapuda Thadapuda Paarthanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadapuda Thadapuda Paarthanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala Thaedi Vandhenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala Thaedi Vandhenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Everywhere I Am Looking For You Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everywhere I Am Looking For You Maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripudan Siripudan Vandheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripudan Siripudan Vandheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu Pola Paartheenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu Pola Paartheenga"/>
</div>
<div class="lyrico-lyrics-wrapper">How Long Do I Waiting For You Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Long Do I Waiting For You Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaehey Hey Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaehey Hey Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaehey Hey Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaehey Hey Hey Hey Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panju Sela Ennaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju Sela Ennaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththa Vekka Vanthaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththa Vekka Vanthaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraiyumae Thoda Vidala Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraiyumae Thoda Vidala Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Innu Paarthenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Innu Paarthenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattiyama Thothaengaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattiyama Thothaengaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukullae Olichu Vaipen Vaangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullae Olichu Vaipen Vaangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kattu Aruvi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kattu Aruvi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaaranamaa Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaaranamaa Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Pattaam Poochi Pudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Pattaam Poochi Pudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka Aasai Illa Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka Aasai Illa Podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naattu Vediya Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naattu Vediya Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Visaiya Thoda Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Visaiya Thoda Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Veetukkulla Adangi Nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veetukkulla Adangi Nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappu Illa Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappu Illa Podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathellam Dhool Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathellam Dhool Parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikkum Kodi Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikkum Kodi Parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketavudan Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketavudan Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthavudan Padappadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthavudan Padappadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Maamoiye Ye Ye Ye Yee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Maamoiye Ye Ye Ye Yee"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaehey Hey Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaehey Hey Hey Hey Hey Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandaku Nataku Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandaku Nataku Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataku Nataku Nataku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataku Nataku Nataku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaehey Hey Hey Hey Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaehey Hey Hey Hey Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthalum Pulithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthalum Pulithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanjaalum Pulithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanjaalum Pulithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Virunthu Vaippen Maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virunthu Vaippen Maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeraadhi Veeran Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeraadhi Veeran Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraadhi Sooran Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraadhi Sooran Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakkum Mattum Kedaikannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakkum Mattum Kedaikannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Aiyanaara Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aiyanaara Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaavalukku Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaavalukku Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Neiya Ooththi Soru Thinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Neiya Ooththi Soru Thinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Illa Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Illa Podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Madura Veeran Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Madura Veeran Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kolgaiyoda Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kolgaiyoda Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kaadhalikka Kaipidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaadhalikka Kaipidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Illa Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Illa Podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathellam Dhool Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathellam Dhool Parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikkum Kodi Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikkum Kodi Parakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketavudan Kalakalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketavudan Kalakalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthavudan Padappadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthavudan Padappadakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mambattiyan Ada Mambattiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mambattiyan Ada Mambattiyan"/>
</div>
</pre>
