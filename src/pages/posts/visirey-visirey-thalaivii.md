---
title: "visirey visirey song lyrics"
album: "Thalaivii"
artist: "G.V. Prakash Kumar"
lyricist: "Kalyana Chakravarthy"
director: "A.L. Vijay"
path: "/albums/thalaivii-lyrics"
song: "Visirey Visirey"
image: ../../images/albumart/thalaivii.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gT0qqzRvtMc"
type: "melody"
singers:
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Visirey Visirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visirey Visirey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unidhi Gaalam Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unidhi Gaalam Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaka Niliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaka Niliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimisham Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimisham Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakavesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakavesham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaapakamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaapakamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avashesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avashesham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aashaapaasham Chirabankaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashaapaasham Chirabankaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulake Dhi Avakaasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulake Dhi Avakaasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulake Dhi Avakaasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulake Dhi Avakaasham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visirey Visirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visirey Visirey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhikaalam Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhikaalam Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhakaniliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhakaniliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kosari Kosari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosari Kosari"/>
</div>
<div class="lyrico-lyrics-wrapper">Marikancham Madhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marikancham Madhi"/>
</div>
</pre>
