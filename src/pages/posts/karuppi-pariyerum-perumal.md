---
title: "karuppi song lyrics"
album: "Pariyerum Perumal"
artist: "Santhosh Narayanan"
lyricist: "	Vivek - Mari Selvaraj"
director: "Mari Selvaraj"
path: "/albums/pariyerum-perumal-lyrics"
song: "Karuppi"
image: ../../images/albumart/pariyerum-perumal.jpg
date: 2018-09-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5IXdCWhQG78"
type: "sad"
singers:
  - Santhosh Narayanan
  - Dr SC Chandilya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pesuradhu kekkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pesuradhu kekkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">En azhugaiyum kekkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En azhugaiyum kekkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha rayilin adiyil sikki sedhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha rayilin adiyil sikki sedhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kathum valiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kathum valiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanaiyum kekkudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanaiyum kekkudha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippa udanae naan unna pakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa udanae naan unna pakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookkil mugam vachi orasanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookkil mugam vachi orasanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un naakkil nakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un naakkil nakki"/>
</div>
<div class="lyrico-lyrics-wrapper">En azhukka kazhuvi poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En azhukka kazhuvi poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vandha unna pakkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga vandha unna pakkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar andha kaattil odanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar andha kaattil odanji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedappadu neeya illa naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedappadu neeya illa naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana illa neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana illa neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeya naana naana neeya karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeya naana naana neeya karuppi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandhadhu neeya iruppadhu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandhadhu neeya iruppadhu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppadhu neeya irandhadhu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppadhu neeya irandhadhu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammala konnaven yaarunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammala konnaven yaarunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku nalla theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku nalla theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga seththadhu yaarunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga seththadhu yaarunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukku mattumdhan puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukku mattumdhan puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhinjadhu neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhinjadhu neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuvadhu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuvadhu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuvadhu neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuvadhu neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhinjadhu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhinjadhu naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhiri karuppi amma kooppudura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhiri karuppi amma kooppudura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikka poganum endhiridi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikka poganum endhiridi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendi pesala en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi pesala en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadi nadandhichi anga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi nadandhichi anga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valiya thaangaama thudichiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiya thaangaama thudichiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi nimisham enna nenaichiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi nimisham enna nenaichiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kollum podhu avan sirichana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kollum podhu avan sirichana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee koraikkumpodhu avan moraichana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee koraikkumpodhu avan moraichana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkitta padichi padichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkitta padichi padichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnanae kettiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnanae kettiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kooptu nee ponaYendi pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar kooptu nee ponaYendi pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana thadava solliyirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana thadava solliyirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam manusanum ingae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam manusanum ingae "/>
</div>
<div class="lyrico-lyrics-wrapper">onnu illaenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu illaenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valathu anaikkuravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valathu anaikkuravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhutha nerikkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha nerikkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna thadavuravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna thadavuravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala nodikkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala nodikkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnu sirikkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnu sirikkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu azhuguravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu azhuguravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevappan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevappan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram manusan undunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram manusan undunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku appovae sonnen kettiya nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku appovae sonnen kettiya nee"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa udanae naan unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa udanae naan unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkanum karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkanum karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalaikku vandhu appa kepparu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalaikku vandhu appa kepparu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennadi badhil solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennadi badhil solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna enganu solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna enganu solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi iruka nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi iruka nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhiri karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhiri karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhiri karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhiri karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhiri karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhiri karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhiri karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhiri karuppi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppi en karuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppi en karuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagathadamae en paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagathadamae en paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaadha kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaadha kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadithaan thiriveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadithaan thiriveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kaalilo naalu kaalilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kaalilo naalu kaalilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha mannilae ulavittu kedakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha mannilae ulavittu kedakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naai illadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naai illadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan illayaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan illayaa nee"/>
</div>
</pre>
