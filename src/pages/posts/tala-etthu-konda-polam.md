---
title: "tala etthu song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "Sirivennela Seetharama Sastry"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Tala Etthu"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/VSPWh4eiSVw"
type: "mass"
singers:
  - M. M. Keeravani
  - Harika Narayan
  - Sri Soumya Varanasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gira gira gira gira gira gira gira gira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira gira gira gira gira gira gira gira"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudigundam lagesthu unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudigundam lagesthu unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Bithuku bithukumanu oopiriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bithuku bithukumanu oopiriki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuku bathuku ani opika posthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku bathuku ani opika posthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkunu munche uppenavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkunu munche uppenavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Tala etthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Tala etthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Tala etthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Tala etthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray ray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray ray Ray ray rayyaray"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talavanchuku chusedhemiti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talavanchuku chusedhemiti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu kada therche mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu kada therche mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tala etthithe kanabaduthundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tala etthithe kanabaduthundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana dhaka rammanu ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana dhaka rammanu ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pada dhose sandhrapu neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada dhose sandhrapu neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Egadhose gaganapu neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egadhose gaganapu neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasindha egasindha ala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasindha egasindha ala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ala laantidhe kadha nee thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala laantidhe kadha nee thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala laantidhe kadha nee thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala laantidhe kadha nee thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala laantidhe kadha nee thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala laantidhe kadha nee thala"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Tala etthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Tala etthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Tala etthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Tala etthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padavaina anthapuramaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaina anthapuramaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Uniki koraku poratam thappadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uniki koraku poratam thappadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu cheyalsina pani chesey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu cheyalsina pani chesey"/>
</div>
<div class="lyrico-lyrics-wrapper">Em jarigina paravanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em jarigina paravanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaremaina anukoniy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaremaina anukoniy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo ninne nuvve chusthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo ninne nuvve chusthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bittharapadi garvapadela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bittharapadi garvapadela"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Tala etthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Tala etthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu Tala etthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu Tala etthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray rayyaray"/>
</div>
<div class="lyrico-lyrics-wrapper">Ray ray ray Ray ray rayyaray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ray ray ray Ray ray rayyaray"/>
</div>
</pre>
