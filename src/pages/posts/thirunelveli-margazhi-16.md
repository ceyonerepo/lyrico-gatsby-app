---
title: "thirunelveli song lyrics"
album: "Margazhi 16"
artist: "E.K. Bobby"
lyricist: "Priyan"
director: "K. Stephen"
path: "/albums/margazhi-16-lyrics"
song: "Thirunelveli"
image: ../../images/albumart/margazhi-16.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aL-qdjbSqZM"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thirunelveli pakkathula thoothukkudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunelveli pakkathula thoothukkudidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhool kelappu aattam poada naanga redidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhool kelappu aattam poada naanga redidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaichadhellaam anubavikka thedhi idhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaichadhellaam anubavikka thedhi idhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaikkaadha ellaathaiyum thedippudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaikkaadha ellaathaiyum thedippudidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhasaiyellaam Orangattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhasaiyellaam Orangattu "/>
</div>
<div class="lyrico-lyrics-wrapper">pudhusakkooda saeththikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhusakkooda saeththikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">aeththu aeththu summa konjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeththu aeththu summa konjam "/>
</div>
<div class="lyrico-lyrics-wrapper">sudhiya aeththudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudhiya aeththudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">varushaththoada kadaisinaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varushaththoada kadaisinaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">New Year-rukku modha naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="New Year-rukku modha naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">thambikku maargazhi padhinaarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thambikku maargazhi padhinaarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirunelveli pakkathula thoothukkudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunelveli pakkathula thoothukkudidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhool kelappu aattam poada naanga redidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhool kelappu aattam poada naanga redidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaichadhellaam anubavikka thedhi idhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaichadhellaam anubavikka thedhi idhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaikkaadha ellaathaiyum thedippudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaikkaadha ellaathaiyum thedippudidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumbu meesa murukki vachchi kurumbukkaattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbu meesa murukki vachchi kurumbukkaattuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">varambu meeri nenaichadhellaam nadaththikkaattuvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varambu meeri nenaichadhellaam nadaththikkaattuvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">poanadhellaam poayeppoachu thookkippodudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poanadhellaam poayeppoachu thookkippodudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyichuppaarkka irukku ini nalla naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyichuppaarkka irukku ini nalla naaludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">koottaaliyoada serndhu ada ellaiththaadumvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottaaliyoada serndhu ada ellaiththaadumvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">adhiradiyaai iruppadhuthaan engappazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhiradiyaai iruppadhuthaan engappazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu vidhamaa jeyippadhuthaan engappazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu vidhamaa jeyippadhuthaan engappazhakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirunelveli pakkathula thoothukkudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunelveli pakkathula thoothukkudidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhool kelappu aattam poada naanga redidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhool kelappu aattam poada naanga redidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaichadhellaam anubavikka thedhi idhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaichadhellaam anubavikka thedhi idhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaikkaadha ellaathaiyum thedippudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaikkaadha ellaathaiyum thedippudidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello ippudikkooththadichi aadunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello ippudikkooththadichi aadunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naangellaam eppa thoonguradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangellaam eppa thoonguradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">engappaa vera Heart Petient
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engappaa vera Heart Petient"/>
</div>
<div class="lyrico-lyrics-wrapper">avaru eppadi resteduppaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaru eppadi resteduppaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei idhu enga theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei idhu enga theru"/>
</div>
<div class="lyrico-lyrics-wrapper">varushaa varusham New Year annikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varushaa varusham New Year annikku"/>
</div>
<div class="lyrico-lyrics-wrapper">ippudithaan paattappottu aaduvoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippudithaan paattappottu aaduvoam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pudhusaa vandhuttu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pudhusaa vandhuttu "/>
</div>
<div class="lyrico-lyrics-wrapper">paatta poadakkoodaadhunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatta poadakkoodaadhunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam poadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam poadura"/>
</div>
<div class="lyrico-lyrics-wrapper">onga appaa Heart Petient-nnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onga appaa Heart Petient-nnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hospital-la poai padukka chollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hospital-la poai padukka chollu"/>
</div>
<div class="lyrico-lyrics-wrapper">aei neeppaatta poadraa maappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aei neeppaatta poadraa maappulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engitta moadhaadhey naan Rajadhi Rajanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engitta moadhaadhey naan Rajadhi Rajanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vambukku izhukkaadhey naan veeraadhi veeranadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambukku izhukkaadhey naan veeraadhi veeranadi"/>
</div>
<div class="lyrico-lyrics-wrapper">adi venaandi enna seendippaarkkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi venaandi enna seendippaarkkadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">un thimiruthanam inga onnum palikkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thimiruthanam inga onnum palikkaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ae poadhundi summaa vaalaattaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ae poadhundi summaa vaalaattaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">theekkullathaan veenakkaiya neettaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theekkullathaan veenakkaiya neettaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engitta moadhaadhey naan Rajadhi Rajanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engitta moadhaadhey naan Rajadhi Rajanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vambukku izhukkaadhey naan veeraadhi veeranadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambukku izhukkaadhey naan veeraadhi veeranadi"/>
</div>
<div class="lyrico-lyrics-wrapper">adi venaandi enna seendippaarkkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi venaandi enna seendippaarkkadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">un thimiruthanam inga onnum palikkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thimiruthanam inga onnum palikkaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ae poadhundi summaa vaalaattaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ae poadhundi summaa vaalaattaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">theekkullathaan veenakkaiya neettaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theekkullathaan veenakkaiya neettaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirunelveli pakkathula thoothukkudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunelveli pakkathula thoothukkudidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhool kelappu aattam poada naanga redidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhool kelappu aattam poada naanga redidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaichadhellaam anubavikka thedhi idhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaichadhellaam anubavikka thedhi idhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaikkaadha ellaathaiyum thedippudidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaikkaadha ellaathaiyum thedippudidaa"/>
</div>
</pre>
