---
title: "prema tholiga thelisenu song lyrics"
album: "Guvva Gorinka"
artist: "Bobbili Suresh"
lyricist: "Krishna Kanth"
director: "Mohan Bammidi"
path: "/albums/guvva-gorinka-lyrics"
song: "Prema Tholiga Thelisenu"
image: ../../images/albumart/guvva-gorinka.jpg
date: 2020-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-Nh80392j0U"
type: "love"
singers:
  - Sai Charan
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Prema Tholigaa Thelisenu Nede Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Tholigaa Thelisenu Nede Neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kadhalaka Nilichenu Ninne Choosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kadhalaka Nilichenu Ninne Choosindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Vinabadi Edhalo Jorabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Vinabadi Edhalo Jorabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagina Aashane Kadhipenu Kadhipenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagina Aashane Kadhipenu Kadhipenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Kanapadi Adugula Alajadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Kanapadi Adugula Alajadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholigaa Mouname Palikenu Palikenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholigaa Mouname Palikenu Palikenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Ontarine Naa Lokamidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Ontarine Naa Lokamidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manasuke Thaakina Varamu Nuvvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasuke Thaakina Varamu Nuvvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Tholigaa Thelisenu Nede Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Tholigaa Thelisenu Nede Neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kadhalaka Nilichenu Ninne Choosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kadhalaka Nilichenu Ninne Choosindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okatai Nadiche Oka Dhaate Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatai Nadiche Oka Dhaate Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugai Piliche Gamyam Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugai Piliche Gamyam Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige Dhooram Mana Iruvuri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige Dhooram Mana Iruvuri "/>
</div>
<div class="lyrico-lyrics-wrapper">Madhya Chanuvika Perige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhya Chanuvika Perige"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate Praanam Iru Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate Praanam Iru Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Raayani Kavithe Manasuni Kudhipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raayani Kavithe Manasuni Kudhipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhikidhe Marojanma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhikidhe Marojanma"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodani Chelime Gathamunu Cheripe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodani Chelime Gathamunu Cheripe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyadhe Idhe Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyadhe Idhe Premaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Ontarine Naa Lokamidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Ontarine Naa Lokamidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manasuke Thaakina Varamu Nuvvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasuke Thaakina Varamu Nuvvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Tholigaa Thelisenu Nede Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Tholigaa Thelisenu Nede Neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kadhalaka Nilichenu Ninne Choosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kadhalaka Nilichenu Ninne Choosindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidigaa Virise Iru Aashala Oose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigaa Virise Iru Aashala Oose"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatai Choope Jathagaane Nilipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatai Choope Jathagaane Nilipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule Bhaaram Mana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule Bhaaram Mana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaburula Baruvu Kalalalo Mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaburula Baruvu Kalalalo Mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhure Neram Ayye Velallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhure Neram Ayye Velallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaagani Dhigule Chedaraka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagani Dhigule Chedaraka "/>
</div>
<div class="lyrico-lyrics-wrapper">Madhike Edhurayyedhepudammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhike Edhurayyedhepudammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Opani Veluge Kanulaki Thagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opani Veluge Kanulaki Thagile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamukai Veche Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamukai Veche Unnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Ontarine Naa Lokamidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Ontarine Naa Lokamidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manasuke Thaakina Varamu Nuvvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasuke Thaakina Varamu Nuvvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Tholigaa Thelisenu Nede Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Tholigaa Thelisenu Nede Neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kadhalaka Nilichenu Ninne Choosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kadhalaka Nilichenu Ninne Choosindhe"/>
</div>
</pre>
