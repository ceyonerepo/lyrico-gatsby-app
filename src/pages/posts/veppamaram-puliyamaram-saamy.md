---
title: "veppamaram puliyamaram song lyrics"
album: "Saamy"
artist: "Harris Jayaraj "
lyricist: "Na. Muthukumar"
director: "Hari"
path: "/albums/saamy-song-lyrics"
song: "Veppamaram Puliyamaram"
image: ../../images/albumart/saamy.jpg
date: 2003-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oDPOmEgX5TI"
type: "Mass"
singers:
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veppamaram Puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppamaram Puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalamaram Arasamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalamaram Arasamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporen Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporen Kettuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathangarai Theppakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathangarai Theppakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkavarum Sengamalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkavarum Sengamalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporen Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporen Kettuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panchaalai Sangu Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchaalai Sangu Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaadha Dhooram Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadha Dhooram Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorai Chuthum Kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai Chuthum Kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaadha Dhooram Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaadha Dhooram Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakki Sattai Pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki Sattai Pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaporen Ooraivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaporen Ooraivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakki Sattai Pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki Sattai Pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaporaan Ooraivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaporaan Ooraivittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppamaram Puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppamaram Puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalamaram Arasamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalamaram Arasamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporen Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporen Kettuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Aathangarai Theppakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aathangarai Theppakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkavarum Sengamalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkavarum Sengamalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporen Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporen Kettuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillunu Kaalaiyil Ezhundhiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillunu Kaalaiyil Ezhundhiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Silamba Kaathula Sulattuvenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silamba Kaathula Sulattuvenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum Silambu Pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum Silambu Pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Kaayam Patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kaayam Patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Porandha Manneduthu Poosuvenunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Porandha Manneduthu Poosuvenunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pala Naal Aasai Nanavaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pala Naal Aasai Nanavaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethavaru Manasu Kulirndhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethavaru Manasu Kulirndhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet Vandi Mela Rocket Vegathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet Vandi Mela Rocket Vegathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhava Seerikittu Pogaporenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhava Seerikittu Pogaporenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarusaamy Bavani Saalaiyila Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarusaamy Bavani Saalaiyila Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannaiyaaru Podum Mel Thundu Irangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaiyaaru Podum Mel Thundu Irangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Color Color Thavaniya Paarthutta Podhumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color Color Thavaniya Paarthutta Podhumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamyoda Bavani Kaiyakatti Nirkkumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamyoda Bavani Kaiyakatti Nirkkumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppamaram Puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppamaram Puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hei Hei Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalamaram Arasamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalamaram Arasamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hei Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppamaram Puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppamaram Puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalamaram Arasamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalamaram Arasamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporen Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporen Kettuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Aathangarai Theppakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aathangarai Theppakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkavarum Sengamalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkavarum Sengamalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporen Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporen Kettuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Raasa Raasa Raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Raasa Raasa Raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Thene Thene Thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thene Thene Thene"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nee Pora Raasa Raasa Raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nee Pora Raasa Raasa Raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Thene Thene Thene Thene Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thene Thene Thene Thene Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayile Ayile Aiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayile Ayile Aiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Nottunga Adikkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Nottunga Adikkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandhu Vatti Vaangi Soranduravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhu Vatti Vaangi Soranduravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi Thiruduravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi Thiruduravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattai Amukkuravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattai Amukkuravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru Maasa Timekkulla Thirundhidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Maasa Timekkulla Thirundhidunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaippaatti Vaicha Odhaippenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaippaatti Vaicha Odhaippenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethavala Thittuna Midhippenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethavala Thittuna Midhippenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli Gilli Thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli Gilli Thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu Pasanga Kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu Pasanga Kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottathil Kondaatama Serndhukkuvenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottathil Kondaatama Serndhukkuvenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakkaru Kuthura Bambarathai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakkaru Kuthura Bambarathai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Neeyum Senja Thalaiyila Kuttuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Neeyum Senja Thalaiyila Kuttuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttu Patta Neeyum Kutham Unarndhuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttu Patta Neeyum Kutham Unarndhuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama Enakku Veronnum Venaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Enakku Veronnum Venaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppamaram Puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppamaram Puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalamaram Arasamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalamaram Arasamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporaan Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporaan Kettuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Aathangarai Theppakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aathangarai Theppakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkavarum Sengamalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkavarum Sengamalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vittu Pogaporaan Kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vittu Pogaporaan Kettuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panchaalai Sangu Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchaalai Sangu Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaadha Dhooram Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaadha Dhooram Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorai Chuthum Kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai Chuthum Kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaadha Dhooram Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaadha Dhooram Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakki Sattai Pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki Sattai Pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaporen Ooraivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaporen Ooraivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaai Kaakki Sattai Pottukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaai Kaakki Sattai Pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaporaan Ooraivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaporaan Ooraivittu"/>
</div>
</pre>
