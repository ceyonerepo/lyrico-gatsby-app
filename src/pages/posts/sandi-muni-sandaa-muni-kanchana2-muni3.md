---
title: "sandi muni sandaa muni song lyrics"
album: "Kanchana2-Muni3"
artist: "S. Thaman - Leon James - C. Sathya"
lyricist: "Viveka"
director: "Raghava Lawrence"
path: "/albums/kanchana2-muni3-lyrics"
song: "Sandi Muni Sandaa Muni"
image: ../../images/albumart/kanchana2-muni3.jpg
date: 2015-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HhOjkhfVU0o"
type: "Mass"
singers:
  - Haricharan
  - Leon James
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandimuni Sadaamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandimuni Sadaamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandimuni Mahaamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandimuni Mahaamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandimuni Vaaidhaamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandimuni Vaaidhaamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paandimuni Paravamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paandimuni Paravamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suruttumuni Sudalamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suruttumuni Sudalamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttumuni Ilaiyamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttumuni Ilaiyamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattumun Karuppumuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattumun Karuppumuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottamuni Moothamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottamuni Moothamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchimuni Ondimuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchimuni Ondimuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadaamuni Maayamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadaamuni Maayamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedhamuni Veeramuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhamuni Veeramuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadhamuni Nallamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhamuni Nallamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solra Muni Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solra Muni Pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottuppaaru Muniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottuppaaru Muniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhirukken Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukken Thaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiramum Thandhiramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiramum Thandhiramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Paniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Paniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetta Vetta Thuluppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Vetta Thuluppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittamittu Ozhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittamittu Ozhippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panni Vechcha Paavathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Vechcha Paavathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadi Pottu Kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Pottu Kuduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Thodu Inga Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Thodu Inga Thodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Thaangamaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thaangamaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vandhu Thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vandhu Thadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Thadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kakkuradhum Munidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkuradhum Munidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkuradhum Munidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkuradhum Munidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannedhirey Saagasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannedhirey Saagasangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiyuradhum Munidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyuradhum Munidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eettiyena Veralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eettiyena Veralum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idipola Kuralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idipola Kuralum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Idhu Enna Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Idhu Enna Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Dhesa Meralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Dhesa Meralum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungappan Un Paataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungappan Un Paataan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaakooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaakooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moochu Thappaathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochu Thappaathadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhi Vaangathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi Vaangathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bali Vaangathaan Vandhenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Vaangathaan Vandhenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhaththaan Vandhomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaththaan Vandhomey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhakkaiyai Pottu Erichiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhakkaiyai Pottu Erichiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannaththu Poochiyin Kootaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannaththu Poochiyin Kootaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konnu Kuvichittiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnu Kuvichittiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatteri Nadamaadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatteri Nadamaadudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Nari Kootthaadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Nari Kootthaadudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aniyayam Vilaiyaadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aniyayam Vilaiyaadudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhukkelaam Kalamaadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkelaam Kalamaadudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andam Adhil Kandam Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Adhil Kandam Pala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandam Adhil Pindam Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandam Adhil Pindam Pala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaamale Vanjam Theer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamale Vanjam Theer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Kaathum Neruppum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Kaathum Neruppum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalandha Uravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandha Uravum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthaal Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Urugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Urugum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhi Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi Modhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusaai Azhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusaai Azhippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaadaa Mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadaa Mooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottuppaaru Muniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottuppaaru Muniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhirukken Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukken Thaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiramum Thandhiramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiramum Thandhiramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Paniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Paniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetta Vetta Thuluppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Vetta Thuluppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittamittu Ozhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittamittu Ozhippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panni Vecha Paavathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Vecha Paavathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaddi Pottu Kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddi Pottu Kuduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Thodu Inga Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Thodu Inga Thodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Thaangamaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thaangamaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vandhu Thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vandhu Thadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Thadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollama Vidamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollama Vidamaatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konniye Enga Kolathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konniye Enga Kolathiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illama Seidhaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illama Seidhaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Ellaam Parichiyedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Ellaam Parichiyedaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaandaamani Kinukinukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaandaamani Kinukinukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadam Soorai Para Parakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadam Soorai Para Parakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mala Kooda Gidu Gidukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala Kooda Gidu Gidukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhayaanai Thada Thadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhayaanai Thada Thadakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai Onnu Kilai Virikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Onnu Kilai Virikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivagaaram Theepudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivagaaram Theepudikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnagamum Veda Vedakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnagamum Veda Vedakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara Hara Hara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Ulagili Ulla Theemaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Ulagili Ulla Theemaikkellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Saavey Paadam Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Saavey Paadam Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urutti Urutti Poratti Poratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutti Urutti Poratti Poratti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolvene Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolvene Vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solra Muni Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solra Muni Pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottuppaaru Muniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottuppaaru Muniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhirukken Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukken Thaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiramum Thandhiramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiramum Thandhiramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaladiyil Paniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaladiyil Paniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetta Vetta Thuluppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Vetta Thuluppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittamittu Ozhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittamittu Ozhippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panni Vechcha Paavathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Vechcha Paavathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadi Pottu Kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi Pottu Kuduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Thodu Inga Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Thodu Inga Thodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Thaangamaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thaangamaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vandhu Thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vandhu Thadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Thadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Thadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kakkuradhum Munidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkuradhum Munidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkuradhum Munidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkuradhum Munidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannedhirey Saagasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannedhirey Saagasangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiyuradhum Munidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyuradhum Munidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eettiyena Veralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eettiyena Veralum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idipola Kuralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idipola Kuralum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Idhu Enna Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Idhu Enna Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Dhesa Meralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Dhesa Meralum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungappan Un Paataan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungappan Un Paataan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaakooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaakooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moochu Thappaathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochu Thappaathadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhi Vaangathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi Vaangathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bali Vaangathaan Vandhenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Vaangathaan Vandhenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandimuni Sadaamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandimuni Sadaamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandimuni Mahaamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandimuni Mahaamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandimuni Vaaidhamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandimuni Vaaidhamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paandimuni Paravamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paandimuni Paravamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suruttumuni Sudalamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suruttumuni Sudalamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttumuni Ilaiyamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttumuni Ilaiyamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattumun Karuppumuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattumun Karuppumuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottamuni Moothamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottamuni Moothamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchimuni Ondimuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchimuni Ondimuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadaamuni Maayamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadaamuni Maayamuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedhamuni Veeramuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhamuni Veeramuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadhamuni Nallamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhamuni Nallamuni"/>
</div>
</pre>
