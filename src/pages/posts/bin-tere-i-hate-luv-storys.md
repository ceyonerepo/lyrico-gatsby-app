---
title: "bin tere song lyrics"
album: "I Hate Luv Storys"
artist: "Vishal Shekhar"
lyricist: "Vishal Dadlani"
director: "Punit Malhotra"
path: "/albums/i-hate-luv-storys-lyrics"
song: "Bin Tere"
image: ../../images/albumart/i-hate-luv-storys.jpg
date: 2010-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/7Eaa-Zg2tYM"
type: "love"
singers:
  - Shafqat Amanat Ali
  - Sunidhi Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lusty lonely, coz you' re the only
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely, coz you' re the only"/>
</div>
<div class="lyrico-lyrics-wrapper">One that knows me and I can' t be without you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One that knows me and I can' t be without you"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely, coz you' re the only
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely, coz you' re the only"/>
</div>
<div class="lyrico-lyrics-wrapper">One that knows me and I can' t be without you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One that knows me and I can' t be without you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai kya yeh jo tere mere darmiyan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai kya yeh jo tere mere darmiyan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">An-dekhi ansuni koi dastaan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="An-dekhi ansuni koi dastaan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai kya yeh jo tere mere darmiyan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai kya yeh jo tere mere darmiyan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">An-dekhi ansuni koi dastaan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="An-dekhi ansuni koi dastaan hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lagne lagi ab zindagi khaali hai meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagne lagi ab zindagi khaali hai meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagne lagi har saans bhi khaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagne lagi har saans bhi khaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin tere, bin tere, bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere, bin tere, bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin tere, bin tere, bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere, bin tere, bin tere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ajnabi se hue hain kyun pal saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajnabi se hue hain kyun pal saare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke nazar se nazar yeh milaate hi nahi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke nazar se nazar yeh milaate hi nahi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ik ghani tanhayi chha rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik ghani tanhayi chha rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manzilein raaston mein hi ghum hone lagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manzilein raaston mein hi ghum hone lagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho gayi ansuni har dua ab meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho gayi ansuni har dua ab meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Reh gayi ankahi bin tere..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reh gayi ankahi bin tere.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin tere, bin tere, bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere, bin tere, bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin tere, bin tere, bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere, bin tere, bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raah mein roshni ne hai kyun hath chhoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raah mein roshni ne hai kyun hath chhoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Is taraf shaam ne kyun hai apna muh moda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is taraf shaam ne kyun hai apna muh moda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yun ke har subah, ik be-reham si raat ban gayi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yun ke har subah, ik be-reham si raat ban gayi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Hai kya yeh jo tere mere darmiyaan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai kya yeh jo tere mere darmiyaan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Andekhi ansuni koi dastaan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andekhi ansuni koi dastaan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagne lagi ab zindagi khaali khaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagne lagi ab zindagi khaali khaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagne lagi har saans bhi khaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagne lagi har saans bhi khaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin tere, bin tere, bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere, bin tere, bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Bin tere, bin tere, bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bin tere, bin tere, bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lusty lonely, lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely, lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Lusty lonely, lusty lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lusty lonely, lusty lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi khalish hai hawaon mein bin tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi khalish hai hawaon mein bin tere"/>
</div>
</pre>
