---
title: 'marappadhilai nenje song lyrics'
album: 'Oh My Kadavule'
artist: 'Leon James'
lyricist: 'Ko Sesha'
director: 'Ashwath Marimuthu'
path: '/albums/oh-my-kadavule-song-lyrics'
song: 'Marappadhilai Nenje'
image: ../../images/albumart/oh-my-kadavule.jpg
date: 2020-02-14
lang: tamil
singers: 
- Sudharshan Ashok
youtubeLink: 'https://www.youtube.com/embed/W6XAQBNmICI'
type: 'sad'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Mozhiyilai mozhiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Mozhiyilai mozhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per sollaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un per sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilai vizhiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhiyilai vizhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam paarkaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un mugam paarkaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirinil unaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirinil unaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan puthaithen indrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan puthaithen indrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinthidum munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Purinthidum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pirinthen anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai pirinthen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhinamum kanavil
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai tholaivil kaangiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai tholaivil kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanal iravai
<input type="checkbox" class="lyrico-select-lyric-line" value="Athanal iravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan neela ketkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan neela ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhuthu pizhaiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhuthu pizhaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kavithai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kavithai aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkae edhiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkae edhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayam aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="En idhayam aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marappadhilai nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marappadhilai nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjil innum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjil innum neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marappadhilai nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marappadhilai nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjil innum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjil innum neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo oo hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo oo hoo oo oo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo oo hoo oo oo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo oo hoo oo oo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo oo hoo oo oo hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mozhiyilai mozhiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Mozhiyilai mozhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per sollaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un per sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilai vizhiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhiyilai vizhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam paarkaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un mugam paarkaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirinil unaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirinil unaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan puthaithen indrae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan puthaithen indrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinthidum munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Purinthidum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pirinthen anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai pirinthen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhinamum kanavil
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai tholaivil kaangiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai tholaivil kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanal iravai
<input type="checkbox" class="lyrico-select-lyric-line" value="Athanal iravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan neela ketkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan neela ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhuthu pizhaiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhuthu pizhaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kavithai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kavithai aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkae edhiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakkae edhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayam aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="En idhayam aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marappadhilai nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marappadhilai nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjil innum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjil innum neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marappadhilai nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marappadhilai nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nenjil innum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh nenjil innum neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo oo hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo oo hoo oo oo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo oo hoo oo oo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo oo hoo oo oo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo oo hoo oo oo hoo ooo"/>
</div>
</pre>