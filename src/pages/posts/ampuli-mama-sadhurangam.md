---
title: "ampuli mama song lyrics"
album: "Sadhurangam"
artist: "Vidyasagar"
lyricist: "Yugabharathi"
director: "Karu Pazhaniappan"
path: "/albums/sadhurangam-lyrics"
song: "Ampuli Mama"
image: ../../images/albumart/sadhurangam.jpg
date: 2011-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gS0ZaKmpOKs"
type: "love"
singers:
  - KK
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ambuli maama ambuli maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambuli maama ambuli maama"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai theera muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai theera muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye kannitheevey kannitheevey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye kannitheevey kannitheevey"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithaai poale muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithaai poale muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithazhoadu ithazhai saerthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithazhoadu ithazhai saerthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en koode nilappethenne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en koode nilappethenne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viraloadu viralai saerthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraloadu viralai saerthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilayaada azhaippathenne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilayaada azhaippathenne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muththangal tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththangal tha tha tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ambuli maama ambuli maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambuli maama ambuli maama"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai theera muththamidu muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai theera muththamidu muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye kannitheevey kannitheevey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye kannitheevey kannitheevey"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithaai poale muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithaai poale muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannakadhappile pinna kazhuthile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakadhappile pinna kazhuthile"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayam aadiduvoama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayam aadiduvoama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchchanthalayile pachchai narambile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchchanthalayile pachchai narambile"/>
</div>
<div class="lyrico-lyrics-wrapper">koalam aaghiduvoama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koalam aaghiduvoama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye kenjum udhattile konjum iduppile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye kenjum udhattile konjum iduppile"/>
</div>
<div class="lyrico-lyrics-wrapper">meesai keeralaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesai keeralaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mundhu sarivile undhu chuliyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundhu sarivile undhu chuliyile"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam aaghidalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam aaghidalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kai raeghai idam maara inaithaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai raeghai idam maara inaithaal enne"/>
</div>
<div class="lyrico-lyrics-wrapper">kan paarvai thadam maara rasithaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan paarvai thadam maara rasithaal enne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iravellaam ennoadu vidinthaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravellaam ennoadu vidinthaal enne"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirellaam unnoadu urainthaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirellaam unnoadu urainthaal enne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thathalikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathalikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thottedukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottedukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muththangal tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththangal tha tha tha tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ambuli maama ambuli maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambuli maama ambuli maama"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai theera muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai theera muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye kannitheevey kannitheevey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye kannitheevey kannitheevey"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithaai poale muththamidu muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithaai poale muththamidu muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge thuvangalaam engu mayangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge thuvangalaam engu mayangalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhamaanathu vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhamaanathu vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engu nerungalaam engu norungalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engu nerungalaam engu norungalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">saethamaanathu naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saethamaanathu naanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga thazhumbalaam enge thayangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thazhumbalaam enge thayangalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">paesi paesi arivoamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paesi paesi arivoamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge nirayalaam engu kurayalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge nirayalaam engu kurayalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum naanum inaivoamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum inaivoamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adayaala nagham poade anaithaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adayaala nagham poade anaithaal enne"/>
</div>
<div class="lyrico-lyrics-wrapper">idaivaelai mudiyaamal inaithaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaivaelai mudiyaamal inaithaal enne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuthikaalil uyir poovai parithaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuthikaalil uyir poovai parithaal enne"/>
</div>
<div class="lyrico-lyrics-wrapper">athikaalai varai paadam padithaal enne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athikaalai varai paadam padithaal enne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thitte thitte kette kette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thitte thitte kette kette"/>
</div>
<div class="lyrico-lyrics-wrapper">muththangal tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththangal tha tha tha tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ambuli maama ambuli maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambuli maama ambuli maama"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai theera muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai theera muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aye kannitheevey kannitheevey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye kannitheevey kannitheevey"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithaai poale muththamidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithaai poale muththamidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithazhoadu ithazhai saerthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithazhoadu ithazhai saerthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en koode nilappethenne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en koode nilappethenne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viraloadu viralai saerthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraloadu viralai saerthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilayaada azhaippathenne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilayaada azhaippathenne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muththangal tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththangal tha tha tha"/>
</div>
</pre>
