---
title: "let's do song lyrics"
album: "Inttelligent"
artist: "S Thaman"
lyricist: "Chandrabose"
director: "V V Vinayak"
path: "/albums/inttelligent-lyrics"
song: "Let's Do"
image: ../../images/albumart/inttelligent.jpg
date: 2018-02-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jw7nGy-uLMQ"
type: "happy"
singers:
  - Saketh Komanduri
  - SriKrishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Pinga Pinga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pinga Pinga "/>
</div>
<div class="lyrico-lyrics-wrapper">Pinga Pingaa Pingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinga Pingaa Pingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Pinga Pinga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pinga Pinga "/>
</div>
<div class="lyrico-lyrics-wrapper">Pinga Pingaa Pingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinga Pingaa Pingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepu Jumping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepu Jumping"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepu Thumping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepu Thumping"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkaasepu Thinking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkaasepu Thinking"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeda Aada Hangking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeda Aada Hangking"/>
</div>
<div class="lyrico-lyrics-wrapper">Signals Sending
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signals Sending"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Time Ayina Trending
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Time Ayina Trending"/>
</div>
<div class="lyrico-lyrics-wrapper">Vundalante Life Ki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vundalante Life Ki "/>
</div>
<div class="lyrico-lyrics-wrapper">Perfect Meaning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perfect Meaning"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Do Lets Do 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Do Lets Do "/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Do It Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Do It Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Ping Ping Ping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ping Ping Ping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Helping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyakapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyakapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lifey Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifey Nothing"/>
</div>
<div class="lyrico-lyrics-wrapper">Ping Ping Ping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ping Ping Ping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Helping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyakapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyakapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lifey Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifey Nothing"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasepu Jumping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepu Jumping"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasepu Thumping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasepu Thumping"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkaasepu Thinking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkaasepu Thinking"/>
</div>
<div class="lyrico-lyrics-wrapper">Vundalante Life Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vundalante Life Ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Perfect Meaning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perfect Meaning"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monnatlaage Eating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monnatlaage Eating"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnatlaage Sleeping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnatlaage Sleeping"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppatlaage Roaming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppatlaage Roaming"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho Entho Boring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho Entho Boring"/>
</div>
<div class="lyrico-lyrics-wrapper">Boring Boring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boring Boring"/>
</div>
<div class="lyrico-lyrics-wrapper">Thippeyyali Steering
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thippeyyali Steering"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopinchaali Caring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopinchaali Caring"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheseyyali Sharing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheseyyali Sharing"/>
</div>
<div class="lyrico-lyrics-wrapper">Stopey Leni Staring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stopey Leni Staring"/>
</div>
<div class="lyrico-lyrics-wrapper">Handni Hand Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Handni Hand Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesey Bonding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesey Bonding"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartni Heart Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartni Heart Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesey Hugging
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesey Hugging"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Do"/>
</div>
<div class="lyrico-lyrics-wrapper">Ping Ping Ping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ping Ping Ping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Helping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyakapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyakapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lifey Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifey Nothing"/>
</div>
<div class="lyrico-lyrics-wrapper">Ping Ping Ping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ping Ping Ping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Helping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyakapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyakapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lifey Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifey Nothing"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondalalo Nelakonna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondalalo Nelakonna "/>
</div>
<div class="lyrico-lyrics-wrapper">Koneti Rayadu Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koneti Rayadu Vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondalantha Varamulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondalantha Varamulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedu Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedu Vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondalalo Nelakonna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondalalo Nelakonna "/>
</div>
<div class="lyrico-lyrics-wrapper">Koneti Rayadu Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koneti Rayadu Vaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choopullona Rocking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopullona Rocking"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvullona Cracking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvullona Cracking"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatallona Firing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatallona Firing"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethallona Cheering
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethallona Cheering"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Nuvvu Making
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Nuvvu Making"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekai Nuvvu Tracking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekai Nuvvu Tracking"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Nuvvu Blocking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Nuvvu Blocking"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhariki Nuvvu Backing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhariki Nuvvu Backing"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali Varsham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Varsham "/>
</div>
<div class="lyrico-lyrics-wrapper">Freegaa Incoming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freegaa Incoming"/>
</div>
<div class="lyrico-lyrics-wrapper">Seva Bhaavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seva Bhaavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesey Out Going
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesey Out Going"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Do"/>
</div>
<div class="lyrico-lyrics-wrapper">Ping Ping Ping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ping Ping Ping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Helping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyakapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyakapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lifey Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifey Nothing"/>
</div>
<div class="lyrico-lyrics-wrapper">Ping Ping Ping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ping Ping Ping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Helping Ping 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Helping Ping "/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam Manamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam Manamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyakapothe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyakapothe "/>
</div>
<div class="lyrico-lyrics-wrapper">Lifey Nothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifey Nothing"/>
</div>
</pre>
