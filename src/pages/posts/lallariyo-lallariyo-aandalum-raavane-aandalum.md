---
title: "Lallariyo Lallariyo song lyrics"
album: "Raame Aandalum Raavane Aandalum"
artist: "Krish"
lyricist: "Ve. Madhankumar"
director: "Arisil Moorthy"
path: "/albums/raame-aandalum-raavane-aandalum-lyrics"
song: "Lallariyo Lallariyo"
image: ../../images/albumart/raame-aandalum-raavane-aandalum.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IN9AS_CH5wE"
type: "happy"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavudu kottum sakku paiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavudu kottum sakku paiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavudu kottum sakku paiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavudu kottum sakku paiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavudu kotti vechirundha andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavudu kotti vechirundha andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi thaanae thinnuduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi thaanae thinnuduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi thaanae thinnuduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi thaanae thinnuduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soaru vadicha neecha thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soaru vadicha neecha thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soaru vadicha neecha thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soaru vadicha neecha thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neecha thanni oothi vechi andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neecha thanni oothi vechi andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulu thaaliyum kottiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulu thaaliyum kottiduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulu thaaliyum kottiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulu thaaliyum kottiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachakulam oarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakulam oarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu paadi naanum vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu paadi naanum vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rettai yeri mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai yeri mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragam paadi nanum varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragam paadi nanum varen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragam paadi nanum varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragam paadi nanum varen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela theru suthi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela theru suthi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu mani aayidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu mani aayidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela theru suthi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela theru suthi vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu mani aayidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu mani aayidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu mani aayidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu mani aayidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhukattu irulum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhukattu irulum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekirama vanthirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekirama vanthirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Menjadhu dhan pothumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menjadhu dhan pothumada"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetukku dhan vaangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukku dhan vaangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhukattu irulum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhukattu irulum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekirama vanthirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekirama vanthirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Menjadhu dhan pothumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menjadhu dhan pothumada"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetukku dhan vaangada…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukku dhan vaangada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavudu kottum sakku paiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavudu kottum sakku paiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavudu kottum sakku paiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavudu kottum sakku paiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavudu kotti vechirundha andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavudu kotti vechirundha andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi thaanae thinnuduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi thaanae thinnuduchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi thaanae thinnuduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi thaanae thinnuduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soaru vadicha neecha thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soaru vadicha neecha thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallariyo lallariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallariyo lallariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Soaru vadicha neecha thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soaru vadicha neecha thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neecha thanni oothi vechi andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neecha thanni oothi vechi andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulu thaaliyum kottiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulu thaaliyum kottiduchae"/>
</div>
</pre>
