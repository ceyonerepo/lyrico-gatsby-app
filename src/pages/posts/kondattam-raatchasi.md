---
title: "kondattam song lyrics"
album: "Raatchasi"
artist: "Sean Roldan"
lyricist: "Thanikodi"
director: "Sy Gowthamraj"
path: "/albums/raatchasi-lyrics"
song: "Kondattam"
image: ../../images/albumart/raatchasi.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3EH1vw7p75M"
type: "happy"
singers:
  - Sean Roldan
  - Bamba Bhagya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dheem Tha Thakka Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Tha Thakka Thin"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem Tha Thakka Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Tha Thakka Thin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheem Tha Thakka Thin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Tha Thakka Thin"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem Tha Thakka Thin Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem Tha Thakka Thin Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondattam Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Palli Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Palli Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvattu Indha Naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvattu Indha Naaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Missu-nu Siru-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missu-nu Siru-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingum Pala Parent-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum Pala Parent-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppodhum Vella Thaaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppodhum Vella Thaaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondattam Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Palli Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Palli Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvattu Indha Naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvattu Indha Naaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Missu-nu Siru-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missu-nu Siru-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingum Pala Parent-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum Pala Parent-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppodhum Vella Thaaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppodhum Vella Thaaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Vannam Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vannam Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Chalk Piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Chalk Piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Natpil Exam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Natpil Exam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetchi Paaru Yellamae Pass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetchi Paaru Yellamae Pass-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaalae Adakaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae Adakaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Sondham Noorudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Sondham Noorudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaadu Velaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaadu Velaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Nammoda Naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nammoda Naaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondattam Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Palli Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Palli Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvattu Indha Naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvattu Indha Naaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Missu-nu Siru-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missu-nu Siru-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingum Pala Parent-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum Pala Parent-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppodhum Vella Thaaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppodhum Vella Thaaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anilaatam Vaaluthanam – Chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anilaatam Vaaluthanam – Chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaagum Aanduvilaa – Attrocity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaagum Aanduvilaa – Attrocity"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Thullum Kannu – Kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Thullum Kannu – Kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookolam Potomae – Medai Katty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookolam Potomae – Medai Katty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaiyattula Chinna – Chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyattula Chinna – Chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangster Pol Urumaaruvom -Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangster Pol Urumaaruvom -Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Maidhaanatha Vittu – Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maidhaanatha Vittu – Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaavom Frindshipla – Rosham Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaavom Frindshipla – Rosham Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam Melae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam Melae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Bhoomi Seiyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Bhoomi Seiyalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeenai Kondaandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeenai Kondaandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhai Yengum Thoovalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai Yengum Thoovalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaipaali Nam Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaipaali Nam Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Yana Paadam Sollalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yana Paadam Sollalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaikodu Illadha – Pudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaikodu Illadha – Pudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookolam Seiyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookolam Seiyalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mun Benchu Paiyan Yellam – Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Benchu Paiyan Yellam – Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pin Benchil Thonralaamae – Kamban Shelly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin Benchil Thonralaamae – Kamban Shelly"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Thozhi Thitti Sonna – Keppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Thozhi Thitti Sonna – Keppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Kaamam Illadha – Nesam Kaapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Kaamam Illadha – Nesam Kaapom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilan Thendralai Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilan Thendralai Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Maara Sonnaarudaa – Abdul Kalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Maara Sonnaarudaa – Abdul Kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Naamelaam Madham – Saadhikelaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naamelaam Madham – Saadhikelaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Veithudhaan – Veipom Salaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Veithudhaan – Veipom Salaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Firstu Ranku Last Ranku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firstu Ranku Last Ranku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Pallikulladhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Pallikulladhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velivandha Bayam Pooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivandha Bayam Pooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yellam Onnaethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yellam Onnaethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragaalae Adaikaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragaalae Adaikaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai Nesam Pola Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Nesam Pola Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaana Uravaagum Nam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaana Uravaagum Nam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaangal Paasam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaangal Paasam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondattam Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Palli Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Palli Kondattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondattam Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Palli Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Palli Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvattu Indha Naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvattu Indha Naaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Missu-nu Siru-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missu-nu Siru-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingum Pala Parent-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum Pala Parent-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppodhum Vella Thaaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppodhum Vella Thaaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Vannam Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vannam Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Chalk Piece-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Chalk Piece-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Natpil Exam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Natpil Exam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetchi Paaru Yellamae Pass-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetchi Paaru Yellamae Pass-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaalae Adakaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae Adakaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Sondham Noorudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Sondham Noorudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaadu Velaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaadu Velaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Nammoda Naaludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nammoda Naaludaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaana Thannaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaana Thannaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan Naana Thannaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan Naana Thannaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannaanan Thanan Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannaanan Thanan Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaana Thannaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaana Thannaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan Naana Thannaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan Naana Thannaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannaanan Thanan Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannaanan Thanan Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy"/>
</div>
</pre>
