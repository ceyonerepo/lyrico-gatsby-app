---
title: "ilaa ilaa song lyrics"
album: "Thalaivii"
artist: "G.V. Prakash Kumar"
lyricist: "Sira Sri"
director: "A.L. Vijay"
path: "/albums/thalaivii-lyrics"
song: "Ilaa Ilaa"
image: ../../images/albumart/thalaivii.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/A5P97AzSZH0"
type: "mass"
singers:
  - Saindhavi Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ilaa ilaa thelanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa ilaa thelanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gilleti galullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gilleti galullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuma ghuma pulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuma ghuma pulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalleti dharullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalleti dharullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila ila thelanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila ila thelanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gilleti galullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gilleti galullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuma ghuma pulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuma ghuma pulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalleti dharullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalleti dharullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa madhi haayiga endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa madhi haayiga endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthilaa paadi oogindhile ooyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthilaa paadi oogindhile ooyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa ilaa thelanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa ilaa thelanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gilleti galullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gilleti galullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuma ghuma pulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuma ghuma pulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalleti dharullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalleti dharullo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meghalloni channirantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghalloni channirantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa meedha oanniru jalletundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa meedha oanniru jalletundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallakilo musthabayyi jabilli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakilo musthabayyi jabilli "/>
</div>
<div class="lyrico-lyrics-wrapper">ooreginatte undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooreginatte undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulo unna kanupaapa kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulo unna kanupaapa kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalevo kantu undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalevo kantu undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelamma manase aa ningikichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelamma manase aa ningikichi"/>
</div>
<div class="lyrico-lyrics-wrapper">O muddhu koruthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O muddhu koruthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa madhi haayiga endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa madhi haayiga endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthilaa paadi oogindhile ooyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthilaa paadi oogindhile ooyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa ilaa thelanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa ilaa thelanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gilleti galullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gilleti galullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuma ghuma pulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuma ghuma pulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalleti dharullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalleti dharullo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machhe leni anandhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machhe leni anandhama"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manasune veedi poniyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manasune veedi poniyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhainanu emainanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhainanu emainanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye badhanu neneu raaniyyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye badhanu neneu raaniyyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Soubashanela thamashagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soubashanela thamashagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa maya chupinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa maya chupinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa premathote paashanamaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa premathote paashanamaina "/>
</div>
<div class="lyrico-lyrics-wrapper">prananni pondheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prananni pondheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa madhi haayiga endhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa madhi haayiga endhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthilaa paadi oogindhile ooyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthilaa paadi oogindhile ooyala"/>
</div>
</pre>
