---
title: "swagatham krishna song lyrics"
album: "Agnyaathavaasi"
artist: "Anirudh Ravichander"
lyricist: "Oothukkadu Sri Venkata Subbaiyer Kriti"
director: "Trivikram Srinivas"
path: "/albums/agnyaathavaasi-lyrics"
song: "Swagatham Krishna"
image: ../../images/albumart/agnyaathavaasi.jpg
date: 2018-01-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eFkXKxvkn6Y"
type: "melody"
singers:
  - Padmalatha
  - Niranjana Ramanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Madhuraapuri Sadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraapuri Sadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mridu Vadhana… Madhusoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mridu Vadhana… Madhusoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Swaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Swaagatham Krishnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharanaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharanaagatham Krishnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhuraapuri Sadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraapuri Sadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mridu Vadhana… Madhusoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mridu Vadhana… Madhusoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Swaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Swaagatham Krishnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharanaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharanaagatham Krishnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhuraapuri Sadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraapuri Sadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mridu Vadhana… Madhusoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mridu Vadhana… Madhusoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Swaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Swaagatham Krishnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharanaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharanaagatham Krishnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhuraapuri Sadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraapuri Sadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mridu Vadhana Madhusoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mridu Vadhana Madhusoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Swaagatham Krishnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Swaagatham Krishnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishnaa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishnaa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mushtikaasoora Chaanoora Malla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushtikaasoora Chaanoora Malla"/>
</div>
<div class="lyrico-lyrics-wrapper">Malla Vishaaratha Madhusoodhanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malla Vishaaratha Madhusoodhanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushtikaasoora Chaanoora Malla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushtikaasoora Chaanoora Malla"/>
</div>
<div class="lyrico-lyrics-wrapper">Malla Vishaaratha Madhusoodhanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malla Vishaaratha Madhusoodhanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushtikaasoora Chaanoora Malla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushtikaasoora Chaanoora Malla"/>
</div>
<div class="lyrico-lyrics-wrapper">Malla Vishaaratha Kuvalayapeeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malla Vishaaratha Kuvalayapeeta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mardhana Kalinga Narthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mardhana Kalinga Narthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gokula Rakshana Sakala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gokula Rakshana Sakala "/>
</div>
<div class="lyrico-lyrics-wrapper">Sulakshana Devaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulakshana Devaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mardhana Kalinga Narthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mardhana Kalinga Narthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gokula Rakshana Sakala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gokula Rakshana Sakala "/>
</div>
<div class="lyrico-lyrics-wrapper">Sulakshana Devaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulakshana Devaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shistha Jana Paala Sankalpa Kalpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shistha Jana Paala Sankalpa Kalpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalpa Shathakoti Asamaparaabhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalpa Shathakoti Asamaparaabhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Shistha Jana Paala Sankalpa Kalpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shistha Jana Paala Sankalpa Kalpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalpa Shathakoti Asamaparaabhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalpa Shathakoti Asamaparaabhava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera Munijana Vihaara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera Munijana Vihaara "/>
</div>
<div class="lyrico-lyrics-wrapper">Madhana Sukumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhana Sukumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaithya Samhaara Devaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaithya Samhaara Devaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera Munijana Vihaara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera Munijana Vihaara "/>
</div>
<div class="lyrico-lyrics-wrapper">Madhana Sukumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhana Sukumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaithya Samhaara Devaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaithya Samhaara Devaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura MadhuraRathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura MadhuraRathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Saahasa Saahasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saahasa Saahasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vraja Yuvati Jana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vraja Yuvati Jana "/>
</div>
<div class="lyrico-lyrics-wrapper">Maanasa Poojitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanasa Poojitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa Dapa Gari PaGaRiSaDaSa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Dapa Gari PaGaRiSaDaSa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">SaRiGaPaDa Sa Da Pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="SaRiGaPaDa Sa Da Pa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ri PaGaRiSaDaSa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri PaGaRiSaDaSa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">SaSaRiRiGaGa PaDaSaSa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="SaSaRiRiGaGa PaDaSaSa"/>
</div>
<div class="lyrico-lyrics-wrapper">Da Pa Pa Ga Ri Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da Pa Pa Ga Ri Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Ga Ri Sa Da Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Ga Ri Sa Da Sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">SaRiGa RiGaPa GaPaDa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="SaRiGa RiGaPa GaPaDa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Dapa GaRi Pagarisadasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Dapa GaRi Pagarisadasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatthik Thakajanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthik Thakajanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthik Thakajanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthik Thakajanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak Thakajanu Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak Thakajanu Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Da Pa Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Da Pa Ga Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Ga Ri Sa Da Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Ga Ri Sa Da Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthik Thakajanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthik Thakajanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatthik Thakajanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatthik Thakajanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak Thakajanu Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak Thakajanu Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Da Pa Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Da Pa Ga Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Ga Ri Sa Da Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Ga Ri Sa Da Sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakathari Kukunthana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathari Kukunthana "/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Thakadheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Thakadheem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishnaa Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishnaa Aa Aa Aa"/>
</div>
</pre>
