---
title: "anbe endan song lyrics"
album: "Pattinapakkam"
artist: "Ishaan Dev"
lyricist: "Murugan Mandhiram"
director: "Jayadev"
path: "/albums/pattinapakkam-lyrics"
song: "Anbe Endan"
image: ../../images/albumart/pattinapakkam.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/L2bKKDTKBKI"
type: "love"
singers:
  - Adheef
  - Akhila Anand
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mmmnanananaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmnanananaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmnanananaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmnanananaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh anbae endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh anbae endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai thindru po po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai thindru po po"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai endraal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai endraal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikondru po po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikondru po po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai thindru po po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai thindru po po"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai endraal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai endraal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikondru po po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikondru po po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kanmai un kangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanmai un kangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai theduma theduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai theduma theduma"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayamae un idhayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhayamae un idhayamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ketkkuma ketkkuma haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ketkkuma ketkkuma haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkudhae paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkudhae paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai ethir paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai ethir paarkkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam polae siru mazhaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam polae siru mazhaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai uthari pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai uthari pogaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil vizhundhae udaiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil vizhundhae udaiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkkudhae paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkudhae paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai ethir paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai ethir paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye paarkkudhae paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye paarkkudhae paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai ethir paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai ethir paarkkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm mmm hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm hmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm hmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo oru theekutchi urasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oru theekutchi urasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila pookkalum pookkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila pookkalum pookkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella nee thottu pogaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella nee thottu pogaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Boogambam thorkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boogambam thorkkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru theekutchi urasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru theekutchi urasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila pookkalum pookkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila pookkalum pookkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella nee thottu pogaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella nee thottu pogaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Boogambam thorkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boogambam thorkkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta maadiyil poochediyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta maadiyil poochediyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchi veyilula naan thavichenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchi veyilula naan thavichenn"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum neeyum perum mazhaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum neeyum perum mazhaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thuli thudichadum naan pizhaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thuli thudichadum naan pizhaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kanmai un kangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanmai un kangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai theduma theduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai theduma theduma"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayamae un idhayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhayamae un idhayamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ketkkuma ketkkuma haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ketkkuma ketkkuma haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkudhae paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkudhae paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai ethir paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai ethir paarkkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam polae siru mazhaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam polae siru mazhaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai uthari pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai uthari pogaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil vizhundhae udaiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil vizhundhae udaiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkkudhae paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkudhae paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai ethir paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai ethir paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkudhae paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkudhae paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir unnai ethir paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir unnai ethir paarkkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm mmm hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm hmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm mmm hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm mmm hmmmm"/>
</div>
</pre>
