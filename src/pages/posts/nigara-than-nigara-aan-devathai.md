---
title: "nigara than nigara song lyrics"
album: "Aan Devathai"
artist: "Ghibran"
lyricist: "Soundararajan K"
director: "Thamira"
path: "/albums/aan-devathai-lyrics"
song: "Nigara Than Nigara"
image: ../../images/albumart/aan-devathai.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9SGKZEcjbwU"
type: "affection"
singers:
  - Vineeth Sreenivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nigara than nigara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigara than nigara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thaalattu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thaalattu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamai per idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamai per idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thanthai mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thanthai mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilthogaiyai ivan vasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilthogaiyai ivan vasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyagidum aan devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyagidum aan devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin uyaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin uyaram "/>
</div>
<div class="lyrico-lyrics-wrapper">uruvam thayumanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvam thayumanavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigara than nigara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigara than nigara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thaalattu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thaalattu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamai per idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamai per idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thanthai mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thanthai mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai indha ulagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai indha ulagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumugam seiyum oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumugam seiyum oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyai vazhithunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyai vazhithunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral pidithidum thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral pidithidum thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pizhaiyai naam pizhaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaiyai naam pizhaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirithidum oru kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirithidum oru kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai un nizhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai un nizhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhaithiduma kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaithiduma kavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiyaga maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyaga maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyalvaai pala neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyalvaai pala neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai meeri adaivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai meeri adaivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai enum vedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai enum vedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaipattam nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaipattam nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumakkum thanthaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumakkum thanthaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigara than nigara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigara than nigara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thaalattu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thaalattu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamai per idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamai per idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thanthai mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thanthai mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilthogaiyai ivan vasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilthogaiyai ivan vasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyagidum aan devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyagidum aan devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin uyaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin uyaram "/>
</div>
<div class="lyrico-lyrics-wrapper">uruvam thayumanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvam thayumanavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madiyae ivan madiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyae ivan madiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maragatha thalaiyanaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maragatha thalaiyanaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai than mazhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai than mazhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbinil ivan nagalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbinil ivan nagalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyal pudhu vidiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal pudhu vidiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthidum kathiravanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthidum kathiravanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavum kanvalarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavum kanvalarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaigalai kathaipavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaigalai kathaipavanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiyendra ondril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyendra ondril"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal enbathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal enbathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Than kangal rendaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than kangal rendaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal oottum annai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal oottum annai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">thanai thanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanai thanthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaikkinaiyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaikkinaiyillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigara than nigara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigara than nigara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thaalattu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thaalattu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamai per idhamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamai per idhamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thanthai mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thanthai mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilthogaiyai ivan vasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilthogaiyai ivan vasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaiyagidum aan devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyagidum aan devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin uyaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin uyaram "/>
</div>
<div class="lyrico-lyrics-wrapper">uruvam thayumanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvam thayumanavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nigara than nigara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigara than nigara"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr thaalattu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr thaalattu nee"/>
</div>
</pre>
