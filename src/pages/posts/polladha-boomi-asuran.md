---
title: 'polladha boomi song lyrics'
album: 'Asuran'
artist: 'G.V. Prakash Kumar'
lyricist: 'Yugabharathi'
director: 'Vetrimaaran'
path: '/albums/asuran-song-lyrics'
song: 'Polladha Boomi'
image: ../../images/albumart/asuran.jpg
date: 2019-10-04
lang: tamil
singers: 
- Dhanush
- G.V. Prakash Kumar
- Ken Karunas
- Tee Jay
youtubeLink: "https://www.youtube.com/embed/ZBn_sEfoUDU"
type: 'message'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Polladha boomi bolippodum aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Polladha boomi bolippodum aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaala ponaa narukkaatho kaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Munnaala ponaa narukkaatho kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Anboda ninnaa thala vanangum ooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Anboda ninnaa thala vanangum ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavesam aana uyiredukkum paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aavesam aana uyiredukkum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veeraapputhaan venaam aiyya
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeraapputhaan venaam aiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettoda iru nee thonaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Veettoda iru nee thonaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Soolaayutham nee thookkunaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Soolaayutham nee thookkunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Villangam varumae vinaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Villangam varumae vinaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un meesa murukkaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Un meesa murukkaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi yethu yethu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodi yethu yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppaattan kolam kaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Muppaattan kolam kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaara maathu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Varalaara maathu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aagaasama ninna nama
<input type="checkbox" class="lyrico-select-lyric-line" value="Aagaasama ninna nama"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaanthu paakkum jillaavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Annaanthu paakkum jillaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Appaaviya thallaaduna
<input type="checkbox" class="lyrico-select-lyric-line" value="Appaaviya thallaaduna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallaandhu povom mannaave
<input type="checkbox" class="lyrico-select-lyric-line" value="Mallaandhu povom mannaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottaandiya aanaalumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oottaandiya aanaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulloora venum oru dhillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulloora venum oru dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaathura aala ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemaathura aala ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkuda modhavara sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enkuda modhavara sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaar mela yaar keezha
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar mela yaar keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaatha ruleu
<input type="checkbox" class="lyrico-select-lyric-line" value="Podaatha ruleu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraada ennaatti maaraathae naalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Poraada ennaatti maaraathae naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththeetti mela paanjaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuththeetti mela paanjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Goyyaala keezha saanjaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Goyyaala keezha saanjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththaana aalaa naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Veththaana aalaa naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga maatten moochae ponaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaga maatten moochae ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy polladha boomi bolippodum aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy polladha boomi bolippodum aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaala ponaa narukkaatho kaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Munnaala ponaa narukkaatho kaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kattaariyum kodaaliyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattaariyum kodaaliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyendhum vaazhva maathaatho
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyendhum vaazhva maathaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchondhiyaa vaazhaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Pachchondhiyaa vaazhaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thanmaanam oora kaakkaatho
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thanmaanam oora kaakkaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manvaasana unmelathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Manvaasana unmelathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaama veesum kudikondu
<input type="checkbox" class="lyrico-select-lyric-line" value="Makkaama veesum kudikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un perula paththooraiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Un perula paththooraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaavaa podum governmentu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pattaavaa podum governmentu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aathaadi en mavan thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aathaadi en mavan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asagaaya sooran
<input type="checkbox" class="lyrico-select-lyric-line" value="Asagaaya sooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatteri vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatteri vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaatha veeran
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalangaatha veeran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Komberi mookkan un koottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Komberi mookkan un koottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottaavae serum en paattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Koottaavae serum en paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummaalam poda naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Gummaalam poda naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae vaaren okay alrightu
<input type="checkbox" class="lyrico-select-lyric-line" value="Serndhae vaaren okay alrightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un meesa murukkaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Un meesa murukkaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi yethu yethu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodi yethu yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppaattan kolam kaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Muppaattan kolam kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaara maathu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Varalaara maathu"/></div>
</div>
</pre>