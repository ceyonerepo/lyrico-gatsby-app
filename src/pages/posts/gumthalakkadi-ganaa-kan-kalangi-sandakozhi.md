---
title: "gumthalakkadi ganaa kan kalangi song lyrics"
album: "Sandakozhi"
artist: "Yuvan Sankar Raja"
lyricist: "Na.Muthukumar"
director: "N. Linguswamy"
path: "/albums/sandakozhi-lyrics"
song: "Gumthalakkadi Gana Kan Kalangi"
image: ../../images/albumart/sandakozhi.jpg
date: 2005-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-_Ar1gy3Kcc"
type: "Farewell"
singers:
  - Karthik
  - Renjith 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gumththalakaddi Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumththalakaddi Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Kalangi Feelu Uda Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kalangi Feelu Uda Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiththalakkadi Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththalakkadi Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice A pola Urugi Nikka Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice A pola Urugi Nikka Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi Gumththalakaddi Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Gumththalakaddi Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Kalangi Feelu Uda Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kalangi Feelu Uda Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiththalakkadi Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththalakkadi Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice A Pola Urugi Nikka Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice A Pola Urugi Nikka Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Photo Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Photo Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polambal venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambal venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Autograph Nottum Vennaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Autograph Nottum Vennaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Instant Kanneer Ethuvum Venaam Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Instant Kanneer Ethuvum Venaam Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai Kovai Mathurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Kovai Mathurai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Salem Ellaithaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Salem Ellaithaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Night u Bus A Pudichcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night u Bus A Pudichcha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Naalaikku Paakalaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Naalaikku Paakalaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumththalakaddi Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumththalakaddi Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Kalangi Feelu Uda Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kalangi Feelu Uda Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiththalakkadi Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththalakkadi Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice A pola Urugi Nikka Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice A pola Urugi Nikka Venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida Whistle U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida Whistle U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida Whistle U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida Whistle U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Santhoshom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Santhoshom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padichchu Kizhichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichchu Kizhichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhichum Padichchom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhichum Padichchom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothum Sagavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Sagavaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indre Kadaisi Poster Otta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indre Kadaisi Poster Otta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Onnum Theatre Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Onnum Theatre Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Moota Thookki Sumakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Moota Thookki Sumakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Onnum Porter Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Onnum Porter Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munusami Peththu Potta Kandhasami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munusami Peththu Potta Kandhasami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mogatha Naan Paathathu Pothumsami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mogatha Naan Paathathu Pothumsami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichaanu Maaththi Putta Krishnasami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichaanu Maaththi Putta Krishnasami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kutha Kutha Veru Edam Paaru Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kutha Kutha Veru Edam Paaru Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bitta Paakka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bitta Paakka Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Vidu Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Vidu Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theva Patta Number Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Patta Number Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phona Pottu Pesu Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phona Pottu Pesu Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da  Udaatha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da  Udaatha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Pirinju Eppadi Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pirinju Eppadi Iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenamum Paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenamum Paakkaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Sister A Kattikirennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Sister A Kattikirennu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavum Piriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Piriyaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College paadam Maranthaa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College paadam Maranthaa Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Friendship Marakkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Friendship Marakkaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athanaalthaanda Arrears Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaalthaanda Arrears Vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkadi Paapom Inimelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Paapom Inimelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trainoda Thandavaalam Piriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trainoda Thandavaalam Piriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Timeoda Rendu Mullum Piriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timeoda Rendu Mullum Piriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beer Adichu Cheers Solli Piriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beer Adichu Cheers Solli Piriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bore Adicha Bye Solli Piriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bore Adicha Bye Solli Piriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththi Suththi Naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Suththi Naama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Poga Porom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Poga Porom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattamaana Bhoomi Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattamaana Bhoomi Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaayama Paakalaamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaayama Paakalaamda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Suraangani Suraangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Suraangani Suraangani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Paadu Da Udaatha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadu Da Udaatha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogaththa Ellaam Mootta Katti Poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogaththa Ellaam Mootta Katti Poduda"/>
</div>
</pre>
