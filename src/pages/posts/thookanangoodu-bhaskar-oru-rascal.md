---
title: "thookanangoodu song lyrics"
album: "Bhaskar Oru Rascal"
artist: "Amresh Ganesh"
lyricist: "Viveka"
director: "Siddique"
path: "/albums/bhaskar-oru-rascal-lyrics"
song: "Thookanangoodu"
image: ../../images/albumart/bhaskar-oru-rascal.jpg
date: 2018-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_mcBb-XAL0g"
type: "happy"
singers:
  - Krish
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thookanangoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookanangoodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil tolet board-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil tolet board-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongutha thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongutha thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi one day vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi one day vaazhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appachi thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appachi thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol angae ingae aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol angae ingae aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhugal kiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhugal kiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thool ganaa paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thool ganaa paadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh…pirinthu poguthae saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh…pirinthu poguthae saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sera koodumo naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sera koodumo naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae kangal podum kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae kangal podum kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae hey kaalam podum kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae hey kaalam podum kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai maatruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai maatruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallam illa ullam yenguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallam illa ullam yenguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moochithan aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moochithan aaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaniliyae maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaniliyae maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasantha megam poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasantha megam poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae vidinja vera naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae vidinja vera naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum vera aaluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum vera aaluda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookanangoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookanangoodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil tolet board-uThongutha thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil tolet board-uThongutha thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi one day vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi one day vaazhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae kandam vittu kandam thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae kandam vittu kandam thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae thedi vandhu sera koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae thedi vandhu sera koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna chinna pookal aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna pookal aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna megam adhai mooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna megam adhai mooduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru nilaa thottama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru nilaa thottama"/>
</div>
<div class="lyrico-lyrics-wrapper">Maariduthae manam yeanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maariduthae manam yeanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha kathirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha kathirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirikol innum varalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirikol innum varalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookanangoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookanangoodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil tolet board-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil tolet board-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongutha thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongutha thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi one day vaazhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi one day vaazhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appachi thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appachi thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol angae ingae aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol angae ingae aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhugal kiliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhugal kiliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thool ganaa paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thool ganaa paadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh pirinthu poguthae saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh pirinthu poguthae saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sera koodumo naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sera koodumo naalai"/>
</div>
</pre>
