---
title: "punnagai maayai song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Thamarai - Kabilan"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Punnagai Maayai"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ALfWJnccxxI"
type: "Love"
singers:
  - Abhay Jodhpurkar
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yen mounamai thoondil ondru veesinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen mounamai thoondil ondru veesinai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevathai isai moli thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevathai isai moli thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">paarvaiyal pesinai ennai thanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvaiyal pesinai ennai thanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un oviyam osaiyai korkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un oviyam osaiyai korkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">athil aayiral vanavil kankiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil aayiral vanavil kankiren"/>
</div>
<div class="lyrico-lyrics-wrapper">yaridam kelaa padalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaridam kelaa padalai"/>
</div>
<div class="lyrico-lyrics-wrapper">unniru kannal paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unniru kannal paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un oviyam osaiyai korkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un oviyam osaiyai korkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">athil aayiral vanavil kankiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil aayiral vanavil kankiren"/>
</div>
<div class="lyrico-lyrics-wrapper">yaridam kelaa padalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaridam kelaa padalai"/>
</div>
<div class="lyrico-lyrics-wrapper">unniru viligalil ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unniru viligalil ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadum isai yavum un kannin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadum isai yavum un kannin"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhiyo mozhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyo mozhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">paadum isai yavum un kannin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadum isai yavum un kannin"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhiyo mozhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyo mozhiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un poo mugam poothidum thavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un poo mugam poothidum thavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">punnagai maayai ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagai maayai ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en punnagai maayai ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en punnagai maayai ival"/>
</div>
<div class="lyrico-lyrics-wrapper">en punnagai maayai ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en punnagai maayai ival"/>
</div>
</pre>
