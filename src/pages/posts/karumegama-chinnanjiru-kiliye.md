---
title: "karumegama song lyrics"
album: "Chinnanjiru Kiliye"
artist: "Mastan Khader"
lyricist: "Padmanaban.G"
director: "Sabarinathan Muthupandian "
path: "/albums/chinnanjiru-kiliye-lyrics"
song: "Karumegama"
image: ../../images/albumart/chinnanjiru-kiliye.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KhfDk0uF8b4"
type: "love"
singers:
  - Haricharan
  - Vandana srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">karumegamai urumarava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumegamai urumarava"/>
</div>
<div class="lyrico-lyrics-wrapper">thuliyagi malaiyagi nathiyagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuliyagi malaiyagi nathiyagura"/>
</div>
<div class="lyrico-lyrics-wrapper">kal paraiya poovakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal paraiya poovakura"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyagi oliyagi uyir aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyagi oliyagi uyir aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aali aathu neera pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aali aathu neera pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thotukira nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotukira nee"/>
</div>
<div class="lyrico-lyrics-wrapper">aalamara kaatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalamara kaatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">sinungura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinungura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">manja vanna vennilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja vanna vennilava"/>
</div>
<div class="lyrico-lyrics-wrapper">veetukulla oli veesuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukulla oli veesuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">veyilu patta ven paniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilu patta ven paniya"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala karanju ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala karanju ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karumegamai urumarava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumegamai urumarava"/>
</div>
<div class="lyrico-lyrics-wrapper">thuliyagi malaiyagi nathiyagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuliyagi malaiyagi nathiyagura"/>
</div>
<div class="lyrico-lyrics-wrapper">kal paraiya poovakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal paraiya poovakura"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyagi oliyagi uyir aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyagi oliyagi uyir aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thenooda thinaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenooda thinaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">mannoda malaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannoda malaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu nee serura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee serura"/>
</div>
<div class="lyrico-lyrics-wrapper">malai megam kandale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai megam kandale"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiradum mayilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiradum mayilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ippothu naan aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippothu naan aaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">silayai irunthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silayai irunthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">sirika vachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirika vachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">pathara kedanthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathara kedanthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">patha aaki putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha aaki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">silayai irunthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silayai irunthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">sirika vachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirika vachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">pathara kedanthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathara kedanthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">patha aaki putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha aaki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">nall isaiyum paatumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nall isaiyum paatumai"/>
</div>
<div class="lyrico-lyrics-wrapper">namma enanju vaalalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma enanju vaalalam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kulalum kathuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kulalum kathuma"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaivom isaivom naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaivom isaivom naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karumegamai urumarava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumegamai urumarava"/>
</div>
<div class="lyrico-lyrics-wrapper">thuliyagi malaiyagi nathiyagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuliyagi malaiyagi nathiyagura"/>
</div>
<div class="lyrico-lyrics-wrapper">kal paraiya poovakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal paraiya poovakura"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyagi oliyagi uyir aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyagi oliyagi uyir aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathoda isai aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoda isai aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaviyoda tamil aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaviyoda tamil aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda naan seruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda naan seruren"/>
</div>
<div class="lyrico-lyrics-wrapper">kaarkaalam vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaarkaalam vanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">uravadum kuyilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravadum kuyilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu naan kooduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu naan kooduren"/>
</div>
<div class="lyrico-lyrics-wrapper">puthirai irunthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthirai irunthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">puriya vachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriya vachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">kirangi kidanthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirangi kidanthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kiruki aaki putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiruki aaki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">puthirai irunthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthirai irunthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">puriya vachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriya vachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">kirangi kidanthen enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirangi kidanthen enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kiruki aaki putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiruki aaki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sediyum kodiyumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sediyum kodiyumai"/>
</div>
<div class="lyrico-lyrics-wrapper">pinni penanju vaalalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinni penanju vaalalam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadalum alaiyumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadalum alaiyumai"/>
</div>
<div class="lyrico-lyrics-wrapper">pirivom inaivom naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirivom inaivom naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karumegamai urumarava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karumegamai urumarava"/>
</div>
<div class="lyrico-lyrics-wrapper">thuliyagi malaiyagi nathiyagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuliyagi malaiyagi nathiyagura"/>
</div>
<div class="lyrico-lyrics-wrapper">kal paraiya poovakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal paraiya poovakura"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyagi oliyagi uyir aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyagi oliyagi uyir aagura"/>
</div>
</pre>
