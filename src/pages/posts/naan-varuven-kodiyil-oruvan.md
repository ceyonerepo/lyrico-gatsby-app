---
title: "naan varuven song lyrics"
album: "Kodiyil Oruvan"
artist: "	Nivas K. Prasanna - Harish arjun"
lyricist: "Arun Bharathy"
director: "Ananda Krishnan"
path: "/albums/kodiyil-oruvan-lyrics"
song: "Naan Varuven"
image: ../../images/albumart/kodiyil-oruvan.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VU7t40u0pGg"
type: "mass"
singers:
  - Haricharan
  - Nivas K Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan varuven naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varuven naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayamai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamai varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyathai kaathidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyathai kaathidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyamaai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyamaai varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu panam kodukaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu panam kodukaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner vazhiyil varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner vazhiyil varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaadhi madham paarkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadhi madham paarkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena thozh tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena thozh tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan varuven naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varuven naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayamai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamai varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyathai kaathidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyathai kaathidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyamaai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyamaai varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan varuven naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varuven naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayamai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamai varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyathai kaathidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyathai kaathidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyamaai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyamaai varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu panam kodukaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu panam kodukaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner vazhiyil varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner vazhiyil varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaadhi madham paarkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadhi madham paarkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena thozh tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena thozh tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemaigalai thiruthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaigalai thiruthidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirudan naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirudan naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhchigalai murithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhchigalai murithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Erimalaiyai ezhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalaiyai ezhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil ennai pudhaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil ennai pudhaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi ezhunthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi ezhunthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul ennai thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul ennai thaduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadamaiyai seithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamaiyai seithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan varuven naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varuven naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayamai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamai varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyathai kaathidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyathai kaathidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyamaai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyamaai varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee porathadhu podhum maaridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee porathadhu podhum maaridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaatrai pudhidhaai maatridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaatrai pudhidhaai maatridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhu ulagae sernthu ethirthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu ulagae sernthu ethirthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee guniyamal thimiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee guniyamal thimiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai aattum koottam neekkidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai aattum koottam neekkidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thookkum kodumaiyai thakkidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai thookkum kodumaiyai thakkidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pin vangamal kan thungamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin vangamal kan thungamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee narigalai ottividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee narigalai ottividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimai polae nitham vazhndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai polae nitham vazhndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum vidiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum vidiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimel adi thaan vizhundhaal kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimel adi thaan vizhundhaal kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharmam saagadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharmam saagadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral neetiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral neetiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi maatriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi maatriduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhai naattiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhai naattiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi kaattiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaattiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral neetiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral neetiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi maatriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi maatriduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhai naattiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhai naattiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi kaattiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi kaattiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan varuven naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan varuven naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nichayamai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nichayamai varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyathai kaathidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyathai kaathidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyamaai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyamaai varuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu panam kodukaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu panam kodukaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ner vazhiyil varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ner vazhiyil varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaadhi madham paarkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaadhi madham paarkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena thozh tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena thozh tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannil ennai pudhaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil ennai pudhaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi ezhunthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi ezhunthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul ennai thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul ennai thaduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadamaiyai seithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamaiyai seithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo oo"/>
</div>
</pre>
