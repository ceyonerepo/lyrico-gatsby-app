---
title: "ennachu song lyrics"
album: "Paadam"
artist: "Ganesh Raghavendra"
lyricist: "Sorkko"
director: "Rajasekhar"
path: "/albums/paadam-song-lyrics"
song: "Ennachu"
image: ../../images/albumart/paadam.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BYDdci0-d74"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enachu ethachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enachu ethachu "/>
</div>
<div class="lyrico-lyrics-wrapper">enamo aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enamo aayachu"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu kaathu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu kaathu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">moongil kaathu pola aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongil kaathu pola aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vanathu poochiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanathu poochiya "/>
</div>
<div class="lyrico-lyrics-wrapper">kangale poothachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangale poothachu"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna chinna kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">kichu kichu kanna moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kichu kichu kanna moochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sollama mella vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollama mella vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">allama alluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allama alluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">pillai mozhi ulle maruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillai mozhi ulle maruthey"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusosai kekum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusosai kekum "/>
</div>
<div class="lyrico-lyrics-wrapper">pathai engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathai engum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyilosai ketkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyilosai ketkirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">siru poova pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru poova pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">pecha kettu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha kettu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam pookirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam pookirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru nodi pathathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodi pathathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir varai verkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir varai verkirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">mounama nikira nimisam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounama nikira nimisam"/>
</div>
<div class="lyrico-lyrics-wrapper">pournamiyil oliya vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pournamiyil oliya vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">mellisaya sethukaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mellisaya sethukaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">thondriya devatha vamsam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondriya devatha vamsam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannee nan eerkira parvaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannee nan eerkira parvaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeena pookum ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeena pookum ulagu"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusosai kekum patha engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusosai kekum patha engum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyilosai ketkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyilosai ketkirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">siru poova pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru poova pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">pecha kettu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha kettu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam pookirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam pookirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megathula nilavu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathula nilavu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavukul megam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavukul megam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla nyabagam irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla nyabagam irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha kili pathu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kili pathu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">padam nu nenaikum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padam nu nenaikum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">natchathiram kangalil mayangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natchathiram kangalil mayangum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirai onnu maranthathu boomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirai onnu maranthathu boomiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">mulu nilava aanathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulu nilava aanathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kolusosai kekum patha engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolusosai kekum patha engum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyilosai ketkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyilosai ketkirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">siru poova pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru poova pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">pecha kettu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha kettu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam pookirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam pookirathey"/>
</div>
</pre>
