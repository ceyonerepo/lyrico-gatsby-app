---
title: "pudhu vidha anubavam song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Arun Prabhu Purushothaman - Muthamil"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Pudhu Vidha Anubavam"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fQSXe4X9DGc"
type: "happy"
singers:
  - Pradeep Kumar
  - Kalyani Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">madai thiranthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madai thiranthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu vidha anubavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu vidha anubavamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madai thiranthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madai thiranthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu vidha anubavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu vidha anubavamo"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaakale manam thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaakale manam thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">vilakale kanaakalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilakale kanaakalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madai thiranthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madai thiranthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu vidha anubavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu vidha anubavamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanam boomi kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam boomi kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyin thunaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyin thunaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">neerai pola nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerai pola nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">illatha vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha vaalvile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanam boomi kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam boomi kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyin thunaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyin thunaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">neerai pola nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerai pola nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">illatha vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha vaalvile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasau theeyil vegum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasau theeyil vegum"/>
</div>
<div class="lyrico-lyrics-wrapper">irulin oliyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulin oliyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madai thiranthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madai thiranthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu vidha anubavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu vidha anubavamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malai veyil paniyil kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai veyil paniyil kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vasantham undagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasantham undagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malai veyil paniyil kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai veyil paniyil kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vasantham undagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasantham undagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mathi vidhi valiyil ularnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi vidhi valiyil ularnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakkam kondadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakkam kondadumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madai thiranthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madai thiranthidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu vidha anubavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu vidha anubavamo"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaakale manam thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaakale manam thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">manam thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">vilakale kanaakalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilakale kanaakalil"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaakale manam thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaakale manam thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">vinaakale manam thanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinaakale manam thanil"/>
</div>
<div class="lyrico-lyrics-wrapper">vilakale kanaakalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilakale kanaakalil"/>
</div>
<div class="lyrico-lyrics-wrapper">vilakale kanaakalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilakale kanaakalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidai ithu sariyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai ithu sariyo"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaguvathum elitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaguvathum elitho"/>
</div>
</pre>
