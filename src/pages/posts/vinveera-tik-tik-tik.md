---
title: "vinveera song lyrics"
album: "Tik Tik Tik"
artist: "D. Imman"
lyricist: "Madhan Karky"
director: "Shakti Soundar Rajan"
path: "/albums/tik-tik-tik-lyrics"
song: "Vinveera"
image: ../../images/albumart/tik-tik-tik.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/amXrHPoRpKE"
type: "mass"
singers:
  - Ranjith
  - Sri Rascol
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vaazhvin saavin maiya kodadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu vaazhvin saavin maiya kodadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan meedhu yeri aattam podadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan meedhu yeri aattam podadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum mudiyum sindhithu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum mudiyum sindhithu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhitha sindhanai nadathi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhitha sindhanai nadathi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mel nambikkai vaithu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mel nambikkai vaithu paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa Vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa Vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum mudiyum sindhithu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum mudiyum sindhithu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhitha sindhanai nadathi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhitha sindhanai nadathi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhanai padaikka ezhundhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanai padaikka ezhundhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhaa. thamizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhaa. thamizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhaa thamizhaa thamizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhaa thamizhaa thamizhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhaa thamizhaa thamizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhaa thamizhaa thamizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyaadhenraal mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaadhenraal mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum idhayam adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum idhayam adaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulin meedhu etti udhaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulin meedhu etti udhaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaa vaanum vidiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaa vaanum vidiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eriyaadhendraal eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyaadhendraal eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Latcham latchiya thiriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham latchiya thiriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam utru paarthaldhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam utru paarthaldhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un theeyin aazham puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theeyin aazham puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theivadhu theivadhu ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theivadhu theivadhu ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pudhidhaai maaridathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pudhidhaai maaridathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhvadhu veezhvadhu ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhvadhu veezhvadhu ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vinnil yeridathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vinnil yeridathaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vaazhvin saavin maiya kodadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu vaazhvin saavin maiya kodadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan meedhu yeri aattam podadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan meedhu yeri aattam podadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru jeyippom muyarchi kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru jeyippom muyarchi kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippukku palan nichayam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippukku palan nichayam undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruthaadhae poraadu vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niruthaadhae poraadu vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhanai padaikka ezhundhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanai padaikka ezhundhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhaa thamizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhaa thamizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodiyinil yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyinil yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraadhae maaraadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraadhae maaraadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poaraadu nee niruthaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poaraadu nee niruthaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali enum ondru illaamal naanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali enum ondru illaamal naanillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyillai ye marakkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyillai ye marakkaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhaai vizhundhu ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhaai vizhundhu ezhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjam indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjam indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanin uyaram alakkudhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin uyaram alakkudhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruthathae vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niruthathae vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandraai urundu thiranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandraai urundu thiranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un tholgal indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholgal indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanin agalam alakkudhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin agalam alakkudhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vaazhvin saavin maiya kodadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu vaazhvin saavin maiya kodadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan meedhu yeri aattam podadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan meedhu yeri aattam podadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetri adaiya edhu thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri adaiya edhu thaduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padai eduthu nee thadai udaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai eduthu nee thadai udaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Va va vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va vinveeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muyarchi eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchi eduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhanai padaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanai padaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va veeraa nee va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va veeraa nee va"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyaa sadhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyaa sadhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadi velluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadi velluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va va vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthidu mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthidu mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal mudiyum saathithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal mudiyum saathithidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam unnai thadukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam unnai thadukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduthu paarkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduthu paarkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarthae sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarthae sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikka sirikkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka sirikkattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan seyalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan seyalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum pozhudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum pozhudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorin vaayai adaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorin vaayai adaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei kuzhappam ellaam maraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kuzhappam ellaam maraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviya pulli theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviya pulli theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaakkai rekkai kondaaldhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaakkai rekkai kondaaldhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai innum viriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai innum viriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vaazhvin saavin maiya kodadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu vaazhvin saavin maiya kodadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinveeraa vinveeraa vinveeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveeraa vinveeraa vinveeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan meedhu yeri aattam podadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan meedhu yeri aattam podadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Here we go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Here we go"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchi udaiyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchi udaiyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Igazhchi adaiyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Igazhchi adaiyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayakkam vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkam vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkam vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyai sidhaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyai sidhaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal udaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal udaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai ezhuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai ezhuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhanai padaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhanai padaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayakkam vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkam vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkam vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyai sidhaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyai sidhaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal udaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal udaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai ezhuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai ezhuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnokki selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnokki selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyai vezhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyai vezhvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri adaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri adaivom"/>
</div>
</pre>
