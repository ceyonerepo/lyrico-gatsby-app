---
title: "inka ennaallu song lyrics"
album: "Drushyam 2"
artist: "Anil Johnson"
lyricist: "Chandrabose"
director: "Jeethu Joseph"
path: "/albums/drushyam-2-lyrics"
song: "Inka Ennaallu"
image: ../../images/albumart/drushyam-2.jpg
date: 2021-11-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JYfS7W9Q6FE"
type: "melody"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enno kalalu kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno kalalu kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni kalathalena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni kalathalena"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu veluthurunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu veluthurunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo cheekatena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo cheekatena"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka ennallo kannilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka ennallo kannilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka yennello bayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka yennello bayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai mugisena ekanthalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai mugisena ekanthalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhi nijamo edhi mayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhi nijamo edhi mayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhi pagalo edhi ratro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhi pagalo edhi ratro"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyakunda brathukuthunnanila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyakunda brathukuthunnanila"/>
</div>
<div class="lyrico-lyrics-wrapper">Alajadulalo alasi poyanila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alajadulalo alasi poyanila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo nene karuguthunna nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo nene karuguthunna nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene aduguthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene aduguthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka ennallo gayalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka ennallo gayalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka ennello gandalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka ennello gandalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai kathakepudu sukhanthalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai kathakepudu sukhanthalu"/>
</div>
</pre>
