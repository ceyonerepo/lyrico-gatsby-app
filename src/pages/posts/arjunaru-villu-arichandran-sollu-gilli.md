---
title: "arjunaru villu arichandran sollu song lyrics"
album: "Gilli"
artist: "Vidyasagar"
lyricist: "Kabilan"
director: "Dharani"
path: "/albums/gilli-lyrics"
song: "Arjunaru Villu Arichandran Sollu"
image: ../../images/albumart/gilli.jpg
date: 2004-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_c2cpeGPCmo"
type: "mass"
singers:
  - Sukhwinder Singh
  - Manikka Vinayagam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arjunaru Villu Arichandran Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arjunaru Villu Arichandran Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanoda Dhillu Poikaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanoda Dhillu Poikaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriya Kollu Imayathai Vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriya Kollu Imayathai Vellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkoru Ellai Kidayaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkoru Ellai Kidayaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Yaarivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaarivano"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Neero Theeyo Yaar Arivaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Neero Theeyo Yaar Arivaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalum Therivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalum Therivano"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Asaithu Paarka Yaar Varuvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Asaithu Paarka Yaar Varuvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arjunaru Villu Arichandran Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arjunaru Villu Arichandran Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriya Kollu Imayathai Vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriya Kollu Imayathai Vellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjuvathu Madam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjuvathu Madam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Dhina Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Dhina Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjuvathu Thidaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjuvathu Thidaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinakku Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinakku Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anju Viral Thodume Aagaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju Viral Thodume Aagaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettividu Vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettividu Vinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Dhina Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Dhina Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethi Vidu Unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi Vidu Unai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinakku Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinakku Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaya Thunaiye Mundhaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaya Thunaiye Mundhaanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Oru Adhisaya Puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Oru Adhisaya Puli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Iruvathu Nagam Adhu Uzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Iruvathu Nagam Adhu Uzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Arindhidum Paghavanin Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Arindhidum Paghavanin Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani Oru Manithanin Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Oru Manithanin Padai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Ezhuvathu Irupathu Vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Ezhuvathu Irupathu Vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Mazhai Veyil Irandukkum Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Mazhai Veyil Irandukkum Kudai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeru Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Karaye Illa Kaataaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Karaye Illa Kaataaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Munnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Munnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vetri Enbathu Kan Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vetri Enbathu Kan Koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arjunaru Villu Arichandran Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arjunaru Villu Arichandran Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriya Kollu Imayathai Vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriya Kollu Imayathai Vellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelema Yele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelema Yele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelema Yele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelema Yele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelama Yele Yele Loo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelama Yele Yele Loo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelema Yele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelema Yele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelema Yele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelema Yele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelama Yele Yele Yele Loo Loo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelama Yele Yele Yele Loo Loo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devedhayin Ragham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devedhayin Ragham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thina Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thina Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilavu Mugham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavu Mugham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinakku Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinakku Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodiyathu Yeno Kaarmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodiyathu Yeno Kaarmegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedal Oru Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal Oru Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thina Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thina Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodal Oru Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodal Oru Kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinakku Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinakku Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Iru Kangal Sugamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Iru Kangal Sugamaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya Thaai Mozhi Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Thaai Mozhi Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Sirikkayil Iravugal Pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Sirikkayil Iravugal Pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ivalukku Ivale Nagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ivalukku Ivale Nagal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaghiya Mezhugena Udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaghiya Mezhugena Udal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhiyinil Etharkadi Kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhiyinil Etharkadi Kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Thudaippathu Ivanathu Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thudaippathu Ivanathu Viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeru Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru Munneru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Karaye Illa Kaataaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Karaye Illa Kaataaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Munnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Munnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vetri Enbathu Kan Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vetri Enbathu Kan Koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arjunaru Villu Arichandran Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arjunaru Villu Arichandran Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriya Kollu Imayathai Vellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriya Kollu Imayathai Vellu"/>
</div>
</pre>
