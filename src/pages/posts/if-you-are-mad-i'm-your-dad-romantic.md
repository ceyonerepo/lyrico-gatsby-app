---
title: "if you are mad i'm your dad song lyrics"
album: "Romantic"
artist: "Sunil Kasyap"
lyricist: "Bhaskarabahtla"
director: "Anil Paduri"
path: "/albums/romantic-lyrics"
song: "If You Are Mad I'm Your Dad"
image: ../../images/albumart/romantic.jpg
date: 2021-10-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/iapR39laFTQ"
type: "happy"
singers:
  - Yazin Nizar
  - Ashwin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Em mayo chesavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em mayo chesavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ededo ayipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ededo ayipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthune chusthune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthune chusthune"/>
</div>
<div class="lyrico-lyrics-wrapper">Mikamlo very padi poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mikamlo very padi poya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanu reppala chappudu vinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanu reppala chappudu vinte"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha chappudu agindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha chappudu agindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalladi pothunde praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalladi pothunde praanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Em mayo chesavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em mayo chesavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ededo ayipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ededo ayipoya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If you are mad i’m your dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you are mad i’m your dad"/>
</div>
<div class="lyrico-lyrics-wrapper">If you are mad i’m your dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you are mad i’m your dad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavullo thiyyadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavullo thiyyadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddulalo pondgalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddulalo pondgalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikalo unna balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikalo unna balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougililo lone chudagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougililo lone chudagalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okaroju apagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaroju apagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendrojulu apagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendrojulu apagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu adigithe vechadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu adigithe vechadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaraina em cheyagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaraina em cheyagalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If you are mad i’m your dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you are mad i’m your dad"/>
</div>
<div class="lyrico-lyrics-wrapper">If you are mad i’m your dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you are mad i’m your dad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">E paruvam poola ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E paruvam poola ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve naa uthapadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa uthapadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvevaravu nenevaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvevaravu nenevaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Okariki okaram hasthagatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okariki okaram hasthagatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaha tahala nippu kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaha tahala nippu kanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadaladule okka kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadaladule okka kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandinchuta gaali gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandinchuta gaali gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaku asalu undadhu jaali gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaku asalu undadhu jaali gunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If you are mad i’m your dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you are mad i’m your dad"/>
</div>
<div class="lyrico-lyrics-wrapper">If you are mad i’m your dad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you are mad i’m your dad"/>
</div>
</pre>
