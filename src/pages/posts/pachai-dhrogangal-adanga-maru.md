---
title: "pachai dhrogangal song lyrics"
album: "Adanga Maru"
artist: "Sam CS"
lyricist: "Yuga Bharathi"
director: "Karthik Thangavel"
path: "/albums/adanga-maru-lyrics"
song: "Pachai Dhrogangal"
image: ../../images/albumart/adanga-maru.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UaEETikJvTA"
type: "mass"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pachai dhrogangal saagamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai dhrogangal saagamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallai izhikkum naal thoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallai izhikkum naal thoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etchil sor unnum kaagam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etchil sor unnum kaagam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum nilayae setharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum nilayae setharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram engendru thedaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram engendru thedaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam uranga kudathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam uranga kudathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham nee endru aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham nee endru aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangi odangi vazhaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi odangi vazhaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullavai mothamum thaavendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullavai mothamum thaavendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaigalum peigalum vaalaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaigalum peigalum vaalaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathai nambi nee por seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathai nambi nee por seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharunam tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharunam tharunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjamum nanjamum kai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjamum nanjamum kai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadagam oduthae oorellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam oduthae oorellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam ondru thaan un kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam ondru thaan un kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Gavanam gavanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gavanam gavanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah ooraiyum peraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah ooraiyum peraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga vaa porattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga vaa porattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemayai theeyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemayai theeyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podavae aarpattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podavae aarpattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valainthu nelinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valainthu nelinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidakkum varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidakkum varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakku vazhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku vazhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nimirnthu ezhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirnthu ezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthaikka thodangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthaikka thodangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etharkkum bayanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etharkkum bayanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaiyum thollaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaiyum thollaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koot endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koot endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooni nee poovathu theervillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooni nee poovathu theervillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrinam kondu nee porittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrinam kondu nee porittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyum jagammae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyum jagammae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaiyum mannaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaiyum mannaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhavae yenginal theengellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhavae yenginal theengellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyum mannilae saikindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum mannilae saikindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamai varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamai varumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutram engedru thedaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram engedru thedaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam uranga kudathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam uranga kudathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham nee endru aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham nee endru aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangi odangi vazhathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi odangi vazhathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tadat dadata daa dat aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadat dadata daa dat aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadat dadata daa dat aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadat dadata daa dat aa"/>
</div>
</pre>
