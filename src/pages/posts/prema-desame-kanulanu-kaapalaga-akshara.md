---
title: "prema desame song lyrics"
album: "Akshara"
artist: "Suresh Bobbili"
lyricist: "Balaji"
director: "B. Chinni Krishna"
path: "/albums/akshara-lyrics"
song: "Prema Desame - Kanulanu Kaapalaga"
image: ../../images/albumart/akshara.jpg
date: 2021-02-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9z6Tvxc8JzA"
type: "love"
singers:
  - Anudeep Dev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanulanu kaapalaga unchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanulanu kaapalaga unchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee peru prema desama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee peru prema desama"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuku thalupulanni therichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuku thalupulanni therichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee premakintha andhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee premakintha andhama"/>
</div>
<div class="lyrico-lyrics-wrapper">pedhavulu kadipithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedhavulu kadipithe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee peru paatala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee peru paatala"/>
</div>
<div class="lyrico-lyrics-wrapper">adugulu aagithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugulu aagithe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee prema chotula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee prema chotula"/>
</div>
<div class="lyrico-lyrics-wrapper">nee jada puvvulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jada puvvulo"/>
</div>
<div class="lyrico-lyrics-wrapper">thummedha la ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thummedha la ila"/>
</div>
<div class="lyrico-lyrics-wrapper">marchesavule nanne nuvvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marchesavule nanne nuvvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">naloni kalalakinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naloni kalalakinka"/>
</div>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">gundello challukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundello challukuntu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">naloni kalalakinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naloni kalalakinka"/>
</div>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">kasthantha dhorakavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasthantha dhorakavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seconu ki oosari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seconu ki oosari"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mata thalachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mata thalachina"/>
</div>
<div class="lyrico-lyrics-wrapper">miguluthu untayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miguluthu untayi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee meedha oohale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee meedha oohale"/>
</div>
<div class="lyrico-lyrics-wrapper">chusina prathisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusina prathisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">andhalu kotthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhalu kotthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kanabadthuntayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanabadthuntayi"/>
</div>
<div class="lyrico-lyrics-wrapper">meetuthu kannuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meetuthu kannuley"/>
</div>
<div class="lyrico-lyrics-wrapper">neetho paatunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neetho paatunte"/>
</div>
<div class="lyrico-lyrics-wrapper">gadichena ee kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadichena ee kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">venakey paduthundi needala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venakey paduthundi needala"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve thodunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve thodunte"/>
</div>
<div class="lyrico-lyrics-wrapper">nakenduku ee lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakenduku ee lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">nedey gadipesthaa janmila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedey gadipesthaa janmila "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">neeloni kulukulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeloni kulukulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">gundello challukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundello challukuntu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">neeloni kulukulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeloni kulukulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">kasthantha dhorakavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasthantha dhorakavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey thadisina paadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thadisina paadhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee nela moputhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee nela moputhu"/>
</div>
<div class="lyrico-lyrics-wrapper">adugulu vesthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugulu vesthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">jaadalu puvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaadalu puvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">podi podi mataina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi mataina"/>
</div>
<div class="lyrico-lyrics-wrapper">nee noru jaarithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee noru jaarithe"/>
</div>
<div class="lyrico-lyrics-wrapper">thenela baanalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenela baanalai"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelu thakeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelu thakeney"/>
</div>
<div class="lyrico-lyrics-wrapper">nadume arasunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadume arasunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nelavanke sariponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelavanke sariponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakalu dheevilona hamsaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakalu dheevilona hamsaley"/>
</div>
<div class="lyrico-lyrics-wrapper">merise merupaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merise merupaina"/>
</div>
<div class="lyrico-lyrics-wrapper">vankaraga untundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vankaraga untundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">niluvagu nee sogasunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niluvagu nee sogasunu"/>
</div>
<div class="lyrico-lyrics-wrapper">minchadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minchadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">neeloni kulukulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeloni kulukulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">gundello challukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundello challukuntu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">neeloni kulukulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeloni kulukulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">naa na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">kasthantha dhorakavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasthantha dhorakavey"/>
</div>
</pre>
