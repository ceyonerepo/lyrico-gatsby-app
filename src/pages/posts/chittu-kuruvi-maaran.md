---
title: "chittu kuruvi song lyrics"
album: "Maaran"
artist: "GV Prakash Kumar"
lyricist: "Dhanush"
director: "Karthick Naren"
path: "/albums/maaran-lyrics"
song: "Chittu Kuruvi"
image: ../../images/albumart/maaran.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dCQX44nx_4U"
type: "melody"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar aditharo kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar aditharo kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen indha mounam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen indha mounam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaigal solla naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigal solla naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal nindru pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal nindru pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal medhuvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal medhuvai "/>
</div>
<div class="lyrico-lyrics-wrapper">medhuvai moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medhuvai moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha irulil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha irulil "/>
</div>
<div class="lyrico-lyrics-wrapper">amaidhi thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amaidhi thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaandi kaetkum kural enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi kaetkum kural enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendi killum viral enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendi killum viral enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam kalaikum kurumbu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam kalaikum kurumbu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal pidikkum karam enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal pidikkum karam enge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidindhum irulaai vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidindhum irulaai vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idindha suvaraai naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idindha suvaraai naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhiram unaiye thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhiram unaiye thaedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvam tholaithai neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam tholaithai neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum pirandhu nee vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum pirandhu nee vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar aditharo kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar aditharo kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen indha mounam chellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen indha mounam chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaigal solla naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigal solla naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal nindru pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal nindru pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal medhuvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal medhuvai "/>
</div>
<div class="lyrico-lyrics-wrapper">medhuvai moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medhuvai moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha irulil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha irulil "/>
</div>
<div class="lyrico-lyrics-wrapper">amaidhi thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amaidhi thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi"/>
</div>
</pre>
