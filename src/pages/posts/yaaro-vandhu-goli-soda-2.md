---
title: "yaaro vandhu song lyrics"
album: "Goli Soda 2"
artist: "Achu Rajamani"
lyricist: "Mani Amuthavan"
director: "Vijay Milton"
path: "/albums/goli-soda-2-lyrics"
song: "Yaaro Vandhu"
image: ../../images/albumart/goli-soda-2.jpg
date: 2018-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/t-qeSvaGI3g"
type: "sad"
singers:
  - Achu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaaro vanthu athigaaram seiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaro vanthu athigaaram seiya "/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalkai enathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkai enathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">evano vanthu aethetho solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evano vanthu aethetho solla"/>
</div>
<div class="lyrico-lyrics-wrapper">enakendru unarvu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakendru unarvu illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaaro vanthu athigaaram seiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaro vanthu athigaaram seiya "/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalkai enathu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkai enathu illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">evano vanthu aethetho solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evano vanthu aethetho solla"/>
</div>
<div class="lyrico-lyrics-wrapper">enakendru unarvu illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakendru unarvu illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nimirthal thalai meethu kallai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirthal thalai meethu kallai"/>
</div>
<div class="lyrico-lyrics-wrapper">nadanthaal vali engum mullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadanthaal vali engum mullai"/>
</div>
<div class="lyrico-lyrics-wrapper">vithaithaal viratti adithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithaithaal viratti adithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">enna solla engu sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna solla engu sella"/>
</div>
<div class="lyrico-lyrics-wrapper">thondangum munne mudikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondangum munne mudikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thondai kuliyai nerikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondai kuliyai nerikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">irupathu ellam parikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupathu ellam parikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">irakkam indri vathaikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakkam indri vathaikuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thondangum munne mudikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondangum munne mudikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thondai kuliyai nerikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondai kuliyai nerikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">irupathu ellam parikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupathu ellam parikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">irakkam indri vathaikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakkam indri vathaikuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharumangal thalaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumangal thalaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugangal killuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugangal killuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal sari ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal sari ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagankal solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagankal solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharumangal thalaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumangal thalaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugangal killuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugangal killuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal sari ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal sari ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagankal solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagankal solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharumangal thalaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumangal thalaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugangal killuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugangal killuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal sari ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal sari ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagankal solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagankal solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharumangal thalaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumangal thalaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugangal killuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugangal killuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal sari ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal sari ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagankal solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagankal solluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tharumangal thalaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumangal thalaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugangal killuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugangal killuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thavarugal sari ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavarugal sari ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagankal solluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagankal solluthe"/>
</div>
</pre>
