---
title: "sara sara saara kathu song lyrics"
album: "Vaagai Sooda Vaa"
artist: "Ghibran"
lyricist: "Vairamuthu"
director: "A. Sarkunam"
path: "/albums/vaagai-sooda-vaa-lyrics"
song: "Sara Sara Saara Kathu"
image: ../../images/albumart/vaagai-sooda-vaa.jpg
date: 2011-09-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Y7ZHkpRcshQ"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sara sara saara kaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara saara kaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir ah paathu pesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ah paathu pesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">satham poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham poduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sara saara kaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara saara kaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir ah paathu pesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ah paathu pesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">satham poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham poduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu ithu ithu pona nenju thaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu ithu ithu pona nenju thaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha paarava paathu sellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha paarava paathu sellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha sotha eluthi tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha sotha eluthi tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu utpada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu utpada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu ithu ithu pona nenju thaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu ithu ithu pona nenju thaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha paarava paathu sellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha paarava paathu sellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha sotha eluthi tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha sotha eluthi tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu utpada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu utpada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea pola nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea pola nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna yean aathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna yean aathura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara saara kaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara saara kaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir ah paathu pesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ah paathu pesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">satham poduthae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham poduthae.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru pudikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru pudikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thanni inikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thanni inikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi varum kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi varum kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">sutta eeral manakutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutta eeral manakutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutta mozhi pudikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutta mozhi pudikava"/>
</div>
<div class="lyrico-lyrics-wrapper">munnu padi samaikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnu padi samaikava"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbhugal kadaikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbhugal kadaikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">enna konja ninaika vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna konja ninaika vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamansooru rusika va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamansooru rusika va"/>
</div>
<div class="lyrico-lyrics-wrapper">samaicha kaiya konjam rasikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaicha kaiya konjam rasikava"/>
</div>
<div class="lyrico-lyrics-wrapper">Modakatha rasam vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modakatha rasam vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">madakathan paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madakathan paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Rettai dosai suttu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai dosai suttu "/>
</div>
<div class="lyrico-lyrics-wrapper">vachu kaava kaakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu kaava kaakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukannu noongu naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukannu noongu naan "/>
</div>
<div class="lyrico-lyrics-wrapper">nikirenMandu ne kangam yen kekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikirenMandu ne kangam yen kekura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara saara kaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara saara kaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir ah paathu pesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ah paathu pesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">satham poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham poduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullu kattu vaasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu kattu vaasama"/>
</div>
<div class="lyrico-lyrics-wrapper">puthikkulla veesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthikkulla veesura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatu mani sathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu mani sathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukul kekura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukul kekura"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta vandi ottura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta vandi ottura"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyalavu manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyalavu manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyeluthu podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyeluthu podura"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni ponnu maarbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni ponnu maarbula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu naala paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu naala paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">ooril entha poovum pookala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooril entha poovum pookala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatu kallu kuliyila orangi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatu kallu kuliyila orangi "/>
</div>
<div class="lyrico-lyrics-wrapper">pogum poonaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum poonaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu vanthu pathuthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu vanthu pathuthan "/>
</div>
<div class="lyrico-lyrics-wrapper">kirangi porenya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kirangi porenya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenuku engura kokku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenuku engura kokku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothavae theriyala makku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothavae theriyala makku nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara saara kaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara saara kaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir ah paathu pesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ah paathu pesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">satham poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham poduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara saara kaathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara saara kaathu "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir ah paathu pesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ah paathu pesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saara paambu pola nenjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saara paambu pola nenjam "/>
</div>
<div class="lyrico-lyrics-wrapper">satham poduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham poduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu ithu ithu pona nenju thaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu ithu ithu pona nenju thaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha paarava paathu sellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha paarava paathu sellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha sotha eluthi tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha sotha eluthi tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu utpada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu utpada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu ithu ithu pona nenju thaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu ithu ithu pona nenju thaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha paarava paathu sellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha paarava paathu sellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha sotha eluthi tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha sotha eluthi tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu utpada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu utpada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tea pola nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea pola nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna yean aathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna yean aathura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu malliga poothirukkudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu malliga poothirukkudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhala kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhala kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu vanthu odipogum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu vanthu odipogum "/>
</div>
<div class="lyrico-lyrics-wrapper">vandukena kaachala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandukena kaachala"/>
</div>
</pre>
