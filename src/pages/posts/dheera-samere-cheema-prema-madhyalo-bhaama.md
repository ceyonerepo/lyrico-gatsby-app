---
title: "dheera samere song lyrics"
album: "Cheema Prema Madhyalo Bhaama"
artist: "Ravi Varma Potedar"
lyricist: "Mukkamala"
director: "Srikanth “Sri” Appalaraju"
path: "/albums/cheema-prema-madhyalo-bhaama-lyrics"
song: "Dheera Samere"
image: ../../images/albumart/cheema-prema-madhyalo-bhaama.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FUT60H0MUnw"
type: "love"
singers:
  - Saandip
  - Swetha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dheera Sameere Yamuna Theere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera Sameere Yamuna Theere"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasathivane Vanamaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasathivane Vanamaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey I love You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey I love You"/>
</div>
<div class="lyrico-lyrics-wrapper">I con’t Live Without You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I con’t Live Without You"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaina Chesthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaina Chesthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Im Here For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Here For You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey  I Miss You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey  I Miss You"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m In Love With You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m In Love With You"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendaakaina Vasthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaakaina Vasthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Any Time For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Any Time For You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Naatho Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Naatho Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sky Paike Velloddaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky Paike Velloddaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Stars Annee Docheddaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stars Annee Docheddaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulatho Ille Katteddaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulatho Ille Katteddaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Neenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Neenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Life Gadipeddaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Life Gadipeddaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Mari Nee Maate Vintunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Mari Nee Maate Vintunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakentho Bagundee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakentho Bagundee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Jathagaa Neevunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Jathagaa Neevunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey I love You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey I love You"/>
</div>
<div class="lyrico-lyrics-wrapper">I con’t Live Without You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I con’t Live Without You"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaina Chesthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaina Chesthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Im Here For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Here For You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheera Sameere Yamuna Theere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera Sameere Yamuna Theere"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasathivane Vanamaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasathivane Vanamaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rain Deopai Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rain Deopai Nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Touch Cheste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Touch Cheste"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachani Pairalle Pulakinthavuthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachani Pairalle Pulakinthavuthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chali Gaalai Nuvve Nanne Thadimesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali Gaalai Nuvve Nanne Thadimesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">wind Chimes Alle Neetho Allari Chesthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wind Chimes Alle Neetho Allari Chesthaanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunday Monday Gurthe Raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday Monday Gurthe Raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Moon Ni choostunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Moon Ni choostunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Vuntunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Vuntunde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Heartu Beatemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Heartu Beatemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Periginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Periginde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallallo Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Screen Ayyinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Screen Ayyinde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sare Padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sare Padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Waiting Deniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting Deniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam ane Kotha lokaaanniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam ane Kotha lokaaanniki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey I love You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey I love You"/>
</div>
<div class="lyrico-lyrics-wrapper">I con’t Live Without You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I con’t Live Without You"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaina Chesthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaina Chesthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Im Here For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Here For You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey I Miss You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey I Miss You"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m In Love With You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m In Love With You"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendaakaina Vasthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendaakaina Vasthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Any Time For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Any Time For You"/>
</div>
</pre>
