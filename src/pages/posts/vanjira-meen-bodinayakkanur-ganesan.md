---
title: "vanjira meen song lyrics"
album: "Bodinayakkanur Ganesan"
artist: "John Peter"
lyricist: "Nandalala"
director: "O. Gnanam"
path: "/albums/bodinayakkanur-ganesan-lyrics"
song: "Vanjira Meen"
image: ../../images/albumart/bodinayakkanur-ganesan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Ranjith
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">manja nambithundirukku mukkoanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja nambithundirukku mukkoanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha maaman vandhu saappidavaa maththiyaanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha maaman vandhu saappidavaa maththiyaanamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchiveiyil megathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchiveiyil megathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchi veiyil nerathula naan venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi veiyil nerathula naan venumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjarameen thondirukku mokkoanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjarameen thondirukku mokkoanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha naama vandhu saappidavaa mathiyaanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha naama vandhu saappidavaa mathiyaanamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchiveiyil megathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchiveiyil megathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchi veiyil nerathula naan venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi veiyil nerathula naan venumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaney thannanannaaney thaaney thannanaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney thannanannaaney thaaney thannanaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi sappukkottithaan naan thinnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi sappukkottithaan naan thinnen"/>
</div>
<div class="lyrico-lyrics-wrapper">udhattu menu rusi enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhattu menu rusi enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochi mutta moochi mutta verundhuttu vaikkattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi mutta moochi mutta verundhuttu vaikkattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullangaalu sippi menu kendakkaalu veraa menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaalu sippi menu kendakkaalu veraa menu"/>
</div>
<div class="lyrico-lyrics-wrapper">iduppu madippu keluthi menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iduppu madippu keluthi menu"/>
</div>
<div class="lyrico-lyrics-wrapper">achacho achacho achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achacho achacho achacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhuthappaarkkum soraa menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthappaarkkum soraa menu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannam thinnum valvaa menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannam thinnum valvaa menu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaalukkonjam neenda menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaalukkonjam neenda menu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manasu aaraa menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu aaraa menu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ommanasu paarai menu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ommanasu paarai menu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjarameen thondirukku mokkoanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjarameen thondirukku mokkoanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha naama vandhu saappidavaa mathiyaanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha naama vandhu saappidavaa mathiyaanamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannanannaaney thaaney thannaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanannaaney thaaney thannaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavuchi thedi ninnaalaiyaa onnathedi minjalaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavuchi thedi ninnaalaiyaa onnathedi minjalaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">menu nenjum poanavazhi kaanala kaanala kaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="menu nenjum poanavazhi kaanala kaanala kaanala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli Aatril kulicha un azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Aatril kulicha un azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">uththuppaartha vellikkenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uththuppaartha vellikkenda"/>
</div>
<div class="lyrico-lyrics-wrapper">seththappiragum kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seththappiragum kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">moodala moodala moodala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodala moodala moodala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannikkulla meenirundhaa thaagamvarumaa vikkalvarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannikkulla meenirundhaa thaagamvarumaa vikkalvarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkippoachu manasu sokkippoachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkippoachu manasu sokkippoachu"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku vikkalum thanniyum neeyadi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku vikkalum thanniyum neeyadi neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjarameen thondirukku mokkoanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjarameen thondirukku mokkoanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adha naama vandhu saappidavaa mathiyaanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha naama vandhu saappidavaa mathiyaanamaa"/>
</div>
</pre>
