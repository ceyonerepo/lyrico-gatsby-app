---
title: "navakali amma song lyrics"
album: "IPC 376"
artist: "Yaadhav Ramalinkgam"
lyricist: "Rakendu Mouli"
director: "Ramkumar Subbaraman"
path: "/albums/ipc-376-lyrics"
song: "Navakali Amma"
image: ../../images/albumart/ipc-376.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1k7vvwPmDoM"
type: "mass"
singers:
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">simhaasini sura poojini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simhaasini sura poojini"/>
</div>
<div class="lyrico-lyrics-wrapper">sree chakra sanchaarini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sree chakra sanchaarini"/>
</div>
<div class="lyrico-lyrics-wrapper">jaya bhadra kali raavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaya bhadra kali raavey"/>
</div>
<div class="lyrico-lyrics-wrapper">paramesuni maharaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paramesuni maharaani"/>
</div>
<div class="lyrico-lyrics-wrapper">mahishaasura mardhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahishaasura mardhini"/>
</div>
<div class="lyrico-lyrics-wrapper">sajjanula gaachey thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sajjanula gaachey thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhi shakthivaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhi shakthivaina"/>
</div>
<div class="lyrico-lyrics-wrapper">jagadambikaa lokaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagadambikaa lokaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaacheti moogaambikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaacheti moogaambikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">karuninchi arudhenchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuninchi arudhenchi"/>
</div>
<div class="lyrico-lyrics-wrapper">lokaalu kaapaadamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokaalu kaapaadamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">navakali amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navakali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">jayakaali amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayakaali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">mammelu maa kalive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammelu maa kalive"/>
</div>
<div class="lyrico-lyrics-wrapper">varakali amma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varakali amma "/>
</div>
<div class="lyrico-lyrics-wrapper">ranakali amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranakali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">mamu gaachu mahankalive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamu gaachu mahankalive"/>
</div>
<div class="lyrico-lyrics-wrapper">omkaari sreemkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="omkaari sreemkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">jayakaarivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayakaarivey"/>
</div>
<div class="lyrico-lyrics-wrapper">sukumaari lokesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sukumaari lokesu"/>
</div>
<div class="lyrico-lyrics-wrapper">samraaginivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samraaginivey"/>
</div>
<div class="lyrico-lyrics-wrapper">soolaanga kaalaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soolaanga kaalaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">darinthuvey kshudrulanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="darinthuvey kshudrulanu"/>
</div>
<div class="lyrico-lyrics-wrapper">khadathercha dayacheyavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khadathercha dayacheyavey"/>
</div>
<div class="lyrico-lyrics-wrapper">deepaalu veliginchaamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepaalu veliginchaamu"/>
</div>
<div class="lyrico-lyrics-wrapper">oo jayankari saapalni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo jayankari saapalni"/>
</div>
<div class="lyrico-lyrics-wrapper">harinchamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harinchamma "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">navakali amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navakali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">jayakaali amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayakaali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">mammelu maa kalive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mammelu maa kalive"/>
</div>
<div class="lyrico-lyrics-wrapper">varakali amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varakali amma"/>
</div>
<div class="lyrico-lyrics-wrapper">ranakli amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranakli amma"/>
</div>
<div class="lyrico-lyrics-wrapper">mamu gaachu mahankalive 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamu gaachu mahankalive "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvu soolam dharimpaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu soolam dharimpaga"/>
</div>
<div class="lyrico-lyrics-wrapper">parivaaram bedhiripoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parivaaram bedhiripoga"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhisesha chathramtho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhisesha chathramtho"/>
</div>
<div class="lyrico-lyrics-wrapper">chandi raavey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chandi raavey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sreechakra kaamakshivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sreechakra kaamakshivey"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu karunisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu karunisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">theerunu kasthamuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerunu kasthamuley"/>
</div>
<div class="lyrico-lyrics-wrapper">sreerudhra meenakshivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sreerudhra meenakshivey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee rakshuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee rakshuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">pothaayi maa vyadhaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothaayi maa vyadhaley"/>
</div>
<div class="lyrico-lyrics-wrapper">maa manavi manninchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa manavi manninchi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamaandhula pidinundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamaandhula pidinundi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapaadamma muguvalakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapaadamma muguvalakey"/>
</div>
<div class="lyrico-lyrics-wrapper">maharaani kannela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maharaani kannela"/>
</div>
<div class="lyrico-lyrics-wrapper">seelaalaki nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seelaalaki nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">rakshamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rakshamma"/>
</div>
<div class="lyrico-lyrics-wrapper">stree jaathi paikoche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stree jaathi paikoche"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">magajaathi paicheyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magajaathi paicheyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thaggaledhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaggaledhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nadhulaki aada perlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhulaki aada perlu"/>
</div>
<div class="lyrico-lyrics-wrapper">pettaaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pettaaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">mathithappi maalinyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathithappi maalinyam"/>
</div>
<div class="lyrico-lyrics-wrapper">chesaaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chesaaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">ugra chandi digiraavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ugra chandi digiraavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee karunatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee karunatho "/>
</div>
<div class="lyrico-lyrics-wrapper">sistulanu rakshinchamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sistulanu rakshinchamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhaga bhagamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhaga bhagamani"/>
</div>
<div class="lyrico-lyrics-wrapper">agni cherigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agni cherigi"/>
</div>
<div class="lyrico-lyrics-wrapper">aade kali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aade kali"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaanavasiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaanavasiram"/>
</div>
<div class="lyrico-lyrics-wrapper">narikey kali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narikey kali"/>
</div>
<div class="lyrico-lyrics-wrapper">mahan kali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahan kali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rudrakali rudhirakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rudrakali rudhirakali"/>
</div>
<div class="lyrico-lyrics-wrapper">chandikali chandrakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chandikali chandrakali"/>
</div>
<div class="lyrico-lyrics-wrapper">sadhyakali sidhakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadhyakali sidhakali"/>
</div>
<div class="lyrico-lyrics-wrapper">bhadrakali ugrakaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhadrakali ugrakaali"/>
</div>
<div class="lyrico-lyrics-wrapper">nyayamga jeevisthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nyayamga jeevisthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">ee magajaathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee magajaathi "/>
</div>
<div class="lyrico-lyrics-wrapper">aadavarni bathakaneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadavarni bathakaneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">mem mathuki maruroopamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mem mathuki maruroopamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aa drushti nundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa drushti nundi"/>
</div>
<div class="lyrico-lyrics-wrapper">mamu nuvu kaapaadamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamu nuvu kaapaadamma"/>
</div>
<div class="lyrico-lyrics-wrapper">durmaargam chelaregindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="durmaargam chelaregindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamukula roopamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamukula roopamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">narthisthondhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narthisthondhi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaache daivam karamulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaache daivam karamulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">khaidile bedeelu vesaaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khaidile bedeelu vesaaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">anyaayam aataade kaalamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anyaayam aataade kaalamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">adhikaaram veduka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhikaaram veduka "/>
</div>
<div class="lyrico-lyrics-wrapper">choosthondhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choosthondhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">athivalaku ee lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athivalaku ee lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">prathi koolamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prathi koolamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kannullo kanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannullo kanneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakaalamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakaalamma "/>
</div>
<div class="lyrico-lyrics-wrapper">akhilaanda paalini neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akhilaanda paalini neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">disalannee pekatilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="disalannee pekatilla "/>
</div>
<div class="lyrico-lyrics-wrapper">nuvuraavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvuraavamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">simhaasini sampoorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simhaasini sampoorani"/>
</div>
<div class="lyrico-lyrics-wrapper">samhaarini jayadhaarini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samhaarini jayadhaarini"/>
</div>
<div class="lyrico-lyrics-wrapper">avathaarini bhavathaarini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avathaarini bhavathaarini"/>
</div>
<div class="lyrico-lyrics-wrapper">omkaarini daakshaayini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="omkaarini daakshaayini"/>
</div>
</pre>
