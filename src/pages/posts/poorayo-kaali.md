---
title: "poorayo song lyrics"
album: "Kaali"
artist: "Vijay Antony"
lyricist: "Thamizhanangu"
director: "Kiruthiga Udhayanidhi"
path: "/albums/kaali-lyrics"
song: "Poorayo"
image: ../../images/albumart/kaali.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_JEvxQkdijU"
type: "sad"
singers:
  - Deepak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna paavam senjaaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paavam senjaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukaaga poranthaaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukaaga poranthaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu engae pooraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu engae pooraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venam intha ulagammunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam intha ulagammunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan inga manushanunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan inga manushanunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaama neeyum pooraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaama neeyum pooraiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraiyooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraiyooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta thunbam podhumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta thunbam podhumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai maari poganumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai maari poganumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi thedi kannae pooraiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi thedi kannae pooraiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaa aaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa aaaaaaaaaaaa"/>
</div>
</pre>
