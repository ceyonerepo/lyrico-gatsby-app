---
title: "kannaale kalalenno song lyrics"
album: "Mismatch"
artist: "Gifton Elias"
lyricist: "Dharma Teja"
director: "N V Nirmal Kumar"
path: "/albums/mismatch-lyrics"
song: "Kannaale Kalalenno"
image: ../../images/albumart/mismatch.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/X6iNm4Yc7Xo"
type: "sad"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannale kalalenno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale kalalenno "/>
</div>
<div class="lyrico-lyrics-wrapper">kanuchoopu nerallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanuchoopu nerallo"/>
</div>
<div class="lyrico-lyrics-wrapper">kalayikalu agavanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalayikalu agavanee"/>
</div>
<div class="lyrico-lyrics-wrapper">kanurapee thalathippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanurapee thalathippi"/>
</div>
<div class="lyrico-lyrics-wrapper">thanu thappukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanu thappukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">kannellu dhagavanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannellu dhagavanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pilupe ee gelupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pilupe ee gelupu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thalape o malupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thalape o malupu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuru padinae varini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuru padinae varini"/>
</div>
<div class="lyrico-lyrics-wrapper">adaganu badhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaganu badhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">okarikiokaru vidipadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okarikiokaru vidipadi"/>
</div>
<div class="lyrico-lyrics-wrapper">adugulu modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugulu modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gamyaalu maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamyaalu maare"/>
</div>
<div class="lyrico-lyrics-wrapper">payanaalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanaalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">mounnaala gaayaalainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounnaala gaayaalainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooraalu chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooraalu chuse"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanaalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanaalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraalu dharicherenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraalu dharicherenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gamyaalu maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamyaalu maare"/>
</div>
<div class="lyrico-lyrics-wrapper">payanaalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanaalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">mounnaala gaayaalainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounnaala gaayaalainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooraalu chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooraalu chuse"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanaalalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanaalalona"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraalu dharicherenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraalu dharicherenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannale kalalenno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale kalalenno "/>
</div>
<div class="lyrico-lyrics-wrapper">kanuchoopu nerallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanuchoopu nerallo"/>
</div>
<div class="lyrico-lyrics-wrapper">kalayikalu agavanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalayikalu agavanee"/>
</div>
<div class="lyrico-lyrics-wrapper">kanurapee thalathippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanurapee thalathippi"/>
</div>
<div class="lyrico-lyrics-wrapper">thanu thappukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanu thappukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">kannellu dhagavanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannellu dhagavanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anuvanuvuna alajadule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anuvanuvuna alajadule "/>
</div>
<div class="lyrico-lyrics-wrapper">reegenaa neelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reegenaa neelo"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaachesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaachesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kala chedirina kanulaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala chedirina kanulaku"/>
</div>
<div class="lyrico-lyrics-wrapper">thadi aareydhenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadi aareydhenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manase musugesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase musugesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kumile praanaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumile praanaala "/>
</div>
<div class="lyrico-lyrics-wrapper">eegunde kotha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eegunde kotha "/>
</div>
<div class="lyrico-lyrics-wrapper">evaraina theerchedhenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaraina theerchedhenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavani katha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavani katha "/>
</div>
<div class="lyrico-lyrics-wrapper">karagane vyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karagane vyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">tharaganidhani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaganidhani "/>
</div>
<div class="lyrico-lyrics-wrapper">theeranidhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeranidhani"/>
</div>
<div class="lyrico-lyrics-wrapper">epudaina teliseydhenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epudaina teliseydhenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhurupadina evarinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhurupadina evarinee"/>
</div>
<div class="lyrico-lyrics-wrapper">adaganu badhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaganu badhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">okarikiokaru vidipadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okarikiokaru vidipadi"/>
</div>
<div class="lyrico-lyrics-wrapper">adugulu modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugulu modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oorantha cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorantha cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">oregisthunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oregisthunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uluku paluku ledhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uluku paluku ledhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ashaa kiranaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ashaa kiranaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">udayisthuvunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udayisthuvunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasanthaa cheekatlenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasanthaa cheekatlenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhooraalu perigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooraalu perigi"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaaralu theguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaaralu theguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraalu maarevelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraalu maarevelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ey chethi raatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ey chethi raatho"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gunde kotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gunde kotha"/>
</div>
<div class="lyrico-lyrics-wrapper">moyaleni yaathanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moyaleni yaathanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee janme nekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee janme nekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee preme naa pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee preme naa pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">naa pranamlo praanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pranamlo praanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neevunde ee gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neevunde ee gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">pindese nee vanvanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pindese nee vanvanna"/>
</div>
<div class="lyrico-lyrics-wrapper">maataraani mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maataraani mounamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">shikhamulaku edigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shikhamulaku edigina"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi vijayaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi vijayaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodiki aanandamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodiki aanandamai"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu malachina nagakuvalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu malachina nagakuvalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanukalaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanukalaina "/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaina anubandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaina anubandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">santhoshaalanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhoshaalanni "/>
</div>
<div class="lyrico-lyrics-wrapper">minnantina vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnantina vela"/>
</div>
<div class="lyrico-lyrics-wrapper">kanumarugai anuraagamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanumarugai anuraagamey"/>
</div>
<div class="lyrico-lyrics-wrapper">saripadadani mudipadadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saripadadani mudipadadani"/>
</div>
<div class="lyrico-lyrics-wrapper">thudivarakidi maaranidani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudivarakidi maaranidani"/>
</div>
<div class="lyrico-lyrics-wrapper">kadasaariga selavadigeyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadasaariga selavadigeyna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhurupadina evarinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhurupadina evarinee"/>
</div>
<div class="lyrico-lyrics-wrapper">adaganu badhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaganu badhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">okarikiokaru vidipadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okarikiokaru vidipadi"/>
</div>
<div class="lyrico-lyrics-wrapper">adugulu modhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugulu modhalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oorantha cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorantha cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">oregisthunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oregisthunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">uluku paluku ledhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uluku paluku ledhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ashaa kiranaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ashaa kiranaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">udayisthuvunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udayisthuvunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasanthaa cheekatlenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasanthaa cheekatlenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhooraalu perigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooraalu perigi"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaaralu theguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaaralu theguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraalu maarevelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraalu maarevelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ey chethi raatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ey chethi raatho"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gunde kotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gunde kotha"/>
</div>
<div class="lyrico-lyrics-wrapper">moyaleni yaathanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moyaleni yaathanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee janme nekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee janme nekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee preme naa pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee preme naa pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">naa pranamlo praanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pranamlo praanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neevunde ee gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neevunde ee gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">pindese nee vanvanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pindese nee vanvanna"/>
</div>
<div class="lyrico-lyrics-wrapper">maataraani mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maataraani mounamaa"/>
</div>
</pre>
