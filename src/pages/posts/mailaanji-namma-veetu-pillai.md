---
title: "mailaanji lyrics"
album: "namma veetu pillai"
artist: "D Imman"
lyricist: "yugabharathi"
director: "pandiraj"
path: "/albums/namma-veetu-pillai-song-lyrics"
song: "mailaanji"
image: ../../images/albumart/namma-veetu-pillai.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZaYEi3ihc2A"
type: "love"
singers:
  - Pradeep Kumar
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayilanji Mayilanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilanji Mayilanji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamen Ohn Mayilanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamen Ohn Mayilanji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodum Kalodum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodum Kalodum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosendi Ena Aanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosendi Ena Aanji,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi Pola, Kadhal Unna Katta,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Pola, Kadhal Unna Katta,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eerezhu Logam, Paathu Nikkurein,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerezhu Logam, Paathu Nikkurein,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Neeyum, Noola Vittu Paakka,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Neeyum, Noola Vittu Paakka,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathadiyaga, Naanum Suthuren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadiyaga, Naanum Suthuren,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadha Sadha Santhoshamaguren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Sadha Santhoshamaguren,"/>
</div>
<div class="lyrico-lyrics-wrapper">Manokara Un Vasathal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manokara Un Vasathal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnala Nanum Nooraaguren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala Nanum Nooraaguren,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">[Instrumental Break]
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="[Instrumental Break]"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkuren Parakkuren, Therinjukadi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkuren Parakkuren, Therinjukadi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Naan Enakku Nee, Purinchukadi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Naan Enakku Nee, Purinchukadi,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayilanji Mayilanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilanji Mayilanji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Nee Mayilanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Nee Mayilanji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodum Kalodum, Poosendi Ena Aanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodum Kalodum, Poosendi Ena Aanji,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">[Instrumental Continue]
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="[Instrumental Continue]"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkuren Parakkuren, Therinjukadi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkuren Parakkuren, Therinjukadi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Naan Enakku Nee, Purinchukadi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Naan Enakku Nee, Purinchukadi,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyil Maniyosa, Kolusoda Kalanthu Pesa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil Maniyosa, Kolusoda Kalanthu Pesa,"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Thaavugindrathey,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Thaavugindrathey,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayin Udal Sootta,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin Udal Sootta,"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravadha Kuzhantha Pola,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravadha Kuzhantha Pola,"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurey Oorugindrathaey,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurey Oorugindrathaey,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilakkum Kooda, Velli Nilavaaga,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakkum Kooda, Velli Nilavaaga,"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Kolam Ennavo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Kolam Ennavo,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakkillaama, Vandhu Vidum Kadhal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkillaama, Vandhu Vidum Kadhal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhappum Seidhi Allavo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhappum Seidhi Allavo,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaga Nee Pesum Thamizha,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Nee Pesum Thamizha,"/>
</div>
<div class="lyrico-lyrics-wrapper">Arinjaa Odaatho Kavala,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arinjaa Odaatho Kavala,"/>
</div>
<div class="lyrico-lyrics-wrapper">Una Naan Thaalattuvenae Manakutula,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Naan Thaalattuvenae Manakutula,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayilaanji Mayilaanji, Mamen Ohn Mayilaanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilaanji Mayilaanji, Mamen Ohn Mayilaanji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodum Kaalodum, Poosendi Ena Aanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodum Kaalodum, Poosendi Ena Aanji,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallakku Pola Neeyum, Enna Thooki,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakku Pola Neeyum, Enna Thooki,"/>
</div>
<div class="lyrico-lyrics-wrapper">Desathi Dhesam Poga Ennuren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desathi Dhesam Poga Ennuren,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellaattu Mela, Pattupoochi Pola,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaattu Mela, Pattupoochi Pola,"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalana Unnai Aala Thulluren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalana Unnai Aala Thulluren,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadhaa-Sadhaa, Santhoshamaguren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhaa-Sadhaa, Santhoshamaguren,"/>
</div>
<div class="lyrico-lyrics-wrapper">Manokari Un Vaasathaal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manokari Un Vaasathaal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala Naanum Nooraaguren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Naanum Nooraaguren,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooraguren,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraguren,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakkuren Parakkuren, Therinjukadi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkuren Parakkuren, Therinjukadi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Naan Enakku Nee, Purinchukadi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Naan Enakku Nee, Purinchukadi,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayilanji Mayilanji, Mama Nee Mayilanji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilanji Mayilanji, Mama Nee Mayilanji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodum Kaalodum, Sethene Una Aanji.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodum Kaalodum, Sethene Una Aanji."/>
</div>
</pre>
