---
title: "sari gama pathani song lyrics"
album: "Paruthiveeran"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Ameer"
path: "/albums/paruthiveeran-lyrics"
song: "Sari Gama Pathani"
image: ../../images/albumart/paruthiveeran.jpg
date: 2007-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fww6Gljpf9Q"
type: "Melody"
singers:
  - Srimathumitha
  - Madurai S
  - Saroja
  - Ameer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sa ri ga ma pa tha ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ma pa tha ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollithaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollithaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyaa kettuttu paaduviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyaa kettuttu paaduviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodukkapuliya parichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukkapuliya parichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thovaiyal araichchu thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thovaiyal araichchu thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli paala karanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli paala karanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaapi pottu thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaapi pottu thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudichcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakkula esai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakkula esai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathaswaram thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathaswaram thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagangala kaththukkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagangala kaththukkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatchchanaiyum thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatchchanaiyum thevai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhappu kaatta sirukkikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhappu kaatta sirukkikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Purushan paththu paarungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purushan paththu paarungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazhambu sangili pottathaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazhambu sangili pottathaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkalum vikkalum adangundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkalum vikkalum adangundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela raththam egiri kuthikkuthoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela raththam egiri kuthikkuthoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangalellaam podaatha aattangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangalellaam podaatha aattangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ni ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ni ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ni ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ni ni dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ni ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ni ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ni ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ni ni dha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh dan dan khaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh dan dan khaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dan dan khaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dan dan khaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh dan dan khaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh dan dan khaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dan dan khaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dan dan khaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yegappatta sarakkirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegappatta sarakkirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai vasamthaan enkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vasamthaan enkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi nalla eththikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi nalla eththikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukka unkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukka unkitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandakkiri rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandakkiri rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththarandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththarandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mammutti kaaren vaaraandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mammutti kaaren vaaraandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ri ga ma pa tha ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ma pa tha ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Solliththaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliththaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyaa kettuttu paaduviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyaa kettuttu paaduviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ni ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ni ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ni ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ni ni dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ri sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa sa sa sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa sa sa sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ri ga ma ga ri ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ma ga ri ga ri sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ri ga ma pa tha ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ma pa tha ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Solliththaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliththaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu vettuven mavalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu vettuven mavalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththiyum kiththiyum kettupochcho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththiyum kiththiyum kettupochcho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada pora varavelellaam paattu paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pora varavelellaam paattu paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaambichchirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaambichchirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttailla utkaanthukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttailla utkaanthukittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ri ga ma pa tha ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ga ma pa tha ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Solliththaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliththaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyaa kettuttu paaduviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyaa kettuttu paaduviya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paatti"/>
</div>
</pre>
