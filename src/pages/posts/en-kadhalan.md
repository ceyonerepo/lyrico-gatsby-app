---
title: "en kadhalan"
album: "Light Sount Studios"
artist: "Vedshanker Sugavanam"
lyricist: "Kumaran Thangarajan"
director: "Haarish Jayavelu"
path: "/albums/en-kadhalan-song-lyrics"
song: "En kadhalan"
image: ../../images/albumart/en-kadhalan.jpg
date: 2020-09-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GJ93FHvbeTM"
type: "album"
singers:
  - Sreekanth Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnudan vazhnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan vazhnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkanam podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkanam podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir thurappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir thurappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madi sainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi sainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innodi podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innodi podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir pirappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mugam paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam paarthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam vizhi modhidhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vizhi modhidhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal kadhal en uyir vaangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal kadhal en uyir vaangidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un idazh pesidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idazh pesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam kadal thaandidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam kadal thaandidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal meendum en udal saithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal meendum en udal saithidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kadhal en nenjin ooram thaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kadhal en nenjin ooram thaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Melum enai kollai kolla ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum enai kollai kolla ketkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum en thaai mugamthai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum en thaai mugamthai paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal uyire…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal uyire…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudan vazhnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan vazhnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkanam podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkanam podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir thurappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir thurappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madi sainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi sainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innodi podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innodi podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir pirappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhalai Tamizh pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai Tamizh pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthum azhaganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthum azhaganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruthe naan marukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruthe naan marukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhari pogamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhari pogamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhainthum thaaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhainthum thaaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhaigirai manathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhaigirai manathile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir thaaram un paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thaaram un paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai naan thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai naan thaanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi oram mazhai neerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi oram mazhai neerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan yenguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan yenguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udainthalum nagamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthalum nagamagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam naan vaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam naan vaanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthirnthalum un paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthirnthalum un paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pada naan veezhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada naan veezhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal en nenjin ooram thaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal en nenjin ooram thaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Melum enai kollai kolla ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum enai kollai kolla ketkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum en thaai mugamthai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum en thaai mugamthai paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal uyire…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal uyire…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudan vazhnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan vazhnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkanam podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkanam podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir thurappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir thurappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madi sainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madi sainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innodi podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innodi podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir pirappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mugam paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam paarthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam vizhi modhidhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vizhi modhidhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal kadhal en uyir vaangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal kadhal en uyir vaangidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un idazh pesidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idazh pesidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam kadal thaandidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam kadal thaandidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal meendum en udal saithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal meendum en udal saithidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kadhal en nenjin ooram thaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kadhal en nenjin ooram thaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Melum enai kollai kolla ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum enai kollai kolla ketkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum en thaai mugamthai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum en thaai mugamthai paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal uyire…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal uyire…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un viral korthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral korthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innodi podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innodi podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan uyir pirappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan uyir pirappen"/>
</div>
</pre>
