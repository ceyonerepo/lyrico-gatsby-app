---
title: "ithu pol song lyrics"
album: "8 Thottakkal"
artist: "KS Sundaramurthy"
lyricist: "Kutti Revathy"
director: "Sri Ganesh"
path: "/albums/8-thottakkal-lyrics"
song: "Ithu Pol"
image: ../../images/albumart/8-thottakkal.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6vO8pAOSZDc"
type: "love"
singers:
  -	Sathya Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ithu pol ithu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pol ithu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimelum vaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum vaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuvum kanavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuvum kanavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaaga neelaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaaga neelaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thathumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thathumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engho vazhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engho vazhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril unarvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril unarvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvo asaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvo asaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini sogam theerumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini sogam theerumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vaasam veesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vaasam veesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perumegam vaanilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumegam vaanilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru saaral kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru saaral kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbu thonuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbu thonuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesakooda aal ilaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesakooda aal ilaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayulthan ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayulthan ponathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikoodi aanaimeeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikoodi aanaimeeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal thaan poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal thaan poguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalmurai vaanam paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalmurai vaanam paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavudan kooda pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavudan kooda pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaludan poga poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaludan poga poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa pesa paathai neelaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa pesa paathai neelaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini sogam theerumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini sogam theerumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vaasam veesuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vaasam veesuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perumegam vaanilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumegam vaanilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru saaral kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru saaral kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbu thonuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbu thonuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu pol oru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pol oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimelum thaaraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum thaaraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Methuvaai melithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methuvaai melithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaipola aavaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaipola aavaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimai siraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai siraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavam pol kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavam pol kidanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathavai thiranthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavai thiranthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyal vilithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyal vilithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu pothum pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu pothum pothumae"/>
</div>
</pre>
