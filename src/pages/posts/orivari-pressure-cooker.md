---
title: "orivari song lyrics"
album: "Pressure Cooker"
artist: "Smaran"
lyricist: "Niklesh Sunkoji"
director: "Sujoi Karampuri - Sushil Karampuri"
path: "/albums/pressure-cooker-lyrics"
song: "Orivari"
image: ../../images/albumart/pressure-cooker.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tC2zTYcv3xs"
type: "happy"
singers:
  - Suresh Bobbili
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oore Idisi Palle Velugu Bus Ekke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Idisi Palle Velugu Bus Ekke"/>
</div>
<div class="lyrico-lyrics-wrapper">Poragaani Sitramaina Badha Sudaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poragaani Sitramaina Badha Sudaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrey Leduro Sinngaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrey Leduro Sinngaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Bahda Baruvu Gattigundiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Bahda Baruvu Gattigundiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali Motarekkaboye Gaali Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali Motarekkaboye Gaali Gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadha Suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadha Suda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Gammunundaraadhu Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Gammunundaraadhu Ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrey Porado Mundhu Seat-u Meedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrey Porado Mundhu Seat-u Meedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu Thiyyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu Thiyyaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Galli Chivaranunnapori Ninnu Chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Galli Chivaranunnapori Ninnu Chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Ellavettethandhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Ellavettethandhuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Inti Pakkanunna Aunty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Pakkanunna Aunty"/>
</div>
<div class="lyrico-lyrics-wrapper">Aame Noru Jarranaina Moosetandhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aame Noru Jarranaina Moosetandhuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Pora Porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Pora Porada"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiripora Poradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiripora Poradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya Peru Nilipetandhuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya Peru Nilipetandhuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Pora Porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Pora Porada"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiripora Poradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiripora Poradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagamaagam Nuvvu Aithavenduko Ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagamaagam Nuvvu Aithavenduko Ro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori Vaari Ori Vaari Ori Vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Vaari Ori Vaari Ori Vaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oore Idisi Palle Velugu Bus Ekke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Idisi Palle Velugu Bus Ekke"/>
</div>
<div class="lyrico-lyrics-wrapper">Poragaani Sitramaina Badha Sudaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poragaani Sitramaina Badha Sudaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrey Leduro Sinngaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrey Leduro Sinngaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Bahda Baruvu Gattigundiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Bahda Baruvu Gattigundiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">GRE Score Patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GRE Score Patti"/>
</div>
<div class="lyrico-lyrics-wrapper">SOP File-u Teesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="SOP File-u Teesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Passport Sankanetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passport Sankanetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dead Line-u Soosukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dead Line-u Soosukoni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harvard-o Stanford-o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harvard-o Stanford-o"/>
</div>
<div class="lyrico-lyrics-wrapper">Princeton-o Ginceton-o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Princeton-o Ginceton-o"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooravatha Dakota La Cheripo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooravatha Dakota La Cheripo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intel-o Syntel-o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intel-o Syntel-o"/>
</div>
<div class="lyrico-lyrics-wrapper">Philips-o Gilipso-o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Philips-o Gilipso-o"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Settayye Job Soosukoro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Settayye Job Soosukoro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori Vaari Ori Vaari Ori Vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Vaari Ori Vaari Ori Vaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Insta FB Postulalla Yenaka Ponti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Insta FB Postulalla Yenaka Ponti"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchu Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchu Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Likulanni Endi Paayero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likulanni Endi Paayero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrey Sudaro Porado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrey Sudaro Porado"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Following Thaggi Paayero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Following Thaggi Paayero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Engineer-u India La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Engineer-u India La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkuva Dharane Palukuthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkuva Dharane Palukuthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelli Roju Badha Padakuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli Roju Badha Padakuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrey Porado Egiripo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrey Porado Egiripo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagamaagam Ayithavenduko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagamaagam Ayithavenduko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrey Porado Egiripo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrey Porado Egiripo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagamaagam Aithavenduko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagamaagam Aithavenduko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ori Vaari Ori Vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ori Vaari Ori Vaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagamaagam Aithavenduko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagamaagam Aithavenduko"/>
</div>
</pre>
