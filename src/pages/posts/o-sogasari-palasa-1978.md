---
title: "o sogasari song lyrics"
album: "Palasa 1978"
artist: "Raghu Kunche"
lyricist: "Lakshmi Bhupala"
director: "Karuna Kumar"
path: "/albums/palasa-1978-lyrics"
song: "O Sogasari"
image: ../../images/albumart/palasa-1978.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YakgvRIxk9M"
type: "love"
singers:
  - S.P. Balasubrahmanyam
  - Baby Pasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O Sogasari Priya Laahiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Sogasari Priya Laahiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari Valapula Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari Valapula Siri"/>
</div>
<div class="lyrico-lyrics-wrapper">O Gadasari Thelenu Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Gadasari Thelenu Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvapu Sharamula Guri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvapu Sharamula Guri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Rammantaavu Vasthe Pommantaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Rammantaavu Vasthe Pommantaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Kavvisthaavu Adhem Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Kavvisthaavu Adhem Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthe Muddhaantaavu Haddhe Dhaateshtaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthe Muddhaantaavu Haddhe Dhaateshtaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Immantaavu Povoi Maree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Immantaavu Povoi Maree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chalivelalo Cheli Elane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalivelalo Cheli Elane"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogasuku Bidiyapu Musugoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasuku Bidiyapu Musugoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Velalo Aagaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Velalo Aagaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Athigaa Pranayam Visugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigaa Pranayam Visugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Virahamantaanu Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahamantaanu Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasurukuntaavu Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasurukuntaavu Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasame Ledhu Sayyaatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasame Ledhu Sayyaatalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Vintoone Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Vintoone Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Antaavu Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Antaavu Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Thantaalu Siggaatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Thantaalu Siggaatalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheli Kurulalo Jalapaathame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli Kurulalo Jalapaathame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvoka Dhanuvai Merupoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvoka Dhanuvai Merupoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranayaalalo Ee Maatale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranayaalalo Ee Maatale"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku Muchhatagolupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku Muchhatagolupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendi Vennello Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendi Vennello Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Jaabilli Navvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Jaabilli Navvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannechekkillu Naa Kosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannechekkillu Naa Kosame"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Sepantu Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Sepantu Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguduthuntaavu Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguduthuntaavu Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapu Chaalinka Nachhaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu Chaalinka Nachhaavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Sogasari Priya Laahiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Sogasari Priya Laahiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari Valapula Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari Valapula Siri"/>
</div>
<div class="lyrico-lyrics-wrapper">O Gadasari Thelenu Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Gadasari Thelenu Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvapu Sharamula Guri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvapu Sharamula Guri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Rammantaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Rammantaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthe Pommantaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthe Pommantaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Kavvisthaavu Adhem Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Kavvisthaavu Adhem Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthe Muddhaantaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthe Muddhaantaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhe Dhaateshtaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhe Dhaateshtaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Immantaavu Povoi Maree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Immantaavu Povoi Maree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahhahaa Hahhaa Ahhahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhahaa Hahhaa Ahhahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhohho Ohoho Ohhohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhohho Ohoho Ohhohho"/>
</div>
</pre>
