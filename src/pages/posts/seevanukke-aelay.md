---
title: "seevanukke song lyrics"
album: "Aelay"
artist: "Kaber Vasuki - Aruldev"
lyricist: "Halitha Shameem"
director: "Halitha Shameem"
path: "/albums/aelay-song-lyrics"
song: "Seevanukke"
image: ../../images/albumart/aelay.jpg
date: 2021-002-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LZPAQ57RJf0"
type: "Love"
singers:
  - Yogi Sekar
  - Roja Adithya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En seevanuke seevan koduthale ammalu raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En seevanuke seevan koduthale ammalu raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kari sorum aakki pottu nalla naalil kannalam kattalandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari sorum aakki pottu nalla naalil kannalam kattalandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu kadalai seeni kalanthathu pola thaane sodiya sernthom di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu kadalai seeni kalanthathu pola thaane sodiya sernthom di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thisai paarthe ini ennalum pozhutha kazhipen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thisai paarthe ini ennalum pozhutha kazhipen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta veyil kaanjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta veyil kaanjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kitta irunthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kitta irunthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam mara nizhal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam mara nizhal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillupa unarvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillupa unarvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi vaana thoothala pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi vaana thoothala pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukku malli sirippale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adukku malli sirippale"/>
</div>
<div class="lyrico-lyrics-wrapper">Amuthakili arugamaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amuthakili arugamaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku dhinam arumarunthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku dhinam arumarunthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya naan kaatuna bus-um kooda nikkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya naan kaatuna bus-um kooda nikkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sada nee kaatiye enakkaga vanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada nee kaatiye enakkaga vanthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla naan unnai thaanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla naan unnai thaanganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanaiyil rayil onnu oduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanaiyil rayil onnu oduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En seevanuke seevan koduthale ammalu raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En seevanuke seevan koduthale ammalu raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kari sorum aakki pottu nalla naalil kannalam kattalandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari sorum aakki pottu nalla naalil kannalam kattalandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu kadalai seeni kalanthathu pola thaane sodiya sernthom di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu kadalai seeni kalanthathu pola thaane sodiya sernthom di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thisai paarthe ini ennalum pozhutha kazhipen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thisai paarthe ini ennalum pozhutha kazhipen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam seevan rendum sernthatha nenache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam seevan rendum sernthatha nenache"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallipoo kattunen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallipoo kattunen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera enakkul korpathil thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pera enakkul korpathil thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai santhosham da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai santhosham da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi udamba silirpatha pola thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi udamba silirpatha pola thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasum thudikkuthu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasum thudikkuthu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai nenaikkum bothu enakku ulloora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nenaikkum bothu enakku ulloora"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu onnu edukkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu onnu edukkuthada"/>
</div>
</pre>
