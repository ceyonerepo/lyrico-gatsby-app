---
title: "yaala yaala song lyrics"
album: "Ruler"
artist: "Chirantan Bhatt"
lyricist: "Ramajogayya Sastry"
director: "K S Ravikumar"
path: "/albums/ruler-lyrics"
song: "Yaala Yaala"
image: ../../images/albumart/ruler.jpg
date: 2019-12-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/nKbURSNm5d0"
type: "love"
singers:
  - Anurag Kulkarni
  - Anusha Mani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaala Yaala Iyyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaala Yaala Iyyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Diyyaa Diyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diyyaa Diyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enniyaala Yaala Joosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enniyaala Yaala Joosi "/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaala Vuyyaaleyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaala Vuyyaaleyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaala Yaala Iyyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaala Yaala Iyyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Diyyaa Diyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diyyaa Diyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enniyaala Yaala Joosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enniyaala Yaala Joosi "/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaala Vuyyaaleyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaala Vuyyaaleyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mila Milaa Andam Khillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mila Milaa Andam Khillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aajaa Ante Khullam Khullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajaa Ante Khullam Khullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchesthunnaa Aakali Kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchesthunnaa Aakali Kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajaa Singamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajaa Singamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelo Chelo Chemmaku Chellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelo Chelo Chemmaku Chellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagisthane Hallaa Gullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagisthane Hallaa Gullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Laagisthane Chekkara Paala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laagisthane Chekkara Paala"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvanala Rasagullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvanala Rasagullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaaramedoo Moginaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaaramedoo Moginaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Maarumoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Maarumoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaala Yaala Iyyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaala Yaala Iyyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Diyyaa Diyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diyyaa Diyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enniyaala Yaala Joosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enniyaala Yaala Joosi "/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaala Vuyyaaleyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaala Vuyyaaleyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Pedavulalo Madhushaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Pedavulalo Madhushaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Alajadule Tharigelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alajadule Tharigelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Padana Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Padana Madhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogasula Keratamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasula Keratamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Usigolipe Sumabaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Usigolipe Sumabaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumathule Adagaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumathule Adagaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sare Ituraa Ananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sare Ituraa Ananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvulu Thagilelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvulu Thagilelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Swayaana Cherukomandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swayaana Cherukomandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukomandi Mallepoola Cherasaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukomandi Mallepoola Cherasaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalaanaa Ichukomandi Puchhukomandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalaanaa Ichukomandi Puchhukomandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandanaala Sandevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandanaala Sandevela"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthadaaka Vachinaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthadaaka Vachinaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanaala Charchalela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanaala Charchalela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilaage Recchagottaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilaage Recchagottaledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaraina Neelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaraina Neelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaala Yaala Iyyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaala Yaala Iyyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Diyyaa Diyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diyyaa Diyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enniyaala Yaala Joosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enniyaala Yaala Joosi "/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaala Vuyyaaleyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaala Vuyyaaleyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kannupadithe Viraboosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannupadithe Viraboosaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaaru Paitale Sarichesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaru Paitale Sarichesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte Korikai Daggarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte Korikai Daggarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggulanni Eggaresaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggulanni Eggaresaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chitti Buggalo Chitikesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chitti Buggalo Chitikesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadum Ompulo Daruvesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadum Ompulo Daruvesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu Muttina Kougilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Muttina Kougilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aggimanta Chavi Chusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aggimanta Chavi Chusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaali Manchulaa Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaali Manchulaa Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchanaa Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchanaa Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhuga Muttadinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhuga Muttadinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jawaani Muchatai Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jawaani Muchatai Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mechukuntaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mechukuntaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vindugaa Aaraginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vindugaa Aaraginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Velapaala Chudakandi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velapaala Chudakandi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Unna Chaaru Sheela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Unna Chaaru Sheela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulaasa Paata Paadamandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulaasa Paata Paadamandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna Raasaleela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna Raasaleela"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaala Yaala Iyyaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaala Yaala Iyyaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Diyyaa Diyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diyyaa Diyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enniyaala Yaala Joosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enniyaala Yaala Joosi "/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaala Vuyyaaleyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaala Vuyyaaleyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mila Milaa Andam Khillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mila Milaa Andam Khillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aajaa Ante Khullam Khullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajaa Ante Khullam Khullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchesthunnaa Aakali Kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchesthunnaa Aakali Kalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajaa Singamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajaa Singamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelo Chelo Chemmaku Chellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelo Chelo Chemmaku Chellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagisthane Hallaa Gullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagisthane Hallaa Gullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Laagisthane Chekkara Paala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laagisthane Chekkara Paala"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvanala Rasagullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvanala Rasagullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaaramedoo Moginaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaaramedoo Moginaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Maarumoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Maarumoola"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaaramedoo Moginaadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaaramedoo Moginaadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Maarumoola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Maarumoola"/>
</div>
</pre>
