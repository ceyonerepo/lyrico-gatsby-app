---
title: "neeye thaniyaai song lyrics"
album: "Yaman"
artist: "Vijay Antony"
lyricist: "Muthumilan"
director: "Jeeva Shankar"
path: "/albums/yaman-lyrics"
song: "Neeye Thaniyaai"
image: ../../images/albumart/yaman.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uSiPnejr8yI"
type: "mass"
singers:
  - Jagadeesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeye Thaniyaai Unn Tholae Thunayaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Thaniyaai Unn Tholae Thunayaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Thunivaai Ulagil Poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Thunivaai Ulagil Poradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Vidiyum Unnaal Mudiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Vidiyum Unnaal Mudiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Modhum Thadayai Udaithe Thoolaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum Thadayai Udaithe Thoolaku "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Sodhanai Unnai Vaatum Podhilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Sodhanai Unnai Vaatum Podhilum "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Dhairiyam Thakka Vaithiru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Dhairiyam Thakka Vaithiru "/>
</div>
<div class="lyrico-lyrics-wrapper">Nermai dhaan Vellume Paadam Kattra Podhilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nermai dhaan Vellume Paadam Kattra Podhilum "/>
</div>
<div class="lyrico-lyrics-wrapper">Poridum Veeranaai Vaalai Thookidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poridum Veeranaai Vaalai Thookidu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Thaniyaai Unn Thole Thunayaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Thaniyaai Unn Thole Thunayaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Thunivaai Ulagil Poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Thunivaai Ulagil Poradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anayadha Paadhai Kalayadha Megham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anayadha Paadhai Kalayadha Megham "/>
</div>
<div class="lyrico-lyrics-wrapper">Yelidhaana Vazhkai Kidayadhu Yengum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelidhaana Vazhkai Kidayadhu Yengum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjamum Dhrogamum Unnai Suttri Vazhndhidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjamum Dhrogamum Unnai Suttri Vazhndhidum "/>
</div>
<div class="lyrico-lyrics-wrapper">Saalaiyil Pallam Pol Thaandi Sendridu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalaiyil Pallam Pol Thaandi Sendridu "/>
</div>
<div class="lyrico-lyrics-wrapper">Poigalum Unmai Pol Vesham Potu Pesume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poigalum Unmai Pol Vesham Potu Pesume "/>
</div>
<div class="lyrico-lyrics-wrapper">Needhi dhan Vendridum Dharma Yudhame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi dhan Vendridum Dharma Yudhame "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Thaniyaai Unn Thole Thunayaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Thaniyaai Unn Thole Thunayaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Thunivaai Ulagil Poradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Thunivaai Ulagil Poradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Vidiyum Unnaal Mudiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Vidiyum Unnaal Mudiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Modhum Thadayai Udaithe Thoolaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum Thadayai Udaithe Thoolaku"/>
</div>
</pre>
