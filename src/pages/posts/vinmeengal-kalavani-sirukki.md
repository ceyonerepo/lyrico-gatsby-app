---
title: "vinmeengal song lyrics"
album: "Kalavani Sirukki"
artist: "Dharun Antony"
lyricist: "unknown"
director: "Ravi Rahul"
path: "/albums/kalavani-sirukki-lyrics"
song: "Vinmeengal"
image: ../../images/albumart/kalavani-sirukki.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jANlidLWnIY"
type: "love"
singers:
  - Unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vinmeengal pootha velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeengal pootha velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">theerthamai pani veezhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerthamai pani veezhumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ananthu paartha vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ananthu paartha vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thegame rendum mothumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegame rendum mothumo"/>
</div>
<div class="lyrico-lyrics-wrapper">minsaara theebame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minsaara theebame"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi ooivedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi ooivedu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha thookame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha thookame"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal poei vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal poei vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu seraa peraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu seraa peraa"/>
</div>
<div class="lyrico-lyrics-wrapper">urasum valaiyalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasum valaiyalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">valasal iduvathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valasal iduvathu "/>
</div>
<div class="lyrico-lyrics-wrapper">kathaiya vidukathaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaiya vidukathaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">thodum kathaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodum kathaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinmeengal pootha velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeengal pootha velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">theerthamai pani veezhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerthamai pani veezhumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ananthu paartha vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ananthu paartha vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thegame rendum mothumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegame rendum mothumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilaga koosum aadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaga koosum aadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">puliyam pazhathu koodugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyam pazhathu koodugal"/>
</div>
<div class="lyrico-lyrics-wrapper">thadai podavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadai podavum "/>
</div>
<div class="lyrico-lyrics-wrapper">edai poduvum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edai poduvum "/>
</div>
<div class="lyrico-lyrics-wrapper">itho maaman vanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itho maaman vanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral pootiya jannalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral pootiya jannalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal kaatiya minnalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal kaatiya minnalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">malai thandavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai thandavum"/>
</div>
<div class="lyrico-lyrics-wrapper">sadai theendavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadai theendavum"/>
</div>
<div class="lyrico-lyrics-wrapper">itho neram endraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itho neram endraan"/>
</div>
<div class="lyrico-lyrics-wrapper">mugathi aadum malligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathi aadum malligai"/>
</div>
<div class="lyrico-lyrics-wrapper">agala koodum solligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agala koodum solligai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni theevil ennaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni theevil ennaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">sirai vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirai vai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinmeengal pootha velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeengal pootha velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">theerthamai pani veezhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerthamai pani veezhumo"/>
</div>
<div class="lyrico-lyrics-wrapper">ananthu paartha vaanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ananthu paartha vaanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thegame rendum mothumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegame rendum mothumo"/>
</div>
<div class="lyrico-lyrics-wrapper">minsaara theebame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minsaara theebame"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi ooivedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi ooivedu"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha thookame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha thookame"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal poei vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal poei vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu seraa peraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu seraa peraa"/>
</div>
<div class="lyrico-lyrics-wrapper">urasum valaiyalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasum valaiyalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">valasal iduvathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valasal iduvathu "/>
</div>
<div class="lyrico-lyrics-wrapper">kathaiya vidukathaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaiya vidukathaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">thodum kathaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodum kathaiya"/>
</div>
</pre>
