---
title: "anantham song lyrics"
album: "Jaanu"
artist: "Govind Vasantha"
lyricist: "Sri Mani"
director: "C. Prem Kumar"
path: "/albums/jaanu-lyrics"
song: "Anantham"
image: ../../images/albumart/jaanu.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Xzos2KEea-A"
type: "happy"
singers:
  - Chinmayi
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaalaala Prema Puttedhi Eppudante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaala Prema Puttedhi Eppudante"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yugaala Prema Jagaalaneluthondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugaala Prema Jagaalaneluthondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaju Laaga Shapinchu Varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaju Laaga Shapinchu Varamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poose Puvvoti Chaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poose Puvvoti Chaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaanni Gelichi Chooputhondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaanni Gelichi Chooputhondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepi Kanneeru Dhaagunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepi Kanneeru Dhaagunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Saagaram Idhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagaram Idhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Prema Kaaryam Raasindhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Prema Kaaryam Raasindhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Evarante Emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarante Emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Prema Gaayam Chesedi Evarante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Prema Gaayam Chesedi Evarante "/>
</div>
<div class="lyrico-lyrics-wrapper">Vivaramedhi Ledhandi Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivaramedhi Ledhandi Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadanna Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadanna Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Needalaaga Vasthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needalaaga Vasthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Avunanna Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avunanna Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayalaagaa Untundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayalaagaa Untundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Premallo Chedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premallo Chedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaname Nishi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaname Nishi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaganantoone Saagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaganantoone Saagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaganantoone Aagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaganantoone Aagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Antoone Moogadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Antoone Moogadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakedhi SaatiRaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakedhi SaatiRaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Praanamenthunna Chaaladhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanamenthunna Chaaladhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Janamalennunna Maaradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janamalennunna Maaradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwamanthunna Premadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwamanthunna Premadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedantha Gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedantha Gunde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Ee Premale Ananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ee Premale Ananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">O Ee Premale Ananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ee Premale Ananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavedhanalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavedhanalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Chinni Mounamulona Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Chinni Mounamulona Enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogisalo Raasi Leni Kaavyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogisalo Raasi Leni Kaavyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosu Kalapadhe Premalake Oopiridhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosu Kalapadhe Premalake Oopiridhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohale Oohale Ninu Vidavavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohale Oohale Ninu Vidavavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeke Praanamai Poose Poose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeke Praanamai Poose Poose"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohale Oohale Ninu Marichina Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohale Oohale Ninu Marichina Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopire Leni Vela Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Leni Vela Aa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Ee Premale Ananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ee Premale Ananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">O Ee Premale Ananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ee Premale Ananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavedhanalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavedhanalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Oho Shreekaarame Aakaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Oho Shreekaarame Aakaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Omkaaram Preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omkaaram Preme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Oho Ananthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Oho Ananthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ananthame Idhantha Preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ananthame Idhantha Preme"/>
</div>
</pre>
