---
title: "pain song song lyrics"
album: "Raja Vaaru Rani Gaaru"
artist: "Jay Krish"
lyricist: "Rakendu Mouli"
director: "Ravi Kiran Kola"
path: "/albums/raja-vaaru-rani-gaaru-lyrics"
song: "Pain Song"
image: ../../images/albumart/raja-vaaru-rani-gaaru.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9K1rvF0I76c"
type: "sad"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Yemaindhilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yemaindhilaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Naatho Naake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Naatho Naake"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeee Yuddaaaley Aagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeee Yuddaaaley Aagavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallanchullona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallanchullona "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneellayye Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneellayye Kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jaaada Thelupavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jaaada Thelupavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chesindhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chesindhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Thappo Theliselogaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thappo Theliselogaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeee Nimushaale Niluvavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeee Nimushaale Niluvavaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmm Sikshinche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmm Sikshinche "/>
</div>
<div class="lyrico-lyrics-wrapper">Yekaaanthaala Yugamulu Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekaaanthaala Yugamulu Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanaani Kokatigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanaani Kokatigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swapnaala Baatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapnaala Baatalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathyaala Vetaluu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathyaala Vetaluu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soonyaala Kotalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soonyaala Kotalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoboochu Laatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoboochu Laatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennaallu Aaganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaallu Aaganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneella Veganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneella Veganu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Orpu Oodene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Orpu Oodene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nittoorpu Kekalaaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nittoorpu Kekalaaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Chedirina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Chedirina "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakalamula Kalathala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalamula Kalathala "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam Edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam Edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhi Madhi Adhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhi Madhi Adhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhiyanu Kadhalani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhiyanu Kadhalani "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha Neeve Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha Neeve Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaga Dhaga Manu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaga Dhaga Manu "/>
</div>
<div class="lyrico-lyrics-wrapper">Sega Ragilina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sega Ragilina "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaga Bhuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga Bhuga "/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeni Kaalchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeni Kaalchey"/>
</div>
<div class="lyrico-lyrics-wrapper">Satha Madha Napu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satha Madha Napu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ruthuvuna Satha Mathamula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruthuvuna Satha Mathamula "/>
</div>
<div class="lyrico-lyrics-wrapper">Chithikina Mathi Chithi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithikina Mathi Chithi "/>
</div>
<div class="lyrico-lyrics-wrapper">Athakani Brathukuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athakani Brathukuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Yemaindhilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yemaindhilaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Naatho Naake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Naatho Naake"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeee Yuddaaaley Aagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeee Yuddaaaley Aagavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallanchullona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallanchullona "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneellayye Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneellayye Kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jaaada Thelupavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jaaada Thelupavaa"/>
</div>
</pre>
