---
title: "uyya uyya uttango song lyrics"
album: "Mudhal Idam"
artist: "D. Imman"
lyricist: "Arivumathi"
director: "R. Kumaran"
path: "/albums/mudhal-idam-lyrics"
song: "Uyya Uyya Uttango"
image: ../../images/albumart/mudhal-idam.jpg
date: 2011-08-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bqPq9EQquhg"
type: "love"
singers:
  - Dindukallu Poovitha
  - Veeramanidasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">urumi Urrrungga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urumi Urrrungga"/>
</div>
<div class="lyrico-lyrics-wrapper">udukka Gurrrungga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udukka Gurrrungga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaada Karrrungga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaada Karrrungga"/>
</div>
<div class="lyrico-lyrics-wrapper">kowthaari Surrrungga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kowthaari Surrrungga"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaththil Vittathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaththil Vittathaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo Pattaiya Kelappa Porom Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo Pattaiya Kelappa Porom Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Nellu Katta Thookkikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nellu Katta Thookkikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">nedu Nedunnu Poravarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedu Nedunnu Poravarae"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla Kitta Mallu Katta Vaariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla Kitta Mallu Katta Vaariya"/>
</div>
<div class="lyrico-lyrics-wrapper">illa Thuppukettu Uchchi Kotta Poriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa Thuppukettu Uchchi Kotta Poriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta Sotta Nenanjuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta Sotta Nenanjuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">sozhattikittu Poravalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sozhattikittu Poravalae"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkavechchu Ikku Vekka Poriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkavechchu Ikku Vekka Poriya"/>
</div>
<div class="lyrico-lyrics-wrapper">illa Ikku Vechchu Sokka Vekka Vaariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa Ikku Vechchu Sokka Vekka Vaariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazha Parakkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazha Parakkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thattaan Thattaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattaan Thattaan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna Thangaththula Senju Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna Thangaththula Senju Avan"/>
</div>
<div class="lyrico-lyrics-wrapper">vittaan Vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittaan Vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">moochchu Muttuthu Moochchu Muttuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochchu Muttuthu Moochchu Muttuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">saachchuputtiyae Machchaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saachchuputtiyae Machchaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">koochcham Nattuthu Koochcham Nattuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koochcham Nattuthu Koochcham Nattuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aechchuputtiyae Machchaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aechchuputtiyae Machchaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">saachchuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saachchuputtiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">aechchuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aechchuputtiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Matta Matta Paakku Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matta Matta Paakku Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">malaarunnu Thaakkiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaarunnu Thaakkiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">atta Pola Ottikka Nee Vaariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atta Pola Ottikka Nee Vaariya"/>
</div>
<div class="lyrico-lyrics-wrapper">illa Thittiputtu Sirichchukittae Poriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa Thittiputtu Sirichchukittae Poriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Maela Parakkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maela Parakkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">sittaan Sittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittaan Sittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna Maechchalukku Avuththu Evan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna Maechchalukku Avuththu Evan"/>
</div>
<div class="lyrico-lyrics-wrapper">vittan Vittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittan Vittan"/>
</div>
<div class="lyrico-lyrics-wrapper">moochchu Muttuthu Moochchu Muttuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochchu Muttuthu Moochchu Muttuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">saachchuputtiyae Machchaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saachchuputtiyae Machchaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">koochcham Nattuthu Koochcham Nattuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koochcham Nattuthu Koochcham Nattuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aechchuputtiyae Machchaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aechchuputtiyae Machchaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">saachchuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saachchuputtiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">aechchuputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aechchuputtiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenukkuththaan Aenguthada Kokku Kokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenukkuththaan Aenguthada Kokku Kokku"/>
</div>
<div class="lyrico-lyrics-wrapper">athu Meenukkunthaan Theriyumada Makku Makku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu Meenukkunthaan Theriyumada Makku Makku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">Thovaram Kaattukkulla Vevaram Kaettkukolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thovaram Kaattukkulla Vevaram Kaettkukolla"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthupudu Anthi Naerama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthupudu Anthi Naerama"/>
</div>
<div class="lyrico-lyrics-wrapper">aahn...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahn..."/>
</div>
<div class="lyrico-lyrics-wrapper">inga Panthi Poda Innum Naerama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga Panthi Poda Innum Naerama"/>
</div>
<div class="lyrico-lyrics-wrapper">athu Seri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu Seri"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
<div class="lyrico-lyrics-wrapper">uyya Uyya Uttaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyya Uyya Uttaango"/>
</div>
<div class="lyrico-lyrics-wrapper">usura Vanthu Thottaango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura Vanthu Thottaango"/>
</div>
</pre>
