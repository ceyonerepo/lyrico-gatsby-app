---
title: "vellu vellu song lyrics"
album: "Aranya"
artist: "Shantanu Moitra"
lyricist: "Vanamali"
director: "Prabhu Solomon"
path: "/albums/aranya-lyrics"
song: "Vellu Vellu Vellave"
image: ../../images/albumart/aranya.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MaHXE_iDKfM"
type: "happy"
singers:
  - Haricharan
  - Shantanu Moitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vellu vellu vellave alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellu vellu vellave alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechhanaina ooha nuvvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechhanaina ooha nuvvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellu vellu vellave alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellu vellu vellave alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechhanaina ooha nuvvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechhanaina ooha nuvvala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prematho… naa prematho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prematho… naa prematho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusunu thelapane alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusunu thelapane alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuputho thana chuputho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuputho thana chuputho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mantalu repe lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantalu repe lopala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhave Padhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhave Padhave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana aachuki theeyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana aachuki theeyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhave Padhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhave Padhave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanatho vachhi vaalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanatho vachhi vaalave"/>
</div>
</pre>
