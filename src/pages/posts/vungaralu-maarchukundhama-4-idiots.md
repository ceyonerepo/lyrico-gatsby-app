---
title: "vungaralu song lyrics"
album: "4 Idiots"
artist: "Jaya surya"
lyricist: "Jaya surya"
director: "S. Satish Kumar"
path: "/albums/4-idiots-lyrics"
song: "Vungaralu Marchukundhama"
image: ../../images/albumart/4-idiots.jpg
date: 2019-11-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YzOozkXnInA"
type: "love"
singers:
  - Jai Srinivas
  - Sruthika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vungaralu Marchukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vungaralu Marchukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Bongaralu Aadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Bongaralu Aadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gutilona Vundipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gutilona Vundipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilinthalona Kandhipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilinthalona Kandhipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu kallu Kammaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu kallu Kammaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollu Vollu Sammatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollu Vollu Sammatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari Gattigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari Gattigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hattukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hattukundama"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathirela Vecchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathirela Vecchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagatiputa Picchigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagatiputa Picchigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Nanna Aatale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Nanna Aatale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadukundhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukundhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippu Rajukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippu Rajukundama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Chesukundhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Chesukundhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukaarala Sattha Telchukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukaarala Sattha Telchukundama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vungaralu Marchukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vungaralu Marchukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Bongaralu Aadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Bongaralu Aadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gutilona Vundipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gutilona Vundipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilinthalona Kandhipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilinthalona Kandhipodama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvveenthagaa Mudhosthu Vunnavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvveenthagaa Mudhosthu Vunnavante"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaipekkele Naalonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaipekkele Naalonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvventhaga Rekkisthu Vunnavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvventhaga Rekkisthu Vunnavante"/>
</div>
<div class="lyrico-lyrics-wrapper">Heetekkele Lolonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heetekkele Lolonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave Vagaalaadi Nene Nee Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave Vagaalaadi Nene Nee Jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Manuvadi Koyistha Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Manuvadi Koyistha Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave Racha Gummadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave Racha Gummadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni pulaguntadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni pulaguntadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Dochesukundam Muntha Maamidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Dochesukundam Muntha Maamidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukora Gaaradi Cherukunta Nee vodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukora Gaaradi Cherukunta Nee vodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummu dulipesukoraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummu dulipesukoraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapotadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapotadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vungaralu Marchukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vungaralu Marchukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Bongaralu Aadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Bongaralu Aadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gutilona Vundipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gutilona Vundipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilinthalona Kandhipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilinthalona Kandhipodama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chiranchulo Nee Peru Rasukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiranchulo Nee Peru Rasukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyanduko Kurradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyanduko Kurradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundelo Nee Bomma Gisukuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundelo Nee Bomma Gisukuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Upeyyave Nee Janda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upeyyave Nee Janda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Neluvella Ekkindira Maikam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Neluvella Ekkindira Maikam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chori Cheseinaa Nee Chekkera Paakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chori Cheseinaa Nee Chekkera Paakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sppedu Penchukundamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sppedu Penchukundamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedu Panchukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu Panchukundama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandu Endalla Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandu Endalla Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanjukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudduladukundamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudduladukundamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddu Hatukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddu Hatukundama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandi kolataminka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandi kolataminka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukundhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vungaralu Marchukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vungaralu Marchukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Bongaralu Aadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Bongaralu Aadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gutilona Vundipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gutilona Vundipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilinthalona Kandhipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilinthalona Kandhipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu kallu Kammaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu kallu Kammaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollu Vollu Sammatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollu Vollu Sammatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari Gattigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari Gattigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hattukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hattukundama"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathirela Vecchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathirela Vecchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagatiputa Pacchigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagatiputa Pacchigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Nanna Aatale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Nanna Aatale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadukundhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadukundhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippu Rajukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippu Rajukundama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Chesukundhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Chesukundhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppukaarala Sattha Telchukundama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppukaarala Sattha Telchukundama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vungaralu Marchukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vungaralu Marchukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Bongaralu Aadukundhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Bongaralu Aadukundhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gutilona Vundipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gutilona Vundipodama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilinthalona Kandhipodama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilinthalona Kandhipodama"/>
</div>
</pre>
