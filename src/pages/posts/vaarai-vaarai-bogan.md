---
title: "vaarai vaarai song lyrics"
album: "Bogan"
artist: "D Imman"
lyricist: "Thamarai - Inno Genga"
director: "Madhan Karky"
path: "/albums/bogan-lyrics"
song: "Vaarai Vaarai"
image: ../../images/albumart/bogan.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/skkr1_Qyld4"
type: "love"
singers:
  - Shankar Mahadevan
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarai nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai nee vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum idam vegu dhooramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum idam vegu dhooramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorai nee koorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorai nee koorai"/>
</div>
<div class="lyrico-lyrics-wrapper">En raththa idhal un muththa kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raththa idhal un muththa kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena koorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena koorai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarai vaarai nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai vaarai nee vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum idam vegu dhooramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum idam vegu dhooramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koorai koorai nee koorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorai koorai nee koorai"/>
</div>
<div class="lyrico-lyrics-wrapper">En raththa idhal un muththa kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raththa idhal un muththa kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena koorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena koorai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjaga nee nenjellam thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaga nee nenjellam thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna kelvi patri kolla vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna kelvi patri kolla vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaamathil vaan kaamathil aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaamathil vaan kaamathil aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyangal veen sutri kolla vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyangal veen sutri kolla vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siripo unadhu idhalo enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripo unadhu idhalo enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiro unadhu udalo enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiro unadhu udalo enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarai nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai nee vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum idam vegu dhooramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum idam vegu dhooramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaarai unai thaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarai unai thaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam kaathirukka ini neramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kaathirukka ini neramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaarai Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaarai Aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarai vaarai nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai vaarai nee vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum idam vegu dhooramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum idam vegu dhooramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharai thaarai unai thaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai thaarai unai thaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam kaathirukka ini neramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kaathirukka ini neramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaarai Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaarai Aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaga nee ullukkul thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaga nee ullukkul thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen innum dhaagam ootrikollatta Aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen innum dhaagam ootrikollatta Aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjathil naan manjathil maan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjathil naan manjathil maan"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjalgal veen alli thinnatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjalgal veen alli thinnatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaiyo unadhu idaiyo enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyo unadhu idaiyo enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam unadhu maarbo enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam unadhu maarbo enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhuUnadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhuUnadhu enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un netriyil thaeninai ootri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un netriyil thaeninai ootri"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paadhathil parugatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhathil parugatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mudhuginil theeyinai yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mudhuginil theeyinai yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil melugena urugatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil melugena urugatuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kilarchi kuviyam arainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kilarchi kuviyam arainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil kiligal parakka vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil kiligal parakka vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey nee unai theenda idathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nee unai theenda idathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naavinil theendidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naavinil theendidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee endhan sondham illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee endhan sondham illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanae maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum undhan melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum undhan melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogam thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal adhai kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal adhai kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalun nagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalun nagam"/>
</div>
<div class="lyrico-lyrics-wrapper">En kamuganai kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kamuganai kaattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaiyal unadhu karangal enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyal unadhu karangal enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaivu unadhu varangal enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaivu unadhu varangal enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilaigal enadhu kanigal unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilaigal enadhu kanigal unadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhi unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi unadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarai vaarai nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai vaarai nee vaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum idam vegu dhooramillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum idam vegu dhooramillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoorai thoorai nee thoorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorai thoorai nee thoorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moga mazhai en utcha nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moga mazhai en utcha nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thoorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thoorai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjaga nee nenjellam thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaga nee nenjellam thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjalgal vendam patri kollatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjalgal vendam patri kollatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjathil naan manjathil maan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjathil naan manjathil maan"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjalgal veen alli thinnatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjalgal veen alli thinnatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siripo unadhu idhalo enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripo unadhu idhalo enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam unadhu maarbo enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam unadhu maarbo enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu enadhu uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu enadhu uu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraiiii"/>
</div>
</pre>
