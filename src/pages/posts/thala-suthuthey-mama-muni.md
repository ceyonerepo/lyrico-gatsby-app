---
title: "thala suthuthey mama song lyrics"
album: "Muni"
artist: "Bharathwaj"
lyricist: "Pa. Vijay - Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/muni-lyrics"
song: "Thala Suthuthey Mama"
image: ../../images/albumart/muni.jpg
date: 2007-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/C-0xwh3XGZo"
type: "Love"
singers:
  - Raghava Lawrence
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oo….. maami……………
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo….. maami……………"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo……………. maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo……………. maami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavuru bomma pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru bomma pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu suththuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu suththuthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey thala kaalu puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey thala kaalu puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ulagam aaduthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagam aaduthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey raamasaami ey kuppusaami…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey raamasaami ey kuppusaami….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ey raamasaami kuppusaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey raamasaami kuppusaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey raamasaami kuppusaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey raamasaami kuppusaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanthasaami munusaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthasaami munusaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enne saamiyo hei saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enne saamiyo hei saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoolu kelappu dhoolu kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolu kelappu dhoolu kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoosu parakka dhoolu kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoosu parakka dhoolu kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnappaiyaa yei cinnappaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnappaiyaa yei cinnappaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu illennu kavla vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu illennu kavla vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal irukku pattaiya kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal irukku pattaiya kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnappaiyaa yei chinnappaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnappaiyaa yei chinnappaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Background edhukkuda unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Background edhukkuda unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kadavul irukkaan namakku namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kadavul irukkaan namakku namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Appaa soththu yethukkudaa unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaa soththu yethukkudaa unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma soththu irukkudaa uzhaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma soththu irukkudaa uzhaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada oram pogaatha othingi nikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada oram pogaatha othingi nikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa vaadaa unnaal mudiyumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa vaadaa unnaal mudiyumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maams …hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maams …hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga paaruppaa kadavula thitraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga paaruppaa kadavula thitraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul ivanukkellaam koduthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul ivanukkellaam koduthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul mela complaint pannaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul mela complaint pannaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye intha paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye intha paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panaththa paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panaththa paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasaththa maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaththa maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peththa thaaya rotula vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa thaaya rotula vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha paiyan baa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha paiyan baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onakkum vayasaagum maappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onakkum vayasaagum maappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pulla vaippaan aappu aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pulla vaippaan aappu aappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjanaal vaazhkkedaa saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjanaal vaazhkkedaa saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thillirunthaa marthatti kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thillirunthaa marthatti kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Veen buildap aanaa vanthaa venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veen buildap aanaa vanthaa venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallavarkku vallavar ingiruppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallavarkku vallavar ingiruppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkaa sonna thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkaa sonna thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanks paatti ..hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks paatti ..hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavuru bomma pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru bomma pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu suththuthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu suththuthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey thala kaalu puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey thala kaalu puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ulagam aaduthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagam aaduthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey raamasaami ey kuppusaami…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey raamasaami ey kuppusaami….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ey raamasaami kuppusaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey raamasaami kuppusaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey raamasaami kuppusaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey raamasaami kuppusaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanthasaami munusaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthasaami munusaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enne saamiyo hei saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enne saamiyo hei saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugaa"/>
</div>
</pre>
