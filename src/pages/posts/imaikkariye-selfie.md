---
title: "imaikkariye song lyrics"
album: "Selfie"
artist: "GV Prakash Kumar"
lyricist: "Arivu"
director: "Mathi Maran"
path: "/albums/selfie-lyrics"
song: "Imaikkariye"
image: ../../images/albumart/selfie.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j22Bu1dPOoc"
type: "love"
singers:
  - GV Prakash Kumar
  - Manasvini Gopal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Look Into My Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Into My Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">Look Into My Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Into My Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High I Feel High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High I Feel High"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High Am I On Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High Am I On Sky"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High I Feel High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High I Feel High"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High Am I On Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High Am I On Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nigare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nigare"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaikkariye Ennai Izhaithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkariye Ennai Izhaithaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadai Paarvaiyal Ennam Karaithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadai Paarvaiyal Ennam Karaithaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Charale Ennai Azhaithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Charale Ennai Azhaithaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaikamale Nenjam Niraithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaikamale Nenjam Niraithaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodhum Un Kai Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhum Un Kai Viral"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Peranbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Peranbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhum Nee Enbadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum Nee Enbadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Yedhinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Yedhinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mozhi Edhum Munagaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mozhi Edhum Munagaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasidhaagam Unaraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasidhaagam Unaraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangal Paarthu Kidakava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal Paarthu Kidakava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidivaadham Kuraiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidivaadham Kuraiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi Neram Vilagaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Neram Vilagaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhoram Muththam Padhikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoram Muththam Padhikava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serum Kadal Aagum Udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Kadal Aagum Udal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Neeraagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Neeraagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochil Modhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochil Modhidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Paaraamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Paaraamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Seidhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Seidhidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Uraiyaadal Kuraiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Uraiyaadal Kuraiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhodum Valayaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhodum Valayaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal Thindru Theerkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal Thindru Theerkava"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigaram Enadhaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigaram Enadhaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalai Namadhaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalai Namadhaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeneer Nee Pottu Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeneer Nee Pottu Kondu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochile Un Koochale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochile Un Koochale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Look Into My Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Into My Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">Look Into My Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look Into My Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High I Feel High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High I Feel High"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High When You’re My Side
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High When You’re My Side"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High I Feel High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High I Feel High"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel High When You’re My Side
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel High When You’re My Side"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nigare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nigare"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaikkariye Ennai Izhaithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkariye Ennai Izhaithaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadai Paarvaiyal Ennam Karaithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadai Paarvaiyal Ennam Karaithaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Charale Ennai Azhaithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Charale Ennai Azhaithaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaikamale Nenjam Niraithaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaikamale Nenjam Niraithaayadi"/>
</div>
</pre>
