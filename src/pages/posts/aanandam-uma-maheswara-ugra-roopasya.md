---
title: "aanandam song lyrics"
album: "Uma Maheswara Ugra Roopasya"
artist: "Bijibal"
lyricist: "Rehman"
director: "Venkatesh Maha"
path: "/albums/uma-maheswara-ugra-roopasya-lyrics"
song: "Aanandam"
image: ../../images/albumart/uma-maheswara-ugra-roopasya.jpg
date: 2020-07-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/X6zPjb2tyCs"
type: "love"
singers:
  - Gowtham Bharadwaj
  - Soumya Ramakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aanandham aaraatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham aaraatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham ante ardham chupincheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham ante ardham chupincheti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Adbutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Adbutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaratam anchullone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaratam anchullone"/>
</div>
<div class="lyrico-lyrics-wrapper">Nityam saage ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nityam saage ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurai pudami kadupuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurai pudami kadupuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalayyeti aa madhaname madhuramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalayyeti aa madhaname madhuramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhayam kosam edhure chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhayam kosam edhure chuse"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishaale nijamaina veeduka kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishaale nijamaina veeduka kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalitham marichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalitham marichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugee theesey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugee theesey"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam ika prathi pootoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam ika prathi pootoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanuka ayipodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanuka ayipodha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeru aaviriga egisinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeru aaviriga egisinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapana perigi adhi kadali nodhilinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapana perigi adhi kadali nodhilinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaru mabbuluga merisinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaru mabbuluga merisinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvu anuvuga oka madhuvuga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvu anuvuga oka madhuvuga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaney vaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney vaanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu adugu kalipi kadhilipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu adugu kalipi kadhilipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalinti dhaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalinti dhaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Malupedhaina gelupe chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malupedhaina gelupe chuse"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugullo asalaina aa anandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugullo asalaina aa anandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile nadhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile nadhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Egise alala edhalopala kshanamagani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egise alala edhalopala kshanamagani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeetham kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeetham kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indhra dhanasulo varnamule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indhra dhanasulo varnamule"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudami odilo padi chiguru thodiginavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudami odilo padi chiguru thodiginavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shardhruthuvulo sarigamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shardhruthuvulo sarigamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodime thadime tholi pilupuga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodime thadime tholi pilupuga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaham theere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaham theere"/>
</div>
<div class="lyrico-lyrics-wrapper">Virula sirulu virisi murisipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virula sirulu virisi murisipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikotha maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikotha maaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ubikey mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ubikey mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urike pranam thanakosam dhigivasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urike pranam thanakosam dhigivasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa aakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Karige dhooram teriche dwaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karige dhooram teriche dwaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamanthata pulakinthalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamanthata pulakinthalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poose vaasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poose vaasantham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandham aaraatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham aaraatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham ante ardham chupincheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham ante ardham chupincheti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Adbutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Adbutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaratam anchullone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaratam anchullone"/>
</div>
<div class="lyrico-lyrics-wrapper">Nityam saage ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nityam saage ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurai pudami kadupuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurai pudami kadupuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalayyeti aa madhaname madhuramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalayyeti aa madhaname madhuramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhayam kosam edhure chuse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhayam kosam edhure chuse"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishaale nijamaina veeduka kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishaale nijamaina veeduka kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalitham marichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalitham marichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugee theesey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugee theesey"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam ika prathi pootoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam ika prathi pootoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanuka ayipodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanuka ayipodha"/>
</div>
</pre>