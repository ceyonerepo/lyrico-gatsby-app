---
title: "thangatha urasa song lyrics"
album: "Enna Thavam Seitheno"
artist: "Dev Guru"
lyricist: "Venkatesh Prabhakar"
director: "Murabasalan"
path: "/albums/enna-thavam-seitheno-lyrics"
song: "Thangatha Urasa"
image: ../../images/albumart/enna-thavam-seitheno.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/khBFOyfv7E8"
type: "love"
singers:
  - Nikil Mathew
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thangatha urasa aasai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangatha urasa aasai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vekatha udaika neram ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekatha udaika neram ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalgal seraiyile satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalgal seraiyile satham"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaai kathu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaai kathu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">theniya iluka thaniya thavichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theniya iluka thaniya thavichu"/>
</div>
<div class="lyrico-lyrics-wrapper">virinju than thamarai poove kidaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virinju than thamarai poove kidaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal pothum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal pothum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukku manja theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukku manja theva illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnulle nan ennulle nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnulle nan ennulle nee"/>
</div>
<div class="lyrico-lyrics-wrapper">namakaga naam sernthomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakaga naam sernthomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">inbangal than arinthomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbangal than arinthomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pimbangal than tholaiyathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pimbangal than tholaiyathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">selai madippu padigal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selai madippu padigal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">athil aeri naan payanam sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil aeri naan payanam sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thangatha urasa aasai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangatha urasa aasai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vekatha udaika neram ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekatha udaika neram ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalgal seraiyile satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalgal seraiyile satham"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaai kathu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaai kathu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">theniya iluka thaniya thavichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theniya iluka thaniya thavichu"/>
</div>
<div class="lyrico-lyrics-wrapper">virinju than thamarai poove kidaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virinju than thamarai poove kidaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal pothum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal pothum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukku manja theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukku manja theva illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siru pillaiyai servomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru pillaiyai servomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyatai than thodarvomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyatai than thodarvomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pani urugum mun pagirvomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani urugum mun pagirvomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thodu vaanathil inaivomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodu vaanathil inaivomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enai thangave nee irukirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai thangave nee irukirai"/>
</div>
<div class="lyrico-lyrics-wrapper">unai kathida naan thudikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai kathida naan thudikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thangatha urasa aasai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangatha urasa aasai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">vekatha udaika neram ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekatha udaika neram ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalgal seraiyile satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalgal seraiyile satham"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaai kathu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaai kathu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">theniya iluka thaniya thavichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theniya iluka thaniya thavichu"/>
</div>
<div class="lyrico-lyrics-wrapper">virinju than thamarai poove kidaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virinju than thamarai poove kidaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal pothum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal pothum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukku manja theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukku manja theva illa"/>
</div>
</pre>
