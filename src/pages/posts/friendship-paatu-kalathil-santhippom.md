---
title: "friendship paatu song lyrics"
album: "Kalathil Santhippom"
artist: "Yuvan Shankar Raja"
lyricist: "Viveka"
director: "N. Rajasekar"
path: "/albums/kutty-kalathil-santhippom-lyrics"
song: "Friendship Paattu"
image: ../../images/albumart/kalathil-santhippom.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0i94BUMM9bY"
type: "Friendship"
singers:
  - Jithinraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vilaiyada pogum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyada pogum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirum pudhirum nippome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirum pudhirum nippome"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyeri vanthal thozhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyeri vanthal thozhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal serthu selvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal serthu selvome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukku vetrinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku vetrinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jore ah aadi theeppome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jore ah aadi theeppome"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha iruppome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha iruppome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivagaramaga thonum summa paakka hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivagaramaga thonum summa paakka hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kooda tharuvom thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kooda tharuvom thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannai kaaka hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai kaaka hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhirana paadhai engaloda vaazhkai hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhirana paadhai engaloda vaazhkai hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirivome naanga ettu thikkum adhira thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirivome naanga ettu thikkum adhira thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyada pogum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyada pogum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirum pudhirum nippome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirum pudhirum nippome"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyeri vanthal thozhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyeri vanthal thozhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal serthu selvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal serthu selvome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukku vetrinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku vetrinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jore ah aadi theeppome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jore ah aadi theeppome"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha iruppome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha iruppome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kankaana thooramthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kankaana thooramthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nammai vaithalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nammai vaithalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbangal vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbangal vanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhan engu kan thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan engu kan thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutri eppodhum pala sondham bandham thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutri eppodhum pala sondham bandham thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavanaalum oru nanban nanban thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavanaalum oru nanban nanban thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathadiyai oru kaathadiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadiyai oru kaathadiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada angum ingum sutri sutri aadi varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada angum ingum sutri sutri aadi varuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eralamai ada eralamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eralamai ada eralamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam vaithu nenjil thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam vaithu nenjil thaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham indha nanban thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham indha nanban thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Example illai poda engaloda natpukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Example illai poda engaloda natpukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Excuse eh kettathillai engalukkul thappukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Excuse eh kettathillai engalukkul thappukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Good morning evening solla maattom oppukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good morning evening solla maattom oppukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan friendshipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan friendshipu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivagaramaga thonum summa paakka hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivagaramaga thonum summa paakka hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kooda tharuvom thozhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir kooda tharuvom thozhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannai kaaka hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai kaaka hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhirana paadhai engaloda vaazhkai hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhirana paadhai engaloda vaazhkai hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirivome naanga ettu thikkum adhira thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirivome naanga ettu thikkum adhira thaan"/>
</div>
</pre>
