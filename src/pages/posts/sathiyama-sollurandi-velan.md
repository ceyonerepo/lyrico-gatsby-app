---
title: "sathiyama sollurandi song lyrics"
album: "Velan"
artist: "Gopi Sundar"
lyricist: "Mugen Rao"
director: "Kavin"
path: "/albums/velan-lyrics"
song: "Sathiyama Sollurandi"
image: ../../images/albumart/velan.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/H1v3Czm26x4"
type: "love"
singers:
  - Mugen Rao
  - Sivaangi Krishnakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sathiyama naan sollurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama naan sollurandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un parvai ala thookkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un parvai ala thookkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathiyama naanum paathukkaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathiyama naanum paathukkaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga vazhkkaya vazhnthapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga vazhkkaya vazhnthapadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukki unn kirukkal ezhuthulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukki unn kirukkal ezhuthulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukka enna neeyum paathivacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukka enna neeyum paathivacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil irukkira aasaiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil irukkira aasaiyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukka naan ummela kaattiputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukka naan ummela kaattiputten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru meengal ooru odaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru meengal ooru odaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneeril thannai izhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril thannai izhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal en kaaviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal en kaaviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodu naan kaikorkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodu naan kaikorkka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna maranthen enna maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna maranthen enna maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama naan unnil vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama naan unnil vizhunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna maranthen enna maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna maranthen enna maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama naan unnil vizhunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama naan unnil vizhunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaandi enakulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaandi enakulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaan naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaan naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaane unpulla enpulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaane unpulla enpulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannathula vizhukuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannathula vizhukuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirikkaiyila valithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirikkaiyila valithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjukkulla katharumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjukkulla katharumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee azhugaiyila azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee azhugaiyila azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee poranthathu athisayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee poranthathu athisayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam un paasathil theriyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam un paasathil theriyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilave en vaazhkkaiyil olimayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilave en vaazhkkaiyil olimayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaay en vaazhkkaiyum maaruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaay en vaazhkkaiyum maaruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagame suzhaluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagame suzhaluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal thaan podhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal thaan podhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Osara nee parakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osara nee parakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura nee tharuviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura nee tharuviyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukkullathaan kaathalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkullathaan kaathalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathukki vachen unnaalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathukki vachen unnaalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kundumuzhiyil thirudiputte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kundumuzhiyil thirudiputte"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaathalaiyum kannaalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaathalaiyum kannaalathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaan nee thaan nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan nee thaan nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaandi enakkulla enakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaandi enakkulla enakkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaan naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaan naanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaane unpulla enpulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaane unpulla enpulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaandi enakkulla enakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaandi enakkulla enakkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaane unpulla enpulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaane unpulla enpulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyama naan sollurandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama naan sollurandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un parvai aala thookkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un parvai aala thookkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathiyaama naanum paathukkaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathiyaama naanum paathukkaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga vazhkkaya vazhnthapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga vazhkkaya vazhnthapadi"/>
</div>
</pre>
