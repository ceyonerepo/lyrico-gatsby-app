---
title: "neenga mudiyuma ninaivu thoonguma song lyrics"
album: "Psycho"
artist: "Ilayaraja"
lyricist: "Kabilan"
director: "Mysskin"
path: "/albums/psycho-song-lyrics"
song: "Neenga Mudiyuma Ninaivu Thoonguma"
image: ../../images/albumart/psycho.jpg
date: 2020-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/C3az3JlJvbg"
type: "Sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neenga Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu Thoonguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Thoonguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu Thoonguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Thoonguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Maarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Maarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Aarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Aarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Pirindha Megamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Pirindha Megamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Unaku Sogamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Unaku Sogamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Poyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Poyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Saagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Saagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatraagave Netraagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraagave Netraagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ponathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ponathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Pogum Naal Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogum Naal Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Theduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Meendum Paarthapin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Meendum Paarthapin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu Thoonguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Thoonguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhevan Eendra Jeevanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevan Eendra Jeevanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Unnai Vendumendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Unnai Vendumendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaanam Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanam Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kangal Thedum Vazhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kangal Thedum Vazhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Karunai Konda Mazhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Karunai Konda Mazhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mazhalai Pesum Mozhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mazhalai Pesum Mozhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manadhai Neitha Izhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manadhai Neitha Izhaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesum Thendral Ennai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Thendral Ennai Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagi Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagi Pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona Thendral Endru Enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Thendral Endru Enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasam Aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasam Aagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhiyile Oru Kanavena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhiyile Oru Kanavena"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Thodaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Thodaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu Thoonguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Thoonguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Maarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Maarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Aarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Aarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moondru Kaalil Kadhal Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondru Kaalil Kadhal Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandu Iravu Irundha Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandu Iravu Irundha Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavai Ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavai Ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kadanthu Pona Dhisaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kadanthu Pona Dhisaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ketka Marandha Isaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketka Marandha Isaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Deivam Thedum Silaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Deivam Thedum Silaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Meetka Enna Vilaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Meetka Enna Vilaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Illai Nee Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Illai Nee Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthu Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Vaazha Naalai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Vaazha Naalai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetka Varugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetka Varugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thanimaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thanimaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thanimaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thanimaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Inaiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Inaiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu Thoonguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Thoonguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Maarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Maarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Aarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Aarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Pirindha Megamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Pirindha Megamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil Unaku Sogamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Unaku Sogamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Poyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Poyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Saagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Saagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatraagave Netraagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraagave Netraagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ponathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ponathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Pogum Naal Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogum Naal Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Theduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Meendum Paarthapin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Meendum Paarthapin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivu Thoonguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Thoonguma"/>
</div>
</pre>
