---
title: "goppadhi ra manishi puttuka song lyrics"
album: "Tholu Bommalata"
artist: "Suresh Bobbili"
lyricist: "Chaitanya Prasad"
director: "Viswanath Maganti"
path: "/albums/tholu-bommalata-lyrics"
song: "Goppadhi Ra Manishi Puttuka"
image: ../../images/albumart/tholu-bommalata.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/W6v932cXX4w"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oo Goppadi Raa Manishi Puttuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Goppadi Raa Manishi Puttuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Tappaduraa Tirigi Vellaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappaduraa Tirigi Vellaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Madhyalo Jeevithamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Madhyalo Jeevithamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagunu O Kadhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagunu O Kadhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Nela Meeda Jeevayathralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Nela Meeda Jeevayathralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaranthata Yenni Pathralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaranthata Yenni Pathralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kadhaye Gatha Charitrayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kadhaye Gatha Charitrayai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchiki Cherunugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchiki Cherunugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayinavalle Vellipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayinavalle Vellipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthu Leni Gunde Kothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthu Leni Gunde Kothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marchalemu Brahma Rathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marchalemu Brahma Rathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadasaariga Mogunu Bhagavadgithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadasaariga Mogunu Bhagavadgithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Runabandham Theerchendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Runabandham Theerchendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadatherchendhuku Dari Jerchendhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadatherchendhuku Dari Jerchendhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veduka Laaga Chese Kaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veduka Laaga Chese Kaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Jeevai Bathikina Chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Jeevai Bathikina Chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Okate Avvalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Okate Avvalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghana Nivvalule Ika Ivvalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Nivvalule Ika Ivvalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevithaminthe Navvu Yedpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevithaminthe Navvu Yedpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saage Tholu Bommalata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saage Tholu Bommalata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dandalu Pettina Dandalni Vesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandalu Pettina Dandalni Vesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Bomma Maa Edalo Undanile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Bomma Maa Edalo Undanile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pindalu Pettina Potharlu Vesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pindalu Pettina Potharlu Vesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aakale Gnapakamundanile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aakale Gnapakamundanile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaara Vidiche Nuvvula Neellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaara Vidiche Nuvvula Neellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Chivari Veedukollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Chivari Veedukollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jerigipovu Aanavallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jerigipovu Aanavallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Teeyani Smruthulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Teeyani Smruthulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadisenu Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadisenu Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Maa Gundello Unnavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Maa Gundello Unnavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathikunnavani Bathikuntavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathikunnavani Bathikuntavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanni Nammi Chese Kaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanni Nammi Chese Kaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Jeevai Nadichina Chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Jeevai Nadichina Chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Okate Avvalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Okate Avvalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghana Nivvalule Ika Ivvalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Nivvalule Ika Ivvalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevithaminthe Navvu Yedpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevithaminthe Navvu Yedpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saage Tholu Bommalata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saage Tholu Bommalata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daanalu Chesina Dharmalu Chesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daanalu Chesina Dharmalu Chesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeloni Prema Gunamunu Theluputake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeloni Prema Gunamunu Theluputake"/>
</div>
<div class="lyrico-lyrics-wrapper">Annalu Pettina Vasthralu Panchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annalu Pettina Vasthralu Panchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aathmaku Shanthini Koorchutake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aathmaku Shanthini Koorchutake"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choosi Navvinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choosi Navvinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Tappe Pattinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Tappe Pattinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vipputaaru Nedu Nollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vipputaaru Nedu Nollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekunnayantu Chaaredu Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekunnayantu Chaaredu Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Maaku Panchalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Maaku Panchalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinchalani Deevinchalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinchalani Deevinchalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Punya Lokam Cherche Kaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Punya Lokam Cherche Kaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeve Jeevai Bathikina Chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Jeevai Bathikina Chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Okate Avvalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Okate Avvalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghana Nivvalule Ika Ivvalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghana Nivvalule Ika Ivvalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevithaminthe Navvu Yedpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevithaminthe Navvu Yedpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saage Tholu Bommalata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saage Tholu Bommalata"/>
</div>
</pre>
