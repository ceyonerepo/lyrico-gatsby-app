---
title: "kamma karaioram song lyrics"
album: "Kannakkol"
artist: "Bobby"
lyricist: "Muthuvijayan"
director: "V.A.Kumaresan"
path: "/albums/kannakkol-lyrics"
song: "Kamma Karaioram"
image: ../../images/albumart/kannakkol.jpg
date: 2018-06-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-f5juc-hu_8"
type: "happy"
singers:
  - Jose
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">un mela un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela un mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela un mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kamma karaioram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamma karaioram"/>
</div>
<div class="lyrico-lyrics-wrapper">kokku onnu ninnu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokku onnu ninnu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">koku suda koku suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koku suda koku suda"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kutha vachu kathu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kutha vachu kathu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">sutu thallatha kundu rendu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutu thallatha kundu rendu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">unna elutha athu etti paraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna elutha athu etti paraku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kamma karaioram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamma karaioram"/>
</div>
<div class="lyrico-lyrics-wrapper">kokku onnu ninnu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokku onnu ninnu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">koku suda koku suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koku suda koku suda"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kutha vachu kathu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kutha vachu kathu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karuku vanam thungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuku vanam thungu"/>
</div>
<div class="lyrico-lyrics-wrapper">sketch potu athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketch potu athu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelambi kontandhu peggu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelambi kontandhu peggu"/>
</div>
<div class="lyrico-lyrics-wrapper">kootathil alli kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootathil alli kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiruki pola than thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiruki pola than thirudan"/>
</div>
<div class="lyrico-lyrics-wrapper">police game aetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="police game aetru"/>
</div>
<div class="lyrico-lyrics-wrapper">game aetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="game aetru"/>
</div>
<div class="lyrico-lyrics-wrapper">athu namathu thesis il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu namathu thesis il"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla vilaiyata sattam iyatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla vilaiyata sattam iyatru"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam iyatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam iyatru"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalil paathi athu en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalil paathi athu en"/>
</div>
<div class="lyrico-lyrics-wrapper">munnal meethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnal meethi"/>
</div>
<div class="lyrico-lyrics-wrapper">poovathal vethi thodati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovathal vethi thodati"/>
</div>
<div class="lyrico-lyrics-wrapper">pogatha vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogatha vaari"/>
</div>
<div class="lyrico-lyrics-wrapper">court suite odu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="court suite odu "/>
</div>
<div class="lyrico-lyrics-wrapper">rap paatu odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rap paatu odu"/>
</div>
<div class="lyrico-lyrics-wrapper">night welcome sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night welcome sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kamma karaioram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamma karaioram"/>
</div>
<div class="lyrico-lyrics-wrapper">kokku onnu ninnu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokku onnu ninnu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">koku suda koku suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koku suda koku suda"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kutha vachu kathu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kutha vachu kathu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manam pona pokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam pona pokku"/>
</div>
<div class="lyrico-lyrics-wrapper">panam pathu enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam pathu enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">romba naal aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba naal aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai enga pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai enga pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi pochu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi pochu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">elunthu parpom nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthu parpom nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illana nillana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illana nillana "/>
</div>
<div class="lyrico-lyrics-wrapper">endrume solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrume solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katha kathaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha kathaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">karancy note aduki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karancy note aduki"/>
</div>
<div class="lyrico-lyrics-wrapper">road podu road podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="road podu road podu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum nagar valam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum nagar valam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu povene tholiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu povene tholiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">tholiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">singa paalodu thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singa paalodu thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">paspathai iravil neetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paspathai iravil neetu"/>
</div>
<div class="lyrico-lyrics-wrapper">iravil neetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravil neetu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaama nerathil kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaama nerathil kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthalu kathavai pootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthalu kathavai pootu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathavai pootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathavai pootu"/>
</div>
<div class="lyrico-lyrics-wrapper">muttrathil thulasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttrathil thulasi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee muthathin arasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee muthathin arasi"/>
</div>
<div class="lyrico-lyrics-wrapper">paapom vaa urasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapom vaa urasi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pathikum manusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pathikum manusi"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiye viratum kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiye viratum kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">rajangam kaati aalgindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rajangam kaati aalgindra"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti kaarangal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti kaarangal than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kamma karaioram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamma karaioram"/>
</div>
<div class="lyrico-lyrics-wrapper">kokku onnu ninnu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokku onnu ninnu iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">koku suda koku suda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koku suda koku suda"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kutha vachu kathu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kutha vachu kathu iruku"/>
</div>
</pre>
