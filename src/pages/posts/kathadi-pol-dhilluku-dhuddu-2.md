---
title: "kathadi pol song lyrics"
album: "Dilluku Dhuddu 2"
artist: "Shabir"
lyricist: "Arun Bharathi"
director: "Rambhala"
path: "/albums/dhilluku-dhuddu-2-lyrics"
song: "Kathadi Pol"
image: ../../images/albumart/dhilluku-dhuddu-2.jpg
date: 2019-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/y75USabB3yQ"
type: "love"
singers:
  - Maria Vincent
  - Shabir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Idhuvarai Idhuvarai Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Idhuvarai Idhuvarai Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Irunthathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Irunthathu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya Pesi Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Pesi Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya Sirichithalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Sirichithalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavaram Nenjil Nadathiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavaram Nenjil Nadathiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagula Sadugudu Aadiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Sadugudu Aadiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounatha Pesa Sollu Sollu Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounatha Pesa Sollu Sollu Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kathadi Pol Maari Ponendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kathadi Pol Maari Ponendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pathathum Gaaliaanendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pathathum Gaaliaanendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathadi Pol Maari Ponendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathadi Pol Maari Ponendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pathathum Gaaliaanendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pathathum Gaaliaanendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Adai Mazhaiyinil Oru Kodaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Adai Mazhaiyinil Oru Kodaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum Poovum Thanimayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Poovum Thanimayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Kaatrile Maram Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Kaatrile Maram Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Vizhunthen Azhagunile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Vizhunthen Azhagunile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral Koththuthaan Manam Parkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Koththuthaan Manam Parkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam Unnai Thinnuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam Unnai Thinnuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Polave Kuzhanthai Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Polave Kuzhanthai Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Idhayam Thudikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Idhayam Thudikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu Pottuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Pottuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Saachitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Saachitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketch Pottuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketch Pottuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Thookitom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Thookitom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Parthathume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Parthathume"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennai Marakkuren Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennai Marakkuren Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathadi Pol Maari Ponendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathadi Pol Maari Ponendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pathathum Gaaliaanendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pathathum Gaaliaanendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathadi Pol Maari Ponendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathadi Pol Maari Ponendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pathathum Gaaliaanendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pathathum Gaaliaanendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Ohhh Ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Ohhh Ohhh"/>
</div>
</pre>
