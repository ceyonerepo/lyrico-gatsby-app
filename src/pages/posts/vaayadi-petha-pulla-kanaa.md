---
title: "vaayadi petha pulla song lyrics"
album: "kanaa"
artist: "Dhibu Ninan Thomas"
lyricist: "GKB"
director: " Arunraja Kamaraj"
path: "/albums/kanaa-lyrics"
song: "Vaaadi Petha Pulla"
image: ../../images/albumart/kanaa.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O0-lHTw3nLU"
type: "Affection"
singers:
  - Aaradhana SivaKarthikeyan
  - Sivakarthikeyan
  - Vaikom Vijayalakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaayadi Petha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayadi Petha Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vara Pora Nella Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Pora Nella Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar iva Yar iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar iva Yar iva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyila Suththura Kaaththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Suththura Kaaththaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththula Aaduthu Kooththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththula Aaduthu Kooththaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula Colora Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Colora Kannaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambukku Vandhu Nippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambukku Vandhu Nippaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar iva Yaar iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar iva Yaar iva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar indha Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar indha Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandha Poomaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Poomaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal Mattum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal Mattum illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settaikkellaam Sondhakkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settaikkellaam Sondhakkaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar indha Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar indha Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Konjum En Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Konjum En Maga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endhan Saamidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Saamidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Peththa Chinna Thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Peththa Chinna Thaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anna Kiliye Vanna Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna Kiliye Vanna Kuyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti Kurumbe Katti Karumbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Kurumbe Katti Karumbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella Kiliye Chinna Chilaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Kiliye Chinna Chilaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appan Nagalaa Pirandhavalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Nagalaa Pirandhavalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanukku Aasthiyum Naandhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanukku Aasthiyum Naandhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiya Vandhe Porandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya Vandhe Porandhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaththil Pattamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththil Pattamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osarakka Parandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osarakka Parandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkirukkum Kanavu Ellamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkirukkum Kanavu Ellamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavukitta Solli Vaippene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavukitta Solli Vaippene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasaththil Vilaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaththil Vilaiyura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayala Pol Iruppene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayala Pol Iruppene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaahaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu Pulla Nenapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Pulla Nenapula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi Enakkila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Enakkila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Sirippula Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Sirippula Mayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillu Kodaikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillu Kodaikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Panjamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Panjamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi Minnal iva Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Minnal iva Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paattu Katti Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Katti Aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar indha Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar indha Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaana Nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal Mattum illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal Mattum illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Maga Enna Senjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Maga Enna Senjaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhatta Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhatta Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Pada Maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Pada Maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maga Aambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maga Aambala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththukku Samandhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththukku Samandhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevuthu Mela Pandha Poladhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevuthu Mela Pandha Poladhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saniyaiyum Solatti Adippaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saniyaiyum Solatti Adippaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiya koodavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiya koodavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annana Nenaippaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana Nenaippaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavume Chella Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavume Chella Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaattu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaattu Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Suzhi Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Suzhi Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peththavanga Mogaththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththavanga Mogaththula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sirippula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Ponnu Aayul Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Ponnu Aayul Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodikittu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodikittu Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaayadi Petha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayadi Petha Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vara Pora Nella Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Pora Nella Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar iva Yar iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar iva Yar iva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyila Suththura Kaaththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Suththura Kaaththaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththula Aaduthu Kooththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththula Aaduthu Kooththaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula Colora Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Colora Kannaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambukku Vandhu Nippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambukku Vandhu Nippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar iva Yaar iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar iva Yaar iva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar indha Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar indha Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandha Poomaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Poomaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal Mattum illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal Mattum illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settaikkellaam Sondhakkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settaikkellaam Sondhakkaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yar indha Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yar indha Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Konjum En Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Konjum En Maga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endhan Saamidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Saamidhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Peththa Chinna Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Peththa Chinna Thaaye"/>
</div>
</pre>
