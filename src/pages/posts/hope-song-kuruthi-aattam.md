---
title: "hope song song lyrics"
album: "Kuruthi Aattam"
artist: "Yuvan Shankar Raja"
lyricist: "Karthik Netha"
director: "Sri Ganesh"
path: "/albums/kuruthi-aattam-lyrics"
song: "Hope Song"
image: ../../images/albumart/kuruthi-aattam.jpg
date: 2022-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xDsS2n8Dx8E"
type: "hope song"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yarodum pesi vidum poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarodum pesi vidum poove"/>
</div>
<div class="lyrico-lyrics-wrapper">peranbil unnai kanden 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peranbil unnai kanden "/>
</div>
<div class="lyrico-lyrics-wrapper">nane nane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nane "/>
</div>
<div class="lyrico-lyrics-wrapper">innoru jeevan nalam vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innoru jeevan nalam vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">viluntha kaneer athu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viluntha kaneer athu "/>
</div>
<div class="lyrico-lyrics-wrapper">thene thene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thene thene"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeengal theerthu vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeengal theerthu vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanil minmini poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanil minmini poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">ondru poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondru poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mele mele vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mele mele vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">peranbin vaale vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peranbin vaale vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un chutti thanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un chutti thanam"/>
</div>
<div class="lyrico-lyrics-wrapper">kallai chudar aakiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallai chudar aakiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peiyattam podugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyattam podugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">soorai kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorai kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">chinnoondu pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinnoondu pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatu paadal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatu paadal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal varum velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal varum velai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavalai madi sainthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavalai madi sainthidalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">avatharam pole unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avatharam pole unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">maatri vidum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatri vidum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">aritharam pona pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aritharam pona pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">eeram sonnan gnanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeram sonnan gnanam"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvorathil pon manjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvorathil pon manjal"/>
</div>
<div class="lyrico-lyrics-wrapper">malar veezhum atharkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malar veezhum atharkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">kai than poovai perume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai than poovai perume"/>
</div>
<div class="lyrico-lyrics-wrapper">senthazhalil mothidum kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthazhalil mothidum kaatre"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna kuzhalai kandu sergirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna kuzhalai kandu sergirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vambalanthidum intha man mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambalanthidum intha man mele"/>
</div>
<div class="lyrico-lyrics-wrapper">anbondrai katti kondu nee thoongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbondrai katti kondu nee thoongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peiyattam podugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyattam podugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">soorai kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorai kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">chinnoondu pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinnoondu pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatu paadal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatu paadal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal varum velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal varum velai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavalai madi sainthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavalai madi sainthidalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru paathi mullai kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paathi mullai kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">ooda solli thoondum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooda solli thoondum"/>
</div>
<div class="lyrico-lyrics-wrapper">maru pathai pallam vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru pathai pallam vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">thaava solli ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaava solli ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadikarangal porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadikarangal porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu serkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu serkum"/>
</div>
<div class="lyrico-lyrics-wrapper">pidivathangal than vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidivathangal than vesam"/>
</div>
<div class="lyrico-lyrics-wrapper">vidume nambikaiyile ezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidume nambikaiyile ezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">poovil kollai alagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovil kollai alagu "/>
</div>
<div class="lyrico-lyrics-wrapper">athai paarthaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai paarthaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">pallam niraiyum mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallam niraiyum mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">neer mele ammadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer mele ammadi "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillai paarthaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillai paarthaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peiyattam podugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyattam podugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">soorai kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorai kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">chinnoondu pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinnoondu pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatu paadal ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatu paadal ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal varum velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal varum velai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavalai madi sainthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavalai madi sainthidalam"/>
</div>
</pre>
