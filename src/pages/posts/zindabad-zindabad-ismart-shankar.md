---
title: "zindabad zindabad song lyrics"
album: "Ismart Shankar"
artist: "Mani Sharma"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Puri Jagannadh"
path: "/albums/ismart-shankar-lyrics"
song: "Zindabad Zindabad"
image: ../../images/albumart/ismart-shankar.jpg
date: 2019-07-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ce-K2qvKmzY"
type: "love"
singers:
  - Sarath Santhosh
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Zindabad zindabad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindabad zindabad"/>
</div>
<div class="lyrico-lyrics-wrapper">Erraani pedavulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erraani pedavulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindabad zindabad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindabad zindabad"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurraadi choopulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurraadi choopulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vahvaa va va va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vahvaa va va va va va"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka muddhu appu kavaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka muddhu appu kavaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vahvaa va va va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vahvaa va va va va va"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirgi ichesthavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirgi ichesthavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arerey okatiki naalugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arerey okatiki naalugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddetho ishtaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddetho ishtaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhave kevvu kekalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhave kevvu kekalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peduthunna vadhalanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peduthunna vadhalanule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumpa thenchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumpa thenchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumaaramedo repaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumaaramedo repaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchesaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchesaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolamedho thechaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolamedho thechaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumpa thenchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumpa thenchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumaaramedo repaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumaaramedo repaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchesaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchesaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolamedho thechaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolamedho thechaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindabad zindabad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindabad zindabad"/>
</div>
<div class="lyrico-lyrics-wrapper">Erraani pedavulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erraani pedavulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindabad zindabad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindabad zindabad"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurraadi choopulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurraadi choopulaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholisari gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholisari gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarige dhaarunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarige dhaarunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sogasey karanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sogasey karanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadagalla vaana laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadagalla vaana laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvey dhookadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvey dhookadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Authundhaa aapadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Authundhaa aapadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhilo nippulu puttadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhilo nippulu puttadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragadam jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragadam jagadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalilo chamatalu kakkadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalilo chamatalu kakkadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maha baagundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maha baagundhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumpa thenchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumpa thenchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumaaramedo repaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumaaramedo repaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchesaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchesaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolamedho thechaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolamedho thechaavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumpa thenchesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumpa thenchesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumaaramedo repaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumaaramedo repaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kompa munchesaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kompa munchesaavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolamedho thechaavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolamedho thechaavey"/>
</div>
</pre>
