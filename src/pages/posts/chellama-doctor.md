---
title: "chellama chellama song lyrics"
album: "Doctor"
artist: "Anirudh"
lyricist: "Sivakarthikeyan"
director: "Nelson Dilipkumar"
path: "/albums/doctor-song-lyrics"
song: "Chellama Chellama"
image: ../../images/albumart/doctor.jpg
date: 2021-10-09
lang: tamil
singers:
  - Anirudh
  - Jonita Gandhi
youtubeLink: "https://www.youtube.com/embed/FyF9CRGb2VU"
type: "love"
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inime Tik Tok ellam inga ban’u ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Inime Tik Tok ellam inga ban’u ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nera duet paada vaayen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Nera duet paada vaayen ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba strict’ah irundhathellam podhumma
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba strict’ah irundhathellam podhumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sweet’ah sirichu pesaen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam sweet’ah sirichu pesaen ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamma chellamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Chellamma chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Angam minnum thangamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Angam minnum thangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnamma mellamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnamma mellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti killenma
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti killenma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu rendum gun amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu rendum gun amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjama konjima
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjama konjima"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu thallenma
<input type="checkbox" class="lyrico-select-lyric-line" value="Suttu thallenma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polladha vayasa seendi thaan ponaye
<input type="checkbox" class="lyrico-select-lyric-line" value="Polladha vayasa seendi thaan ponaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduthaalum unakke vizhuven naane
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaduthaalum unakke vizhuven naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi manasa kal veesi paathaye
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannadi manasa kal veesi paathaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Odanjaalum kaatuven unnai naane
<input type="checkbox" class="lyrico-select-lyric-line" value="Odanjaalum kaatuven unnai naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mezhugu doll’u nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Mezhugu doll’u nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu school’u nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagu school’u nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku yethava neethandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku yethava neethandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Handsome aalu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Handsome aalu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Super cool’u nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Super cool’u nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum neeyum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma jodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Semma jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhuva dhoni pola naanum calm’u ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhuva dhoni pola naanum calm’u ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikki excitement aanen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Innikki excitement aanen ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannal valaya veesi ennai thookku ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannal valaya veesi ennai thookku ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Life time settlement’u naan thaan ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Life time settlement’u naan thaan ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime Tik Tok ellam inga ban’u ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Inime Tik Tok ellam inga ban’u ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nera duet paada vaayen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Nera duet paada vaayen ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba strict’ah irundhathellam podhumma
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba strict’ah irundhathellam podhumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sweet’ah sirichu pesaen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam sweet’ah sirichu pesaen ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo kudaila neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Ayyayo kudaila neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaye mazhaiyena neeyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhaye mazhaiyena neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjodu izhukkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjodu izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell’odu orasura
<input type="checkbox" class="lyrico-select-lyric-line" value="Cell’odu orasura"/>
</div>
<div class="lyrico-lyrics-wrapper">Hormone’il kalakkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Hormone’il kalakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirkka vakkiriye
<input type="checkbox" class="lyrico-select-lyric-line" value="Silirkka vakkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallana manasa thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallana manasa thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill’ana sirippula
<input type="checkbox" class="lyrico-select-lyric-line" value="Chill’ana sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallave karaikkira
<input type="checkbox" class="lyrico-select-lyric-line" value="Nallave karaikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasiyam vakkiriye
<input type="checkbox" class="lyrico-select-lyric-line" value="Vasiyam vakkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjala kekkum un vaarthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjala kekkum un vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha korpene
<input type="checkbox" class="lyrico-select-lyric-line" value="Adha korpene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai vaarpene
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavithai vaarpene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnala thaakkum un kannula maiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnala thaakkum un kannula maiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuvene
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga thozhuvene
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhaga thozhuvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polladha vayasa seendi thaan ponaye
<input type="checkbox" class="lyrico-select-lyric-line" value="Polladha vayasa seendi thaan ponaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduthaalum unakke vizhuven naane
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaduthaalum unakke vizhuven naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi manasa kal veesi paathaye
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaadi manasa kal veesi paathaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Odanjaalum kaatuven unnai naane
<input type="checkbox" class="lyrico-select-lyric-line" value="Odanjaalum kaatuven unnai naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamma…
<input type="checkbox" class="lyrico-select-lyric-line" value="Chellamma…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugu doll’u nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Mezhugu doll’u nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu school’u nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagu school’u nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku yethava neethandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku yethava neethandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Handsome aalu nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Handsome aalu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Super cool’u nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Super cool’u nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum neeyum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma jodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Semma jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhuva dhoni pola naanum calm’u ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhuva dhoni pola naanum calm’u ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikki excitement aanen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Innikki excitement aanen ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannal valaya veesi ennai thookku ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannal valaya veesi ennai thookku ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Life time settlement’u naan thaan ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Life time settlement’u naan thaan ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inime Tik Tok ellam inga ban’u ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Inime Tik Tok ellam inga ban’u ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nera duet paada vaayen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Nera duet paada vaayen ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba strict’ah irundhathellam podhumma
<input type="checkbox" class="lyrico-select-lyric-line" value="Romba strict’ah irundhathellam podhumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sweet’ah sirichu pesaen ma
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam sweet’ah sirichu pesaen ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamma chellamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Chellamma chellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Angam minnum thangamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Angam minnum thangamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnamma mellamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnamma mellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti killenma
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti killenma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu rendum gun amma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu rendum gun amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjama konjima
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjama konjima"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu thallenma
<input type="checkbox" class="lyrico-select-lyric-line" value="Suttu thallenma"/>
</div>
</pre>
