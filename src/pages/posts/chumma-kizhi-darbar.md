---
title: 'chumma kizhi song lyrics'
album: 'Darbar'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'A R Murugadoss'
path: '/albums/darbar-song-lyrics'
song: 'Chumma Kizhi'
image: ../../images/albumart/darbar.jpg
date: 2020-01-09
lang: tamil
singers:
- S P Balasubramaniam
youtubeLink: "https://www.youtube.com/embed/w0nyEoXBEiA"
type: 'hero entry'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Naan thaan daa inimelu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan daa inimelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninna darbaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthu ninna darbaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda gangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan da lead-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan da lead-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Billaa en varalaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Billaa en varalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathavan naan palaperu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathavan naan palaperu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda pettaikku
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda pettaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan da lord-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan da lord-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ragala uttaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Ragala uttaakka"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ra   Raakunakukkurum ellamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ra   Raakunakukkurum ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukkaa vanthaakaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinukkaa vanthaakaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ji   Jikkunukkurumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ji   Jikkunukkurumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirutha paathaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirutha paathaaka"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ni   Ninikunukkurum thodathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ni   Ninikunukkurum thodathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichaa enaava dandanakkanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichaa enaava dandanakkanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Rajini
<input type="checkbox" class="lyrico-select-lyric-line" value="Rajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppu peroda
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu peroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kudutha star-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kudutha star-oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikkum raja naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Innikkum raja naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettupaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettupaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chumma kizhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karuppu tholoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppu tholoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam varum scene-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Singam varum scene-oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idamae paththikkum anthamaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Idamae paththikkum anthamaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma kizhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan thaan daa inimelu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan daa inimelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninna darbaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthu ninna darbaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda gangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan da lead-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan da lead-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Billaa en varalaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Billaa en varalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathavan naan palaperu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathavan naan palaperu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda pettaikku
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda pettaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan da lord-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan da lord-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ranagalam sagala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ranagalam sagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saab ragala namma annaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Saab ragala namma annaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan limit alarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan limit alarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu kada podaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu kada podaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Onnu vutta sevulu
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnu vutta sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thavulu nee ennaattha
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan thavulu nee ennaattha"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan konjam moradan
<input type="checkbox" class="lyrico-select-lyric-line" value="Annan konjam moradan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rayil otaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee rayil otaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kizhi ………………….
<input type="checkbox" class="lyrico-select-lyric-line" value="Kizhi …………………."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nerma unakirundhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nerma unakirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Style-lo style-uu
<input type="checkbox" class="lyrico-select-lyric-line" value="Style-lo style-uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva therikkuthunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarva therikkuthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Whistle- oh whistle-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Whistle- oh whistle-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nambum manasiruntha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nambum manasiruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Style-lo style-uu
<input type="checkbox" class="lyrico-select-lyric-line" value="Style-lo style-uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam pirikuthunna
<input type="checkbox" class="lyrico-select-lyric-line" value="Vegam pirikuthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalo-oh puyal-uh
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyalo-oh puyal-uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Irumbu sogamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Irumbu sogamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaa katti ukaarnthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyaa katti ukaarnthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odanji thurumbaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Odanji thurumbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillu sillaa kottum paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Sillu sillaa kottum paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uzhaippa mathichi
<input type="checkbox" class="lyrico-select-lyric-line" value="Uzhaippa mathichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal eduthu vechaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaal eduthu vechaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai muzhusaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilamai muzhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae ottum paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Un koodavae ottum paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ragala uttaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Ragala uttaakka"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ra   Raakunakukkurum ellamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ra   Raakunakukkurum ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinukkaa vanthaakaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinukkaa vanthaakaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ji   Jikkunukkurumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ji   Jikkunukkurumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirutha paathaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirutha paathaaka"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ni   Ninikunukkurum thodathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ni   Ninikunukkurum thodathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichaa enaava dandanakkanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichaa enaava dandanakkanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Rajini
<input type="checkbox" class="lyrico-select-lyric-line" value="Rajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppu peroda
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu peroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kudutha star-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kudutha star-oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikkum raja naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Innikkum raja naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettupaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettupaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chumma kizhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karuppu tholoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppu tholoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam varum scene-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Singam varum scene-oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idamae paththikkum anthamaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Idamae paththikkum anthamaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma kizhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan thaan daa inimelu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thaan daa inimelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu ninna darbaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthu ninna darbaru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey…(4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Billaa en varalaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Billaa en varalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathavan naan palaperu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathavan naan palaperu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey…(4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey…"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ranagalam sagala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ranagalam sagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saab ragala namma annaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Saab ragala namma annaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan limit alarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan limit alarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu kada podaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu kada podaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Onnu vutta sevulu
<input type="checkbox" class="lyrico-select-lyric-line" value="Onnu vutta sevulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thavulu nee ennaattha
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan thavulu nee ennaattha"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan konjam moradan
<input type="checkbox" class="lyrico-select-lyric-line" value="Annan konjam moradan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rayil otaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee rayil otaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppu peroda
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu peroda"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ragala namma annaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ragala namma annaatha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee kudutha star-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kudutha star-oda"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kada podaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kada podaatha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Innikkum raja naan…aaa…aa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Innikkum raja naan…aaa…aa…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Thavulu nee ennaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavulu nee ennaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neruppu peroda
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppu peroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kudutha star-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kudutha star-oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Innikkum raja naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Innikkum raja naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettupaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettupaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Chumma kizhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karuppu tholoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppu tholoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam varum scene-oda
<input type="checkbox" class="lyrico-select-lyric-line" value="Singam varum scene-oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idamae paththikkum anthamaari da
<input type="checkbox" class="lyrico-select-lyric-line" value="Idamae paththikkum anthamaari da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajini Laughing  Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line" value="Rajini Laughing  Hahahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma Kizhi ………………….
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma Kizhi …………………."/>
</div>
</pre>