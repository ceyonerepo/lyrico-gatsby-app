---
title: "rangu rakkara song lyrics"
album: "Shivalinga"
artist: "S S Thaman"
lyricist: "Viveka"
director: "P Vasu"
path: "/albums/shivalinga-lyrics"
song: "Rangu Rakkara"
image: ../../images/albumart/shivalinga.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/usXxW1TnVyA"
type: "love"
singers:
  -	Anirudh Ravichander
  - Geetha Madhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaladikkum kannakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkum kannakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan irukken dontu worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan irukken dontu worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakethukku acham acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakethukku acham acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asaigala allow pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaigala allow pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Achangala delete pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achangala delete pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi varalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi varalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchcham uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchcham uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ai meow meow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ai meow meow"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bendha bendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bendha bendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulichikinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulichikinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beedhi adaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedhi adaiyura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Total aaga maari neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total aaga maari neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi trendya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi trendya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar notta kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar notta kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathirichu namma India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathirichu namma India"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma India"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma India"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaladikkum kannakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkum kannakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan irukken dontu worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan irukken dontu worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakethukku acham acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakethukku acham acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asaigala allow pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaigala allow pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Achangala delete pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achangala delete pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi varalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi varalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchcham uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchcham uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ai meow meow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ai meow meow"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bendha bendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bendha bendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulichikinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulichikinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beedhi adaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedhi adaiyura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Total aaga maari neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total aaga maari neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi trendya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi trendya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar notta kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar notta kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathirichu namma India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathirichu namma India"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palanathu venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palanathu venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha oththi vaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha oththi vaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu pallaarunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu pallaarunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathula mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula mutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore oru muthathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore oru muthathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyamaaten daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyamaaten daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna muthathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna muthathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Metha senji thoonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Metha senji thoonga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaippen daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaippen daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee rendaairayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rendaairayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rubaa note tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rubaa note tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan serthu vaikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan serthu vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa munnum pinnum kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa munnum pinnum kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu orginalaa doubtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu orginalaa doubtu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kindal ellaam cuteb tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kindal ellaam cuteb tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan aadiputen flatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aadiputen flatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee fix-u pannu date tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee fix-u pannu date tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaikkurenda treatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaikkurenda treatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaladikkum kannakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkum kannakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan irukken dontu worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan irukken dontu worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakethukku acham acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakethukku acham acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru sentimenta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sentimenta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jel aagi naan irupenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jel aagi naan irupenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee centimeter pirinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee centimeter pirinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thavippen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thavippen da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kendakaala paathidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kendakaala paathidave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjiduven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjiduven di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un konda poova kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un konda poova kondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kumbiduven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kumbiduven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee munnum pinnum massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee munnum pinnum massu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan mullillaadha rose su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan mullillaadha rose su"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa polam kaadhal race su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa polam kaadhal race su"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu ooru pooraa Newsu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ooru pooraa Newsu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee watermelon juice su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee watermelon juice su"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaatamaana glassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaatamaana glassu"/>
</div>
<div class="lyrico-lyrics-wrapper">En valibame ice su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En valibame ice su"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini vazhkai very nice su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini vazhkai very nice su"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaladikkum kannakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaladikkum kannakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan irukken dontu worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan irukken dontu worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakethukku acham acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakethukku acham acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asaigala allow pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaigala allow pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Achangala delete pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achangala delete pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi varalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi varalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchcham uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchcham uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ai meow meow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ai meow meow"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mersal aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bendha bendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bendha bendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulichikinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulichikinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beedhi adaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedhi adaiyura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Totalaaga maari neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Totalaaga maari neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi trendya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi trendya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar notta kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar notta kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathirichu namma India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathirichu namma India"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakkara rakkara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakkara rakkara raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rangu"/>
</div>
</pre>
