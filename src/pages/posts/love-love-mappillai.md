---
title: "love love song lyrics"
album: "Mappillai"
artist: "Mani Sharma"
lyricist: "Snehan"
director: "Suraj"
path: "/albums/mappillai-lyrics"
song: "Love Love"
image: ../../images/albumart/mappillai.jpg
date: 2011-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/y2IzhnOGbrw"
type: "love"
singers:
  - Rahul Nambiar
  - Rita Thyagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Love Love Love Lovudee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Love Lovudee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Laila Neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Laila Neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Nenjukul Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Nenjukul Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thil Thil Thil Thil Thilluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thil Thil Thil Thil Thilluda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Theendura Killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Theendura Killadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Va Va Paarkalam Vilaiyaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Va Va Paarkalam Vilaiyaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ultra Modela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ultra Modela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthaal Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaal Vennilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amstrong Muzhumaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amstrong Muzhumaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholainthaai Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholainthaai Vennilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cell Phona Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Phona Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Silunguraen Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Silunguraen Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sim Cardappola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sim Cardappola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaayaen Ullaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaayaen Ullaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rocket’tai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket’tai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Suthuraen Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Suthuraen Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satlaitaa Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satlaitaa Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Paaradi Kannaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Paaradi Kannaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Love Love Lovudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Love Lovudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Laila Neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Laila Neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Nenjukul Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Nenjukul Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thil Thil Thil Thil Thilluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thil Thil Thil Thil Thilluda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Theendura Killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Theendura Killadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Va Va Paarkalam Vilaiyaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Va Va Paarkalam Vilaiyaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minsaaram Paachura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaaram Paachura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poochedi Neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poochedi Neethaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minsaarappoovai Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaarappoovai Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthamiduvaen Naanthaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamiduvaen Naanthaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathalaal Seigindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalaal Seigindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayangal Valikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal Valikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavuley Tholainthaalkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuley Tholainthaalkooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Mattum Koraiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Mattum Koraiyaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennenna Inbam Saerthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Inbam Saerthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaithaan Neithaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaithaan Neithaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppothum Unai Sutra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Unai Sutra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Aanai Avane Seithaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Aanai Avane Seithaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cell Phona Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Phona Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Silunguraen Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Silunguraen Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sim Cardappola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sim Cardappola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaayaen Ullaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaayaen Ullaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rocket’tai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket’tai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Suthuraen Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Suthuraen Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satlaitaa Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satlaitaa Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Paaradi Kannaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Paaradi Kannaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Love Love Lovudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Love Lovudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Laila Neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Laila Neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jil Jil Jil Nenjukul Nee Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jil Jil Jil Nenjukul Nee Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velvettu Thaegathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvettu Thaegathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthathum Vizhunthaene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathum Vizhunthaene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalvettupola Unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvettupola Unthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Pattu Ezhunthaene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Pattu Ezhunthaene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomiya Thaandiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiya Thaandiyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poguraen Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguraen Thannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaeroru Graharathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeroru Graharathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhuraen Unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhuraen Unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaikku Rekka Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikku Rekka Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalum Parakkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Parakkaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angathil Asaivoam Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angathil Asaivoam Seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaeru Paadu Kidaiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeru Paadu Kidaiyaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cell Phona Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Phona Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Silunguraen Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Silunguraen Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sim Cardappola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sim Cardappola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaayaen Ullaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaayaen Ullaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rocket’tai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket’tai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Suthuraen Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Suthuraen Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satlaitaa Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satlaitaa Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Paaradi Kannaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Paaradi Kannaalaa"/>
</div>
</pre>
