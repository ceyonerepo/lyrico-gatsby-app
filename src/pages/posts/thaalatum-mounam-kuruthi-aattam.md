---
title: "thaalatum mounam song lyrics"
album: "Kuruthi Aattam"
artist: "Yuvan Shankar Raja"
lyricist: "Karthik Netha"
director: "Sri Ganesh"
path: "/albums/kuruthi-aattam-lyrics"
song: "Thaalatum Mounam"
image: ../../images/albumart/kuruthi-aattam.jpg
date: 2022-01-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BKhb5jmfz78"
type: "melody"
singers:
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaalatum mounam ondril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalatum mounam ondril"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan karainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan karainthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollatha anbin vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollatha anbin vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unarnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unarnthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo ennai kootti sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo ennai kootti sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathai neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum ennul vattam podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum ennul vattam podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal neethane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaramal unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaramal unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">paarpathai nee unarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarpathai nee unarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum athai moodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum athai moodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vaithu yenga vaipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithu yenga vaipai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalatum mounam ondril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalatum mounam ondril"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan karainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan karainthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai paarkum naalil mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai paarkum naalil mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhgindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhgindrene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo ennai kootti sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo ennai kootti sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathai neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum ennul vattam podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum ennul vattam podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal neethane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe unai naan kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe unai naan kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna verenna vendum ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna verenna vendum ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai ellam ponal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai ellam ponal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil aazhgindra maayam thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil aazhgindra maayam thani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppothum nee thantha paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum nee thantha paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippothum vaazhgindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippothum vaazhgindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneeril veezhgindra thooralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril veezhgindra thooralai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal neezhgindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal neezhgindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanivai malarum un varthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivai malarum un varthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragai enaye thetrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragai enaye thetrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada adada en naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada adada en naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru piravi ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru piravi ketkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo ennai kootti sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo ennai kootti sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathai neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum ennul vattam podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum ennul vattam podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadal neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal neethane"/>
</div>
</pre>
