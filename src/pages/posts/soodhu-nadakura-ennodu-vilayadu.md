---
title: "soodhu nadakura song lyrics"
album: "Ennodu Vilayadu"
artist: "A. Moses - Sudharshan M. Kumar"
lyricist: "Arunraja Kamaraj"
director: "Arun Krishnaswami"
path: "/albums/ennodu-vilayadu-lyrics"
song: "Soodhu Nadakura"
image: ../../images/albumart/ennodu-vilayadu.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YzXwWDm3ofE"
type: "mass"
singers:
  - Ramya NSK
  - Sudharshan M Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">soodhu nadakkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodhu nadakkura "/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">modha athukkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modha athukkoru"/>
</div>
<div class="lyrico-lyrics-wrapper">veeram venum thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram venum thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadhai marakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhai marakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooram thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooram thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">bodhai kidaikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhai kidaikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">oda oda thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda oda thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porkalam thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalam thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">potti poda vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potti poda vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">vittupudikaa speeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittupudikaa speeda"/>
</div>
<div class="lyrico-lyrics-wrapper">ettipudikka nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettipudikka nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa machan vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa machan vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">gajini unga dad ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gajini unga dad ah"/>
</div>
<div class="lyrico-lyrics-wrapper">tholvi ellam bad ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholvi ellam bad ah"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkira appa nee god ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkira appa nee god ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">soodhu nadakkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodhu nadakkura "/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">modha athukkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modha athukkoru"/>
</div>
<div class="lyrico-lyrics-wrapper">veeram venum thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram venum thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadhai marakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhai marakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooram thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooram thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">bodhai kidaikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhai kidaikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">oda oda thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda oda thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatangal ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatangal ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivil onnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivil onnudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadiye paarkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadiye paarkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyippan king da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyippan king da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatangal ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatangal ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivil onnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivil onnudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aadiye paarkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadiye paarkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyippan king da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyippan king da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaalum unnoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaalum unnoda"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum thallada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum thallada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanmoodi kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmoodi kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">kandupudi kootathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandupudi kootathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo nee poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo nee poga"/>
</div>
<div class="lyrico-lyrics-wrapper">ennanamo nee theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennanamo nee theda"/>
</div>
<div class="lyrico-lyrics-wrapper">ennangal kai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennangal kai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">ishtam pola inbam theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ishtam pola inbam theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa machan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa machan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ini vetri irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini vetri irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu ottumotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu ottumotha"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthagaiya kittam namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthagaiya kittam namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa machan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa machan "/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu satham ezhuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu satham ezhuppu"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ekkachakkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ekkachakkamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">varum thuttu namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum thuttu namakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">soodhu nadakkura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodhu nadakkura "/>
</div>
<div class="lyrico-lyrics-wrapper">neram thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">modha athukkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modha athukkoru"/>
</div>
<div class="lyrico-lyrics-wrapper">veeram venum thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeram venum thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadhai marakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhai marakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">dhooram thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhooram thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">bodhai kidaikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhai kidaikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">oda oda thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oda oda thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porkalam thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalam thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">potti poda vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potti poda vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">vittipuduchi speeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittipuduchi speeda"/>
</div>
<div class="lyrico-lyrics-wrapper">ettipudikka nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettipudikka nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa machan vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa machan vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">gajini unga dad ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gajini unga dad ah"/>
</div>
<div class="lyrico-lyrics-wrapper">tholvi ellam bad ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholvi ellam bad ah"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkira appa nee god ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkira appa nee god ah"/>
</div>
</pre>
