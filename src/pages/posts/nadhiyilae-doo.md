---
title: "nadhiyilae song lyrics"
album: "Doo"
artist: "Abhishek — Lawrence"
lyricist: "Na. Muthukumar"
director: "Sriram Padmanabhan"
path: "/albums/doo-lyrics"
song: "Nadhiyilae"
image: ../../images/albumart/doo.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dGFfCOYomjs"
type: "love"
singers:
  - Silambarasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nadhiyiley alai ondru marubadi midhakkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyiley alai ondru marubadi midhakkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">gnaabagathin kadhavugal adikkadi pirakkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gnaabagathin kadhavugal adikkadi pirakkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">sinna chinnadhaai undhan ennam nuzhandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinna chinnadhaai undhan ennam nuzhandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sithravadhaiyaai ennai kollugindradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithravadhaiyaai ennai kollugindradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">enna mandhiram iru kannil seigindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna mandhiram iru kannil seigindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam undhan vazhi sellugindradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam undhan vazhi sellugindradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru paarvaiyaaley poothadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paarvaiyaaley poothadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhal pookkal vizhiyoaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhal pookkal vizhiyoaram"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum vandhadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum vandhadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vandhadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vandhadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">naam pazhagiya naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam pazhagiya naatkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhu unmaiyil enna pirivin valithaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhu unmaiyil enna pirivin valithaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">penney penney pirivil naaney anbinai unarndheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penney penney pirivil naaney anbinai unarndheney"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam unnai ninaikkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam unnai ninaikkavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">manam adhai meendum thulirkkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam adhai meendum thulirkkavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanam indri ennai parakkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanam indri ennai parakkavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillil ennai nanaikka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillil ennai nanaikka vaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru paarvaiyaaley poothadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paarvaiyaaley poothadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhal pookkal vizhiyoaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhal pookkal vizhiyoaram"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum vandhadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum vandhadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vandhadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vandhadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">naam pazhagiya naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam pazhagiya naatkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaamboochi meendum endhan viralil varugiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaamboochi meendum endhan viralil varugiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalum iravum dhinam puriyaadha mayakkathai tharugiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum dhinam puriyaadha mayakkathai tharugiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nilaa poal nee adhai rasikkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaa poal nee adhai rasikkavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaakkalil ennai vasikkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaakkalil ennai vasikkavaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam ennul vandhu inikka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam ennul vandhu inikka vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">neram kaalam adhai marakkavaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram kaalam adhai marakkavaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru paarvaiyaaley poothadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paarvaiyaaley poothadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhal pookkal vizhiyoaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhal pookkal vizhiyoaram"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum vandhadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum vandhadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vandhadhu penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vandhadhu penney"/>
</div>
<div class="lyrico-lyrics-wrapper">naam pazhagiya naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam pazhagiya naatkal"/>
</div>
</pre>
