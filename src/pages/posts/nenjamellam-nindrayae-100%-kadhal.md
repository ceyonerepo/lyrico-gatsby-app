---
title: "nenjamellam nindrayae song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Nenjamellam Nindrayae"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DHUSw6lU0TA"
type: "sad"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Nenjellaam Nindraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenjellaam Nindraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivellaam Vendraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivellaam Vendraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Vittu Sendraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vittu Sendraaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmunne Vanthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunne Vanthaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannellaam Nindraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannellaam Nindraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaaga Kalainthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaaga Kalainthaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaithaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Vaanam Tholaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vaanam Tholaiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Poley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Kadhal Maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Kadhal Maaradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Bandham Pogaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Bandham Pogaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Seththu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Seththu Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Sondham Maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Sondham Maaradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Enna Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Enna Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Unnai Maravaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Unnai Maravaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kaayam Marainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaayam Marainthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhan Thazhumbo Maraiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Thazhumbo Maraiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpil Varum Sondhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpil Varum Sondhangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirinthaal Adhu Pirindhathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthaal Adhu Pirindhathu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravil Varum Sondhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravil Varum Sondhangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirinthaal Kooda Piriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthaal Kooda Piriyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Uravai Maatra Mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Uravai Maatra Mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallam Ondril Vizhunthaal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam Ondril Vizhunthaal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Theriyumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Theriyumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Adhu Udainthaal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Adhu Udainthaal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuyaram Puriyumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyaram Puriyumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nenjellaam Nindraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenjellaam Nindraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivellaam Vendraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivellaam Vendraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Sendraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Sendraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengaamale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmunne Vanthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunne Vanthaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannellaam Nindraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannellaam Nindraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaaga Kalainthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaaga Kalainthaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaithaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Vaanam Tholaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vaanam Tholaiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Poley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Kadhal Maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Kadhal Maaradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Vittu Ponaalum Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vittu Ponaalum Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Bandham Pogaadhu Pogaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Bandham Pogaadhu Pogaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Seththu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Seththu Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Sondham Maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Sondham Maaradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Enna Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Enna Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Unnai Maravaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Unnai Maravaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maravaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kaayam Marainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaayam Marainthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhan Thazhumbo Maraiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Thazhumbo Maraiyaathu"/>
</div>
</pre>
