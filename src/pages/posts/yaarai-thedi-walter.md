---
title: 'yaarai thedi song lyrics'
album: 'Walter'
artist: 'Dharma Prakash'
lyricist: 'Arun Bharathi'
director: 'U Anbu'
path: '/albums/walter-song-lyrics'
song: 'Yaarai Thedi Nenjamee'
image: ../../images/albumart/walter.jpg
date: 2020-03-13
lang: tamil
singers: 
- K S Chithra
youtubeLink: 'https://www.youtube.com/embed/Y7qlGFT5lwI'
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Yaarai thaedi nenjamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarai thaedi nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil jaadai pesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil jaadai pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai nerunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum ennai nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi saaral veesudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kodi saaral veesudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Megakoottam yaavumae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Megakoottam yaavumae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham keezhae oduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadham keezhae oduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalin meedhu kadanthu pogum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalin meedhu kadanthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiyaai nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaiyaai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kadanthaai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayam kadanthaai…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh hoi…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh hoi…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaarai thaedi nenjamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarai thaedi nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil jaadai pesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil jaadai pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai nerunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum ennai nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi saaral veesudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kodi saaral veesudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh thaavum kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh thaavum kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola nenjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Pola nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandu thaavi sella
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai kandu thaavi sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai thokki konja
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum ennai thokki konja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkum uyirai enna sollaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavikkum uyirai enna sollaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Iru vizhi indru
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru vizhi indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi pari seidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi pari seidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum undhan paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogum undhan paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru udal indru
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru udal indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru uyir endru
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru uyir endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagum undhan thendalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Aagum undhan thendalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Endhan aayul regai
<input type="checkbox" class="lyrico-select-lyric-line" value="Endhan aayul regai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kaiyil paarkkiren….ae…….ae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan kaiyil paarkkiren….ae…….ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaarai thaedi nenjamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarai thaedi nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil jaadai pesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil jaadai pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai nerunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum ennai nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi saaral veesudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kodi saaral veesudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh kaatril eera padhangal koodum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh kaatril eera padhangal koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam thandhaai neeyum mella
<input type="checkbox" class="lyrico-select-lyric-line" value="Maayam thandhaai neeyum mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatril odum ilaiyai pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Aatril odum ilaiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum unnil neendhi chellaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum unnil neendhi chellaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kurunthogai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurunthogai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbugal alli
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurumbugal alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil nee neettinaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyil nee neettinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai indha unarvugal illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhuvarai indha unarvugal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan uyir neevinaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Needhaan uyir neevinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nizhalai pola neeyum vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nizhalai pola neeyum vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai aakkinaai….aaa….aa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhalai aakkinaai….aaa….aa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaarai thaedi nenjamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarai thaedi nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil jaadai pesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil jaadai pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai nerunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum ennai nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi saaral veesudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kodi saaral veesudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Megakoottam yaavumae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Megakoottam yaavumae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham keezhae oduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadham keezhae oduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalin meedhu kadanthu pogum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalin meedhu kadanthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiyaai nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaiyaai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kadanthaai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayam kadanthaai…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh oh hoi…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh hoi…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaarai thaedi nenjamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaarai thaedi nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil jaadai pesudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannil jaadai pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai nerunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum ennai nerunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kodi saaral veesudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru kodi saaral veesudhu"/>
</div>
</pre>