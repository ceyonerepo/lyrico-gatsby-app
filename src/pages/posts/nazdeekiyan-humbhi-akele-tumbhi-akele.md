---
title: "nazdeekiyan song lyrics"
album: "Hum Bhi Akele Tum Bhi Akele"
artist: "Oni-Adil"
lyricist: "Saurabh Negi"
director: "Harish Vyas"
path: "/albums/humbhi-akele-tumbhi-akele-lyrics"
song: "Nazdeekiyan"
image: ../../images/albumart/humbhi-akele-tumbhi-akele.jpg
date: 2020-02-14
lang: hindi
youtubeLink: "https://www.youtube.com/embed/eLETcqYYnhk"
type: "Love"
singers:
  - Adil Rasheed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tu hai nazar mein haseen sa hai jaise koyi manzar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hai nazar mein haseen sa hai jaise koyi manzar"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bin rehna nahi ek pal bhi mujhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin rehna nahi ek pal bhi mujhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri hi khushboo se mehakta mera dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri hi khushboo se mehakta mera dil"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai naya ishq har din tujhse mujhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai naya ishq har din tujhse mujhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai ye guzaarish ho teri hi baarish
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai ye guzaarish ho teri hi baarish"/>
</div>
<div class="lyrico-lyrics-wrapper">Kehna mera maan le haathon ko tu thaam le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehna mera maan le haathon ko tu thaam le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyun mere dil ki har ek dastan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun mere dil ki har ek dastan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein ab shamil hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein ab shamil hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu meri saanson ko de ab panah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri saanson ko de ab panah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan iss kaabil hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan iss kaabil hai tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu shab hai tu hi hai ab subah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu shab hai tu hi hai ab subah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse mukammal jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse mukammal jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhool baitha main saara jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhool baitha main saara jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Huyi jab se nazdeekiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huyi jab se nazdeekiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bekabu hokar yeh dil dekhe tera raasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekabu hokar yeh dil dekhe tera raasta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere saath ka asar yoon hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere saath ka asar yoon hua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil ki intezariyan huyi kamil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ki intezariyan huyi kamil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadkan se aage yeh dil tujhko maange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadkan se aage yeh dil tujhko maange"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanson mein tu iss tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanson mein tu iss tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai jaise tu har jagah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai jaise tu har jagah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyun mere dil ki har ek dastan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun mere dil ki har ek dastan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein ab shamil hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein ab shamil hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu meri saanson ko de ab panah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri saanson ko de ab panah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan iss kaabil hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan iss kaabil hai tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu shab hai tu hi hai ab subah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu shab hai tu hi hai ab subah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse mukammal jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse mukammal jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhool baitha main saara jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhool baitha main saara jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Huyi jab se nazdeekiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huyi jab se nazdeekiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soona hai tere bina aasman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soona hai tere bina aasman"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi hai mera sitara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi hai mera sitara"/>
</div>
<div class="lyrico-lyrics-wrapper">Roshni hai teri har jagah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roshni hai teri har jagah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwabon mein aana nahi kuch jatana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwabon mein aana nahi kuch jatana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere mere ishq ka chale ab yahi silsila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere mere ishq ka chale ab yahi silsila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyun mere dil ki har ek dastan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun mere dil ki har ek dastan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein ab shamil hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein ab shamil hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu meri saanson ko de ab panah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri saanson ko de ab panah"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan iss kaabil hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan iss kaabil hai tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu shab hai tu hi hai ab subah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu shab hai tu hi hai ab subah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhse mukammal jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhse mukammal jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhool baitha main saara jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhool baitha main saara jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Huyi jab se nazdeekiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huyi jab se nazdeekiyan"/>
</div>
</pre>
