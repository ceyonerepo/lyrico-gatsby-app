---
title: "yeanadi yeanadi song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Yeanadi Yeanadi"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tPBQJ1fSKPs"
type: "sad"
singers:
  - Keshav Vinod
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yenadi Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadi Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrile Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile Aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagitham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooramaai Pogavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaai Pogavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nernthathu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nernthathu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalaai Therigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaai Therigira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhali Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhali Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiye Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiye Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambiye Arugiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiye Arugiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendrathu Veenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendrathu Veenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenadi Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadi Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrile Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile Aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagitham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooramaai Pogavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaai Pogavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nernthathu Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nernthathu Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalaai Therigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaai Therigira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhali Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhali Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyae Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyae Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambiye Arugiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiye Arugiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendrathu Veenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendrathu Veenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhigal Pothavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhigal Pothavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuthida Kangal Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthida Kangal Kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Imai Indru Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Imai Indru Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukku Ninaivu Indru Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku Ninaivu Indru Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalukku Uyirindru Thooram Aanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukku Uyirindru Thooram Aanathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilai Melae Inai Serntha Pookkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilai Melae Inai Serntha Pookkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyalaley Ondringu Manmel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalaley Ondringu Manmel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vezhnthaalum Athu Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vezhnthaalum Athu Meendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sera Koodumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sera Koodumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Tholil Naan Saaintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Tholil Naan Saaintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namm Kaigal Kai Koratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Kaigal Kai Koratha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Enagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Enagey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkaaga Naam Seitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkaaga Naam Seitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam enagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam enagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Kann Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Kann Munne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraithathengey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraithathengey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethuvaraiyil Vaanam Undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvaraiyil Vaanam Undo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athuvarai Megam Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvarai Megam Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adipathuvum Anaipathuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adipathuvum Anaipathuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Endru Unarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Endru Unarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Udaiyaathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Udaiyaathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Swaasam Kaatrodu Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Swaasam Kaatrodu Vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithene Verenna Koora Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithene Verenna Koora Ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Indru Maari Ponathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Indru Maari Ponathoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaterndraal Kadanthodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaterndraal Kadanthodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru Puriyaamal Irunthaene Anadru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Puriyaamal Irunthaene Anadru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochum Tholaivaaga Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochum Tholaivaaga Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Naan Vaazhntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Naan Vaazhntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodigallellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigallellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi Athu Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Athu Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udainthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondraga Naam Sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraga Naam Sertha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivu Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvondrai En Munnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondrai En Munnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therinthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigalai Manam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigalai Manam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaraathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaithellaam Pidithu Vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithellaam Pidithu Vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalum Azhuthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalum Azhuthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagaathamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagaathamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaadhal En Kobam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal En Kobam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaadhal Un Monunam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhal Un Monunam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyaamal Irukkindrom Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaamal Irukkindrom Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Varthai Naan Sonnaal Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Varthai Naan Sonnaal Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maru Varthai Nee Sonnaal Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Varthai Nee Sonnaal Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Thalaikeezhai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Thalaikeezhai Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenadi Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenadi Yenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrile Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile Aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagitham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitham Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooramaai Pogavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaai Pogavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nernthathu Yaenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nernthathu Yaenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalaai Therigira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalaai Therigira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhali Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhali Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiye Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiye Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambiye Arugilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiye Arugilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendrathu Veenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendrathu Veenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhigal Podhavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhigal Podhavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuthida Kangal Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthida Kangal Kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukku Imai Indru Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku Imai Indru Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukku Ninaivu Indru Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukku Ninaivu Indru Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalukku Uyirindru Thooram Aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukku Uyirindru Thooram Aanathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilai Melye Inai Serntha Pookkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilai Melye Inai Serntha Pookkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyalale Ondringu Manmel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalale Ondringu Manmel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vezhnthaalum Athu Meendum Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vezhnthaalum Athu Meendum Sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodumo"/>
</div>
</pre>
