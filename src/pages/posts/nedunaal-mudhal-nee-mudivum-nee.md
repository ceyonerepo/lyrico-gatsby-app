---
title: "nedunaal song lyrics"
album: "Mudhal Nee Mudivum Nee"
artist: "Darbuka Siva"
lyricist: "Keerthi"
director: "Darbuka Siva"
path: "/albums/mudhal-nee-mudivum-nee-lyrics"
song: "Nedunaal"
image: ../../images/albumart/mudhal-nee-mudivum-nee.jpg
date: 2022-01-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bPjchJsLeDM"
type: "happy"
singers:
  - Sreekanth Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nedunaal mun nindra paattu ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunaal mun nindra paattu ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaaga thodargindradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaaga thodargindradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala aandaai ennaiyillaa dheebangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala aandaai ennaiyillaa dheebangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga sudar kondadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga sudar kondadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvellai niram oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvellai niram oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannalae vannam koodudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalae vannam koodudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayil ondril vitta novelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayil ondril vitta novelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi thedi vandhu vaasikka solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi thedi vandhu vaasikka solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai maarum osaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai maarum osaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaaga ikkaatrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaaga ikkaatrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhimaarum vaarthaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhimaarum vaarthaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhai pol karaiyum podhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai pol karaiyum podhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam thaandi meendum kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thaandi meendum kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotram maari nenjam maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotram maari nenjam maaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaya meesaigal marupadi pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaya meesaigal marupadi pookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaya vaasanai en melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaya vaasanai en melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai maarum osaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai maarum osaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaaga ikkaatrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaaga ikkaatrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhimaarum vaarthaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhimaarum vaarthaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhai pol karaiyum podhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai pol karaiyum podhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedunaal mun nindra paattu ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunaal mun nindra paattu ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaaga thodargindradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaaga thodargindradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai maarum osaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai maarum osaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyaaga ikkaatrilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyaaga ikkaatrilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhimaarum vaarthaigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhimaarum vaarthaigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhai pol karaiyum podhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai pol karaiyum podhilae"/>
</div>
</pre>
