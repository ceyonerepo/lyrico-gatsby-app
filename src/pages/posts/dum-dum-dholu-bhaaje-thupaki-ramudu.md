---
title: "dum dum dholu bhaaje song lyrics"
album: "Thupaki Ramudu"
artist: "T Prabhakar "
lyricist: "Abhinaya Srinivas"
director: "T Prabhakar"
path: "/albums/thupaki-ramudu-lyrics"
song: "Dum Dum Dholu Bhaaje"
image: ../../images/albumart/thupaki-ramudu.jpg
date: 2019-10-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9Gqgl541PZA"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">maa oorule dhoom dhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa oorule dhoom dhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaraale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaraale "/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">mogindhiro aadudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogindhiro aadudham"/>
</div>
<div class="lyrico-lyrics-wrapper">teenumaare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teenumaare "/>
</div>
<div class="lyrico-lyrics-wrapper">kammukunna cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammukunna cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">kanche dhaatindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanche dhaatindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">andamaina thoorupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andamaina thoorupu"/>
</div>
<div class="lyrico-lyrics-wrapper">tellavarindhile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tellavarindhile "/>
</div>
<div class="lyrico-lyrics-wrapper">cheruvu thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheruvu thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">pandagocchene nede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandagocchene nede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">maa oorule dhoom dhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa oorule dhoom dhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaraale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaraale "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ramayya nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ramayya nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">vasthene sandhadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthene sandhadi "/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvunte navvukuntamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvunte navvukuntamu"/>
</div>
<div class="lyrico-lyrics-wrapper">padi padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi padi"/>
</div>
<div class="lyrico-lyrics-wrapper">kapatamerungani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kapatamerungani "/>
</div>
<div class="lyrico-lyrics-wrapper">pasithaname needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasithaname needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">andharitho kalagalipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharitho kalagalipe"/>
</div>
<div class="lyrico-lyrics-wrapper">varasalu prematho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varasalu prematho"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvu piliche pilupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvu piliche pilupulu"/>
</div>
<div class="lyrico-lyrics-wrapper">ramasakkani manasante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ramasakkani manasante"/>
</div>
<div class="lyrico-lyrics-wrapper">neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">oori kosam okkadu neevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oori kosam okkadu neevai"/>
</div>
<div class="lyrico-lyrics-wrapper">sahasanne chesavayya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sahasanne chesavayya "/>
</div>
<div class="lyrico-lyrics-wrapper">peruke memandharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruke memandharam"/>
</div>
<div class="lyrico-lyrics-wrapper">unna mundhu nilichina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna mundhu nilichina"/>
</div>
<div class="lyrico-lyrics-wrapper">dheerdavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheerdavayya"/>
</div>
<div class="lyrico-lyrics-wrapper">mana oore ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana oore ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">maruvudhule oo ramayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruvudhule oo ramayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">maa oorule dhoom dhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa oorule dhoom dhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaraale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaraale "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">innalu nuvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innalu nuvu "/>
</div>
<div class="lyrico-lyrics-wrapper">maalona okadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalona okadani"/>
</div>
<div class="lyrico-lyrics-wrapper">etlunnaavani yenaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etlunnaavani yenaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">adagani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adagani "/>
</div>
<div class="lyrico-lyrics-wrapper">parula kosamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parula kosamu"/>
</div>
<div class="lyrico-lyrics-wrapper">pariugulu theesaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pariugulu theesaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">uppongi gattu dhaateti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppongi gattu dhaateti"/>
</div>
<div class="lyrico-lyrics-wrapper">gangaku vegamga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gangaku vegamga "/>
</div>
<div class="lyrico-lyrics-wrapper">atu dhuketi varadhaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atu dhuketi varadhaku"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuru nilabadi praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuru nilabadi praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">parichaavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parichaavu "/>
</div>
<div class="lyrico-lyrics-wrapper">ayina vaare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayina vaare "/>
</div>
<div class="lyrico-lyrics-wrapper">andharu meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharu meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">aatma bandhuvule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatma bandhuvule "/>
</div>
<div class="lyrico-lyrics-wrapper">ayinaaru kadupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayinaaru kadupu"/>
</div>
<div class="lyrico-lyrics-wrapper">nimpina ammalu meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimpina ammalu meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna kodukuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kodukuga "/>
</div>
<div class="lyrico-lyrics-wrapper">nannu penchaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu penchaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">mee runame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mee runame "/>
</div>
<div class="lyrico-lyrics-wrapper">ika theerchanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika theerchanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">adhi theeranidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhi theeranidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">maa oorule dhoom dhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa oorule dhoom dhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaraale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaraale "/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">mogindhiro aadudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogindhiro aadudham"/>
</div>
<div class="lyrico-lyrics-wrapper">teenumaare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teenumaare "/>
</div>
<div class="lyrico-lyrics-wrapper">kammukunna cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammukunna cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">kanche dhaatindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanche dhaatindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">andamaina thoorupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andamaina thoorupu"/>
</div>
<div class="lyrico-lyrics-wrapper">tellavarindhile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tellavarindhile "/>
</div>
<div class="lyrico-lyrics-wrapper">cheruvu thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheruvu thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">pandagocchene nede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandagocchene nede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dum dum dholu bhaaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dholu bhaaje"/>
</div>
<div class="lyrico-lyrics-wrapper">maa oorule dhoom dhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa oorule dhoom dhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaraale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaraale "/>
</div>
</pre>
