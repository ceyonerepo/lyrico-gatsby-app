---
title: "railaa railaa odum song lyrics"
album: "Nadodigal 2"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "Samuthrakani"
path: "/albums/nadodigal-2-lyrics"
song: "Railaa Railaa Odum"
image: ../../images/albumart/nadodigal-2.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/T3JZdhPKFEQ"
type: "Love"
singers:
  - Achu Rajamani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Railaa Railaa Odum Railaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railaa Railaa Odum Railaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaiyum Kadathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaiyum Kadathuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyira Kaiyira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyira Kaiyira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nylon Kaiyira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nylon Kaiyira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasaiyum Ezhukiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasaiyum Ezhukiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaale Kavadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaale Kavadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Nee Thookuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Nee Thookuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala En Naadiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala En Naadiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumburudhadi Eni Naan Thaan Onn Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumburudhadi Eni Naan Thaan Onn Jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agaaya Oonjal Pola Un Paarva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agaaya Oonjal Pola Un Paarva"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaaga Aazha Thookki Mela Veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaaga Aazha Thookki Mela Veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Yethhum Pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Yethhum Pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuroram Pei Mazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuroram Pei Mazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda Saanju Naanum Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda Saanju Naanum Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam Poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam Poduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha Saamigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha Saamigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthen Un Saayalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthen Un Saayalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai Viral Modhayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Viral Modhayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyil Koovudhu Moolaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyil Koovudhu Moolaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paanchaali Sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanchaali Sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pole En Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pole En Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Una Sera Yengi Yengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Sera Yengi Yengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelamaaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelamaaguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozhungaaga Kottula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhungaaga Kottula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai Potta Kaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Potta Kaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Una Parkkum Aasaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Una Parkkum Aasaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam Poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Poduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhoram Seithigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoram Seithigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesum Velaiyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesum Velaiyilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Thoongavum Koodalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thoongavum Koodalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Kootura Kaadhalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Kootura Kaadhalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Railaa Railaa Odum Railaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railaa Railaa Odum Railaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaiyum Kadathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaiyum Kadathuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyira Kaiyira Nylon Kaiyira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyira Kaiyira Nylon Kaiyira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasaiyum Ezhukiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasaiyum Ezhukiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannale Kavadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannale Kavadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyum Nee Thookuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyum Nee Thookuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaale En Naadiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale En Naadiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumburudhadi Eni Naan Thaan On Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumburudhadi Eni Naan Thaan On Jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dhinnaku Dhinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dhinnaku Dhinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakku Dhinnakku Dhinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakku Dhinnakku Dhinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinnaku Dhinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinnaku Dhinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakku Dhinnakku Dhinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakku Dhinnakku Dhinnaa"/>
</div>
</pre>
