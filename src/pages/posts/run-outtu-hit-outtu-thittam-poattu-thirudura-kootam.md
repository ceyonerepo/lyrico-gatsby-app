---
title: "run outtu hit outtu song lyrics"
album: "Thittam Poattu Thirudura Kootam"
artist: "Ashwath"
lyricist: "Muralidaran K N"
director: "Sudhar"
path: "/albums/thittam-poattu-thirudura-kootam-lyrics"
song: "Run Outtu Hit Outtu"
image: ../../images/albumart/thittam-poattu-thirudura-kootam.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o0Q3H_5mrTY"
type: "happy"
singers:
  - Venkat Prabhu
  - Premji Amaren
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">cup onnu maatikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cup onnu maatikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan poren walk out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan poren walk out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">cup onnu maatikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cup onnu maatikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan poren walk out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan poren walk out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa machan vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa machan vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">un potti ellam katikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un potti ellam katikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaulatha kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaulatha kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">en pinalaye thorthikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pinalaye thorthikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadura aatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadura aatta"/>
</div>
<div class="lyrico-lyrics-wrapper">nama kooraya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nama kooraya than"/>
</div>
<div class="lyrico-lyrics-wrapper">keesikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keesikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">kita ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kita ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">tea-yum bun num
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tea-yum bun num"/>
</div>
<div class="lyrico-lyrics-wrapper">naasthaya than thunukinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naasthaya than thunukinu"/>
</div>
<div class="lyrico-lyrics-wrapper">oataya pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oataya pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">anga vechadellam sutukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga vechadellam sutukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">aapicer vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aapicer vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sulute ah than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sulute ah than"/>
</div>
<div class="lyrico-lyrics-wrapper">vechinunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechinunu"/>
</div>
<div class="lyrico-lyrics-wrapper">maamanum machanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maamanum machanum"/>
</div>
<div class="lyrico-lyrics-wrapper">pangu renda eduthukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pangu renda eduthukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">micha meedhi thangunadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="micha meedhi thangunadha"/>
</div>
<div class="lyrico-lyrics-wrapper">sillangoali uttukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillangoali uttukunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu walk outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu walk outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">assaulta vandhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="assaulta vandhaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">salpeta knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salpeta knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennalum kondaatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennalum kondaatam"/>
</div>
<div class="lyrico-lyrics-wrapper">thannala undaagum daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannala undaagum daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cup onnu thoka poram daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cup onnu thoka poram daa"/>
</div>
<div class="lyrico-lyrics-wrapper">machi sikkama than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi sikkama than"/>
</div>
<div class="lyrico-lyrics-wrapper">essu aavom daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="essu aavom daa"/>
</div>
<div class="lyrico-lyrics-wrapper">cup onnu thoka poram daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cup onnu thoka poram daa"/>
</div>
<div class="lyrico-lyrics-wrapper">andha black ah white aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha black ah white aa"/>
</div>
<div class="lyrico-lyrics-wrapper">maatha porom daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatha porom daa"/>
</div>
<div class="lyrico-lyrics-wrapper">daa daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daa daa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">metera pottu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="metera pottu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">getha full ah yethukittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="getha full ah yethukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">peter aa vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peter aa vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">avan moonji 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan moonji "/>
</div>
<div class="lyrico-lyrics-wrapper">mogara odachukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogara odachukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">bikula yeri antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bikula yeri antha"/>
</div>
<div class="lyrico-lyrics-wrapper">front wheel ah thookikitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="front wheel ah thookikitu"/>
</div>
<div class="lyrico-lyrics-wrapper">stump ah nattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stump ah nattu"/>
</div>
<div class="lyrico-lyrics-wrapper">toss ah pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="toss ah pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">gumman kutha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gumman kutha "/>
</div>
<div class="lyrico-lyrics-wrapper">yeraki kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeraki kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">fight ah than potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fight ah than potu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pitch ah namma puduchikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pitch ah namma puduchikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">sizer u ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sizer u ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru baby over kudthukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru baby over kudthukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">veetula adchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetula adchu"/>
</div>
<div class="lyrico-lyrics-wrapper">anga kanadiya odachikunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga kanadiya odachikunu"/>
</div>
<div class="lyrico-lyrics-wrapper">aayaa vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayaa vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">paayaava than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paayaava than"/>
</div>
<div class="lyrico-lyrics-wrapper">moonji mela oothikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonji mela oothikinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">cup onnu maatikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cup onnu maatikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan poren walk out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan poren walk out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">run outtu hit outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run outtu hit outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">catch outtu knock outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch outtu knock outtu"/>
</div>
<div class="lyrico-lyrics-wrapper">cup onnu maatikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cup onnu maatikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan poren walk out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan poren walk out"/>
</div>
</pre>
