---
title: "konjali song lyrics"
album: "Diya"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/diya-song-lyrics"
song: "Konjali"
image: ../../images/albumart/diya.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/B0qSkw3NV5A"
type: "happy"
singers:
  - Sathyaprakash
  - Neha Venugopal
  - JKA Shalini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Azhagooththi Senju Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagooththi Senju Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Alankattiyattamkannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alankattiyattamkannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaana Pathaaponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaana Pathaaponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Pulla En Kai Kothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Pulla En Kai Kothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ini En Moochu Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ini En Moochu Kaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Omakku Kaathu Kedandhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Omakku Kaathu Kedandhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakkum Kaatha Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum Kaatha Ippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjali Konjali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjali Konjali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravellam Nee Enjolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravellam Nee Enjolli"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjali Konjali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjali Konjali"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjathil Naan Unjolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjathil Naan Unjolli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Theendhum Kanavu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Theendhum Kanavu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagooththi Senju Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagooththi Senju Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Alankattiyattamkannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alankattiyattamkannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaana Theethathinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaana Theethathinnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoorigai Adhan Thooralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorigai Adhan Thooralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undha Kaadhalo Vizhugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undha Kaadhalo Vizhugiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarinen Niram Maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarinen Niram Maarinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Naanamo Neelgiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Naanamo Neelgiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ennakaatil Vannamkootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ennakaatil Vannamkootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malare"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vinnil Yeri Kannil Paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vinnil Yeri Kannil Paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meedhi Vazhkaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meedhi Vazhkaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaipu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaipu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjali Konjali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjali Konjali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravellam Nee Enjolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravellam Nee Enjolli"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjali Konjali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjali Konjali"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjathil Naan Unjolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjathil Naan Unjolli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjali Konjali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjali Konjali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravellam Nee Enjolli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravellam Nee Enjolli"/>
</div>
</pre>
