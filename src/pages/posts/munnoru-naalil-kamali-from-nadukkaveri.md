---
title: "munnoru naalil song lyrics"
album: "Kamali from Nadukkaveri"
artist: "Dheena Dhayalan"
lyricist: "Madhan Karky"
director: "Rajasekar Duraisamy"
path: "/albums/kamali-from-nadukkaveri-lyrics"
song: "Munnoru Naalil"
image: ../../images/albumart/kamali-from-nadukkaveri.jpg
date: 2021-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vfcTOMdRMJo"
type: "Love"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Munnoru naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siridhoru koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siridhoru koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnoru naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siridhoru koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siridhoru koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mega megamaai kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mega megamaai kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai illaadha virivu…uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai illaadha virivu…uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal endhan thozhigal polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal endhan thozhigal polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeri paaiven vaanin melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri paaiven vaanin melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha naan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha naan..oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha naan..oo hoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduthal siragaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduthal siragaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooduthal siragae ganamanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduthal siragae ganamanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooduthal siragaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduthal siragaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooduthal siragae ganamanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduthal siragae ganamanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arimugam illaa thozhvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arimugam illaa thozhvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaikulukki ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaikulukki ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhakkaiyum marandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakkaiyum marandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakkaiyum marandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakkaiyum marandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha naan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha naan..oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha naan..oo hoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi vaa endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa endraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhom kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhom kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum sendraai..haa…aa…aa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum sendraai..haa…aa…aa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi vaa endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa endraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhom kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhom kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum endhan kaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum endhan kaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan siriya koottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan siriya koottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naetrin valigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetrin valigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhu vaazhgindren..a.en…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhu vaazhgindren..a.en…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram kunjugal angae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram kunjugal angae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanavaai kondana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanavaai kondana"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnal paravai ennai paarthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnal paravai ennai paarthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakka kattrana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka kattrana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha naan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Engae andha naan..oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae andha naan..oo hoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnoru naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum virithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum virithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan siragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan siragai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnoru naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnoru naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum virithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum virithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan siragai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan siragai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum kannil kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum kannil kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum nenjil thunivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum nenjil thunivu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai thadukka yaarum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thadukka yaarum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal iniyum baaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal iniyum baaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer ennul michcham illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer ennul michcham illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvigal thazhuva achcham illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvigal thazhuva achcham illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha naan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha naan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingae andha vaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha vaan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha naan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae andha naan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha naan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae andha naan..oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae andha naan..oo hoo oo"/>
</div>
</pre>
