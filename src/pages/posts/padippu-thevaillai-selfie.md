---
title: "padippu thevaillai song lyrics"
album: "Selfie"
artist: "GV Prakash Kumar"
lyricist: "Arivu - Arjun Karthik"
director: "Mathi Maran"
path: "/albums/selfie-lyrics"
song: "Padippu Thevaillai"
image: ../../images/albumart/selfie.jpg
date: 2022-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eX9GAkvOsvA"
type: "mass"
singers:
  - Arunraja Kamaraj
  - Arivu
  - Robert Sargunam
  - GV Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Padippu Theva Illa Paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padippu Theva Illa Paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketka Yaarum Illa En Kaalamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketka Yaarum Illa En Kaalamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Padicha Pudunginga Vaaraan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padicha Pudunginga Vaaraan Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Yaaru Keezha En Kaalu Keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Yaaru Keezha En Kaalu Keezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padippu Theva Illa Paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padippu Theva Illa Paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketka Yaarum Illa En Kaalamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketka Yaarum Illa En Kaalamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Padicha Pudunginga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padicha Pudunginga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Yaaru Keezha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Yaaru Keezha "/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalu Keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalu Keezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attakathiya Irunthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attakathiya Irunthome"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakathi Aanome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakathi Aanome"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikaama Irunthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikaama Irunthome"/>
</div>
<div class="lyrico-lyrics-wrapper">Panatha Paathome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panatha Paathome"/>
</div>
<div class="lyrico-lyrics-wrapper">Panatha Thaan Paathathume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panatha Thaan Paathathume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkal Aanome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkal Aanome"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennanu Puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennanu Puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalasuthi Ninnome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalasuthi Ninnome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Ah Maranthache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Ah Maranthache"/>
</div>
<div class="lyrico-lyrics-wrapper">Moola Thelivaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moola Thelivaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Route Tu Potache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Route Tu Potache"/>
</div>
<div class="lyrico-lyrics-wrapper">Thug Ah Aayache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thug Ah Aayache"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattam Thaan Thatniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattam Thaan Thatniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattapalaga Aaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattapalaga Aaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Padichathaan Kaasunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichathaan Kaasunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengalam Loosuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalam Loosuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cool I’m A Kombu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cool I’m A Kombu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushar Mela Thembu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushar Mela Thembu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Mela Kaal Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Mela Kaal Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkaarven Da Kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaarven Da Kingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Navuruma Sokku Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navuruma Sokku Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookku Mela Finger Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookku Mela Finger Vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoguruma Kokku Maakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoguruma Kokku Maakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Currency Note Tu Karandhaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Currency Note Tu Karandhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arakka Parakka Padichi Mudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakka Parakka Padichi Mudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edutha Marku Edhukku Namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edutha Marku Edhukku Namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi Pudichu Deal Ah Mudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi Pudichu Deal Ah Mudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedacha Kaasil Nadathu Virunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedacha Kaasil Nadathu Virunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">College Namba Control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Namba Control"/>
</div>
<div class="lyrico-lyrics-wrapper">Village Out Of Control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Village Out Of Control"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Vaangi Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Vaangi Enna Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Vaangi Route Ah Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Vaangi Route Ah Maathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attakathiya Irunthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attakathiya Irunthome"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattakathi Aanome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattakathi Aanome"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikaama Irunthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikaama Irunthome"/>
</div>
<div class="lyrico-lyrics-wrapper">Panatha Paathome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panatha Paathome"/>
</div>
<div class="lyrico-lyrics-wrapper">Panatha Thaan Paathathume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panatha Thaan Paathathume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkal Aanome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkal Aanome"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennanu Puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennanu Puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalasuthi Ninnome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalasuthi Ninnome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College Ah Maranthache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Ah Maranthache"/>
</div>
<div class="lyrico-lyrics-wrapper">Moola Thelivaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moola Thelivaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Route Tu Potache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Route Tu Potache"/>
</div>
<div class="lyrico-lyrics-wrapper">Thug Ah Aayache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thug Ah Aayache"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattam Thaan Thatniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattam Thaan Thatniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattapalaga Aaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattapalaga Aaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Padichathaan Kaasunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichathaan Kaasunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengalam Loosuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalam Loosuthaan"/>
</div>
</pre>
