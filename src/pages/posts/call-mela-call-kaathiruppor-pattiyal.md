---
title: "call mela call song lyrics"
album: "Kaathiruppor Pattiyal"
artist: "Sean Roldan"
lyricist: "Sean Roldan"
director: "Balaiya D. Rajasekhar"
path: "/albums/kaathiruppor-pattiyal-song-lyrics"
song: "Call Mela Call"
image: ../../images/albumart/kaathiruppor-pattiyal.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XEI3gnb8BgY"
type: "love"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">call mela calla pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="call mela calla pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">reel mela reela vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reel mela reela vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathale torture panrele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathale torture panrele"/>
</div>
<div class="lyrico-lyrics-wrapper">reel mele reela vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reel mele reela vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">fool aaka try panrele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fool aaka try panrele"/>
</div>
<div class="lyrico-lyrics-wrapper">ayyayo kaadhu thenjuduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyayo kaadhu thenjuduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pesi than vayum onjuduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi than vayum onjuduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhil pajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil pajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil pajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil pajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil pajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil pajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil pajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil pajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">seivom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seivom "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">call mela calla pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="call mela calla pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">reel mela reela vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reel mela reela vuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ball mela balla pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ball mela balla pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">bold aaka try pandrele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bold aaka try pandrele"/>
</div>
<div class="lyrico-lyrics-wrapper">thol mel kaiya potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thol mel kaiya potu"/>
</div>
<div class="lyrico-lyrics-wrapper">paket la kaiya vidrele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paket la kaiya vidrele"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam innum aagalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam innum aagalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">en pillaikum policy solluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pillaikum policy solluriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhil bajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhil bajanai seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhil bajanai seivom"/>
</div>
</pre>
