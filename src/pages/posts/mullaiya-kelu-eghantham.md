---
title: "mullaiya kelu song lyrics"
album: "Eghantham"
artist: "Ganesh Raghavendra"
lyricist: "Yuga Bharathi"
director: "Arsal Arumugam"
path: "/albums/eghantham-lyrics"
song: "Mullaiya Kelu"
image: ../../images/albumart/eghantham.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h7m9ni_cq8s"
type: "happy"
singers:
  - Surmukhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mullaiya kelu malliya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaiya kelu malliya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu virinja alliya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu virinja alliya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moramaaman mothathula rajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moramaaman mothathula rajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balliya kelu patchiya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balliya kelu patchiya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha vayalu naththaiya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha vayalu naththaiya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarae thaan ennudaiya ghajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarae thaan ennudaiya ghajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamal aatum mutham kodupparu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamal aatum mutham kodupparu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijay aatum suthi adippaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijay aatum suthi adippaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettudhisaiyum sernthu kannu vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettudhisaiyum sernthu kannu vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Superaana siru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superaana siru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna azhaga pola alli kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna azhaga pola alli kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarukku eedu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarukku eedu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettudhisaiyum sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettudhisaiyum sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Superaana siru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superaana siru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna azhaga pola alli kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna azhaga pola alli kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarukku eedu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarukku eedu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellaiyum sollaiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaiyum sollaiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar munnae vandhu nikka kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar munnae vandhu nikka kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullaiyum malligaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaiyum malligaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivapaagum ammammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivapaagum ammammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangala kungumamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangala kungumamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar alli thandha anbai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar alli thandha anbai solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Velliyum thangamumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velliyum thangamumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliveesum sollammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliveesum sollammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sothu sugam serthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sothu sugam serthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaratha maamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaratha maamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna solli paaraatuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solli paaraatuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethava eedaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethava eedaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai anba naa kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai anba naa kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil vechu thaalaatuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil vechu thaalaatuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayum vaththum varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayum vaththum varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boologam suthum varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologam suthum varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarae ulagil sirantha aambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarae ulagil sirantha aambala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullaiya kelu malliya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaiya kelu malliya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu virinja alliya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu virinja alliya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moramaaman mothathula rajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moramaaman mothathula rajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullaiyum kuttiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaiyum kuttiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar ullam mattum kattikolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar ullam mattum kattikolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam ullathelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam ullathelaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvenae ovvonnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvenae ovvonnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthura bombaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura bombaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaru sonnathu elaam seiya enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaru sonnathu elaam seiya enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppovum ennaiya pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppovum ennaiya pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvenae ellunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvenae ellunaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thollai yedhum thaaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai yedhum thaaraama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal dhorum maamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal dhorum maamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil vechu naan kaakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil vechu naan kaakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullavara neengaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullavara neengaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayaara maamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayaara maamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellam konji soru ootanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam konji soru ootanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooraaru solli pugazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraaru solli pugazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veryedhum illa kavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veryedhum illa kavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothuva avara mudinjen moochula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuva avara mudinjen moochula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullaiya kelu malliya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullaiya kelu malliya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu virinja alliya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu virinja alliya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moramaaman mothathula rajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moramaaman mothathula rajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balliya kelu patchiya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balliya kelu patchiya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha vayalu naththaiya kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha vayalu naththaiya kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarae thaan ennudaiya ghajini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarae thaan ennudaiya ghajini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamal aatum mutham kodupparu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamal aatum mutham kodupparu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijay aatum suthi adippaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijay aatum suthi adippaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettudhisaiyum sernthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettudhisaiyum sernthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kannu vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Superaana siru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superaana siru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna azhaga pola alli kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna azhaga pola alli kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarukku eedu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarukku eedu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettudhisaiyum sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettudhisaiyum sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Superaana siru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superaana siru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna azhaga pola alli kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna azhaga pola alli kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarukku eedu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarukku eedu yaaru"/>
</div>
</pre>
