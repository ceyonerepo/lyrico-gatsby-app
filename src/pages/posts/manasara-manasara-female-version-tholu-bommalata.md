---
title: "manasara manasara female version song lyrics"
album: "Tholu Bommalata"
artist: "Suresh Bobbili"
lyricist: "Chaitanya Prasad"
director: "Viswanath Maganti"
path: "/albums/tholu-bommalata-lyrics"
song: "Manasara Manasara Female Version"
image: ../../images/albumart/tholu-bommalata.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3FtFMviYZFs"
type: "sad"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasaraa Manasaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaraa Manasaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulu Verayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulu Verayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabato Porapato 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabato Porapato "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedabatayyele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedabatayyele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Raayani Kadha Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Raayani Kadha Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraham Migilele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraham Migilele"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavve Velipothu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavve Velipothu "/>
</div>
<div class="lyrico-lyrics-wrapper">Palike Veedkole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palike Veedkole"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Praname 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Praname "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannodhilesi Velipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannodhilesi Velipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavedane Thodaiyindi Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavedane Thodaiyindi Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamannadhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamannadhe "/>
</div>
<div class="lyrico-lyrics-wrapper">O Mathi Leni Maataiyye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Mathi Leni Maataiyye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Nenu Ga Vidipoyindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Nenu Ga Vidipoyindile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Shunyame Ika Na Nesthamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Shunyame Ika Na Nesthamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Koluvai Untundhi Jantai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Koluvai Untundhi Jantai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharahasame Oka Parihasamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharahasame Oka Parihasamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriga Nene Nadiche Vinthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriga Nene Nadiche Vinthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Guri Chusi Kodithe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guri Chusi Kodithe "/>
</div>
<div class="lyrico-lyrics-wrapper">Thagilindi Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagilindi Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha Loni Gaayam Maneyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha Loni Gaayam Maneyna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Badhatho Ney Bhathakali Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Badhatho Ney Bhathakali Kalakaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhe Sukham Anu Kovalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhe Sukham Anu Kovalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Praname Nannodhilesi Velipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Praname Nannodhilesi Velipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Avedhane Thodaiyindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avedhane Thodaiyindile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Oopire Penu Sudigaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Oopire Penu Sudigaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasiga Dhooram Thosindi Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasiga Dhooram Thosindi Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oohale Nanu Oopeyaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oohale Nanu Oopeyaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhe Patti Thiriga Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhe Patti Thiriga Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Raaku Annaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Raaku Annaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeru Vinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeru Vinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalanni Karigi Kurisele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalanni Karigi Kurisele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sokame Na Lokanga Marindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sokame Na Lokanga Marindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Aashane Masichesindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Aashane Masichesindile"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Praname Nannodhilesi Velipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Praname Nannodhilesi Velipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Avedhane Thodaiyindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avedhane Thodaiyindile"/>
</div>
</pre>
