---
title: "jigina jigina song lyrics"
album: "Thulam"
artist: "Alex Premnath"
lyricist: "Nadhi Vijaykumar"
director: "Rajanagajothi"
path: "/albums/thulam-lyrics"
song: "Jigina Jigina"
image: ../../images/albumart/thulam.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tEUXEzOXXas"
type: "gaana"
singers:
  - Gaana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jigina jigina jigina jigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigina jigina jigina jigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigina saree daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigina saree daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu dagulu dagulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dagulu dagulu dagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu party daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu party daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Struggle-u struggle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Struggle-u struggle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Struggle-u struggle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Struggle-u struggle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Struggle-u vaazhkai daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Struggle-u vaazhkai daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bigulu bigulu bigulu bigulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigulu bigulu bigulu bigulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bigulu oodhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigulu oodhu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fair and lovely face-ula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fair and lovely face-ula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Fashion kaattuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fashion kaattuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava barbie dollu bommai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava barbie dollu bommai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam kaattuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam kaattuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi sweety en smartie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi sweety en smartie"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sight adikkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sight adikkathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Touchu panni ichchu panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touchu panni ichchu panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saavadikkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saavadikkathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salla salla salla salla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salla salla salla salla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku salla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku salla"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulla fulla fulla fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulla fulla fulla fulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooththu fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooththu fulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi indha indha indha indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi indha indha indha indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidedish indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidedish indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaa konda kondaa kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa konda kondaa kondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekiram kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekiram kondaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whats app party ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whats app party ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chatting thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chatting thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook ulla pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook ulla pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheating thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheating thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cycle gappil auto otta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle gappil auto otta"/>
</div>
<div class="lyrico-lyrics-wrapper">Love u thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love u thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">One way-ah kaadhalichchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One way-ah kaadhalichchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Javvu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Javvu thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emailu gmailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emailu gmailu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathiruntha kedaikkum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathiruntha kedaikkum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Dating thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dating thaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram kaadhalichchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram kaadhalichchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fixing thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fixing thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu naalu leavu vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu naalu leavu vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mixing thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mixing thaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei salla salla salla salla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei salla salla salla salla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku salla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku salla"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulla fulla fulla fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulla fulla fulla fulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooththu fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooththu fulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi indha indha indha indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi indha indha indha indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidedish indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidedish indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaa konda kondaa kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa konda kondaa kondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekiram kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekiram kondaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigulu bigulu bigulu bigulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigulu bigulu bigulu bigulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu dagulu dagulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dagulu dagulu dagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bigulu bigulu bigulu bigulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigulu bigulu bigulu bigulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu dagulu dagulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dagulu dagulu dagulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boys ellam ponnungala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys ellam ponnungala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjuvaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjuvaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenjum bothu romba thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjum bothu romba thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minjuvaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minjuvaangadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minjunaalum one day unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minjunaalum one day unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjuvaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjalukku pinaalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjalukku pinaalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjuvaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjuvaangadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sabbaru kaattum pasanga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabbaru kaattum pasanga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Waste-u thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waste-u thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigarthanda party kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigarthanda party kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatti kathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatti kathadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aal illa rocket u naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal illa rocket u naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Loveula venum unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loveula venum unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Softu thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Softu thaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei salla salla salla salla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei salla salla salla salla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku salla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku salla"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulla fulla fulla fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulla fulla fulla fulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooththu fulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooththu fulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi indha indha indha indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi indha indha indha indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidedish indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidedish indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaa konda kondaa kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa konda kondaa kondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekiram kondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekiram kondaa"/>
</div>
</pre>
