---
title: "uyir song lyrics"
album: "Seethakaathi"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "Balaji Tharaneetharan"
path: "/albums/seethakaathi-lyrics"
song: "Uyir"
image: ../../images/albumart/seethakaathi.jpg
date: 2018-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qR78zRQru24"
type: "sad"
singers:
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aiyaavae edhai paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaavae edhai paarkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu neramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu neramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhndhadhellam edhirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhndhadhellam edhirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarugaai veezhgiradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarugaai veezhgiradho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththanai pasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththanai pasamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalpatta yekkamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalpatta yekkamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr nodiyil thuliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr nodiyil thuliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaai kaaigiradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaai kaaigiradho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraiyin mel peru velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraiyin mel peru velicham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangi sendrae pogiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi sendrae pogiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiyagamellaam thalai kunindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyagamellaam thalai kunindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounangalaai azhugiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounangalaai azhugiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir saavil mudivathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir saavil mudivathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmm mmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmm mmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmm mmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmm mmmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalae meiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalae meiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagivittaal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagivittaal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeramaai thodum nadhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeramaai thodum nadhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Silirpathundo ullaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silirpathundo ullaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhathin aazhangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhathin aazhangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paartha maunathinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha maunathinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai jeyipathundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai jeyipathundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathin dhooram varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathin dhooram varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vergal vitta maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vergal vitta maram"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavil uthirvathundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavil uthirvathundo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannanthaniyae aluthu vittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanthaniyae aluthu vittae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponadhengae koomaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponadhengae koomaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppanai thaan kalaintha pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppanai thaan kalaintha pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugamaai aanaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugamaai aanaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalai saavai madhipathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalai saavai madhipathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegaantha malar sainthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaantha malar sainthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eri meedhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eri meedhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegirathae thaniyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegirathae thaniyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan mudivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan mudivaa"/>
</div>
</pre>
