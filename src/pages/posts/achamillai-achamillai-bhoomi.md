---
title: 'achamillai achamillai lyrics'
album: 'Bhoomi'
artist: 'D Imman'
lyricist: 'Madhan Karky'
director: 'Lakshman'
path: '/albums/bhoomi-song-lyrics'
song: 'Achamillai Achamillai'
image: ../../images/albumart/bhoomi.jpg
date: 2020-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-9vCwdO__5w"
type: 'Motivational'
singers: 
- Jithin Raj
- Shenbagaraj
- Narayanan
- Deepak
- Soundarya Nandakumar
- Abinaya 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbadhu illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbadhu illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbadhu illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbadhu illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchi meedhu vaan idinthu veezhkindra podhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi meedhu vaan idinthu veezhkindra podhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbadhu illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbadhu illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyan aravanaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan aravanaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeriyudai miniminukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeriyudai miniminukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda kai korthu pudhiya ulagam thodanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda kai korthu pudhiya ulagam thodanganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On panna mazhai sirikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On panna mazhai sirikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boom sonna vaazhi porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom sonna vaazhi porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa kanna onnonna neraiya kalaiya pudunganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa kanna onnonna neraiya kalaiya pudunganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midukka namma aalu nadakka nadakka whistle-u parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midukka namma aalu nadakka nadakka whistle-u parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasayam 2.0
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasayam 2.0"/>
</div>
<div class="lyrico-lyrics-wrapper">Edutha kaiyellam kalappa edukka neruppu therikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edutha kaiyellam kalappa edukka neruppu therikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasayam 2.0
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasayam 2.0"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varappula thanthane thanthane thanthanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varappula thanthane thanthane thanthanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiruthu Bluetooth-u thenmangu paatto (paatto)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiruthu Bluetooth-u thenmangu paatto (paatto)"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayalula thanthane thanthane thanthanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayalula thanthane thanthane thanthanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuthu vivasayam 2.0 oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuthu vivasayam 2.0 oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru (tamizhan endu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru (tamizhan endu)"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhan endru (tamizhan endru)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru (tamizhan endru)"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhan endru (sollada)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru (sollada)"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhan endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru (tamizhan endu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru (tamizhan endu)"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhan endru (tamizhan endru)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru (tamizhan endru)"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhan endru (sollada)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru (sollada)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee aazha atha rasichavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aazha atha rasichavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vida ennai mithachavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vida ennai mithachavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanthu paarkka thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanthu paarkka thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum veduchi valaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum veduchi valaranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamiyil vayal vazhiyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamiyil vayal vazhiyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Zoom panna puzhu nizhiyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zoom panna puzhu nizhiyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannilla ponnennu manasum manasum unarunum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannilla ponnennu manasum manasum unarunum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adukku veedulam idichi odachi vayalu viriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adukku veedulam idichi odachi vayalu viriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasayam 2.0
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasayam 2.0"/>
</div>
<div class="lyrico-lyrics-wrapper">Padicha pullainga nilamthil irangi kalakku kalakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padicha pullainga nilamthil irangi kalakku kalakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivayasam 2.0
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivayasam 2.0"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varappula thanthane thanthane thanthanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varappula thanthane thanthane thanthanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiruthu Bluetooth-u thenmangu paatto (paatto)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiruthu Bluetooth-u thenmangu paatto (paatto)"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayalula thanthane thanthane thanthanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayalula thanthane thanthane thanthanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuthu vivasayam 2.0 oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuthu vivasayam 2.0 oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IT’il share-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IT’il share-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Swiggy-il soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swiggy-il soru"/>
</div>
<div class="lyrico-lyrics-wrapper">PUBG’il war-u nethu nethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="PUBG’il war-u nethu nethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamtha paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamtha paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow sollum youth-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow sollum youth-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Gammavin kaathil serthu koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gammavin kaathil serthu koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Work-um workout-um onnachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Work-um workout-um onnachina"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambum manasum pudhusa maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambum manasum pudhusa maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum pocket-um onnachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum pocket-um onnachina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam muzhukka azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam muzhukka azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velinaadukku naan thantha moolaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velinaadukku naan thantha moolaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma thaai mannai kaapatha onnache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma thaai mannai kaapatha onnache"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma comalinu sonna koottamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma comalinu sonna koottamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Collection-eh paartha pinna bayanthuduche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collection-eh paartha pinna bayanthuduche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamizhan endru sollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizhan endru sollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimirndhu nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimirndhu nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharaniyai nee vellada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharaniyai nee vellada"/>
</div>
</pre>