---
title: "mahatma song lyrics"
album: "Operation Gold Fish"
artist: "Sricharan Pakala"
lyricist: "Ramajogayya Sastry"
director: "Sai Kiran Adivi"
path: "/albums/operation-gold-fish-lyrics"
song: "Mahatma"
image: ../../images/albumart/operation-gold-fish.jpg
date: 2019-10-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7PWwtBrOBaU"
type: "sad"
singers:
  - MM Keeravani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aayudhamai Pelene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayudhamai Pelene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabalinche Kaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabalinche Kaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahuthi Gaa Koolene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahuthi Gaa Koolene"/>
</div>
<div class="lyrico-lyrics-wrapper">Viharinche Paavuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viharinche Paavuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaadame Tootaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaadame Tootaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Durmargamee Aata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durmargamee Aata"/>
</div>
<div class="lyrico-lyrics-wrapper">Baliyaina Praanaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baliyaina Praanaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirigicchedevarata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirigicchedevarata"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahatmaa Digiraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahatmaa Digiraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maralaa Maralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maralaa Maralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahimsaa Padhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahimsaa Padhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamu Nadipinchela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamu Nadipinchela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mahatmaa Digiraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahatmaa Digiraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maralaa Maralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maralaa Maralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Abhimatamula Nadumana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhimatamula Nadumana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchelu Tenchelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchelu Tenchelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raghupati Raaghava Raajaraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghupati Raaghava Raajaraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pateetha Paavana Seetharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pateetha Paavana Seetharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eswar Allah Tero Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eswar Allah Tero Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab Ko Sanmathi De Bhagavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab Ko Sanmathi De Bhagavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Om Shanti Shanti Shanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om Shanti Shanti Shanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Om Shanti Shanti Shanti Om
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om Shanti Shanti Shanti Om"/>
</div>
</pre>
