---
title: "a square b square female version song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/-lyrics"
song: "A Square B Square Female Version"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WC92XV7AmTw"
type: "happy"
singers:
  - Roshini
  - Kiran Shravan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">A Square B Square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Square B Square"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A+B Whole Square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A+B Whole Square"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rattukkum Cattukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rattukkum Cattukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththikichu Waru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththikichu Waru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaiyum Sarchaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyum Sarchaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooyadhu Yeppaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyadhu Yeppaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tigeru Spider Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tigeru Spider Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkikichu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikichu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulla Mullaala Eduththaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla Mullaala Eduththaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeya Theeyaala Anachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Theeyaala Anachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirror Effecta Koduthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirror Effecta Koduthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Tumbleru Thanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Tumbleru Thanneeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tsunamiyaa Aachidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tsunamiyaa Aachidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avana Avan Routil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Avan Routil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odi Thaan Pudichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Thaan Pudichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alattal Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alattal Illaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alekka Kavuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alekka Kavuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhikki Pazhi Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhikki Pazhi Vaangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaadi Rasichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaadi Rasichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhukki Vizha Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukki Vizha Vechi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaara Kuthichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaara Kuthichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulli Avan Vechchaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Avan Vechchaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolam Iva Pottaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam Iva Pottaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodu Avan Pottaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodu Avan Pottaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roadu Iva Pottaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadu Iva Pottaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yanaikku Kaalam Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yanaikku Kaalam Vandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poonaikkum Kaalam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonaikkum Kaalam Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulikku Vegam Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulikku Vegam Vandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanukkum Vegam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanukkum Vegam Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attack Attack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attack Attack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saamy Bootham Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamy Bootham Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attack Attack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attack Attack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oosi Mala Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi Mala Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attack Attack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attack Attack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugaru Thanni Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugaru Thanni Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attack Attack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attack Attack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Sunnu Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Sunnu Mela"/>
</div>
</pre>
