---
title: "vaana panthal song lyrics"
album: "Khyla"
artist: "Shravan"
lyricist: "Vadivarasu "
director: "Baskar Sinouvassane"
path: "/albums/khyla-lyrics"
song: "Vaana Panthal"
image: ../../images/albumart/khyla.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/arPUYc0zyCw"
type: "happy"
singers:
  - Dana Naidu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanap pandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanap pandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">pesung kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesung kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">mega pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mega pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">alaip paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaip paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">natchathira ezhuhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natchathira ezhuhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadhi nadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhi nadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">thattaan paathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattaan paathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilai pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilai pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">andha vaanap pandhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha vaanap pandhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru thaangip pidipatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru thaangip pidipatho"/>
</div>
<div class="lyrico-lyrics-wrapper">indha pesung kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha pesung kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru anuppi vaipatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru anuppi vaipatho"/>
</div>
<div class="lyrico-lyrics-wrapper">andha megap pookalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha megap pookalai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru nattu valarpatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru nattu valarpatho"/>
</div>
<div class="lyrico-lyrics-wrapper">indha alaip paatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha alaip paatai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaru kettu rasipatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaru kettu rasipatho"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalaigal illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaigal illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kai veesi parapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai veesi parapom"/>
</div>
<div class="lyrico-lyrics-wrapper">no solla yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no solla yaarumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">theerak kudipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerak kudipom"/>
</div>
<div class="lyrico-lyrics-wrapper">yes solli kalipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yes solli kalipom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">so long my flame my 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so long my flame my "/>
</div>
<div class="lyrico-lyrics-wrapper">warmth my fear my fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="warmth my fear my fight"/>
</div>
<div class="lyrico-lyrics-wrapper">the road's calling 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the road's calling "/>
</div>
<div class="lyrico-lyrics-wrapper">again tonight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="again tonight"/>
</div>
<div class="lyrico-lyrics-wrapper">dreaming under 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dreaming under "/>
</div>
<div class="lyrico-lyrics-wrapper">street lights
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="street lights"/>
</div>
<div class="lyrico-lyrics-wrapper">maybe i'll catch a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maybe i'll catch a "/>
</div>
<div class="lyrico-lyrics-wrapper">train so long
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="train so long"/>
</div>
<div class="lyrico-lyrics-wrapper">see the world until
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="see the world until"/>
</div>
<div class="lyrico-lyrics-wrapper">i can't go on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i can't go on"/>
</div>
<div class="lyrico-lyrics-wrapper">then maybe i'll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then maybe i'll"/>
</div>
<div class="lyrico-lyrics-wrapper">come traveling home
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come traveling home"/>
</div>
<div class="lyrico-lyrics-wrapper">see the world until
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="see the world until"/>
</div>
<div class="lyrico-lyrics-wrapper">i can't go on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i can't go on"/>
</div>
<div class="lyrico-lyrics-wrapper">then maybe i'll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then maybe i'll"/>
</div>
<div class="lyrico-lyrics-wrapper">come traveling home
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come traveling home"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetriyai kooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriyai kooti"/>
</div>
<div class="lyrico-lyrics-wrapper">magizhchiyai peruki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magizhchiyai peruki"/>
</div>
<div class="lyrico-lyrics-wrapper">tholvigal adharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholvigal adharku"/>
</div>
<div class="lyrico-lyrics-wrapper">mutrupulli vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutrupulli vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">igazhchiyai kazhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="igazhchiyai kazhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">pugazhchiyai vaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugazhchiyai vaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal adharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal adharku"/>
</div>
<div class="lyrico-lyrics-wrapper">vannangal adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannangal adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">vannangal adipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannangal adipom"/>
</div>
<div class="lyrico-lyrics-wrapper">edu edu rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu edu rightu"/>
</div>
<div class="lyrico-lyrics-wrapper">thoda thoda heightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda thoda heightu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavil maalai aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavil maalai aagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vidu vidu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidu vidu ponadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">kodu kodu irupathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodu kodu irupathai"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai pagalai maatralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai pagalai maatralam"/>
</div>
<div class="lyrico-lyrics-wrapper">indha ulagam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha ulagam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">adhil vaazhkai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil vaazhkai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">ponal varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponal varaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanap pandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanap pandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">pesung kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesung kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">mega pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mega pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">alaip paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaip paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">natchathira ezhuhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natchathira ezhuhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadhi nadanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhi nadanam"/>
</div>
<div class="lyrico-lyrics-wrapper">thattaan paathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattaan paathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ilai pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilai pechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">haaaaay haaayy hay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haaaaay haaayy hay"/>
</div>
<div class="lyrico-lyrics-wrapper">haaaaay haaayy hay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haaaaay haaayy hay"/>
</div>
<div class="lyrico-lyrics-wrapper">haaaaay haaayy hay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haaaaay haaayy hay"/>
</div>
<div class="lyrico-lyrics-wrapper">haaaaay haaayy hay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haaaaay haaayy hay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">endha pookalum ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha pookalum ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">azhuvathillai paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhuvathillai paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">idhanai purindhal vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhanai purindhal vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">innum innum joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum innum joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edu edu rightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edu edu rightu"/>
</div>
<div class="lyrico-lyrics-wrapper">thoda thoda heightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda thoda heightu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavil maalai aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavil maalai aagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vidu vidu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidu vidu ponadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">kodu kodu irupathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodu kodu irupathai"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai pagalai maatralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai pagalai maatralam"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai pagalai maatralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai pagalai maatralam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooooooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooooooooh"/>
</div>
</pre>
