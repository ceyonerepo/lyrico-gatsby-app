---
title: "solli tholaiyen ma song lyrics"
album: "Yaakkai"
artist: "Yuvan Shankar Raja"
lyricist: "Vignesh Shivan"
director: "Kuzhandai Velappan"
path: "/albums/yaakkai-lyrics"
song: "Solli Tholaiyen Ma"
image: ../../images/albumart/yaakkai.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DO9CWS4mb3w"
type: "love"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaanaa pona kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa pona kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana kenji ketkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana kenji ketkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona pogudhu kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona pogudhu kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli tholaiyen ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli tholaiyen ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veenaa neram pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaa neram pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En maanam kappal yerudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En maanam kappal yerudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana vandhu kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana vandhu kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli tholaiyen ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli tholaiyen ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ok solli tholanjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ok solli tholanjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara kuththa poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara kuththa poduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa venaa solla thuninjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa venaa solla thuninjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Soga songa paaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soga songa paaduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaku wait panniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku wait panniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Body weak aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body weak aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Basement shake aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basement shake aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu break aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu break aagudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-a sollaadhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-a sollaadhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju lock aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju lock aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Current illaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current illaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">oor pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Dark aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dark aagudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaram onnula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram onnula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram rendula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram rendula"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonaam varamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonaam varamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathulayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathulayum "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada en ma en ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada en ma en ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Koocha padaama nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koocha padaama nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallaa elikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa elikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Lova sonna mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lova sonna mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">En ma moraikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ma moraikura"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyae illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyae illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada pomaa pomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pomaa pomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No no summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No no summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnen ammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnen ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga porandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga porandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mattum thaanmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mattum thaanmaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda vazhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda vazhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapaaththa kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapaaththa kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli tholaiyen maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli tholaiyen maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wooh woooh woooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wooh woooh woooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona pogudhu kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona pogudhu kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli tholaiyen ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli tholaiyen ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaku wait panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku wait panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Wait panni wait panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wait panni wait panni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaku wait panniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku wait panniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Body weak aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body weak aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Basement shake aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basement shake aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu break aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu break aagudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-a sollaadhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-a sollaadhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju lock aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju lock aagudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Current illaadha oor pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current illaadha oor pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Dark aagudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dark aagudhu"/>
</div>
</pre>
