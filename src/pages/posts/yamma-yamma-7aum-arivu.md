---
title: "yamma yamma song lyrics"
album: "7aum Arivu"
artist: "Harris Jayaraj"
lyricist: "Kabilan"
director: "A.R. Murugadoss"
path: "/albums/7aum-arivu-lyrics"
song: "Yamma Yamma"
image: ../../images/albumart/7aum-arivu.jpg
date: 2011-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IjbxEc9W2Ww"
type: "sad"
singers:
  - S.P. Balasubrahmanyam
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yamma Yamma Kadhal Ponnama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Yamma Kadhal Ponnama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ena Vittu Ponathennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ena Vittu Ponathennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkule Kaayam Aachamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkule Kaayam Aachamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pattam Poochi Saayam Pochamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pattam Poochi Saayam Pochamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Aanoda Kaathal Kai Rega Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aanoda Kaathal Kai Rega Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennoda Kadhal Kai Kutta Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennoda Kadhal Kai Kutta Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavukkulla Avala Vechene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukkulla Avala Vechene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna Renda Thirudi Ponaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna Renda Thirudi Ponaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullaangulala Kaiyil Thanthaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaangulala Kaiyil Thanthaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochu Kaaththa Vaangi Ponaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochu Kaaththa Vaangi Ponaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pombalaiya Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalaiya Nambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettu Ponavanga Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Ponavanga Romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Varisaiyil Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Varisaiyil Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippa Kadaisiyil Ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Kadaisiyil Ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththedukka Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththedukka Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Moochadangum Thannaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochadangum Thannaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Muthedutha Pinnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Muthedutha Pinnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Pithamaagum Pennaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Pithamaagum Pennaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kaiyavittuthaan Poyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kaiyavittuthaan Poyaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Rendume Poiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendume Poiyaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Enbathu Veen Pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Enbathu Veen Pechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Unnale Punnaai Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Unnale Punnaai Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Paathai Kallu Mulluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Paathai Kallu Mulluda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Kadanthu Pona Aale Illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kadanthu Pona Aale Illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Oru Bothai Maaththira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Oru Bothai Maaththira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Pottukitta Moongil Yaaththirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Pottukitta Moongil Yaaththirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamma Yamma Kadhal Ponnama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Yamma Kadhal Ponnama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ena Vittu Ponathennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ena Vittu Ponathennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkule Kaayam Aachamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkule Kaayam Aachamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pattam Poochi Saayam Pochamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pattam Poochi Saayam Pochamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otta Potta Moongil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta Potta Moongil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Paatu Paada Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Paatu Paada Koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Otta Potta Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Otta Potta Pinnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Unna Pathi Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Unna Pathi Paadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Ponathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Ponathaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nanthavana Thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nanthavana Thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambi Nonthu Ponen Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi Nonthu Ponen Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Poo Illa Naaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Poo Illa Naaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thittam Pottu Nee Thirudaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thittam Pottu Nee Thirudaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etta Ninnu Nee Varudaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etta Ninnu Nee Varudaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katterumba Pol Nerudaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katterumba Pol Nerudaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Thaangathey Thaangathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Thaangathey Thaangathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillin Kolam Neeyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillin Kolam Neeyamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaanam Thaandi Ponathengamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanam Thaandi Ponathengamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathal Illaa Ooru Engadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Illaa Ooru Engadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kanna Katti Kootti Pongadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kanna Katti Kootti Pongadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamma Yamma Kaathal Ponnamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Yamma Kaathal Ponnamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ena Vittu Ponathennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ena Vittu Ponathennamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkule Kaayam Aachamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkule Kaayam Aachamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pattam Boochi Saayam Pochamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pattam Boochi Saayam Pochamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Aanoda Kadhal Kai Rega Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aanoda Kadhal Kai Rega Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennoda Kaathal Kai Kutta Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennoda Kaathal Kai Kutta Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavukkulla Avala Vechene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavukkulla Avala Vechene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanna Renda Thirudi Ponaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanna Renda Thirudi Ponaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pullaanguyala Kaiyil Thanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanguyala Kaiyil Thanthale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochu Kaathe Vaangi Ponaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochu Kaathe Vaangi Ponaley"/>
</div>
</pre>
