---
title: "kolaikara analatchu en muchu song lyrics"
album: "Thambi Vettothi Sundaram"
artist: "Vidyasagar"
lyricist: "Vairamuthu"
director: "V.C. Vadivudaiyan"
path: "/albums/thambi-vettothi-sundaram-lyrics"
song: "Kolaikara Analatchu En Muchu"
image: ../../images/albumart/thambi-vettothi-sundaram.jpg
date: 2011-11-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IBL7xsuzHeM"
type: "love"
singers:
  - Kalyani
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kolaikaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Analaachu en moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analaachu en moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi maari poyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi maari poyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kolaikaaraaaa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kolaikaaraaaa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna paatthu usur pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paatthu usur pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu pochu en moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu pochu en moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kolaikaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kolaikaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madiyil seeraattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyil seeraattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen manasa thaalaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen manasa thaalaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha alai mel paai pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha alai mel paai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen azhagai neeraattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen azhagai neeraattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaikaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Analaachu en moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analaachu en moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi maari poyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi maari poyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kolaikaaraaaa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kolaikaaraaaa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalum sorum ungaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalum sorum ungaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha thanni sellaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha thanni sellaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththu iththu ponaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththu iththu ponaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Echil muttham illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echil muttham illaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjaangani thaangaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaangani thaangaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kannum thoongaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kannum thoongaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil sugam kaangaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil sugam kaangaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaman seiyum naattaamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaman seiyum naattaamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjillaama thee illaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjillaama thee illaama "/>
</div>
<div class="lyrico-lyrics-wrapper">paththa vacha kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paththa vacha kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Butthikkulla kaththi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Butthikkulla kaththi "/>
</div>
<div class="lyrico-lyrics-wrapper">veesi povadhenna thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesi povadhenna thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha vaazha thoppukkulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha vaazha thoppukkulla "/>
</div>
<div class="lyrico-lyrics-wrapper">pandhi vaikka vaadi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandhi vaikka vaadi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal pazhangal koodaikkulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal pazhangal koodaikkulla "/>
</div>
<div class="lyrico-lyrics-wrapper">paththiyamum thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paththiyamum thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaikaariae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikaariae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah aah aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah aah aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanjil naattu kadalellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanjil naattu kadalellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kandu valai veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kandu valai veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangu muththu yellaamae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu muththu yellaamae "/>
</div>
<div class="lyrico-lyrics-wrapper">thanga kaala vilai paesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga kaala vilai paesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orakkarai yellaamae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orakkarai yellaamae "/>
</div>
<div class="lyrico-lyrics-wrapper">ottikkollum meen vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ottikkollum meen vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mattum thottaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna mattum thottaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasam ellaam poovaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasam ellaam poovaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhi kolai senju puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kolai senju puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappi sellum moodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappi sellum moodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththamittu mothaththaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththamittu mothaththaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konnu puttu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konnu puttu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa vacha pombalaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa vacha pombalaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Anju naalaa thookkamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju naalaa thookkamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesa vacha aambalaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa vacha aambalaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Meththa vaanga nenappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththa vaanga nenappillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolakaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna paatthu usur pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paatthu usur pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu pochu en moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu pochu en moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kolaikaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kolaikaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaikaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Analaachu en moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analaachu en moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi maari poyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi maari poyaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kolaikaaraaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kolaikaaraaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un madiyil seeraattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyil seeraattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen manasa thaalaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen manasa thaalaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha alai mel paai pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha alai mel paai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">En azhagae neeraattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En azhagae neeraattu"/>
</div>
</pre>
