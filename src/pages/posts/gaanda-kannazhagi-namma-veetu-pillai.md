---
title: "gaanda kannazhagi song lyrics"
album: "namma veetu pillai"
artist: "D Imman"
lyricist: "Sivakarthikeyan"
director: "pandiraj"
path: "/albums/namma-veetu-pillai-song-lyrics"
song: "gaanda kannazhagi"
image: ../../images/albumart/namma-veetu-pillai.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/b5c6ayALwRA"
type: "love"
singers:
  - Anirudh Ravichander
  - Neeti Mohan
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gaanda kannazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanda kannazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-u vittu kick-u yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look-u vittu kick-u yethum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththu palazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththu palazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sera vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sera vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaanda kannazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanda kannazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkunu thaan thatti thookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkunu thaan thatti thookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththu palazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththu palazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam onnu thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam onnu thaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponna paatha manna paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna paatha manna paakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangathoda leader-u naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathoda leader-u naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paatha pinne atha resign pannene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paatha pinne atha resign pannene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal enum twitter-u’la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal enum twitter-u’la"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalillama kaathu iruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillama kaathu iruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai follow pannathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai follow pannathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Trending aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trending aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single ippa sixer aanene…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single ippa sixer aanene…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummara gummara gummara gummara…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummara gummara gummara gummara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummara gummara gummara gummara…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummara gummara gummara gummara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaanda kannazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanda kannazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-u vittu kick-u yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look-u vittu kick-u yethum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththu palazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththu palazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sera vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sera vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavil land-u vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavil land-u vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Machu veedu kattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machu veedu kattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Internet-u illamale vaazhalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Internet-u illamale vaazhalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu pulla peththu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu pulla peththu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh mattum solli thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh mattum solli thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam kadhai solla kekalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam kadhai solla kekalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillu jillu jigaruthanda kitta vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillu jillu jigaruthanda kitta vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna apadiye saapuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna apadiye saapuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedi illa killadi thaan theriyum mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedi illa killadi thaan theriyum mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ketkamale thanthiduven ennai aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ketkamale thanthiduven ennai aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu thaan thottathume gaali aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu thaan thottathume gaali aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjinathum nenjikulla jolly aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjinathum nenjikulla jolly aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummara gummara gummara gummara…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummara gummara gummara gummara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummara gummara gummara gummara…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummara gummara gummara gummara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaanda kannazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanda kannazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-u vittu kick-u yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look-u vittu kick-u yethum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththu palazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththu palazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam onnu thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam onnu thaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponna paatha manna paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna paatha manna paakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangathoda leader-u naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathoda leader-u naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paatha pinne atha resign pannene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paatha pinne atha resign pannene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal enum twitter-u’la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal enum twitter-u’la"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalillama kaathu iruthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillama kaathu iruthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai follow pannathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai follow pannathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Trending aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trending aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single ippa sixer aanene…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single ippa sixer aanene…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummara gummara gummara gummara…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummara gummara gummara gummara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummura tappura gummura tappura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummura tappura gummura tappura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummara gummara gummara gummara…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummara gummara gummara gummara…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
</pre>
