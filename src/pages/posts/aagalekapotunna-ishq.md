---
title: "aagalekapotunna song lyrics"
album: "Ishq"
artist: "Mahati Swara Sagar"
lyricist: "Sri Mani"
director: "	S.S. Raju"
path: "/albums/ishq-lyrics"
song: "Aagalekapotunna"
image: ../../images/albumart/ishq.jpg
date: 2021-07-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/S5vR6Tu1c_Y"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagalekapothunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagalekapothunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegamandhuko chaka chaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegamandhuko chaka chaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapaleka adugesaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapaleka adugesaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhale taka taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhale taka taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo aagaavo kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo aagaavo kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika anthenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika anthenammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe nee vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe nee vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali choopinchamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali choopinchamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo aagaavo kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo aagaavo kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika anthenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika anthenammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe nee vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe nee vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali choopinchamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali choopinchamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegalekapothunnane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegalekapothunnane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalasyam cheyakika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalasyam cheyakika"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaganiyyakunda dhooraanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaganiyyakunda dhooraanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaateyve taka taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaateyve taka taka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo aagaavo kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo aagaavo kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika anthenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika anthenammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe nee vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe nee vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali choopinchamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali choopinchamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo aagaavo kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo aagaavo kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika anthenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika anthenammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthe nee vegama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthe nee vegama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali choopinchamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali choopinchamma"/>
</div>
</pre>
