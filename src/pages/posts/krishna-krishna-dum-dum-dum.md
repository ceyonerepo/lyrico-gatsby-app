---
title: "krishna krishna song lyrics"
album: "Dum Dum Dum"
artist: "Karthik Raja"
lyricist: "Na. Muthukumar - Vaali - Pa. Vijay"
director: "Azhagam Perumal"
path: "/albums/dum-dum-dum-song-lyrics"
song: "Krishna Krishna Hiyo Krishna"
image: ../../images/albumart/dum-dum-dum.jpg
date: 2001-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lZTTSje9or0"
type: "happy"
singers:
  - Harish Raghavendra
  - Karthik
  - Tippu
  - Febi Mani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">krishna krishna hiyo krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishna krishna hiyo krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda favourite god nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda favourite god nee"/>
</div>
<div class="lyrico-lyrics-wrapper">onna renda girl friends than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna renda girl friends than"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda varisu enna paru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda varisu enna paru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dating undu dancing undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dating undu dancing undu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam leelai leelai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam leelai leelai than"/>
</div>
<div class="lyrico-lyrics-wrapper">jil jil endru koothadikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil endru koothadikum "/>
</div>
<div class="lyrico-lyrics-wrapper">kootam namaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam namaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dum dum dum melan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dum melan"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kalkattu edhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kalkattu edhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">kalluriyil ethanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalluriyil ethanaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni mayil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni mayil than"/>
</div>
<div class="lyrico-lyrics-wrapper">katti kondal valkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti kondal valkai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru central jail than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru central jail than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">a joke solugindra age
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a joke solugindra age"/>
</div>
<div class="lyrico-lyrics-wrapper">vendam namaku oru cage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendam namaku oru cage"/>
</div>
<div class="lyrico-lyrics-wrapper">manam pogum margathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam pogum margathile"/>
</div>
<div class="lyrico-lyrics-wrapper">poda poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dalpin jaathi namma ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dalpin jaathi namma ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">twist dance aadi kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="twist dance aadi kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">thullum nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thullum nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne fast food pola thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne fast food pola thane"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalibatha ennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalibatha ennu"/>
</div>
<div class="lyrico-lyrics-wrapper">atha aara potu vita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha aara potu vita"/>
</div>
<div class="lyrico-lyrics-wrapper">taste pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taste pochu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vo vo vo vo vo vo vo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vo vo vo vo vo vo vo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey jil jil endru koothadikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey jil jil endru koothadikum "/>
</div>
<div class="lyrico-lyrics-wrapper">kootam namaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam namaku"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dum melan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dum melan"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kalkattu edhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kalkattu edhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">kalluriyil ethanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalluriyil ethanaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni mayil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni mayil than"/>
</div>
<div class="lyrico-lyrics-wrapper">katti kondal valkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti kondal valkai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru central jail than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru central jail than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">krishna krishna maya krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishna krishna maya krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda favourite god nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda favourite god nee"/>
</div>
<div class="lyrico-lyrics-wrapper">onna renda girl friends than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna renda girl friends than"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda varisu enna paru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda varisu enna paru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
..... <div class="lyrico-lyrics-wrapper">aatam paamai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paamai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poda painthu vinnin male
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda painthu vinnin male"/>
</div>
<div class="lyrico-lyrics-wrapper">malai megam tholai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai megam tholai"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu vada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu vada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">internetil mattum moham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="internetil mattum moham"/>
</div>
<div class="lyrico-lyrics-wrapper">ilangar netru sikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilangar netru sikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">mattram nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattram nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poomalai poda thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poomalai poda thane"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai illai yarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai illai yarum"/>
</div>
<div class="lyrico-lyrics-wrapper">naama pillai kutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naama pillai kutti "/>
</div>
<div class="lyrico-lyrics-wrapper">pera neram illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pera neram illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ho ho ho ho ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho ho ho ho ho ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalluriyil ethanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalluriyil ethanaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni mayil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni mayil than"/>
</div>
<div class="lyrico-lyrics-wrapper">katti kondal valkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti kondal valkai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru central jail than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru central jail than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">krishna krishna maya krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishna krishna maya krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda favourite god nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda favourite god nee"/>
</div>
<div class="lyrico-lyrics-wrapper">onna renda girl friends than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna renda girl friends than"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda varisu enna paru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda varisu enna paru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dating undu dancing undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dating undu dancing undu"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam leelai leelai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam leelai leelai than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vo vo vo vo vo vo vo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vo vo vo vo vo vo vo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalluriyil ethanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalluriyil ethanaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni mayil than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni mayil than"/>
</div>
<div class="lyrico-lyrics-wrapper">katti kondal valkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti kondal valkai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru central jail than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru central jail than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jil jil endru koothadikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jil jil endru koothadikum "/>
</div>
<div class="lyrico-lyrics-wrapper">kootam namaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootam namaku"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dum melan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dum melan"/>
</div>
<div class="lyrico-lyrics-wrapper">kotti kalkattu edhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotti kalkattu edhuku"/>
</div>
</pre>
