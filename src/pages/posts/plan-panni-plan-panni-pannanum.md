---
title: "plan panni song lyrics"
album: "Plan Panni Pannanum"
artist: "Yuvan Shankar Raja"
lyricist: "Arunraja Kamaraj"
director: "Badri Venkatesh"
path: "/albums/plan-panni-pannanum-lyrics"
song: "Plan Panni"
image: ../../images/albumart/plan-panni-pannanum.jpg
date: 2021-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mr4K6iVU0E4"
type: "mass"
singers:
  - Premgi Amaren
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Plan Panni Pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan Panni Pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhegam Varaathe Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhegam Varaathe Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rule Keelu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rule Keelu Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham Tharaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham Tharaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Mukki Nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Mukki Nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katti Podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katti Podaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thikki Thikki Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thikki Thikki Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Confusion Aaagaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confusion Aaagaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Dead End Ah Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dead End Ah Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Weekend Paakkaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Weekend Paakkaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Treatmentu Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Treatmentu Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Unfriend Pannaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Unfriend Pannaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Vetti Pecha Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Vetti Pecha Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Koottam Seraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Koottam Seraathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Happinessa Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Happinessa Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Planaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Planaa Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plan Panni Pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan Panni Pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhegam Varaathe Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhegam Varaathe Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rule Keelu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rule Keelu Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham Tharaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham Tharaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dance Flooru Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Flooru Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Bodhi Tree Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Bodhi Tree Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Motivation Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Motivation Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mindum Free Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mindum Free Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devathai Inge Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathai Inge Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Kaattuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kaattuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Harmongal Adha Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harmongal Adha Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kushiyaaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kushiyaaguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedhaalam Pola Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhaalam Pola Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Seruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Seruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidukaadha Puthiraanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidukaadha Puthiraanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Vilayaaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Vilayaaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaraga Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraga Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasonnu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasonnu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Nerathil Maranthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Nerathil Maranthome"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Funna Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Funna Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weekend Thaan Namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weekend Thaan Namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Play Ground Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play Ground Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Sweet End Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Sweet End Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathathaan Poraadalaam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathathaan Poraadalaam Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plan Panni Pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan Panni Pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhegam Varaathe Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhegam Varaathe Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plan Panni Pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan Panni Pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhegam Varaathe Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhegam Varaathe Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rule Keelu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rule Keelu Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham Tharaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham Tharaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dance Flooru Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance Flooru Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Bodhi Tree Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Bodhi Tree Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Motivation Vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Motivation Vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mindum Free Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mindum Free Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Circlukkul Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Circlukkul Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Control Pannaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Control Pannaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kumbalukkul Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kumbalukkul Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Template Aavaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Template Aavaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Suththi Oru Kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suththi Oru Kottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Gate Ah Podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Gate Ah Podaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Puththam Pudhu Plana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Puththam Pudhu Plana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pottu Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pottu Thaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plan Panni Pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plan Panni Pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhegam Varaathe Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhegam Varaathe Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rule Keelu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rule Keelu Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham Tharaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham Tharaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plannu Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plannu Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Plannu Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plannu Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Plannu Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plannu Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Plannu Plan Plan Plan Plannnuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plannu Plan Plan Plan Plannnuu"/>
</div>
</pre>
