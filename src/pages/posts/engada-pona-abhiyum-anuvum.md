---
title: "engada pona song lyrics"
album: "Abhiyum Anuvum"
artist: "Dharan Kumar"
lyricist: "Madhan Karky"
director: "B.R. Vijayalakshmi"
path: "/albums/abhiyum-anuvum-lyrics"
song: "Engada Pona"
image: ../../images/albumart/abhiyum-anuvum.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xVA5Oew6iiU"
type: "sad"
singers:
  - Benny Dayal
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal Kasakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal Kasakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Paarvaigal Arukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Paarvaigal Arukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosiya Theendalgal Erikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosiya Theendalgal Erikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Erivom Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erivom Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Maatrida Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Maatrida Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Thaandida Ninaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Thaandida Ninaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thondridum Kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thondridum Kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kelvikuri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kelvikuri "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Valargirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Valargirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Karaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Karaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttru Pulli Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttru Pulli Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkkai Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkkai Virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Paranthiduvọm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Paranthiduvọm "/>
</div>
<div class="lyrico-lyrics-wrapper">Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pona Engada Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Engada Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pọna Engada Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Engada Pọna"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pọna Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalil Vizhuvathum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalil Vizhuvathum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Udaivathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Udaivathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bọọmiyil Naalume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bọọmiyil Naalume "/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhvathu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhvathu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayinum Ivvali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayinum Ivvali "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaararivaarọ Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaararivaarọ Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruthiyin Varaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthiyin Varaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruthigal Seithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruthigal Seithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuthiya Yaavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthiya Yaavume"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhuthilyil Kidapathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthilyil Kidapathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaigalai Mithithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaigalai Mithithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamum Nadanthiduvọm Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamum Nadanthiduvọm Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthamidum Antha Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamidum Antha Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kọththi Kọlluthe Iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kọththi Kọlluthe Iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yudha Kalamena Manathu Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yudha Kalamena Manathu Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marabanukkalin Pizhaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marabanukkalin Pizhaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anu Anuvaai Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anu Anuvaai Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Azhivathu Sariyaa Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Azhivathu Sariyaa Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kelvikuri Unnul Valargirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kelvikuri Unnul Valargirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Karaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Karaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivọm Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivọm Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttru Pulli Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttru Pulli Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkkai Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkkai Virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Paranthiduvọm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Paranthiduvọm "/>
</div>
<div class="lyrico-lyrics-wrapper">Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal Kasakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal Kasakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Paarvaigal Arukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Paarvaigal Arukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kọọsiya Theendalgal Erikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kọọsiya Theendalgal Erikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Erivọm Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erivọm Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Maatrida Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Maatrida Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Thaandida Ninaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Thaandida Ninaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thọndridum Kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thọndridum Kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapọm Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapọm Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pọna Engada Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Engada Pọna"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pọna Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engadi Pona Engadi Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Engadi Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthadi Valikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthadi Valikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engadi Pọna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pọna Engada Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Engada Pọna"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pọna Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
</pre>
