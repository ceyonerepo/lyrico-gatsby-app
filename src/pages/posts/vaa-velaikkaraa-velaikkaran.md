---
title: "vaa velaikkaraa song lyrics"
album: "Velaikkaran"
artist: "Anirudh Ravichander"
lyricist: "Vivek"
director: "Mohan Raja"
path: "/albums/velaikkaran-lyrics"
song: "Vaa Velaikkaraa"
image: ../../images/albumart/velaikkaran.jpg
date: 2017-12-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2Wz-sgMHCRo"
type: "Motivational"
singers:
  - Shakthisree Gopalan
  - Bjorn Surrao
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un Kai Naalai Uyaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Naalai Uyaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaraatho Uyaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaraatho Uyaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Un Paer Ezhudhaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Un Paer Ezhudhaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhudhaadho Ezhudhaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhudhaadho Ezhudhaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thookkida Un Thozhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thookkida Un Thozhiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theivamey Nee Seithidum Pallaakkiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theivamey Nee Seithidum Pallaakkiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">May Maadhamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="May Maadhamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Maadhama Yaar Sonnathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maadhama Yaar Sonnathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalumey Un Vervaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalumey Un Vervaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondaadidum Vaa Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadidum Vaa Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Kai Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Kai Viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhigal Aagudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhigal Aagudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Seigiraai Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Seigiraai Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigal Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivan Neeyada Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan Neeyada Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaikkara Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkara Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Thoongi Viduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thoongi Viduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandathillai Thuliyum Oivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathillai Thuliyum Oivu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ododi Uzhaiththum Nagaraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ododi Uzhaiththum Nagaraamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirkkum Avanin Vaazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum Avanin Vaazhvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaasaiyil Man Veesiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaasaiyil Man Veesiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Aasaiyai Kodiyetrinaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Aasaiyai Kodiyetrinaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhirkaalamey Namadhaagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirkaalamey Namadhaagavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Boomiyil Kudiyetrinaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Boomiyil Kudiyetrinaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Kai Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Kai Viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhigal Aagudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhigal Aagudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Seigiraai Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Seigiraai Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigal Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivan Neeyada Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan Neeyada Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaikkara Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkara Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Kai Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Kai Viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhigal Aagudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhigal Aagudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagai Seigiraai Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Seigiraai Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valigal Thaangi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Thaangi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigal Seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaivan Neeyada Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan Neeyada Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaikkara Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkara Velaikkara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkara"/>
</div>
</pre>
