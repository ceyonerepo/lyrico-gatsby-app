---
title: "evada unna petha song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "CS Amudhan - Chandru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Evada Unna Petha"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DMMRiENC5Lc"
type: "happy"
singers:
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evada evada unna petha petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evada evada unna petha petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila kedacha ava setha setha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila kedacha ava setha setha"/>
</div>
<div class="lyrico-lyrics-wrapper">Evada evada unna petha petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evada evada unna petha petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila kedacha ava setha setha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila kedacha ava setha setha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhada avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhada avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhada avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhada avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evada evada unna petha petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evada evada unna petha petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Petha petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petha petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila kedacha ava setha setha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila kedacha ava setha setha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi veliya vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi veliya vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai selavukku naanga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai selavukku naanga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiss adichida naanga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiss adichida naanga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Girlfriend irukkunu getha sutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girlfriend irukkunu getha sutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungalukku naanga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalukku naanga venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna sundaykku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sundaykku love pandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mondaykku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna mondaykku love pandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna tuesdaykku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna tuesdaykku love pandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna fridaykku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna fridaykku love pandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unkkuda irukkum bodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unkkuda irukkum bodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En friend-ah route vidra naayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En friend-ah route vidra naayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu culture-nu enkittayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu culture-nu enkittayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee lecture adikkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee lecture adikkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saniyanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saniyanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adakki aala nenaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki aala nenaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Donkey neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donkey neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un solla kettu aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un solla kettu aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Monke-ah naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monke-ah naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bull shit
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bull shit"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhada avanaVetraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhada avanaVetraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhada avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhada avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evada evada unna petha petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evada evada unna petha petha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila kedacha ava sethaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila kedacha ava sethaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dummu thanni adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummu thanni adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naasamaathaan pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee naasamaathaan pova"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naanum thiruthi thiruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naanum thiruthi thiruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">En life-eh tholaikkanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life-eh tholaikkanuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna kondaikku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kondaikku love pandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna mandaikku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna mandaikku love pandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sandaikku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sandaikku love pandra"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thondaikku love pandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thondaikku love pandra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love failure-nu solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love failure-nu solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Acidu adikkira neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acidu adikkira neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Engakitta yenda kolaveri paththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engakitta yenda kolaveri paththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamkettu pesuringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamkettu pesuringa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enna vittu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna vittu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Loss enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loss enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mokka moonjikkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mokka moonjikkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooril ponna illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooril ponna illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhada avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhada avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetraa avana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetraa avana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaiyae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla iruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla iruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nalla iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nalla iruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Samarpanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarpanam"/>
</div>
</pre>
