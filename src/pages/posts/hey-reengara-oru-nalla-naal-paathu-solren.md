---
title: "hey reengara song lyrics"
album: "Oru Nalla Naal Paathu Solren"
artist: "Justin Prabhakaran"
lyricist: "Karthik Netha"
director: "Arumuga Kumar"
path: "/albums/oru-nalla-naal-paathu-solren-lyrics"
song: "Hey Reengara"
image: ../../images/albumart/oru-nalla-naal-paathu-solren.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cmG2UtKOSEw"
type: "love"
singers:
  - Nikhita Gandhi
  - Saisharan
  - Mark Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey reengaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey reengaara"/>
</div>
<div class="lyrico-lyrics-wrapper">You are so crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are so crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen eerthayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen eerthayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal poo veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal poo veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itho mazhai oonjalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itho mazhai oonjalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho yenai aattuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho yenai aattuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athae anal meethilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athae anal meethilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nila kudai aguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila kudai aguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill honey honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill honey honey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey reengaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey reengaara"/>
</div>
<div class="lyrico-lyrics-wrapper">You are so crazyoooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are so crazyoooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyay yae thaararaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyay yae thaararaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agila ulaga aruvai kathaiyin thilagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agila ulaga aruvai kathaiyin thilagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagala manamum virumbi rasikkum uruvamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagala manamum virumbi rasikkum uruvamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinara thinara thurathi varuthum oruvanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinara thinara thurathi varuthum oruvanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeninum ivanin iyalbu adada pidikumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeninum ivanin iyalbu adada pidikumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verupathu yethu pidipathu yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verupathu yethu pidipathu yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai athai sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai athai sollividu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena ivanidam kettal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena ivanidam kettal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru nalla naal paathu solren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nalla naal paathu solren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey reengaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey reengaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahah ahahaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahah ahahaah"/>
</div>
<div class="lyrico-lyrics-wrapper">You are so crazyeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are so crazyeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamane eh eh hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamane eh eh hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Irrangi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irrangi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Evano un kaala sutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evano un kaala sutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Posungi ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posungi ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi erimettu sarayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi erimettu sarayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum kothichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum kothichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un yamakantha moonjal adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un yamakantha moonjal adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamanae sethen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamanae sethen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan porali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan porali"/>
</div>
<div class="lyrico-lyrics-wrapper">Velven vazhenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velven vazhenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaa.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaa."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey reengaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey reengaara"/>
</div>
<div class="lyrico-lyrics-wrapper">You are so crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are so crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill honey honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill honey honey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan porali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan porali"/>
</div>
<div class="lyrico-lyrics-wrapper">Velven vazhenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velven vazhenthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chill honey honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill honey honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan porali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan porali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill honey honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill honey honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey reengaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey reengaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharara ra rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharara ra rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey reengaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey reengaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill honey honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill honey honey"/>
</div>
</pre>
