---
title: "oduran oduran song lyrics"
album: "Kasu Mela Kasu"
artist: "M.S. Pandian"
lyricist: "Karuppaiah"
director: "K.S. Pazhani"
path: "/albums/kasu-mela-kasu-lyrics"
song: "Oduran Oduran"
image: ../../images/albumart/kasu-mela-kasu.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lgqfEMB7IAY"
type: "happy"
singers:
  - Thanjai Antony Dass
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oduran oduran oduran oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oduran oduran oduran oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna theduran theduran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna theduran theduran "/>
</div>
<div class="lyrico-lyrics-wrapper">theduran theduran odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduran theduran odi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi theduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi theduran"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkala sikkala ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkala sikkala ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkala tension aaguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkala tension aaguran"/>
</div>
<div class="lyrico-lyrics-wrapper">pilla love panna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilla love panna "/>
</div>
<div class="lyrico-lyrics-wrapper">appan pakuran ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appan pakuran ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu kulla vathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu kulla vathu"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu netta kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu netta kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi than kalyanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi than kalyanamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oduran oduran oduran oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oduran oduran oduran oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna theduran theduran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna theduran theduran "/>
</div>
<div class="lyrico-lyrics-wrapper">theduran theduran odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduran theduran odi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi theduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi theduran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aiyayo bagavaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyayo bagavaane"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kolaya kola kolaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kolaya kola kolaya"/>
</div>
<div class="lyrico-lyrics-wrapper">munthirika ponnu engu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthirika ponnu engu "/>
</div>
<div class="lyrico-lyrics-wrapper">iruka engu iruka engu iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruka engu iruka engu iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">color color ah vaga vagaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="color color ah vaga vagaya"/>
</div>
<div class="lyrico-lyrics-wrapper">ullathapa ponnu sikudume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullathapa ponnu sikudume"/>
</div>
<div class="lyrico-lyrics-wrapper">sikudume luck iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikudume luck iruntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mayilapporil maranju irupalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilapporil maranju irupalo"/>
</div>
<div class="lyrico-lyrics-wrapper">avan manthaveliyil olinju irupalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan manthaveliyil olinju irupalo"/>
</div>
<div class="lyrico-lyrics-wrapper">omr il kudi irupaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="omr il kudi irupaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kodambakam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kodambakam "/>
</div>
<div class="lyrico-lyrics-wrapper">kulunga vaipaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulunga vaipaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">metro train il parappalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="metro train il parappalo"/>
</div>
<div class="lyrico-lyrics-wrapper">merina vil nadapaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merina vil nadapaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">millionara irupaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="millionara irupaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">minu minu nu minnuvalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minu minu nu minnuvalo"/>
</div>
<div class="lyrico-lyrics-wrapper">face book la jolipaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="face book la jolipaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">whatsup la varuvaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whatsup la varuvaale"/>
</div>
<div class="lyrico-lyrics-wrapper">internet il irupaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="internet il irupaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">instagram il siripaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="instagram il siripaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi thaan kalyaanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi thaan kalyaanamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oduran oduran oduran oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oduran oduran oduran oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna theduran theduran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna theduran theduran "/>
</div>
<div class="lyrico-lyrics-wrapper">theduran theduran odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduran theduran odi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi theduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi theduran"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkala sikkala ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkala sikkala ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkala tension aaguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkala tension aaguran"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyaiyo bagavaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyaiyo bagavaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">it park ah asara vaipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="it park ah asara vaipo"/>
</div>
<div class="lyrico-lyrics-wrapper">ava angel pola enga vaipalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava angel pola enga vaipalo"/>
</div>
<div class="lyrico-lyrics-wrapper">panagal parkil poothu irupalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panagal parkil poothu irupalo"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kathal panna kathu irupalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kathal panna kathu irupalo"/>
</div>
<div class="lyrico-lyrics-wrapper">bilgates ponna irupaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bilgates ponna irupaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">suitcase kondu varuvaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suitcase kondu varuvaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">builder ponna irupaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="builder ponna irupaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">buildup panni nadipaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buildup panni nadipaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">sandal wood ah manapaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandal wood ah manapaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">thekku wood ah anaipalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekku wood ah anaipalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ambal motta siripaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambal motta siripaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku veeta irupaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku veeta irupaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi thaan kalyanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi thaan kalyanamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oduran oduran oduran oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oduran oduran oduran oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna theduran theduran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna theduran theduran "/>
</div>
<div class="lyrico-lyrics-wrapper">theduran theduran odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduran theduran odi"/>
</div>
<div class="lyrico-lyrics-wrapper">odi theduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi theduran"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkala sikkala ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkala sikkala ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkala tension aaguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkala tension aaguran"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyaiyo bagavaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyaiyo bagavaane"/>
</div>
</pre>
