---
title: "gogulathu rathai vanthal song lyrics"
album: "Aanandham"
artist: "S. A. Rajkumar"
lyricist: "P. Vijay"
director: "N. Lingusamy"
path: "/albums/aanandham-lyrics"
song: "Gogulathu Rathai Vanthal"
image: ../../images/albumart/aanandham.jpg
date: 2001-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JYs6EexvwSI"
type: "happy"
singers:
  - Unni Menon
  - Sujatha
  - S. P. B. Charan
  - Yugendran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sudi thantha sudarkodiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudi thantha sudarkodiye"/>
</div>
<div class="lyrico-lyrics-wrapper">subavelai nee varuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="subavelai nee varuga"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi konda thiraviyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi konda thiraviyame"/>
</div>
<div class="lyrico-lyrics-wrapper">then maalai pola varuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then maalai pola varuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minnum ambu vili chandira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnum ambu vili chandira"/>
</div>
<div class="lyrico-lyrics-wrapper">bimbam ponnoli veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bimbam ponnoli veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">mangai maithili ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangai maithili ival"/>
</div>
<div class="lyrico-lyrics-wrapper">semmalar paatha thandaigal aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semmalar paatha thandaigal aada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sangam soola varuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangam soola varuga"/>
</div>
<div class="lyrico-lyrics-wrapper">sri rangam thaalai adaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sri rangam thaalai adaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">valar thingalagi varuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valar thingalagi varuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kogulathu radhai vanthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kogulathu radhai vanthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kalyana therile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kalyana therile"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyana therile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana therile"/>
</div>
<div class="lyrico-lyrics-wrapper">mithilai nagar seethai vanthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithilai nagar seethai vanthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veetotu vaalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veetotu vaalave"/>
</div>
<div class="lyrico-lyrics-wrapper">veetotu vaalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetotu vaalave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha thenmadurai meenal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha thenmadurai meenal"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaketra vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaketra vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">seethamai kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seethamai kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">thaipasam kondu vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaipasam kondu vanthal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kogulathu radhai vanthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kogulathu radhai vanthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kalyana therile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kalyana therile"/>
</div>
<div class="lyrico-lyrics-wrapper">mithilai nagar seethai vanthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithilai nagar seethai vanthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veetotu vaalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veetotu vaalave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponnu kondu vantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu kondu vantha "/>
</div>
<div class="lyrico-lyrics-wrapper">seer vangi vaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seer vangi vaika"/>
</div>
<div class="lyrico-lyrics-wrapper">perusa veedu onu katunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perusa veedu onu katunga"/>
</div>
<div class="lyrico-lyrics-wrapper">thanga maapilaiki eedaga neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanga maapilaiki eedaga neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">innum pathu madangu kottunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum pathu madangu kottunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga maapilaiyin nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga maapilaiyin nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">ammanai aadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammanai aadi"/>
</div>
<div class="lyrico-lyrics-wrapper">selaiyile mudunju jeyipale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selaiyile mudunju jeyipale"/>
</div>
<div class="lyrico-lyrics-wrapper">enga ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moonu molam malligai poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu molam malligai poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam alvavum pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam alvavum pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">entha ponnum ambala nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha ponnum ambala nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">adi thalai sanju mayangume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi thalai sanju mayangume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanjavur bommai pola than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanjavur bommai pola than"/>
</div>
<div class="lyrico-lyrics-wrapper">unga mapilai than thalai aatuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga mapilai than thalai aatuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">aama thalai aatuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama thalai aatuvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thesingu raja enga annan hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thesingu raja enga annan hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kuthurai vaalatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kuthurai vaalatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">aama vaalatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama vaalatuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei thapama enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei thapama enga"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu seivalle thanthiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu seivalle thanthiran"/>
</div>
<div class="lyrico-lyrics-wrapper">singatha katti podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singatha katti podum"/>
</div>
<div class="lyrico-lyrics-wrapper">thalagani manthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalagani manthiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">potiyellam potu pathomdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potiyellam potu pathomdi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu namakula thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu namakula thanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">jodiyathan nalla parudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodiyathan nalla parudi"/>
</div>
<div class="lyrico-lyrics-wrapper">madura meenachi sokkandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madura meenachi sokkandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vellipani mutrathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellipani mutrathil"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam enum thotatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam enum thotatil"/>
</div>
<div class="lyrico-lyrics-wrapper">malligai poothatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malligai poothatho"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu malligai poothatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu malligai poothatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maada kuyil sathathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maada kuyil sathathil"/>
</div>
<div class="lyrico-lyrics-wrapper">manjal mugam naanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal mugam naanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">mangalam vanthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangalam vanthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam mangalam vanthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam mangalam vanthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ini engal nenja koodathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini engal nenja koodathil"/>
</div>
<div class="lyrico-lyrics-wrapper">deepa thiruvila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepa thiruvila"/>
</div>
<div class="lyrico-lyrics-wrapper">engal vaanil velicham veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal vaanil velicham veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna vennila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engal anbuku annan panbuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal anbuku annan panbuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thendral santhanam pusutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral santhanam pusutho"/>
</div>
<div class="lyrico-lyrics-wrapper">engal veetukul veesum thendralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal veetukul veesum thendralai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thevathai vanthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thevathai vanthatho"/>
</div>
</pre>
