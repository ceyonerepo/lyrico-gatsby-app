---
title: "uyire song lyrics"
album: "Gauthamante Radham"
artist: "Ankit Menon - Anuraj O. B"
lyricist: "Vinayak Sasikumar"
director: "Anand Menon"
path: "/albums/gauthamante-radham-lyrics"
song: "Uyire"
image: ../../images/albumart/gauthamante-radham.jpg
date: 2020-01-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/-hsdXiwA4c0"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyire kavarum uyire pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire kavarum uyire pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthaanu nee enthaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthaanu nee enthaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah kadhal mazhayaye thanuvil cherum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah kadhal mazhayaye thanuvil cherum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaranu nee aaranu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaranu nee aaranu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyare chirapol pravin nilavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyare chirapol pravin nilavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaril madhuvo kaanaan kanavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaril madhuvo kaanaan kanavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannodu kannodu kannoramay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannodu kannodu kannoramay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathodu kathodu kathoramay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathodu kathodu kathoramay"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu nenjodu nenjoramay niray
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjodu nenjoramay niray"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thorathe thorathe theeratheyay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thorathe thorathe theeratheyay"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayathe mayathe mayatheyaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayathe mayathe mayatheyaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennolum ninnolum ennolamay padaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennolum ninnolum ennolamay padaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ta ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ta ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire uyirin uyare moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire uyirin uyare moodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaanu nee theeyaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaanu nee theeyaanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal kanalaye akame neerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal kanalaye akame neerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Novanu nee novanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novanu nee novanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini en nizhalaye vazhvin nadhiyaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini en nizhalaye vazhvin nadhiyaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Njanen arike ninne thiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njanen arike ninne thiraye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannodu kannodu kannoramay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannodu kannodu kannoramay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathodu kathodu kathoramay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathodu kathodu kathoramay"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu nenjodu nenjoramay niraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjodu nenjoramay niraye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thorathe thorathe theeratheyay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thorathe thorathe theeratheyay"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayathe mayathe mayatheyay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayathe mayathe mayatheyay"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennolum ninnolum ennolamay padaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennolum ninnolum ennolamay padaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ta ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta ra ra ra"/>
</div>
</pre>
