---
title: "ada da en meethu song lyrics"
album: "Pathinaaru"
artist: "Yuvan Shankar Raja"
lyricist: "Karthik Netha"
director: "Sabapathy - Dekshinamurthy"
path: "/albums/pathinaaru-lyrics"
song: "Ada Da En Meethu"
image: ../../images/albumart/pathinaaru.jpg
date: 2011-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_wUOcyMbXys"
type: "love"
singers:
  - Hariharan
  - Bela Shende
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adada En Meethu Devathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada En Meethu Devathai "/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanai Kaathal Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanai Kaathal Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaye Engengum Katchigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaye Engengum Katchigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Kattidum Kaathal Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattidum Kaathal Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanum Varam Pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanum Varam Pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkalam Vasam Vasam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkalam Vasam Vasam Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Paathai Maram Yaavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Paathai Maram Yaavum "/>
</div>
<div class="lyrico-lyrics-wrapper">Enakaaga Mazhai Mazhai Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaaga Mazhai Mazhai Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Unnai Ennai Yaar Serthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Unnai Ennai Yaar Serthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Vasam Thannai Yaar Parthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Vasam Thannai Yaar Parthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyril Oru Kodi Vanavil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyril Oru Kodi Vanavil "/>
</div>
<div class="lyrico-lyrics-wrapper">Poothidum Kaathal Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothidum Kaathal Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethire Nee Vanthaal Vanavil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethire Nee Vanthaal Vanavil "/>
</div>
<div class="lyrico-lyrics-wrapper">Thondridum Kaathal Athuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondridum Kaathal Athuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramiyam Thathumbum Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramiyam Thathumbum Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kandathum Piranthathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandathum Piranthathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil Vazhiyum Neeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Vazhiyum Neeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Sakkarai Thiraluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Sakkarai Thiraluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Varum Veethiyil Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Varum Veethiyil Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Vasanai Kamaluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Vasanai Kamaluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathalin Padalai Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalin Padalai Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Jeeven Paadiduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Jeeven Paadiduthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La La La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Vanthu Idiyai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Vanthu Idiyai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathil Meethu Vizhunthadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathil Meethu Vizhunthadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathalilae Nammai Nam Izhandoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalilae Nammai Nam Izhandoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Nam Unarndoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Nam Unarndoam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kalandoam Nam Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kalandoam Nam Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Alainthoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Alainthoam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katrinil Alayum Iragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrinil Alayum Iragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Paravai Uthirthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Paravai Uthirthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathalil Mayangum Manthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalil Mayangum Manthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Kadavulum Koduthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kadavulum Koduthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootiya Kathavin Idukil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootiya Kathavin Idukil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Velicham Nuzhainthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Velicham Nuzhainthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimaiyin Viralai Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimaiyin Viralai Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Kaathalum Varudutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Kaathalum Varudutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanda Naalil Irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanda Naalil Irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu Vaazhkai Kidaithathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu Vaazhkai Kidaithathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennuyirai Thirakum Saavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyirai Thirakum Saavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathu Uyiril Irukuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu Uyiril Irukuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaliyae Ithu Vaesham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaliyae Ithu Vaesham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Petham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Petham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aethum Illai Pani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aethum Illai Pani "/>
</div>
<div class="lyrico-lyrics-wrapper">Thuzhiyil Saayam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuzhiyil Saayam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada En Meethu Devathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada En Meethu Devathai "/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanai Kaathal Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanai Kaathal Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaye Engengum Katchigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaye Engengum Katchigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Kattidum Kaathal Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattidum Kaathal Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanum, Varam Pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanum, Varam Pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkalam Vasam Vasam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkalam Vasam Vasam Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Paathai Maram Yaavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Paathai Maram Yaavum "/>
</div>
<div class="lyrico-lyrics-wrapper">Enakaaga Mazhai Mazhai Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaaga Mazhai Mazhai Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Unnai Ennai Yaar Serthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Unnai Ennai Yaar Serthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Vasam Thannai Yaar Parthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Vasam Thannai Yaar Parthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyril Oru Kodi Vanavil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyril Oru Kodi Vanavil "/>
</div>
<div class="lyrico-lyrics-wrapper">Poothidum Kaathal Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothidum Kaathal Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethire Nee Vanthaal Vanavil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethire Nee Vanthaal Vanavil "/>
</div>
<div class="lyrico-lyrics-wrapper">Thondridum Kaathal Athuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondridum Kaathal Athuvo"/>
</div>
</pre>
