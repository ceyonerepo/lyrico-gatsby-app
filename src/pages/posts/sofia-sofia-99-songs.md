---
title: "sofia sofia song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Madhan Karky"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Sofia Sofia Sofia"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x5uYsms9QOk"
type: "Love"
singers:
  - Sreekanth Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yavarum keela en paadal ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yavarum keela en paadal ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee matum ketkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee matum ketkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimai than en thunai endru valthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimai than en thunai endru valthen"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame nee aagirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame nee aagirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udaithe kidanthen sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaithe kidanthen sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram thundena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram thundena"/>
</div>
<div class="lyrico-lyrics-wrapper">anaithe inaithai sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithe inaithai sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">aaginen ondrena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaginen ondrena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sudamale theendiya thee pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudamale theendiya thee pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal pesugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal pesugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">irulin kadaisi thuligal kaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulin kadaisi thuligal kaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">erikinrai vegamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erikinrai vegamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un mounathile sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mounathile sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">thai moli ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai moli ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kangalinal sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangalinal sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiyai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiyai aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagal uyirai thoduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagal uyirai thoduval"/>
</div>
<div class="lyrico-lyrics-wrapper">sirippal ennai pandhaduval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirippal ennai pandhaduval"/>
</div>
<div class="lyrico-lyrics-wrapper">inimai imaiyal manathul veesuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimai imaiyal manathul veesuval"/>
</div>
<div class="lyrico-lyrics-wrapper">isaiyin saaral amuthai maatruval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaiyin saaral amuthai maatruval"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam nenjile malarai malarvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam nenjile malarai malarvaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viralgal korkaiyil sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralgal korkaiyil sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiye kaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiye kaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">ithalgal korkaiyil sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithalgal korkaiyil sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaname naavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaname naavile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sudamale theendiya thee pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudamale theendiya thee pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal pesugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal pesugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">irulin kadaisi thuligal kaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulin kadaisi thuligal kaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">erikinrai vegamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erikinrai vegamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">sofia sofia sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sofia sofia sofia"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udaithe kidanthen sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaithe kidanthen sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram thundena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram thundena"/>
</div>
<div class="lyrico-lyrics-wrapper">anaithe inaithai sofia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithe inaithai sofia"/>
</div>
<div class="lyrico-lyrics-wrapper">aaginen ondrena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaginen ondrena"/>
</div>
</pre>
