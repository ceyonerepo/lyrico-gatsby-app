---
title: "jolly o gymkhana song lyrics"
album: "Beast"
artist: "Anirudh Ravichander"
lyricist: "Ku Karthik"
director: "Nelson"
path: "/albums/beast-lyrics"
song: "Jolly O Gymkhana"
image: ../../images/albumart/beast.jpg
date: 2022-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/76wGlgnPluQ"
type: "happy"
singers:
  - Thalapathy Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendula Onnu Paakalaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendula Onnu Paakalaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkuriya Themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkuriya Themba"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavum Life’u Thirumbalaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Life’u Thirumbalaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namburiya Nanba Yappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namburiya Nanba Yappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendula Onnu Paakalaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendula Onnu Paakalaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkuriya Themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkuriya Themba"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavum Life’u Thirumbalaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Life’u Thirumbalaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namburiya Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namburiya Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Inga Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Inga Vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamuruthi Paathaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamuruthi Paathaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraama Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaraama Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Odhungi Povaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Odhungi Povaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athanaiyum Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Empty-Aathan Ninnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Empty-Aathan Ninnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patharaama Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patharaama Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Beast’u Neethanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Beast’u Neethanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamamma Hey Raamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamamma Hey Raamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasamma Hey Raasamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasamma Hey Raasamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkudha En Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkudha En Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamamma Hey Raamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamamma Hey Raamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasamma Hey Raasamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasamma Hey Raasamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadhu Serdhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadhu Serdhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serdhaana Serdhaanpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serdhaana Serdhaanpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Thayangi Nikkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Thayangi Nikkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamba Paathu Oduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamba Paathu Oduriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Onna Neeye Korachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Onna Neeye Korachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeda Pottu Paakuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeda Pottu Paakuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Weight Ah Kaattanundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Weight Ah Kaattanundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Modhi Paakkanudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Modhi Paakkanudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanba Evan Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanba Evan Vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alara Vittu Getha Kaattanundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alara Vittu Getha Kaattanundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyiladhaan Nee Pudichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyiladhaan Nee Pudichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Prachana Theeraadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachana Theeraadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Adha Nee Erinjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Adha Nee Erinjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension’u Yeraadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension’u Yeraadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukkudhaan Nee Bayandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkudhaan Nee Bayandhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaikku Aavaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikku Aavaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkmae Nee Adangaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkmae Nee Adangaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriya Vidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriya Vidaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polamburavan Thamaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamburavan Thamaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuthu Ninnaa Nee Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuthu Ninnaa Nee Maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Onnu Nenachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Onnu Nenachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Nadathanum Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Nadathanum Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Confirm!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Morathaan Thottaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Morathaan Thottaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maela Kaidhaan Vechaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maela Kaidhaan Vechaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi Adha Kudutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi Adha Kudutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Beast’u Neethanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Beast’u Neethanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamamma Hey Raamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamamma Hey Raamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasamma Hey Raasamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasamma Hey Raasamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkudha En Gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkudha En Gaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamamma Hey Raamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamamma Hey Raamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasamma Hey Raasamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasamma Hey Raasamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadhu Serdhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadhu Serdhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serdhaana Serdhaanpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serdhaana Serdhaanpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly O Gymkhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly O Gymkhana"/>
</div>
</pre>
