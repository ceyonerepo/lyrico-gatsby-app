---
title: "achchamillai achchamillai song lyrics"
album: "Singam II"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-2-lyrics"
song: "Achchamillai Achchamillai"
image: ../../images/albumart/singam-2.jpg
date: 2013-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/g_IM0rWvID0"
type: "Motivational"
singers:
  - Devi Sri Prasad
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Achamillai Achamillai Acham Enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai Achamillai Acham Enbathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaye Uchi Meedhu Vaan Idinthu Veezhukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaye Uchi Meedhu Vaan Idinthu Veezhukindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhilum Thunchamagi Enni Nammai Thooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhilum Thunchamagi Enni Nammai Thooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seidha Podhilum Achamillai Achamillai Acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidha Podhilum Achamillai Achamillai Acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enbathu Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbathu Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Game U Da Game U Da Thirudan Police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game U Da Game U Da Thirudan Police"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Game U Da Paaruda Paaruda Jeipathu Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game U Da Paaruda Paaruda Jeipathu Ingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaruda Kaatraaga Puyalaaga Varuvaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaruda Kaatraaga Puyalaaga Varuvaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police Haa Idiyaaga Mazhaiyaaga Therippaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Haa Idiyaaga Mazhaiyaaga Therippaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ongi Adichaa Ondra Ton Nu Weightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi Adichaa Ondra Ton Nu Weightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paanji Adichaa Pathu Ton Nu Weightu Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanji Adichaa Pathu Ton Nu Weightu Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatta Thoonga Mattadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatta Thoonga Mattadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Singam He is Durai Singam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Singam He is Durai Singam Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthal Podhum Pagaiyum Odhungum Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthal Podhum Pagaiyum Odhungum Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam He Is Durai Singam Ivan Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam He Is Durai Singam Ivan Vandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Vamurai Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Vamurai Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki Satta Potuu Vegamaaga Vandhaaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Satta Potuu Vegamaaga Vandhaaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakka Vandha Kootam Thadathadakkum Oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakka Vandha Kootam Thadathadakkum Oora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Paarthal Dhooramaaga Nirkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Paarthal Dhooramaaga Nirkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutravaali Nenjam Padapadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutravaali Nenjam Padapadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelivaaga Thimiriraaga Thirivaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelivaaga Thimiriraaga Thirivaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police Thimiraaga Thirinjaale Udhaipaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Thimiraaga Thirinjaale Udhaipaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police Hey Etti Midhicha Norungi Pogum Dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Hey Etti Midhicha Norungi Pogum Dhegam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti Adhicha Kola Nadungi Pogum Athirathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Adhicha Kola Nadungi Pogum Athirathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyanaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyanaru Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Singam He is Durai Singam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Singam He is Durai Singam Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirithaal Kooda Bayamum Thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithaal Kooda Bayamum Thodangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillai Achamillai Acham Enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai Achamillai Acham Enbathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaye Uchi Meedhu Vaan Idinthu Veezhukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaye Uchi Meedhu Vaan Idinthu Veezhukindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhilum Thunchamagi Enni Nammai Thooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhilum Thunchamagi Enni Nammai Thooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seidha Podhilum Achamillai Achamillai Acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidha Podhilum Achamillai Achamillai Acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enbathu Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbathu Illaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaval Kaakum Velai Kanakku Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval Kaakum Velai Kanakku Podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulai Kaathirunthu Mella Kaai Nagarthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulai Kaathirunthu Mella Kaai Nagarthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhivaasi Pola Paadhivaasi Vaazhkai Aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhivaasi Pola Paadhivaasi Vaazhkai Aana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Nenjam Adhai Virumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Nenjam Adhai Virumbum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaamal Oliyaamal Uzhaipaan Da Police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaamal Oliyaamal Uzhaipaan Da Police"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravenna Pagalenna Urangaathu Police Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravenna Pagalenna Urangaathu Police Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasai Iraithu Velai Kaatum Aalai Oosai Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai Iraithu Velai Kaatum Aalai Oosai Illaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olithu Kattum Velai Needhi Kaakum Kaakikaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olithu Kattum Velai Needhi Kaakum Kaakikaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Singam He Is Durai Singam Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Singam He Is Durai Singam Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnal Kuda Padaiyum Pathungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnal Kuda Padaiyum Pathungum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillai Achamillai Acham Enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai Achamillai Acham Enbathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaye Uchi Meedhu Vaan Idinthu Veezhukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaye Uchi Meedhu Vaan Idinthu Veezhukindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhilum Thunchamagi Enni Nammai Thooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhilum Thunchamagi Enni Nammai Thooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seidha Podhilum Achamillai Achamillai Acham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidha Podhilum Achamillai Achamillai Acham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enbathu Illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbathu Illaye"/>
</div>
</pre>
