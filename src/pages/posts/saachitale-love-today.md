---
title: "Saachitale song lyrics"
album: "Love Today"
artist: "Yuvan Shankar Raja"
lyricist: "Pradeep Ranganathan"
director: "Pradeep Ranganathan"
path: "/albums/love-today-lyrics"
song: "Saachitale"
image: ../../images/albumart/love-today.jpg
date: 2022-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gM2__pEU0jw?controls=0"
type: "Love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tu Tu Tu Tu Tu</div>
<div class="lyrico-lyrics-wrapper">Tu Tu Tu Tu Tu…</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oore Awala Patha</div>
<div class="lyrico-lyrics-wrapper">Ava Enna Mattum Patha</div>
<div class="lyrico-lyrics-wrapper">Yaar Sonnalum Namba Mata</div>
<div class="lyrico-lyrics-wrapper">Ava Dhanda Number Keta</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasanga Scene Eh Pota</div>
<div class="lyrico-lyrics-wrapper">Ava Kandukave Mata</div>
<div class="lyrico-lyrics-wrapper">En Kooda Pecha Pota</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Avalo Smart Ah</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cute Aana Smile Eh</div>
<div class="lyrico-lyrics-wrapper">Potu Saachitale</div>
<div class="lyrico-lyrics-wrapper">Kan Jada Kaati</div>
<div class="lyrico-lyrics-wrapper">Enna Kouthitale</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Ellam Ozhunga Ninne</div>
<div class="lyrico-lyrics-wrapper">Ennadi Enna Ponna</div>
<div class="lyrico-lyrics-wrapper">Nidhanam Ippo Illa</div>
<div class="lyrico-lyrics-wrapper">Kaalum Tharayil Nikkavilla</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saachitale Aala Saachitale</div>
<div class="lyrico-lyrics-wrapper">Maathitale Vazhka Maathitale</div>
<div class="lyrico-lyrics-wrapper">Saachitale Aala Saachitale</div>
<div class="lyrico-lyrics-wrapper">Maathitale Vazhka Maathitale</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Saachitale</div>
<div class="lyrico-lyrics-wrapper">Vazhka Maathitale</div>
<div class="lyrico-lyrics-wrapper">Aala Saachitale</div>
<div class="lyrico-lyrics-wrapper">Vazhka Maathitale</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu Tu Tu Tu Tu</div>
<div class="lyrico-lyrics-wrapper">Tu Tu Tu Tu Tu…</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verupana Neram Ellam</div>
<div class="lyrico-lyrics-wrapper">Vaazhka Venam Thonum</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Kooda</div>
<div class="lyrico-lyrics-wrapper">Irundhale Ellame Maarum</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nejamana Kiss Eh Venam</div>
<div class="lyrico-lyrics-wrapper">Kiss’u Smiley Podhum</div>
<div class="lyrico-lyrics-wrapper">Adha Pathu Pathu</div>
<div class="lyrico-lyrics-wrapper">Sirichipendi Ovvoru Naalum</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appapo Kovam Varum</div>
<div class="lyrico-lyrics-wrapper">Un Kooda Sanda Varum</div>
<div class="lyrico-lyrics-wrapper">Ichhunu Onnu Thandha</div>
<div class="lyrico-lyrics-wrapper">Ellame Maarum</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Unna Therinjikiten</div>
<div class="lyrico-lyrics-wrapper">Unna Fulla Purinjikiten</div>
<div class="lyrico-lyrics-wrapper">Nee Thappe Pannunalum</div>
<div class="lyrico-lyrics-wrapper">Adha Rasichikiten</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Una Pudichikiten</div>
<div class="lyrico-lyrics-wrapper">Tighta Pudichikiten</div>
<div class="lyrico-lyrics-wrapper">Inni Vidave Maaten</div>
<div class="lyrico-lyrics-wrapper">Unna Vidave Maaten</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Una Pudichikiten</div>
<div class="lyrico-lyrics-wrapper">Tighta Pudichikiten</div>
<div class="lyrico-lyrics-wrapper">Inni Vidave Maaten</div>
<div class="lyrico-lyrics-wrapper">Unna Vidave Maaten</div>
<div class="lyrico-lyrics-wrapper">Tan Tan Tan Tan</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oore Awala Patha</div>
<div class="lyrico-lyrics-wrapper">Ava Enna Mattum Patha</div>
<div class="lyrico-lyrics-wrapper">Yaar Sonnalum Namba Mata</div>
<div class="lyrico-lyrics-wrapper">Ava Dhanda Number Keta</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasanga Scene Eh Pota</div>
<div class="lyrico-lyrics-wrapper">Ava Kandukave Mata</div>
<div class="lyrico-lyrics-wrapper">En Kooda Pecha Pota</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Avalo Smart Ah</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu Tu Tu Tu Tu</div>
<div class="lyrico-lyrics-wrapper">Tu Tu Tu Tu Tu…</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Saachitale</div>
<div class="lyrico-lyrics-wrapper">Vazhka Maathitale</div>
<div class="lyrico-lyrics-wrapper">Aala Saachitale</div>
<div class="lyrico-lyrics-wrapper">Vazhka Maathitale</div>
</pre>
