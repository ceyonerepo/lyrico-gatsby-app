---
title: "yendi unna pidikudhu song lyrics"
album: "Maanagaram"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Lokesh Kanagaraj"
path: "/albums/maanagaram-lyrics"
song: "Yendi Unna Pidikudhu"
image: ../../images/albumart/maanagaram.jpg
date: 2017-03-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1WmzFpGd1dc"
type: "love"
singers:
  -	Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yendi unna pidikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi unna pidikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho onnu izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho onnu izhukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ora parvaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora parvaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkum vaarthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkum vaarthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veraiyaatheriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veraiyaatheriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theendaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemoottum sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemoottum sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendam enna vaattaatha azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendam enna vaattaatha azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkama kettaayae manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkama kettaayae manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyum paarvaiyum orasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyum paarvaiyum orasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaatha kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaatha kollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaatha vayasu vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha vayasu vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Killaatha killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaatha killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho jaadaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho jaadaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanen megam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen megam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen mela mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen mela mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanil neendhurenae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil neendhurenae "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaatha kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaatha kollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaatha vayasu vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha vayasu vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Killaatha killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaatha killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho jaadaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho jaadaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi unna pidikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi unna pidikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho onnu izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho onnu izhukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ora parvaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora parvaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkum vaarthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkum vaarthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veraiyaatheriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veraiyaatheriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haann manasu mandraaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haann manasu mandraaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethukku thindaaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethukku thindaaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukku ennaanathu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukku ennaanathu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillunu sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillunu sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna pannaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna pannaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi pogum avala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi pogum avala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavippu nenjodu undaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavippu nenjodu undaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaaru thundaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaaru thundaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhoshama odanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhoshama odanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Engae naan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae naan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Angeyum veredhum paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angeyum veredhum paakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevadhaiya rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevadhaiya rasippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madras-u climate-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madras-u climate-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Melbourne-u polaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melbourne-u polaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoosu kooda thooralaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosu kooda thooralaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam kooda thooramaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam kooda thooramaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal piththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal piththu "/>
</div>
<div class="lyrico-lyrics-wrapper">muththi pochchudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththi pochchudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi unna pidikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi unna pidikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho onnu izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho onnu izhukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ora parvaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora parvaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkum vaarthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkum vaarthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Veraiyaatheriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veraiyaatheriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theendaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemoottum sirippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemoottum sirippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendam enna vaattaatha azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendam enna vaattaatha azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkama kettaayae manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkama kettaayae manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyum paarvaiyum orasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyum paarvaiyum orasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollaatha kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaatha kollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaatha vayasu vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha vayasu vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Killaatha killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaatha killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetho jaadaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho jaadaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen megam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen megam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen mela mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen mela mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanil neendhurenae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil neendhurenae "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalaala"/>
</div>
</pre>
