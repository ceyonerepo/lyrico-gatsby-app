---
title: "maman kodukum seeru song lyrics"
album: "Marutha"
artist: "Isaignani Ilaiyaraaja"
lyricist: "GRS"
director: "GRS"
path: "/albums/marutha-lyrics"
song: "Maman Kodukum Seeru"
image: ../../images/albumart/marutha.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zsuLb4_l9Zg"
type: "happy"
singers:
  - SP Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thaaimaman seervarisai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaimaman seervarisai "/>
</div>
<div class="lyrico-lyrics-wrapper">vagaivagaiya varuthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vagaivagaiya varuthu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaasu melam pottu padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaasu melam pottu padai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirandu varuthu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirandu varuthu paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paasamulla thangamayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasamulla thangamayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">veeramulla singamayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeramulla singamayya"/>
</div>
<div class="lyrico-lyrics-wrapper">maamanathan varavethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maamanathan varavethu"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram kaikal vanangumayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram kaikal vanangumayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaman kodukkum seeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman kodukkum seeru"/>
</div>
<div class="lyrico-lyrics-wrapper">marutha varai pesum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marutha varai pesum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">seimuraiya paatha sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seimuraiya paatha sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayadaichu pogum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayadaichu pogum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattasu vedi vedichu koluthungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattasu vedi vedichu koluthungada"/>
</div>
<div class="lyrico-lyrics-wrapper">pangaali maaman machan serungada doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pangaali maaman machan serungada doi"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthattam kummi kotti aadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthattam kummi kotti aadungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaattam podum neram koodungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaattam podum neram koodungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kurinji poovula maalaiya kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurinji poovula maalaiya kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulavai pottu melatha kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulavai pottu melatha kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman manasu kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman manasu kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">kulira maalaiya podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulira maalaiya podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaman kodukkum seeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman kodukkum seeru"/>
</div>
<div class="lyrico-lyrics-wrapper">marutha varai pesum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marutha varai pesum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">seimuraiya paatha sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seimuraiya paatha sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayadaichu pogum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayadaichu pogum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">machan malaiku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan malaiku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">ponakooda thunaikku venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponakooda thunaikku venum"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla sonthatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla sonthatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu irunthaa enna venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu irunthaa enna venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">machan thuttu kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan thuttu kodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">vatti kidaikkum yaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vatti kidaikkum yaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kaiya koduthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kaiya koduthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikai epavum kodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikai epavum kodavarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naanga vaazhum manne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga vaazhum manne"/>
</div>
<div class="lyrico-lyrics-wrapper">enga koyil thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga koyil thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">athil kula saamiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil kula saamiye"/>
</div>
<div class="lyrico-lyrics-wrapper">enga maaman thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga maaman thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhinam kootanchore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam kootanchore"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vazhakkam thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vazhakkam thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla pasiyodu rusipaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla pasiyodu rusipaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">arumai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arumai thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naanga kovamulla sananga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga kovamulla sananga"/>
</div>
<div class="lyrico-lyrics-wrapper">paasamulla pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasamulla pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sirichu vanthaa servom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sirichu vanthaa servom"/>
</div>
<div class="lyrico-lyrics-wrapper">nee morachu vanthaa veruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee morachu vanthaa veruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">ada irukkum varai neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada irukkum varai neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">onna irupom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna irupom vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaman kodukkum seeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman kodukkum seeru"/>
</div>
<div class="lyrico-lyrics-wrapper">marutha varai pesum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marutha varai pesum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">seimuraiya paatha sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seimuraiya paatha sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayadaichu pogum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayadaichu pogum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga ezhu porappum nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga ezhu porappum nee "/>
</div>
<div class="lyrico-lyrics-wrapper">thane enga kooda varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane enga kooda varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla karuthu vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla karuthu vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathiruka vakka tharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathiruka vakka tharanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inga seimurai than saathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga seimurai than saathi"/>
</div>
<div class="lyrico-lyrics-wrapper">sanathuku perusu machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanathuku perusu machan"/>
</div>
<div class="lyrico-lyrics-wrapper">athai thavaraama senjavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai thavaraama senjavan"/>
</div>
<div class="lyrico-lyrics-wrapper">thaan usathi machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaan usathi machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inga elevatam kudhiyatam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga elevatam kudhiyatam poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ange therukootam vaayathoranthu pakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ange therukootam vaayathoranthu pakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">inge virundhuku karisoru veguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge virundhuku karisoru veguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">athai pathakka pasimeeri poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai pathakka pasimeeri poguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oravu sananga vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oravu sananga vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enga usura kooda tharuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga usura kooda tharuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">maana rosam kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maana rosam kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga meendum poranthu varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga meendum poranthu varuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ada irukkum varai neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada irukkum varai neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">onna irupom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna irupom vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaman kodukkum seeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman kodukkum seeru"/>
</div>
<div class="lyrico-lyrics-wrapper">marutha varai pesum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marutha varai pesum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">seimuraiya paatha sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seimuraiya paatha sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayadaichu pogum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayadaichu pogum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pattasu vedi vedichu koluthungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattasu vedi vedichu koluthungada"/>
</div>
<div class="lyrico-lyrics-wrapper">pangaali maaman machan serungada doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pangaali maaman machan serungada doi"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthatam kummi kotti aadungada hey yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthatam kummi kotti aadungada hey yei"/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam podum neram koodungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam podum neram koodungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kurinji poovula maalaiya kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurinji poovula maalaiya kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulavai pottu melatha kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulavai pottu melatha kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">maaman manasu kulira kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman manasu kulira kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">maalaiya podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalaiya podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maaman kodukkum seeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaman kodukkum seeru"/>
</div>
<div class="lyrico-lyrics-wrapper">marutha varai pesum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marutha varai pesum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">seimuraiya paatha sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seimuraiya paatha sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayadaichu pogum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayadaichu pogum paaru"/>
</div>
</pre>
