---
title: "mirchi mirchi mirchi song lyrics"
album: "Punya Paap"
artist: "Phenom"
lyricist: "DIVINE"
director: "Ruel Dausan Varindani - Joel D’souza (JD)"
path: "/albums/punya-paap-lyrics"
song: "Mirchi Mirchi Mirchi"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/w5Aioq5VYF0"
type: "happy"
singers:
  - DIVINE
  - MC Altaf
  - Phenom
  - Stylo G
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mirchi mirchi mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi mirchi mirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazrein tirchhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazrein tirchhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal hai firni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal hai firni"/>
</div>
<div class="lyrico-lyrics-wrapper">Harkatein filmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harkatein filmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl, do you feel me?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl, do you feel me?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera bhai bhi to hard hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera bhai bhi to hard hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Har gully mein star hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har gully mein star hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poora bawaal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poora bawaal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal pe scar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal pe scar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil mein sirf pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mein sirf pyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Call call call, zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call call call, zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Call pe call wo line pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call pe call wo line pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh my god
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh my god"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss garmi mein wohi to zimmedar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss garmi mein wohi to zimmedar"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh bengali gujrati mix hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh bengali gujrati mix hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rickshaw mein nikli par lena to risk hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rickshaw mein nikli par lena to risk hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teekhi, teekhi, teekhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teekhi, teekhi, teekhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamla hai risky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamla hai risky"/>
</div>
<div class="lyrico-lyrics-wrapper">Chand ka tukda hai chehra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chand ka tukda hai chehra"/>
</div>
<div class="lyrico-lyrics-wrapper">Par band rakhti pyaar ki khidki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par band rakhti pyaar ki khidki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh meri mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh meri mirchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ruko na miss I miss you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruko na miss I miss you"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss se, tumko missus banana hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss se, tumko missus banana hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum sirf baith ke dhyaan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum sirf baith ke dhyaan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko kitchen mein dishes banana hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko kitchen mein dishes banana hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadi mein bahut se fishes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadi mein bahut se fishes"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum fish nahi to fishing nahi jaana hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum fish nahi to fishing nahi jaana hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar ke mission pe aaya tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar ke mission pe aaya tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haar ya jeet yeh ladka deewana hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haar ya jeet yeh ladka deewana hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal na tu bombay, maa se milana hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal na tu bombay, maa se milana hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haath se khilana hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haath se khilana hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal na tu bombay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal na tu bombay"/>
</div>
<div class="lyrico-lyrics-wrapper">Khana bana ke phir chalu na apna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khana bana ke phir chalu na apna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirchi mirchi mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi mirchi mirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazrein tirchhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazrein tirchhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal hai firni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal hai firni"/>
</div>
<div class="lyrico-lyrics-wrapper">Harkatein filmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harkatein filmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl, do you feel me?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl, do you feel me?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera bhai bhi to hard hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera bhai bhi to hard hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Har gully mein star hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har gully mein star hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poora bawaal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poora bawaal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal pe scar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal pe scar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil mein sirf pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mein sirf pyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Call call call, zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call call call, zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karegi call ab puchegi bata ki karne ka kya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karegi call ab puchegi bata ki karne ka kya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saturday done ab sunday ka kya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saturday done ab sunday ka kya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Saucy mirchi nachti teekhе tareeke se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saucy mirchi nachti teekhе tareeke se"/>
</div>
<div class="lyrico-lyrics-wrapper">Reham na khaayеgi dj pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reham na khaayеgi dj pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar nazrein tirchhi peeche dekh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar nazrein tirchhi peeche dekh"/>
</div>
<div class="lyrico-lyrics-wrapper">Usko pasand hai gully gang music ka achha hai taste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usko pasand hai gully gang music ka achha hai taste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil to ye bacha hai dekh (mirchi)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil to ye bacha hai dekh (mirchi)"/>
</div>
<div class="lyrico-lyrics-wrapper">Isliye to fasta hai ye (mirchi)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isliye to fasta hai ye (mirchi)"/>
</div>
<div class="lyrico-lyrics-wrapper">Usko to farak nahi padta woh apne mein rehti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usko to farak nahi padta woh apne mein rehti"/>
</div>
<div class="lyrico-lyrics-wrapper">Par milne toh banta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par milne toh banta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirchi hai spicy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi hai spicy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanon mein bali hai icy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanon mein bali hai icy"/>
</div>
<div class="lyrico-lyrics-wrapper">South delhi di kudiyan pricy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="South delhi di kudiyan pricy"/>
</div>
<div class="lyrico-lyrics-wrapper">Mix karein fendi aur nike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mix karein fendi aur nike"/>
</div>
<div class="lyrico-lyrics-wrapper">She like me haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She like me haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bass pe jhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bass pe jhoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat ye meri tu bass pe ghoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat ye meri tu bass pe ghoom"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm calling her number she pick up my phone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm calling her number she pick up my phone"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ladke hai chhote is bade ko chun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ladke hai chhote is bade ko chun"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre baat to sun meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre baat to sun meri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirchi mirchi mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi mirchi mirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazrein tirchhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazrein tirchhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal hai firni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal hai firni"/>
</div>
<div class="lyrico-lyrics-wrapper">Harkatein filmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harkatein filmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl, do you feel me?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl, do you feel me?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera bhai bhi to hard hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera bhai bhi to hard hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Har gully mein star hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har gully mein star hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poora bawaal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poora bawaal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal pe scar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal pe scar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil me sirf pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil me sirf pyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Call call call, zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call call call, zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirchi mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi mirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">She got di fire, di chemistry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She got di fire, di chemistry"/>
</div>
<div class="lyrico-lyrics-wrapper">Hennessy hennessy hennessy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hennessy hennessy hennessy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pull up on her with the remedy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull up on her with the remedy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hu, energy energy energy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hu, energy energy energy"/>
</div>
<div class="lyrico-lyrics-wrapper">Batty c*** up mi a penny her
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batty c*** up mi a penny her"/>
</div>
<div class="lyrico-lyrics-wrapper">But she a wine up herself fi make many see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But she a wine up herself fi make many see"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tell her fi do it fi do boss, ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tell her fi do it fi do boss, ha"/>
</div>
<div class="lyrico-lyrics-wrapper">It no matter what it cost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It no matter what it cost"/>
</div>
<div class="lyrico-lyrics-wrapper">Put it pon mi make me trouble you, come ya, come ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Put it pon mi make me trouble you, come ya, come ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bend up like a double u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bend up like a double u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready mi deh yah fi challenge you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready mi deh yah fi challenge you"/>
</div>
<div class="lyrico-lyrics-wrapper">Anywhere you go mi go with you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anywhere you go mi go with you"/>
</div>
<div class="lyrico-lyrics-wrapper">Wine slow then fast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wine slow then fast"/>
</div>
<div class="lyrico-lyrics-wrapper">Tonight mi waa give you a pause
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tonight mi waa give you a pause"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal, you got mi weak mi love how you a do it,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal, you got mi weak mi love how you a do it,"/>
</div>
<div class="lyrico-lyrics-wrapper">A mi fi be with you at last
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A mi fi be with you at last"/>
</div>
<div class="lyrico-lyrics-wrapper">Bassline go so boom, it a bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bassline go so boom, it a bam"/>
</div>
<div class="lyrico-lyrics-wrapper">She back it up, then me jam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She back it up, then me jam"/>
</div>
<div class="lyrico-lyrics-wrapper">Every gal fi a wine pon a man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every gal fi a wine pon a man"/>
</div>
<div class="lyrico-lyrics-wrapper">Same way you do it fi di don
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Same way you do it fi di don"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirchi mirchi mirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirchi mirchi mirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazrein tirchhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazrein tirchhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal hai firni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal hai firni"/>
</div>
<div class="lyrico-lyrics-wrapper">Harkatein filmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harkatein filmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl, do you feel me?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl, do you feel me?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera bhai bhi to hard hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera bhai bhi to hard hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Har gully mein star hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har gully mein star hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poora bawaal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poora bawaal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakal pe scar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakal pe scar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil me sirf pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil me sirf pyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Call call call, zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call call call, zara"/>
</div>
</pre>
