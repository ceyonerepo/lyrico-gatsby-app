---
title: "very very sad song lyrics"
album: "Chal Mohan Ranga"
artist: "S S Thaman"
lyricist: "Balaji"
director: "Krishna Chaitanya"
path: "/albums/chal-mohan-ranga-lyrics"
song: "Very Very Sad"
image: ../../images/albumart/chal-mohan-ranga.jpg
date: 2018-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wjNSjyGw_W0"
type: "sad"
singers:
  -	Yazin Nizar
  - Sanjana Divaker Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rendu aksharale dacheyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu aksharale dacheyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa lakshanalle laksha avadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa lakshanalle laksha avadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edam edamga edampakannuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edam edamga edampakannuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundellni pindeyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellni pindeyadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very very sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The boy is sad the girl is sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The boy is sad the girl is sad"/>
</div>
<div class="lyrico-lyrics-wrapper">They are just very very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They are just very very very sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo lopale tega nacheyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lopale tega nacheyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa lopale taguvachheyaddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa lopale taguvachheyaddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prati kota eda ide pata katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prati kota eda ide pata katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli ila cheppadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli ila cheppadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very very sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The boy is sad the girl is sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The boy is sad the girl is sad"/>
</div>
<div class="lyrico-lyrics-wrapper">They are just very very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They are just very very very sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heart ane placelo hurt aye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart ane placelo hurt aye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hobby ee tamasha oo nasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hobby ee tamasha oo nasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq ane risklo muskurat undada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq ane risklo muskurat undada"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamesha ado nasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamesha ado nasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very very sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The boy is sad the girl is sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The boy is sad the girl is sad"/>
</div>
<div class="lyrico-lyrics-wrapper">They are just sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They are just sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidipoyentala mudipadaleduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidipoyentala mudipadaleduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasayentala mattalevuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasayentala mattalevuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakochentala oosullevuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakochentala oosullevuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakochentala adugullevuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakochentala adugullevuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very sad"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh very very
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh very very"/>
</div>
<div class="lyrico-lyrics-wrapper">Very very very sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very very very sad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The boy is sad the girl is sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The boy is sad the girl is sad"/>
</div>
<div class="lyrico-lyrics-wrapper">They are just sad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They are just sad"/>
</div>
</pre>
