---
title: "vambula thumbula song lyrics"
album: "Sarpatta Parambarai"
artist: "Santhosh Narayanan"
lyricist: "Kabilan - Madras Miran"
director: "Pa. Ranjith"
path: "/albums/sarpatta-parambarai-lyrics"
song: "Vambula Thumbula"
image: ../../images/albumart/sarpatta-parambarai.jpg
date: 2021-07-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Rj11FTX74pg"
type: "happy"
singers:
  - Santhosh Narayanan
  - Gana Muthu
  - Isaivani
  - Gana Ulagam Dharani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vanam Vidinjuruhu Kasuda Melaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanam Vidinjuruhu Kasuda Melaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sola Maranjiruchu Poduda Attaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sola Maranjiruchu Poduda Attaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varanda Thallikko Veranda Vachukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varanda Thallikko Veranda Vachukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabilan Varanda Thallikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabilan Varanda Thallikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Veranda Vachukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Veranda Vachukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththakkal Satta Pamparam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththakkal Satta Pamparam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Suthuna Narambu Anthurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Suthuna Narambu Anthurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unapol Illa Enthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unapol Illa Enthiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vetriyo Veera Thanthiram Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vetriyo Veera Thanthiram Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ala Pathu Vaya Sathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Pathu Vaya Sathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Attanthan Kattathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attanthan Kattathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Serthu Kaiya Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Serthu Kaiya Korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththattam Nee Poden Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththattam Nee Poden Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vambula Thumbula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vambula Thumbula "/>
</div>
<div class="lyrico-lyrics-wrapper">Vambula Thumbula Mattikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambula Thumbula Mattikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayila Veththala Vayila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayila Veththala Vayila "/>
</div>
<div class="lyrico-lyrics-wrapper">Vethala Pottukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethala Pottukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vambula Thumbula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vambula Thumbula "/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbula Vambula Mattikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbula Vambula Mattikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vayila Vethala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vayila Vethala "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayila Vethala Pottukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayila Vethala Pottukkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanam Vidinduchu Kasu Da Melatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanam Vidinduchu Kasu Da Melatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sola Maranjiruchu Podu Da Attatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sola Maranjiruchu Podu Da Attatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varan Da Thallikko Veranda Vechukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varan Da Thallikko Veranda Vechukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabilan Varan Da Thallikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabilan Varan Da Thallikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Veranda Vechukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Veranda Vechukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathadi Keezha Sunguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathadi Keezha Sunguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Kaiya Than Vacha Sanguda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Kaiya Than Vacha Sanguda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattasa Podu Ingada Ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasa Podu Ingada Ada "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaina Mama King-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaina Mama King-u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey King-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey King-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ala Pathu Vaya Sathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Pathu Vaya Sathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Attam Than Kattatha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam Than Kattatha Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Serthu Kaiya Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Serthu Kaiya Korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthattam Nee Podu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthattam Nee Podu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vambula Thumbula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vambula Thumbula "/>
</div>
<div class="lyrico-lyrics-wrapper">Vambula Thumbula Mattikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambula Thumbula Mattikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vayila Vethala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vayila Vethala "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayila Vethala Pottukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayila Vethala Pottukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vambula Thumbula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vambula Thumbula "/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbula Vambula Mattikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbula Vambula Mattikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vayila Vethala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vayila Vethala "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayila Vethala Pottukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayila Vethala Pottukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vanam Vidinduchu Kasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vanam Vidinduchu Kasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Da Melatha Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da Melatha Mamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai Kallu Mookkuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai Kallu Mookkuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja Thanni Arathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Thanni Arathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Ippa Mappulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Ippa Mappulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Puliyan Thoppula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Puliyan Thoppula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikka Vachu Ala Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikka Vachu Ala Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka Thakka Melam Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka Thakka Melam Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Rubai Malai Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Rubai Malai Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Vachu Kolam Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Vachu Kolam Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorai Pattu Minukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorai Pattu Minukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthu Vara Kalakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Vara Kalakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennayilla Karuthalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennayilla Karuthalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Morappa Verappa Nikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morappa Verappa Nikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpooram Paththa Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpooram Paththa Vaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosaniya Suththi Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosaniya Suththi Vaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Patta Edthu Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Patta Edthu Vaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellathayum Oththi Vaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellathayum Oththi Vaiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vambula Thumbula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vambula Thumbula "/>
</div>
<div class="lyrico-lyrics-wrapper">Vambula Thumbula Mattikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambula Thumbula Mattikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vayila Vethala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vayila Vethala "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayila Vethala Pottukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayila Vethala Pottukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vambula Thumbula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vambula Thumbula "/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbula Vambula Mattikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbula Vambula Mattikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vayila Vethala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vayila Vethala "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayila Vethala Pottukkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayila Vethala Pottukkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vanam Vidinduchu Kasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vanam Vidinduchu Kasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Da Melatha Mamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da Melatha Mamu"/>
</div>
</pre>
