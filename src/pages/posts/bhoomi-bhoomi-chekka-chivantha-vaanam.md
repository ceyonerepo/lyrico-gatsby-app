---
title: "bhoomi bhoomi song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Bhoomi Bhoomi"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ejX_gTSHYqA"
type: "happy"
singers:
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mudhal yaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal yaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivethuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivethuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivilla vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivilla vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivathumundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivathumundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathendro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathendro"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalai polae uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalai polae uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo azhivathumundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo azhivathumundo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalindru paandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalindru paandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthuvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Katharum manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katharum manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavaloru vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavaloru vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaigal uyirum pozhuthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaigal uyirum pozhuthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram azhivummillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram azhivummillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Agrinai polae andradum vaazhthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agrinai polae andradum vaazhthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae ae nizhaiyillaiye ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae ae nizhaiyillaiye ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh…bhoomi bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh…bhoomi bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhi aazhi kathum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi aazhi kathum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithan manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yutha satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yutha satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil engae ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil engae ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilin satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil engae ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil engae ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilin satham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalil meen onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil meen onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaikku seidhi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaikku seidhi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayamae thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayamae thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaanguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh…bhoomi bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh…bhoomi bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhi aazhi kathum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi aazhi kathum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithan manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yutha satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yutha satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil engae ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil engae ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilin satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil engae ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil engae ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilin satham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalil meen onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil meen onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaikku seidhi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaikku seidhi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavi nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavi nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththa vacha panjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththa vacha panjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjil saambal minjathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjil saambal minjathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvathai vidavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvathai vidavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyae kodithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyae kodithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhvathai vidavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhvathai vidavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivae kodithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivae kodithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvarai illa muthalum alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai illa muthalum alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivurai illa mudivum alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivurai illa mudivum alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer varudhu unmai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer varudhu unmai solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhum manadhu ketkudhumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhum manadhu ketkudhumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee engae nee engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee engae nee engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku naanum angae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku naanum angae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh…bhoomi bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh…bhoomi bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhi aazhi kathum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi aazhi kathum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithan manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yutha satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yutha satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil engae ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil engae ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilin satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil engae ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil engae ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilin satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilin satham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalil meen onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil meen onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaikku seidhi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaikku seidhi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalil meen onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil meen onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaikku seidhi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaikku seidhi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruma"/>
</div>
</pre>
