---
title: "vidai kodu song lyrics"
album: "Kannathil Muthamittal"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kannathil-muthamittal-song-lyrics"
song: "Vidai Kodu Engal Naade"
image: ../../images/albumart/kannathil-muthamittal.jpg
date: 2002-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HjNsz1yQ6Mo"
type: "sad"
singers:
  - M. S. Viswanathan
  - Balram
  - Febi Mani
  - A.R. Reihana
  - Manikka Vinayagam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vidai Kodu Engal Naadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Kodu Engal Naadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Vaasal Thellikkum Veedey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Vaasal Thellikkum Veedey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panai Mara Kaadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai Mara Kaadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravaigal Koodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Koodey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumurai Orumurai Paarpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Orumurai Paarpomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhatil Punnagai Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatil Punnagai Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Udambukul Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Udambukul Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Koodugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Koodugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorvalam Poogindrom Ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorvalam Poogindrom Ohooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Kodu Engal Naadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Kodu Engal Naadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Vaasal Thellikkum Veedey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Vaasal Thellikkum Veedey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panai Mara Kaadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai Mara Kaadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravaigal Koodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Koodey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumurai Orumurai Paarpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Orumurai Paarpomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhatil Punnagai Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatil Punnagai Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Udambukul Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Udambukul Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Koodugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Koodugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorvalam Poogindrom Ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorvalam Poogindrom Ohooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhal Aanalum Thaai Madipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhal Aanalum Thaai Madipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sugam Varumaa Varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sugam Varumaa Varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhal Aanalum Thaai Madipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhal Aanalum Thaai Madipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sugam Varumaa Varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sugam Varumaa Varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorgam Sendralum Sondha Oor Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Sendralum Sondha Oor Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhandiram Varumaa Varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhandiram Varumaa Varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Thirandha Desam Angey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thirandha Desam Angey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Moodum Desam Yengey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodum Desam Yengey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Thirandha Desam Angey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Thirandha Desam Angey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Moodum Desam Yengey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodum Desam Yengey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivom Nadhigaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivom Nadhigaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pizhithaal Varugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhithaal Varugirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Thaaygam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Thaaygam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaithaal Varugirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaithaal Varugirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Thiraiyil Pirandha Mannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Thiraiyil Pirandha Mannai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaisiyaga Paarkindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiyaga Paarkindrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Kodu Engal Naadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Kodu Engal Naadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Vaasal Thellikkum Veedey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Vaasal Thellikkum Veedey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panai Mara Kaadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai Mara Kaadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravaigal Koodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Koodey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumurai Orumurai Paarpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Orumurai Paarpomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhatil Punnagai Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatil Punnagai Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Udambukul Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Udambukul Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Koodugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Koodugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorvalam Poogindrom Ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorvalam Poogindrom Ohooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Sangeetham Pillaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Sangeetham Pillaiyin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhugaiyiley Tholaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhugaiyiley Tholaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal Ilamthingal Vedi Kundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Ilamthingal Vedi Kundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pugaiyiley Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugaiyiley Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munniravil Malaril Kidanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munniravil Malaril Kidanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinniravil Mullil Kizhinthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinniravil Mullil Kizhinthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Neer Paravaigaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Neer Paravaigaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irundhal Sandhipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhal Sandhipom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanamey Malaigaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanamey Malaigaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyil Koncham Nenjil Adhigam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyil Koncham Nenjil Adhigam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumaigal  Sumandhu Pogindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaigal  Sumandhu Pogindrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Kodu Engal Naadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Kodu Engal Naadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Vaasal Thellikkum Veedey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Vaasal Thellikkum Veedey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panai Mara Kaadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai Mara Kaadey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravaigal Koodey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Koodey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumurai Orumurai Paarpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Orumurai Paarpomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhatil Punnagai Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatil Punnagai Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirai Udambukul Pudhaithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Udambukul Pudhaithom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Koodugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Koodugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorvalam Poogindrom Ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorvalam Poogindrom Ohooo"/>
</div>
</pre>
