---
title: "sakalakala valli song lyrics"
album: "Kazhugu 2"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Rajan"
director: "Sathyasiva"
path: "/albums/kazhugu-2-lyrics"
song: "Sakalakala Valli"
image: ../../images/albumart/kazhugu-2.jpg
date: 2019-08-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N2IiNy9DtIk"
type: "happy"
singers:
  - Guru Ayya Durai
  - Suvi Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gaandha Kannazhagi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaandha Kannazhagi Da"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanja Meesai Nooru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanja Meesai Nooru Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandha Sondhakari Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandha Sondhakari Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Night-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Night-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Ponathilla Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Ponathilla Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pambara Nenjula Panjaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pambara Nenjula Panjaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavan Latcham Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavan Latcham Peru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sakkarai Sollula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sakkarai Sollula"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru Aanavan Micham Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru Aanavan Micham Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kattulu Mela Savara Kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kattulu Mela Savara Kaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Chakravarthiya Suppana Kaanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Chakravarthiya Suppana Kaanavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Vennilave Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vennilave Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Vettai Velli Pub-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Vettai Velli Pub-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakalakala Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakala Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaiyum Seiva Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaiyum Seiva Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakalakala Valli Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakala Valli Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkaram Thaama Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkaram Thaama Alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pub-la Suththi Suththi Takkarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pub-la Suththi Suththi Takkarana"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-u Irukkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-u Irukkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Anga Poiyu Aada Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Anga Poiyu Aada Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Enakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Enakkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhasu Bussunu Pesuvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhasu Bussunu Pesuvale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Seivada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Seivada"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Yes-u No-u Thavura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Yes-u No-u Thavura"/>
</div>
<div class="lyrico-lyrics-wrapper">Verai Enna Theriyum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verai Enna Theriyum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikondu Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikondu Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottininnu Oorasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottininnu Oorasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattupadu Illaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattupadu Illaiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Theriyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannalaye Darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalaye Darling"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Senjiduven Scanning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Senjiduven Scanning"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeri Kitta Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeri Kitta Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannuva Warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannuva Warning"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Enna Korachal Poo Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Enna Korachal Poo Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Kaakka Kulichi Eppo Kokkua Achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Kaakka Kulichi Eppo Kokkua Achu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakalakala Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakala Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaiyum Seiva Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaiyum Seiva Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakalakala Valli Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakala Valli Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkaram Thaama Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkaram Thaama Alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey English’u Padam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey English’u Padam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Nadakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Nadakkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Anga Poiyu Vanathavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Anga Poiyu Vanathavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Soldran Paaru Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soldran Paaru Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda Kanda Kanavellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda Kanda Kanavellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Vekkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Vekkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thappu Panna Gym Boys-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thappu Panna Gym Boys-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nongu Edupange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nongu Edupange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nee Sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka Maatenen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka Maatenen"/>
</div>
<div class="lyrico-lyrics-wrapper">Orakka Ulla Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orakka Ulla Poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapen Chellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapen Chellame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gethu Kaatta Car-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu Kaatta Car-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ulla Pova Shoe-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ulla Pova Shoe-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Kootti Kalicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Kootti Kalicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Zero Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Zero Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatti Vehnam Ithuve Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatti Vehnam Ithuve Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Velakka Anacha Iruttu Ellam Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Velakka Anacha Iruttu Ellam Onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakalakala Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakala Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaiyum Seiva Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaiyum Seiva Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakalakala Valli Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakala Valli Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkaram Thaama Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkaram Thaama Alli"/>
</div>
</pre>
