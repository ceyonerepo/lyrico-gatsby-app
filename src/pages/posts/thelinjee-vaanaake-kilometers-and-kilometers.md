---
title: "thelinjee vaanaake song lyrics"
album: "Kilometers and Kilometers"
artist: "Sooraj S. Kurup"
lyricist: "B. K. Harinarayanan - Lakshmi Menon"
director: "Jeo Baby"
path: "/albums/kilometers-and kilometers-lyrics"
song: "Thelinjee Vaanaake"
image: ../../images/albumart/kilometers-and-kilometers.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/RqXxZgcUU20"
type: "happy"
singers:
  - Sithara Krishnakumar
  - Sooraj S. Kurup
  - Aditi Nair R
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aao ji aao saiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aao saiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">dharthi tujhe bulati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharthi tujhe bulati hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aao ji aao saiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aao saiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">dharthi tujhe bulati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharthi tujhe bulati hai"/>
</div>
<div class="lyrico-lyrics-wrapper">rangon ki chadar bhi hai yaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangon ki chadar bhi hai yaha"/>
</div>
<div class="lyrico-lyrics-wrapper">khushiyon ki hariyali hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khushiyon ki hariyali hai"/>
</div>
<div class="lyrico-lyrics-wrapper">rangon ki chadar bhi hai yaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangon ki chadar bhi hai yaha"/>
</div>
<div class="lyrico-lyrics-wrapper">khushiyon ki hariyali hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khushiyon ki hariyali hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu naa samjhe saiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu naa samjhe saiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">har dil ko yeh bhar jaati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="har dil ko yeh bhar jaati hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu naa samjhe saiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu naa samjhe saiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">har dil ko yeh bhar jaati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="har dil ko yeh bhar jaati hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelinje Vaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinje Vaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranje Njaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranje Njaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinje Vaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinje Vaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranje Njaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranje Njaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Parannee Paathiraavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parannee Paathiraavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathilolam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathilolam Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hasde Thanne saiyaan ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne saiyaan ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasden Thanne des bulaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasden Thanne des bulaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne saiyaan ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne saiyaan ji"/>
</div>
<div class="lyrico-lyrics-wrapper">hasde hasde Aa AA
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hasde hasde Aa AA"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne saiyaan ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne saiyaan ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasden Thanne des bulaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasden Thanne des bulaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne saiyaan ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne saiyaan ji"/>
</div>
<div class="lyrico-lyrics-wrapper">hasde hasde Aa AA
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hasde hasde Aa AA"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelinje Vaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinje Vaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranje Njaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranje Njaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinje Vaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinje Vaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranje Njaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranje Njaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Parannee Paathiraavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parannee Paathiraavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathilolam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathilolam Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh boy the songs on your book
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh boy the songs on your book"/>
</div>
<div class="lyrico-lyrics-wrapper">All night bang it on the radio
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All night bang it on the radio"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh boy the songs on your book
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh boy the songs on your book"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh boy the songs on your book
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh boy the songs on your book"/>
</div>
<div class="lyrico-lyrics-wrapper">All night bang it on the radio
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All night bang it on the radio"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh boy the songs on your book
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh boy the songs on your book"/>
</div>
<div class="lyrico-lyrics-wrapper">Aao ji aaoji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aaoji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akame Kanal kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akame Kanal kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanavaai Uthirunnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanavaai Uthirunnuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Take the train
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take the train"/>
</div>
<div class="lyrico-lyrics-wrapper">Travel like a rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Travel like a rain"/>
</div>
<div class="lyrico-lyrics-wrapper">Akalamivide Maanju Pathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akalamivide Maanju Pathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Take the train
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take the train"/>
</div>
<div class="lyrico-lyrics-wrapper">Travel like a rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Travel like a rain"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhikaliloram Cheraan Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhikaliloram Cheraan Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aao ji aao saiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aao saiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">dharthi tujhe bulati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharthi tujhe bulati hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aao ji aao saiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aao ji aao saiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">dharthi tujhe bulati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharthi tujhe bulati hai"/>
</div>
<div class="lyrico-lyrics-wrapper">rangon ki chadar bhi hai yaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangon ki chadar bhi hai yaha"/>
</div>
<div class="lyrico-lyrics-wrapper">khushiyon ki hariyali hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khushiyon ki hariyali hai"/>
</div>
<div class="lyrico-lyrics-wrapper">rangon ki chadar bhi hai yaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangon ki chadar bhi hai yaha"/>
</div>
<div class="lyrico-lyrics-wrapper">khushiyon ki hariyali hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khushiyon ki hariyali hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu naa samjhe saiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu naa samjhe saiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">har dil ko yeh bhar jaati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="har dil ko yeh bhar jaati hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu naa samjhe saiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu naa samjhe saiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">har dil ko yeh bhar jaati hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="har dil ko yeh bhar jaati hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelinje Vaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinje Vaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranje Njaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranje Njaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinje Vaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinje Vaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Niranje Njaanaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niranje Njaanaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Parannee Paathiraavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parannee Paathiraavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathilolam Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathilolam Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Hasde Thanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasde Thanne"/>
</div>
</pre>
