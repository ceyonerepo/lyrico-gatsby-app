---
title: "cycle wheela pola lyrics"
album: "Dikkiloona"
artist: "Yuvan shankar raja"
lyricist: "Arunraja kamaraj"
director: "Karthik Yogi"
path: "/albums/dikkiloona-song-lyrics"
song: "Thamizhan Pattu"
image: ../../images/albumart/dikkiloona.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4-Y8hzSBMLY"
type: "sad"
singers:
  - Jithinraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ottha seruponnu pola ennakoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottha seruponnu pola ennakoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u kedaichiruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u kedaichiruche"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitthi kodumaiku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitthi kodumaiku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru wife-uh amainjuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru wife-uh amainjuruche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My future romba torture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My future romba torture"/>
</div>
<div class="lyrico-lyrics-wrapper">Total damage aagi fracture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total damage aagi fracture"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle wheel ah pola vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle wheel ah pola vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaa meela paaka keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaa meela paaka keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike il back il pogum luck ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike il back il pogum luck ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Walking poi eppa pudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walking poi eppa pudika"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena roshamum sogamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena roshamum sogamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tie up panniruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tie up panniruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona poguthu hair nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona poguthu hair nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vitturuchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vitturuchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal pechu happy scotchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal pechu happy scotchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marriage aachu ellam pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriage aachu ellam pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poattu vecha kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poattu vecha kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitam enna poattu thala vanthu nikkithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitam enna poattu thala vanthu nikkithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu vazhi saalai kooda inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vazhi saalai kooda inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu kizhi aagi mukkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu kizhi aagi mukkudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My future romba torture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My future romba torture"/>
</div>
<div class="lyrico-lyrics-wrapper">Total damage aagi fracture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total damage aagi fracture"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle wheel ah pola vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle wheel ah pola vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaa meela paaka keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaa meela paaka keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike il back il pogum luck ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike il back il pogum luck ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Walking poi eppa pudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walking poi eppa pudika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottha seruponnu pola ennakoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottha seruponnu pola ennakoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u kedaichiruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u kedaichiruche"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitthi kodumaiku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitthi kodumaiku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru wife-uh amainjuruche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru wife-uh amainjuruche"/>
</div>
<div class="lyrico-lyrics-wrapper">My future romba torture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My future romba torture"/>
</div>
<div class="lyrico-lyrics-wrapper">Total damage aagi fracture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total damage aagi fracture"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cycle wheel ah pola vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle wheel ah pola vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaa meela paaka keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaa meela paaka keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike il back il pogum luck ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike il back il pogum luck ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Walking poi eppa pudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walking poi eppa pudika"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena roshamum sogamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena roshamum sogamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tie up panniruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tie up panniruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona poguthu hair nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona poguthu hair nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vitturuchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vitturuchae"/>
</div>
</pre>
