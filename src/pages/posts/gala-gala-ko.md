---
title: "gala gala song lyrics"
album: "Ko"
artist: "Harris Jayaraj"
lyricist: "Kabilan"
director: "K.V. Anand"
path: "/albums/ko-lyrics"
song: "Gala Gala"
image: ../../images/albumart/ko.jpg
date: 2011-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/700-GQQGm_k"
type: "happy"
singers:
  - Tippu
  - Krish
  - Haricharan
  - Sayanora Philip
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gala Gala Gaala Gangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gaala Gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Bala Baila Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Bala Baila Songu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Oru Kanavil Thoongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Oru Kanavil Thoongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangaiyil Ulagai Vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyil Ulagai Vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koppalikkum Ootru Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppalikkum Ootru Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppaththukku Kaatru Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppaththukku Kaatru Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramukku Maatru Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramukku Maatru Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedan Illa Vedanthaangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedan Illa Vedanthaangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala Gaala Gangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gaala Gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Bala Baila Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Bala Baila Songu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Oru Kanavil Thoongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Oru Kanavil Thoongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangaiyil Ulagai Vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyil Ulagai Vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Oru Vaaliba Kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Oru Vaaliba Kottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthidu Nee Vantha Veettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthidu Nee Vantha Veettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enakku Naan Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakku Naan Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthirunthaal Naam Nammakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthirunthaal Naam Nammakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigalil Eerame illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalil Eerame illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil Baaramum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil Baaramum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pal Mulaiththa Minnalai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal Mulaiththa Minnalai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Muzhuthum Naam Sirippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Muzhuthum Naam Sirippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pondra Naatkal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pondra Naatkal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhiraatha Pookkal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhiraatha Pookkal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal Nilavum Kathirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal Nilavum Kathirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaindha Pozhuthaavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaindha Pozhuthaavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala Gaala Gangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gaala Gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Bala Baila Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Bala Baila Songu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Oru Kanavil Thoongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Oru Kanavil Thoongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangaiyil Ulagai Vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyil Ulagai Vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathu Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Vilaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanaththa Paaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththa Paaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Vidai Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Vidai Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathu Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Vilaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanaththa Paaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththa Paaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Vidai Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Vidai Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nathigalum Thenguvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathigalum Thenguvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Kadal Thoonguvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Kadal Thoonguvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Varai Vizhithirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Varai Vizhithirunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanavai Yaar Parippaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavai Yaar Parippaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athigamaai Aasaigal Kolvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigamaai Aasaigal Kolvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithigalai Vervaiyil Velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithigalai Vervaiyil Velvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrumaiyin Veraruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrumaiyin Veraruththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavillai Sernthiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillai Sernthiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondru Koodi Yosiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Koodi Yosiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Naame Nesiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Naame Nesiththom"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Vizhiyil Inimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Vizhiyil Inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Mugam Paarkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Mugam Paarkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala Gaala Gangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gaala Gangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Bala Baila Songu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Bala Baila Songu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Oru Kanavil Thoongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Oru Kanavil Thoongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangaiyil Ulagai Vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangaiyil Ulagai Vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koppalikkum Ootru Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koppalikkum Ootru Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppaththukku Kaatru Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppaththukku Kaatru Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramukku Maatru Naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramukku Maatru Naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedan Illa Vedanthaangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedan Illa Vedanthaangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Vanthu Cuts
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Vanthu Cuts"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Bala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Bala Bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Bala Bala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Drop You Say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drop You Say"/>
</div>
<div class="lyrico-lyrics-wrapper">Bala Bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bala Bala"/>
</div>
<div class="lyrico-lyrics-wrapper">Boombastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boombastha"/>
</div>
</pre>
