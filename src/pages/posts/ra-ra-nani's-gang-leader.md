---
title: "ra ra song lyrics"
album: "Nani's Gang Leader"
artist: "Anirudh Ravichander"
lyricist: "Anantha Sriram"
director: "Vikram Kumar"
path: "/albums/nani's-gang-leader-lyrics"
song: "Ra Ra"
image: ../../images/albumart/nanis-gang-leader.jpg
date: 2019-09-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cZzxKoDr3Mc"
type: "mass"
singers:
  - Prudhvi Chandra
  - BasherMax
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raa Raa Jagathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Jagathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayanchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayanchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Charithani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Charithani"/>
</div>
<div class="lyrico-lyrics-wrapper">Likinchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likinchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Bhavithani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Bhavithani"/>
</div>
<div class="lyrico-lyrics-wrapper">Savaalu Cheysey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaalu Cheysey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavaathu Cheddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavaathu Cheddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Theginchudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theginchudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Raa Nadumulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Nadumulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Biginchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biginchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Pidugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Pidugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharinchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharinchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Chedunika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Chedunika"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhahinchiveysey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhahinchiveysey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahasya Vyooham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahasya Vyooham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rachinchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rachinchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hehahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hehahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadhulu Gaduluga Gadapalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadhulu Gaduluga Gadapalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadalu Dhadulu Dharulanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadalu Dhadulu Dharulanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiregi Regiri Yegiri Regiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiregi Regiri Yegiri Regiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri Dhasa Disala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiri Dhasa Disala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosaku Podhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosaku Podhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeralu Moralu Cheralanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeralu Moralu Cheralanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharulu Girulu Jharulanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharulu Girulu Jharulanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiregi Regiri Yegiri Regiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiregi Regiri Yegiri Regiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri Tudi Gelupu Merupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiri Tudi Gelupu Merupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">We Bring The Game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Bring The Game"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah We’Ve Come Here
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah We’Ve Come Here"/>
</div>
<div class="lyrico-lyrics-wrapper">To Stay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Stay"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah We’Ve Come Here
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah We’Ve Come Here"/>
</div>
<div class="lyrico-lyrics-wrapper">To Play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Play"/>
</div>
<div class="lyrico-lyrics-wrapper">Repeat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repeat"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing With Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing With Me"/>
</div>
<div class="lyrico-lyrics-wrapper">We Bring The Game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Bring The Game"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah We’Ve Come Here
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah We’Ve Come Here"/>
</div>
<div class="lyrico-lyrics-wrapper">To Stay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Stay"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah We’Ve Come Here
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah We’Ve Come Here"/>
</div>
<div class="lyrico-lyrics-wrapper">To Play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Play"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariga Sariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariga Sariga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Sakthulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Sakthulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">O Chota Cheyrchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Chota Cheyrchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Twaragaa Twaragaa Mana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twaragaa Twaragaa Mana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tappulanni Saridhiddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappulanni Saridhiddhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chematey Chematey Chamuraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chematey Chematey Chamuraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaahanam Dhehame Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaahanam Dhehame Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sramakey Sramakey Thanu Korukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sramakey Sramakey Thanu Korukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyanni Choopudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyanni Choopudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarala Thalalu Thaakudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarala Thalalu Thaakudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Theeruni Thelupudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Theeruni Thelupudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarani Thapane Aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarani Thapane Aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Poruni Salupudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Poruni Salupudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadhulu Gaduluga Gadapalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadhulu Gaduluga Gadapalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadalu Dhadulu Dharulanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadalu Dhadulu Dharulanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiregi Regiri Yegiri Regiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiregi Regiri Yegiri Regiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri Dhasa Disala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiri Dhasa Disala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosaku Podhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosaku Podhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeralu Moralu Cheralanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeralu Moralu Cheralanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharulu Girulu Jharulanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharulu Girulu Jharulanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaatey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaatey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiregi Regiri Yegiri Regiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiregi Regiri Yegiri Regiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegiri Tudi Gelupu Merupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegiri Tudi Gelupu Merupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Raa Jagathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Jagathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayanchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayanchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Charithani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Charithani"/>
</div>
<div class="lyrico-lyrics-wrapper">Likinchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likinchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Bhavithani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Bhavithani"/>
</div>
<div class="lyrico-lyrics-wrapper">Savaalu Cheysey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savaalu Cheysey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavaathu Cheddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavaathu Cheddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Theginchudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theginchudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Raa Nadumulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Nadumulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Biginchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biginchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Pidugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Pidugulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharinchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharinchudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa Chedunika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Chedunika"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhahinchiveysey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhahinchiveysey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahasya Vyooham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahasya Vyooham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rachinchudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rachinchudhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hehahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hehahaha"/>
</div>
</pre>
