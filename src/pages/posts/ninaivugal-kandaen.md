---
title: "ninaivugal song lyrics"
album: "Kandaen"
artist: "Vijay Ebenezer"
lyricist: "Ravindran"
director: "A.C. Mugil"
path: "/albums/kandaen-lyrics"
song: "Ninaivugal"
image: ../../images/albumart/kandaen.jpg
date: 2011-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zVUIBSe92PA"
type: "love"
singers:
  - Devan Ekambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vaazhthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vaazhthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Veezhthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Veezhthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaakki Sudugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaakki Sudugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Endrendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Endrendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavazhgiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavazhgiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vaazhthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vaazhthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Veezhthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Veezhthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaakki Sudugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaakki Sudugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Endrendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Endrendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavazhgiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavazhgiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Punnagaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Punnagaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Sangamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Sangamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Eppadi Aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Eppadi Aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kann Vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kann Vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kasindhu Poavadhenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kasindhu Poavadhenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endhan Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Endha Moolaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Endha Moolaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedi Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedi Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Kasangiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kasangiyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vaazhthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vaazhthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Veezhthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Veezhthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaakki Sudugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaakki Sudugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivugal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivugal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Endrendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Endrendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavazhgiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavazhgiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Endrendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Endrendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavazhgiradhe Hmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavazhgiradhe Hmm Mmm"/>
</div>
</pre>
