---
title: "jwalamukhi song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Madhan Karky"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Jwalamukhi"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Mp8w_RLqokw"
type: "Love"
singers:
  - Poorvi Koutish
  - Sarthak Kalyani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnai vendumendru kettena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vendumendru kettena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendam podhumendru sonnena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendam podhumendru sonnena"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaviyam neeyum theetida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaviyam neeyum theetida"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan vazhkaiye vilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan vazhkaiye vilaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jwalamukhi nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwalamukhi nenjile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jwalamukhi kannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwalamukhi kannile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jwalamukhi ennile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwalamukhi ennile"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yetrinai nee kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yetrinai nee kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan illai, idhu naan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan illai, idhu naan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan illamal adhu vaan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan illamal adhu vaan illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru poi solla en kaatrukkum yen naavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru poi solla en kaatrukkum yen naavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame maarum yennam unmai yen illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame maarum yennam unmai yen illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kanneeril nee theervaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanneeril nee theervaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un senthiyil naan theeveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un senthiyil naan theeveno"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpom vaa kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpom vaa kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jwalamukhi nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwalamukhi nenjile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jwalamukhi kannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwalamukhi kannile"/>
</div>
<div class="lyrico-lyrics-wrapper">Jwalamukhi ennile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwalamukhi ennile"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen yetrinai nee kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen yetrinai nee kadhale"/>
</div>
</pre>
