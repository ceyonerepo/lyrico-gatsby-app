---
title: "maruthamalli song lyrics"
album: "Marutha"
artist: "Isaignani Ilaiyaraaja"
lyricist: "ARP Jayaraman"
director: "GRS"
path: "/albums/marutha-lyrics"
song: "Maruthamalli"
image: ../../images/albumart/marutha.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7zw9fPkJYgY"
type: "love"
singers:
  - Jithin
  - Vibhava Hari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">maruthamalli soodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthamalli soodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vita amuthavalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vita amuthavalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">maruthamalli soodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthamalli soodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vita amuthavalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vita amuthavalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">urava solli ottikita 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava solli ottikita "/>
</div>
<div class="lyrico-lyrics-wrapper">paasa mullaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasa mullaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">thooli katti vidava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooli katti vidava "/>
</div>
<div class="lyrico-lyrics-wrapper">pethukidava thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethukidava thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">katta varava thaaramagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta varava thaaramagava"/>
</div>
<div class="lyrico-lyrics-wrapper">neril varum saami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neril varum saami "/>
</div>
<div class="lyrico-lyrics-wrapper">rendu maalai kodukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu maalai kodukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maruthamalli soodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthamalli soodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vita amuthavalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vita amuthavalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">urava solli ottikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava solli ottikita"/>
</div>
<div class="lyrico-lyrics-wrapper">paasa mullaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasa mullaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuthalathu aruvi thulli thulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthalathu aruvi thulli thulli"/>
</div>
<div class="lyrico-lyrics-wrapper">mathalam kotuthu nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathalam kotuthu nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">thithikum then ellam pattam poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thithikum then ellam pattam poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">mothama oothuthu osurukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothama oothuthu osurukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">oora chuthum unna suthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora chuthum unna suthum"/>
</div>
<div class="lyrico-lyrics-wrapper">mallu kattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallu kattum "/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum ulla vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum ulla vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">ipa vanthu koturaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipa vanthu koturaye"/>
</div>
<div class="lyrico-lyrics-wrapper">panthana than mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panthana than mela"/>
</div>
<div class="lyrico-lyrics-wrapper">vethala kodi naan kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethala kodi naan kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">venam nu theriyalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam nu theriyalaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maruthamalli soodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthamalli soodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vita amuthavalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vita amuthavalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">urava solli ottikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava solli ottikita"/>
</div>
<div class="lyrico-lyrics-wrapper">paasa mullaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasa mullaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">santhana kinnamai thotukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhana kinnamai thotukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">kunguma vannama vetkapatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunguma vannama vetkapatta"/>
</div>
<div class="lyrico-lyrics-wrapper">munnala irunthen kalli poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala irunthen kalli poova"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala marinen kanni poova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala marinen kanni poova"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathaduthe karisal kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathaduthe karisal kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">poothaduthe poovu rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothaduthe poovu rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">kai korkava kalalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai korkava kalalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kan kaatuthe rosa sendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan kaatuthe rosa sendu"/>
</div>
<div class="lyrico-lyrics-wrapper">sinanjiru paruvam seendi viduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinanjiru paruvam seendi viduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">etho etho panna solli thundi viduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etho etho panna solli thundi viduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maruthamalli soodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthamalli soodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vita amuthavalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vita amuthavalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">urava solli ottikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava solli ottikita"/>
</div>
<div class="lyrico-lyrics-wrapper">paasa mullaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasa mullaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">thooli katti vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooli katti vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">pethukidava thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethukidava thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">katta varava thaaramagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta varava thaaramagava"/>
</div>
<div class="lyrico-lyrics-wrapper">neril varum saami 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neril varum saami "/>
</div>
<div class="lyrico-lyrics-wrapper">rendu maalai kodukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu maalai kodukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maruthamalli soodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthamalli soodi "/>
</div>
<div class="lyrico-lyrics-wrapper">vita amuthavalliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vita amuthavalliye"/>
</div>
<div class="lyrico-lyrics-wrapper">urava solli ottikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava solli ottikita"/>
</div>
<div class="lyrico-lyrics-wrapper">paasa mullaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasa mullaiye"/>
</div>
</pre>
