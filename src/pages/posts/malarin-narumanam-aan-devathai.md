---
title: "malarin narumanam song lyrics"
album: "Aan Devathai"
artist: "Ghibran"
lyricist: "Kaviko"
director: "Thamira"
path: "/albums/aan-devathai-lyrics"
song: "Malarin Narumanam"
image: ../../images/albumart/aan-devathai.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7lxA4_6jB7w"
type: "happy"
singers:
  - Yazin Nizar
  - S Riyaz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Malarin narumanam pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin narumanam pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalin paadalgal pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalin paadalgal pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaintha sudargal pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaintha sudargal pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaan naamum pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaan naamum pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarin narumanam pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin narumanam pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalin paadalgal pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalin paadalgal pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaintha sudargal pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaintha sudargal pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaan naamum pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaan naamum pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madha koyil jeba ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha koyil jeba ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli vaasal azhaipozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli vaasal azhaipozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindu aalaya mani ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindu aalaya mani ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam ondraai pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam ondraai pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madha koyil jeba ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha koyil jeba ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli vaasal azhaipozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli vaasal azhaipozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindu aalaya mani ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindu aalaya mani ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam ondraai pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam ondraai pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha idam nam sontha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha idam nam sontha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithu porulum vantha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithu porulum vantha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae madhangal yedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae madhangal yedhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaidhikkendrum setham illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaidhikkendrum setham illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha idam nam sontha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha idam nam sontha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithu porulum vantha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithu porulum vantha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae madhangal yedhum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae madhangal yedhum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaidhikkendrum setham illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaidhikkendrum setham illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhuvum vandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuvum vandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer punnagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer punnagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvum ithuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvum ithuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithum ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithum ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarin narumanam pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin narumanam pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhalin paadalgal pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhalin paadalgal pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaintha sudargal pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaintha sudargal pogum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaan naamum pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaan naamum pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum idam"/>
</div>
</pre>
