---
title: "aagaaya neelangalil song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Thamarai"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Aagaaya Neelangalil"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8W3_gPYci_4"
type: "Love"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagaaya neelangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya neelangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankal vaangi vanthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankal vaangi vanthaayi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thantha aazhangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thantha aazhangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandedutha mutthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandedutha mutthaayi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya neelangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya neelangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankal vaangi vanthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankal vaangi vanthaayi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thantha aazhangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thantha aazhangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandedutha mutthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandedutha mutthaayi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarvarnnanoothum poonkuzhal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarvarnnanoothum poonkuzhal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhavoodalaisalaamin gaanamum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhavoodalaisalaamin gaanamum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabeerin devaraagam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabeerin devaraagam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyendraale nee than kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyendraale nee than kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaanen thembaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanen thembaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru naalai netrum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru naalai netrum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilinnum kaanaamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilinnum kaanaamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam koytha kalvan nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam koytha kalvan nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage aaruyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage aaruyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvin kaarirule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvin kaarirule"/>
</div>
<div class="lyrico-lyrics-wrapper">Munpe kandaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munpe kandaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini neethaan oli deepame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini neethaan oli deepame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar polum neeyillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar polum neeyillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai ondre yen saayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai ondre yen saayale"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaipaartthum anchaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaipaartthum anchaathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethai kettum kenchaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethai kettum kenchaathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya neelangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya neelangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankal vaangi vanthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankal vaangi vanthaayi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thantha aazhangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thantha aazhangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandedutha mutthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandedutha mutthaayi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaaya neelangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaaya neelangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankal vaangi vanthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankal vaangi vanthaayi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thantha aazhangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thantha aazhangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandedutha mutthaayi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandedutha mutthaayi nee"/>
</div>
</pre>
