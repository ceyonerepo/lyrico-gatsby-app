---
title: "iruchi song lyrics"
album: "Irandam Ulagaporin Kadaisi Gundu"
artist: "Tenma"
lyricist: "Umadevi"
director: "Athiyan Athirai"
path: "/albums/irandam-ulagaporin-kadaisi-gundu-lyrics"
song: "Iruchi"
image: ../../images/albumart/irandam-ulagaporin-kadaisi-gundu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JOHoliSPPAQ"
type: "happy"
singers:
  - Senthil Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kondaa Iruchi Yammaaaaaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaa Iruchi Yammaaaaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedham Konda Sandai Naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham Konda Sandai Naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulathula Mel Adukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulathula Mel Adukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulathula Mel Adukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulathula Mel Adukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunathula Nee Midukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunathula Nee Midukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Gunathula Nee Midukkuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Gunathula Nee Midukkuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethana Vendinaalum Ethana Vendinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Vendinaalum Ethana Vendinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athnaikkum Saami Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athnaikkum Saami Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi Pithukala Theerpatharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Pithukala Theerpatharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi Pithukala Theerpatharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Pithukala Theerpatharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jenmangondu Vaadi Yammayei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jenmangondu Vaadi Yammayei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Naagai Manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Naagai Manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagai Konda Ambigaiyae Mariyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Konda Ambigaiyae Mariyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Madhaviyin Koodu Vaazhntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Madhaviyin Koodu Vaazhntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manimegalaiyae Pidariyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manimegalaiyae Pidariyaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Seliyamma Oonniyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Seliyamma Oonniyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyamala Pappathiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyamala Pappathiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepanchamma Murugesan Kannagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepanchamma Murugesan Kannagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavarasan Nayagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavarasan Nayagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandai Naadu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai Naadu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Thondai Naadu Vanthavalaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Thondai Naadu Vanthavalaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaikkum Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaikkum Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedha Baethamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedha Baethamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Posungidummaa Thaayae Posungidummaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posungidummaa Thaayae Posungidummaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Nee Vaikkum Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Nee Vaikkum Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Madha Bothayellam Thelinjidumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha Bothayellam Thelinjidumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelinjidumma Thelinjidumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinjidumma Thelinjidumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Vaikkum Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Vaikkum Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavangal Azhinjidum Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavangal Azhinjidum Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhinjidum Ma Azhinjidum Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhinjidum Ma Azhinjidum Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Vaikkum Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Vaikkum Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu Ellaam Santhanam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu Ellaam Santhanam Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayae Santhanam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayae Santhanam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Etti Vaikkum Kaalgalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Etti Vaikkum Kaalgalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Samathuvangal Poothidumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samathuvangal Poothidumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothidumma Poothidumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothidumma Poothidumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pethan Peru Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethan Peru Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethan Peru Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethan Peru Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumaikku Seeru Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumaikku Seeru Vachaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadala Nokki Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala Nokki Odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadala Nokki Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala Nokki Odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Aatha Katti Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Aatha Katti Vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udanpokku Kuthamunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanpokku Kuthamunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanpokku Kuthamunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanpokku Kuthamunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollilae Thean Thadavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollilae Thean Thadavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaama Visham Thadavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama Visham Thadavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum Gumbalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum Gumbalathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollum Gumbalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum Gumbalathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kola Nadunga Sitharadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola Nadunga Sitharadika"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusu Satham Vedi Vedikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusu Satham Vedi Vedikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Poo Vaasam Puyaladikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Poo Vaasam Puyaladikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottu Theeyattam Analadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Theeyattam Analadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theervaagaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theervaagaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theervaaga Vaadi Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theervaaga Vaadi Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Theervaaga Vaadi Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theervaaga Vaadi Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukku Theervaaga Vaadi Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukku Theervaaga Vaadi Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angae Idi Muzhangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Idi Muzhangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanathanam Ellaam Norunga Pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanathanam Ellaam Norunga Pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae Idi Muzhangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Idi Muzhangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanathanam Ellaam Norunga Pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanathanam Ellaam Norunga Pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae Nadu Nadunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae Nadu Nadunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruchi Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruchi Yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Perae Kidu Kidunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perae Kidu Kidunguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Selaiyaa Selai Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Selaiyaa Selai Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeru Petra Kalai Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeru Petra Kalai Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Selaiya Selai Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Selaiya Selai Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeru Petra Kalai Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeru Petra Kalai Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyae Adi Nazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyae Adi Nazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarnthu Varum Ival Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarnthu Varum Ival Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangul Vidinthu Kolluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangul Vidinthu Kolluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Kaatum Dhisaiyil Ulagam Selludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Kaatum Dhisaiyil Ulagam Selludhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangul Vidinthu Kolluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangul Vidinthu Kolluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Kaatum Dhisaiyil Ulagam Selludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Kaatum Dhisaiyil Ulagam Selludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorin Kadhavu Thirakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorin Kadhavu Thirakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Poril Penmai Jeyichu Nikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Poril Penmai Jeyichu Nikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorin Kadhavu Thirakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorin Kadhavu Thirakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Poril Penmai Jeyichu Nikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Poril Penmai Jeyichu Nikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angam Vananga Solludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angam Vananga Solludhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Nadaiyil Singam Nadanthu Selludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Nadaiyil Singam Nadanthu Selludhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angam Vananga Solludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angam Vananga Solludhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Nadaiyil Singam Nadanthu Selludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Nadaiyil Singam Nadanthu Selludhu"/>
</div>
</pre>
