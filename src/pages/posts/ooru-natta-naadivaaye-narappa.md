---
title: "ooru natta naadivaaye song lyrics"
album: "Narappa"
artist: "Mani Sharma"
lyricist: "Sirivennela Seetharama Sastry"
director: "Srikanth Addala"
path: "/albums/narappa-lyrics"
song: "Ooru Natta Naadivaaye"
image: ../../images/albumart/narappa.jpg
date: 2021-07-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HdWWSVEtAoI"
type: "sad"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oorunatta nadivaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorunatta nadivaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daari kanta padadhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daari kanta padadhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jaada cheppedhevaru naakinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jaada cheppedhevaru naakinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudagalano ledo neninka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudagalano ledo neninka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela chudu vetaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela chudu vetaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugu kuda itaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugu kuda itaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Opaleni baruvaipoye baanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opaleni baruvaipoye baanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapaleni parugaipoye paadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapaleni parugaipoye paadhalu"/>
</div>
</pre>
