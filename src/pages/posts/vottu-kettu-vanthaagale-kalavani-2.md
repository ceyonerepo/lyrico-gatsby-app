---
title: "vottu kettu vanthaagale song lyrics"
album: "Kalavani 2"
artist: "Mani Amudhavan"
lyricist: "T. Kottai Samy - Vivekha"
director: "A. Sarkunam"
path: "/albums/kalavani-2-lyrics"
song: "Vottu Kettu Vanthaagale"
image: ../../images/albumart/kalavani-2.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ef1E0Up9QPE"
type: "happy"
singers:
  - Mariyammal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Car-u Pottu Odi Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-u Pottu Odi Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyeduthu Salam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyeduthu Salam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-u Pottu Odi Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-u Pottu Odi Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyeduthu Salam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyeduthu Salam Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkanu Thangachi Avasaram Urava Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkanu Thangachi Avasaram Urava Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkanu Thangachi Avasaram Urava Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkanu Thangachi Avasaram Urava Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalikku Thangam Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalikku Thangam Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalikka Vengayam Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalikka Vengayam Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalikku Thangam Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalikku Thangam Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalikka Vengayam Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalikka Vengayam Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koolikku Velai Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koolikku Velai Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooppitakka Odiyaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooppitakka Odiyaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Koolikku Velai Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koolikku Velai Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooppitakka Odiyaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooppitakka Odiyaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saathi Namma Saathiyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Namma Saathiyinnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimaare Saranammunnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimaare Saranammunnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi Namma Saathiyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Namma Saathiyinnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaimaare Saranammunnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimaare Saranammunnar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podugamma Vottuyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podugamma Vottuyinnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Podalangaya Paathuyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podalangaya Paathuyinnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Podugamma Vottuyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Podugamma Vottuyinnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Podalangaya Paathuyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podalangaya Paathuyinnar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yezhai Eliyavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhai Eliyavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarukkum Nanmaiyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum Nanmaiyinnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhai Eliyavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhai Eliyavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarukkum Nanmaiyinnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum Nanmaiyinnar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvu Malaruminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvu Malaruminnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumai Ellam Koraiyuminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumai Ellam Koraiyuminnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvu Malaruminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvu Malaruminnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumai Ellam Koraiyuminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumai Ellam Koraiyuminnar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Kedaikkuminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Kedaikkuminnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaivaasi Koraiyuminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaivaasi Koraiyuminnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Velai Kedaikkuminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Kedaikkuminnar"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaivaasi Koraiyuminnar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaivaasi Koraiyuminnar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondhakaran Adichikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhakaran Adichikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaikaran Sirichikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaikaran Sirichikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhakaran Adichikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhakaran Adichikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaikaran Sirichikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaikaran Sirichikiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachaiya Pallulichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachaiya Pallulichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angalinna Pangalinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angalinna Pangalinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachaiya Pallulichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachaiya Pallulichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angalinna Pangalinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angalinna Pangalinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu Naalu Koothuthaana Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Naalu Koothuthaana Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Naalu Koothuthaana Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Naalu Koothuthaana Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aprom Partha Kuda Sirikka Maattan Yennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aprom Partha Kuda Sirikka Maattan Yennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aprom Partha Kuda Sirikka Maattan Yennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aprom Partha Kuda Sirikka Maattan Yennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallu Vetti Thundu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallu Vetti Thundu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mike’u Vechu Vilambarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mike’u Vechu Vilambarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallu Vetti Thundu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallu Vetti Thundu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mike’u Vechu Vilambarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mike’u Vechu Vilambarama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Veettu Pillai Innar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Veettu Pillai Innar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Poda Venum Innar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Poda Venum Innar"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Veettu Pillai Innar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Veettu Pillai Innar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Poda Venum Innar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Poda Venum Innar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutty Potta Punai Pola Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Potta Punai Pola Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty Potta Punai Pola Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Potta Punai Pola Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Kumbutha Kai Yerakkalaiye Yennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Kumbutha Kai Yerakkalaiye Yennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar Kumbutha Kai Yerakkalaiye Yennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar Kumbutha Kai Yerakkalaiye Yennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkayila Dubai Sendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkayila Dubai Sendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoolu Mela Vellai Thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoolu Mela Vellai Thundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkayila Dubai Sendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkayila Dubai Sendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoolu Mela Vellai Thundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoolu Mela Vellai Thundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponavaatti President
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponavaatti President"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadiyum Varanumendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadiyum Varanumendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponavaatti President
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponavaatti President"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadiyum Varanumendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadiyum Varanumendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaychu Vanthathume Sellatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaychu Vanthathume Sellatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaychu Vanthathume Sellatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaychu Vanthathume Sellatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oore Singapore Maathuvenu Sonnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Singapore Maathuvenu Sonnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oore Singapore Maathuvenu Sonnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Singapore Maathuvenu Sonnatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vottu Kekka Vanthaagale Sinnatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vottu Kekka Vanthaagale Sinnatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Orutharaiyum Kaanalaiye Ennatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Orutharaiyum Kaanalaiye Ennatha"/>
</div>
</pre>
