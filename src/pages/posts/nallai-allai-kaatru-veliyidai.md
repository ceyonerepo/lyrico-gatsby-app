---
title: "nallai allai song lyrics"
album: "Kaatru Veliyidai"
artist: "A R Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kaatru-veliyidai-lyrics"
song: "Nallai Allai"
image: ../../images/albumart/kaatru-veliyidai.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zQuwbmmpKrU"
type: "love"
singers:
  -	Sathya Prakash
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanil thedi nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil thedi nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhi nee adaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi nee adaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhi naan vizhunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi naan vizhunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanil nee ezhundhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil nee ezhundhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai natchathira kattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai natchathira kattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alayavittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alayavittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan endra ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan endra ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholayavittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholayavittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallai allai nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallai allai nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilavae nee nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilavae nee nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallai allai nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallai allai nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalliravae nee nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalliravae nee nallai allai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oligalin thedal enbadhellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oligalin thedal enbadhellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil mudigindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil mudigindradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathin thedal enbadhellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathin thedal enbadhellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnyaanathil mudigindradhaeaeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnyaanathil mudigindradhaeaeeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan unnai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyilae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyilae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam soodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam soodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odivittaaiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odivittaaiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallai allai nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallai allai nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilavae nee nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilavae nee nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallai allai nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallai allai nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalliravae nee nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalliravae nee nallai allai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooo ooo oohhoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo ooo oohhoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooooohoooooo ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooooohoooooo ohooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugai mugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugai mugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottendra nilaigalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottendra nilaigalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muganthoda kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muganthoda kaathirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar endra nilai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar endra nilai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooththirundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooththirundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam kolla kaathirunthenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam kolla kaathirunthenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magarandham thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarandham thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nugarum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nugarum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyil kaattil veezhndhuvittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyil kaattil veezhndhuvittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallai allai nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallai allai nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarumpoovae nee nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarumpoovae nee nallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallai allainallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallai allainallai allai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai kollai nee nallai allai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai kollai nee nallai allai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohoooohoooohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohoooohoooohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooo"/>
</div>
</pre>
