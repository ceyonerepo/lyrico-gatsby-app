---
title: "yaarparthadhu song lyrics"
album: "Danny"
artist: "Santhosh Dhayanidhi - Sai Bhaskar"
lyricist: "Thanikodi"
director: "LC Santhanamoorthy"
path: "/albums/danny-lyrics"
song: "Yaarparthadhu"
image: ../../images/albumart/danny.jpg
date: 2020-08-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lHJUAf3uyTo"
type: "melody"
singers:
  - Uthira Unnikrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar parthadhu vaanil pooranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar parthadhu vaanil pooranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar vazhnthathu vaazhkai yaavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vazhnthathu vaazhkai yaavaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam vandhadhu sendru seravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vandhadhu sendru seravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerinil nandri kooravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerinil nandri kooravae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oosaigal uyir oosaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosaigal uyir oosaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai nenjil ozhi yetrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai nenjil ozhi yetrumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamae adhu podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamae adhu podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin vannam nammai theettumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin vannam nammai theettumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam kaatridam moochai vaanginom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kaatridam moochai vaanginom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal theernthadhum thandhu pogirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal theernthadhum thandhu pogirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondha uravo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha uravo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha uravo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha uravo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagalamum maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagalamum maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pozhudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pozhudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum poi serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum poi serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbu manangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu manangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konda viralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konda viralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyinai kothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyinai kothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai manadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru nirangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru nirangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaivadhum oor saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaivadhum oor saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasam tharum vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam tharum vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yaar thaan arivaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai yaar thaan arivaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum kuyil nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum kuyil nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yaar thaan varaivaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai yaar thaan varaivaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyilae ilaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyilae ilaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam naam pogirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam naam pogirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaigalin marangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigalin marangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai naam koorinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai naam koorinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhiya uyirgalpoi sergirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhiya uyirgalpoi sergirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar parthadhu vaanil pooranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar parthadhu vaanil pooranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar vazhnthathu vaazhkai yaavaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vazhnthathu vaazhkai yaavaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam vandhadhu sendru seravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vandhadhu sendru seravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerinil nandri kooravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerinil nandri kooravae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oosaigal uyir oosaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosaigal uyir oosaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai nenjil ozhi yetrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai nenjil ozhi yetrumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamae adhu podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamae adhu podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin vannam nammai theettumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin vannam nammai theettumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam kaatridam moochai vaanginom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kaatridam moochai vaanginom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal theernthadhum thandhu pogirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal theernthadhum thandhu pogirom"/>
</div>
</pre>
