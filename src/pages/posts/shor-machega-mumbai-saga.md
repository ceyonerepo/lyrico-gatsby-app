---
title: "shor machega song lyrics"
album: "Mumbai Saga"
artist: "Yo Yo Honey Singh"
lyricist: "Yo Yo Honey Singh - Hommie Dilliwala"
director: "Sanjay Gupta"
path: "/albums/mumbai-saga-lyrics"
song: "Shor Machega"
image: ../../images/albumart/mumbai-saga.jpg
date: 2021-03-19
lang: hindi
youtubeLink: "https://www.youtube.com/embed/5G2y80X6s6g"
type: "celebration"
singers:
  - Yo Yo Honey Singh
  - Hommie Dilliwala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bache na koyi meri nazron se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bache na koyi meri nazron se"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachaun gulzar ki gazlon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachaun gulzar ki gazlon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naach ke jao ghar subah to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naach ke jao ghar subah to"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishan pairon pe zakhmon ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishan pairon pe zakhmon ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bache na koyi meri nazron se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bache na koyi meri nazron se"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachaun gulzar ki gazlon pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachaun gulzar ki gazlon pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naach ke jao ghar subah to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naach ke jao ghar subah to"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishan pairon pe zakhmon ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishan pairon pe zakhmon ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main hakeem laaya dawayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hakeem laaya dawayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis se honge saare high
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis se honge saare high"/>
</div>
<div class="lyrico-lyrics-wrapper">Do number ki meri kamayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do number ki meri kamayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Se machata hun tabahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Se machata hun tabahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main hakeem laaya dawayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hakeem laaya dawayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis se honge saare high
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis se honge saare high"/>
</div>
<div class="lyrico-lyrics-wrapper">Do number ki meri kamayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do number ki meri kamayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Se machata hun tabahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Se machata hun tabahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tabhi to machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tabhi to machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ab iss party mein kisi ko na chhodunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab iss party mein kisi ko na chhodunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Taang adaane walon ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taang adaane walon ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nacha ke taangein todunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nacha ke taangein todunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedon pe daaru ki bolatlein sajaun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedon pe daaru ki bolatlein sajaun"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya bottle pe chammach baja ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya bottle pe chammach baja ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya ko nachaun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya ko nachaun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khulle mein jiyo ghut ke na maro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khulle mein jiyo ghut ke na maro"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir roll mein thodi thodi grass bharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir roll mein thodi thodi grass bharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar mere maal pe tumko bharosa nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar mere maal pe tumko bharosa nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">To pehle istemaal karo phir vishwas karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To pehle istemaal karo phir vishwas karo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodi booze karke body loose karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodi booze karke body loose karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir bulb ghar ke tu fuse karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir bulb ghar ke tu fuse karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe hadh se jyada karunga pyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe hadh se jyada karunga pyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Misuse nahi mehsoos karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Misuse nahi mehsoos karke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sharma na paas aa na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharma na paas aa na"/>
</div>
<div class="lyrico-lyrics-wrapper">Gale se laga na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gale se laga na"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayega maza na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayega maza na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kahegi rozana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahegi rozana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor, machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor, machega machega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega, floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega, floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo honey singh!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo honey singh!"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Hommie dilli wala!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hommie dilli wala!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<div class="lyrico-lyrics-wrapper">Hilega hilega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hilega hilega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
<div class="lyrico-lyrics-wrapper">Machega machega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machega machega"/>
</div>
</pre>
