---
title: "Paravaigala lyrics"
album: "Ka Pae Ranasingam"
artist: "Ghibran"
lyricist: "Vairamuthu"
director: "P Virumandi"
path: "/albums/ka-pae-ranasingam-lyrics"
song: "Paravaigala"
image: ../../images/albumart/ka-pae-ranasingam.jpg
date: 2020-10-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ss3nBoHapek"
type: "sad"
singers:
  - Manikandan Perumpadappu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hoo ooo hoo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo hoo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo oooo hoo oo hoo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hooo oooo hoo oo hoo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paravaigala paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaigala paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyedutha paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasiyedutha paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhveliyil irai thedum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paazhveliyil irai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaivana paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaivana paravaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uravellam vayiru valakkavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravellam vayiru valakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai vikka poningala
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirai vikka poningala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullooru aatta viththu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullooru aatta viththu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagatha maechigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottagatha maechigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneril sondham polambudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneril sondham polambudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thaandi ketkum thaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thaandi ketkum thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paravaigala paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaigala paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyedutha paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasiyedutha paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhveliyil irai thedum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paazhveliyil irai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaivana paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaivana paravaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haaa… aaa…aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa… aaa…aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa….aaa…aaa….haa…aa…aa..aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa….aaa…aaa….haa…aa…aa..aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haaa… aaa…aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa… aaa…aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haaa… aaa…aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa… aaa…aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo ooo ooo ooo oo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo ooo oo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo ooo oo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo ooo oo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo oo hoo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo oo hoo oo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo ooo oo o ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hooo ooo oo o ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nadhi illadha oorai vittu odi vandhiga
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadhi illadha oorai vittu odi vandhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai illadha naadu thedi vaazha vandhiga
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai illadha naadu thedi vaazha vandhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalum theanum odumendru paalai vandhiga
<input type="checkbox" class="lyrico-select-lyric-line" value="Paalum theanum odumendru paalai vandhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eecha marathil veppan kaayae kaikka kandiga
<input type="checkbox" class="lyrico-select-lyric-line" value="Eecha marathil veppan kaayae kaikka kandiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pondu pulla kaangkaama kannu yengudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pondu pulla kaangkaama kannu yengudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha velai theeraama vayasaagudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandha velai theeraama vayasaagudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaikatchiyil oor paarkaiyil uyir theiyudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholaikatchiyil oor paarkaiyil uyir theiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paravaigala paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaigala paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyedutha paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasiyedutha paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhveliyil irai thedum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paazhveliyil irai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaivana paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaivana paravaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uravellam vayiru valakkavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravellam vayiru valakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai vikka poningala
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirai vikka poningala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullooru aatta viththu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullooru aatta viththu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagatha maechigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottagatha maechigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneril sondham polambudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneril sondham polambudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thaandi ketkum thaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thaandi ketkum thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paravaigala paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaigala paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyedutha paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasiyedutha paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhveliyil irai thedum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paazhveliyil irai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaivana paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaivana paravaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sondha ooril sondha bandham
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondha ooril sondha bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhu nikkuthuga
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondhu nikkuthuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona usuru vaaratheppo
<input type="checkbox" class="lyrico-select-lyric-line" value="Pona usuru vaaratheppo"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambi nikkuthuga
<input type="checkbox" class="lyrico-select-lyric-line" value="Polambi nikkuthuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veettukaaran vaetti sattai
<input type="checkbox" class="lyrico-select-lyric-line" value="Veettukaaran vaetti sattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevi paakkuthuga
<input type="checkbox" class="lyrico-select-lyric-line" value="Neevi paakkuthuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullai ellam bommaiyoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullai ellam bommaiyoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi paakkuthuga
<input type="checkbox" class="lyrico-select-lyric-line" value="Pesi paakkuthuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhndha bhoomi thorathalayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhndha bhoomi thorathalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumai thorathudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Varumai thorathudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thanni pirikkalaiyae kasu pirikkudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thanni pirikkalaiyae kasu pirikkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaagam poguthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaagam poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam poguthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Megam poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga povoma
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanga povoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paravaigala paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaigala paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyedutha paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasiyedutha paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhveliyil irai thedum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paazhveliyil irai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaivana paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaivana paravaigala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uravellam vayiru valakkavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravellam vayiru valakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai vikka poningala
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirai vikka poningala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullooru aatta viththu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullooru aatta viththu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagatha maechigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottagatha maechigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneril sondham polambudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneril sondham polambudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thaandi ketkum thaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thaandi ketkum thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paravaigala paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Paravaigala paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiyedutha paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasiyedutha paravaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhveliyil irai thedum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paazhveliyil irai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaivana paravaigala
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaivana paravaigala"/>
</div>
</pre>
