---
title: "varum aana varaathu song lyrics"
album: "Seemaraja"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/seemaraja-lyrics"
song: "Varum Aana Varaathu"
image: ../../images/albumart/seemaraja.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HH-Fgn4skEs"
type: "love"
singers:
  - D Imman
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varum aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum aana varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum aana varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu ottura ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu ottura ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaru takkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru takkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaru takkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru takkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattunu vettura ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattunu vettura ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru makkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru makkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru makkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru makkaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu ottura ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu ottura ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaru takkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru takkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaru takkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru takkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattunu vettura ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattunu vettura ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru makkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru makkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru makkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru makkaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnunga pala vitham da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunga pala vitham da"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna iva thani ragam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna iva thani ragam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjama ava siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjama ava siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula mazhai varum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula mazhai varum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaana azhagu selai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaana azhagu selai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pambarama sutha varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambarama sutha varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaana en manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaana en manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachai kili kotha varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai kili kotha varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana varaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala nenachi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala nenachi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kerangi vathangi kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerangi vathangi kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Odainja kalayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odainja kalayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Odungi adangi nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odungi adangi nadakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silambattam aadum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silambattam aadum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja vazhiyama vangi thingum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja vazhiyama vangi thingum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli aattam potta ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli aattam potta ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva korangattam thaava vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva korangattam thaava vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala vithaiya kathava kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vithaiya kathava kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava nakkalum vikkalum dhool-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava nakkalum vikkalum dhool-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai partha confirm thondridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai partha confirm thondridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaal-u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaal-u thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana varaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthana thanae nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanae nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha nanae nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana thanae nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanae nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha nanae nae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthana thanae nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanae nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha nanae nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana thanae nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanae nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha nanae nae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramaiya vasthavaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramaiya vasthavaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramaiya vasthavaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramaiya vasthavaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramaiya vasthavaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramaiya vasthavaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramaiya vasthavaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramaiya vasthavaiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mein ne dil tujh ko diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein ne dil tujh ko diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein ne dil tujh ko diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein ne dil tujh ko diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramaiya vasthavaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramaiya vasthavaiyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramaiya vasthavaiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramaiya vasthavaiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthana thanthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana thanthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanthana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruva murukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruva murukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura surukki izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura surukki izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu kolathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu kolathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhutha pudichi amukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha pudichi amukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudhi ethum antha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudhi ethum antha pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Wine-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wine-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala kudicha thaan theerum intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala kudicha thaan theerum intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pain-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pain-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkulla oodum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulla oodum kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Train-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Train-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava mudi sooda munnae vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava mudi sooda munnae vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Queen-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Queen-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava chikkunu nikkira rosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava chikkunu nikkira rosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veral thottuda sokkuren raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veral thottuda sokkuren raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu lesa pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu lesa pookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthidum kaadhal-u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthidum kaadhal-u thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana varaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama bheema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama bheema"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooohey ehhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hooohey ehhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum aana varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum aana varum"/>
</div>
</pre>
