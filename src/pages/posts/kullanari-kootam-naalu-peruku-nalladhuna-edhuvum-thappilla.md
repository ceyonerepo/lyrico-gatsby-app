---
title: "kullanari kootam song lyrics"
album: "Naalu Peruku Nalladhuna Edhuvum Thappilla"
artist: "Navin"
lyricist: "Kalai Sai Arun"
director: "Dinesh Selvaraj"
path: "/albums/naalu-peruku-nalladhuna-edhuvum-thappilla-lyrics"
song: "Kullanari Kootam"
image: ../../images/albumart/naalu-peruku-nalladhuna-edhuvum-thappilla.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v_Ua9vVLS_Y"
type: "happy"
singers:
  - Anirudh Ravichander
  - MC Rude
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vanakam makkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakam makkale"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarum thayaraikongas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarum thayaraikongas"/>
</div>
<div class="lyrico-lyrics-wrapper">suma majava semaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suma majava semaya"/>
</div>
<div class="lyrico-lyrics-wrapper">takara paati vara poguthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takara paati vara poguthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanakam makkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakam makkale"/>
</div>
<div class="lyrico-lyrics-wrapper">kullanari kootam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kullanari kootam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kolaiyila kodi panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolaiyila kodi panam"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nekka padu sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nekka padu sokka"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kaavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kaavali"/>
</div>
<div class="lyrico-lyrics-wrapper">kootha theru kootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootha theru kootha"/>
</div>
<div class="lyrico-lyrics-wrapper">palakura komaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palakura komaali"/>
</div>
<div class="lyrico-lyrics-wrapper">machi nogama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi nogama "/>
</div>
<div class="lyrico-lyrics-wrapper">nungu thunnura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nungu thunnura"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram poova suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram poova suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">nokama nungu thunura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nokama nungu thunura"/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram poova suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram poova suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">machi semma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi semma"/>
</div>
<div class="lyrico-lyrics-wrapper">takker planningu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takker planningu"/>
</div>
<div class="lyrico-lyrics-wrapper">vilaiyadi mudipom eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilaiyadi mudipom eppo"/>
</div>
<div class="lyrico-lyrics-wrapper">just like the gammingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just like the gammingu"/>
</div>
<div class="lyrico-lyrics-wrapper">katuven unaku nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katuven unaku nan"/>
</div>
<div class="lyrico-lyrics-wrapper">live va na stremingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="live va na stremingu"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu thool kelapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu thool kelapu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalathil eppo neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalathil eppo neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">erangu maama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erangu maama "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than polappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than polappu"/>
</div>
<div class="lyrico-lyrics-wrapper">i guess ithu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i guess ithu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini no more explanation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini no more explanation"/>
</div>
<div class="lyrico-lyrics-wrapper">just sit back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just sit back"/>
</div>
<div class="lyrico-lyrics-wrapper">and watch us now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and watch us now"/>
</div>
<div class="lyrico-lyrics-wrapper">negative and action
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="negative and action"/>
</div>
<div class="lyrico-lyrics-wrapper">ethukku thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethukku thaney"/>
</div>
<div class="lyrico-lyrics-wrapper">pottum komali vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottum komali vesam"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pada vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pada vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">velai mudithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velai mudithu"/>
</div>
<div class="lyrico-lyrics-wrapper">kada kada vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada kada vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">kasai eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasai eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhadam theriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhadam theriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">escapeu vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="escapeu vida"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyatha velaiethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyatha velaiethu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalu peru onna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalu peru onna "/>
</div>
<div class="lyrico-lyrics-wrapper">sentha thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sentha thaney"/>
</div>
<div class="lyrico-lyrics-wrapper">team work 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="team work "/>
</div>
<div class="lyrico-lyrics-wrapper">life fula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life fula"/>
</div>
<div class="lyrico-lyrics-wrapper">ethu pola illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu pola illa"/>
</div>
<div class="lyrico-lyrics-wrapper">semma kikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma kikku"/>
</div>
<div class="lyrico-lyrics-wrapper">theyvai verum lucku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theyvai verum lucku"/>
</div>
<div class="lyrico-lyrics-wrapper">kathukko intha tricku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathukko intha tricku"/>
</div>
<div class="lyrico-lyrics-wrapper">nogama nungu thinnura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nogama nungu thinnura "/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram poova suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram poova suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">nogama nungu thinnura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nogama nungu thinnura "/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram poova suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram poova suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">nogama nungu thinnura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nogama nungu thinnura "/>
</div>
<div class="lyrico-lyrics-wrapper">kathoram poova suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathoram poova suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa pa pa pa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa pa pa pa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">eppa semma kutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppa semma kutha"/>
</div>
<div class="lyrico-lyrics-wrapper">erukku ellarukum nandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erukku ellarukum nandri"/>
</div>
</pre>
