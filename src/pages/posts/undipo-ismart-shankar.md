---
title: "undipo song lyrics"
album: "Ismart Shankar"
artist: "Mani Sharma"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Puri Jagannadh"
path: "/albums/ismart-shankar-lyrics"
song: "Undipo"
image: ../../images/albumart/ismart-shankar.jpg
date: 2019-07-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/C-Cf0ZWdyEE"
type: "love"
singers:
  - Anurag Kulkarni
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Undipo undipo chethilo geethala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipo undipo chethilo geethala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu undipo nuditipai raathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu undipo nuditipai raathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipo undipo kallalo kaanthila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipo undipo kallalo kaanthila"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu undipo pedavipai navvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu undipo pedavipai navvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone nindipoye naa jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone nindipoye naa jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadilesi vellanandi ye jnaapakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadilesi vellanandi ye jnaapakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasey moyyalenanthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasey moyyalenanthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti kolavalenanthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti kolavalenanthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vippi cheppalenanthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vippi cheppalenanthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye kammukuntundhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye kammukuntundhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento chanti pillaadila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento chanti pillaadila"/>
</div>
<div class="lyrico-lyrics-wrapper">Neney thappipoyaanuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neney thappipoyaanuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne vethukuthu undaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne vethukuthu undaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo dhorukuthunnaanuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo dhorukuthunnaanuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undipo undipo chethilo geethala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipo undipo chethilo geethala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu undipo nuditipai raathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu undipo nuditipai raathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarikotha thadabaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikotha thadabaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarindhi alavaatu laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarindhi alavaatu laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi chedda alavaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi chedda alavaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadilesi oka maatu raava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadilesi oka maatu raava"/>
</div>
<div class="lyrico-lyrics-wrapper">Medavampu thaakuthunte munivellatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medavampu thaakuthunte munivellatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bidiyaalu paaripova yetuvaipuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bidiyaalu paaripova yetuvaipuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha sanngaga sannaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha sanngaga sannaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanna jaajila navvaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanna jaajila navvaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam lechi vacchindhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam lechi vacchindhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli puttinattundhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli puttinattundhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho mellaga mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho mellaga mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatukallane thippaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatukallane thippaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neno rangula raatnamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neno rangula raatnamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu thiruguthunnaanuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu thiruguthunnaanuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala nimire chanuvavuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala nimire chanuvavuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu gaani polamaruthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu gaani polamaruthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa maate nizamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa maate nizamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi saari polamaaripotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi saari polamaaripotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagaali gani nuvvu alavokaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaali gani nuvvu alavokaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa praanamaina istha adagocchuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa praanamaina istha adagocchuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam needhani naadhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam needhani naadhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu veruga levugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu veruga levugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepudo kalupukunnam kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudo kalupukunnam kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiga undalenanthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiga undalenanthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Undham adugulo adugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undham adugulo adugula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vindham premalo gala gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vindham premalo gala gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham bigisipoyindhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham bigisipoyindhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Antham kaadule mana katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antham kaadule mana katha"/>
</div>
</pre>
