---
title: "lolikiriya song lyrics"
album: "Junga"
artist: "Siddharth Vipin"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/junga-lyrics"
song: "Lolikiriya"
image: ../../images/albumart/junga.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nsUl-D4Iyxs"
type: "happy"
singers:
  - Marana Gana Viji
  - Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">North-u madrasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="North-u madrasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethu potta mettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu potta mettu"/>
</div>
<div class="lyrico-lyrics-wrapper">North-u koriyaavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="North-u koriyaavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma talent-u viral hit-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma talent-u viral hit-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasar gaanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasar gaanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Teaser vitta paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teaser vitta paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Australia-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Australia-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaasthi girls-ku favourite-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaasthi girls-ku favourite-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illa coach-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illa coach-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennooru beach-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennooru beach-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu large-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu large-u"/>
</div>
<div class="lyrico-lyrics-wrapper">En paattu uruvaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paattu uruvaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anna nagar arch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna nagar arch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Mumbai reach-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mumbai reach-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaiyum thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaiyum thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Foreign pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lolikiriya kaamikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kaamikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya kalaaikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kalaaikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya kozhikariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kozhikariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya thaalikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya thaalikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya sirikkiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya sirikkiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya moraikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya moraikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya kochikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kochikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya pichikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya pichikiriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru idli rotikkum kashtam appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru idli rotikkum kashtam appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Italy naatula pasta ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Italy naatula pasta ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattini kedandha singer appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattini kedandha singer appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paris city-la star-u ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paris city-la star-u ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jone jone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jone jone"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna jone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna jone"/>
</div>
<div class="lyrico-lyrics-wrapper">Jone jone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jone jone"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna jone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna jone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei anjukkum paththukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei anjukkum paththukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaalraa appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalraa appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuthu dhuttu paarra ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuthu dhuttu paarra ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakkalu kindalu oorla appo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakkalu kindalu oorla appo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilu kuyilu suththudhu ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilu kuyilu suththudhu ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evalum annaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalum annaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kandukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kandukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana enakku innikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana enakku innikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala girls-u tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala girls-u tholla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadakkae ganga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakkae ganga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhakkae vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakkae vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaikkae sonnaru annan junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaikkae sonnaru annan junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha vecharu ennai king-a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha vecharu ennai king-a"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lolikiriya kaamikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kaamikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya kalaaikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kalaaikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya yosikkiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya yosikkiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya vasikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya vasikiriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada lajakku bajakku karlaa katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada lajakku bajakku karlaa katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kubukku kubukku raththam sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kubukku kubukku raththam sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku ethukku annan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku ethukku annan kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Edakku madakku vechikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakku madakku vechikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadakku thadakku thavadakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadakku thadakku thavadakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Padakku padakku odichikkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padakku padakku odichikkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulu pinjirum yaaru kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulu pinjirum yaaru kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarlu meralu annan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarlu meralu annan kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othungu othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othungu othungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada othikkappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada othikkappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari pathungu pathungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari pathungu pathungu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada padthukkappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada padthukkappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadakkae ganga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadakkae ganga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhakkae vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakkae vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaikkae sonnaru annan junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaikkae sonnaru annan junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha vecharu ennai king-a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha vecharu ennai king-a"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lolikiriya kaamikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kaamikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya kalaaikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya kalaaikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya nallakkiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya nallakkiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolikiriya dullakkiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolikiriya dullakkiriya"/>
</div>
</pre>
