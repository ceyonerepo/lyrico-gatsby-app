---
title: "kozhi mutta song lyrics"
album: "Seyal"
artist: "Siddharth Vipin"
lyricist: "Siddharth Vipin"
director: "Ravi Abbulu"
path: "/albums/seyal-lyrics"
song: "Kozhi Mutta"
image: ../../images/albumart/seyal.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ww5jG1bGZpw"
type: "gana"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">color color kathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="color color kathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanangada koothadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanangada koothadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasi medu meenbadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasi medu meenbadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaja muja un body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaja muja un body"/>
</div>
<div class="lyrico-lyrics-wrapper">power power kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="power power kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">paranthu adikum killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthu adikum killadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu paru munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu paru munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">so one so one so one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so one so one so one"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bigiru bigur thakkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bigiru bigur thakkali"/>
</div>
<div class="lyrico-lyrics-wrapper">thamma thundu vakkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamma thundu vakkale"/>
</div>
<div class="lyrico-lyrics-wrapper">nagaru nagaru nakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagaru nagaru nakale"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga semma sokkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga semma sokkale"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu mooku kammalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu mooku kammalu"/>
</div>
<div class="lyrico-lyrics-wrapper">matiguna ambal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matiguna ambal"/>
</div>
<div class="lyrico-lyrics-wrapper">oothiduvom pasumbalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothiduvom pasumbalu"/>
</div>
<div class="lyrico-lyrics-wrapper">so one so one so one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="so one so one so one"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye sagalupeta salpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye sagalupeta salpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">savara kathi nan pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savara kathi nan pota"/>
</div>
<div class="lyrico-lyrics-wrapper">mana pattu mai pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana pattu mai pota"/>
</div>
<div class="lyrico-lyrics-wrapper">manja thane kai patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja thane kai patta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei idi puducha pul thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei idi puducha pul thane"/>
</div>
<div class="lyrico-lyrics-wrapper">idiya thane thanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idiya thane thanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">sarale sarale sarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarale sarale sarale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kozhi mutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kozhi mutta "/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo kuruvi mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo kuruvi mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">gana kuyir mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana kuyir mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">poda kuma kumango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda kuma kumango"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaathu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaathu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha vaala matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha vaala matta"/>
</div>
<div class="lyrico-lyrics-wrapper">oosa oora vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosa oora vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">poda duma dumanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda duma dumanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pambu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pambu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa palli mutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa palli mutta "/>
</div>
<div class="lyrico-lyrics-wrapper">kadal meenu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal meenu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">adra amma ammango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adra amma ammango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee poochi mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee poochi mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">white pura mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="white pura mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">kata erumbu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata erumbu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka jathy pathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka jathy pathuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">namma koova nathi aalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma koova nathi aalam"/>
</div>
<div class="lyrico-lyrics-wrapper">katuvenda paalam pulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katuvenda paalam pulli"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha kolam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha kolam "/>
</div>
<div class="lyrico-lyrics-wrapper">dar daru teen age
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dar daru teen age"/>
</div>
<div class="lyrico-lyrics-wrapper">dung war old age
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dung war old age"/>
</div>
<div class="lyrico-lyrics-wrapper">dosa kallu sudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dosa kallu sudu"/>
</div>
100 <div class="lyrico-lyrics-wrapper">la speed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la speed"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthala thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthala thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">ittu podura ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ittu podura ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu podura va va va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu podura va va va"/>
</div>
<div class="lyrico-lyrics-wrapper">height ah sonthu kaima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="height ah sonthu kaima"/>
</div>
<div class="lyrico-lyrics-wrapper">moocku mela kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moocku mela kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">moochi kaluvu veliya naluvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochi kaluvu veliya naluvu"/>
</div>
<div class="lyrico-lyrics-wrapper">dastha aaiduva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dastha aaiduva da"/>
</div>
<div class="lyrico-lyrics-wrapper">sithu moola kara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithu moola kara"/>
</div>
<div class="lyrico-lyrics-wrapper">pori orunda avul orunda thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pori orunda avul orunda thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan nalla soru ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan nalla soru ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">solaava ninnu enjoy pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solaava ninnu enjoy pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">alagu da ithu biriyani sonnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagu da ithu biriyani sonnu"/>
</div>
<div class="lyrico-lyrics-wrapper">meriju mela paru intha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meriju mela paru intha "/>
</div>
<div class="lyrico-lyrics-wrapper">kallela suthama aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallela suthama aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">nee kozhi mutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kozhi mutta "/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo kuruvi mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo kuruvi mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">gana kuyir mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana kuyir mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">poda kuma kumango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda kuma kumango"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaathu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaathu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha vaala matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha vaala matta"/>
</div>
<div class="lyrico-lyrics-wrapper">oosa oora vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosa oora vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">poda duma dumanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda duma dumanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pambu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pambu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa palli mutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa palli mutta "/>
</div>
<div class="lyrico-lyrics-wrapper">kadal meenu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal meenu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">adra amma ammango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adra amma ammango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee poochi mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee poochi mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">white pura mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="white pura mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">kata erumbu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata erumbu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka jathy pathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka jathy pathuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arisi vetti sela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arisi vetti sela "/>
</div>
<div class="lyrico-lyrics-wrapper">putukuna maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putukuna maala"/>
</div>
<div class="lyrico-lyrics-wrapper">mintuthanga saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mintuthanga saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">deal no deal no doubt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deal no deal no doubt"/>
</div>
<div class="lyrico-lyrics-wrapper">hint nan potena kouut
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hint nan potena kouut"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu kutty aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu kutty aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">aari pogum paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aari pogum paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">surutikada vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="surutikada vaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">thava thavura thavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thava thavura thavala"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya koovura goyalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya koovura goyalla"/>
</div>
<div class="lyrico-lyrics-wrapper">wrong right ethu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wrong right ethu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sathuvana aala seenda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathuvana aala seenda "/>
</div>
<div class="lyrico-lyrics-wrapper">solura thandi thullura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solura thandi thullura"/>
</div>
<div class="lyrico-lyrics-wrapper">podu marunthu oosi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu marunthu oosi da"/>
</div>
<div class="lyrico-lyrics-wrapper">thodachukada dhoosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodachukada dhoosi"/>
</div>
<div class="lyrico-lyrics-wrapper">kangaiyila irukutha da kasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangaiyila irukutha da kasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">life share auto pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life share auto pola "/>
</div>
<div class="lyrico-lyrics-wrapper">share panni vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="share panni vaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">vename feelu kai poduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vename feelu kai poduda"/>
</div>
<div class="lyrico-lyrics-wrapper">nan palcova pizza pangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan palcova pizza pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">tharen size ah ne kondadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharen size ah ne kondadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadu nice ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu nice ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kozhi mutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kozhi mutta "/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo kuruvi mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo kuruvi mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">gana kuyir mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana kuyir mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">poda kuma kumango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda kuma kumango"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaathu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaathu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha vaala matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha vaala matta"/>
</div>
<div class="lyrico-lyrics-wrapper">oosa oora vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosa oora vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">poda duma dumanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda duma dumanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pambu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pambu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">pa pa palli mutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa pa palli mutta "/>
</div>
<div class="lyrico-lyrics-wrapper">kadal meenu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal meenu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">adra amma ammango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adra amma ammango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee poochi mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee poochi mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">white pura mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="white pura mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">kata erumbu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kata erumbu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka jathy pathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka jathy pathuko"/>
</div>
</pre>
