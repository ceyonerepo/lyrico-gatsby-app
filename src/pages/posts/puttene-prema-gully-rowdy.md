---
title: "puttene prema song lyrics"
album: "Gully Rowdy"
artist: "Sai Karthik – Ram Miriyala"
lyricist: "Bhaskarabhatla"
director: "G. Nageswara Reddy"
path: "/albums/gully-rowdy-lyrics"
song: "Puttene Prema"
image: ../../images/albumart/gully-rowdy.jpg
date: 2021-09-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/txjK_4EtQ5A"
type: "love"
singers:
  - Ram Miriyala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puttene prema padagottene prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttene prema padagottene prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Em chesavo emo kadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em chesavo emo kadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalo prema anthalo koma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo prema anthalo koma"/>
</div>
<div class="lyrico-lyrics-wrapper">Athalakuthalam avuthunnanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athalakuthalam avuthunnanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee perento cheppu pachhabottesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee perento cheppu pachhabottesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oorento cheppu pette sardesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oorento cheppu pette sardesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell numberni cheppu ring ichhesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell numberni cheppu ring ichhesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi date unte cheppu pelli cheseseukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi date unte cheppu pelli cheseseukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttene Puttene prema padagottene prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttene Puttene prema padagottene prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Em chesavo emo kadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em chesavo emo kadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalo prema anthalo koma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo prema anthalo koma"/>
</div>
<div class="lyrico-lyrics-wrapper">Athalakuthalam avuthunnanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athalakuthalam avuthunnanamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katthulatho eppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthulatho eppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">kallolanga unde darullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallolanga unde darullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvulaaga merisave o…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvulaaga merisave o…"/>
</div>
<div class="lyrico-lyrics-wrapper">Maga purugulatho chirakuga unde jeevithamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maga purugulatho chirakuga unde jeevithamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada vasanipude chupave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada vasanipude chupave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee caste ento cheppu nenu marchesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee caste ento cheppu nenu marchesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee taste ento cheppu vanta nerchesukuntanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee taste ento cheppu vanta nerchesukuntanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu cheppedhi cheppu nenu oppesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu cheppedhi cheppu nenu oppesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannku appunte cheppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannku appunte cheppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene teerchesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene teerchesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttene Puttene prema padagottene prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttene Puttene prema padagottene prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Em chesavo emo kadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em chesavo emo kadhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Doma theralaaga oosuranivunde naa life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doma theralaaga oosuranivunde naa life"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendi thera chesave o...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendi thera chesave o..."/>
</div>
<div class="lyrico-lyrics-wrapper">Okka navvuthone kundilanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka navvuthone kundilanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjigundelona prema vitthanale jallesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjigundelona prema vitthanale jallesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee istalu cheppu list rasesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee istalu cheppu list rasesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kastalu cheppu netthi meedhesunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kastalu cheppu netthi meedhesunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Em kaavalo cheppu gift ichhesukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em kaavalo cheppu gift ichhesukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu kadhante cheppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu kadhante cheppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu ooresukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu ooresukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Puttene Puttene prema padagottene prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttene Puttene prema padagottene prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Em chesavo emo kadhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em chesavo emo kadhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalo prema anthalo koma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo prema anthalo koma"/>
</div>
<div class="lyrico-lyrics-wrapper">Athalakuthalam avuthunnanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athalakuthalam avuthunnanamma"/>
</div>
</pre>
