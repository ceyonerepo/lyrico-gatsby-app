---
title: "naalukaalu song lyrics"
album: "Julieum 4 Perum"
artist: "Raghu Sravan Kumar"
lyricist: "G G Ganesh"
director: "R V Satheesh"
path: "/albums/julieum-4-perum-lyrics"
song: "Naalu Kaalu"
image: ../../images/albumart/julieum-4-perum.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5_ZuJ9YPKxQ"
type: "sad gaana"
singers:
  -	Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naalu Kaalu Usurungo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Kaalu Usurungo "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Peru Tholachengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Peru Tholachengo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu Kaalu Usurungo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Kaalu Usurungo "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Peru Tholachengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Peru Tholachengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie Eduka Marandhengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Eduka Marandhengo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadanukaaga Nenachikinu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanukaaga Nenachikinu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaya Usar Pannikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaya Usar Pannikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Vazhiyila Naaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Vazhiyila Naaya "/>
</div>
<div class="lyrico-lyrics-wrapper">Pitu Engaluku Reveetu      
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitu Engaluku Reveetu      "/>
</div>
<div class="lyrico-lyrics-wrapper">Aathula Oru Kaala Vechi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathula Oru Kaala Vechi "/>
</div>
<div class="lyrico-lyrics-wrapper">Sethula Oru Kaala Vuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethula Oru Kaala Vuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhupola Aachi Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhupola Aachi Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Kadhaiyum Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Kadhaiyum Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammu Kutty Chella Kutty 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammu Kutty Chella Kutty "/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Maa Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Maa Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Biskothum Ruskothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biskothum Ruskothum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Naan Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Naan Thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammu Kutty Chella Kutty 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammu Kutty Chella Kutty "/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Maa Nee Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Maa Nee Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Biskothum Ruskothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biskothum Ruskothum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Naan Thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Naan Thaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manishana Vida Naayiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishana Vida Naayiku "/>
</div>
<div class="lyrico-lyrics-wrapper">Mavusu Adhigam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavusu Adhigam Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Juli Yala Naama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Juli Yala Naama "/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Therinjikinom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Therinjikinom Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Manishana Vida Naayiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishana Vida Naayiku "/>
</div>
<div class="lyrico-lyrics-wrapper">Mavusu Adhigam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavusu Adhigam Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Juli Yala Naama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Juli Yala Naama "/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Therinjikinom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Therinjikinom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannathukaaga Naayaga Oduraanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannathukaaga Naayaga Oduraanga "/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Panatha Koduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Panatha Koduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaya Thaan Vaanguranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaya Thaan Vaanguranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannathukaaga Naayaga Oduraanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannathukaaga Naayaga Oduraanga "/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Panatha Koduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Panatha Koduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaya Thaan Vaanguranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaya Thaan Vaanguranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidinjidunga Kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjidunga Kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyum Senja Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyum Senja Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjidunga Kaalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjidunga Kaalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyum Senja Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyum Senja Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu Bondhukula Alaiyuran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu Bondhukula Alaiyuran "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaya Thedi Oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaya Thedi Oduran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patta Padipa Thorandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Padipa Thorandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Soru Thaniya Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Thaniya Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta Padipa Thorandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Padipa Thorandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Soru Thaniya Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Thaniya Marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayi Pozhapu Pozhaikiran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayi Pozhapu Pozhaikiran "/>
</div>
<div class="lyrico-lyrics-wrapper">Nayi Paadu Padurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayi Paadu Padurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu Kaalu Usurungo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Kaalu Usurungo "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Peru Tholachengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Peru Tholachengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie Eduka Marandhengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Eduka Marandhengo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Naalu Kaalu Usurungo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naalu Kaalu Usurungo "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Peru Tholachengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Peru Tholachengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Selfie Eduka Marandhengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Eduka Marandhengo"/>
</div>
</pre>
