---
title: "mannil irunthu song lyrics"
album: "Thuneri"
artist: "Kalaiarasan"
lyricist: "K.N. Karunakaran"
director: "Sunil Dixon"
path: "/albums/thuneri-song-lyrics"
song: "Mannil Irunthu"
image: ../../images/albumart/thuneri.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OwwowUTIp5U"
type: "melody"
singers:
  - Janani SV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mannil irunthu saral adithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil irunthu saral adithal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam kulirthu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam kulirthu vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thannai kaana kangal kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannai kaana kangal kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai magizhthu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai magizhthu vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">illanthalire manathukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illanthalire manathukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam vandhu nitradum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam vandhu nitradum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee irunthal bayam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee irunthal bayam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal oodi vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal oodi vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">illankandru thulli odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illankandru thulli odum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai tedum nalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai tedum nalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mannil irunthu saral adithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil irunthu saral adithal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam kulirthu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam kulirthu vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thannai kaana kangal kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannai kaana kangal kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai magizhthu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai magizhthu vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en kanamaniye un kurumbugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanamaniye un kurumbugal"/>
</div>
<div class="lyrico-lyrics-wrapper">rasikindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasikindren "/>
</div>
<div class="lyrico-lyrics-wrapper">thanthai madi methu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthai madi methu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavum alagai negzhkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavum alagai negzhkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaikalay nagarthu thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaikalay nagarthu thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">sendral thavipugal ennile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendral thavipugal ennile"/>
</div>
<div class="lyrico-lyrics-wrapper">un balathai ingu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un balathai ingu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">mattum ariven en uyir unnile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum ariven en uyir unnile"/>
</div>
<div class="lyrico-lyrics-wrapper">iravai thandi vediyum vanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravai thandi vediyum vanam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayame ingu unnil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayame ingu unnil naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mannil irunthu saral adithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil irunthu saral adithal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam kulirthu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam kulirthu vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thannai kaana kangal kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannai kaana kangal kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkai magizhthu vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkai magizhthu vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">illanthalire manathukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illanthalire manathukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam vandhu nitradum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam vandhu nitradum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee irunthal bayam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee irunthal bayam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal oodi vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal oodi vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">illankandru thulli odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illankandru thulli odum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai tedum nalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai tedum nalume"/>
</div>
</pre>
