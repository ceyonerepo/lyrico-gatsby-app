---
title: "kirukkan song lyrics"
album: "Vanjagar Ulagam"
artist: "	Sam CS"
lyricist: "Sam CS"
director: "Manoj Beedha"
path: "/albums/vanjagar-ulagam-lyrics"
song: "Kirukkan"
image: ../../images/albumart/vanjagar-ulagam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Sv3dyJz19O0"
type: "mass"
singers:
  - Sam CS
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuthuth thuthuth thuthuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuthuth thuthuth thuthuth thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vedikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vedikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vedikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vedikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarraa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarraa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmma va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma va"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen ennai kaduppethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen ennai kaduppethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppethi paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppethi paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei venam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam da"/>
</div>
<div class="lyrico-lyrics-wrapper">En kovam unnai erichidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kovam unnai erichidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothachidum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothachidum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya mootra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya mootra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil nenjila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil nenjila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya mootra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya mootra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethura yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethura yethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuthuth thuthuth thuthuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuthuth thuthuth thuthuth thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kozha illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kozha illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna appadi paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna appadi paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir mela aasaruntha nikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir mela aasaruntha nikkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kozha illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kozha illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal varum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal varum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Enta mattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enta mattatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum oru naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum oru naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven da"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru maari thadam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru maari thadam "/>
</div>
<div class="lyrico-lyrics-wrapper">maariScene-ah vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maariScene-ah vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda puriyatha theriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda puriyatha theriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan somba illanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan somba illanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum thaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum thaan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya mootra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya mootra"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil nenjila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil nenjila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya mootra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya mootra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovam yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethura yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethura yethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vedikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vedikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuththu thuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuththu thuth thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuthuth thuthuth thuthuth thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuthuth thuthuth thuthuth thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan satharana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan satharana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal thaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal thaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai matra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai matra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal varum"/>
</div>
<div class="lyrico-lyrics-wrapper">En gun point-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En gun point-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai nikka vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nikka vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">O***a kathara kathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O***a kathara kathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Shoot pannuven da naayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shoot pannuven da naayae"/>
</div>
<div class="lyrico-lyrics-wrapper">T***a pasangala k***a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="T***a pasangala k***a"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennada pannen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennada pannen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kirukkan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kirukkan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vedikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vedikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarraa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarraa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmma va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma va"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen ennai kaduppethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen ennai kaduppethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppethi paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppethi paakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei venam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei venam da"/>
</div>
<div class="lyrico-lyrics-wrapper">En kovam unnai erichidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kovam unnai erichidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothachidum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothachidum da"/>
</div>
</pre>
