---
title: "mazhai pozhindhidum song lyrics"
album: "Oru Kuppai Kathai"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Kaali Rangasamy"
path: "/albums/oru-kuppai-kathai-lyrics"
song: "Mazhai Pozhindhidum"
image: ../../images/albumart/oru-kuppai-kathai.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NTO3FplUbb8"
type: "happy"
singers:
  - Madhu Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thanaaa thaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaaa thaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaaa thana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaaa thana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai pozhindhidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pozhindhidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhil oor eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil oor eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum enna dhooram ahaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna dhooram ahaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyena un mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyena un mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyena en dhaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyena en dhaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeppatridum neram ahaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppatridum neram ahaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru murai oru mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai oru mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai oru thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai oru thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai adhu erikkum ahaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai adhu erikkum ahaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvarai idhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvarai idhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvarai naam povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvarai naam povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrena ondraavomvaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrena ondraavomvaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam irandum urigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam irandum urigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrathu ondrena maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrathu ondrena maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgal kalakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgal kalakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon maalai nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon maalai nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada pennae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada pennae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naan naan enna koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naan naan enna koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil vizhum nadhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil vizhum nadhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanaaa thaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaaa thaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaaa thana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaaa thana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae enai eerthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae enai eerthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ariyen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ariyen anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi enai saaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi enai saaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru naan vizhundhen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru naan vizhundhen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acham madam naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham madam naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai thurandhen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai thurandhen anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno ennullae yedho aanadha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno ennullae yedho aanadha da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaai unthozhil saigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaai unthozhil saigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam mel yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam mel yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam neendhudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam neendhudhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeen koottaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeen koottaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kalandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kalandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam irandum urigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam irandum urigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrathu ondrena maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrathu ondrena maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgal kalakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgal kalakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon maalai nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon maalai nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada pennae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada pennae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naan naan enna koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naan naan enna koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil vizhum nadhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil vizhum nadhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanaaa thaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaaa thaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaaa thana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaaa thana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam moham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam moham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam dhaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam dhaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thukkam pogum ohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thukkam pogum ohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum vaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum vaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram dhooram pogum ohoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram dhooram pogum ohoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai pozhindhidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai pozhindhidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhil oor eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil oor eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum enna dhooram ahaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna dhooram ahaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyena un mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyena un mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyena en dhaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyena en dhaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeppatridum neram ahaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppatridum neram ahaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru murai oru mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai oru mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai oru thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai oru thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai adhu erikkum ahaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai adhu erikkum ahaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvarai idhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvarai idhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvarai naam povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvarai naam povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrena ondraavomvaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrena ondraavomvaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaaa"/>
</div>
</pre>
