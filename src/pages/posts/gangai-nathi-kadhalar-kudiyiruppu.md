---
title: "gangai nathi song lyrics"
album: "Kadhalar Kudiyiruppu"
artist: "James Vasanthan"
lyricist: "Na. Muthukumar"
director: "AMR Ramesh"
path: "/albums/kadhalar-kudiyiruppu-lyrics"
song: "Gangai Nathi"
image: ../../images/albumart/kadhalar-kudiyiruppu.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/22kYSMaVfmw"
type: "happy"
singers:
  - Bellie Raj
  - Padmanabhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gangai nadhi engeyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangai nadhi engeyo "/>
</div>
<div class="lyrico-lyrics-wrapper">pirandhu vandhadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirandhu vandhadhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kaaviri aaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kaaviri aaru "/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo pirandhu vandhadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo pirandhu vandhadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum ondraai aanadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum ondraai aanadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpin kadal serndhadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpin kadal serndhadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam engum pongudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam engum pongudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal vaazhkkai vandhadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal vaazhkkai vandhadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanum mannum ullavarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum mannum ullavarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum sondham idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum sondham idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru idhayangal inaiyum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru idhayangal inaiyum tharunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangai nadhi engeyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangai nadhi engeyo "/>
</div>
<div class="lyrico-lyrics-wrapper">pirandhu vandhadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirandhu vandhadhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kaaviri aaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kaaviri aaru "/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo pirandhu vandhadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo pirandhu vandhadhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thoattathil poovena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thoattathil poovena "/>
</div>
<div class="lyrico-lyrics-wrapper">nanbaa vandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbaa vandhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaatril neengidaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaatril neengidaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam veesudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam veesudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosanthin thooralaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosanthin thooralaai "/>
</div>
<div class="lyrico-lyrics-wrapper">sangeedhathin saaralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangeedhathin saaralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaanadhor paadalaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaanadhor paadalaai "/>
</div>
<div class="lyrico-lyrics-wrapper">veedu aanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedu aanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalthorumey thevaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalthorumey thevaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">theivam varum thaazhvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivam varum thaazhvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalthorumey thevaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalthorumey thevaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">theivam varum thaazhvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivam varum thaazhvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Eazhezhu jenmam thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eazhezhu jenmam thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">eduthaalum appoadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthaalum appoadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un natpai naan kaetpen thoazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un natpai naan kaetpen thoazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangai nadhi engeyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangai nadhi engeyo "/>
</div>
<div class="lyrico-lyrics-wrapper">pirandhu vandhadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirandhu vandhadhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kaaviri aaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kaaviri aaru "/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo pirandhu vandhadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo pirandhu vandhadhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorgolathil thaer ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorgolathil thaer ena "/>
</div>
<div class="lyrico-lyrics-wrapper">ullam aanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam aanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaaley kanavu yaavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaaley kanavu yaavum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil vandhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil vandhadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan thoongaiyil kaavalaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan thoongaiyil kaavalaai "/>
</div>
<div class="lyrico-lyrics-wrapper">kanneerinil kaadhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneerinil kaadhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En paadhaiyil thedalaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paadhaiyil thedalaai "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaangeren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaangeren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thoalgalil saaigiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thoalgalil saaigiren "/>
</div>
<div class="lyrico-lyrics-wrapper">urchaagamaai vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urchaagamaai vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thoalgalil saaigiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thoalgalil saaigiren "/>
</div>
<div class="lyrico-lyrics-wrapper">urchaagamaai vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urchaagamaai vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Eazhezhu jenmam thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eazhezhu jenmam thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">eduthaalum appoadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduthaalum appoadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un natpai naan kaetpen thoazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un natpai naan kaetpen thoazhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangai nadhi engeyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangai nadhi engeyo "/>
</div>
<div class="lyrico-lyrics-wrapper">pirandhu vandhadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirandhu vandhadhudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kaaviri aaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kaaviri aaru "/>
</div>
<div class="lyrico-lyrics-wrapper">engeyo pirandhu vandhadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyo pirandhu vandhadhadaa"/>
</div>
</pre>
