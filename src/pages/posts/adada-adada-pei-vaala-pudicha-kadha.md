---
title: "adada adada song lyrics"
album: "Pei Vaala Pudicha Kadha"
artist: "Arun Prasath"
lyricist: "Durai Siddarthan"
director: "Raasi Manivasakam"
path: "/albums/pei-vaala-pudicha-kadha-lyrics"
song: "Adada Adada"
image: ../../images/albumart/pei-vaala-pudicha-kadha.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qpF067QG7oI"
type: "mass"
singers:
  - VM Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adada adada poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada poda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mirugangal valgira nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mirugangal valgira nada"/>
</div>
<div class="lyrico-lyrics-wrapper">adada adada poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada poda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mirugangal valgira nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mirugangal valgira nada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ratham kothikuthu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratham kothikuthu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru manithanai alaithu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru manithanai alaithu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">ratham kothikuthu thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratham kothikuthu thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">oru manithanai alaithu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru manithanai alaithu vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sathiyam saaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyam saaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sarithiram veguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sarithiram veguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyam saaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyam saaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sarithiram veguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sarithiram veguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada adada poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada poda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mirugangal valgira nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mirugangal valgira nada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panathai ketu uyirai kudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panathai ketu uyirai kudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">paavigal rajiyam nadakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavigal rajiyam nadakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinathai pottu vayirai valarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinathai pottu vayirai valarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">aavigal naatinil irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavigal naatinil irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">panathai ketu uyirai kudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panathai ketu uyirai kudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">paavigal rajiyam nadakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavigal rajiyam nadakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinathai pottu vayirai valarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinathai pottu vayirai valarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">aavigal naatinil irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavigal naatinil irukuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei enakenna unakenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei enakenna unakenna"/>
</div>
<div class="lyrico-lyrics-wrapper">ithukoru mudivu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithukoru mudivu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">yei enakenna unakenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei enakenna unakenna"/>
</div>
<div class="lyrico-lyrics-wrapper">ithukoru mudivu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithukoru mudivu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">evanukum inge theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanukum inge theriyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithukenna kanakenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithukenna kanakenna"/>
</div>
<div class="lyrico-lyrics-wrapper">sathikoru mudivundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathikoru mudivundu"/>
</div>
<div class="lyrico-lyrics-wrapper">olikanum illana vidiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olikanum illana vidiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada adada poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada poda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mirugangal valgira nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mirugangal valgira nada"/>
</div>
<div class="lyrico-lyrics-wrapper">adada adada poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada adada poda"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mirugangal valgira nada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mirugangal valgira nada"/>
</div>
</pre>
