---
title: "naalo unna andhagaada song lyrics"
album: "Utthara"
artist: "Suresh bobbili"
lyricist: "Purna Chari"
director: "Thirupathi Sr"
path: "/albums/utthara-lyrics"
song: "Naalo Unna Andhagaada"
image: ../../images/albumart/utthara.jpg
date: 2020-01-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/D2DZVDwhy5o"
type: "love"
singers:
  - Nutana Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo unna andhagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo unna andhagada"/>
</div>
<div class="lyrico-lyrics-wrapper">neelo nannu chudavera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo nannu chudavera"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalloki jaru vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalloki jaru vela"/>
</div>
<div class="lyrico-lyrics-wrapper">idhem thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhem thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">mayadhari matakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayadhari matakari"/>
</div>
<div class="lyrico-lyrics-wrapper">matalthoti chera vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matalthoti chera vera"/>
</div>
<div class="lyrico-lyrics-wrapper">mansantha nindu koni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mansantha nindu koni "/>
</div>
<div class="lyrico-lyrics-wrapper">kathe maare ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathe maare ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ela ila nii useee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela ila nii useee"/>
</div>
<div class="lyrico-lyrics-wrapper">ureginchi pothuntee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ureginchi pothuntee"/>
</div>
<div class="lyrico-lyrics-wrapper">mari mari ni vaipe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari mari ni vaipe "/>
</div>
<div class="lyrico-lyrics-wrapper">ramantundhi ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ramantundhi ra"/>
</div>
<div class="lyrico-lyrics-wrapper">andhagada allukora 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhagada allukora "/>
</div>
<div class="lyrico-lyrics-wrapper">andhamantha allukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhamantha allukora"/>
</div>
<div class="lyrico-lyrics-wrapper">akaakayyi evvanale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akaakayyi evvanale "/>
</div>
<div class="lyrico-lyrics-wrapper">pilla galilona thela ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilla galilona thela ra"/>
</div>
<div class="lyrico-lyrics-wrapper">konte gada kanu viti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konte gada kanu viti"/>
</div>
<div class="lyrico-lyrics-wrapper">mata jare matram vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mata jare matram vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">nannu nake ivvakunda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu nake ivvakunda "/>
</div>
<div class="lyrico-lyrics-wrapper">nannu tisukellara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu tisukellara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh thalam vesi kallake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh thalam vesi kallake"/>
</div>
<div class="lyrico-lyrics-wrapper">dharam katti gundeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharam katti gundeke"/>
</div>
<div class="lyrico-lyrics-wrapper">ninne dhachu kunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne dhachu kunna "/>
</div>
<div class="lyrico-lyrics-wrapper">chotu kasta chuda ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chotu kasta chuda ra"/>
</div>
<div class="lyrico-lyrics-wrapper">sage padham inthaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sage padham inthaki "/>
</div>
<div class="lyrico-lyrics-wrapper">age chotu edhi ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="age chotu edhi ani"/>
</div>
<div class="lyrico-lyrics-wrapper">malli malli chusi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli malli chusi "/>
</div>
<div class="lyrico-lyrics-wrapper">pilli moggalu eseraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilli moggalu eseraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nijam neda theladhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijam neda theladhe"/>
</div>
<div class="lyrico-lyrics-wrapper">navve nannu vidadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navve nannu vidadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nedhe nedhe mayalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedhe nedhe mayalo "/>
</div>
<div class="lyrico-lyrics-wrapper">veeru dyasa em undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeru dyasa em undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">adham nanne chudanante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adham nanne chudanante"/>
</div>
<div class="lyrico-lyrics-wrapper">adham ninne chuda mante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adham ninne chuda mante"/>
</div>
<div class="lyrico-lyrics-wrapper">mukku midha koopam anthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukku midha koopam anthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">siggu povu ayyi pusindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siggu povu ayyi pusindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andhagada allukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhagada allukora"/>
</div>
<div class="lyrico-lyrics-wrapper">andham anthaallokora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andham anthaallokora"/>
</div>
<div class="lyrico-lyrics-wrapper">akaakayyi evvanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akaakayyi evvanale"/>
</div>
<div class="lyrico-lyrics-wrapper">pilla galilona thela ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilla galilona thela ra"/>
</div>
<div class="lyrico-lyrics-wrapper">konte gada kanu viti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konte gada kanu viti"/>
</div>
<div class="lyrico-lyrics-wrapper">mata jare matram vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mata jare matram vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">nannu nake ivvakunda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu nake ivvakunda "/>
</div>
<div class="lyrico-lyrics-wrapper">nannu tisukellara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu tisukellara"/>
</div>
</pre>
