---
title: "enna enna idhu song lyrics"
album: "Azhagumagan"
artist: "James Vasanthan"
lyricist: "Mohan Raj"
director: "Azhagan Selva"
path: "/albums/azhagumagan-lyrics"
song: "Enna Enna Idhu"
image: ../../images/albumart/azhagumagan.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IeR8BhsZqH4"
type: "love"
singers:
  - Jay
  - Sai Sutha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enna enna idhu then mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna idhu then mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">sinthuthu kaal varai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthuthu kaal varai "/>
</div>
<div class="lyrico-lyrics-wrapper">ponguthu poo nurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponguthu poo nurai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seivathu intha kathalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seivathu intha kathalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai naal varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai naal varai"/>
</div>
<div class="lyrico-lyrics-wrapper">yengidum soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengidum soolnilai"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu pirinthidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu pirinthidum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">thondruthu intha yuir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondruthu intha yuir "/>
</div>
<div class="lyrico-lyrics-wrapper">enathillai enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathillai enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thotu anaithidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotu anaithidum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannile chinna alagaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannile chinna alagaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu muttuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu muttuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vilai thanthum ithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vilai thanthum ithai"/>
</div>
<div class="lyrico-lyrics-wrapper">alli vaithu kolla aasai thotruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli vaithu kolla aasai thotruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna enna idhu then mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna idhu then mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">sinthuthu kaal varai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthuthu kaal varai "/>
</div>
<div class="lyrico-lyrics-wrapper">ponguthu poo nurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponguthu poo nurai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seivathu intha kathalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seivathu intha kathalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai naal varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai naal varai"/>
</div>
<div class="lyrico-lyrics-wrapper">yengidum soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengidum soolnilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poongatru theenda puthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poongatru theenda puthai"/>
</div>
<div class="lyrico-lyrics-wrapper">mannil vilunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil vilunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vanthu yentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vanthu yentha"/>
</div>
<div class="lyrico-lyrics-wrapper">puthithaaga elunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthithaaga elunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">oruthi neeradum karaiyidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthi neeradum karaiyidam"/>
</div>
<div class="lyrico-lyrics-wrapper">alaigalai eriyuthu kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigalai eriyuthu kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">naalathil thindadum manathidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalathil thindadum manathidam"/>
</div>
<div class="lyrico-lyrics-wrapper">anumathi kodukuthu udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anumathi kodukuthu udal"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyidam thirudiya kulirinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyidam thirudiya kulirinai"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyidam kuduthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyidam kuduthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalile karainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalile karainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna enna idhu then mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna idhu then mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">sinthuthu kaal varai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthuthu kaal varai "/>
</div>
<div class="lyrico-lyrics-wrapper">ponguthu poo nurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponguthu poo nurai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seivathu intha kathalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seivathu intha kathalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai naal varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai naal varai"/>
</div>
<div class="lyrico-lyrics-wrapper">yengidum soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengidum soolnilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaal pona pokil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal pona pokil "/>
</div>
<div class="lyrico-lyrics-wrapper">kana thooram ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kana thooram ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vantha pinnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vantha pinnal"/>
</div>
<div class="lyrico-lyrics-wrapper">thisai kaati aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisai kaati aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongatha un kannin imaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongatha un kannin imaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">varudida thudikuthu viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varudida thudikuthu viral"/>
</div>
<div class="lyrico-lyrics-wrapper">ketkatha poiyaana kuralinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkatha poiyaana kuralinai"/>
</div>
<div class="lyrico-lyrics-wrapper">isaiyena eluputhu idhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaiyena eluputhu idhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavukul eluthiya kadithathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavukul eluthiya kadithathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaivinil padithathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaivinil padithathu "/>
</div>
100 <div class="lyrico-lyrics-wrapper">murai thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murai thinamum"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu pirinthidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu pirinthidum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">thondruthu intha yuir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondruthu intha yuir "/>
</div>
<div class="lyrico-lyrics-wrapper">enathillai enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathillai enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thotu anaithidum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotu anaithidum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannile chinna alagaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannile chinna alagaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu muttuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu muttuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vilai thanthum ithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vilai thanthum ithai"/>
</div>
<div class="lyrico-lyrics-wrapper">alli vaithu kolla aasai thotruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli vaithu kolla aasai thotruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna enna idhu then mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enna idhu then mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">sinthuthu kaal varai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthuthu kaal varai "/>
</div>
<div class="lyrico-lyrics-wrapper">ponguthu poo nurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponguthu poo nurai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna seivathu intha kathalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna seivathu intha kathalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai naal varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai naal varai"/>
</div>
<div class="lyrico-lyrics-wrapper">yengidum soolnilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengidum soolnilai"/>
</div>
</pre>
