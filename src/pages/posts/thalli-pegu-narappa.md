---
title: "thalli pegu song lyrics"
album: "Narappa"
artist: "Mani Sharma"
lyricist: "Sirivennela Seetharama Sastry"
director: "Srikanth Addala"
path: "/albums/narappa-lyrics"
song: "Thalli Pegu"
image: ../../images/albumart/narappa.jpg
date: 2021-07-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FR47dI-ICgE"
type: "sad"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thalli Pegu chudu ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pegu chudu ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladillipoyenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladillipoyenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu moosi eto vellipokayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu moosi eto vellipokayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kanna thandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kanna thandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa ravayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa ravayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadupulona undaka ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadupulona undaka ee "/>
</div>
<div class="lyrico-lyrics-wrapper">pudamikela vasthivayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudamikela vasthivayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankanetthukunnantha sepu levayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankanetthukunnantha sepu levayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalone meayyavo cheppayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone meayyavo cheppayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakasi cheekatilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi cheekatilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye keedu thakindho ekaakivai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye keedu thakindho ekaakivai "/>
</div>
<div class="lyrico-lyrics-wrapper">poyava chandrayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyava chandrayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallera chesi reyini chudayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallera chesi reyini chudayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellari sooridalle raavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellari sooridalle raavayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee semata thadi inka inkaneledhayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee semata thadi inka inkaneledhayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee thotanantha thadimi sudayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee thotanantha thadimi sudayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee navvu leka gudu sinnaboyenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu leka gudu sinnaboyenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalone runamu theerey neekayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone runamu theerey neekayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattini chilchukochhe vitthanamai raavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattini chilchukochhe vitthanamai raavayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavuni kuda sampe satthuva needhayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavuni kuda sampe satthuva needhayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Asta dhikkulanni suttu muduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asta dhikkulanni suttu muduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittenatti sappuna intiki ravayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittenatti sappuna intiki ravayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Pegu chudu ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pegu chudu ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladillipoyenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladillipoyenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu moosi eto vellipokayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu moosi eto vellipokayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kanna thandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kanna thandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa ravayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa ravayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu dheeli chupelli masakesi poyelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu dheeli chupelli masakesi poyelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello manta ninnu chupena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello manta ninnu chupena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannodhili nuvvelli kudi katti pranale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannodhili nuvvelli kudi katti pranale"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa aashakinka aayuvu inka migilera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa aashakinka aayuvu inka migilera"/>
</div>
<div class="lyrico-lyrics-wrapper">Katika nijam needhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katika nijam needhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammalante kastam kada naayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammalante kastam kada naayana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakaina alladadhe chudayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaina alladadhe chudayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu rakunte gaaladadhe kannayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu rakunte gaaladadhe kannayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Pegu chudu ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pegu chudu ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladillipoyenayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladillipoyenayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu moosi eto vellipokayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu moosi eto vellipokayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu kanna thandri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu kanna thandri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa ravayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa ravayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadupulona undaka ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadupulona undaka ee "/>
</div>
<div class="lyrico-lyrics-wrapper">pudamikela vasthivayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudamikela vasthivayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankanetthukunnantha sepu levayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankanetthukunnantha sepu levayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalone meayyavo cheppayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone meayyavo cheppayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakasi cheekatilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi cheekatilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye keedu thakindho ekaakivai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye keedu thakindho ekaakivai "/>
</div>
<div class="lyrico-lyrics-wrapper">poyava chandrayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyava chandrayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallera chesi reyini chudayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallera chesi reyini chudayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellari sooridalle raavayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellari sooridalle raavayya"/>
</div>
</pre>
