---
title: "madura palapalakkuthu song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Yugabharathi"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Madura Palapalakkuthu"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3we0chTPqtE"
type: "happy"
singers:
  - Nivas K. Prasanna
  - Vijay Sethupathi
  - Priyanka Deshpande
  - Niranjana Ramanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yei Anga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Anga Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Inga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Inga Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Anga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Anga Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Inga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Inga Manakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Anga Manakka Inga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Anga Manakka Inga Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Manakka Inga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Manakka Inga Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Manakka Inga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Manakka Inga Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Manakka Inga Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Manakka Inga Manakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Thappadikka Thaviladikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thappadikka Thaviladikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottadikka Koothadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottadikka Koothadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthaattam Kai Thatta Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaattam Kai Thatta Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Kaathadikka Kathavadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Kaathadikka Kathavadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Idikka Vedi Vedikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Idikka Vedi Vedikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaattam Whistle Adikka Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaattam Whistle Adikka Aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vandhaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vandhaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppanna Saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppanna Saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thanmaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thanmaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraiyaadha Boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiyaadha Boomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaasa Patha Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasa Patha Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pammaatha Oththi Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pammaatha Oththi Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathaappa Suthi Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathaappa Suthi Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kithaappa Yethi Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kithaappa Yethi Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaattam Kotta Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaattam Kotta Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyilaattam Katta Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyilaattam Katta Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliyaattam Poda Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyaattam Poda Vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devarattam Aada Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devarattam Aada Vaiyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallapetti Kannukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallapetti Kannukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Katterumbu Sollukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katterumbu Sollukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyheyheyheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyheyheyheyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallapetti Kannukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallapetti Kannukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Katterumbu Sollukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katterumbu Sollukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpoorathu Pallukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpoorathu Pallukaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Pinna Dhillukaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Pinna Dhillukaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanannanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanannanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanna Thannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannanannanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannanannanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Ellaam Flex Vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Ellaam Flex Vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedha Vedhama Sangam Vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedha Vedhama Sangam Vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alumbaathaan Ezhuthuvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alumbaathaan Ezhuthuvaainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Alapparaiya Kaattuvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alapparaiya Kaattuvaainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaraiya Koottuvaainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaraiya Koottuvaainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukkaandhu Yosippaingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaandhu Yosippaingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Yosichittu Ukkaruvangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Yosichittu Ukkaruvangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkaandhu Yosippaingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaandhu Yosippaingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Yosichittu Ukkaruvangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Yosichittu Ukkaruvangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiruthadi Vaanavedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiruthadi Vaanavedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappungada Naattuvedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappungada Naattuvedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiruthadi Vaanavedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiruthadi Vaanavedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappungada Naattuvedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappungada Naattuvedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukkaandhu Yosippaingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaandhu Yosippaingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Yosichittu Ukkaruvangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Yosichittu Ukkaruvangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaane Thanna Nanna Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaane Thanna Nanna Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannananna Naana Naana Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannananna Naana Naana Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaane Thanna Nanna Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaane Thanna Nanna Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Thannananna Naana Naana Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Thannananna Naana Naana Ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Pechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Pechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Vanthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vanthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vetri Aattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vetri Aattatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Kuthu Kuthunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Kuthu Kuthunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthittu Irukaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthittu Irukaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Nandooda Nellu Veippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Nandooda Nellu Veippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Narigal Ooda Karumbu Veppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigal Ooda Karumbu Veppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandi Ooda Vaazha Veppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandi Ooda Vaazha Veppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Theru Ooda Thenna Veppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Ooda Thenna Veppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urutti Podu Uruvi Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urutti Podu Uruvi Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Therikka Kuthaattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Therikka Kuthaattatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Erakki Podu Izhuthu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erakki Podu Izhuthu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyum Parakka Karakaattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyum Parakka Karakaattatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kacheri Thaan Kalakkattuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kacheri Thaan Kalakkattuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Sanam Ada Sokkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Sanam Ada Sokkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Ellaam Onnu Kooduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Ellaam Onnu Kooduthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sokkaaliyaa Ada Suthuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sokkaaliyaa Ada Suthuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kacheri Thaan Kalakkattuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kacheri Thaan Kalakkattuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Sanam Ada Sokkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Sanam Ada Sokkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Ellaam Onnu Kooduthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Ellaam Onnu Kooduthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sokkaaliyaa Ada Suthuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sokkaaliyaa Ada Suthuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ennappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ennappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Kuthukke Intha Kuthukke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Kuthukke Intha Kuthukke"/>
</div>
<div class="lyrico-lyrics-wrapper">Naai Elacha Mathiri Elachukittu Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naai Elacha Mathiri Elachukittu Irukkom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Thookku Rasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thookku Rasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Amukku Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Amukku Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Thookku Rasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thookku Rasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Amukku Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Amukku Rani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Thookku Thookku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thookku Thookku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Amukku Amukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Amukku Amukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Thookku Thookku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thookku Thookku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Amukku Amukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Amukku Amukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Thooku Yei Amukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thooku Yei Amukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Thooku Yei Amukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Thooku Yei Amukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Rasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Rasaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Murukku Suttalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Murukku Suttalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pathalaiyaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pathalaiyaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nera Kadaikku Ponalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nera Kadaikku Ponalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vanganathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vanganathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyula Kaasu Pathalaiyaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyula Kaasu Pathalaiyaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thaan Ennatha Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thaan Ennatha Senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thaan Ennatha Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thaan Ennatha Senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu Muthatha Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Muthatha Vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rangu Sillukkuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Sillukkuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkunu Samanju Vanthalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkunu Samanju Vanthalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Haanhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haanhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangu Maamandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangu Maamandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Kolanju Ninnaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Kolanju Ninnaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Yaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Panguni Raathiri Niceah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panguni Raathiri Niceah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil Ponalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil Ponalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manguni Maamandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manguni Maamandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamthula Nadungi Ponanaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamthula Nadungi Ponanaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lightaah Anachiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lightaah Anachiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lightaah Anachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lightaah Anachikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Weightah Kaattiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightah Kaattiputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa Papa Pappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa Papa Pappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naarapaiyan Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarapaiyan Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachaan Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachaan Kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Koorapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Koorapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikitten Vasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikitten Vasamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kena Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kena Paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli Vachaan Ponnamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Vachaan Ponnamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Keerathanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Keerathanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikitten Nesamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikitten Nesamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela Veettu Chinnamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela Veettu Chinnamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Veettu Pattamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Veettu Pattamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthomaa Ponomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthomaa Ponomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkachakka Vambamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkachakka Vambamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nara Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara Paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara Paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Vachaan Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Vachaan Kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Koorapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Koorapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikitten Vasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikitten Vasamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kena Paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kena Paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli Vachaan Ponnamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli Vachaan Ponnamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Keerathanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Keerathanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikitten Nesamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikitten Nesamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madura Palapalakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura Palapalakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo Manamanakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo Manamanakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Malliyappoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Malliyappoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Malliyappoo Malliyappoo Malliyappoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliyappoo Malliyappoo Malliyappoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Malli Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Malli Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Malli Malli Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Malli Malli Malli"/>
</div>
</pre>
