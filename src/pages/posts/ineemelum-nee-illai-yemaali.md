---
title: "ineemelum nee illai song lyrics"
album: "Yemaali"
artist: "Sam D. Raj"
lyricist: "Mohan Rajan"
director: "V.Z. Durai"
path: "/albums/yemaali-lyrics"
song: "Ineemelum Nee Illai"
image: ../../images/albumart/yemaali.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/clA5SmvoSXA"
type: "love"
singers:
  - Velu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inimelum Nee Illai Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum Nee Illai Thani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ininaanum Naanillai Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ininaanum Naanillai Thani"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Maalai Nerampol Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maalai Nerampol Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithidumaa Ketkiren Honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithidumaa Ketkiren Honey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Paarkalaam Pesalaam Pazhagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Paarkalaam Pesalaam Pazhagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oer Kadhalaal Inaiyalaam Verkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oer Kadhalaal Inaiyalaam Verkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thee Thee Koduthaa Thee Erindhenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thee Thee Koduthaa Thee Erindhenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anu Kee Kee Uyir Thaaki Nuzhaindhaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anu Kee Kee Uyir Thaaki Nuzhaindhaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaanaval Thimiraanaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaanaval Thimiraanaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiraanaval Enaketraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiraanaval Enaketraval"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhiyaanaval Madhiyaadhaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiyaanaval Madhiyaadhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaadhaval Enai Saaithaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaadhaval Enai Saaithaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endhan Thozhiyaa Adhaiyum Thaandiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Thozhiyaa Adhaiyum Thaandiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Solvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennil Paadhiyaa Naan Unnil Meedhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennil Paadhiyaa Naan Unnil Meedhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhal Vidhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhal Vidhiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimelum Nee Illai Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum Nee Illai Thani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ininaanum Naanillai Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ininaanum Naanillai Thani"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Maalai Nerampol Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maalai Nerampol Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithidumaa Ketkiren Honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithidumaa Ketkiren Honey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil Ulla Kadhal Ellam Vizhi Orathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Ulla Kadhal Ellam Vizhi Orathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaanadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril Ulla Enkaadhalo Idhazh Eerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril Ulla Enkaadhalo Idhazh Eerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaanadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagum Aabathum Ondraaga Vandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagum Aabathum Ondraaga Vandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munnaal Nindradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munnaal Nindradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Pithan Enbaayo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Pithan Enbaayo "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Sithan Sonnaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Sithan Sonnaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Jithan Aaveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Jithan Aaveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aasai Ennaavadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasai Ennaavadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazh Meendum Ketkindradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Meendum Ketkindradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiraaga Muraikindradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiraaga Muraikindradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaalum Pidikindradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaalum Pidikindradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimelum Nee Illai Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum Nee Illai Thani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ininaanum Naanillai Thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ininaanum Naanillai Thani"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Maalai Nerampol Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maalai Nerampol Ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithidumaa Ketkiren Honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithidumaa Ketkiren Honey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Paarkalaam Pesalaam Pazhagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Paarkalaam Pesalaam Pazhagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oer Kadhalaal Inaiyalaam Verkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oer Kadhalaal Inaiyalaam Verkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thee Thee Koduthaa Thee Erindhenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thee Thee Koduthaa Thee Erindhenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anu Kee Kee Uyir Thaaki Nuzhaindhaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anu Kee Kee Uyir Thaaki Nuzhaindhaayadi"/>
</div>
</pre>
