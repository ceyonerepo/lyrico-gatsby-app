---
title: "gnyabagam varugiradha edm version song lyrics"
album: "Vishwaroopam 2"
artist: "Ghibran Muhammad"
lyricist: "Vairamuthu"
director: "Kamal Haasan"
path: "/albums/vishwaroopam-2-lyrics"
song: "Gnyabagam Varugiradha EDM Version"
image: ../../images/albumart/vishwaroopam-2.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PBy5Znx_lWY"
type: "mass"
singers:
  - Arvind Srinivas
  - Sarath Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gnyabagam Varugiradha?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnyabagam Varugiradha?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Theeyenru Therigiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Theeyenru Therigiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai Venrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai Venrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariththiram Padaiththavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariththiram Padaiththavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnyabagam Varugiradha?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnyabagam Varugiradha?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerinri Verillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerinri Verillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaarukkum Arasanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaarukkum Arasanillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadugal Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadugal Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkinra Pozhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkinra Pozhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrrukku Kaayamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrrukku Kaayamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yavan Yenru Ninaiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavan Yenru Ninaiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaikkandu Siriththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaikkandu Siriththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Velippadum Pudhu Suyaroobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velippadum Pudhu Suyaroobam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppukkup Pirandhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppukkup Pirandhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Valarndhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Valarndhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velippadum Pudhu Suyaroobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velippadum Pudhu Suyaroobam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vishwaroopam Vishwaroopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwaroopam Vishwaroopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwaroopam Vishwaroopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwaroopam Vishwaroopam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gnyabagam Varugiradha?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnyabagam Varugiradha?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Theeyenru Therigiradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Theeyenru Therigiradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai Venrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai Venrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariththiram Padaiththavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariththiram Padaiththavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnyabagam Varugiradha?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnyabagam Varugiradha?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerinri Verillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerinri Verillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaarukkum Arasanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaarukkum Arasanillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadugal Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadugal Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkinra Pozhudhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkinra Pozhudhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrrukku Kaayamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrrukku Kaayamillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yavan Yenru Ninaiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavan Yenru Ninaiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaikkandu Siriththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaikkandu Siriththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Velippadum Pudhu Suyaroobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velippadum Pudhu Suyaroobam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppukkup Pirandhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppukkup Pirandhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Valarndhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Valarndhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Velippadum Pudhu Suyaroobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velippadum Pudhu Suyaroobam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vishwaroopam Vishwaroopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwaroopam Vishwaroopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwaroopam Vishwaroopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwaroopam Vishwaroopam"/>
</div>
</pre>
