---
title: "yendi soodamani song lyrics"
album: "Pammal K Sambandam"
artist: "Deva"
lyricist: "Vaali"
director: "Moulee"
path: "/albums/pammal-k-sambandam-lyrics"
song: "Yendi Soodamani"
image: ../../images/albumart/pammal-k-sambandam.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ykYlAnvVb4g"
type: "sad"
singers:
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Valiya Paarthathundodeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Valiya Paarthathundodeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal Kanneer Thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal Kanneer Thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Naalum Vaarthathundodee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Naalum Vaarthathundodee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnnunaa Aan Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnnunaa Aan Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Engirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Engirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaithaan Kai Vaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaithaan Kai Vaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Kollurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Kollurathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Valiya Paarthathundodeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Valiya Paarthathundodeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthathundodeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathundodeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal Kanneer Thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal Kanneer Thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Naalum Vaarthathundodee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Naalum Vaarthathundodee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnnunaa Aan Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnnunaa Aan Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Engirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Engirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaithaan Kai Vaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaithaan Kai Vaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Kollurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Kollurathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Valiya Paarthathundodeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Valiya Paarthathundodeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Mela Kadhal Vachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Mela Kadhal Vachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Unakku Sonnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Unakku Sonnaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Nee Eichum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Nee Eichum Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Poruththu Ninnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Poruththu Ninnaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnmela Kuththam Edhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnmela Kuththam Edhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhaama Senjaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaama Senjaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ull Manasu Vellam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ull Manasu Vellam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneera Vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneera Vittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Anachu Kaiyai Kazhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Anachu Kaiyai Kazhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaayo Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaayo Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Kaalam Aaninpaavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Kaalam Aaninpaavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraadho Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraadho Pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Valiya Paarthathundodeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Valiya Paarthathundodeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittadha Meendum Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittadha Meendum Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbidutho Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbidutho Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottadha Meendum Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottadha Meendum Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondangiratho Ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondangiratho Ennam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poththi Poththi Vachaa Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poththi Poththi Vachaa Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaadhu Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadhu Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Neram Enna Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Neram Enna Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaadhu Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaadhu Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Kooda Kadhal Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Kooda Kadhal Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaadhu Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaadhu Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Patta Pinnaal Gnaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Patta Pinnaal Gnaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaacho Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaacho Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendi Soodaamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendi Soodaamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Valiya Paarthathundodeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Valiya Paarthathundodeee"/>
</div>
</pre>
