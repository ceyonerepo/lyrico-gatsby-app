---
title: "oh sone sone song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "Pa. Vijay - M. C. Vickey"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "Oh Sone Sone"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RVzkc0h-ouY"
type: "Entry"
singers:
  - Javed Ali
  - Priya Subramaniyan
  - M. C. Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jingu Jingu Mani Osthadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingu Jingu Mani Osthadayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Rimgu Ringu Look Chesthadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rimgu Ringu Look Chesthadayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Rangu Thonga Kaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Rangu Thonga Kaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesthadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesthadayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jillu Jilluga Choosthadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillu Jilluga Choosthadayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Gilli Champesthadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Gilli Champesthadayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillu Dhilluga Kallu Kallumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillu Dhilluga Kallu Kallumani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisthadayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisthadayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Hello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Hello"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move It Like A Rolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It Like A Rolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Marum Bothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Marum Bothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Singan Machan Inga Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singan Machan Inga Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala Vechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vechan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Never Messing With Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never Messing With Him"/>
</div>
<div class="lyrico-lyrics-wrapper">He Be Rolling You All
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Be Rolling You All"/>
</div>
<div class="lyrico-lyrics-wrapper">International Dabber
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="International Dabber"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing In With Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing In With Him"/>
</div>
<div class="lyrico-lyrics-wrapper">Song A With Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Song A With Him"/>
</div>
<div class="lyrico-lyrics-wrapper">Fast-ah Tight-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fast-ah Tight-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Sonic"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">VaaMaanae Nanae Energy Tonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="VaaMaanae Nanae Energy Tonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Seivaen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Seivaen Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Unique
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Unique"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhada Onnu Dhaan Pottu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Onnu Dhaan Pottu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vida Kondana Varuvaenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vida Kondana Varuvaenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Sattunu Theepudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Sattunu Theepudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta Vangi Naa Nenjanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Vangi Naa Nenjanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thera Bytela Tension Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thera Bytela Tension Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Terror Aagave Nippenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terror Aagave Nippenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Paathukka En Mogathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Paathukka En Mogathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Pakka Singam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Pakka Singam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Maanae Nanae Energy Tonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maanae Nanae Energy Tonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Seivaen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Seivaen Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Unique
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Unique"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathiriyum Robberyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathiriyum Robberyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Sernthu Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manda Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manda Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neurons Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neurons Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho Onna Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Onna Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathiriyum Robberyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathiriyum Robberyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Sernthu Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manda Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manda Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neurons Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neurons Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho Onna Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Onna Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Saakasuraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Saakasuraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paada Varaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paada Varaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Masakkapurayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakkapurayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Aaduven Theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aaduven Theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Jollyah Jolliah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Jollyah Jolliah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Maanae Nanae Energy Tonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maanae Nanae Energy Tonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Seivaen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Seivaen Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Unique
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Unique"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Othhakannum Unna Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Othhakannum Unna Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ultra Scan Pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ultra Scan Pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan Enga Oodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Enga Oodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaalum En GPS-um Minnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaalum En GPS-um Minnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithan Namma Angam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithan Namma Angam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Seendina Bangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Seendina Bangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaam Oru Sangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaam Oru Sangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Aayiram Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aayiram Singam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaatula Maatuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaatula Maatuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Maanae Nanae Energy Tonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maanae Nanae Energy Tonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei Sone Sone Super Sonic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Sone Sone Super Sonic"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Seivaen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Seivaen Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Unique
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Unique"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhada Onnu Dhaan Pottu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Onnu Dhaan Pottu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vida Kondana Varuvaenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vida Kondana Varuvaenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Sattunu Theepudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Sattunu Theepudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta Vangi Naa Nenjanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta Vangi Naa Nenjanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thera Bytela Tension Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thera Bytela Tension Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Terror Aagave Nippenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terror Aagave Nippenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Paathukka En Mogathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Paathukka En Mogathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Pakka Singam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Pakka Singam Da"/>
</div>
</pre>
