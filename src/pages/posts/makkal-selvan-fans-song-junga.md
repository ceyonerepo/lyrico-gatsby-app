---
title: "makkal selvan fans song song lyrics"
album: "Junga"
artist: "Siddharth Vipin"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/junga-lyrics"
song: "Makkal Selvan Fans Song"
image: ../../images/albumart/junga.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Go7fW6XpL6c"
type: "mass"
singers:
  - Suraj Jagan
  - Jagadeesh Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aattatha paar-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattatha paar-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei dei athiradi-ah paar-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei dei athiradi-ah paar-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukuravan yaar-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukuravan yaar-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna sonna kel-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna sonna kel-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera maari aal-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera maari aal-raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoranai paar-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranai paar-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna thottadhu ellaam dhool-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna thottadhu ellaam dhool-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma aalu paar-raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma aalu paar-raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasiganai rasikkum thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasiganai rasikkum thalaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkal selvan vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal selvan vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vetri mugan vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vetri mugan vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vella yevan vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vella yevan vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru pottrum thalaiva vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru pottrum thalaiva vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa vaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa vaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makkal selvan vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal selvan vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa singa tamizha vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa singa tamizha vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa sanga tamizha vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa sanga tamizha vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum ucham thoda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum ucham thoda vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh junga junga aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh junga junga aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh junga junga aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh junga junga aaaaaa"/>
</div>
</pre>
