---
title: "yaar azhaippadhu lyrics"
album: "Maara"
artist: "Ghibran"
lyricist: "Thamarai"
director: "Dhilip Kumar"
path: "/albums/maara-song-lyrics"
song: "Yaar Azhaippadhu"
image: ../../images/albumart/maara.jpg
date: 2021-01-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r9L5mfGSGTg"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Yaar azhaippathu yaar azhaippathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar azhaippathu yaar azhaippathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kural idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kural idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatharuginil kaatharuginil
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatharuginil kaatharuginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yean olikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yean olikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Po ena adhai naan thurathida
<input type="checkbox" class="lyrico-select-lyric-line" value="Po ena adhai naan thurathida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai marukkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaai marukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralin viralai pidithu thodarathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuralin viralai pidithu thodarathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Udalin narampugal
<input type="checkbox" class="lyrico-select-lyric-line" value="Udalin narampugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal kairu aagumaa raaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Oonjal kairu aagumaa raaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai paravasamaakki
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirai paravasamaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaikkuma aariroo raaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Isaikkuma aariroo raaroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mazhai vidaathu vara adaathi
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai vidaathu vara adaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda thaegam nanayum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoda thaegam nanayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam ulaavi vara alaadhi idam thaedum….ooho
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam ulaavi vara alaadhi idam thaedum….ooho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaar azhaippathu yaar azhaippathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar azhaippathu yaar azhaippathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kural idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kural idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralin viralai pidithu thodarathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuralin viralai pidithu thodarathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Saerum varai pogum idam theriyaathanil
<input type="checkbox" class="lyrico-select-lyric-line" value="Saerum varai pogum idam theriyaathanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai tharum perinbam verulladhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Bodhai tharum perinbam verulladhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi varai kekkum kadhai mudiyaathanil
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadhi varai kekkum kadhai mudiyaathanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi kadhai thedaamal yaar solluvaar
<input type="checkbox" class="lyrico-select-lyric-line" value="Meedhi kadhai thedaamal yaar solluvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kalaivaar avarellam tholaivaar
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalaivaar avarellam tholaivaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanam thavaru alaivaar avardhaney adaivaar
<input type="checkbox" class="lyrico-select-lyric-line" value="Vasanam thavaru alaivaar avardhaney adaivaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar adaiyum pudhayal perithu….
<input type="checkbox" class="lyrico-select-lyric-line" value="Avar adaiyum pudhayal perithu…."/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaatha nadoodi kaatrallava
<input type="checkbox" class="lyrico-select-lyric-line" value="Adangaatha nadoodi kaatrallava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaar azhaippathu yaar azhaippathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar azhaippathu yaar azhaippathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kural idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kural idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatharuginil kaatharuginil
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatharuginil kaatharuginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yean olikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yean olikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Po ena adhai naan thurathida
<input type="checkbox" class="lyrico-select-lyric-line" value="Po ena adhai naan thurathida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai marukkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaai marukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralin viralai pidithu thodarathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuralin viralai pidithu thodarathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Payanam nigalgira paadhai muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Payanam nigalgira paadhai muzhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Medaiyaai maarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Medaiyaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarum arimugam illai eninum
<input type="checkbox" class="lyrico-select-lyric-line" value="Evarum arimugam illai eninum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadagam oodum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naadagam oodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vidai illaatha pala vinaavum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidai illaatha pala vinaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezha thedal thodangum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezha thedal thodangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilai illaadha oru vinoodha sugam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilai illaadha oru vinoodha sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrum….ohhh
<input type="checkbox" class="lyrico-select-lyric-line" value="Thondrum….ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaar azhaippathu yaar azhaippathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar azhaippathu yaar azhaippathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kural idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kural idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralin viralai pidithu thodarathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuralin viralai pidithu thodarathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thudikkuthu"/>
</div>
</pre>
