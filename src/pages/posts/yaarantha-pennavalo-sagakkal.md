---
title: "yaarantha pennavalo song lyrics"
album: "Sagakkal"
artist: "Thayarathnam"
lyricist: "Daya Rathnam"
director: "L. Muthukumaraswamy"
path: "/albums/sagakkal-lyrics"
song: "Yaarantha Pennavalo"
image: ../../images/albumart/sagakkal.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_JY9MY31Ypw"
type: "love"
singers:
  - Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar andha pennavalo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar andha pennavalo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">oorenna perum enna ariyeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorenna perum enna ariyeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar andha pennavalo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar andha pennavalo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">oorenna perum enna ariyeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorenna perum enna ariyeney"/>
</div>
<div class="lyrico-lyrics-wrapper">malligaiyapoala mella sirithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malligaiyapoala mella sirithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">mellisaiyaai pesi nenjaipparippaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mellisaiyaai pesi nenjaipparippaal"/>
</div>
<div class="lyrico-lyrics-wrapper">konji kalappaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji kalappaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar andha pennavalo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar andha pennavalo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">oorenna perum enna ariyeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorenna perum enna ariyeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sekka sevandha nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekka sevandha nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee irundhaal uchchi melum kuliru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee irundhaal uchchi melum kuliru"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam irukka vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam irukka vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku oru sorkkam thoazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku oru sorkkam thoazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam namakku parandhu kedakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam namakku parandhu kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkum enakkum thanimai edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkum enakkum thanimai edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai eppozhudhum en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai eppozhudhum en "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal puththagamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal puththagamey"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkangal irukkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkangal irukkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un pechu salikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pechu salikkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un pechu salikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pechu salikkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meththa maadiyil paduththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththa maadiyil paduththu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku oru kutti kadhaiyum sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku oru kutti kadhaiyum sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaththoam thagida thoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaththoam thagida thoam"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu vizhi thaalam poadum allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu vizhi thaalam poadum allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">peritchai vidhaiyaai nilathil vizhundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peritchai vidhaiyaai nilathil vizhundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku mazhaiyaai eppoadhu varuvaayO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku mazhaiyaai eppoadhu varuvaayO"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ennai thittuvadhum naan vandhu ottuvadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ennai thittuvadhum naan vandhu ottuvadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellakkul irukkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellakkul irukkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasai kuraiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasai kuraiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasai kuraiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasai kuraiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyaadhey ariyeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyaadhey ariyeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar andha pennavalo theriyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar andha pennavalo theriyaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">oorenna perum enna ariyeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorenna perum enna ariyeney"/>
</div>
</pre>
