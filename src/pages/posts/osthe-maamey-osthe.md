---
title: "osthe maamey song lyrics"
album: "Osthe"
artist: "Sai Thaman"
lyricist: "Vaali"
director: "S. Dharani"
path: "/albums/osthe-lyrics"
song: "Osthe Maamey"
image: ../../images/albumart/osthe.jpg
date: 2011-12-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/loReWvwsDOo"
type: "happy"
singers:
  - S. Thaman
  - Baba Sehgal
  - Rahul Nambiar
  - Ranjith
  - Naveen Madhav
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tamil naatu cop dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil naatu cop dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Darani elam top dhaan osthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darani elam top dhaan osthee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thupakiyil thottadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thupakiyil thottadhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa eduthu potta dhaan osthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa eduthu potta dhaan osthee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha bollywood
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha bollywood"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kollywood
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kollywood"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana robinhood
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana robinhood"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthana da maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthana da maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edupen robber kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edupen robber kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodupen labour kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodupen labour kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukum thuninja katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukum thuninja katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthana da osthee maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthana da osthee maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamil naatu cop dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil naatu cop dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Darani elam top dhaan osthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darani elam top dhaan osthee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thupakiyil thottadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thupakiyil thottadhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa eduthu potta dhaan osthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa eduthu potta dhaan osthee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartha thunda jackie chanin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha thunda jackie chanin"/>
</div>
<div class="lyrico-lyrics-wrapper">Police story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police story"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthirundha sollum adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthirundha sollum adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma seidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma seidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolla panam alli varan mollamaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla panam alli varan mollamaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Konduvanthu serka poran kuppam seri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konduvanthu serka poran kuppam seri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum thappu thanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum thappu thanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Panna kanda opathaavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna kanda opathaavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu appurama uppu kandanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu appurama uppu kandanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumbu kanna pinna kandam thundam dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumbu kanna pinna kandam thundam dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thaan inspector-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan inspector-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum encounter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum encounter-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethilum all rounder-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethilum all rounder-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennalum dhaan maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum dhaan maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edupen revolver-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edupen revolver-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudipen dubakuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudipen dubakuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arupen danguvaruArnold naan dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arupen danguvaruArnold naan dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamil naatu cop dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil naatu cop dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Darani elam top dhaan osthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darani elam top dhaan osthee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada annatha dhaan aaduraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada annatha dhaan aaduraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Othiko othiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othiko othiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sakka podu poduvaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sakka podu poduvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Othuko othuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othuko othuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada annatha dhaan aaduraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada annatha dhaan aaduraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathuko paathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathuko paathuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Terror-u nu vanthu ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terror-u nu vanthu ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Terrror ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terrror ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhar-u nu vanthu ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhar-u nu vanthu ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhar ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhar ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaa pola una kaatum mirror ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaa pola una kaatum mirror ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pola innoruthan ingae evan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pola innoruthan ingae evan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaa kallerinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaa kallerinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundaagathu kannadi dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundaagathu kannadi dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku endrum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku endrum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham micham dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham micham dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruku ennudaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku ennudaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham ucham dan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham ucham dan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru echarikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru echarikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enpeyar ucharikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enpeyar ucharikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhadu theepidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhadu theepidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koopunudhaan maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koopunudhaan maamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rowdi ooril undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rowdi ooril undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Police ooril undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police ooril undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdi police undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rowdi police undaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nnaandhaan adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nnaandhaan adhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae osthi maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Osthi maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osthi maamae"/>
</div>
</pre>
