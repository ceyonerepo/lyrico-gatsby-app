---
title: "kaanal neer aanathe song lyrics"
album: "Velan"
artist: "Gopi Sundar"
lyricist: "Lalithanad"
director: "Kavin"
path: "/albums/velan-lyrics"
song: "Kaanal Neer Aanathe"
image: ../../images/albumart/velan.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tG8wIDUYVCM"
type: "sad"
singers:
  - Mugen Rao
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaanal neer aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal neer aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhale en kaadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhale en kaadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thoovi kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thoovi kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaarkkiren tharai vaarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaarkkiren tharai vaarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai serum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai serum nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai meeri ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai meeri ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai maari kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai maari kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekhai ooruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekhai ooruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru noolil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru noolil naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vidamaatte soolan unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vidamaatte soolan unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithai maattrakkoor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithai maattrakkoor"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo yaaraalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo yaaraalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanal neer aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal neer aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhale en kaadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhale en kaadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer thoovi kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer thoovi kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaarkkiren tharai vaarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaarkkiren tharai vaarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraatha soonilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraatha soonilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nernthaale aattrilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nernthaale aattrilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraatha vaazhvillai yeradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraatha vaazhvillai yeradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal soolntha neeril vaalvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal soolntha neeril vaalvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manal veedu thaane kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manal veedu thaane kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marinthaal unnaal unnai nyaabakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marinthaal unnaal unnai nyaabakam"/>
</div>
</pre>
