---
title: "kadhal enbadhai song lyrics"
album: "Kullanari Koottam"
artist: "V. Selvaganesh"
lyricist: "Surya Prabhakar"
director: "Sribalaji"
path: "/albums/kullanari-koottam-lyrics"
song: "Kadhal Enbadhai"
image: ../../images/albumart/kullanari-koottam.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0p9xxA-bg70"
type: "sad"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal Enbadhai Kadhal Enbadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadhai Kadhal Enbadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathil Koduthu Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathil Koduthu Vittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Enbadhai Kadhal Enbadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadhai Kadhal Enbadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil Solli Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil Solli Vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Unnai Ninaithaal Poovaagum Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Unnai Ninaithaal Poovaagum Manadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennulle Irukkum Uyir Kooda Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulle Irukkum Uyir Kooda Unadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Swasam Koduthaai Manmeedhu Vazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Swasam Koduthaai Manmeedhu Vazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Ondre Podhum Vinnai Naan Azha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Ondre Podhum Vinnai Naan Azha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nenjukulla Sandhosam Iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nenjukulla Sandhosam Iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Adhisayam Uyir Mattum Nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Adhisayam Uyir Mattum Nadakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Enbadhai Kadhal Enbadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadhai Kadhal Enbadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathil Koduthu Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathil Koduthu Vittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Enbadhai Kadhal Enbadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadhai Kadhal Enbadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil Solli Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil Solli Vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne Nadandha Padhayayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Nadandha Padhayayai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kalgal Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kalgal Thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Partha Idamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha Idamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paarvai Parkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvai Parkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivendru Ninaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivendru Ninaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyaamal Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyaamal Ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil Ne Irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil Ne Irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thottu Anaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thottu Anaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivendru Ninaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivendru Ninaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyaamal Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyaamal Ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil Ne Irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil Ne Irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thottu Anaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thottu Anaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nenjukulla Sandhosam Iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nenjukulla Sandhosam Iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Adhisayam Uyir Mattum Nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Adhisayam Uyir Mattum Nadakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Enbadhai Kadhal Enbadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enbadhai Kadhal Enbadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathil Koduthu Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathil Koduthu Vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupin Thooram Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupin Thooram Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerodu Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhiyin Thooram Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiyin Thooram Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu Mudiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrin Thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin Thooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhal Thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Thooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnulle Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnulle Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrin Thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin Thooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhal Thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Thooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnulle Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnulle Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Nenjukulla Sandhosam Iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nenjukulla Sandhosam Iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Enna Adhisayam Uyir Mattum Nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Adhisayam Uyir Mattum Nadakka"/>
</div>
</pre>
