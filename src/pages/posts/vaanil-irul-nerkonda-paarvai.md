---
title: 'vaanil irul song lyrics'
album: 'Nerkonda Paarvai'
artist: 'Yuvan Shankar Raja'
lyricist: 'Uma Devi'
director: 'H.Vinoth'
path: '/albums/comali-song-lyrics'
song: 'Vaanil Irul'
image: ../../images/albumart/nerkonda-paarvai.jpg
date: 2019-08-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dwm_ThnOD94"
type: 'sad'
singers: 
- Dhee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Vaanil irul soozhumbothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanil irul soozhumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum minnal thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnum minnal thunaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum serumbothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum neeyum serumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiyaagidumae vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidaiyaagidumae vaazhvae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veezhadhadha veezhadhadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Veezhadhadha veezhadhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyaalum siraigal veezhadhadhaagumo
<input type="checkbox" class="lyrico-select-lyric-line" value="Unaiyaalum siraigal veezhadhadhaagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraadhatha aaraadhadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraadhatha aaraadhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyae thunaiyaai nee maatridu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Unaiyae thunaiyaai nee maatridu…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vidhigal thaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhigal thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil aadum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalil aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulgal keeri oligal paayum
<input type="checkbox" class="lyrico-select-lyric-line" value="Irulgal keeri oligal paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan andha kathiraagiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan andha kathiraagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Agandru odum
<input type="checkbox" class="lyrico-select-lyric-line" value="Agandru odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhigal aagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadhigal aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruvi paadum kadhaigalaagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Aruvi paadum kadhaigalaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan indha nilamaagiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan indha nilamaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pizhaigalin kolangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Pizhaigalin kolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">En tholil thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="En tholil thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigalin vari ingu yaardhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Sarigalin vari ingu yaardhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakakkaadha kaadellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirakakkaadha kaadellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pookaathu pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poo pookaathu pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaanil irul soozhumbothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaanil irul soozhumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum minnal thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Minnum minnal thunaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum serumbothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum neeyum serumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiyaagidumae vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidaiyaagidumae vaazhvae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veezhadhadha veezhadhadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Veezhadhadha veezhadhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyaalum siraigal veezhadhadhaagumo
<input type="checkbox" class="lyrico-select-lyric-line" value="Unaiyaalum siraigal veezhadhadhaagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraadhatha aaraadhadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraadhatha aaraadhadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyae thunaiyaai nee maatridu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Unaiyae thunaiyaai nee maatridu…"/>
</div>
</pre>