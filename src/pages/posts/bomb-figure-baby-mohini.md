---
title: "bomb figure baby song lyrics"
album: "Mohini"
artist: "	Vivek-Mervin"
lyricist: "Samuel Deepak - Switche"
director: "Ramana Madhesh"
path: "/albums/mohini-lyrics"
song: "Bomb Figure Baby"
image: ../../images/albumart/mohini.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/490WJn6pZBI"
type: "love"
singers:
  - Benny Dayal
  - Sanjana
  - Switche
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bomb figure baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomb figure baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh naatu lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh naatu lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s get this party 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s get this party "/>
</div>
<div class="lyrico-lyrics-wrapper">ready steady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ready steady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomb figure baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomb figure baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh naatu lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh naatu lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Drop it like
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drop it like"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomb figure baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomb figure baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh naatu lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh naatu lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s get this party 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s get this party "/>
</div>
<div class="lyrico-lyrics-wrapper">ready steady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ready steady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice vekkira vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice vekkira vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkira heartukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkira heartukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hype yethura yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hype yethura yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethura oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethura oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice vekkira vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice vekkira vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkira heartukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkira heartukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hype yethura yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hype yethura yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Drop it like
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drop it like"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trisha illena divya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trisha illena divya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un boya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un boya"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa un toya?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa un toya?"/>
</div>
<div class="lyrico-lyrics-wrapper">Make a guy go crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make a guy go crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">You know they’ll be screamin hosanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know they’ll be screamin hosanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethila pottu pothysu pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethila pottu pothysu pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saree ah kattu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saree ah kattu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Get that azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get that azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">That sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">That madippu one point
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That madippu one point"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo curves and yo moves 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo curves and yo moves "/>
</div>
<div class="lyrico-lyrics-wrapper">like mick jagger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="like mick jagger"/>
</div>
<div class="lyrico-lyrics-wrapper">On point on point
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On point on point"/>
</div>
<div class="lyrico-lyrics-wrapper">on point on point
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="on point on point"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bring it back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bring it back"/>
</div>
<div class="lyrico-lyrics-wrapper">Hig heels-ah paatha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hig heels-ah paatha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangover aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangover aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum aadama steadiya ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum aadama steadiya ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Makeup-eh podama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makeup-eh podama"/>
</div>
<div class="lyrico-lyrics-wrapper">Wake up aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wake up aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakup-eh aagama paathukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breakup-eh aagama paathukuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saturday night-u aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saturday night-u aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Party pannuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party pannuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada 6 mani aana dhaan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada 6 mani aana dhaan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Address theduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Address theduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bass konjam yethi yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bass konjam yethi yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Music poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Music poduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma podum sound-u 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma podum sound-u "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">London-eh aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="London-eh aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomb figure baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomb figure baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh naatu lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh naatu lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s get this party 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s get this party "/>
</div>
<div class="lyrico-lyrics-wrapper">ready steady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ready steady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice vekkira vekkira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice vekkira vekkira "/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkira heartukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkira heartukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hype yethura yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hype yethura yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethura oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethura oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice vekkira vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice vekkira vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkira heartukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkira heartukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hype yethura yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hype yethura yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Drop it like
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drop it like"/>
</div>
</pre>
