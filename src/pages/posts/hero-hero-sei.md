---
title: "hero hero song lyrics"
album: "Sei"
artist: "NyX Lopez"
lyricist: "Yuga Bharathih"
director: "Raj Babu"
path: "/albums/sei-song-lyrics"
song: "Hero Hero"
image: ../../images/albumart/sei.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UwSR0fPHd_E"
type: "happy"
singers:
  - Shankar Mahadevan
  - Chinnaponnu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey hit adikkum hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hit adikkum hero hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku naanae potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku naanae potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduppenae naan paetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppenae naan paetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajini sir-ah pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajini sir-ah pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappum en style-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappum en style-uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadippil naan puyalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadippil naan puyalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hollywood-um poiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hollywood-um poiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppannae kaalu kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppannae kaalu kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha vanguvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha vanguvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oscar-uh aaru ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oscar-uh aaru ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Video call-ah paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Video call-ah paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda face face
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda face face"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook-ah kettu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook-ah kettu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthanae maassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthanae maassu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En cut outtu nikkummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En cut outtu nikkummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai achi adichi vikkummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai achi adichi vikkummae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai kulam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai kulam than"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ullathilae veikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ullathilae veikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Whistle-uh satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistle-uh satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchi vaanathaiyum ettummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi vaanathaiyum ettummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae inimae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae inimae "/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae inimaeinimae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae inimaeinimae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hit adikkum hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hit adikkum hero hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku naanae potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku naanae potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduppenae naan paetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppenae naan paetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajini sir-ah pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajini sir-ah pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappum en style-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappum en style-uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadippil naan puyalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadippil naan puyalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hollywood-um poiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hollywood-um poiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppannae kaalu kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppannae kaalu kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Getha vanguvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getha vanguvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oscar-uh aaru ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oscar-uh aaru ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En cut outtu nikkummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En cut outtu nikkummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai achi adichi vikkummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai achi adichi vikkummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai kulam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai kulam than"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ullathilae veikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ullathilae veikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Whistle-uh satham mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistle-uh satham mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiraiyila naanum theriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraiyila naanum theriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruthaiya maarum janamdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruthaiya maarum janamdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadigaigal enna orassa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadigaigal enna orassa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravummae nitham vathanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravummae nitham vathanthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasoola naan alli kuvikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasoola naan alli kuvikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Osarummae vangi kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osarummae vangi kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura pol ennai nenaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura pol ennai nenaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasigar than sondham enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasigar than sondham enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kai kaasu thanthu thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kai kaasu thanthu thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Call sheettukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call sheettukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala company thoothu anuppum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala company thoothu anuppum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thina sari thina sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thina sari thina sari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thina sari thina sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thina sari thina sari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha sirusuga sirusuga mansula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha sirusuga sirusuga mansula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan edathaiyum pudippen mudivula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan edathaiyum pudippen mudivula"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru agila ulaga nadigaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru agila ulaga nadigaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru vennae perumperu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru vennae perumperu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">En cut outtu nikkummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En cut outtu nikkummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai achi adichi vikkummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai achi adichi vikkummae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai kulam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai kulam than"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ullathilae veikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ullathilae veikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Whistle-uh satham mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistle-uh satham mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heii heii heii heii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heii heii heii heii"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hit adikkum hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hit adikkum hero hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku naanae potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku naanae potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduppenae naan paetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppenae naan paetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hit adikkum hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hit adikkum hero hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku naanae potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku naanae potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduppenae naan paetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppenae naan paetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heii heii heii heiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heii heii heii heiii"/>
</div>
</pre>