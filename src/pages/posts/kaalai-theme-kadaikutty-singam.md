---
title: "kaalai theme song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Kaalai Theme"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hTAacElzEHk"
type: "theme"
singers:
  - Nivas
  - Sai Vignesh
  - TS Sarath
  - Narayanan
  - Jithin Raj
  - Vignesh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaalai kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai mayilakaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai mayilakaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai mayilakaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai mayilakaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai kaalai mayilakaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai kaalai mayilakaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illa sevalakaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illa sevalakaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela mela yegirum kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela mela yegirum kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Merandudaatha oyila kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merandudaatha oyila kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalai aalai adhattunkaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalai aalai adhattunkaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamilla murattu kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamilla murattu kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda oda asathum kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda oda asathum kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora innum osathum kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora innum osathum kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam mariyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam mariyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha kaakka vantha kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha kaakka vantha kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ooravaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ooravaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolasaami intha kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolasaami intha kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam koraiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam koraiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyera vellum kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veriyera vellum kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema therandaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema therandaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meralaama sellum kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meralaama sellum kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peru pugaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru pugaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusaagum naama kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusaagum naama kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai kazhuthera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai kazhuthera"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaadum vetri kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaadum vetri kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peru pugaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru pugaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">Perusaagum naama kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perusaagum naama kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai kazhuthera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai kazhuthera"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyaadum vetri kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyaadum vetri kaalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo oooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo oooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai kaalai"/>
</div>
</pre>
