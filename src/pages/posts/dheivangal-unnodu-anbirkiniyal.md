---
title: "dheivangal unnodu song lyrics"
album: "Anbirkiniyal"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/anbirkiniyal-lyrics"
song: "Dheivangal Unnodu"
image: ../../images/albumart/anbirkiniyal.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5S68ftnCsOI"
type: "Motivational"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dheivangal unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivangal unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">penne nil thembodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nil thembodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thunive thunaiyai pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunive thunaiyai pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kambeeram unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambeeram unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaandeebam kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaandeebam kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">arive aranaai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arive aranaai aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilugira moongilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilugira moongilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">maaridatha neeyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaridatha neeyume"/>
</div>
<div class="lyrico-lyrics-wrapper">aerugira aeniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aerugira aeniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum maarum nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum maarum nerame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thurumbaiyum thoongalaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurumbaiyum thoongalaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">pathukappu aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathukappu aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarangal thoorum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarangal thoorum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">veelthidathu vaaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelthidathu vaaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karuvil irunthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvil irunthai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimai enna puthitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimai enna puthitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paniyin kudathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyin kudathil"/>
</div>
<div class="lyrico-lyrics-wrapper">piranthai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piranthai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">suvargal enna valiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvargal enna valiyatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mothi paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothi paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">thisaigal thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisaigal thirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaigal unaku ethum peritha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigal unaku ethum peritha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veelven endra ninaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelven endra ninaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">vithiye ennum kelvi elum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithiye ennum kelvi elum"/>
</div>
<div class="lyrico-lyrics-wrapper">velvi polutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velvi polutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dheivangal unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivangal unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">penne nil thembodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nil thembodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thunive thunaiyai pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunive thunaiyai pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kambeeram unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambeeram unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kandeebam kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandeebam kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">arive aranaai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arive aranaai aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dheivangal unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivangal unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">penne nil thembodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nil thembodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thunive thunaiyai pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunive thunaiyai pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kambeeram unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambeeram unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">kandeebam kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandeebam kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">arive aranaai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arive aranaai aagum"/>
</div>
</pre>
