---
title: "yetikokka poota song lyrics"
album: "Tuck Jagadish"
artist: "S. Thaman"
lyricist: "Kalyan Chakravarthy"
director: "Shiva Nirvana"
path: "/albums/tuck-jagadish-lyrics"
song: "Yetikokka Poota"
image: ../../images/albumart/tuck-jagadish.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JEGJCEUQ0zs"
type: "happy"
singers:
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yetikokka poota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetikokka poota"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaanaadhi paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanaadhi paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayudori nota nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayudori nota nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhindhe maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhindhe maata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muddhapasupai kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhapasupai kurise"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchhataina bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchhataina bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuku raayalenidhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku raayalenidhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma muddhu paasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma muddhu paasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannapegu panchukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannapegu panchukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna garu thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna garu thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaselleli selabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaselleli selabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma gillaneedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma gillaneedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angisuttu madathesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angisuttu madathesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi sedu vadabosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi sedu vadabosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu muttukuntaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttu muttukuntaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttamalle kaapesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttamalle kaapesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erraleruvaga mesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erraleruvaga mesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Errabadda bhoodevi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Errabadda bhoodevi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurragaali thagilaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurragaali thagilaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu thersukunnaadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu thersukunnaadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu choosi nikaramga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choosi nikaramga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rommu idsukunnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rommu idsukunnaadi"/>
</div>
</pre>
