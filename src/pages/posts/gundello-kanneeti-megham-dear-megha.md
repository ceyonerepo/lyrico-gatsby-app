---
title: "gundello kanneeti megham song lyrics"
album: "Dear Megha"
artist: "Gowra Hari"
lyricist: "Krishna Kanth"
director: "A. Sushanth Reddy"
path: "/albums/dear-megha-lyrics"
song: "Gundello Kanneeti Megham"
image: ../../images/albumart/dear-megha.jpg
date: 2021-09-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-FWOWqjDUlQ"
type: "love"
singers:
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gundello kanneti megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello kanneti megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammindha thanaithe dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammindha thanaithe dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adige lope athade lede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adige lope athade lede"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatemo naligindha pedhavula venuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatemo naligindha pedhavula venuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam… lone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam… lone"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello kanneti megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello kanneti megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammindha thanaithe dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammindha thanaithe dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundello kanneti megham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello kanneti megham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammindha thanaithe dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammindha thanaithe dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathame podhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathame podhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupe raade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupe raade"/>
</div>
<div class="lyrico-lyrics-wrapper">Guruthosthe thana swase manasuni kose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruthosthe thana swase manasuni kose"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayam Chese Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam Chese Aa"/>
</div>
</pre>
