---
title: "soththumoottai song lyrics"
album: "Puppy"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Nattu Dev"
path: "/albums/puppy-lyrics"
song: "Soththumoottai"
image: ../../images/albumart/puppy.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ep6XhH_Cw7w"
type: "love"
singers:
  - Dharan Kumar
  - RJ Balaji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nera Bus Yeri Vandhaalae Ormaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nera Bus Yeri Vandhaalae Ormaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Locala Suthuren Somari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Locala Suthuren Somari"/>
</div>
<div class="lyrico-lyrics-wrapper">Soru Sorunnu Dhenam Dhenamum Polambura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Sorunnu Dhenam Dhenamum Polambura"/>
</div>
<div class="lyrico-lyrics-wrapper">Konar Idly Kettu Amma Uyira Vaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konar Idly Kettu Amma Uyira Vaangura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunnu Paatha Vodanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunnu Paatha Vodanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Potten Oru Idea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potten Oru Idea"/>
</div>
<div class="lyrico-lyrics-wrapper">City Fulla Sutha Porom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City Fulla Sutha Porom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Ippo Jodiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Ippo Jodiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takku Takkunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takku Takkunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hotel Pera Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hotel Pera Sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Moodinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Menu Card-ah Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menu Card-ah Sollavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukkubhaai Varutha Curry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkubhaai Varutha Curry"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Tharen Dont Worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Tharen Dont Worry"/>
</div>
<div class="lyrico-lyrics-wrapper">Completaa Ajinomoto Freeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Completaa Ajinomoto Freeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daily Veetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily Veetula"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayal Seiya Venam Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal Seiya Venam Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitchen Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitchen Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Katta Naan Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Katta Naan Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaku Edhellaam Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaku Edhellaam Pudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Listu Pottu Solladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Listu Pottu Solladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Adhellaam Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Adhellaam Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Tharuven Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Tharuven Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daily Veetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily Veetula"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayal Seiya Venam Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal Seiya Venam Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitchen Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitchen Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Katta Naan Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Katta Naan Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daily Daily
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily Daily"/>
</div>
<div class="lyrico-lyrics-wrapper">Dai Dai Dai Dai Dai Dai Dai Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Dai Dai Dai Dai Dai Dai Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Daily Veetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily Veetula"/>
</div>
<div class="lyrico-lyrics-wrapper">Dai Dai Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Dai Dai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samayal Seiya Venam Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal Seiya Venam Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Dai Dai Dai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Dai Dai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitchen Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitchen Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitchen Kitchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitchen Kitchen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalathi Kalathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathi Kalathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathi Rosemilk-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathi Rosemilk-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Sollataaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Sollataaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dowser Kada Curry Sora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dowser Kada Curry Sora"/>
</div>
<div class="lyrico-lyrics-wrapper">Parcel Pannataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parcel Pannataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bombay Lassi Sakara Thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bombay Lassi Sakara Thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Tharattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Tharattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenikka Pala Taste-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenikka Pala Taste-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kaatataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kaatataa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nei Pongal Ooti Vidataaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nei Pongal Ooti Vidataaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Taa Taa Taa Taa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taa Taa Taa Taa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhaa Kattanchaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhaa Kattanchaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi Tharataa Taa Taa Taa Taa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi Tharataa Taa Taa Taa Taa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Vera Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Vera Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigama Pathani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigama Pathani"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadiketha Moodi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadiketha Moodi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Is Only Koththu Parottaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Only Koththu Parottaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dai Dai Dai Dailyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai Dai Dai Dailyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitchen Ke Ke Ke Kitchenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitchen Ke Ke Ke Kitchenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nera Bus Yeri Vandhaalae Ormaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nera Bus Yeri Vandhaalae Ormaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Locala Suthuren Somari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Locala Suthuren Somari"/>
</div>
<div class="lyrico-lyrics-wrapper">Soru Sorunnu Dhenam Dhenamum Polambura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Sorunnu Dhenam Dhenamum Polambura"/>
</div>
<div class="lyrico-lyrics-wrapper">Konar Idly Kettu Amma Uyira Vaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konar Idly Kettu Amma Uyira Vaangura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Pudichu En Kaadhal Sollata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Pudichu En Kaadhal Sollata"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Anachi Oru Duet Paadattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Anachi Oru Duet Paadattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hotel Poga Venamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hotel Poga Venamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Cooking Panna Naanum Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cooking Panna Naanum Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok Sonna Ootukaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok Sonna Ootukaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiuna Samayakaaren Dii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiuna Samayakaaren Dii"/>
</div>
<div class="lyrico-lyrics-wrapper">Dii Dii Diii Diii Diii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dii Dii Diii Diii Diii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daily Veetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily Veetula"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayal Seiya Venam Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayal Seiya Venam Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitchen Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitchen Illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Katta Naan Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Katta Naan Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaku Edhellaam Pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaku Edhellaam Pudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Listu Pottu Solladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Listu Pottu Solladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Adhellaam Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Adhellaam Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Tharuven Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Tharuven Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chokka Puri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chokka Puri"/>
</div>
<div class="lyrico-lyrics-wrapper">Chokka Puri Thediyae Bhai Saab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chokka Puri Thediyae Bhai Saab"/>
</div>
<div class="lyrico-lyrics-wrapper">Chokka Puri Thediyae Bhai Saab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chokka Puri Thediyae Bhai Saab"/>
</div>
<div class="lyrico-lyrics-wrapper">Chokka Puri Thediyae Bhai Saab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chokka Puri Thediyae Bhai Saab"/>
</div>
<div class="lyrico-lyrics-wrapper">Chokka Puri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chokka Puri"/>
</div>
</pre>
