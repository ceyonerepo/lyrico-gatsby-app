---
title: "idhuthaan kaathal song lyrics"
album: "Nandhi"
artist: "Bharathwaj"
lyricist: "Muthu Vijayan"
director: "Tamilvannan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Idhuthaan Kaathal"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EImBqo8phBg"
type: "happy"
singers:
  - Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhuthaan kaadhal enbadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaan kaadhal enbadhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam maarichelvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam maarichelvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuthaan kaadhal enbadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaan kaadhal enbadhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam maarichelvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam maarichelvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhai marandhu nindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhai marandhu nindren "/>
</div>
<div class="lyrico-lyrics-wrapper">baashai marandhu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baashai marandhu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai marandhu nindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai marandhu nindren "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai manandhukkonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai manandhukkonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaan kaadhal enbadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaan kaadhal enbadhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam maarichelvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam maarichelvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuthaan kaadhal enbadhaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaan kaadhal enbadhaa "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam maarichelvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam maarichelvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhai marandhu nindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhai marandhu nindren "/>
</div>
<div class="lyrico-lyrics-wrapper">baashai marandhu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baashai marandhu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai marandhu nindren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai marandhu nindren "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai manandhukkonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai manandhukkonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal naal Mudhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal naal Mudhal "/>
</div>
<div class="lyrico-lyrics-wrapper">naal thoorathil paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal thoorathil paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">urakkam pariththu ozhindhukkondaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakkam pariththu ozhindhukkondaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa thana thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa thana thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">marunaal marunaal aruginil paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marunaal marunaal aruginil paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal enakku parisalithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal enakku parisalithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">un moochu en thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moochu en thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">un pechu en vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pechu en vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangal un paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangal un paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">en netri un vervai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en netri un vervai"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhaa enadhaa kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhaa enadhaa kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">namadhey namadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namadhey namadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada unnaal innaal pon naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada unnaal innaal pon naal"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ellaa naalum nannaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ellaa naalum nannaal"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ennidam tharuvadhu ennaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ennidam tharuvadhu ennaal"/>
</div>
<div class="lyrico-lyrics-wrapper">annaal nannaal ennaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaal nannaal ennaalo"/>
</div>
</pre>
