---
title: 'kadhal sadugudu song lyrics'
album: 'Alaipayuthey'
artist: 'A R Rahman'
lyricist: 'Vairamuthu'
director: 'Maniratnam'
path: '/albums/Alaipayuthey-song-lyrics'
song: 'Kadhal Sadugudu'
image: ../../images/albumart/Alaipayuthey.jpg
date: 2000-04-14
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/OHu-T6s5_u0'
type: 'romantic'
singers: 
- S.P.B.Charan
---

Alaipayuthey song lyrics

<pre class="lyrics-native">
    Coming Soon
</pre>
<pre class="lyrics-english">
   <div class="lyrico-lyrics-wrapper">Kaadhal sadugudugudu kannae thodu thodu  (4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal sadugudugudu kannae thodu thodu  "/></div>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyae sitralaiyae karai vanthu vanthu pogum alaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alaiyae sitralaiyae karai vanthu vanthu pogum alaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thoduvaai methuvaai padarvaai endraal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai thoduvaai methuvaai padarvaai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuraiyaai karaiyum alaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nuraiyaai karaiyum alaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tholaivil paarthaal aamaam engindraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholaivil paarthaal aamaam engindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil vandhaal illai endraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Arugil vandhaal illai endraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Nagila nagila nagilaa oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line" value="Nagila nagila nagilaa oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagidaathu nagilaa oh oh  (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilagidaathu nagilaa oh oh  "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pazhagumpozhudhu kumariyaagi ennai velvaai pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhagumpozhudhu kumariyaagi ennai velvaai pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukai araiyil kuzhandhaiyaagi ennai kolvaai kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Padukai araiyil kuzhandhaiyaagi ennai kolvaai kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Kaadhal sadugudugudu kannae thodu thodu  (4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal sadugudugudu kannae thodu thodu  "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neeratum nerathil en annaiyaginraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeratum nerathil en annaiyaginraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaatum nerathil en pillaiyaginraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhaatum nerathil en pillaiyaginraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaga thottaalo mullaagi poginraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaga thottaalo mullaagi poginraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaga thottaalo poovaga aaginraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyaga thottaalo poovaga aaginraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En kanneer en thanneer ellamae neeanbae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kanneer en thanneer ellamae neeanbae"/>
</div>
<div class="lyrico-lyrics-wrapper">En inbam en thunbam ellamae neeanbae
<input type="checkbox" class="lyrico-select-lyric-line" value="En inbam en thunbam ellamae neeanbae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvum en saavum un kannil asaivilae
<input type="checkbox" class="lyrico-select-lyric-line" value="En vaazhvum en saavum un kannil asaivilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Nagila nagila nagilaa oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line" value="Nagila nagila nagilaa oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagidaathu nagilaa oh oh  (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilagidaathu nagilaa oh oh  "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pazhagumpozhudhu kumariyaagi ennai velvaai pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhagumpozhudhu kumariyaagi ennai velvaai pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukai araiyil kuzhandhaiyaagi ennai kolvaai kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Padukai araiyil kuzhandhaiyaagi ennai kolvaai kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Kaadhal sadugudugudu kannae thodu thodu  (4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal sadugudugudu kannae thodu thodu  "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un ullam naan kaana ennaayul pothaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un ullam naan kaana ennaayul pothaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbai naan solla un kaalam pothaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="En anbai naan solla un kaalam pothaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal inaiyenna un nenju kaanaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal inaiyenna un nenju kaanaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum en mutham sollaamal pogaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanaalum en mutham sollaamal pogaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kondaalum kondraalum en sondham needhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kondaalum kondraalum en sondham needhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraalum sendraalum un sondham naandhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nindraalum sendraalum un sondham naandhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vetkai pinnaalae en vaazhkai valaiyumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un vetkai pinnaalae en vaazhkai valaiyumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Nagila nagila nagilaa oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line" value="Nagila nagila nagilaa oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagidaathu nagilaa oh oh  (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilagidaathu nagilaa oh oh  "/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pazhagumpozhudhu kumariyaagi ennai velvaai pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhagumpozhudhu kumariyaagi ennai velvaai pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukai araiyil kuzhandhaiyaagi ennai kolvaai kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Padukai araiyil kuzhandhaiyaagi ennai kolvaai kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Kaadhal sadugudugudu kannae thodu thodu  (4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal sadugudugudu kannae thodu thodu  "/></div>
</div>
</pre>