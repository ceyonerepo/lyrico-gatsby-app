---
title: "clara my name is clara song lyrics"
album: "Laabam"
artist: "D.Imman"
lyricist: "Yugabharathi"
director: "SP. Jhananathan"
path: "/albums/laabam-lyrics"
song: "Clara My Name is Clara"
image: ../../images/albumart/laabam.jpg
date: 2021-09-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1qqkbeTyQTY"
type: "happy"
singers:
  - Sunidhi Chauhan
  - KPY Naveen 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">check check check
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="check check check"/>
</div>
<div class="lyrico-lyrics-wrapper">all in all alagu rasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all in all alagu rasi"/>
</div>
<div class="lyrico-lyrics-wrapper">instagram angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="instagram angel"/>
</div>
<div class="lyrico-lyrics-wrapper">facebook beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="facebook beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">udaiyila jenifer lofas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaiyila jenifer lofas"/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiyile ulaga nayagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiyile ulaga nayagi"/>
</div>
<div class="lyrico-lyrics-wrapper">iva gaanava kalakiruva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva gaanava kalakiruva"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya kaala kulukiruva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya kaala kulukiruva"/>
</div>
<div class="lyrico-lyrics-wrapper">discovum paduchuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="discovum paduchuduva"/>
</div>
<div class="lyrico-lyrics-wrapper">tip top ah siruchuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tip top ah siruchuduva"/>
</div>
<div class="lyrico-lyrics-wrapper">cinemavula iva sentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cinemavula iva sentha"/>
</div>
<div class="lyrico-lyrics-wrapper">silukum kooda out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silukum kooda out"/>
</div>
<div class="lyrico-lyrics-wrapper">ivala seendinaka atharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivala seendinaka atharku"/>
</div>
<div class="lyrico-lyrics-wrapper">aparam vaalurathe doubt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aparam vaalurathe doubt"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">athagapatathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athagapatathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paraseega varuman padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paraseega varuman padi"/>
</div>
<div class="lyrico-lyrics-wrapper">palastheena alagu lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palastheena alagu lady"/>
</div>
<div class="lyrico-lyrics-wrapper">pongi valigira ginger peera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongi valigira ginger peera"/>
</div>
<div class="lyrico-lyrics-wrapper">podha yethuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podha yethuva"/>
</div>
<div class="lyrico-lyrics-wrapper">namma clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma clara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thom thom thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thom thom thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thatha thakita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatha thakita"/>
</div>
<div class="lyrico-lyrics-wrapper">thom thom thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thom thom thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thatha thakita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatha thakita"/>
</div>
<div class="lyrico-lyrics-wrapper">thom thom thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thom thom thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">thatha thakita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatha thakita"/>
</div>
<div class="lyrico-lyrics-wrapper">thakida thakida tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakida thakida tham"/>
</div>
<div class="lyrico-lyrics-wrapper">thakida thakida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakida thakida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">clara my name is clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clara my name is clara"/>
</div>
<div class="lyrico-lyrics-wrapper">jora potene dera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jora potene dera"/>
</div>
<div class="lyrico-lyrics-wrapper">kathara kathara kathalichava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathara kathara kathalichava"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaigal ariya kan mulichava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaigal ariya kan mulichava"/>
</div>
<div class="lyrico-lyrics-wrapper">kanije than poothava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanije than poothava"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalum iravum pateduthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum pateduthava"/>
</div>
<div class="lyrico-lyrics-wrapper">palagi periya per eduthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagi periya per eduthava"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula paathava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula paathava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagaiye naane jayichava thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagaiye naane jayichava thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ambalaingala atharikavum maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambalaingala atharikavum maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">clara my name is clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clara my name is clara"/>
</div>
<div class="lyrico-lyrics-wrapper">jora potene dera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jora potene dera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">coolie vela seiya enni pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coolie vela seiya enni pona"/>
</div>
<div class="lyrico-lyrics-wrapper">podha yarum enna nambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podha yarum enna nambala"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi kodi rokkam vanthu senrtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi kodi rokkam vanthu senrtha"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum oorum onnum sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum oorum onnum sollala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kangani aanalum kaikooli aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangani aanalum kaikooli aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarum ponna dav adikuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarum ponna dav adikuran"/>
</div>
<div class="lyrico-lyrics-wrapper">samsari aanalum sanyasi aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samsari aanalum sanyasi aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">sollama ponna meya nikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollama ponna meya nikiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnala nanum kooda madangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala nanum kooda madangala"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala otta thanga thayangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala otta thanga thayangala"/>
</div>
<div class="lyrico-lyrics-wrapper">sellabam seithu santhosam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sellabam seithu santhosam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nan vanga vantha rowdy pombala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan vanga vantha rowdy pombala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">clara my name is clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clara my name is clara"/>
</div>
<div class="lyrico-lyrics-wrapper">jora potene dera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jora potene dera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathara kathara kathalichava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathara kathara kathalichava"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaigal ariya kan mulichava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaigal ariya kan mulichava"/>
</div>
<div class="lyrico-lyrics-wrapper">kanije than poothava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanije than poothava"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalum iravum pateduthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum pateduthava"/>
</div>
<div class="lyrico-lyrics-wrapper">palagi periya per eduthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palagi periya per eduthava"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula paathava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula paathava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagaiye naane jayichava thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagaiye naane jayichava thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ambalaingala atharikavum maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambalaingala atharikavum maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">clara clara clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clara clara clara"/>
</div>
<div class="lyrico-lyrics-wrapper">clara my name is clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clara my name is clara"/>
</div>
<div class="lyrico-lyrics-wrapper">jora potene dera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jora potene dera"/>
</div>
<div class="lyrico-lyrics-wrapper">clara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="clara"/>
</div>
</pre>
