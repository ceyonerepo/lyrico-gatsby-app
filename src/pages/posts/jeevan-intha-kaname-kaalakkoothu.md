---
title: "jeevan intha kaname song lyrics"
album: "Kaalakkoothu"
artist: "Justin Prabhakaran"
lyricist: "M Nagarajan"
director: "M. Nagarajan"
path: "/albums/kaalakkoothu-lyrics"
song: "Jeevan Intha Kaname"
image: ../../images/albumart/kaalakkoothu.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Hn-uG2pWAzQ"
type: "love"
singers:
  - Sathyaprakash
  - Latha Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jeevan indha kanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevan indha kanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu serumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu serumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothum indha nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum indha nimidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam kaadhal vaazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam kaadhal vaazhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontham neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagam unathallava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagam unathallava"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum un mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum un mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan maiyal kollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan maiyal kollava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazh naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazh naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam iruvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam iruvarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondragavae vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondragavae vaazhalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum ithu nilaiththidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum ithu nilaiththidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam ini vendalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam ini vendalaam"/>
</div>
</pre>
