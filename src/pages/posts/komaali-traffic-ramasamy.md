---
title: "komaali song lyrics"
album: "Traffic Ramasamy"
artist: "Balamurali Balu"
lyricist: "Kabilan Vairamuthu"
director: "Vicky"
path: "/albums/traffic-ramasamy-lyrics"
song: "Komaali"
image: ../../images/albumart/traffic-ramasamy.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KpiqBW3iGhI"
type: "happy"
singers:
  - Sindhuri Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saaraya Mandaya Mandaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaraya Mandaya Mandaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundakka Mandakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundakka Mandakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjile Saachika Javvathu Iva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjile Saachika Javvathu Iva"/>
</div>
<div class="lyrico-lyrics-wrapper">Jammuna Poosika Vaada Makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jammuna Poosika Vaada Makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaatha Semmara Kattaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaatha Semmara Kattaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkatha Vitutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkatha Vitutu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettika Kattika Koosaamathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettika Kattika Koosaamathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootani Vechuka Kumbittuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootani Vechuka Kumbittuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayam Illamaley Kolla Aduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam Illamaley Kolla Aduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumam Kaathidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumam Kaathidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumai Ozhikka Vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumai Ozhikka Vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhikka Vanthomnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhikka Vanthomnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadaya Suttukkulaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadaya Suttukkulaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaamaa Vanthaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaamaa Vanthaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tsunami Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tsunami Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Sombula Pudikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Sombula Pudikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkanaa Thappichikalaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkanaa Thappichikalaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppuna Thodachikalaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thodachikalaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Altaapu Anthapurathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Altaapu Anthapurathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Kuthikira Kalli Idupula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Kuthikira Kalli Idupula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalaye Killi Mudikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalaye Killi Mudikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Kirukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Kirukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavodu Kottunu Kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavodu Kottunu Kottara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandhira Oliya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandhira Oliya "/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthaama Sitharaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthaama Sitharaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tubelight Taa Sandhayil Vikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tubelight Taa Sandhayil Vikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagasa Kaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagasa Kaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumari Kottathuku Vaayaa Machaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari Kottathuku Vaayaa Machaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalu Koopuduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalu Koopuduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kosuru Arasiyalu Ippo Ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kosuru Arasiyalu Ippo Ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru Kaathirikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru Kaathirikey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaayam Theenthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam Theenthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegaaya Inbangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegaaya Inbangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Theerathathaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Theerathathaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkanaa Thappichikalaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkanaa Thappichikalaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppuna Thodachikalaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppuna Thodachikalaame"/>
</div>
</pre>
