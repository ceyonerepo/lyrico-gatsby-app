---
title: "neethane en kadhale song lyrics"
album: "Varisi"
artist: "Nandha"
lyricist: "Umaramanan"
director: "Karthik Doss"
path: "/albums/varisi-song-lyrics"
song: "Neethane En Kadhale"
image: ../../images/albumart/varisi.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I1CGmR_omY4"
type: "love"
singers:
  - Yazin Nizar
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee thane en kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thane en kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thane en kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thane en kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh oh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh "/>
</div>
<div class="lyrico-lyrics-wrapper">oru kadhal pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kadhal pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">mannil naam vaazhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannil naam vaazhave"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli thedal pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thedal pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil naam serave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil naam serave"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli thedal pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thedal pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil naam serave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil naam serave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vizhigalil un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vizhigalil un"/>
</div>
<div class="lyrico-lyrics-wrapper">tholil saaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholil saaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiye pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiye pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">udane naan sagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udane naan sagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha nodiye pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nodiye pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">udane naan sagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udane naan sagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">niranjana niranjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niranjana niranjana"/>
</div>
<div class="lyrico-lyrics-wrapper">nigar illa kanchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nigar illa kanchana"/>
</div>
<div class="lyrico-lyrics-wrapper">niranjana niranjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niranjana niranjana"/>
</div>
<div class="lyrico-lyrics-wrapper">yenguthe en kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenguthe en kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhale kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhale kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhale kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhale kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaname porvaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaname porvaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiye methaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiye methaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">logame kadhalin leelaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="logame kadhalin leelaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udal illaya uyir allaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal illaya uyir allaya"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhalil "/>
</div>
<div class="lyrico-lyrics-wrapper">ellaiye illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaiye illaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iruthi varaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruthi varaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">pririyathu iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pririyathu iru"/>
</div>
<div class="lyrico-lyrics-wrapper">privin vazhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="privin vazhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyathu iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyathu iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yarenum yathenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarenum yathenum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalum namalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalum namalum"/>
</div>
<div class="lyrico-lyrics-wrapper">yarenum yathenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarenum yathenum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalum namalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalum namalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">azhage unai kaanum inbam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhage unai kaanum inbam "/>
</div>
<div class="lyrico-lyrics-wrapper">athu seiyum maayam manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu seiyum maayam manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kodi kodi yugam vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kodi kodi yugam vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum arugil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhi thandi neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi thandi neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli vilaginal oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli vilaginal oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai pesamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai pesamal"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbinal siru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbinal siru "/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthaiyaga unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthaiyaga unai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi naan alaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi naan alaruven"/>
</div>
<div class="lyrico-lyrics-wrapper">un thaimaiyin katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thaimaiyin katha"/>
</div>
<div class="lyrico-lyrics-wrapper">kathapile malaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathapile malaruven"/>
</div>
</pre>
