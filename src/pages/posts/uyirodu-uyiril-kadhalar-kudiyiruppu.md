---
title: "uyirodu uyiril song lyrics"
album: "Kadhalar Kudiyiruppu"
artist: "James Vasanthan"
lyricist: "Na. Muthukumar"
director: "AMR Ramesh"
path: "/albums/kadhalar-kudiyiruppu-lyrics"
song: "Uyirodu Uyiril"
image: ../../images/albumart/kadhalar-kudiyiruppu.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/m9Yj9oTBZj0"
type: "love"
singers:
  - Chinmayee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirey en uyiril vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirey en uyiril vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirukku uyirai thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukku uyirai thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroadum pesaamal engeyum poagaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroadum pesaamal engeyum poagaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paadhai paartheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhai paartheney"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil eadho seidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil eadho seidhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaaney kaadhal endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaaney kaadhal endraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadigaaram paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadigaaram paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi naeram thoongaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi naeram thoongaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiththaan kaetteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiththaan kaetteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkiren thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkiren thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil vaazhgiren kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil vaazhgiren kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">theendiththudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendiththudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkiren tholaindhuppoagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkiren tholaindhuppoagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnullaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi paarkkumboadhum kai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi paarkkumboadhum kai "/>
</div>
<div class="lyrico-lyrics-wrapper">veesippoagum poadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesippoagum poadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paeraicholli chollippaarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paeraicholli chollippaarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhoadu eadhoppaada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoadu eadhoppaada "/>
</div>
<div class="lyrico-lyrics-wrapper">kaatroadu kaetkumboadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatroadu kaetkumboadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhal ennithaaney kaetpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal ennithaaney kaetpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadharugey naan irundhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadharugey naan irundhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">idaivilikkoalam poadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaivilikkoalam poadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaliravin boadhu ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaliravin boadhu ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvizhaa koalamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvizhaa koalamaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nijamaa idhu kanavaa unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nijamaa idhu kanavaa unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikkaadha naaley illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikkaadha naaley illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkiren thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkiren thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil vaazhgiren kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil vaazhgiren kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">theendiththudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendiththudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkiren tholaindhuppoagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkiren tholaindhuppoagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnullaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoadu poagumboadhu en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoadu poagumboadhu en "/>
</div>
<div class="lyrico-lyrics-wrapper">saalai orammeedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalai orammeedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengum pookkul pookkal paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengum pookkul pookkal paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal theendumboadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigal theendumboadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai thaandumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai thaandumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En penmai eadho aagakkanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En penmai eadho aagakkanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugalaai ninaivugalaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugalaai ninaivugalaal "/>
</div>
<div class="lyrico-lyrics-wrapper">thadam purandoadum boadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadam purandoadum boadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukkathinaal nerukkathinaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerukkathinaal nerukkathinaal "/>
</div>
<div class="lyrico-lyrics-wrapper">nilaikkulaindhaaley raadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaikkulaindhaaley raadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nijamaa idhu kanavaa unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nijamaa idhu kanavaa unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikkaadha naaley illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikkaadha naaley illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirey en uyiril vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirey en uyiril vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirukku uyirai thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirukku uyirai thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaroadum pesaamal engeyum poagaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaroadum pesaamal engeyum poagaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paadhai paartheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhai paartheney"/>
</div>
</pre>
