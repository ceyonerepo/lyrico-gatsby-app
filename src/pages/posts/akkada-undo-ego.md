---
title: "akkada undo song lyrics"
album: "Ego"
artist: "Sai Karthik "
lyricist: "Bhaskarabhatla"
director: "RV Subramanyam"
path: "/albums/ego-lyrics"
song: "Akkada Undo"
image: ../../images/albumart/ego.jpg
date: 2018-01-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tuKjecvPVO8"
type: "love"
singers:
  - Sai Charan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">akkada undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkada undo"/>
</div>
<div class="lyrico-lyrics-wrapper">ikkada undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ikkada undo"/>
</div>
<div class="lyrico-lyrics-wrapper">etu vaipellindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etu vaipellindo"/>
</div>
<div class="lyrico-lyrics-wrapper">guppedu gundenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guppedu gundenu"/>
</div>
<div class="lyrico-lyrics-wrapper">pattuku poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattuku poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ekkada daagundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekkada daagundo"/>
</div>
<div class="lyrico-lyrics-wrapper">upponge santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="upponge santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">thakinde aakaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakinde aakaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">naa pedaviki navvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa pedaviki navvula"/>
</div>
<div class="lyrico-lyrics-wrapper">pandaga vachinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandaga vachinde"/>
</div>
<div class="lyrico-lyrics-wrapper">naa choopulu veliginche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa choopulu veliginche"/>
</div>
<div class="lyrico-lyrics-wrapper">oorantha gaalinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorantha gaalinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnippatikippudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnippatikippudu"/>
</div>
<div class="lyrico-lyrics-wrapper">choodalani unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choodalani unde"/>
</div>
<div class="lyrico-lyrics-wrapper">oo oho oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo oho oo"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ga re ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ga re ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma ga ma re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma ga ma re"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa ma pa nipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa ma pa nipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga mare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga mare "/>
</div>
<div class="lyrico-lyrics-wrapper">sa ga re ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ga re ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma ga ma re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma ga ma re"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa ma pa nipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa ma pa nipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga mare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga mare "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee gaajula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gaajula"/>
</div>
<div class="lyrico-lyrics-wrapper">savvadi naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savvadi naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vinabaduthoonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinabaduthoonde"/>
</div>
<div class="lyrico-lyrics-wrapper">aa savvadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa savvadi"/>
</div>
<div class="lyrico-lyrics-wrapper">venake pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venake pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">parigeduthunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parigeduthunde"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvodile oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvodile oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thaguluthu unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaguluthu unde"/>
</div>
<div class="lyrico-lyrics-wrapper">aa vechhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa vechhani"/>
</div>
<div class="lyrico-lyrics-wrapper">oopiri nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopiri nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">bratikisthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bratikisthonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nuvvante naakistam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvante naakistam"/>
</div>
<div class="lyrico-lyrics-wrapper">nenante neekistam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenante neekistam"/>
</div>
<div class="lyrico-lyrics-wrapper">inthistam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthistam"/>
</div>
<div class="lyrico-lyrics-wrapper">enduku dachesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enduku dachesam"/>
</div>
<div class="lyrico-lyrics-wrapper">kopaalu thaapalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kopaalu thaapalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaralu miriyalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaralu miriyalu"/>
</div>
<div class="lyrico-lyrics-wrapper">nooresthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooresthu"/>
</div>
<div class="lyrico-lyrics-wrapper">satruvulaipoyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satruvulaipoyam"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvventha nuvventha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvventha nuvventha"/>
</div>
<div class="lyrico-lyrics-wrapper">anukuntu premantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukuntu premantha"/>
</div>
<div class="lyrico-lyrics-wrapper">daggaraga unnannallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daggaraga unnannallu"/>
</div>
<div class="lyrico-lyrics-wrapper">vadilesaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadilesaam"/>
</div>
<div class="lyrico-lyrics-wrapper">mana madyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana madyana"/>
</div>
<div class="lyrico-lyrics-wrapper">premundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">ani telisetappatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani telisetappatiki"/>
</div>
<div class="lyrico-lyrics-wrapper">mari anthaku antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari anthaku antha"/>
</div>
<div class="lyrico-lyrics-wrapper">dooram ayipoyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooram ayipoyam"/>
</div>
<div class="lyrico-lyrics-wrapper">oo oho oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo oho oo"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ga re ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ga re ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma ga ma re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma ga ma re"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa ma pa nipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa ma pa nipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga mare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga mare "/>
</div>
<div class="lyrico-lyrics-wrapper">sa ga re ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ga re ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma ga ma re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma ga ma re"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa ma pa nipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa ma pa nipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga mare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga mare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa kaalu aagadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kaalu aagadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naa cheyyi aadadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa cheyyi aadadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">kanipinchedaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanipinchedaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">nakemi thochadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakemi thochadu"/>
</div>
<div class="lyrico-lyrics-wrapper">naakinkem nachhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naakinkem nachhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">na sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvayye dhaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvayye dhaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu vethike udyogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu vethike udyogam"/>
</div>
<div class="lyrico-lyrics-wrapper">naakentho nachhinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naakentho nachhinde"/>
</div>
<div class="lyrico-lyrics-wrapper">oka nimisham kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oka nimisham kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">selave pattanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selave pattanule"/>
</div>
<div class="lyrico-lyrics-wrapper">mana madyana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana madyana "/>
</div>
<div class="lyrico-lyrics-wrapper">ee dooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee dooram"/>
</div>
<div class="lyrico-lyrics-wrapper">daram la tegipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daram la tegipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa devudi gudilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa devudi gudilo"/>
</div>
<div class="lyrico-lyrics-wrapper">tenkay kodathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tenkay kodathale"/>
</div>
<div class="lyrico-lyrics-wrapper">oo oho oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo oho oo"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ga re ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ga re ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma ga ma re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma ga ma re"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa ma pa nipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa ma pa nipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga mare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga mare "/>
</div>
<div class="lyrico-lyrics-wrapper">sa ga re ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ga re ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma ga ma re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma ga ma re"/>
</div>
<div class="lyrico-lyrics-wrapper">sa pa ma pa nipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa pa ma pa nipa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga mare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga mare"/>
</div>
</pre>
