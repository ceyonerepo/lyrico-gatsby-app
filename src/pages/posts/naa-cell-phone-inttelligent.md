---
title: "naa cell phone song lyrics"
album: "Inttelligent"
artist: "S Thaman"
lyricist: "Varikuppala Yadagiri"
director: "V V Vinayak"
path: "/albums/inttelligent-lyrics"
song: "Naa Cell Phone"
image: ../../images/albumart/inttelligent.jpg
date: 2018-02-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/1_tWZ4iGMGM"
type: "love"
singers:
  - Manisha Eerabathini
  - Jaspreet Jasz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O Cellphona Naa Dhilsona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Cellphona Naa Dhilsona "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Eepaina Nee Banisa Kaanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Eepaina Nee Banisa Kaanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">O Ringtonaa Mera Dheevaanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ringtonaa Mera Dheevaanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Nuntunaa Nee Voohalalonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Nuntunaa Nee Voohalalonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Thala Merise Shepe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thala Merise Shepe "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumuthu Matthekki Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumuthu Matthekki Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagadhimi Nadakala Touch Screen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagadhimi Nadakala Touch Screen "/>
</div>
<div class="lyrico-lyrics-wrapper">Kiss Lo Lockaiponaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiss Lo Lockaiponaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tring Tring Kaburulalonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tring Tring Kaburulalonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Kudhupavana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Kudhupavana "/>
</div>
<div class="lyrico-lyrics-wrapper">Titi Ting Soundulalonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Titi Ting Soundulalonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Idhi Panchukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Idhi Panchukonaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Cellphona Naa Dhilsona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Cellphona Naa Dhilsona "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Eepaina Nee Banisa Kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Eepaina Nee Banisa Kaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Facebook Lo Sweet Nothings Sharing 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook Lo Sweet Nothings Sharing "/>
</div>
<div class="lyrico-lyrics-wrapper">Twitter Lo Tea Coffee Something 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twitter Lo Tea Coffee Something "/>
</div>
<div class="lyrico-lyrics-wrapper">Twenty Megapixel Lo Styling 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twenty Megapixel Lo Styling "/>
</div>
<div class="lyrico-lyrics-wrapper">Oorinchi Udikinche Teasing 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorinchi Udikinche Teasing "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirty Second Timelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirty Second Timelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Heart Teaser Pampana Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Heart Teaser Pampana Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventane Kodithe Reply 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventane Kodithe Reply "/>
</div>
<div class="lyrico-lyrics-wrapper">Four G Lo Vaali Pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Four G Lo Vaali Pona "/>
</div>
<div class="lyrico-lyrics-wrapper">Tension Pedithe Nene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Pedithe Nene "/>
</div>
<div class="lyrico-lyrics-wrapper">Two G Buffer Avanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two G Buffer Avanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viber Lo Every Time Busy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viber Lo Every Time Busy "/>
</div>
<div class="lyrico-lyrics-wrapper">We Chat Lo Every Where Khushi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Chat Lo Every Where Khushi "/>
</div>
<div class="lyrico-lyrics-wrapper">Line Lona Lovely Lovely Meeting 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Line Lona Lovely Lovely Meeting "/>
</div>
<div class="lyrico-lyrics-wrapper">Whatsapp Lo Naughty 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp Lo Naughty "/>
</div>
<div class="lyrico-lyrics-wrapper">Naughty Fighting 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naughty Fighting "/>
</div>
<div class="lyrico-lyrics-wrapper">Scype Lo Sweetu Sweetu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scype Lo Sweetu Sweetu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maataltho Palakarinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataltho Palakarinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukula Belukula Feeling 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalukula Belukula Feeling "/>
</div>
<div class="lyrico-lyrics-wrapper">Chooputho Pattukonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooputho Pattukonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalaka Kadhalanu Choope 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalaka Kadhalanu Choope "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Pettukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Pettukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O Cellphona Naa Dhilsona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Cellphona Naa Dhilsona "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Eepaina Nee Banisa Kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Eepaina Nee Banisa Kaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O Ringtonaa Mera Dheevaanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Ringtonaa Mera Dheevaanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Nuntunaa Nee Voohalalonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Nuntunaa Nee Voohalalonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O Cellphonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Cellphonaa"/>
</div>
</pre>
