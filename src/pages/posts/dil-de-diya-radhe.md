---
title: "dil de diya song lyrics"
album: "Radhe"
artist: "Himesh Reshammiya"
lyricist: "Shabbir Ahmed"
director: "Prabhu Deva"
path: "/albums/radhe-lyrics"
song: "Dil De Diya"
image: ../../images/albumart/radhe.jpg
date: 2021-05-13
lang: hindi
youtubeLink: "https://www.youtube.com/embed/e1Bj4tMGh64"
type: "Love"
singers:
  - Kamaal Khan
  - Payal Dev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hai sama pyaar ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai sama pyaar ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa zara humse aake mil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa zara humse aake mil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho gaye hum fida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho gaye hum fida"/>
</div>
<div class="lyrico-lyrics-wrapper">Maangte hain hum tumse dil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangte hain hum tumse dil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isne bhi dil maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isne bhi dil maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Usne bhi dil maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne bhi dil maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Isne bhi dil maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isne bhi dil maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Usne bhi dil maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne bhi dil maanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maine inkaar kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine inkaar kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ja pardesi tujhe dil de diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ja pardesi tujhe dil de diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine ikraar kiya, ja maine pyar kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine ikraar kiya, ja maine pyar kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ja pardesi tujhe dil de diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ja pardesi tujhe dil de diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Logon ke kehne se humko kya lena hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logon ke kehne se humko kya lena hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Parwaah karein kyun kisi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parwaah karein kyun kisi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Badnaam hote hain hum tum to ho jayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badnaam hote hain hum tum to ho jayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Dar ke mohabbat nahi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dar ke mohabbat nahi ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan sach bolun khuda kasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan sach bolun khuda kasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Lut gaye lut gaye sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lut gaye lut gaye sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri gali aaye jabse hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri gali aaye jabse hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana jaana teri gali rozana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana jaana teri gali rozana"/>
</div>
<div class="lyrico-lyrics-wrapper">Toote na yeh yaarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toote na yeh yaarana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isne bhi dil maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isne bhi dil maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Usne bhi dil maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne bhi dil maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine inkaar kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine inkaar kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ja pardesi tujhe dil de diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ja pardesi tujhe dil de diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe dil de diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe dil de diya"/>
</div>
</pre>
