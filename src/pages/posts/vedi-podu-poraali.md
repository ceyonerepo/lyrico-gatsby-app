---
title: "vedi podu song lyrics"
album: "Poraali"
artist: "Sundar C Babu"
lyricist: "Kabilan"
director: "Samuthirakani"
path: "/albums/poraali-lyrics"
song: "Vedi Podu"
image: ../../images/albumart/poraali.jpg
date: 2011-12-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UIOlui5r_zE"
type: "happy"
singers:
  - Velmurugan
  - Thanjai Selvi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vedi Pottu Paaduda Adi pottu aaduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi Pottu Paaduda Adi pottu aaduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudikaama bodha tharren vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudikaama bodha tharren vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marapaachi ponnuda kilipechu kannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marapaachi ponnuda kilipechu kannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaada raatinatha paarrenda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaada raatinatha paarrenda "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhanam poosi vandha sappathi kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam poosi vandha sappathi kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta manam veesum thoongaatha malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta manam veesum thoongaatha malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillara pallu kaari sirichaale pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillara pallu kaari sirichaale pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula iva vandha kankollaa kaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula iva vandha kankollaa kaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unveedu pillai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unveedu pillai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">un sondham pandham endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sondham pandham endru"/>
</div>
<div class="lyrico-lyrics-wrapper">onnaa ne moota kattaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnaa ne moota kattaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayilla pillaikum thaalattu paadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayilla pillaikum thaalattu paadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">annai pol thaangi kollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annai pol thaangi kollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta vetta kaaikira vazha maram pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta vetta kaaikira vazha maram pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nambi ohngi nillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nambi ohngi nillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye rattam katti yeruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye rattam katti yeruda"/>
</div>
<div class="lyrico-lyrics-wrapper">mettu katti kooruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mettu katti kooruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti thotti namma ooruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti thotti namma ooruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedi Pottu Paaduda Adi pottu aaduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi Pottu Paaduda Adi pottu aaduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudikaama bodha tharren vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudikaama bodha tharren vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marapaachi ponnuda kilipechu kannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marapaachi ponnuda kilipechu kannuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavaada raatinatha paarrenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaada raatinatha paarrenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum rendu kaamathuku kalla chaavida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum rendu kaamathuku kalla chaavida"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thotta odipogum aavida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thotta odipogum aavida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla oduradhu ratham illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla oduradhu ratham illada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee uchithandhu pacha kutthum muthamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uchithandhu pacha kutthum muthamda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannaiyaaru thottathuku vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaiyaaru thottathuku vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pambarama thudikudhu naadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambarama thudikudhu naadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kokkarako sevaluku kozhi neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kokkarako sevaluku kozhi neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda vaangi vachiruke naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda vaangi vachiruke naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai unnaku pakkam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai unnaku pakkam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">vegathoda vaagai sooda vaada vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegathoda vaagai sooda vaada vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada pogappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pogappa"/>
</div>
</pre>
