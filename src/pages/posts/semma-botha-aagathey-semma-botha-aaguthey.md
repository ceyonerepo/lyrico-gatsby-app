---
title: "semma botha aagathey song lyrics"
album: "Semma Botha Aaguthey"
artist: "Yuvan Shankar Raja"
lyricist: "Rokesh - Badri Venkatesh"
director: "Badri Venkatesh"
path: "/albums/semma-botha-aaguthey-lyrics"
song: "Semma Botha Aagathey"
image: ../../images/albumart/semma-botha-aaguthey.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6VrK07pdw10"
type: "mass"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Lady Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Lady Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Dhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Dhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Alainchiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alainchiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vetkam Eenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vetkam Eenam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanam Suda Soranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanam Suda Soranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Maranthuten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Maranthuten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aayita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aayita"/>
</div>
<div class="lyrico-lyrics-wrapper">Chickennuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chickennuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aayita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aayita"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasthiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasthiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Lady Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Lady Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Dhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Dhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Norikkiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norikkiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Yaarum Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Yaarum Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendi Paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendi Paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavane Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavane Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Polanthuten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polanthuten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aayita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aayita"/>
</div>
<div class="lyrico-lyrics-wrapper">Chickennuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chickennuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aayita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aayita"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasthiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasthiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavakara Aalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavakara Aalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vudura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vudura"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Yedhuthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Yedhuthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaraiyum Ooda Vudura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaraiyum Ooda Vudura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorathura Kumbalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathura Kumbalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Single Athan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Single Athan"/>
</div>
<div class="lyrico-lyrics-wrapper">Samalikkum Dhavaluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samalikkum Dhavaluthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikira Adiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikira Adiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri Kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri Kai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidiyil Sithari Pathari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiyil Sithari Pathari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathari Aluvuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathari Aluvuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yessu Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yessu Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithakaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithakaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonchi Kilichi Unna Gali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonchi Kilichi Unna Gali"/>
</div>
<div class="lyrico-lyrics-wrapper">Panna Porenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna Porenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Lady Body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Lady Body"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Dhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Dhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Norikkiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norikkiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thedi Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thedi Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodachal Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodachal Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatuvene Gethuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatuvene Gethuthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aayita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aayita"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Bodha Aayita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Bodha Aayita"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejarudaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejarudaaaaaa"/>
</div>
</pre>
