---
title: "muntha kallu song lyrics"
album: "ABCD"
artist: "Judah Sandhy"
lyricist: "Tirupathi Jaavana"
director: "Sanjeev Reddy"
path: "/albums/abcd-lyrics"
song: "Muntha Kallu"
image: ../../images/albumart/abcd.jpg
date: 2019-05-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yVEMAMqRQ24"
type: "happy"
singers:
  - Tirupathi Jaavana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muntha kallu cheta patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muntha kallu cheta patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Taagutunte nuvvu ethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taagutunte nuvvu ethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollantaa matthekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollantaa matthekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogipodaa vorabbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogipodaa vorabbi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jattu katti podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jattu katti podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">swargame chooddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swargame chooddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalanni chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalanni chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Digi kindaki vaddamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digi kindaki vaddamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veddaamaa damme kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veddaamaa damme kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Veddaamaa lungi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veddaamaa lungi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppule ippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steppule ippude"/>
</div>
<div class="lyrico-lyrics-wrapper">iraga Dheeddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraga Dheeddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillake kannu kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillake kannu kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">whistle ye veddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whistle ye veddaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa fillaa boom racche cheddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa fillaa boom racche cheddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing naa boom recchi podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing naa boom recchi podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa fillaa boom danchi kodadaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa fillaa boom danchi kodadaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing naa boom kummeddaam maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing naa boom kummeddaam maamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muntha kallu cheta patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muntha kallu cheta patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Taagutunte nuvvu ethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taagutunte nuvvu ethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollantaa matthekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollantaa matthekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogipodaa vorabbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogipodaa vorabbi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerigaadi kottukelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerigaadi kottukelli"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaraa killi teddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaraa killi teddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorigaadi baddikelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorigaadi baddikelli"/>
</div>
<div class="lyrico-lyrics-wrapper">beedine veligiddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beedine veligiddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkane topudu bandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkane topudu bandi"/>
</div>
<div class="lyrico-lyrics-wrapper">meeda palleelu kondaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeda palleelu kondaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbunnodu evadu manalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbunnodu evadu manalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vundaledanipiddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vundaledanipiddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tellaaridaaka intikellaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tellaaridaaka intikellaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirige ganate manadero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirige ganate manadero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakali veste pallu tomaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakali veste pallu tomaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaba gaba tinedi manamero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaba gaba tinedi manamero"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu choosina amma naannato
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu choosina amma naannato"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitlu tinedi manamero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitlu tinedi manamero"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadakellina manamunde chote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadakellina manamunde chote"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaataralaage untadiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaataralaage untadiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa fillaa boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa fillaa boom"/>
</div>
<div class="lyrico-lyrics-wrapper">racche cheddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="racche cheddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing naa boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing naa boom"/>
</div>
<div class="lyrico-lyrics-wrapper">recchi podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="recchi podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa fillaa boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa fillaa boom"/>
</div>
<div class="lyrico-lyrics-wrapper">danchi kodadaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danchi kodadaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing naa boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing naa boom"/>
</div>
<div class="lyrico-lyrics-wrapper">kummeddaam mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummeddaam mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meda meeda mede katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meda meeda mede katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Daantlo unte em laabham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daantlo unte em laabham"/>
</div>
<div class="lyrico-lyrics-wrapper">Baitakocchi choosaavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baitakocchi choosaavante"/>
</div>
<div class="lyrico-lyrics-wrapper">Telustundoy prapancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telustundoy prapancham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vontariga unnaavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vontariga unnaavante"/>
</div>
<div class="lyrico-lyrics-wrapper">Picchodu antaaru ee janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchodu antaaru ee janam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekantu snehitulunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekantu snehitulunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Tappaka chestaaru saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappaka chestaaru saayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jattu katti podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jattu katti podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">swarganne chooddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swarganne chooddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalanni chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalanni chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">digi kindaki vaddamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="digi kindaki vaddamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veddaamaa damme kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veddaamaa damme kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Veddaamaa lungi katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veddaamaa lungi katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppule ippude iraga deeddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steppule ippude iraga deeddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillake kannu kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillake kannu kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">whistle ye veddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whistle ye veddaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa fillaa boom racche cheddaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa fillaa boom racche cheddaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing naa boom recchi podaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing naa boom recchi podaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa fillaa boom danchi kodadaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa fillaa boom danchi kodadaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sing naa boom kummeddaam mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sing naa boom kummeddaam mama"/>
</div>
</pre>
