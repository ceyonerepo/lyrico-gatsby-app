---
title: "cheekati chirujwaalai song lyrics"
album: "Ishq"
artist: "Mahati Swara Sagar"
lyricist: "Sri Mani"
director: "	S.S. Raju"
path: "/albums/ishq-lyrics"
song: "Cheekati Chirujwaalai"
image: ../../images/albumart/ishq.jpg
date: 2021-07-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LnXTW8bxdwU"
type: "sad"
singers:
  - Anurag Kulkarni
  - Uma Neha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Seekati chirujaawali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekati chirujaawali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippulu kurisindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippulu kurisindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththulu doosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththulu doosindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundenu kosindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundenu kosindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayam sesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam sesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam lekundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam lekundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam lekundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam lekundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raguluthundi rakthakanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raguluthundi rakthakanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelona nippukanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona nippukanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyi pagalu leni ranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyi pagalu leni ranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatallo cheppaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatallo cheppaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamtho aapaleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamtho aapaleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Athamavani yuddhamedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athamavani yuddhamedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalopala jaruguthondhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalopala jaruguthondhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nanu kaalchuthondhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nanu kaalchuthondhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishamai yegabaakuthondhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishamai yegabaakuthondhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa premaku malinamanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa premaku malinamanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa hrudhayama baggumandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa hrudhayama baggumandhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalechesina em chesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalechesina em chesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaloni narakaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaloni narakaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichedelaa ee yaathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichedelaa ee yaathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalechesina em chesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalechesina em chesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaloni narakaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaloni narakaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichedelaa ee yaathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichedelaa ee yaathana"/>
</div>
</pre>
