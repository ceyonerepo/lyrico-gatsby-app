---
title: "thollai seiyum kadhal song lyrics"
album: "Maanagaram"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Lokesh Kanagaraj"
path: "/albums/maanagaram-lyrics"
song: "Thollai Seiyum Kadhal"
image: ../../images/albumart/maanagaram.jpg
date: 2017-03-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xBrtn4-UZv8"
type: "love"
singers:
  -	Aarthi N Ashwin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thollai Seiyum Kadhal Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai Seiyum Kadhal Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyum Thollai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyum Thollai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Solla Va Kadhil Solla Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Solla Va Kadhil Solla Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Dhooram Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Dhooram Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Mounam Baram Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Mounam Baram Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Poga Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Poga Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thalli Poga Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thalli Poga Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Partha Parvai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Partha Parvai Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Nadandha Paadhai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Nadandha Paadhai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbamana Imsai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamana Imsai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ketkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ketkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thollai Seiyum Kadhal Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai Seiyum Kadhal Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyum Thollai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyum Thollai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Solla Va Kadhil Solla Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Solla Va Kadhil Solla Va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Endru Vaayum Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endru Vaayum Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Undu Endru Paarvai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu Endru Paarvai Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Thondrum Modhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Thondrum Modhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerum Indha Thedale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerum Indha Thedale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piriyum Neram Thirumbi Parkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum Neram Thirumbi Parkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Dhaana Kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Dhaana Kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thollai Seiyum Kadhal Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollai Seiyum Kadhal Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyum Thollai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyum Thollai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Solla Va Kadhil Solla Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Solla Va Kadhil Solla Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Dhooram Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Dhooram Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Mounam Baram Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Mounam Baram Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Poga Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Poga Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thalli Poga Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thalli Poga Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru Partha Parvai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Partha Parvai Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Nadandha Paadhai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Nadandha Paadhai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbamana Imsai Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamana Imsai Dhaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ketkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ketkava"/>
</div>
</pre>
