---
title: "yei oodu oodu song lyrics"
album: "RK Nagar"
artist: "Premgi Amaren"
lyricist: "Parthi Bhasker"
director: "Saravana Rajan"
path: "/albums/rk-nagar-lyrics"
song: "Yei Oodu Oodu"
image: ../../images/albumart/rk-nagar.jpg
date: 2019-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/w5LKltGXkQE"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yei Oodu Oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Oodu Oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangakatti Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangakatti Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedu Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedu Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrippadi Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrippadi Dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Moongilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Moongilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullanguzhal Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullanguzhal Dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodu Nikkamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodu Nikkamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhum Vazhi Kodi Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum Vazhi Kodi Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Thedi Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Thedi Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Koodi Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Koodi Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanae Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kezhvi Ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kezhvi Ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Vazhkai Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Vazhkai Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Vettri Mel Vettri Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Vettri Mel Vettri Thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Oodu Oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Oodu Oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangakatti Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangakatti Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedu Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedu Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrippadi Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrippadi Dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Moongilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Moongilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullanguzhal Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullanguzhal Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodu Nikkamalae Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodu Nikkamalae Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hoi Hoi Hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hoi Hoi Hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nettru Pola Indru Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nettru Pola Indru Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Pola Naalai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Pola Naalai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaitha Velaigal Nadakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaitha Velaigal Nadakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pola Yarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pola Yarum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedipparu Oorukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedipparu Oorukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai Sakkaram Maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai Sakkaram Maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhthu Sollithaan Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhthu Sollithaan Suthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Megam Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Megam Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Vaanam Maarathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vaanam Maarathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhkai Maaripponaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhkai Maaripponaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Maarathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Maarathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaalam Vandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalam Vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Yogam Vandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Yogam Vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Suthum Boomi Pola Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Suthum Boomi Pola Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutha Poren Sutha Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutha Poren Sutha Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Oodu Oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Oodu Oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangakatti Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangakatti Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedu Thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedu Thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Vazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Vazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrippadi Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrippadi Dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadi Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Moongilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Moongilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullanguzhal Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullanguzhal Dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodu Nikkamalae Heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodu Nikkamalae Heyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Vandhaal Kangal Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vandhaal Kangal Pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal Ingae Kaatru Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Ingae Kaatru Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullum Izhamai Mutham Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullum Izhamai Mutham Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Kaatrum Unnai Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Kaatrum Unnai Thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Vandhaal Kaigal Theendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vandhaal Kaigal Theendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Kooda Ellai Thaandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Kooda Ellai Thaandum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Idazhgal Kavithai Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Idazhgal Kavithai Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin Uyirae Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Uyirae Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravum Nee Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravum Nee Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin Azhagae Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin Azhagae Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagam Nee Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagam Nee Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae Ethu Putham Puthusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Ethu Putham Puthusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaadhal Parisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaadhal Parisu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vazhkai Perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vazhkai Perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Raani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethu Kaadhal Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu Kaadhal Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Micham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Micham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Boomiyae Sorgam Thaan Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Boomiyae Sorgam Thaan Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaadhalin Sorga Boomiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaadhalin Sorga Boomiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Kaadhalae Vazhum Satchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kaadhalae Vazhum Satchiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Vazhkaiyae Maatrum Vetriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vazhkaiyae Maatrum Vetriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sollamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sollamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isai Paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Ullathil Kaadhal Thulluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Ullathil Kaadhal Thulluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Megangal Pookkal Thoovuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Megangal Pookkal Thoovuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaatum Pudhu Padhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaatum Pudhu Padhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha Solluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazha Solluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aie Hey En Kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aie Hey En Kaadhalae"/>
</div>
</pre>
