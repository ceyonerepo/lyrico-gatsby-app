---
title: "ali ali haq ali song lyrics"
album: "Bhoomi 2020"
artist: "Salim Sulaiman"
lyricist: "Kamal Haji"
director: "Shakti Hasija"
path: "/albums/bhoomi-2020-lyrics"
song: "Ali Ali Haq Ali"
image: ../../images/albumart/bhoomi-2020.jpg
date: 2020-12-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/Ebt89uXDOzQ"
type: "happy"
singers:
  - Salim Merchant
  - Salman Ali
  - Vipul Mehta
  - Raj Pandit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ali zinda hai, ali maujood hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali zinda hai, ali maujood hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali, imamim muneen hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali, imamim muneen hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali hussain hai, zain-ul-aabdeen hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali hussain hai, zain-ul-aabdeen hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali hi haq hai, amir-ul-momineen hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali hi haq hai, amir-ul-momineen hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali ali ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkil kusha maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkil kusha maula ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, ya ali, haq ali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali ali haq, ali ali haq
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali haq, ali ali haq"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, maula ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali haq, ali ali haq
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali haq, ali ali haq"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, maula ali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere zikar mein main chalta raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere zikar mein main chalta raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere zikar mein main chalta raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere zikar mein main chalta raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss fikar mein main jalta raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss fikar mein main jalta raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh raaz phir mujh pe khula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh raaz phir mujh pe khula"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil mein dekha tu wahin mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil mein dekha tu wahin mila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Har zakhm ka, marham hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har zakhm ka, marham hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Har zakhm ka, marham hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har zakhm ka, marham hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Har dard ki hai tu dava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har dard ki hai tu dava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar de karam de de shifaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar de karam de de shifaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali maula ali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali ali ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkil kusha maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkil kusha maula ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, ya ali, haq ali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali ali haq, ali ali haq
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali haq, ali ali haq"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, maula ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali haq, ali ali haq
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali haq, ali ali haq"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, maula ali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere dar se jo maanga mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere dar se jo maanga mila"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere dar se jo maanga mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere dar se jo maanga mila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh nematon ka silsila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh nematon ka silsila"/>
</div>
<div class="lyrico-lyrics-wrapper">Chala mominon ka qaafila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chala mominon ka qaafila"/>
</div>
<div class="lyrico-lyrics-wrapper">Haq maangne tu de sila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haq maangne tu de sila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dua hai tu mannat hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dua hai tu mannat hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dua hai tu mannat hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dua hai tu mannat hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iss khaak ki jannat hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iss khaak ki jannat hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Himmat hai tu rehmat hai tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Himmat hai tu rehmat hai tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali maula ali...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali maula ali..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali ali ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushkil kusha maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushkil kusha maula ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, ya ali, haq ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, ya ali, haq ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, ya ali, haq ali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ali ali haq, ali ali haq
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali haq, ali ali haq"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, maula ali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali haq, ali ali haq
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali haq, ali ali haq"/>
</div>
<div class="lyrico-lyrics-wrapper">Ali ali, maula ali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ali ali, maula ali"/>
</div>
</pre>
