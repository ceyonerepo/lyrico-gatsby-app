---
title: "burberry aale bag song lyrics"
album: "Moosetape"
artist: "The Kidd"
lyricist: "Sidhu Moose Wala"
director: "Sidhu Moose Wala"
path: "/albums/moosetape-lyrics"
song: "Burberry aale bag"
image: ../../images/albumart/moosetape.jpg
date: 2021-05-15
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/h8r0z_q--b4"
type: "Mass"
singers:
  - Sidhu Moose Wala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aye yo the kidd!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye yo the kidd!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho kaali range kaale sheeshe aa nishani jatt ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kaali range kaale sheeshe aa nishani jatt ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali aa siyahi kaale geet likhda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali aa siyahi kaale geet likhda"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaan wangu veche na zameeran allhade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaan wangu veche na zameeran allhade"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhoti umar ch gabru da naa vikkda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhoti umar ch gabru da naa vikkda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh sidhu moose wala hor sheh mithiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh sidhu moose wala hor sheh mithiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaz kithe bediyan ch bond hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaz kithe bediyan ch bond hunde ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho dil chhati ch daler lalkare maarda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dil chhati ch daler lalkare maarda"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeli sheetan naal jihnu massa gajj da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeli sheetan naal jihnu massa gajj da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kharchila ghat usmaan matti da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kharchila ghat usmaan matti da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho duniya de utte jihda pota vajjda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho duniya de utte jihda pota vajjda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mard de matthe utte maut khooni ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mard de matthe utte maut khooni ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho lokaan de tan dollar te pound hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho lokaan de tan dollar te pound hunde ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne aye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne aye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh khabbe hath te down rolly mithiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh khabbe hath te down rolly mithiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sajji baanh te tattoo hatthi paaya kada sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajji baanh te tattoo hatthi paaya kada sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabb naal bhanneya ae kaal jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabb naal bhanneya ae kaal jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kha je jehda banda khada khada sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kha je jehda banda khada khada sohniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho lokaan saame launde jehde ne udariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho lokaan saame launde jehde ne udariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aihde muhre underground hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aihde muhre underground hunde ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir utte hunda ae kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda ae kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh pinde utte bane injh kaale rang de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh pinde utte bane injh kaale rang de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo zindagi ae kise di makaan sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo zindagi ae kise di makaan sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pairan ch pair nike airforce te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pairan ch pair nike airforce te"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinna thalle saara aa jahan sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinna thalle saara aa jahan sohniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaande fashion'an to darr aa trend'an vich ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaande fashion'an to darr aa trend'an vich ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanhiyon dekh lok saanu chakachaundh hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanhiyon dekh lok saanu chakachaundh hunde ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Sir utte hunda aa kafan jatt de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir utte hunda aa kafan jatt de"/>
</div>
<div class="lyrico-lyrics-wrapper">Burberry aale bag vich round hunde ne ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burberry aale bag vich round hunde ne ae"/>
</div>
</pre>
