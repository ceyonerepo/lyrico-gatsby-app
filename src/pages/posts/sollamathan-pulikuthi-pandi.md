---
title: "sollamathan song lyrics"
album: "Pulikuthi Pandi"
artist: "N R Raghunanthan"
lyricist: "Mohan Raj"
director: "M. Muthaiah"
path: "/albums/pulikuthi-pandi-song-lyrics"
song: "Sollamathan"
image: ../../images/albumart/pulikuthi-pandi.jpg
date: 2021-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LR0IvayH_sw"
type: "Love"
singers:
  - Srinisha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sollamathan solliputtan michcham illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamathan solliputtan michcham illama"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannu rendu aakkiputtan thookkam illam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannu rendu aakkiputtan thookkam illam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadu karai medellam achcham illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadu karai medellam achcham illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kaththavachan avan perai koocham illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kaththavachan avan perai koocham illama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chumma chumma kannadiya paakka vachane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumma chumma kannadiya paakka vachane"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu murai powder alli poosa vachane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu murai powder alli poosa vachane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitha vithama vekkapada kaththu thanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitha vithama vekkapada kaththu thanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Raga ragama kanavulathan muththam thanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raga ragama kanavulathan muththam thanthane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pambarama ennai avan aakkiputtane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pambarama ennai avan aakkiputtane"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan paarvaiyala katti vachu suththa vittane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paarvaiyala katti vachu suththa vittane"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey pambarama ennai avan aakkiputtane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pambarama ennai avan aakkiputtane"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan paarvaiyala katti vachu suththa vittane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paarvaiyala katti vachu suththa vittane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollamathan solliputtan michcham illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamathan solliputtan michcham illama"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannu rendu aakkiputtan thookkam illam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannu rendu aakkiputtan thookkam illam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaadu karai medellam achcham illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kaadu karai medellam achcham illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kaththavachan avan perai koocham illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kaththavachan avan perai koocham illama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan potta sattai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan potta sattai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pottu paakka aasai vanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pottu paakka aasai vanthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">En mundhaniyil avan mogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mundhaniyil avan mogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thottu paakka bodhai vanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thottu paakka bodhai vanthuchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyum kaalum thaan rekka aanuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyum kaalum thaan rekka aanuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam thaandi thaan poga thonuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam thaandi thaan poga thonuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyum kaalum thaan rekka aanuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyum kaalum thaan rekka aanuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam thaandi thaan poga thonuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam thaandi thaan poga thonuchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arana kodiyaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arana kodiyaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana suththi kedappane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana suththi kedappane"/>
</div>
<div class="lyrico-lyrics-wrapper">Arai nodi pirinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arai nodi pirinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan piththu pidippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan piththu pidippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan kaal azhaga paarthu paarthu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaal azhaga paarthu paarthu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigai nadhikaraiya katti vachanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai nadhikaraiya katti vachanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thozh azhaga paartha pinnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thozh azhaga paartha pinnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Varusa naattu malai chinnathunaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varusa naattu malai chinnathunaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyyo paiyapulla chekka sevappu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyyo paiyapulla chekka sevappu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum mudiyum thaan udambil karuppu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum mudiyum thaan udambil karuppu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyyo paiyapulla chekka sevappu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyyo paiyapulla chekka sevappu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum mudiyum thaan udambil karuppu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum mudiyum thaan udambil karuppu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagar theraattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagar theraattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nadaiyum irukkum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nadaiyum irukkum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayul muzhukka naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayul muzhukka naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nizhalla nadappen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nizhalla nadappen da"/>
</div>
</pre>
