---
title: "friendy da song lyrics"
album: "90 ML"
artist: "Silambarasan"
lyricist: "Mirchi Vijay"
director: "Anita Udeep"
path: "/albums/90-ml-lyrics"
song: "Friendy Da"
image: ../../images/albumart/90-ml.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CEWS9yoQyAM"
type: "friendship"
singers:
  - Aishwarya
  - Maria
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend In Need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend In Need"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Indeed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Indeed"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpala Life Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpala Life Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Trendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Amma Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Amma Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhayum Solla Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhayum Solla Mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Kettu Edhayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Kettu Edhayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Maraikka Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Maraikka Mattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Soga Kathaiya Kettu Mudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Soga Kathaiya Kettu Mudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Light’ah Oru Gap Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light’ah Oru Gap Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Kalaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Kalaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Kalangi Pakkam Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalangi Pakkam Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaajal Kuduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaajal Kuduppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusa Naalu Pasangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Naalu Pasangalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Intro Kuduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intro Kuduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennalum Engeyum Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalum Engeyum Eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkooda Nippa Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkooda Nippa Friendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend In Need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend In Need"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Indeed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Indeed"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpala Life Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpala Life Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Trendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friend’u Oruthi Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend’u Oruthi Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kanna Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perachanaiya Kandu Pidippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perachanaiya Kandu Pidippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashtathula Kooda Iruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtathula Kooda Iruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thozhiyavum Thozh Kuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thozhiyavum Thozh Kuduppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Natpu Mattum Kooda Irundha Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Natpu Mattum Kooda Irundha Pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Kitta Vandha Kooda Bayandhu Oodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Kitta Vandha Kooda Bayandhu Oodume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaiyum Kaanama Pogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaiyum Kaanama Pogume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Natpu Mattum Kooda Irundha Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Natpu Mattum Kooda Irundha Pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogam Kitta Vandha Kooda Bayandhu Oodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Kitta Vandha Kooda Bayandhu Oodume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai Kooda Ingu Kaanam Pogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Kooda Ingu Kaanam Pogume"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpala Life’eh Maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpala Life’eh Maarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaloda Thanniyil Vandhu Mothamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaloda Thanniyil Vandhu Mothamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppai Edukka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppai Edukka Mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpennum Urave Illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpennum Urave Illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Vazha Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Vazha Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ponnoda Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ponnoda Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Ponnukku Thaan Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ponnukku Thaan Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkum Keakka Aalu Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum Keakka Aalu Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu Illa Pugundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Illa Pugundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsam Panna Poromda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsam Panna Poromda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend In Need
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend In Need"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Indeed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Indeed"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Friendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Friendy Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpala Life Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpala Life Aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Trendy Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trendy Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Amma Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Amma Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhayum Solla Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhayum Solla Mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend Kettu Edhayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend Kettu Edhayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Maraikka Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Maraikka Mattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Soga Kathaiya Kettu Mudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Soga Kathaiya Kettu Mudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Light’ah Oru Gap Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light’ah Oru Gap Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Kalaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Kalaippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalukku Tholvi Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalukku Tholvi Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukku Ennaikkum Tholvi Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukku Ennaikkum Tholvi Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friendship’ai Konda Duvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship’ai Konda Duvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend’u Illatti Thindaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend’u Illatti Thindaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Color’ala Mozhiyala Madhamala Pirandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color’ala Mozhiyala Madhamala Pirandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpala Onnu Seruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpala Onnu Seruvom"/>
</div>
</pre>
