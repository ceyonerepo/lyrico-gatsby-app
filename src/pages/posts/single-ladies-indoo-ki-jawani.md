---
title: "single ladies song lyrics"
album: "Indoo Ki Jawani"
artist: "Rochak Kohli"
lyricist: "Gurpreet Saini - Gautam G Sharma"
director: "Abir Sengupta"
path: "/albums/indoo-ki-jawani-lyrics"
song: "Single Ladies"
image: ../../images/albumart/indoo-ki-jawani.jpg
date: 2020-12-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/qVNM42dFDKY"
type: "happy"
singers:
  - Rochak Kohli
  - SukhE
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Single ladies song, kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single ladies song, kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli kalli kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli kalli kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naughty naughty kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naughty naughty kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kalli kalli, single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kalli kalli, single ladies song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khao piyo aish karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khao piyo aish karo"/>
</div>
<div class="lyrico-lyrics-wrapper">Party koyi crash karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party koyi crash karo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek night ki fight hai baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek night ki fight hai baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Swipe karo hook up karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swipe karo hook up karo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single ladies song"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiyon thoda jee lo aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyon thoda jee lo aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari umar lo pee lo aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari umar lo pee lo aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Khatam huyi to wine shop se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khatam huyi to wine shop se"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaake khud pick up karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaake khud pick up karo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaj pajama party hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj pajama party hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyat thodi naughty hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyat thodi naughty hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl gang ikkatha karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl gang ikkatha karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarvjanik break up karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarvjanik break up karo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarvjanik break up karo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarvjanik break up karo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu ki main kithe jaake nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu ki main kithe jaake nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaru peeke saari raat tappna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaru peeke saari raat tappna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera jee kare ni ajj rukna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera jee kare ni ajj rukna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhalle dhuyein ke udana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhalle dhuyein ke udana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur gaana single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur gaana single ladies song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single ladies song, kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single ladies song, kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli kalli kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli kalli kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naughty naughty kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naughty naughty kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kalli kalli, single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kalli kalli, single ladies song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli kalli kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli kalli kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naughty naughty kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naughty naughty kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kalli kalli, single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kalli kalli, single ladies song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi kabhi mujhe tujh pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi kabhi mujhe tujh pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar aata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu khwabon mein hoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu khwabon mein hoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo taiyaar aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo taiyaar aata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan kabhi kabhi mujhe tujh pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan kabhi kabhi mujhe tujh pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar aata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu khwabon mein hoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu khwabon mein hoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo taiyaar aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo taiyaar aata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main gaati hun gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main gaati hun gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu guitar bajata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu guitar bajata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paas bulata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paas bulata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir sath mein gaata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir sath mein gaata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli kalli kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli kalli kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naughty naughty kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naughty naughty kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kalli kalli, single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kalli kalli, single ladies song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaad mein jaaye jo bhi tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaad mein jaaye jo bhi tere"/>
</div>
<div class="lyrico-lyrics-wrapper">Seh nahi sakta tantrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seh nahi sakta tantrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mohalle ki madonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mohalle ki madonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj na kariyo tu sharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj na kariyo tu sharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maar lipstick tu yaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maar lipstick tu yaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehange wala mascara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehange wala mascara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakht sakht launde bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakht sakht launde bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere aage ho jaayein naram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere aage ho jaayein naram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakht sakht launde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakht sakht launde"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakht sakht launde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakht sakht launde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu ki main kithe jaake nachna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu ki main kithe jaake nachna"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaru peeke saari raat tappna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaru peeke saari raat tappna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera jee kare ni ajj rukna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera jee kare ni ajj rukna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho saara bhull ke zamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho saara bhull ke zamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Main taan gaana single ladies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main taan gaana single ladies"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single ladies song, kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single ladies song, kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli kalli kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli kalli kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naughty naughty kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naughty naughty kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kalli kalli, single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kalli kalli, single ladies song"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli kalli kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli kalli kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho naughty naughty kudiyan da gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho naughty naughty kudiyan da gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kalli kalli sing single ladies song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kalli kalli sing single ladies song"/>
</div>
</pre>
