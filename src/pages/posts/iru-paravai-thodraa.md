---
title: "iru paravai song lyrics"
album: "Thodraa"
artist: "RN Uthamaraja - Navin Shander"
lyricist: "Kaniyur Varatharaj"
director: "Madhuraj"
path: "/albums/thodraa-lyrics"
song: "Iru Paravai"
image: ../../images/albumart/thodraa.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FpalUz8dEDc"
type: "melody"
singers:
  - Senthildass Velayutham
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">iru paravai irai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru paravai irai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">imaikkamal parakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikkamal parakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu ondru kai sera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu ondru kai sera "/>
</div>
<div class="lyrico-lyrics-wrapper">kai korthu alaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai korthu alaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal vaazhum thesathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vaazhum thesathai"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal kaatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal kaatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal kaatum paathaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal kaatum paathaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai neeluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai neeluma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iru paravai irai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru paravai irai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">imaikkamal parakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikkamal parakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu ondru kai sera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu ondru kai sera "/>
</div>
<div class="lyrico-lyrics-wrapper">kai korthu alaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai korthu alaigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valigal tholaiyum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valigal tholaiyum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">uthadu sirukkum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthadu sirukkum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">vegu thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegu thooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">vasantha naatkalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasantha naatkalin"/>
</div>
<div class="lyrico-lyrics-wrapper">varugai pathivetil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varugai pathivetil"/>
</div>
<div class="lyrico-lyrics-wrapper">iniyum peyar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iniyum peyar "/>
</div>
<div class="lyrico-lyrics-wrapper">ezhutha koodumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhutha koodumo"/>
</div>
<div class="lyrico-lyrics-wrapper">siragu thanthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragu thanthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam tharugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam tharugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">varangal mazhaiyagi pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varangal mazhaiyagi pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal sumantha vadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal sumantha vadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil theriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil theriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam karaiyil kondu serkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam karaiyil kondu serkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">thervalamaai oor varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thervalamaai oor varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarangale thunai varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarangale thunai varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">thani thaniai vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani thaniai vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">or uravai nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or uravai nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">pala jenmangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala jenmangal "/>
</div>
<div class="lyrico-lyrics-wrapper">thandi vazhnthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandi vazhnthiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iru paravai irai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru paravai irai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">imaikkamal parakirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaikkamal parakirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu ondru kai sera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu ondru kai sera "/>
</div>
<div class="lyrico-lyrics-wrapper">kai korthu alaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai korthu alaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal vaazhum thesathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vaazhum thesathai"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal kaatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal kaatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal kaatum paathaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal kaatum paathaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai neeluma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai neeluma"/>
</div>
</pre>
