---
title: "ninna nedu repu song lyrics"
album: "Bichagada Majaka"
artist: "Sri Venkat"
lyricist: "B. Chandra Sekhar (Pedda Babu)"
director: "K.S. Nageswara Rao"
path: "/albums/bichagada-majaka-lyrics"
song: "Ninna Nedu Repu"
image: ../../images/albumart/bichagada-majaka.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tJvPazORoc4"
type: "happy"
singers:
  - Anurag Kulakarni
  - Divya N
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninna nedu repu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna nedu repu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipesede love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipesede love"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anipinchede love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinchede love"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari gama padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari gama padha"/>
</div>
<div class="lyrico-lyrics-wrapper">Suswarale yedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suswarale yedu"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you ani piliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you ani piliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhaale ee moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaale ee moodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanulara choostene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulara choostene"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayaani taake love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayaani taake love"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvula varamisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvula varamisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirakalam niliche love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirakalam niliche love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mabbulo maatesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulo maatesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukalle raale love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukalle raale love"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila naina kariginche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila naina kariginche"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitrangi ee love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitrangi ee love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaline thaaketi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaline thaaketi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningidhi love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningidhi love"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerame chumbinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerame chumbinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alave love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alave love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choopule laekaluga maare love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopule laekaluga maare love"/>
</div>
<div class="lyrico-lyrics-wrapper">Mouname bhavamai poye love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouname bhavamai poye love"/>
</div>
<div class="lyrico-lyrics-wrapper">Swapnala veedullona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapnala veedullona"/>
</div>
<div class="lyrico-lyrics-wrapper">Harivillu virise love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harivillu virise love"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvella gaayam chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvella gaayam chese"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyani haaye love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyani haaye love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooranni deggara chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooranni deggara chese"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhyase pere ga love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhyase pere ga love"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashale swasaga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashale swasaga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhvathamaye love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhvathamaye love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasule sakshiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasule sakshiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubandhamayindi love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubandhamayindi love"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakshame nenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakshame nenani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Bandhuvayindi love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Bandhuvayindi love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna nedu repu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna nedu repu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipesede love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipesede love"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anipinchede love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinchede love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelo guvvala odhige love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelo guvvala odhige love"/>
</div>
<div class="lyrico-lyrics-wrapper">Endaale vennelaga maare love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endaale vennelaga maare love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dikkule okkatai piliche love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikkule okkatai piliche love"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkate praanamai niliche love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkate praanamai niliche love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaganaala dhaarullona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaganaala dhaarullona"/>
</div>
<div class="lyrico-lyrics-wrapper">Meghamai saage love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghamai saage love"/>
</div>
<div class="lyrico-lyrics-wrapper">Aananda dolkalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aananda dolkalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvella tadipe love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvella tadipe love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yugamule kshanamuga maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugamule kshanamuga maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalani aape love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalani aape love"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamantha nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamantha nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viharinchu haaye love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viharinchu haaye love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hrudayapu savvade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayapu savvade"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeethamanipinchu love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethamanipinchu love"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeethame nenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethame nenani"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagaalu palikinche love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagaalu palikinche love"/>
</div>
</pre>
