---
title: "one two three four song lyrics"
album: "Karimugan"
artist: "Chella Thangaiah"
lyricist: "unknown"
director: "Chella Thangaiah"
path: "/albums/karimugan-song-lyrics"
song: "One Two Three Four"
image: ../../images/albumart/karimugan.jpg
date: 2018-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HpDF53KCulY"
type: "happy"
singers:
  - Senthil Ganesh
  - linsi pransis
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">one two three four tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one two three four tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">five six seven eighti tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five six seven eighti tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva nayanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva nayanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">first meet la love varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first meet la love varala"/>
</div>
<div class="lyrico-lyrics-wrapper">first night um nee tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first night um nee tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva layanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva layanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paathum paakaththathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathum paakaththathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pol oru paarva nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol oru paarva nee"/>
</div>
<div class="lyrico-lyrics-wrapper">paathale vilagumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathale vilagumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en porva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en porva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa paathalum paakatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paathalum paakatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sethaalum sekkatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaalum sekkatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">eppathaan ennoda nee serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppathaan ennoda nee serva"/>
</div>
<div class="lyrico-lyrics-wrapper">eppathaan ennoda nee serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppathaan ennoda nee serva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">one two three four tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one two three four tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">five six seven eighti tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five six seven eighti tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva nayanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva nayanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">first meet la love varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first meet la love varala"/>
</div>
<div class="lyrico-lyrics-wrapper">first night um nee tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first night um nee tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva layanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva layanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna vittu tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vittu tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thotukiravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thotukiravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oon kittathula vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oon kittathula vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum ottikiravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum ottikiravaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">onna vittu kudupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna vittu kudupen"/>
</div>
<div class="lyrico-lyrics-wrapper">mella thottu kuduppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella thottu kuduppe"/>
</div>
<div class="lyrico-lyrics-wrapper">oon kitathula vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oon kitathula vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kaatipidipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kaatipidipe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasai irukku aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai irukku aana"/>
</div>
<div class="lyrico-lyrics-wrapper">maatengira en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatengira en"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaya sonna athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaya sonna athu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetta ungira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta ungira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh vetakaaraa onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh vetakaaraa onna"/>
</div>
<div class="lyrico-lyrics-wrapper">nerungi putten on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerungi putten on"/>
</div>
<div class="lyrico-lyrics-wrapper">setta therinju naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setta therinju naa"/>
</div>
<div class="lyrico-lyrics-wrapper">othungi kitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othungi kitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathal yutham varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathal yutham varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla nee kaama mutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla nee kaama mutham"/>
</div>
<div class="lyrico-lyrics-wrapper">tharanum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharanum pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kiss thanthaalum tharatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiss thanthaalum tharatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthalum varaatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthalum varaatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathaan ini nee vidamaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathaan ini nee vidamaata"/>
</div>
<div class="lyrico-lyrics-wrapper">ennathaan ini nee vidamaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennathaan ini nee vidamaata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">one two three four tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one two three four tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">five six seven eighti tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five six seven eighti tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva nayanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva nayanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">first meet la love varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first meet la love varala"/>
</div>
<div class="lyrico-lyrics-wrapper">first night um nee tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first night um nee tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva layanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva layanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasa arupathu naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa arupathu naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam mupathu naalu adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam mupathu naalu adi"/>
</div>
<div class="lyrico-lyrics-wrapper">aaga motham enaku pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaga motham enaku pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">thonooru naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thonooru naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un aasa paasam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aasa paasam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">verum moone maasam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verum moone maasam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vesam pottu naasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vesam pottu naasam"/>
</div>
<div class="lyrico-lyrics-wrapper">senja neeyum mosam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senja neeyum mosam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu nooru varusa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu nooru varusa "/>
</div>
<div class="lyrico-lyrics-wrapper">kanakku pulla naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakku pulla naa "/>
</div>
<div class="lyrico-lyrics-wrapper">noothanamaana kanakupulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noothanamaana kanakupulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee potta kanaku puriyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee potta kanaku puriyavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ketta kanakku mudiyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ketta kanakku mudiyavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neram kaalam varatum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram kaalam varatum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">namma nenacha kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma nenacha kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">tharattum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharattum pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee sonnalum sollatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonnalum sollatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthalum illatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthalum illatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan ona ini vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan ona ini vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaan ona ini vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaan ona ini vida maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">one two three four tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one two three four tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">five six seven eighti tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five six seven eighti tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva nayanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva nayanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">first meet la love varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first meet la love varala"/>
</div>
<div class="lyrico-lyrics-wrapper">first night um nee tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first night um nee tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva layanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva layanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paathum paakaththathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathum paakaththathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pol oru paarva nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pol oru paarva nee"/>
</div>
<div class="lyrico-lyrics-wrapper">paathale vilagumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathale vilagumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en porva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en porva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa paathalum paakatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa paathalum paakatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sethaalum sekkatiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaalum sekkatiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">eppathaan ennoda nee serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppathaan ennoda nee serva"/>
</div>
<div class="lyrico-lyrics-wrapper">eppathaan ennoda nee serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppathaan ennoda nee serva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">one two three four tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="one two three four tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">five six seven eighti tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="five six seven eighti tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva nayanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva nayanthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">first meet la love varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first meet la love varala"/>
</div>
<div class="lyrico-lyrics-wrapper">first night um nee tharala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="first night um nee tharala"/>
</div>
<div class="lyrico-lyrics-wrapper">nayanthaaraa iva layanthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanthaaraa iva layanthaaraa"/>
</div>
</pre>
