---
title: "bad boy song lyrics"
album: "Saaho"
artist: "Badshah"
lyricist: "Vignesh Shivan"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Bad Boy"
image: ../../images/albumart/saaho.jpg
date: 2019-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EAJCr5vvYz0"
type: "happy"
singers:
  - Badshah
  - Benny Dayal
  - Sunitha Sarathy	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Baby Hold Your Breathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Hold Your Breathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Breathe Breathe Breathe Breathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breathe Breathe Breathe Breathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Neluvaiyum Suzhivaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Neluvaiyum Suzhivaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivoda Gavanippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivoda Gavanippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Trust Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trust Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Wait A Sec
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Wait A Sec"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nerunga Nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nerunga Nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Norunguren Keranguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norunguren Keranguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Crush Me Crush Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crush Me Crush Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let Me Tease You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Tease You"/>
</div>
<div class="lyrico-lyrics-wrapper">Blaah Blaah Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blaah Blaah Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can Feel It…feel It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can Feel It…feel It"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nice-a Jaada Kudutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nice-a Jaada Kudutha"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can Read It…read It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can Read It…read It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathungura Maana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungura Maana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudippavan Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudippavan Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby I’m A Bad Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby I’m A Bad Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Can You Be My Bad Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can You Be My Bad Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby I’m A Bad Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby I’m A Bad Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Can You Be My Bad Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can You Be My Bad Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hi Baby So High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi Baby So High"/>
</div>
<div class="lyrico-lyrics-wrapper">Hi S T A Y F L Y
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi S T A Y F L Y"/>
</div>
<div class="lyrico-lyrics-wrapper">Fly Like A Helicopter Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fly Like A Helicopter Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m So Sexy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Sexy"/>
</div>
<div class="lyrico-lyrics-wrapper">Imma Called Them A Doctor Yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imma Called Them A Doctor Yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Cash No Cheques
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Cash No Cheques"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Stay Calm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Stay Calm"/>
</div>
<div class="lyrico-lyrics-wrapper">Let The Swag Flex
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let The Swag Flex"/>
</div>
<div class="lyrico-lyrics-wrapper">Always On The Move
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always On The Move"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby What’s Next
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby What’s Next"/>
</div>
<div class="lyrico-lyrics-wrapper">What If Boyfriend Become Your Ex
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What If Boyfriend Become Your Ex"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ragasiya Romeo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ragasiya Romeo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ragalaigal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ragalaigal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paakkuren Aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paakkuren Aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiya Pasiya Thoondura Nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya Pasiya Thoondura Nalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ragasiya Romeo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ragasiya Romeo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ragalaigal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ragalaigal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paakkuren Aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paakkuren Aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiya Pasiya Thoondura Nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya Pasiya Thoondura Nalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Paayum Puliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Paayum Puliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Paanjidu Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Paanjidu Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enai Vettai Aadidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enai Vettai Aadidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkira Maanae Naanae Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkira Maanae Naanae Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby I’m A Bad Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby I’m A Bad Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Can You Be My Bad Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can You Be My Bad Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Know You’re A Bad Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Know You’re A Bad Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can Be You Bad Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can Be You Bad Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby I’m A Bad Boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby I’m A Bad Boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Can You Be My Bad Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can You Be My Bad Girl"/>
</div>
</pre>
