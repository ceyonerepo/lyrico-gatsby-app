---
title: "neenda malare song lyrics"
album: "Kanne Kalaimane"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Seenu Ramasamy"
path: "/albums/kanne-kalaimane-lyrics"
song: "Neenda Malare"
image: ../../images/albumart/kanne-kalaimane.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-URUBr6Si88"
type: "love"
singers:
  - Shweta Pandit
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neenda Malare Neenda Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Malare Neenda Malare"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum Ennam Thonduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum Ennam Thonduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Perillatha Aasai Meeruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perillatha Aasai Meeruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollai Azhage Kollai Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Azhage Kollai Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Kolla Thonuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Kolla Thonuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Poga Ullam Yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Poga Ullam Yenguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Kalaimaane Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kalaimaane Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Nenju Katharuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Nenju Katharuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Unthan Perai Thavira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unthan Perai Thavira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Mozhiyum Azhiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Mozhiyum Azhiyuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutri Kolla Vendum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutri Kolla Vendum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutru Suzhal Maranthapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutru Suzhal Maranthapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorkal Ennai Kaividum Ullapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorkal Ennai Kaividum Ullapadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenda Malare Neenda Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Malare Neenda Malare"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum Ennam Thonduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum Ennam Thonduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Perillatha Aasai Meeruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perillatha Aasai Meeruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollai Azhage Kollai Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Azhage Kollai Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Kolla Thonuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Kolla Thonuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Poga Ullam Yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Poga Ullam Yenguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Enni Chalai Ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Enni Chalai Ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Kadanthe Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Kadanthe Pogindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhum Ezhum Paththu Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhum Ezhum Paththu Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Thappai Enninen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Thappai Enninen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhakku Enge Merku Engu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakku Enge Merku Engu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthu Poche Unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu Poche Unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Irukkum Thisai Ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irukkum Thisai Ellame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhakku Endre Kaanuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakku Endre Kaanuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vergalil Neeragirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vergalil Neeragirai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pookalil Theanagirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pookalil Theanagirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Innum Enna Seiya Pogirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Innum Enna Seiya Pogirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimaiyaga Thooimai Seigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaiyaga Thooimai Seigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Parvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Parvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kondru Inbam Seigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kondru Inbam Seigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kundru Pole Nindru Nimirgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundru Pole Nindru Nimirgindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Parthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Parthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundri Maniyai Kundri Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundri Maniyai Kundri Pogindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenda Malare Neenda Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda Malare Neenda Malare"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum Ennam Thonduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum Ennam Thonduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Perillatha Aasai Meeruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perillatha Aasai Meeruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollai Azhage Kollai Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Azhage Kollai Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Kolla Thonuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Kolla Thonuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Poga Ullam Yenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Poga Ullam Yenguthe"/>
</div>
</pre>
