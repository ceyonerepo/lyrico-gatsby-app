---
title: "dil bandar song lyrics"
album: "Darbaan"
artist: "Amartya Bobo Rahut"
lyricist: "Siddhant Kaushal"
director: "Bipin Nadkarni"
path: "/albums/darbaan-lyrics"
song: "Dil Bandar"
image: ../../images/albumart/darbaan.jpg
date: 2020-12-04
lang: hindi
youtubeLink: "https://www.youtube.com/embed/E9nzVwICthY"
type: "happy"
singers:
  - Tushar Joshi
  - Amartya Bobo Rahut
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chakar sthalon chikur tikur dil bhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chakar sthalon chikur tikur dil bhaage"/>
</div>
<div class="lyrico-lyrics-wrapper">bhagad dum oonl uthal kar jaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhagad dum oonl uthal kar jaage"/>
</div>
<div class="lyrico-lyrics-wrapper">he chakar bhoomi chikur tikur dil bhaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he chakar bhoomi chikur tikur dil bhaage"/>
</div>
<div class="lyrico-lyrics-wrapper">bhagad dum oonl uthal kar jaage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhagad dum oonl uthal kar jaage"/>
</div>
<div class="lyrico-lyrics-wrapper">dil bandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil bandar"/>
</div>
<div class="lyrico-lyrics-wrapper">mastiyon ka ek samandar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mastiyon ka ek samandar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">furaasaton ka bhee kalandar ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="furaasaton ka bhee kalandar ta"/>
</div>
<div class="lyrico-lyrics-wrapper">baith ja re o bavandar too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baith ja re o bavandar too"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dil bandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil bandar"/>
</div>
<div class="lyrico-lyrics-wrapper">mastiyon ka ek samandar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mastiyon ka ek samandar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">furaasaton ka bhee kalandar ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="furaasaton ka bhee kalandar ta"/>
</div>
<div class="lyrico-lyrics-wrapper">baith ja re o bavandar too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baith ja re o bavandar too"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ghode bech sota mein bhee hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghode bech sota mein bhee hai"/>
</div>
<div class="lyrico-lyrics-wrapper">sota he jaise bhaaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sota he jaise bhaaloo"/>
</div>
<div class="lyrico-lyrics-wrapper">be tera nakhare saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="be tera nakhare saare"/>
</div>
<div class="lyrico-lyrics-wrapper">main kaise bataoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main kaise bataoon"/>
</div>
<div class="lyrico-lyrics-wrapper">sir khae ja jana ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sir khae ja jana ka"/>
</div>
<div class="lyrico-lyrics-wrapper">mandarae jhunajhuna sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandarae jhunajhuna sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ghumakkad idhar udhar dil ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ghumakkad idhar udhar dil ho"/>
</div>
<div class="lyrico-lyrics-wrapper">latak kar khusur pusur ripa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latak kar khusur pusur ripa"/>
</div>
<div class="lyrico-lyrics-wrapper">ai .. ghumakkad idhar udhar dil ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ai .. ghumakkad idhar udhar dil ho"/>
</div>
<div class="lyrico-lyrics-wrapper">latak kar khusur pusur ripa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latak kar khusur pusur ripa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dil bandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil bandar"/>
</div>
<div class="lyrico-lyrics-wrapper">mastiyon ka ek samandar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mastiyon ka ek samandar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">furaasaton ka bhee kalandar ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="furaasaton ka bhee kalandar ta"/>
</div>
<div class="lyrico-lyrics-wrapper">baith ja re o bavandar too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baith ja re o bavandar too"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dil bandar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil bandar"/>
</div>
<div class="lyrico-lyrics-wrapper">mastiyon ka ek samandar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mastiyon ka ek samandar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">furaasaton ka bhee kalandar ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="furaasaton ka bhee kalandar ta"/>
</div>
<div class="lyrico-lyrics-wrapper">baith ja re o bavandar too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baith ja re o bavandar too"/>
</div>
</pre>
