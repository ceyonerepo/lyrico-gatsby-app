---
title: "sigarame song lyrics"
album: "Raatchasi"
artist: "Sean Roldan"
lyricist: "Sean Roldan"
director: "Sy Gowthamraj"
path: "/albums/raatchasi-lyrics"
song: "Sigarame"
image: ../../images/albumart/raatchasi.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2l0K97-7grk"
type: "happy"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sigaramae pidikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaramae pidikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti pidithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti pidithidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilae thakidathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae thakidathaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adithidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sigaramae pidikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaramae pidikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti pidithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti pidithidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilae thakidathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae thakidathaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adithidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virumbum kanavai kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbum kanavai kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai ninaivaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai ninaivaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamum pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamum pirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai sari paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai sari paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmam therindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmam therindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai payanaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai payanaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam thodangum ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam thodangum ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sigaramae pidikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaramae pidikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti pidithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti pidithidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilae thakidathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae thakidathaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adithidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sigaramae sigaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaramae sigaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikathaan pidikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikathaan pidikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti pidithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti pidithidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilae manadhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae manadhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakidathaam thakidathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakidathaam thakidathaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adithidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaatha naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaatha naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhodi vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhodi vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaala thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaala thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru koodalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru koodalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palarum sernthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palarum sernthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Balamum serum thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balamum serum thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku yethu ethiraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku yethu ethiraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee melae yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee melae yeri vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaatha naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaatha naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhodi vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhodi vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaala thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaala thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru koodalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru koodalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palarum sernthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palarum sernthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Balamum serum thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balamum serum thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku yethu ethiraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku yethu ethiraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee melae yeri vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee melae yeri vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virumbum kanavai kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbum kanavai kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai ninaivaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai ninaivaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamum pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamum pirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhai sari paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhai sari paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmam therindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmam therindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai payanaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai payanaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam thodangum ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam thodangum ho ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sigaramae pidikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaramae pidikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti pidithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti pidithidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilae thakidathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae thakidathaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adithidu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sigaramae sigaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaramae sigaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikathaan pidikathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikathaan pidikathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti pidithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti pidithidu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhilae manadhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhilae manadhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakidathaam thakidathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakidathaam thakidathaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adithidu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adithidu nee"/>
</div>
</pre>
