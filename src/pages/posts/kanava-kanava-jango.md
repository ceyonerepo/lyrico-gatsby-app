---
title: "kanava kanava song lyrics"
album: "Jango"
artist: "Ghibran"
lyricist: "Sri Devi"
director: "Mano Karthikeyan"
path: "/albums/jango-lyrics"
song: "Kanava Kanava"
image: ../../images/albumart/jango.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/z4EiGHt3ZCU"
type: "mass"
singers:
  - Namitha Babu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhayathil iruppavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil iruppavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkena piranthavanae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena piranthavanae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalodu kaadhalaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalodu kaadhalaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaivittu nadakkaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaivittu nadakkaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thottu kadappavanae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thottu kadappavanae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum paadhi naanum paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum paadhi naanum paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaaga en uyirae neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaaga en uyirae neeyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanava kanava en kanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava kanava en kanava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil kaanum ninaivae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil kaanum ninaivae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaa uravaa en uravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaa uravaa en uravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril serndhida vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril serndhida vaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruginil varugaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil varugaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli kuraigiradhae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli kuraigiradhae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kannil ennai kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kannil ennai kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyadhu vidai tharudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyadhu vidai tharudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiyadhu uyir tharudhae.. naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiyadhu uyir tharudhae.. naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandu ullam sera en manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandu ullam sera en manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaaga un gunamae naanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaaga un gunamae naanaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhama idhama un nidama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhama idhama un nidama"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam serndhae karaindhiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam serndhae karaindhiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamaa varamaa en vasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamaa varamaa en vasamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhil vaazhndhidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil vaazhndhidumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai azhagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaigiren aasaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaigiren aasaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragaai idhayam virigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragaai idhayam virigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaa vilagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaa vilagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumanam serathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumanam serathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravaai pagalaai thavikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaai pagalaai thavikkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa kanaa kannoaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa kanaa kannoaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaa vinaa nenjoaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaa vinaa nenjoaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaamalae sellaadhae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaamalae sellaadhae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaakkal pogum saalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaakkal pogum saalaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathil iruppavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil iruppavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkena piranthavanae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena piranthavanae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalodu kaadhalaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalodu kaadhalaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaivittu nadakkaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaivittu nadakkaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thottu kadappavanae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thottu kadappavanae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum paadhi naanum paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum paadhi naanum paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaaga en uyirae neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaaga en uyirae neeyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanava kanava en kanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava kanava en kanava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil kaanum ninaivae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil kaanum ninaivae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaa uravaa en uravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaa uravaa en uravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril serndhida vaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril serndhida vaaa"/>
</div>
</pre>
