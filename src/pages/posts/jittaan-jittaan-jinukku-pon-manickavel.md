---
title: "jittaan jittaan jinukku song lyrics"
album: "Pon Manickavel"
artist: "D. Imman"
lyricist: "GKB"
director: "A.C. Mugil Chellappan"
path: "/albums/pon-manickavel-lyrics"
song: "Jittaan Jittaan Jinukku"
image: ../../images/albumart/pon-manickavel.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UjESCcflEAk"
type: "happy"
singers:
  - Shika Prabhakaran
  - Sukumar Rajendran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jinukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanasa Sanjararae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanasa Sanjararae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manasilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanasa Sanjararae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanasa Sanjararae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ikkada Soodu Paavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkada Soodu Paavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Hippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Hippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetinga Poda Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetinga Poda Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkoda Marujenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkoda Marujenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mothamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mothamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Peru Thookkang Kettaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Peru Thookkang Kettaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppula Thenamunthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppula Thenamunthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthusaa Venum Sulukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthusaa Venum Sulukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucha Macham Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucha Macham Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamanukkunu Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamanukkunu Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaampadisanu Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaampadisanu Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichu Norukkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Norukkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattilukkunu Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattilukkunu Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenthu Vittava Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenthu Vittava Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirichu Maeyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirichu Maeyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enakku Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enakku Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Visayam Mothamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visayam Mothamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evalukkirukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalukkirukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenam Munnukkum Pinnukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenam Munnukkum Pinnukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaengi Nikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaengi Nikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Thavikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Thavikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usuroda Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuroda Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasaamathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasaamathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidunchaalum Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidunchaalum Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamaatten Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamaatten Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu Katha Thodarattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Katha Thodarattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiya Mudiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiya Mudiyathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucha Macham Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucha Macham Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanasa Sanjararae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanasa Sanjararae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manasilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanasa Sanjararae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanasa Sanjararae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ikkada Soodu Paavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkada Soodu Paavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Hippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Hippula"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetinga Poda Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetinga Poda Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkooda Marujenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkooda Marujenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mothamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mothamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Peru Thookkangkettaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Peru Thookkangkettaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppula Thenamunthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppula Thenamunthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthusaa Venum Sulukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthusaa Venum Sulukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucha Macham Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucha Macham Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jittaan Jittaan Jinukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jittaan Jittaan Jinukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ucha Macham Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ucha Macham Unakku"/>
</div>
</pre>
