---
title: "paye paye song lyrics"
album: "Falaknuma Das"
artist: "Vivek Sagar"
lyricist: "Suddala Ashok Teja"
director: "Vishwak Sen"
path: "/albums/falaknuma-das-lyrics"
song: "Paye Paye"
image: ../../images/albumart/falaknuma-das.jpg
date: 2019-05-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/AYRCmAKmVSo"
type: "happy"
singers:
  - Shiva Nagulu
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raju Atu Rani Itu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju Atu Rani Itu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadinde Aata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadinde Aata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju Atu Rani Itu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju Atu Rani Itu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadinde Aata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadinde Aata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaajulitu Gajjelatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaajulitu Gajjelatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadinde Paata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadinde Paata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaajulitu Gajjelatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaajulitu Gajjelatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadinde Paata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadinde Paata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukka Itu Sukka Atu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukka Itu Sukka Atu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkinde Kickuu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkinde Kickuu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukka Itu Sukka Atu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukka Itu Sukka Atu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkinde Kickuu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkinde Kickuu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Neeku Adi Naaku Idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Neeku Adi Naaku Idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkinde Luckuu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkinde Luckuu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Neeku Adi Naaku Idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Neeku Adi Naaku Idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkinde Luckuu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkinde Luckuu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Joru Jorunde Poraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joru Jorunde Poraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nawaabu Vaitavu Soodiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nawaabu Vaitavu Soodiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Taapu Lepedi Nuvvu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taapu Lepedi Nuvvu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Gapu Lekunda Kummara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Gapu Lekunda Kummara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Lucky Handunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Lucky Handunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathale Queue Kadatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathale Queue Kadatai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lucky Handunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lucky Handunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathale Queue Kadatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathale Queue Kadatai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimanodu Heartilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimanodu Heartilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavarlu Lai Lestay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavarlu Lai Lestay"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimanodu Heartilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimanodu Heartilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavarlu Lai Lestay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavarlu Lai Lestay"/>
</div>
<div class="lyrico-lyrics-wrapper">Rummu Thone Damme Kodithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rummu Thone Damme Kodithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Rummy Aata Naade Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rummy Aata Naade Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Okati Moodu Padamoodu Aithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okati Moodu Padamoodu Aithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Lucky Fouru Soosko Bey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucky Fouru Soosko Bey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvu Aadinde Aata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Aadinde Aata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadinde Paata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadinde Paata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Ekkinde Kicku Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Ekkinde Kicku Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkinde Lucku Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkinde Lucku Ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andar Lo Baahar Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andar Lo Baahar Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Vaipu Nenunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Vaipu Nenunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Andar Lo Baahar Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andar Lo Baahar Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Vaipu Nuvvunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Vaipu Nuvvunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarlo Gambling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarlo Gambling"/>
</div>
<div class="lyrico-lyrics-wrapper">Khiladi Nene Bey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khiladi Nene Bey"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarlo Gambling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarlo Gambling"/>
</div>
<div class="lyrico-lyrics-wrapper">Khiladi Nuvve Bey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khiladi Nuvve Bey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jerripothu Laga Unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jerripothu Laga Unde"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarra Chudu Danarekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarra Chudu Danarekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nede Kaadu Repu Kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nede Kaadu Repu Kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Nene Gadi Lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Nene Gadi Lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pille Pellamayi Vele Pattinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pille Pellamayi Vele Pattinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapai Freedomae Govindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapai Freedomae Govindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru Daataka Intikochava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Daataka Intikochava"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedroom Avtundi Kishkinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedroom Avtundi Kishkinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Bachelor Ga Unte Baava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bachelor Ga Unte Baava"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Datingu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Datingu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Joru Jorunde Poraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joru Jorunde Poraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nawaabu Vaitavu Soodiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nawaabu Vaitavu Soodiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Taapu Lepedi Nuvvu Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taapu Lepedi Nuvvu Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Gapu Lekunda Kummara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Gapu Lekunda Kummara"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Paaye Paaye Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaye Paaye Paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Antha Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Antha Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaye Daaye Daaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaye Daaye Daaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri Kaanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri Kaanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvu Aadinde Aata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Aadinde Aata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadinde Paata Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadinde Paata Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Ekkinde Kicku Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Ekkinde Kicku Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkinde Lucku Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkinde Lucku Ra"/>
</div>
</pre>
