---
title: "achamillai song lyrics"
album: "Hey Sinamika"
artist: "Govind Vasantha"
lyricist: "Madhan Karky"
director: "Brinda"
path: "/albums/hey-sinamika-lyrics"
song: "Achamillai"
image: ../../images/albumart/hey-sinamika.jpg
date: 2022-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Buav7NychK4"
type: "mass"
singers:
  - Dulquer Salmaan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey mummy tummy kullayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mummy tummy kullayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neendhum bothu acham illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neendhum bothu acham illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boom boom nee kan muzhichu paathadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom boom nee kan muzhichu paathadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ellamae ellamae achathin aatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi ellamae ellamae achathin aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">LKG seat-u kettu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="LKG seat-u kettu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Interview appo start-u thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Interview appo start-u thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seat-u kedacha pinna rekka udachu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seat-u kedacha pinna rekka udachu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uniform ah maattivittu school-u kulla parakka vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uniform ah maattivittu school-u kulla parakka vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettu paadam thandhu roast panni toast panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettu paadam thandhu roast panni toast panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty idhayatha force panni waste panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty idhayatha force panni waste panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu vayasula rank panni fail panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu vayasula rank panni fail panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Crack-ah jack-ah crack-ah jack-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crack-ah jack-ah crack-ah jack-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo pola ready panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo pola ready panni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna thozh mela meratti meratti mootta yetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna thozh mela meratti meratti mootta yetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu vettaikkaaga veratti veratti route-ah maatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu vettaikkaaga veratti veratti route-ah maatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veera thozha urichu urichu acham ootta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veera thozha urichu urichu acham ootta"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai paada solli thalaiyil kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai paada solli thalaiyil kotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah fear fear fear kan muzhikka fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah fear fear fear kan muzhikka fear fear"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah fear fear fear road-il poga fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah fear fear fear road-il poga fear fear"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah fear fear fear school ninaichu fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah fear fear fear school ninaichu fear fear"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah fear fear fear teacher-ku torture-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah fear fear fear teacher-ku torture-ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Future-ku fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Future-ku fear fear"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah fear fear fear ponna paakka fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah fear fear fear ponna paakka fear fear"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah fear fear fear pakkam poga fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah fear fear fear pakkam poga fear fear"/>
</div>
<div class="lyrico-lyrics-wrapper">Fear fear fear kaadhal solla fear fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fear fear fear kaadhal solla fear fear"/>
</div>
<div class="lyrico-lyrics-wrapper">Fear fear fear uchi meedhu vaanidindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fear fear fear uchi meedhu vaanidindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhugindra podhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhugindra podhilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhappathil ooru bhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhappathil ooru bhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinjathum ooru bhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinjathum ooru bhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagame bhaya mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagame bhaya mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongum bothu kanavula oru bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongum bothu kanavula oru bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjadhum oru bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjadhum oru bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamae baya maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae baya maayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu panam illa naalum oru bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu panam illa naalum oru bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha pinnae oru bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha pinnae oru bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamae baya mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae baya mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalikka aalu yaarum illaiyunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalikka aalu yaarum illaiyunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha aalum odirumnnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha aalum odirumnnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi neram bakku bakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi neram bakku bakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi neram dhikku dhikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi neram dhikku dhikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pei pisaasaa innoru virus-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei pisaasaa innoru virus-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu viyadhiya. arasiyal vaadhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu viyadhiya. arasiyal vaadhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Stock market-ah pick pocket-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stock market-ah pick pocket-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Terrorist-ah malligai list-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terrorist-ah malligai list-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Interview-ah movie queue-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Interview-ah movie queue-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Venda kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venda kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa acham vida ucham thoda vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa acham vida ucham thoda vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai illai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai illai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai achamillai acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai achamillai acham enbathillayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham enbathillayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham enbathillayae"/>
</div>
</pre>
