---
title: "vaarome kanavodu song lyrics"
album: "Marudhavelu"
artist: "James Vasanthan"
lyricist: "Mohan Raj"
director: "R.K.R. Aathimoolam"
path: "/albums/marudhavelu-lyrics"
song: "Vaarome Kanavodu"
image: ../../images/albumart/marudhavelu.jpg
date: 2011-11-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZchT2_wiv0Y"
type: "sad"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaaroam kanaavoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaroam kanaavoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">poarom vinaavoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poarom vinaavoadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaroam kanaavoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaroam kanaavoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">poarom vinaavoadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poarom vinaavoadu"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil vaazhuroam sila kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil vaazhuroam sila kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">poalamburoam pala kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poalamburoam pala kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">joaraaga vaazhunganney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="joaraaga vaazhunganney"/>
</div>
<div class="lyrico-lyrics-wrapper">O mannoadu serummunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mannoadu serummunney"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaroam kannaavoadu vaazhum boomikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaroam kannaavoadu vaazhum boomikki"/>
</div>
<div class="lyrico-lyrics-wrapper">kondaattam poadungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaattam poadungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O vinaavoadu vaazha sinukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O vinaavoadu vaazha sinukki"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangaama vaazhungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangaama vaazhungadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boomiyil vaazhuroam sila kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil vaazhuroam sila kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">poalamburoam pala kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poalamburoam pala kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">joaraaga vaazhunganney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="joaraaga vaazhunganney"/>
</div>
<div class="lyrico-lyrics-wrapper">O mannoadu serummunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mannoadu serummunney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anboadu vaazhum vaazhkkaiya naamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anboadu vaazhum vaazhkkaiya naamum"/>
</div>
<div class="lyrico-lyrics-wrapper">vamboadu vaazhndhu vaaduroam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vamboadu vaazhndhu vaaduroam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaandhu naamum poaguravaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaandhu naamum poaguravaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">manda kanathil vaazhuroam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manda kanathil vaazhuroam"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku piragum indha boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku piragum indha boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku piragum indha boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku piragum indha boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">maarippoagaama endrumey idhupoal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarippoagaama endrumey idhupoal"/>
</div>
<div class="lyrico-lyrics-wrapper">joaraaga vaazhunganney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="joaraaga vaazhunganney"/>
</div>
<div class="lyrico-lyrics-wrapper">O mannoadu serummunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mannoadu serummunney"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyiley yaarukku soagamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyiley yaarukku soagamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarukku baaramilla joaraaga vaazhunganney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukku baaramilla joaraaga vaazhunganney"/>
</div>
<div class="lyrico-lyrics-wrapper">O mannoadu serummunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mannoadu serummunney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammoada manasu pachoandhi thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoada manasu pachoandhi thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru naalum maaruroam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru naalum maaruroam"/>
</div>
<div class="lyrico-lyrics-wrapper">muttayapoala vaazhkkaiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttayapoala vaazhkkaiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">attaiyaa adhula ooruroam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="attaiyaa adhula ooruroam"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhudha vizhungiya vedhaiyappoaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhudha vizhungiya vedhaiyappoaley"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhudha vizhungiya vedhaiyappoaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhudha vizhungiya vedhaiyappoaley"/>
</div>
<div class="lyrico-lyrics-wrapper">namakkul irukkura vidhiyathaan marakkuroam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakkul irukkura vidhiyathaan marakkuroam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">joaraaga vaazhunganney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="joaraaga vaazhunganney"/>
</div>
<div class="lyrico-lyrics-wrapper">O mannoadu serummunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mannoadu serummunney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkai enbadhu ezhubadhu varusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai enbadhu ezhubadhu varusham"/>
</div>
<div class="lyrico-lyrics-wrapper">thookkathil irubadhu poagumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkathil irubadhu poagumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vettipechil vambu vazhakkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettipechil vambu vazhakkil"/>
</div>
<div class="lyrico-lyrics-wrapper">melum irubadhu poagumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melum irubadhu poagumey"/>
</div>
<div class="lyrico-lyrics-wrapper">meedhi iruppadhu muppadhu thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meedhi iruppadhu muppadhu thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">saadhi sandaigal adhukkul enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saadhi sandaigal adhukkul enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">joaraaga vaazhunganney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="joaraaga vaazhunganney"/>
</div>
<div class="lyrico-lyrics-wrapper">O mannoadu serummunney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mannoadu serummunney"/>
</div>
</pre>
