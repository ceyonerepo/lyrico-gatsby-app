---
title: "enga kaiyila song lyrics"
album: "Adutha Saattai"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "M. Anbazhagan"
path: "/albums/adutha-saattai-lyrics"
song: "Enga Kaiyila"
image: ../../images/albumart/adutha-saattai.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zDYTYh-GNg4"
type: "happy"
singers:
  - Chellankuppam Subramaniyan
  - Lady Kash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enga Kaiyila Naatta Kudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kaiyila Naatta Kudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaththaiyum Maaththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaththaiyum Maaththurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Vaangi Yemaththaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Vaangi Yemaththaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala Mela yeththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala Mela yeththurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenga Enga Kaiyila…Naatta Kudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenga Enga Kaiyila…Naatta Kudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaththaiyum Maaththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaththaiyum Maaththurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Vaangi Yemaththaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Vaangi Yemaththaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala Mela Yeththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala Mela Yeththurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Over Nightil Karuppu Panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Over Nightil Karuppu Panam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyumnu Muzhangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyumnu Muzhangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenga Vaari Kuduththa Variyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenga Vaari Kuduththa Variyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli Naatta Suththa Kelambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli Naatta Suththa Kelambala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tassu maakka Thoranthu Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tassu maakka Thoranthu Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhainga Thaliya Arukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhainga Thaliya Arukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Ponnum Paiyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Ponnum Paiyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Doctor-ava Neet’u Therva Nadaththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doctor-ava Neet’u Therva Nadaththala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ariya Periya Thittam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariya Periya Thittam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkal Naatti Niruththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkal Naatti Niruththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Saathi Mathaththa Thoondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Saathi Mathaththa Thoondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Sananga Usura Edukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Sananga Usura Edukkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viyasaya Nelaththa Pudungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyasaya Nelaththa Pudungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesaththa Nanga Kalakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesaththa Nanga Kalakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Vada Naattu Company-kaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vada Naattu Company-kaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkala Suttu Posukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkala Suttu Posukkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Kaiyila… Neenga Enga Kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kaiyila… Neenga Enga Kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Enga Kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Enga Kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kaiyila Naatta Kudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kaiyila Naatta Kudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaththaiyum Maaththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaththaiyum Maaththurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Vaangi Yemaththama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Vaangi Yemaththama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala Mela Yeththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala Mela Yeththurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothuma Intha Koduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuma Intha Koduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Naattuku Vantha Nelama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Naattuku Vantha Nelama"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaruma Inga Varuma Vali Theeruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaruma Inga Varuma Vali Theeruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Puratchi Pirakkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puratchi Pirakkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Muzhukka Paravi Kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Muzhukka Paravi Kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhan Nilama Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Nilama Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Iyarkai Valaththa Surandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Iyarkai Valaththa Surandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhuththa Thiruttu Narigal Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhuththa Thiruttu Narigal Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ottuku Panamum Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ottuku Panamum Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Osila Kodukka Vendaam…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Osila Kodukka Vendaam…"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Ilaiya Thalaimuraikku Unga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Ilaiya Thalaimuraikku Unga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaiya Kathaiyum Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaiya Kathaiyum Vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaennamo Solli Namma Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaennamo Solli Namma Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellatha Kaasukku Enna Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellatha Kaasukku Enna Konna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichchi Therinchiduchi Purichiduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichchi Therinchiduchi Purichiduchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Reel’u Anthuduchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reel’u Anthuduchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sungasavadi Pera Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sungasavadi Pera Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi pariyum Nadaththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi pariyum Nadaththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanga Yeri Kulaththa Akkiramichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanga Yeri Kulaththa Akkiramichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethi Mandram Thorakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethi Mandram Thorakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vangi Panaththa Abaes Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi Panaththa Abaes Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthalali ku Kodukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalali ku Kodukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanga Kalvi Kadana Adaikka Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanga Kalvi Kadana Adaikka Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Kazhuththa Nerikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Kazhuththa Nerikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalai kazhiva Thiranthu Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalai kazhiva Thiranthu Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelaththa Paazhu Paduththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaththa Paazhu Paduththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanga Alavillama Velaya Yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanga Alavillama Velaya Yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaru Vera Avukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaru Vera Avukkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandavan Kitta Kaasu Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandavan Kitta Kaasu Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchiya Naanga Valakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchiya Naanga Valakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Pathavikaga Katchiya Nadaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Pathavikaga Katchiya Nadaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarisa Thukki Pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarisa Thukki Pudikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Kaiyila… Neenga Enga Kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kaiyila… Neenga Enga Kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Enga Kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Enga Kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta Kudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Kudunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaththaiyum Maaththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaththaiyum Maaththurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Vaangi Yemaththaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Vaangi Yemaththaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala Mela Yeththurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala Mela Yeththurom"/>
</div>
</pre>
