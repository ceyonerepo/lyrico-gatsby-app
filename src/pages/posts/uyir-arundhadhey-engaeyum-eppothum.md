---
title: "uyir arundhadhey song lyrics"
album: "Engaeyum Eppothum"
artist: "C Sathya"
lyricist: "M Saravanan"
director: "M. Saravanan"
path: "/albums/engaeyum-eppothum-lyrics"
song: "Uyir Arundhadhey"
image: ../../images/albumart/engaeyum-eppothum.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h2AneEuEB9s"
type: "sad"
singers:
  - Sayanora
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyir arunthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir arunthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal vizhunthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal vizhunthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravizhanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravizhanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam marukkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam marukkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranaththai kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranaththai kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvarum illaiNaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvarum illaiNaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappinai vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappinai vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravanum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu onbathu vaasal veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu onbathu vaasal veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinbu eesan aalum kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinbu eesan aalum kaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir arunthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir arunthathae"/>
</div>
</pre>
