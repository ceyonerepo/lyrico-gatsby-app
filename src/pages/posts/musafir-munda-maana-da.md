---
title: "musafir song lyrics"
album: "Munda Maana Da"
artist: "Desi Crew"
lyricist: "Korala Maan"
director: "Parm Chahal"
path: "/albums/munda-maana-da-lyrics"
song: "Musafir"
image: ../../images/albumart/munda-maana-da.jpg
date: 2021-03-24
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/m_CGSc6UQuw"
type: "Mass"
singers:
  - Korala Maan
  - Gurlej Akhtar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Desi crew! Desi crew!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi crew! Desi crew!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve maithon time jatt paa ni hunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve maithon time jatt paa ni hunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Te taithon time utte aa ni hunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te taithon time utte aa ni hunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve maithon time jatt paa ni hunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve maithon time jatt paa ni hunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Te taithon time utte aa ni hunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te taithon time utte aa ni hunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na teri surat thikane rehndi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na teri surat thikane rehndi ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimaag dass kehdi vi chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimaag dass kehdi vi chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve mainu sacho sacchi das chobbran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve mainu sacho sacchi das chobbran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera kithe kithe ki chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera kithe kithe ki chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu sacho sacchi das chobbran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu sacho sacchi das chobbran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera kithe kithe ki chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera kithe kithe ki chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paule hatth rakh lakk utte ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paule hatth rakh lakk utte ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Addi rehni ae tu shaq utte ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addi rehni ae tu shaq utte ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Paule hatth rakh lakk utte ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paule hatth rakh lakk utte ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Addi rehni ae tu shaq utte ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addi rehni ae tu shaq utte ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dekh taan vi teri padhe haaziri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh taan vi teri padhe haaziri"/>
</div>
<div class="lyrico-lyrics-wrapper">Phavein gabru faraar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phavein gabru faraar chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhora shehar tere vair chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhora shehar tere vair chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla tere naal pyaar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla tere naal pyaar chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhora shehar tere vair chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhora shehar tere vair chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla tere naal pyaar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla tere naal pyaar chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh hun tainu gall na aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh hun tainu gall na aayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve teri ankh mere vall na aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve teri ankh mere vall na aayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve naar nu tu rakhe kall te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve naar nu tu rakhe kall te"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun kade teri kal na aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun kade teri kal na aayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve khani chah de naal band karde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve khani chah de naal band karde"/>
</div>
<div class="lyrico-lyrics-wrapper">Taan hi soch vich mih chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan hi soch vich mih chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve mainu sacho sacchi das chobbran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve mainu sacho sacchi das chobbran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera kithe kithe ki chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera kithe kithe ki chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu sacho sacchi das chobbran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu sacho sacchi das chobbran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera kithe kithe ki chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera kithe kithe ki chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyun soch teri bhaar hoya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun soch teri bhaar hoya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda inch vi na bahar hoya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda inch vi na bahar hoya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Phavein puchi tu saheli kolo ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phavein puchi tu saheli kolo ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhani khani da na yaar hoya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhani khani da na yaar hoya ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh leke raahan vich dil khadiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh leke raahan vich dil khadiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jithon jithon tera yaar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jithon jithon tera yaar chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhora shehar tere vair chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhora shehar tere vair chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla tere naal pyaar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla tere naal pyaar chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhora shehar tere vair chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhora shehar tere vair chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla tere naal pyaar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla tere naal pyaar chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maan'an das kadon saar lavenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan'an das kadon saar lavenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavan kadon ve tu chaar lavenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavan kadon ve tu chaar lavenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve tu jadon nu seyana bann na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve tu jadon nu seyana bann na"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere pyaar nu vi maar lavenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere pyaar nu vi maar lavenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mainu naam ton bulaya kar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mainu naam ton bulaya kar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun tere muhon ji ji chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun tere muhon ji ji chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ve mainu sacho sacchi das chobbran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve mainu sacho sacchi das chobbran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera kithe kithe ki chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera kithe kithe ki chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu sacho sacchi das chobbran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu sacho sacchi das chobbran"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera kithe kithe ki chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera kithe kithe ki chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal rehnde nabeda jattiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal rehnde nabeda jattiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal laa li pher gehda jattiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal laa li pher gehda jattiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni tu shareaam meri hovegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni tu shareaam meri hovegi"/>
</div>
<div class="lyrico-lyrics-wrapper">Korala hou tera jattiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korala hou tera jattiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni teri mere utte ankh chaldi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni teri mere utte ankh chaldi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere hatthon hathiyar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere hatthon hathiyar chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhora shehar tere vair chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhora shehar tere vair chalde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla tere naal pyaar chalde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla tere naal pyaar chalde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Desi crew! Desi crew!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi crew! Desi crew!"/>
</div>
</pre>
