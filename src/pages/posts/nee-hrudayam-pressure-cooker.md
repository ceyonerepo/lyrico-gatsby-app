---
title: "nee hrudayam song lyrics"
album: "Pressure Cooker"
artist: "Sunil Kashyap"
lyricist: "Shreshta"
director: "Sujoi Karampuri - Sushil Karampuri"
path: "/albums/pressure-cooker-lyrics"
song: "Nee Hrudayam"
image: ../../images/albumart/pressure-cooker.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OMrkWE-nCL8"
type: "love"
singers:
  - Aditi Bhavaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Hrudayam Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hrudayam Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Hrudayam Kalusukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudayam Kalusukoni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhu oohalalo Teliponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhu oohalalo Teliponi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chigure Thodige Ee Aashalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigure Thodige Ee Aashalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi Basaleney Pooyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi Basaleney Pooyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulakinche Purivippey Paravasamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulakinche Purivippey Paravasamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavvinchey Kanthulaney Vedhajhalleney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavvinchey Kanthulaney Vedhajhalleney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Hrudayam Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hrudayam Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Hrudayam Kalusukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudayam Kalusukoni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhu Oohalalo Teliponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhu Oohalalo Teliponi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inkoncham Ee chelimine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkoncham Ee chelimine"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundariki Nettalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundariki Nettalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukoga Polamarane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukoga Polamarane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Talupu Neede Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Talupu Neede Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekantham Mahaa Mudduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekantham Mahaa Mudduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anipinche neevallane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinche neevallane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sontham Avvalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sontham Avvalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaratam Kaligindile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaratam Kaligindile"/>
</div>
<div class="lyrico-lyrics-wrapper">Urike Oopirilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urike Oopirilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevele Neevele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevele Neevele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ubike Ooholalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ubike Ooholalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevele Neevele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevele Neevele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urinche Udikinche Uusulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urinche Udikinche Uusulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Undundi Uppenelaa Maarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undundi Uppenelaa Maarene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Hrudayam Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hrudayam Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Hrudayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalusukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalusukoni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhu oohalalo Teliponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhu oohalalo Teliponi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedinche Ne Thalaputho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedinche Ne Thalaputho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadinche Paruvam Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadinche Paruvam Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Valinche Nee Perune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valinche Nee Perune"/>
</div>
<div class="lyrico-lyrics-wrapper">Veekshinchava Yevinthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veekshinchava Yevinthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhochche Maimarupule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhochche Maimarupule"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchettey Naa Manasunee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchettey Naa Manasunee"/>
</div>
<div class="lyrico-lyrics-wrapper">Andinche Aanandame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andinche Aanandame"/>
</div>
<div class="lyrico-lyrics-wrapper">Srushtinche Ee Chitrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srushtinche Ee Chitrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Vanukulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Vanukulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevele Neevele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevele Neevele"/>
</div>
<div class="lyrico-lyrics-wrapper">Perige Yadhalayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perige Yadhalayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevele Neevele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevele Neevele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuvanuvu Chalarege Alajadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvu Chalarege Alajadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Asantham Aaraatam Penchenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asantham Aaraatam Penchenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Hrudayam Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hrudayam Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Hrudayam Kalusukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Hrudayam Kalusukoni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhu oohalaloTeliponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhu oohalaloTeliponi"/>
</div>
</pre>
