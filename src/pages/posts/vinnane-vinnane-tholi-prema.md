---
title: "vinnane vinnane song lyrics"
album: "Tholi Prema"
artist: "S Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/tholi-prema-lyrics"
song: "Vinnane Vinnane"
image: ../../images/albumart/tholi-prema.jpg
date: 2018-02-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/J2whskXsZe8"
type: "happy"
singers:
  -	Armaan Malik
  - Devan Ekambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lovely Lovely Melody Edo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovely Lovely Melody Edo "/>
</div>
<div class="lyrico-lyrics-wrapper">Madilo Valape Chesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madilo Valape Chesa "/>
</div>
<div class="lyrico-lyrics-wrapper">Enno Enno Rojulu Vechina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno Enno Rojulu Vechina "/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishamulo Adugesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishamulo Adugesa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanne Kaalanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanne Kaalanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapesa Aapesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapesa Aapesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashanne Daatesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashanne Daatesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnaane Vinnaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaane Vinnaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedave Chebutunte Vinnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedave Chebutunte Vinnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaane Unnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaane Unnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholipremai Neelone Unnaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholipremai Neelone Unnaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yedalo Yedalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yedalo Yedalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Puttesinda Prema Naa Paina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttesinda Prema Naa Paina "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Manase 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Manase "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipichinda Kastha Late Ayina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipichinda Kastha Late Ayina "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Venake Venake 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Venake Venake "/>
</div>
<div class="lyrico-lyrics-wrapper">Vachesthunna Dooram Enthunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachesthunna Dooram Enthunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Epudi Epudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Epudi Epudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Rojosthundani Vechichusthunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojosthundani Vechichusthunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Endarunna Andamayina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Endarunna Andamayina "/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Naaku Chepesavuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Naaku Chepesavuga "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Vanda Chandamamalunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Vanda Chandamamalunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Chotuloke Nettesavuga  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotuloke Nettesavuga  "/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaane Vinnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaane Vinnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedave Chebutunte Vinnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedave Chebutunte Vinnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaane Unnaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaane Unnaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholipremai Neelone Unnaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholipremai Neelone Unnaane "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dooba Dooba Dooba Dooba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooba Dooba Dooba Dooba "/>
</div>
<div class="lyrico-lyrics-wrapper">Dooba Dooba Dooba Diba Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooba Dooba Dooba Diba Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Dooba Dooba Dooba Dooba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dooba Dooba Dooba Dooba "/>
</div>
<div class="lyrico-lyrics-wrapper">Dooba Dooba Dooba Diba Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooba Dooba Dooba Diba Da "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paluke Vintu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paluke Vintu "/>
</div>
<div class="lyrico-lyrics-wrapper">Tenalane Marichaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tenalane Marichaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Alake Kantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Alake Kantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Aakaline Vidichale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaline Vidichale "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Niddura Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Niddura Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalala Tere Terichale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalala Tere Terichale "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Melukuva Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Melukuva Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Veluturule Parichale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluturule Parichale "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Merise Merise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Merise Merise "/>
</div>
<div class="lyrico-lyrics-wrapper">Hariville Nee Rangu Nenanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hariville Nee Rangu Nenanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Kurise Kurise 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Kurise Kurise "/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelave Nee Reyi Nenavuta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelave Nee Reyi Nenavuta "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pere Piliche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pere Piliche "/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaramaina Neeku Radanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaramaina Neeku Radanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Kannire Thudiche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannire Thudiche "/>
</div>
<div class="lyrico-lyrics-wrapper">Velai Nene Neeku Todunta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Nene Neeku Todunta "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Endarunna Andamayina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Endarunna Andamayina "/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Naaku Chepesavuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Naaku Chepesavuga "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Vanda Chandamamalunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Vanda Chandamamalunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Chotuloke Nettesavuga  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotuloke Nettesavuga  "/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala Vinnaane Lalala Vinnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala Vinnaane Lalala Vinnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala Lalala Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala Lalala Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Pedave Chebutunte Vinnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedave Chebutunte Vinnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala Unnaane Lalala Unnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala Unnaane Lalala Unnaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalala Lalala Tholipremai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalala Lalala Tholipremai "/>
</div>
<div class="lyrico-lyrics-wrapper">Neelone Unnaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone Unnaane"/>
</div>
</pre>
