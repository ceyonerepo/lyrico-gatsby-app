---
title: "nanduri enkila song lyrics"
album: "Bichagada Majaka"
artist: "Sri Venkat"
lyricist: "B. Chandra Sekhar (Pedda Babu)"
director: "K.S. Nageswara Rao"
path: "/albums/bichagada-majaka-lyrics"
song: "Nanduri Enkila"
image: ../../images/albumart/bichagada-majaka.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/UNVn-Nu3WXI"
type: "love"
singers:
  - Prasad
  - Uma Neha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nanduri enkila undiro pilladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanduri enkila undiro pilladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eraani cheera katti errikisthunadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraani cheera katti errikisthunadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arrare Nanduri enkila undiro pilladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrare Nanduri enkila undiro pilladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eraani cheera katti errikisthunadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraani cheera katti errikisthunadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daani thaluku choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daani thaluku choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">beruku choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beruku choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandireega nadumu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandireega nadumu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantachenu ghatu dhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantachenu ghatu dhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadaa daruvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadaa daruvule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye gunturu mirapakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gunturu mirapakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillado pillado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillado pillado"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehh sandhu choosi sandakada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehh sandhu choosi sandakada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilludo gilludo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilludo gilludo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gunturu mirapakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gunturu mirapakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillado pillado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillado pillado"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu choosi sandakada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu choosi sandakada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilludo gilludo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilludo gilludo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi choopu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi choopu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udumpattu birusu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udumpattu birusu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondavaagu dhookudalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondavaagu dhookudalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Biraa biraa parugule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biraa biraa parugule"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennello godari chinna boye pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennello godari chinna boye pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee andham chusaaka manasu gulla gulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee andham chusaaka manasu gulla gulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharagotte pilladamma thattukunedhela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharagotte pilladamma thattukunedhela"/>
</div>
<div class="lyrico-lyrics-wrapper">Adharalanu bedaragotte pantigatulela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adharalanu bedaragotte pantigatulela"/>
</div>
<div class="lyrico-lyrics-wrapper">Putta tene chusinaka tagakunte ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putta tene chusinaka tagakunte ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattumani padarellu naku levu kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattumani padarellu naku levu kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanuvanta neekosam talakindulaitande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanuvanta neekosam talakindulaitande"/>
</div>
<div class="lyrico-lyrics-wrapper">Manuvadinaka haddulanni radde raddega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manuvadinaka haddulanni radde raddega"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanduri enkila undiro pilladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanduri enkila undiro pilladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye gunturu mirapakaya pillado pillado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gunturu mirapakaya pillado pillado"/>
</div>
<div class="lyrico-lyrics-wrapper">Paira gaali dhuduku choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paira gaali dhuduku choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Repa repa repa repa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repa repa repa repa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paitagaali soki gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paitagaali soki gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada dhada dhada dhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada dhada dhada dhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Are korikemo gurramaite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are korikemo gurramaite"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka taka taka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka taka taka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru vaada navvukoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru vaada navvukoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paka paka paka paka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paka paka paka paka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkulanni mudupu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkulanni mudupu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudharadhante elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudharadhante elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaku chatu pindhe nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaku chatu pindhe nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha thonderela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha thonderela"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone vasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone vasantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Odilesey nee pantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odilesey nee pantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee maata poola banamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee maata poola banamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Theesthadi praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesthadi praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arrare Nanduri enkila undiro pilladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrare Nanduri enkila undiro pilladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eraani cheera katti errikisthunadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eraani cheera katti errikisthunadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daani thaluku choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daani thaluku choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">beruku choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beruku choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandireega nadumu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandireega nadumu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantachenu ghatu dhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantachenu ghatu dhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadaa daruvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadaa daruvule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye gunturu mirapakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gunturu mirapakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillado pillado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillado pillado"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehh sandhu choosi sandakada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehh sandhu choosi sandakada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilludo gilludo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilludo gilludo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Gunturu mirapakaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gunturu mirapakaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillado pillado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillado pillado"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu choosi sandakada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu choosi sandakada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilludo gilludo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilludo gilludo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi choopu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi choopu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udumpattu birusu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udumpattu birusu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondavaagu dhookudalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondavaagu dhookudalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Biraa biraa parugule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biraa biraa parugule"/>
</div>
</pre>
