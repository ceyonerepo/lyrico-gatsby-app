---
title: "manditho paatugaa song lyrics"
album: "Yatra"
artist: "K"
lyricist: "Sirivennela Sitaramasastri"
director: "Mahi V Raghav"
path: "/albums/yatra-lyrics"
song: "Manditho Paatugaa"
image: ../../images/albumart/yatra.jpg
date: 2019-02-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2HjvzVieXDQ"
type: "sad"
singers:
  - Saicharan Bhasakaruni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manditho Paatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manditho Paatugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munduke Saaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munduke Saaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enduko Thochakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enduko Thochakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontigaa Aaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontigaa Aaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edari ledhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edari ledhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhadam Maananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhadam Maananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthaki Telanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthaki Telanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Prashnagaa Marenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prashnagaa Marenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manditho Paatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manditho Paatugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munduke Saaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munduke Saaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enduko Thochakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enduko Thochakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontigaa Aaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontigaa Aaganaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manditho Paatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manditho Paatugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munduke Saaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munduke Saaganaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu Aashagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu Aashagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Chusthundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Chusthundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukshanam Needagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukshanam Needagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venta Vasthundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venta Vasthundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modati Adugai Nuvvee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modati Adugai Nuvvee"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadapa Dataligaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadapa Dataligaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammakam Bhatagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakam Bhatagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadichi Theeraligaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadichi Theeraligaa"/>
</div>
</pre>
