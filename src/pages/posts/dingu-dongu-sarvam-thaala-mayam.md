---
title: "dingu dongu song lyrics"
album: "Sarvam Thaala Mayam"
artist: "A. R. Rahman"
lyricist: "Arunraja Kamaraj"
director: "Rajiv Menon"
path: "/albums/sarvam-thaala-mayam-lyrics"
song: "Dingu Dongu"
image: ../../images/albumart/sarvam-thaala-mayam.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u2Mzv0W3hk0"
type: "happy"
singers:
  - Bamba Bakya
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eppo Varumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varumo Enga Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumo Enga Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Kuraiyum Enga Baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Kuraiyum Enga Baaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyum Bhedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Bhedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varumoo Enga Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumoo Enga Neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Kuraiyum Enga Bhaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Kuraiyum Enga Bhaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyum Bhedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Bhedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Kottum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Kottum Bodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Naadham Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Naadham Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Melaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Melaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellame Aadividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Aadividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dong Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dong Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dong Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dong Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varumo Enga Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumo Enga Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Kuraiyum Enga Baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Kuraiyum Enga Baaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyum Bhedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Bhedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varumoo Enga Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumoo Enga Neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Kuraiyum Enga Bhaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Kuraiyum Enga Bhaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyum Bhedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Bhedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhaikkira Kai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaikkira Kai Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethulathaan Mukkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethulathaan Mukkanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soththula Kai Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththula Kai Vaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varisaiyila Nikkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varisaiyila Nikkanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaikkura Tholulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaikkura Tholulathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkira Vaadhiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkira Vaadhiyatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padaikkira Kaigalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaikkira Kaigalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaikkira Gnyaanam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaikkira Gnyaanam Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Vaazhvodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vaazhvodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaiyodum Naadham Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyodum Naadham Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namba Pattu Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Pattu Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettu Pola Oore Senthu Kettaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu Pola Oore Senthu Kettaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dong Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dong Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dong Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dong Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varumo Enga Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumo Enga Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Kuraiyum Enga Baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Kuraiyum Enga Baaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyum Bhedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Bhedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varumoo Enga Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varumoo Enga Neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo Kuraiyum Enga Bhaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Kuraiyum Enga Bhaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyum Bhedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Bhedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Kottum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Kottum Bodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Naadham Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Naadham Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Melaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Melaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellamey Aadividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamey Aadividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dong Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dong Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dong Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dong Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Dingu Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Dingu Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dingu Dongu Ding Dongu Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dingu Dongu Ding Dongu Ding"/>
</div>
</pre>
