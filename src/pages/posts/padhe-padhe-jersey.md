---
title: "padhe padhe song lyrics"
album: "Jersey"
artist: "Anirudh Ravichander"
lyricist: "Krishna Kanth"
director: "Gowtam Tinnanuri"
path: "/albums/jersey-lyrics"
song: "Padhe Padhe"
image: ../../images/albumart/jersey.jpg
date: 2019-04-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/nkzLA-Dl4Rs"
type: "mass"
singers:
  - Anirudh Ravichander
  - Shakthisree Gopalan
  - Brodha V
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muddu Pettanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu Pettanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Paine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Paine"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu Muttanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Muttanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaviri Laane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaviri Laane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kottenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kottenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Muttagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Muttagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Hathukundhuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hathukundhuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakharu La Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakharu La Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothagundhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothagundhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikharu Lo Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikharu Lo Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Thittina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Thittina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapanu Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapanu Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Undu Guttugaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu Guttugaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe Padhe Pedhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe Padhe Pedhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade Panai Shramimchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade Panai Shramimchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaalane Pandinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaalane Pandinchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ede Nadhai Muncheyyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ede Nadhai Muncheyyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe Padhe Pedhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe Padhe Pedhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade Panai Shramimchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade Panai Shramimchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaalane Pandinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaalane Pandinchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ede Nadhai Muncheyyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ede Nadhai Muncheyyani"/>
</div>
<div class="lyrico-lyrics-wrapper">I’M In Pursit Of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’M In Pursit Of"/>
</div>
<div class="lyrico-lyrics-wrapper">Some Therapy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Some Therapy"/>
</div>
<div class="lyrico-lyrics-wrapper">As Soon As The Melody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="As Soon As The Melody"/>
</div>
<div class="lyrico-lyrics-wrapper">Hits
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hits"/>
</div>
<div class="lyrico-lyrics-wrapper">I’Ll Never Resist
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’Ll Never Resist"/>
</div>
<div class="lyrico-lyrics-wrapper">The Minute I Look At You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Minute I Look At You"/>
</div>
<div class="lyrico-lyrics-wrapper">Licking You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Licking You"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvety Lips
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvety Lips"/>
</div>
<div class="lyrico-lyrics-wrapper">An Endless Abyss Of
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="An Endless Abyss Of"/>
</div>
<div class="lyrico-lyrics-wrapper">Animalistic Energy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Animalistic Energy"/>
</div>
<div class="lyrico-lyrics-wrapper">Heavenly Thrills
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heavenly Thrills"/>
</div>
<div class="lyrico-lyrics-wrapper">I’M Letting Your
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’M Letting Your"/>
</div>
<div class="lyrico-lyrics-wrapper">Empathy Enter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Empathy Enter"/>
</div>
<div class="lyrico-lyrics-wrapper">The Feeling I Feel Is So Heavy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Feeling I Feel Is So Heavy"/>
</div>
<div class="lyrico-lyrics-wrapper">And Steadily Builds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Steadily Builds"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Yo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Yo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalices Of Intimacy Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalices Of Intimacy Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Always Got My Tunnel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Got My Tunnel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vision In Focus When
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vision In Focus When"/>
</div>
<div class="lyrico-lyrics-wrapper">You’Re Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’Re Around"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Yo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Yo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalices Of Intimacy Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalices Of Intimacy Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Always Got My Tunnel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Got My Tunnel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vision In Focus When
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vision In Focus When"/>
</div>
<div class="lyrico-lyrics-wrapper">You’Re Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’Re Around"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaki Kaapala Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaki Kaapala Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaladhi Aagani Dookude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaladhi Aagani Dookude"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kottenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kottenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Muttagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Muttagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasuki Opika Lokuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasuki Opika Lokuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvuki Aashala Thaakide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvuki Aashala Thaakide"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni Thittina Aapanu Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni Thittina Aapanu Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Undu Guttugaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu Guttugaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaleva Raaleva Raaleva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaleva Raaleva Raaleva"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanalle Naameeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanalle Naameeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Deham Vesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deham Vesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poleva Poleva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poleva Poleva"/>
</div>
<div class="lyrico-lyrics-wrapper">Matese Dooralu Poleva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matese Dooralu Poleva"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naa Kosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naa Kosame"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe Padhe Pedhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe Padhe Pedhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade Panai Shramimchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade Panai Shramimchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaalane Pandinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaalane Pandinchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ede Nadhai Muncheyyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ede Nadhai Muncheyyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhe Padhe Pedhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhe Padhe Pedhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade Panai Shramimchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade Panai Shramimchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaalane Pandinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaalane Pandinchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ede Nadhai Muncheyyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ede Nadhai Muncheyyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Yo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Yo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalices Of Intimacy Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalices Of Intimacy Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Always Got My Tunnel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Got My Tunnel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vision In Focus When
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vision In Focus When"/>
</div>
<div class="lyrico-lyrics-wrapper">You’Re Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’Re Around"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Yo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Yo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalices Of Intimacy Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalices Of Intimacy Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Always Got My Tunnel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Always Got My Tunnel"/>
</div>
<div class="lyrico-lyrics-wrapper">Vision In Focus When
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vision In Focus When"/>
</div>
<div class="lyrico-lyrics-wrapper">You’Re Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’Re Around"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaki Kaapala Ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaki Kaapala Ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaladhi Aagani Dookude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaladhi Aagani Dookude"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasuki Opika Lokuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasuki Opika Lokuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvuki Aashala Thaakide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvuki Aashala Thaakide"/>
</div>
</pre>
