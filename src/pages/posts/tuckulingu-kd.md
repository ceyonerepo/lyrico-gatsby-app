---
title: "tuckulingu song lyrics"
album: "K D"
artist: "Karthikeya Murthy"
lyricist: "Sabarivaasan Shanmugam"
director: "Madhumitha"
path: "/albums/kd-lyrics"
song: "Tuckulingu"
image: ../../images/albumart/kd.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NSd_VWT9xXE"
type: "happy"
singers:
  - Benny Dayal
  - Andrea
  - Karthikeya Murthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">roja poovum raaja mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poovum raaja mela"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaja panni nova aagiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaja panni nova aagiyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raaja partha rangamanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja partha rangamanai"/>
</div>
<div class="lyrico-lyrics-wrapper">roja poova thana mariyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poova thana mariyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada dei kalavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada dei kalavani"/>
</div>
<div class="lyrico-lyrics-wrapper">ini naan ambani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini naan ambani"/>
</div>
<div class="lyrico-lyrics-wrapper">thavuttu mootaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavuttu mootaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thangame nee gavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangame nee gavani"/>
</div>
<div class="lyrico-lyrics-wrapper">sirisu kedi paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirisu kedi paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">perusu kutti durai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perusu kutti durai"/>
</div>
<div class="lyrico-lyrics-wrapper">therikkum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikkum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">tharigida tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigida tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thai thai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thai thai "/>
</div>
<div class="lyrico-lyrics-wrapper">thai thai thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai thai thai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">roja poovum raaja mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poovum raaja mela"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaja panni nova aagiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaja panni nova aagiyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">raaja partha rangamanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja partha rangamanai"/>
</div>
<div class="lyrico-lyrics-wrapper">roja poova thana mariyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poova thana mariyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tingu tingu tingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tingu tingu tingu"/>
</div>
<div class="lyrico-lyrics-wrapper">teegana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teegana "/>
</div>
<div class="lyrico-lyrics-wrapper">tangu tangu tangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tangu tangu tangu"/>
</div>
<div class="lyrico-lyrics-wrapper">tagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tagana"/>
</div>
<div class="lyrico-lyrics-wrapper">jalabula jalabula jungs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jalabula jalabula jungs"/>
</div>
<div class="lyrico-lyrics-wrapper">aadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadina"/>
</div>
<div class="lyrico-lyrics-wrapper">sadugudu sadugudu thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadugudu sadugudu thana"/>
</div>
<div class="lyrico-lyrics-wrapper">gama gama gama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gama gama gama"/>
</div>
<div class="lyrico-lyrics-wrapper">koozhu oothina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koozhu oothina"/>
</div>
<div class="lyrico-lyrics-wrapper">muru muru muru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muru muru muru"/>
</div>
<div class="lyrico-lyrics-wrapper">muruku erina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muruku erina"/>
</div>
<div class="lyrico-lyrics-wrapper">kadamuda vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadamuda vena"/>
</div>
<div class="lyrico-lyrics-wrapper">kodalu kalakkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodalu kalakkina"/>
</div>
<div class="lyrico-lyrics-wrapper">sadugudu sadugudu thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadugudu sadugudu thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">biriyani raasa naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="biriyani raasa naan"/>
</div>
<div class="lyrico-lyrics-wrapper">sariya nee ketukapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sariya nee ketukapu"/>
</div>
<div class="lyrico-lyrics-wrapper">yemagathaga paiya ivan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemagathaga paiya ivan than"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirkama poyidappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirkama poyidappu"/>
</div>
<div class="lyrico-lyrics-wrapper">jigarthanda jilu jilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jigarthanda jilu jilu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu pola enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu pola enga"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sirisu kedi paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirisu kedi paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">perusu kutti durai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perusu kutti durai"/>
</div>
<div class="lyrico-lyrics-wrapper">therikkum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikkum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">tharigida tharigida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharigida tharigida"/>
</div>
<div class="lyrico-lyrics-wrapper">thaka thaka thai thai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaka thaka thai thai "/>
</div>
<div class="lyrico-lyrics-wrapper">thai thai thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai thai thai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">roja poovum raaja mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poovum raaja mela"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaja panni nova aagiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaja panni nova aagiyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">raaja partha rangamanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raaja partha rangamanai"/>
</div>
<div class="lyrico-lyrics-wrapper">roja poova thana mariyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poova thana mariyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
<div class="lyrico-lyrics-wrapper">tucklu tuckluilingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tucklu tuckluilingu"/>
</div>
</pre>
