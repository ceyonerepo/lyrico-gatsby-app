---
title: "ishq karo dil se song lyrics"
album: "Koi Jaane Na"
artist: "Amaal Mallik"
lyricist: "Kumaar"
director: "Amin Hajee"
path: "/albums/koi-jaane-na-lyrics"
song: "Ishq Karo Dil Se"
image: ../../images/albumart/koi-jaane-na.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/1IU4eSxSR3o"
type: "happy"
singers:
  - Jubin Nautiyal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hai Pal Do Pal Zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai Pal Do Pal Zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Aaj Hai Kal Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Aaj Hai Kal Nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paya Kya Khoya Kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Kya Khoya Kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Parvaah Nahi Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parvaah Nahi Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Sath Hain To Har
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Sath Hain To Har"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamha Haseen Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamha Haseen Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jitna Bhi Jeena Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitna Bhi Jeena Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Pal Mein Jeena Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Pal Mein Jeena Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhodo Na Kuch Kal Pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhodo Na Kuch Kal Pe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Karo Dil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Karo Dil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Dil Milte Hain Mushkil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dil Milte Hain Mushkil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq Karo Dil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Karo Dil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Dil Milte Hain Mushkil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dil Milte Hain Mushkil Se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Dil Milte Hain Mushkil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dil Milte Hain Mushkil Se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaha Hai Sabhi Ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaha Hai Sabhi Ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Yun Fikron Mein Jeene Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yun Fikron Mein Jeene Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoga Bhala Kya Bata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoga Bhala Kya Bata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Awaara Sa Ban Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awaara Sa Ban Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Yun Gir Ke Sambhal Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yun Gir Ke Sambhal Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Hi Milta Hai Khud Ka Pata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi Milta Hai Khud Ka Pata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm.. Saanson Se Aage Bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm.. Saanson Se Aage Bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Zindagi Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Zindagi Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Sath Hain To Har
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Sath Hain To Har"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamha Haseen Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamha Haseen Hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jitna Bhi Jeena Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitna Bhi Jeena Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Pal Mein Jeena Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Pal Mein Jeena Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhodo Na Kuch Kal Pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhodo Na Kuch Kal Pe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq Karo Dil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Karo Dil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Dil Milte Hain Mushkil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dil Milte Hain Mushkil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq Karo Dil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq Karo Dil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Dil Milte Hain Mushkil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dil Milte Hain Mushkil Se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Dil Milte Hain Mushkil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Dil Milte Hain Mushkil Se"/>
</div>
</pre>
