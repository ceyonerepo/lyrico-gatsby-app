---
title: "puthu saththam song lyrics"
album: "Sulthan"
artist: "vivek - mervin"
lyricist: "Thanikodi"
director: "Bakkiyaraj Kannan"
path: "/albums/sulthan-song-lyrics"
song: "Puthu Saththam"
image: ../../images/albumart/sulthan.jpg
date: 2020-04-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NT0gj1tlRQY"
type: "Celebration"
singers:
  - Kailash Kher
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ae Mannu Athuruthe Vanam Uthiruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Mannu Athuruthe Vanam Uthiruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka Mnasuthan Thulli Eguruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka Mnasuthan Thulli Eguruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Hey Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Hey Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Ooru Oramura Onnu Theraluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ooru Oramura Onnu Theraluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Atam Alapara Pottu Kalakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atam Alapara Pottu Kalakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Puthu Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Puthu Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithura Vazha Maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithura Vazha Maram"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Vanavillu Charam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Vanavillu Charam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi Poda Megam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi Poda Megam Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanam Mooku Mela Kai Veikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanam Mooku Mela Kai Veikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Vecha Ellam Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vecha Ellam Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thanda Namba Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thanda Namba Gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakolam Paka Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakolam Paka Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Sami Kuda Moi Veikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Sami Kuda Moi Veikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Mannu Athuruthe Vanam Uthiruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Mannu Athuruthe Vanam Uthiruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Makka Mnasuthan Thulli Eguruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makka Mnasuthan Thulli Eguruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Hey Hey Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Hey Hey Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Ooru Oramura Onnu Theraluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Ooru Oramura Onnu Theraluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Atam Alapara Pottu Kalakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atam Alapara Pottu Kalakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Puthu Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Puthu Saththam"/>
</div>
(<div class="lyrico-lyrics-wrapper">Oh! Oh! Oh!)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh! Oh! Oh!)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorumuga Velaka Puranama Iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorumuga Velaka Puranama Iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koorapattu Selayil Enna Vangita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorapattu Selayil Enna Vangita"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenna Mara Kurutha Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenna Mara Kurutha Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn Ivan Madichan Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn Ivan Madichan Hey Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakkapiri Kairagaththan Onna Pinnitan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkapiri Kairagaththan Onna Pinnitan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Kuththi Ulla Kodaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Kuththi Ulla Kodaiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjaram Pottu Enna Adaikura Ho!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaram Pottu Enna Adaikura Ho!"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathoda Iraga Mari Alaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathoda Iraga Mari Alaiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththi Un Kannu Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththi Un Kannu Kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyuran Tholaiyuran Tholaiyuran.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyuran Tholaiyuran Tholaiyuran."/>
</div>
(<div class="lyrico-lyrics-wrapper">Oh! Oh! Oh!)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh! Oh! Oh!)"/>
</div>
</pre>
