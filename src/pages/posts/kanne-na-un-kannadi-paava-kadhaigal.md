---
title: "kanne na un kannadi song lyrics"
album: "Paava Kadhaigal"
artist: "Karthik"
lyricist: "Madhan Karky"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "Kanne Na Un Kannadi"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WWMkpoiJwWQ"
type: "sad"
singers:
  - Bombay Jayashri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanney Naan Un Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Naan Un Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kaattura Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaattura Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam En Santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosam En Santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Veesuna Thaayeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Veesuna Thaayeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Naan Un Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Naan Un Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mayyai Theettura Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mayyai Theettura Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneera Un Kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneera Un Kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pannuven Thaayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pannuven Thaayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marukkaa Pooththida Sirippa Paaththida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukkaa Pooththida Sirippa Paaththida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhakkum Kaangala Kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakkum Kaangala Kanney"/>
</div>
<div class="lyrico-lyrics-wrapper">Idunju Nee Azha Udanju Naan Vizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idunju Nee Azha Udanju Naan Vizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyum Kaangala Kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyum Kaangala Kanney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Naan Un Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Naan Un Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kaattura Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaattura Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam En Santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosam En Santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Veesuna Thaayeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Veesuna Thaayeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney Azhugaiya Thingaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney Azhugaiya Thingaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Theekkura Thingaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Theekkura Thingaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayaththa Intha Kaayaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayaththa Intha Kaayaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththaal Theeppaen Thaayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththaal Theeppaen Thaayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththa Kaathula Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththa Kaathula Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Aachunu Sollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Aachunu Sollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorellaam Ingu Pollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorellaam Ingu Pollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Peiga Vaaluthu Thaayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiga Vaaluthu Thaayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orunaal Maaridum Ranamum Aaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orunaal Maaridum Ranamum Aaridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakka Paaradi Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakka Paaradi Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaiya Theththavey Valiya Kaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaiya Theththavey Valiya Kaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiya Theththuran Kanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiya Theththuran Kanney"/>
</div>
</pre>
