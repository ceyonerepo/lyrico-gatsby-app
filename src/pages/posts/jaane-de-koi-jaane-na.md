---
title: "jaane de song lyrics"
album: "Koi Jaane Na"
artist: "Rochak Kohli"
lyricist: "Manoj Muntashir"
director: "Amin Hajee"
path: "/albums/koi-jaane-na-lyrics"
song: "Jaane De"
image: ../../images/albumart/koi-jaane-na.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/2XE4-NvIZow"
type: "Melody"
singers:
  - B Praak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jaane de yeh bhi jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de yeh bhi jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Dard ko thoda sa muskurane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard ko thoda sa muskurane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane de yeh bhi jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de yeh bhi jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Dard ko thoda sa muskurane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dard ko thoda sa muskurane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaane de ye bhi jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de ye bhi jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadon ko na aur dil dukhane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadon ko na aur dil dukhane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Badi der se nahi soye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badi der se nahi soye"/>
</div>
<div class="lyrico-lyrics-wrapper">Roye roye naino ko zara neend aane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roye roye naino ko zara neend aane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane de yeh bhi jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de yeh bhi jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri meri jo kahani thi na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri jo kahani thi na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagazon pe likh nahi paaye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagazon pe likh nahi paaye hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho teri meri jo kahani thi na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho teri meri jo kahani thi na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagazon pe likh nahi paaye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagazon pe likh nahi paaye hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaad nahi ik dusre ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaad nahi ik dusre ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kis mod pe chhod aaye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kis mod pe chhod aaye hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh jo hona tha nahi hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh jo hona tha nahi hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuan dhuan sa hai dil isse jal jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuan dhuan sa hai dil isse jal jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaane de yeh bhi jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de yeh bhi jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Khalish hai isse guzar jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khalish hai isse guzar jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane de yeh bhi jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de yeh bhi jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Zeher hai isse utar jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zeher hai isse utar jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaane de, jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de, jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane de, jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de, jaane de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh doriyan jo kheenchti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh doriyan jo kheenchti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khamoshiyan jo cheekhti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khamoshiyan jo cheekhti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dushmani jo hai roshni se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dushmani jo hai roshni se"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh bedili jo zindagi se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh bedili jo zindagi se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ab toh inhe jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab toh inhe jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane de, jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de, jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane de"/>
</div>
</pre>
