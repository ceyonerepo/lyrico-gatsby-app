---
title: "naan pizhaippeno song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Naan Pizhaippeno"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pYBIpM5mBm4"
type: "love"
singers:
  - D. Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maamu pozhudhu pogala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamu pozhudhu pogala"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam pidikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam pidikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil pasumai kanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil pasumai kanala"/>
</div>
<div class="lyrico-lyrics-wrapper">Katru kooda adikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru kooda adikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thamarai neerinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thamarai neerinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Illamal ingae yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illamal ingae yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru megalai padhanggal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru megalai padhanggal"/>
</div>
<div class="lyrico-lyrics-wrapper">Man meedhu punnavadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man meedhu punnavadhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr oviyam kagidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr oviyam kagidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollamal ingae yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollamal ingae yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan ayiram ayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan ayiram ayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannangal pennavadhu yenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal pennavadhu yenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan pizhaipeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan pizhaipeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu vanguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu vanguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorayum thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorayum thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaichchal yerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaichchal yerudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyabagam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyabagam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai aguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai aguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadagam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natkal pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natkal pogudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayiram pokkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayiram pokkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoova thonudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoova thonudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondridumbodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondridumbodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavam theerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavam theerudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarigai yalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarigai yalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum marudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum marudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanilai veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanilai veppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottru pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottru pogudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalai vizhippu vandhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalai vizhippu vandhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil aval mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil aval mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai pudhiya oruvanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai pudhiya oruvanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyum seiyum arimugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyum seiyum arimugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhunaal varai naalvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhunaal varai naalvarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illadha poonthottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illadha poonthottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thidu dhippena dhippena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thidu dhippena dhippena"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengengum yen vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengengum yen vandhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai parpadhu nichayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai parpadhu nichayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrana andradam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrana andradam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai sillida vaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai sillida vaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boogambam than thandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boogambam than thandhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan pizhaipeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan pizhaipeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu vanguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu vanguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorayum thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorayum thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaichchal yerudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaichchal yerudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyabagam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyabagam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai aguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai aguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadagam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadagam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natkal pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natkal pogudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen unnai parthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen unnai parthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorva nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorva nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhettu noolai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhettu noolai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu poganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetukku ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukku ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angum un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veembudan vandhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembudan vandhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhthi parkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhthi parkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilaa dhoorathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaa dhoorathu "/>
</div>
<div class="lyrico-lyrics-wrapper">parvaigal podhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parvaigal podhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yennidam vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai yennidam vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">endru sonnalum varadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru sonnalum varadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nangaindhu varthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangaindhu varthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan serkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan serkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaira kal pola ovvondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaira kal pola ovvondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan korkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan korkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhenum pesamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhenum pesamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradhini"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraiyum pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyum pani"/>
</div>
</pre>
