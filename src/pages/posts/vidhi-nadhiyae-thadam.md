---
title: "vidhi nadhiyae song lyrics"
album: "Thadam"
artist: "Arun Raj"
lyricist: "Madhan Karky"
director: "Magizh Thirumeni"
path: "/albums/thadam-lyrics"
song: "Vidhi Nadhiyae"
image: ../../images/albumart/thadam.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kfSgfmddk7g"
type: "love"
singers:
  - L. V. Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaraai Maanam Aaraai Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraai Maanam Aaraai Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivili Aaraagavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivili Aaraagavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayum Undhan Azhaigalin Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Undhan Azhaigalin Meley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oar Ethirozhi Poley Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Ethirozhi Poley Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraai Manam Aaraai Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraai Manam Aaraai Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viraindhidum Aaraagavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraindhidum Aaraagavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelum Adhan Karaigalin Meley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum Adhan Karaigalin Meley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal Thadangalai Poley Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Thadangalai Poley Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhazh Mele Aniyum Punnagaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Mele Aniyum Punnagaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Ulley Pudhaiyum Kanneerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Ulley Pudhaiyum Kanneerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamellaam Ganakkum Ninaivugalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamellaam Ganakkum Ninaivugalodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munney Selgindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munney Selgindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oru Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Paaigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Marudhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Marudhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaindhu Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaindhu Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennai Engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondu Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Selgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oru Kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Kanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadalaagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadalaagiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Marukanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Marukanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theindhupogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theindhupogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennai Engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondu Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Selgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakaai Sila Pookkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaai Sila Pookkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirakkaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirakkaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakaai Sila Thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaai Sila Thooral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vin Kilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Kilaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Aasaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Aasaigalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niraivetridhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivetridhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Aasaigalai Nuraipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Aasaigalai Nuraipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orunaal Veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orunaal Veezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunaal Meezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunaal Meezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjukku Sollithandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukku Sollithandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oru Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Paaigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Marudhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Marudhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaindhu Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaindhu Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennai Engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondu Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Selgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oru Kanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Kanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadalaagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadalaagiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Marukanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Marukanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theindhupogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theindhupogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennai Engey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Engey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondu Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Selgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vidhi Nadhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vidhi Nadhiyae"/>
</div>
</pre>
