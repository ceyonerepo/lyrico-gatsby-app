---
title: "baras baras song lyrics"
album: "Durgamati"
artist: "Tanishk Bagchi"
lyricist: "Tanishk Bagchi"
director: "G. Ashok"
path: "/albums/durgamati-lyrics"
song: "Baras Baras"
image: ../../images/albumart/durgamati.jpg
date: 2020-12-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/NOf2fU1fchY"
type: "motivational"
singers:
  - B Praak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unki yeh nazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unki yeh nazar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo nazar se mili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo nazar se mili"/>
</div>
<div class="lyrico-lyrics-wrapper">Main pighlata raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main pighlata raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Usme hi kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usme hi kahin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main khuda se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main khuda se"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan khuda se kahoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan khuda se kahoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mera tu mera haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mera tu mera haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke ankhiyan baras baras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke ankhiyan baras baras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Naina taras taras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naina taras taras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohe daras dikha jaa re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohe daras dikha jaa re"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya re.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke ankhiyan baras baras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke ankhiyan baras baras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Naina taras taras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naina taras taras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke mohe daras dikha jaa re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke mohe daras dikha jaa re"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya re.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khone laga main iss tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khone laga main iss tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa asar tera hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa asar tera hua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh dil pe chale tera hi zor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dil pe chale tera hi zor"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kheenche chalun main teri aur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kheenche chalun main teri aur"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera hua main sabko chhod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera hua main sabko chhod"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh mera ishq nahi kamzor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh mera ishq nahi kamzor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unse bepanah jo mohabbat huyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unse bepanah jo mohabbat huyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Main pighlata raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main pighlata raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Usme hi kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usme hi kahin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main khuda se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main khuda se"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan khuda se kahoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan khuda se kahoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mera tu mera haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mera tu mera haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke ankhiyan baras baras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke ankhiyan baras baras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Naina taras taras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naina taras taras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohe daras dikha jaa re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohe daras dikha jaa re"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya re.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ke ankhiyan baras baras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke ankhiyan baras baras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Naina taras taras jaayein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naina taras taras jaayein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke mohe daras dikha jaa re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke mohe daras dikha jaa re"/>
</div>
<div class="lyrico-lyrics-wrapper">Piya re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piya re"/>
</div>
</pre>
