---
title: 'pudhu suriyan song lyrics'
album: 'Pattas'
artist: 'Vivek Mervin'
lyricist: 'Mervin Solomon, Vivek Siva'
director: 'R.S.Durai Senthilkumar'
path: '/albums/pattas-song-lyrics'
song: 'Pudhu Suriyan'
image: ../../images/albumart/pattas.jpg
date: 2020-01-15
lang: tamil
singers:
- Anuradha Sriram
youtubeLink: 'https://www.youtube.com/embed/6P6WvQ205HI'
type: 'parenting'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Pudhu suriyan en veetilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu suriyan en veetilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaaga thaan vilaiyaduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaaga thaan vilaiyaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru thozhilum sugam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru thozhilum sugam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thookki naan pasi aaruven
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai thookki naan pasi aaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aruginil valarum piraiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aruginil valarum piraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarnthae paravum mazhaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Valarnthae paravum mazhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan nilavu thiraiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaan nilavu thiraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandae jolikkum azhagae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirandae jolikkum azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa sirandha mozhiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa sirandha mozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalgal thirandha vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Madalgal thirandha vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan udaindha silaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan udaindha silaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaiyil mulaikkum uyirae
<input type="checkbox" class="lyrico-select-lyric-line" value="Silaiyil mulaikkum uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadal thaandi neerum poi viduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thaandi neerum poi viduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai thaandi meenum vaazhthiduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Karai thaandi meenum vaazhthiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Anudhinam unai
<input type="checkbox" class="lyrico-select-lyric-line" value="Anudhinam unai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikaiyil manam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ninaikaiyil manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anu anuvaai thudikkudhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Anu anuvaai thudikkudhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumugam thandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirumugam thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru viral kondu
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru viral kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru valigalai thudaithida vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Peru valigalai thudaithida vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi unai madiyinil pera
<input type="checkbox" class="lyrico-select-lyric-line" value="Marubadi unai madiyinil pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvarai thavam kidakkudhu vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvarai thavam kidakkudhu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neeyindri naan vaazha
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyindri naan vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambam yaedhingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aarambam yaedhingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae naan vaazha
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae naan vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatharavaa anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aatharavaa anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaan thaandi suriyanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaan thaandi suriyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorangal poividuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoorangal poividuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola vaazhvellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaai pola vaazhvellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayangal thondruma
<input type="checkbox" class="lyrico-select-lyric-line" value="Nyayangal thondruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalai sainthidu aaraariro
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai sainthidu aaraariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai moodidu aaraariro
<input type="checkbox" class="lyrico-select-lyric-line" value="Imai moodidu aaraariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai sainthidu aaraariro
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalai sainthidu aaraariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai moodidu aaraariro
<input type="checkbox" class="lyrico-select-lyric-line" value="Imai moodidu aaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aruginil valarum piraiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aruginil valarum piraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarnthae paravum mazhaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Valarnthae paravum mazhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan nilavu thiraiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaan nilavu thiraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandae jolikkum azhagae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirandae jolikkum azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa sirandha mozhiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa sirandha mozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Madalgal thirandha vazhiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Madalgal thirandha vazhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan udaindha silaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan udaindha silaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaiyil mulaikkum uyirae
<input type="checkbox" class="lyrico-select-lyric-line" value="Silaiyil mulaikkum uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadal thaandi neerum poi viduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thaandi neerum poi viduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai thaandi meenum vaazhthiduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Karai thaandi meenum vaazhthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thaandi neerum poi viduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadal thaandi neerum poi viduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai thaandi meenum vaazhthiduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Karai thaandi meenum vaazhthiduma"/>
</div>
</pre>