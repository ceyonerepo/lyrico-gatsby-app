---
title: "idhudhana vithidhana song lyrics"
album: "Eghantham"
artist: "Ganesh Raghavendra"
lyricist: "Yuga Bharathi"
director: "Arsal Arumugam"
path: "/albums/eghantham-lyrics"
song: "Idhudhana Vithidhana"
image: ../../images/albumart/eghantham.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/z5o971iXMUw"
type: "sad"
singers:
  - Barani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhuthaana vidhithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaana vidhithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhithannai vidhaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhithannai vidhaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai ingae yaar thaan solluvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ingae yaar thaan solluvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaana vidhithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaana vidhithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhithannai vidhaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhithannai vidhaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai ingae yaar thaan solluvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ingae yaar thaan solluvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil kanneer thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil kanneer thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil kaanal alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil kaanal alaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil kanneer thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil kanneer thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil kaanal alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil kaanal alaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaana vidhithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaana vidhithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhithannai vidhaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhithannai vidhaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai ingae yaar thaan solluvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ingae yaar thaan solluvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa kannammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa kannammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam ennai katti veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam ennai katti veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enuyirai thirudi sendrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enuyirai thirudi sendrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondukulla yaaro vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondukulla yaaro vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal veesi udainthu ponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal veesi udainthu ponathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravae sugam thunba vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravae sugam thunba vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuravae ithu enthan vetkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuravae ithu enthan vetkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiya sathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiya sathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal kondradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal kondradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathadi aathadi enuravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi aathadi enuravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukullae eluthi veithenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullae eluthi veithenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae en uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae en uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai ennum vaal eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai ennum vaal eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyai nee vetti sendraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyai nee vetti sendraayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae yen adhai sithaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae yen adhai sithaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Analai yen pinnae vidhaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analai yen pinnae vidhaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithiyo sathiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiyo sathiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvai kondrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvai kondrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaana vidhithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaana vidhithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhithannai vidhaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhithannai vidhaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai ingae yaar thaan solluvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ingae yaar thaan solluvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaana vidhithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaana vidhithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhithannai vidhaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhithannai vidhaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai ingae yaar thaan solluvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ingae yaar thaan solluvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil kanneer thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil kanneer thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil kaanal alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil kaanal alaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil kanneer thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil kanneer thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyil kaanal alaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyil kaanal alaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana naana thana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naana thana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana naana thana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naana thana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa naa naa thaana nae naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa naa naa thaana nae naa naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana naana thana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naana thana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana naana thana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naana thana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa naa naa thaana nae naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa naa naa thaana nae naa naa"/>
</div>
</pre>
