---
title: "praaptham song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Praaptham"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HqgTM0WkKfk"
type: "mass"
singers:
  - AR Rahman
  - karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engae engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhvom endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhvom endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaa mazhai thuligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaa mazhai thuligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhai mel ondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai mel ondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidhai mel ondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidhai mel ondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhvathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhvathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi vetkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi vetkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayamatra kaalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayamatra kaalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvaiyae uthiram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvaiyae uthiram "/>
</div>
<div class="lyrico-lyrics-wrapper">soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha vazhi yega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha vazhi yega"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha vazhi yega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha vazhi yega"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Logam vegindra pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logam vegindra pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanum mannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum mannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekka chivakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekka chivakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam uthiram aanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam uthiram aanal"/>
</div>
<div class="lyrico-lyrics-wrapper">Semmazhai thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semmazhai thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyyam engum peiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyyam engum peiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiram soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram soozhgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram soozhgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Logam vegindra pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logam vegindra pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanum mannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum mannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sekka chivakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sekka chivakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam uthiram aanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam uthiram aanal"/>
</div>
<div class="lyrico-lyrics-wrapper">Semmazhai thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semmazhai thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaiyyam engum peiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaiyyam engum peiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrigalo tholvigalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrigalo tholvigalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivagalo kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivagalo kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolgal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolgal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriyanai sutruvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanai sutruvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathin saripaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathin saripaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulukkul neenthuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulukkul neenthuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvodu nee kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvodu nee kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam pondra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam pondra "/>
</div>
<div class="lyrico-lyrics-wrapper">saabam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saabam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam pol kaanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam pol kaanum "/>
</div>
<div class="lyrico-lyrics-wrapper">thunbam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunbam ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam pol thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam pol thunbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaptham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaptham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ooo ooo"/>
</div>
</pre>
