---
title: "raaja paatu song lyrics"
album: "Kee"
artist: "Vishal Chandrasekhar"
lyricist: "Sri Rascol"
director: "Kalees"
path: "/albums/kee-lyrics"
song: "Raaja Paatu"
image: ../../images/albumart/kee.jpg
date: 2019-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4U-fZz_whgE"
type: "mass"
singers:
  - Christopher Stanley
  - Sri Rascol
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Annaiku yedho pub-kkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiku yedho pub-kkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nuzhainjaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nuzhainjaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootathil azhaga oruthi mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootathil azhaga oruthi mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathil bar stool yenakkunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil bar stool yenakkunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Utkaarndhu takkunu tequila-vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utkaarndhu takkunu tequila-vai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollivittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollivittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda phone model yennaannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda phone model yennaannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nottam vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nottam vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennoda kangala kokki pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennoda kangala kokki pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikkittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikkittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyo paartha maadhirinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo paartha maadhirinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambichchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambichchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukkulla tequila vandhiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkulla tequila vandhiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sip adichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sip adichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaikku DJ sema moodil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaikku DJ sema moodil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkaadha paattellaam pottu pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaadha paattellaam pottu pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavadichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavadichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinnaala raja paattu onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaala raja paattu onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhukkul bodhaiya yethi kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhukkul bodhaiya yethi kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooda vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooda vittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tha tha tha thokkaa touchu panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tha tha thokkaa touchu panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda aattam poda solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kooda aattam poda solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Lookku vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lookku vittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannoda tequila micham meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannoda tequila micham meedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudichuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Da da da dance-nu solli enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da da da dance-nu solli enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli vittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey yoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on lets partyyyyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on lets partyyyyyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Sri rascol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sri rascol"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu vidha iravu mayakkamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vidha iravu mayakkamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae un azhagu kandu get low
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae un azhagu kandu get low"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae un ilamai aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae un ilamai aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini patharavida palavidhamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini patharavida palavidhamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalil sikkiththom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalil sikkiththom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki sikki thavitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki sikki thavitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikki vikki meendum aditho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikki vikki meendum aditho"/>
</div>
<div class="lyrico-lyrics-wrapper">En udhado thedudho vali thedudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En udhado thedudho vali thedudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me take it slow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me take it slow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadai udai idai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai udai idai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaigal maradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaigal maradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai idai uraiyadum kavidhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai idai uraiyadum kavidhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai vilaiyadum yen thiramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai vilaiyadum yen thiramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Magimai nadanam mavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magimai nadanam mavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palavidham pengalin koota maatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palavidham pengalin koota maatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Damn un asaivu pasangalin thindaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damn un asaivu pasangalin thindaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham dham dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham dham dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Lal la la laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lal la la laaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannoda car la yerikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannoda car la yerikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Geara pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geara pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaala candy crush game a pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaala candy crush game a pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadhanae thanae thalladikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadhanae thanae thalladikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiyum ninnu pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandiyum ninnu pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada muttikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada muttikittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adinga poda enna solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adinga poda enna solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiya thalli thalli podakooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandiya thalli thalli podakooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mothathil mudinju pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothathil mudinju pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada moottakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada moottakattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaikkul innum odudhandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaikkul innum odudhandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh yeh yeh yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeh yeh yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh yeh let us rock baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh yeh let us rock baby"/>
</div>
</pre>
