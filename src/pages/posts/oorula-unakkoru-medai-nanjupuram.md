---
title: "oorula unakkoru medai song lyrics"
album: "Nanjupuram"
artist: "Raaghav"
lyricist: "Magudeshwaran"
director: "Charles"
path: "/albums/nanjupuram-lyrics"
song: "Oorula Unakkoru Medai"
image: ../../images/albumart/nanjupuram.jpg
date: 2011-04-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dXahRHkG-Kg"
type: "happy"
singers:
  - Pushpavanam Kuppuswamy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oorula unakkoru meda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula unakkoru meda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanula unakkodu manjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanula unakkodu manjam"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkkum paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkkum paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">parava paarvaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parava paarvaiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakku keezhoru ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku keezhoru ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanakkuthaanaa iyangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanakkuthaanaa iyangum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum kaatchi kadavul kaatchiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum kaatchi kadavul kaatchiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyellaam maithaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyellaam maithaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottal veli thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottal veli thaambaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai kaala parigaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai kaala parigaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">andharathil sanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharathil sanjaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">pottal veli thaambaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottal veli thaambaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai kaala parigaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai kaala parigaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">andharathil sanjaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharathil sanjaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarba bayam unna mudakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarba bayam unna mudakka"/>
</div>
<div class="lyrico-lyrics-wrapper">saamamenna nanpagalenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamamenna nanpagalenna"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaimugam kaanavumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaimugam kaanavumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu vaarthai pesavumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu vaarthai pesavumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarba bayam unna mudakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarba bayam unna mudakka"/>
</div>
<div class="lyrico-lyrics-wrapper">saamamenna nanpagalenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamamenna nanpagalenna"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaimugam kaanavumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaimugam kaanavumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu vaarthai pesavumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu vaarthai pesavumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">aalamaram vizhuthaanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalamaram vizhuthaanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu padukkum man madimela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu padukkum man madimela"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalillaadha kuranginai poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalillaadha kuranginai poala"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhuriye mannukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhuriye mannukku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalillaadha kuranginai poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalillaadha kuranginai poala"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhuriye mannukku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhuriye mannukku mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorula unakkoru meda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula unakkoru meda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanula unakkodu manjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanula unakkodu manjam"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkkum paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkkum paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">parava paarvaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parava paarvaiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathadikka veyilumadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadikka veyilumadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kadunguliru veesiyadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadunguliru veesiyadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">nerndhirukkum noanbu mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerndhirukkum noanbu mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathadikka veyilumadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadikka veyilumadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">kadunguliru veesiyadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadunguliru veesiyadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">nerndhirukkum noanbu mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerndhirukkum noanbu mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">saathirangal sadangugalellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathirangal sadangugalellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">saaramillaa vaasagangal illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaramillaa vaasagangal illa"/>
</div>
<div class="lyrico-lyrics-wrapper">maathuvadhu elidhu alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathuvadhu elidhu alla"/>
</div>
<div class="lyrico-lyrics-wrapper">manam mayangi aavandhumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam mayangi aavandhumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">maathuvadhu elidhu alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathuvadhu elidhu alla"/>
</div>
<div class="lyrico-lyrics-wrapper">manam mayangi aavandhumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam mayangi aavandhumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorula unakkoru meda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula unakkoru meda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanula unakkodu manjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanula unakkodu manjam"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkkum paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkkum paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">parava paarvaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parava paarvaiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paartha vazhi paarthukidakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha vazhi paarthukidakka "/>
</div>
<div class="lyrico-lyrics-wrapper">paithiyama buthi thavikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paithiyama buthi thavikka"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthukkulley suzhalakkedakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthukkulley suzhalakkedakka"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam pattu iranguvadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam pattu iranguvadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">nethupoala innaikki illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethupoala innaikki illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nelaichiruppadhu edhuvumey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelaichiruppadhu edhuvumey illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nelaichiruppadhu edhuvumey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelaichiruppadhu edhuvumey illa"/>
</div>
<div class="lyrico-lyrics-wrapper">vetrulaga vaasiyappoala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetrulaga vaasiyappoala "/>
</div>
<div class="lyrico-lyrics-wrapper">verubattu vaaduren pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verubattu vaaduren pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vetrulaga vaasiyappoala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetrulaga vaasiyappoala "/>
</div>
<div class="lyrico-lyrics-wrapper">verubattu vaaduren pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verubattu vaaduren pulla"/>
</div>
</pre>
