---
title: "karu karu vizhigalil song lyrics"
album: "Ghajinikanth"
artist: "Balamurali Balu"
lyricist: "Balamurali Balu"
director: "Santhosh P. Jayakumar"
path: "/albums/ghajinikanth-lyrics"
song: "Karu Karu Vizhigalil"
image: ../../images/albumart/ghajinikanth.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/if4XoOx9fF4"
type: "love"
singers:
  - Sanjith Hegde
  - Aishwarya Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karu karu vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu karu vizhigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru gandham thondruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru gandham thondruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru siru udhadugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru siru udhadugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kavvi sendrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kavvi sendrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathin thudipilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin thudipilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru isaithaan ketkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru isaithaan ketkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh pennae pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh pennae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm "/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmm"/>
</div>
</pre>
