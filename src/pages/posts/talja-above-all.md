---
title: "talja song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Japjeet Dhillon"
path: "/albums/above-all-lyrics"
song: "talja"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/wyQg8bBIwLE"
type: "Love"
singers:
  - Jassa Dhillon
  - Deepak Dhillon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh baby bas rehan do na lado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh baby bas rehan do na lado"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere husan de dushmana naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere husan de dushmana naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh zamana zalim ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh zamana zalim ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Te zalim hi rehna ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te zalim hi rehna ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye aa kacchiyan ambiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye aa kacchiyan ambiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Taan main chabh ke kha ju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan main chabh ke kha ju"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhi chal banda ki alet laine saare main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhi chal banda ki alet laine saare main"/>
</div>
<div class="lyrico-lyrics-wrapper">Bichon paad dun mere saale de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bichon paad dun mere saale de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ford de, ford de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ford de, ford de"/>
</div>
<div class="lyrico-lyrics-wrapper">Ford de tochan wargi yaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ford de tochan wargi yaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pai ju pind tere te bhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pai ju pind tere te bhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho vigdeya jatt te jhota same
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho vigdeya jatt te jhota same"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho kar dun ragaan chon garmi saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kar dun ragaan chon garmi saari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naal jo mere oh bande agg
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal jo mere oh bande agg"/>
</div>
<div class="lyrico-lyrics-wrapper">Patandar lot lende ne thugg
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patandar lot lende ne thugg"/>
</div>
<div class="lyrico-lyrics-wrapper">Roz da yabb tu giriyan chab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roz da yabb tu giriyan chab"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho dekh nazare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dekh nazare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laa shartan yaar tera thoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laa shartan yaar tera thoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Te bade saware ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te bade saware ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Je kahe bana da scene main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Je kahe bana da scene main"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunne dubare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunne dubare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laa shartan yaar tera thoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laa shartan yaar tera thoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Te bade saware ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te bade saware ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Je kahe bana da scene main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Je kahe bana da scene main"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunne dubare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunne dubare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gur sidhu music!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gur sidhu music!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho talja ho talja ho talja talja talja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho talja ho talja ho talja talja talja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho talja ho talja ho talja talja talja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho talja ho talja ho talja talja talja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho talja talja ve badmasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho talja talja ve badmasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aivein ho ju koyi tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aivein ho ju koyi tamasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve kahton jaan dukhan vich pauni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve kahton jaan dukhan vich pauni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve koyi laake rakh dun passa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve koyi laake rakh dun passa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kat le rabb ton thoda dar ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kat le rabb ton thoda dar ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve thode ghutt sabar de bhar ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve thode ghutt sabar de bhar ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare pind di ankh vich rad ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare pind di ankh vich rad ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve phadeya jayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve phadeya jayenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aivein vaily bannda bannda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aivein vaily bannda bannda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragadeya jayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragadeya jayenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aivein vaili bannda bannda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aivein vaili bannda bannda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragadeya jayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragadeya jayenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa hukam de ikke dukke ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa hukam de ikke dukke ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere jatt de muhre phikke ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere jatt de muhre phikke ni"/>
</div>
<div class="lyrico-lyrics-wrapper">On flash'an hon kude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On flash'an hon kude"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehnde saah vairi dekhiche ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehnde saah vairi dekhiche ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mast aa rahindi chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast aa rahindi chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Lafdeyan naal kardi ae laal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lafdeyan naal kardi ae laal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayi takkre vailli mittran nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayi takkre vailli mittran nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saada vinga vi na baal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saada vinga vi na baal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baba naal te khideya maal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baba naal te khideya maal"/>
</div>
<div class="lyrico-lyrics-wrapper">Te aaun hulaare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te aaun hulaare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laa shartan yaar tera thoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laa shartan yaar tera thoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Te bade saware ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te bade saware ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Je kahe bana da scene main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Je kahe bana da scene main"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunne dubare ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunne dubare ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beghane putt hoye aa jutt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beghane putt hoye aa jutt"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve raat pawayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve raat pawayenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa patlon da dil kehnda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa patlon da dil kehnda"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu att karayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu att karayenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shot shot ke pauna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shot shot ke pauna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve jatta kurte kaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve jatta kurte kaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa chakk lo chakk lo kehnde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chakk lo chakk lo kehnde"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo pattu rakhda naale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo pattu rakhda naale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho poora tora dhakad chhora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho poora tora dhakad chhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigre bamb kude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigre bamb kude"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah rees jatt di launde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah rees jatt di launde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jaane hamb kude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jaane hamb kude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tohna chakkde roz hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tohna chakkde roz hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Godde thale laa deyage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godde thale laa deyage"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu bol pyaar de bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu bol pyaar de bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni hunne puga deyage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni hunne puga deyage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tauba tauba teriyan gallan ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tauba tauba teriyan gallan ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tauba tauba tere karaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tauba tauba tere karaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Na ishq tere vich bhora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na ishq tere vich bhora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve saare auto ne hathiyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve saare auto ne hathiyar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho din ne char te lambi car
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho din ne char te lambi car"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirre kujh layenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirre kujh layenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jatta vailly bannda bannda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatta vailly bannda bannda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragadeya jayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragadeya jayenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatta vaily bannda bannda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatta vaily bannda bannda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragadeya jayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragadeya jayenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laa shartan yaar tera thoke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laa shartan yaar tera thoke"/>
</div>
<div class="lyrico-lyrics-wrapper">Te bade sanware ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te bade sanware ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho talja ho talja ho talja talja talja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho talja ho talja ho talja talja talja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaily bannda bannda ragadeya jayenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaily bannda bannda ragadeya jayenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho talja ho talja ho talja talja talja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho talja ho talja ho talja talja talja"/>
</div>
<div class="lyrico-lyrics-wrapper">Laa shartan yaar tera thoke te bade sanware ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laa shartan yaar tera thoke te bade sanware ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gur sidhu music!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gur sidhu music!"/>
</div>
</pre>
