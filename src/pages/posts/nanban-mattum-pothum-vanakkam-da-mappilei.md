---
title: "nanban mattum pothum song lyrics"
album: "Vanakkam Da Mappilei"
artist: "G V Prakash Kumar"
lyricist: "Saravedi Saran"
director: "M. Rajesh"
path: "/albums/vanakkam-da-mappilei-song-lyrics"
song: "Nanban Mattum Pothum"
image: ../../images/albumart/vanakkam-da-mappilei.jpg
date: 2021-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/L2QdO6j6cDE"
type: "Friendship"
singers:
  - Arivu
  - Saravedi Saran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">daara kiluchuven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daara kiluchuven "/>
</div>
<div class="lyrico-lyrics-wrapper">daar achuchu daar ah na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daar achuchu daar ah na"/>
</div>
<div class="lyrico-lyrics-wrapper">cear ah aaiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cear ah aaiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban inga varan ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban inga varan ah"/>
</div>
<div class="lyrico-lyrics-wrapper">daar aana nanban varan ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daar aana nanban varan ah"/>
</div>
<div class="lyrico-lyrics-wrapper">daar aana nanban varan ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daar aana nanban varan ah"/>
</div>
<div class="lyrico-lyrics-wrapper">college life athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college life athu"/>
</div>
<div class="lyrico-lyrics-wrapper">vera maari seen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera maari seen da"/>
</div>
<div class="lyrico-lyrics-wrapper">moonu varusam enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu varusam enga"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalka thaaru maaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalka thaaru maaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">college life athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college life athu"/>
</div>
<div class="lyrico-lyrics-wrapper">vera maari seen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera maari seen da"/>
</div>
<div class="lyrico-lyrics-wrapper">moonu varusam enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu varusam enga"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalka thaaru maaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalka thaaru maaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam konjam saptukuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam konjam saptukuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">lunch la naanga molam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lunch la naanga molam"/>
</div>
<div class="lyrico-lyrics-wrapper">adipom last bench la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipom last bench la"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam konjam saptukuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam konjam saptukuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">lunch la naanga molam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lunch la naanga molam"/>
</div>
<div class="lyrico-lyrics-wrapper">adipom last bench la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipom last bench la"/>
</div>
<div class="lyrico-lyrics-wrapper">antha maadi vudum venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha maadi vudum venam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha adi caarum venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha adi caarum venam"/>
</div>
<div class="lyrico-lyrics-wrapper">entha sondhakarum venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha sondhakarum venam"/>
</div>
<div class="lyrico-lyrics-wrapper">en nanban mattum pothum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanban mattum pothum da"/>
</div>
<div class="lyrico-lyrics-wrapper">antha maadi vudum venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha maadi vudum venam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha adi carum venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha adi carum venam"/>
</div>
<div class="lyrico-lyrics-wrapper">entha sondhakarum venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha sondhakarum venam"/>
</div>
<div class="lyrico-lyrics-wrapper">en nanban mattum pothum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanban mattum pothum da"/>
</div>
<div class="lyrico-lyrics-wrapper">exam la bit adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="exam la bit adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiponda knowledge ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiponda knowledge ah"/>
</div>
<div class="lyrico-lyrics-wrapper">culturals vanthuchu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="culturals vanthuchu na"/>
</div>
<div class="lyrico-lyrics-wrapper">alara viduvom college ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alara viduvom college ah"/>
</div>
<div class="lyrico-lyrics-wrapper">navuru navuru navuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navuru navuru navuru"/>
</div>
<div class="lyrico-lyrics-wrapper">navuru navuru navuru eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navuru navuru navuru eh eh"/>
</div>
<div class="lyrico-lyrics-wrapper">exam la bit adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="exam la bit adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiponda knowledge ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiponda knowledge ah"/>
</div>
<div class="lyrico-lyrics-wrapper">culturals vanthuchu na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="culturals vanthuchu na"/>
</div>
<div class="lyrico-lyrics-wrapper">alara viduvom college ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alara viduvom college ah"/>
</div>
<div class="lyrico-lyrics-wrapper">figure ah usaar panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="figure ah usaar panni"/>
</div>
<div class="lyrico-lyrics-wrapper">suthurathu perusu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthurathu perusu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">friend oda photo irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friend oda photo irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda purse la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda purse la"/>
</div>
<div class="lyrico-lyrics-wrapper">figure ah usaar panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="figure ah usaar panni"/>
</div>
<div class="lyrico-lyrics-wrapper">suthurathu perusu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthurathu perusu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">friend oda photo irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friend oda photo irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda purse la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda purse la"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda purse la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda purse la"/>
</div>
<div class="lyrico-lyrics-wrapper">peruku padipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruku padipom"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirayum kudipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirayum kudipom"/>
</div>
<div class="lyrico-lyrics-wrapper">en friend ah ne thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en friend ah ne thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiya kaala odaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiya kaala odaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">peruku padipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruku padipom"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirayum kudipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirayum kudipom"/>
</div>
<div class="lyrico-lyrics-wrapper">en friend ah ne thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en friend ah ne thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiya kaala odaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiya kaala odaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">pakathu college ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathu college ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">irukuthu da pakka va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukuthu da pakka va"/>
</div>
<div class="lyrico-lyrics-wrapper">enga college ponnunga mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga college ponnunga mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya moonchi akka va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya moonchi akka va"/>
</div>
<div class="lyrico-lyrics-wrapper">pakathu college ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakathu college ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">irukuthu da pakka va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukuthu da pakka va"/>
</div>
<div class="lyrico-lyrics-wrapper">enga college ponnunga mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga college ponnunga mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya moonchi akka va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya moonchi akka va"/>
</div>
<div class="lyrico-lyrics-wrapper">kuppa pola senthu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppa pola senthu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">avalo keethu arear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avalo keethu arear"/>
</div>
<div class="lyrico-lyrics-wrapper">enga than da poga pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga than da poga pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda career uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda career uh"/>
</div>
<div class="lyrico-lyrics-wrapper">kuppa pola senthu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppa pola senthu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">avalo keethu arear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avalo keethu arear"/>
</div>
<div class="lyrico-lyrics-wrapper">enga than da poga pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga than da poga pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda career uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda career uh"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda career uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda career uh"/>
</div>
<div class="lyrico-lyrics-wrapper">en machanum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en machanum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">en mamavum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mamavum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">en nanbanum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanbanum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku ellame nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku ellame nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">en machanum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en machanum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">en mamavum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mamavum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">en nanbanum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nanbanum nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku ellame nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku ellame nee than"/>
</div>
</pre>
