---
title: "aandavan thoorigayil song lyrics"
album: "Champion"
artist: "Arrol Corelli"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/champion-lyrics"
song: "Aandavan Thoorigayil"
image: ../../images/albumart/champion.jpg
date: 2019-12-13
lang: tamil
youtubeLink: 
type: "love"
singers:
  - Karthik Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aandavan Thoorigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Thoorigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manuda Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manuda Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arai Nodi Kobathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arai Nodi Kobathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vannam Poi Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vannam Poi Vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaanai Pol Thondrum Kobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanai Pol Thondrum Kobam"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbaaga Theindhae Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbaaga Theindhae Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathiru Kaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathiru Kaathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathiram Serthae Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathiram Serthae Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathiram Illai Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathiram Illai Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vazhi Paadhai Vaazhkai Maravaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vazhi Paadhai Vaazhkai Maravaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maravaadhae Manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaadhae Manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Thavaraalae Uyir Pari Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Thavaraalae Uyir Pari Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravaadhae Manamae Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaadhae Manamae Vaazhvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravaadhae Manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaadhae Manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhal Kooda Un Kaal Vaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhal Kooda Un Kaal Vaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravaadhae Manamae Vaazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maravaadhae Manamae Vaazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soozhnilaiyin Kaiyil Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhnilaiyin Kaiyil Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhi Ena Maaridalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhi Ena Maaridalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Dhoosu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Dhoosu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaalam Povadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaalam Povadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaichu Vecha Irumbin Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaichu Vecha Irumbin Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootridum Neerai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootridum Neerai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeri Paaivadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Paaivadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kobam Aalvadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kobam Aalvadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Oru Iniyavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Oru Iniyavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sinam Pagai Aakkividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sinam Pagai Aakkividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Oru Kodiyavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Oru Kodiyavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagai Thozhamai Aakki Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagai Thozhamai Aakki Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavan Thoorigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Thoorigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manuda Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manuda Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arai Nodi Kobathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arai Nodi Kobathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vannam Poi Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vannam Poi Vidum"/>
</div>
</pre>
