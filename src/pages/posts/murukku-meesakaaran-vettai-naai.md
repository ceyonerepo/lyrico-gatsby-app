---
title: "murukku meesakaaran song lyrics"
album: "Vettai Naai"
artist: "Ganesh Chandrasekaran"
lyricist: "Mohanrajan"
director: "S. Jai Shankar"
path: "/albums/vettai-naai-lyrics"
song: "Murukku Meesakaaran"
image: ../../images/albumart/vettai-naai.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bRAYtAla0LM"
type: "Love"
singers:
  - Shakthisree Gopalan
  - Sreekanth Hariharan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">murukku meesakaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murukku meesakaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">morappa paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="morappa paakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">murattu veesham potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murattu veesham potu"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaga thaakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaga thaakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">asaththum azhagukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaththum azhagukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">aala kavukkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala kavukkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasaal manasa keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasaal manasa keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa kaatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa kaatturaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kora paayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kora paayila"/>
</div>
<div class="lyrico-lyrics-wrapper">kolam poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolam poduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu thoongaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu thoongaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">oora kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam kaatuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam kaatuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhli koovayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhli koovayila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamam iranthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamam iranthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">maala mathayula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maala mathayula"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal vaazhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal vaazhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhnthu paakayula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhnthu paakayula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">murukku meesakaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murukku meesakaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">morappa paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="morappa paakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">murattu veesham potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murattu veesham potu"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaga thaakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaga thaakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuzhanthayai kuthikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuzhanthayai kuthikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pesidum othadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesidum othadu"/>
</div>
<div class="lyrico-lyrics-wrapper">mounamai natakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounamai natakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thodum pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thodum pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakulle naa vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakulle naa vara"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaikuthe manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaikuthe manathu"/>
</div>
<div class="lyrico-lyrics-wrapper">utalena usurena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="utalena usurena"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvathe uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvathe uravu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasa athigam vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa athigam vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">aala asara vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala asara vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">aazham puriya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aazham puriya vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">anbalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbalathan"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa masiya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa masiya vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasa pasikka vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasa pasikka vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaippa pesaya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaippa pesaya vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannalathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udhatil neruppa vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhatil neruppa vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">odamba kulira vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odamba kulira vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">onappa peruga vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onappa peruga vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamathan"/>
</div>
<div class="lyrico-lyrics-wrapper">kanava peruga vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanava peruga vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">usura uruga vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura uruga vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">nezhala orasa vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nezhala orasa vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongamathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongamathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagathil usanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathil usanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee tharum paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee tharum paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">enakathu podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakathu podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">veranna venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veranna venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenaikura nenaipellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaikura nenaipellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mattum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">irukura edamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukura edamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vara venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vara venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karadu muradu yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadu muradu yena"/>
</div>
<div class="lyrico-lyrics-wrapper">iruntha manasa mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruntha manasa mella"/>
</div>
<div class="lyrico-lyrics-wrapper">kanniya kanniya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanniya kanniya vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">thannalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannalathan"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkum unakkum ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkum unakkum ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">nerukkam nerukkam adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerukkam nerukkam adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">innum iruga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum iruga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbalathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhigal thiranthapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal thiranthapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam maranthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam maranthathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu methakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu methakuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkamathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkamathan"/>
</div>
<div class="lyrico-lyrics-wrapper">usuru irukkum nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usuru irukkum nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">uravu irukkummadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravu irukkummadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu jeyukumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu jeyukumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unnalathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnalathan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">murukku meesakaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murukku meesakaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">morappa paakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="morappa paakuran"/>
</div>
<div class="lyrico-lyrics-wrapper">murattu veesham potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="murattu veesham potu"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaga thaakuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaga thaakuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">asaththum azhagukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaththum azhagukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">aala kavukkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala kavukkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasaal manasa keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasaal manasa keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa kaatturaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa kaatturaa"/>
</div>
</pre>
