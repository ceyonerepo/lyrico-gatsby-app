---
title: "engeyum kaadhal song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Engeyum Kaadhal"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OH3_Jna80Uk"
type: "happy"
singers:
  - Aalap Raju
  - Devan Ekambaram
  - Ranina Reddy	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engeyum Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyum Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil Vanthu Ovvondrum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Vanthu Ovvondrum Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Kaalai Saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Kaalai Saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugathinil Vanthu Sattendru Modha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathinil Vanthu Sattendru Modha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadha Paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha Paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasam Thanthu Paathathil Oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasam Thanthu Paathathil Oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Varum Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Varum Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Munnooru Aandu Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Munnooru Aandu Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Ennum Thaene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Ennum Thaene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Alaigalil Kaanum Neelam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Alaigalil Kaanum Neelam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaane Vanna Meene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaane Vanna Meene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Veyil Ena Naangu Kaalam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Veyil Ena Naangu Kaalam Neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadarkaraiyil Athan Manal Veliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadarkaraiyil Athan Manal Veliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaatrodu Kaatraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaatrodu Kaatraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakkuralgal Pala Pala Viralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakkuralgal Pala Pala Viralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamai Pathivu Seithirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamai Pathivu Seithirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyalilum Nadu Iravinilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalilum Nadu Iravinilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Oyaathey Oyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Oyaathey Oyaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripinilum Pala Sinangalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripinilum Pala Sinangalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Miga Kalanthu Kaathirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga Kalanthu Kaathirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Pesamal Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Pesamal Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Thaangaathu Thaangathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Thaangaathu Thaangathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalthaan Pinbu Thoongaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalthaan Pinbu Thoongaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyum Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyum Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil Vandhu Ovvondrum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Vandhu Ovvondrum Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Kaalai Saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Kaalai Saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugathinil Vanthu Sattendru Modha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathinil Vanthu Sattendru Modha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadha Paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha Paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasam Thanthu Paathaththil Oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasam Thanthu Paathaththil Oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Varum Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Varum Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Munnooru Aandu Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Munnooru Aandu Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adam Pidikkum Ithu Vadam Izhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adam Pidikkum Ithu Vadam Izhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Sonnaalum Ketkaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Sonnaalum Ketkaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Marukkum Pin Thalai Kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Marukkum Pin Thalai Kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Purandu Theerthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Purandu Theerthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugangalaiyo Udal Nirangalaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugangalaiyo Udal Nirangalaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Paarkaathey Paarkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Paarkaathey Paarkaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Udalil Oar Uyir Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Udalil Oar Uyir Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Muyandru Paarthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Muyandru Paarthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Yaarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Yaarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae Nesikka Nernthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Nesikka Nernthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ange Poonthottam Undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ange Poonthottam Undaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonchendaai Bhoomi Thindaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonchendaai Bhoomi Thindaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeyum Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyum Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil Vanthu Ovvondrum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Vanthu Ovvondrum Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Kaalai Saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Kaalai Saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugathinil Vanthu Sattendru Modha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathinil Vanthu Sattendru Modha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadha Paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha Paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasam Thanthu Paathathil Oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasam Thanthu Paathathil Oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Varum Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Varum Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Munnooru Aandu Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Munnooru Aandu Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Ennum Thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Ennum Thaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Alaigalil Kaanum Neelam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Alaigalil Kaanum Neelam Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanae Vanna Meenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanae Vanna Meenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Veyil Ena Naangu Kaalam Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Veyil Ena Naangu Kaalam Neeyae"/>
</div>
</pre>
