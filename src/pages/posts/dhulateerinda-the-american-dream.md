---
title: "dhulateerinda song lyrics"
album: "The American Dream"
artist: "Abhinay TJ"
lyricist: "Aditya iyengar"
director: "Dr. Vighnesh Koushik "
path: "/albums/the-american-dream-lyrics"
song: "Dhulateerinda"
image: ../../images/albumart/the-american-dream.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yfenHgiub5o"
type: "happy"
singers:
  - Harshika gudi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dhulateerinda box 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhulateerinda box "/>
</div>
<div class="lyrico-lyrics-wrapper">motham badhalaipointha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motham badhalaipointha "/>
</div>
<div class="lyrico-lyrics-wrapper">pulusu kaarindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulusu kaarindha"/>
</div>
<div class="lyrico-lyrics-wrapper">pedha rod ea neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedha rod ea neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">kindha diginda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kindha diginda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhulateerinda box 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhulateerinda box "/>
</div>
<div class="lyrico-lyrics-wrapper">motham badhalaipointha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motham badhalaipointha "/>
</div>
<div class="lyrico-lyrics-wrapper">pulusu kaarindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulusu kaarindha"/>
</div>
<div class="lyrico-lyrics-wrapper">pedha rod ea neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedha rod ea neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">kindha diginda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kindha diginda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee gaadha cheppalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gaadha cheppalante"/>
</div>
<div class="lyrico-lyrics-wrapper">simple bashe saripodhoi raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simple bashe saripodhoi raja"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamantha maaripoyenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamantha maaripoyenu"/>
</div>
<div class="lyrico-lyrics-wrapper">chudu dreamulanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chudu dreamulanni "/>
</div>
<div class="lyrico-lyrics-wrapper">mingipoyenu nedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mingipoyenu nedu "/>
</div>
<div class="lyrico-lyrics-wrapper">raata maarutundemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raata maarutundemo"/>
</div>
<div class="lyrico-lyrics-wrapper">anukunte devudaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukunte devudaina "/>
</div>
<div class="lyrico-lyrics-wrapper">jaatakanni sari diddaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaatakanni sari diddaledu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhulateerinda box 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhulateerinda box "/>
</div>
<div class="lyrico-lyrics-wrapper">motham badhalaipointha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motham badhalaipointha "/>
</div>
<div class="lyrico-lyrics-wrapper">pulusu kaarindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulusu kaarindha"/>
</div>
<div class="lyrico-lyrics-wrapper">pedha rod ea neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedha rod ea neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">kindha diginda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kindha diginda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panoduaina vantoduaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panoduaina vantoduaina"/>
</div>
<div class="lyrico-lyrics-wrapper">yedhaiomna nuvvega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedhaiomna nuvvega"/>
</div>
<div class="lyrico-lyrics-wrapper">bodi padinthe pedhadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodi padinthe pedhadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">ee kotha malupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee kotha malupu"/>
</div>
<div class="lyrico-lyrics-wrapper">neekunna balupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekunna balupu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhara oo kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhara oo kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">uacha yendharu vadhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uacha yendharu vadhanna"/>
</div>
<div class="lyrico-lyrics-wrapper">ollantha vanchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ollantha vanchi"/>
</div>
<div class="lyrico-lyrics-wrapper">aachi toochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aachi toochi"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu kadagara paachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu kadagara paachi"/>
</div>
<div class="lyrico-lyrics-wrapper">cheyyantha vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheyyantha vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu peekedeuti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu peekedeuti"/>
</div>
<div class="lyrico-lyrics-wrapper">antaku thinchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antaku thinchi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee gaadha cheppalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gaadha cheppalante"/>
</div>
<div class="lyrico-lyrics-wrapper">simple bashe saripodhoi raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simple bashe saripodhoi raja"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamantha maaripoyenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamantha maaripoyenu"/>
</div>
<div class="lyrico-lyrics-wrapper">chudu dreamulanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chudu dreamulanni "/>
</div>
<div class="lyrico-lyrics-wrapper">mingipoyenu nedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mingipoyenu nedu "/>
</div>
<div class="lyrico-lyrics-wrapper">raata maarutundemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raata maarutundemo"/>
</div>
<div class="lyrico-lyrics-wrapper">anukunte sacchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukunte sacchina"/>
</div>
<div class="lyrico-lyrics-wrapper">maaranandi inka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaranandi inka"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu yedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu yedu"/>
</div>
</pre>
