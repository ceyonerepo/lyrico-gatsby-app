---
title: "chinna chinna kavithai song lyrics"
album: "Saayam"
artist: "Nagha Udhayan"
lyricist: "Viveka"
director: "Antony Samy"
path: "/albums/saayam-lyrics"
song: "Chinna Chinna Kavithai"
image: ../../images/albumart/saayam.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Vb3dOvJVWHY"
type: "love"
singers:
  - Prasanna Rao
  - Surumugi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chinna chinna kavithai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna kavithai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">un siripula uthiruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un siripula uthiruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalula nee nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalula nee nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">antha veyilukkum kuliruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha veyilukkum kuliruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kanda maru nodi ulagathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kanda maru nodi ulagathula "/>
</div>
<div class="lyrico-lyrics-wrapper">enna thoda theriyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thoda theriyalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanna nilava vanthu nikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna nilava vanthu nikira"/>
</div>
<div class="lyrico-lyrics-wrapper">un patham patta paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un patham patta paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">manna padam panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna padam panni"/>
</div>
<div class="lyrico-lyrics-wrapper">maati vaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maati vaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoondil neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondil neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">adi thullum meenanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi thullum meenanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chinna chinna kavithai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna kavithai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">un siripula uthiruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un siripula uthiruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalula naa nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalula naa nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">antha veyilukkum kuliruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha veyilukkum kuliruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muthal muthala mala polinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal muthala mala polinja"/>
</div>
<div class="lyrico-lyrics-wrapper">manalveliya en manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manalveliya en manam "/>
</div>
<div class="lyrico-lyrics-wrapper">muducha indru magilchiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muducha indru magilchiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kulikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulikuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iruvizhi illama ithayathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruvizhi illama ithayathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">en ithayathuku un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ithayathuku un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">mattum theriyuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum theriyuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">minnal mugam moosi vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal mugam moosi vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">meni ennai thaakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meni ennai thaakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thaakum pothe innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaakum pothe innum"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum endru nenjam ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum endru nenjam ketkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnodu irunthidum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu irunthidum tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu en vaalvil kidaithita amutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu en vaalvil kidaithita amutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yengi thavikiren en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengi thavikiren en"/>
</div>
<div class="lyrico-lyrics-wrapper">nilalum than nadunguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilalum than nadunguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chinna chinna kavithai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna kavithai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">un siripula uthiruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un siripula uthiruthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paravaigalai naan paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaigalai naan paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sendru kanavugalai oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendru kanavugalai oru"/>
</div>
<div class="lyrico-lyrics-wrapper">koodugale ingu kavithaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodugale ingu kavithaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">varaivomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varaivomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadal alavu ulla kathala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal alavu ulla kathala than"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigalile en vizhigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalile en vizhigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">naan ana katti vaipene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan ana katti vaipene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannum kannum mothum oosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannum kannum mothum oosai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulle ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulle ketkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam neram yavum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam neram yavum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril poga vaaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril poga vaaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee thane ennoda usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thane ennoda usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu verethum venamdi visuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu verethum venamdi visuru"/>
</div>
<div class="lyrico-lyrics-wrapper">yengi thavikiran en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengi thavikiran en"/>
</div>
<div class="lyrico-lyrics-wrapper">nilalum than nadunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilalum than nadunguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chinna chinna kavithai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna kavithai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">un siripula uthiruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un siripula uthiruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalula naa nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalula naa nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">antha veyilukkum kuliruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha veyilukkum kuliruthadi"/>
</div>
</pre>
