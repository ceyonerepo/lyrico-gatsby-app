---
title: "ayya kalarey song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Andony Dasan"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Ayya Kalarey Kalar Vaasam"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2021-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Fb2iD4zc_PY"
type: "sad"
singers:
  - Andony Dasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ayya kalarey kalar vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya kalarey kalar vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalarey kalar vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalarey kalar vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">karthigayum poo vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karthigayum poo vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">indha sandaala seemayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha sandaala seemayila"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vittu ponadhu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vittu ponadhu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">yen manasa rambam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen manasa rambam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">arrukudhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arrukudhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pona yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pona yedam"/>
</div>
<div class="lyrico-lyrics-wrapper">periya yedam periya yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="periya yedam periya yedam"/>
</div>
<div class="lyrico-lyrics-wrapper">punniyavan vaazhum yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punniyavan vaazhum yedam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thedum yedam kedaikalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedum yedam kedaikalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thedum yedam kedaikalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedum yedam kedaikalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thembalaye yen raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembalaye yen raasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaalamulla pallathuku pallathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalamulla pallathuku pallathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thalamuzhuga ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalamuzhuga ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya thalamuzhuga ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya thalamuzhuga ponathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaalamullu thachadhunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalamullu thachadhunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalamullu thachadhunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalamullu thachadhunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">thala saanja maayam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala saanja maayam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thala saanja mayam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala saanja mayam enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayya paakamecha aramanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya paakamecha aramanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">aramanaiyum panjanga saavadiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aramanaiyum panjanga saavadiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaku panjaanga saavadayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaku panjaanga saavadayum"/>
</div>
<div class="lyrico-lyrics-wrapper">sekkanummu nenachavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekkanummu nenachavara"/>
</div>
<div class="lyrico-lyrics-wrapper">sekkanummu nenachavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sekkanummu nenachavara"/>
</div>
<div class="lyrico-lyrics-wrapper">aaradi than panju metha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi than panju metha"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku aaradi then panju metha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku aaradi then panju metha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayya dhesadhi desamengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya dhesadhi desamengum"/>
</div>
<div class="lyrico-lyrics-wrapper">dhesamengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhesamengum"/>
</div>
<div class="lyrico-lyrics-wrapper">kotakatta nenacha paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotakatta nenacha paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">aya kotta katta nenacha paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aya kotta katta nenacha paavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaradi kotta ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi kotta ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">indha aaru adi kotta ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha aaru adi kotta ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">neeti padukka nimmadhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeti padukka nimmadhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee neeti padukka nimmadhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee neeti padukka nimmadhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya neeti padukka nimmadhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya neeti padukka nimmadhiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yen ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">yen ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">yen ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna peththa ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna peththa ayya"/>
</div>
</pre>
