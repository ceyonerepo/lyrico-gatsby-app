---
title: "yaarin mel pizhai song lyrics"
album: "Vellai Pookal"
artist: "Ramgopal Krishnaraju"
lyricist: "Madhan Karky"
director: "Vivek Elangovan"
path: "/albums/vellai-pookal-lyrics"
song: "Yaarin Mel Pizhai"
image: ../../images/albumart/vellai-pookal.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4VuROPG7Cq8"
type: "melody"
singers:
  - Sathyaprakash
  - TS Ayyappan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uravondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirai Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirai Aanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraiyaanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraiyaanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kobangal Neengaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kobangal Neengaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Poli Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Poli Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Paasangal Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Paasangal Sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Neenda Nadippu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Neenda Nadippu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooram Pala Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Pala Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthinge Vandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthinge Vandhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjathai Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjathai Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Netril Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Netril Vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthai Oru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Oru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnulle Pirandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnulle Pirandhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkaigal Neeki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkaigal Neeki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vaanil Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vaanil Vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraikaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraivadhu Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivadhu Kaalam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marukaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaalgal Pinn Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaalgal Pinn Sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kangal Mei Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal Mei Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kobangal Neengaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kobangal Neengaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Poli Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Poli Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Paasangal Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Paasangal Sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Neenda Nadippu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Neenda Nadippu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Pala Aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Pala Aara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Naatkal Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Naatkal Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Sila Aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Sila Aara"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Aayul Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aayul Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Adhai Koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Adhai Koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Kodi Vazhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Kodi Vazhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobam Sila Koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam Sila Koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingillai Mozhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingillai Mozhigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirapaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirapaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaithathu Nee Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaithathu Nee Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Marappaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marappaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Maunam Thandikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maunam Thandikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenjam Mannikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjam Mannikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kobangal Neengaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kobangal Neengaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Poli Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Poli Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Paasangal Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Paasangal Sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Neenda Nadippu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Neenda Nadippu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarin Mel Pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarin Mel Pizhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kobangal Neengaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kobangal Neengaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Poli Sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Poli Sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Paasangal Sollaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Paasangal Sollaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Neenda Nadippu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Neenda Nadippu Yen"/>
</div>
</pre>
