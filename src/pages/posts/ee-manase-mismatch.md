---
title: "ee manase song lyrics"
album: "Mismatch"
artist: "Gifton Elias"
lyricist: "Sirivennela Seetharama Sastry"
director: "N V Nirmal Kumar"
path: "/albums/mismatch-lyrics"
song: "Ee Manase"
image: ../../images/albumart/mismatch.jpg
date: 2019-12-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/B-i9-akCExU"
type: "love"
singers:
  - LV Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ee manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">parugedutondi neekese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugedutondi neekese"/>
</div>
<div class="lyrico-lyrics-wrapper">vinamantondi tana oosee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinamantondi tana oosee"/>
</div>
<div class="lyrico-lyrics-wrapper">alalegase kalavaramaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alalegase kalavaramaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tanalo ninu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tanalo ninu choose"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enno kalalanu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enno kalalanu choose"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne kanukodilese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanukodilese"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve tanu vethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve tanu vethike"/>
</div>
<div class="lyrico-lyrics-wrapper">aa tolvelugani telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa tolvelugani telise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enno kalalanu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enno kalalanu choose"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne kanukodilese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne kanukodilese"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve tanu vethike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve tanu vethike"/>
</div>
<div class="lyrico-lyrics-wrapper">aa tolvelugani telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa tolvelugani telise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">korukunna teeranne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korukunna teeranne"/>
</div>
<div class="lyrico-lyrics-wrapper">taanu cherenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taanu cherenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">teeriponi aaratamto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teeriponi aaratamto"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavarinchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavarinchenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venakane tirugutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venakane tirugutu"/>
</div>
<div class="lyrico-lyrics-wrapper">cheli jata viduvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheli jata viduvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dorikina varamuto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dorikina varamuto"/>
</div>
<div class="lyrico-lyrics-wrapper">kudhuruga niluvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudhuruga niluvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">emcheste baaguntundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emcheste baaguntundo"/>
</div>
<div class="lyrico-lyrics-wrapper">cheppani vintha nase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppani vintha nase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeto chelimini chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeto chelimini chese"/>
</div>
<div class="lyrico-lyrics-wrapper">neelo chaluvanu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo chaluvanu choose"/>
</div>
<div class="lyrico-lyrics-wrapper">ayinaa inkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayinaa inkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">edo adige atyaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo adige atyaase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeto chelimini chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeto chelimini chese"/>
</div>
<div class="lyrico-lyrics-wrapper">neelo chaluvanu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo chaluvanu choose"/>
</div>
<div class="lyrico-lyrics-wrapper">ayinaa inkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayinaa inkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">edo adige atyaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo adige atyaase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">velluvanti nee sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velluvanti nee sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">nannu allinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nannu allinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vennelanti nee navvullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennelanti nee navvullo"/>
</div>
<div class="lyrico-lyrics-wrapper">chemmagillina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chemmagillina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tahataha taragadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tahataha taragadu"/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi anagadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi anagadu"/>
</div>
<div class="lyrico-lyrics-wrapper">tana sodha idi ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tana sodha idi ani"/>
</div>
<div class="lyrico-lyrics-wrapper">talapunu telupadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="talapunu telupadu"/>
</div>
<div class="lyrico-lyrics-wrapper">emiste shantis tundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emiste shantis tundo"/>
</div>
<div class="lyrico-lyrics-wrapper">telusaa yem varase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="telusaa yem varase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">parugedutondi neekese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parugedutondi neekese"/>
</div>
<div class="lyrico-lyrics-wrapper">vinamantondi tana oosee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinamantondi tana oosee"/>
</div>
<div class="lyrico-lyrics-wrapper">alalegase kalavaramaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alalegase kalavaramaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tanalo ninu choose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tanalo ninu choose"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ee manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
<div class="lyrico-lyrics-wrapper">naa manase se se se 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa manase se se se "/>
</div>
<div class="lyrico-lyrics-wrapper">se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se se se se"/>
</div>
</pre>
