---
title: "sharminda haan song lyrics"
album: "Sun Ve Rabba part 2"
artist: "Gurmeet Singh"
lyricist: "Kulwant Garaia"
director: "Gurmeet Singh"
path: "/albums/sun-ve-rabba-part2-lyrics"
song: "Sharminda Haan"
image: ../../images/albumart/sun-ve-rabba-part2.jpg
date: 2021-04-17
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/PHnbKTFXAYE"
type: "Love"
singers:
  - Khan Saab
  - Mannat Noor
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haan.. Haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan.. Haan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main tere mere rishte nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tere mere rishte nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi vi naam ni de paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi vi naam ni de paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main teri pak mohabbat nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main teri pak mohabbat nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Safal anzaam ni de paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Safal anzaam ni de paaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu chup si te main rondi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu chup si te main rondi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakh na ho tarle paundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakh na ho tarle paundi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu chup si te main rondi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu chup si te main rondi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Wakh na ho tarle paundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wakh na ho tarle paundi rahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tainu vaar vaar samjhaundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu vaar vaar samjhaundi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Socheya si tere bina mar jyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Socheya si tere bina mar jyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya nu alvida kar jyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya nu alvida kar jyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Par zinda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par zinda haan sharminda haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na tere layin khad paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na tere layin khad paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinna tainu tadpaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinna tainu tadpaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu bewafa bewafa bewafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu bewafa bewafa bewafa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu wang pagalan chahya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu wang pagalan chahya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main ki tera mull paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main ki tera mull paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main bewafa bewafa bewafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main bewafa bewafa bewafa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main rondi rahi kurlaundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main rondi rahi kurlaundi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere pairan nu hath laundi rahiy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere pairan nu hath laundi rahiy"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu vaade yaad karaundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu vaade yaad karaundi rahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lagda na isi injh dar janga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagda na isi injh dar janga"/>
</div>
<div class="lyrico-lyrics-wrapper">Socheya si tere bina mar jaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Socheya si tere bina mar jaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Par zinda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par zinda haan sharminda haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu rabb da si sarmaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu rabb da si sarmaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri chahat nu thukraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri chahat nu thukraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu kafira kafira kafira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kafira kafira kafira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu apna aap lutaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu apna aap lutaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main pith te vaar chalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main pith te vaar chalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main kafir haan kafir haan kafir haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main kafir haan kafir haan kafir haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulwant main phir vi chaundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulwant main phir vi chaundi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Te khud te dosh lagaundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Te khud te dosh lagaundi rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokan ton sach lukaundi rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokan ton sach lukaundi rahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere thode dukh ghat kar jyanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere thode dukh ghat kar jyanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Socheya si tere bina mar jaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Socheya si tere bina mar jaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Par zinda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par zinda haan sharminda haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharminda haan sharminda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharminda haan sharminda"/>
</div>
</pre>
