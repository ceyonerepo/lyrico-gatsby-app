---
title: "vaan mathi marainthidum song lyrics"
album: "Calls"
artist: "Thameem Ansari"
lyricist: "Dr. Nandhudasan"
director: "J. Sabarish"
path: "/albums/calls-lyrics"
song: "Vaan Mathi Marainthidum"
image: ../../images/albumart/calls.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sR0p5I_4oUE"
type: "Sad"
singers:
  - Ajaey shravan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaan mathi marainthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mathi marainthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee vizhi thoongathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee vizhi thoongathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo athu puyalena maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo athu puyalena maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean thuli maarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean thuli maarathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam oru podhum marainthu vidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam oru podhum marainthu vidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi nee odu kadanthuvidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi nee odu kadanthuvidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram sumaiyaga irunthuvidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram sumaiyaga irunthuvidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaikkai unathaga mathi vazhi nadaiyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaikkai unathaga mathi vazhi nadaiyidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Analin vizhi endrum azha vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analin vizhi endrum azha vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koba kanneerai thara vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koba kanneerai thara vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvi endrum vaarthai ini vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvi endrum vaarthai ini vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thiramai ulagalum marakkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thiramai ulagalum marakkathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigalum vazhvai mudakkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalum vazhvai mudakkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhairiyam endradrum thorkkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhairiyam endradrum thorkkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madaigalum mazhaiyai thadukkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madaigalum mazhaiyai thadukkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha saambalin kaatru theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha saambalin kaatru theriyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan mathi marainthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mathi marainthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee vizhi thoongathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee vizhi thoongathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo athu puyalena maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo athu puyalena maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean thuli maarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean thuli maarathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam oru podhum marainthu vidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam oru podhum marainthu vidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi nee odu kadanthuvidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi nee odu kadanthuvidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram sumaiyaga irunthuvidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram sumaiyaga irunthuvidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaikkai unathaga mathi vazhi nadaiyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaikkai unathaga mathi vazhi nadaiyidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchamillaiye achchamillaiye unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchamillaiye achchamillaiye unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunive thunaiyena vidhai ondru podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunive thunaiyena vidhai ondru podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai irunthal unmai irunthal ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai irunthal unmai irunthal ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram adhu thaan nee odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram adhu thaan nee odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennam uyarnthu ennam uyarnthu yezhunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam uyarnthu ennam uyarnthu yezhunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai thuli unnil uzhaippe thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai thuli unnil uzhaippe thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannai maranthu thannai maranthu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai maranthu thannai maranthu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaikkinai adaiya poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaikkinai adaiya poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oru murai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru murai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo ulagil piranthiduvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo ulagil piranthiduvayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen enbaval thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen enbaval thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil arithana pirappinai eduppaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil arithana pirappinai eduppaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigalum vazhvai mudakkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalum vazhvai mudakkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhairiyam endradrum thorkkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhairiyam endradrum thorkkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madaigalum mazhaiyai thadukkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madaigalum mazhaiyai thadukkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha saambalin kaatru theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha saambalin kaatru theriyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Analin vizhi endrum azha vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analin vizhi endrum azha vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koba kanneerai thara vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koba kanneerai thara vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhvi endrum vaarthai ini vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhvi endrum vaarthai ini vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thiramai ulagalum marakkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thiramai ulagalum marakkathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan mathi marainthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan mathi marainthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee vizhi thoongathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee vizhi thoongathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo athu puyalena maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo athu puyalena maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean thuli maarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean thuli maarathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam oru podhum marainthu vidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam oru podhum marainthu vidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi nee odu kadanthuvidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi nee odu kadanthuvidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram sumaiyaga irunthuvidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram sumaiyaga irunthuvidathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaikkai unathaga mathi vazhi nadaiyidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaikkai unathaga mathi vazhi nadaiyidu"/>
</div>
</pre>
