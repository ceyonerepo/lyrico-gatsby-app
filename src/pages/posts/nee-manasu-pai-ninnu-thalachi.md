---
title: "nee manasu pai song lyrics"
album: "Ninnu Thalachi"
artist: "Yellender mahaveera"
lyricist: "Shree Mani"
director: "Anil thota"
path: "/albums/ninnu-thalachi-lyrics"
song: "Nee Manasu Pai"
image: ../../images/albumart/ninnu-thalachi.jpg
date: 2019-09-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Yqm1l_aF1WE"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Manasu Pai Musuge Theesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manasu Pai Musuge Theesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Vayasulo Lusuge Chusai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Vayasulo Lusuge Chusai"/>
</div>
<div class="lyrico-lyrics-wrapper">Visuge Vadhilesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visuge Vadhilesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Gunde Chappudu Dharuvesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Gunde Chappudu Dharuvesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kanti Reppala Thera Theesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kanti Reppala Thera Theesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalane Vippesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalane Vippesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakasari Nuvve Gonthu Vippi Padu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakasari Nuvve Gonthu Vippi Padu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Neetho Padaidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Neetho Padaidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakasari Nuvve Steppu Vesi Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakasari Nuvve Steppu Vesi Chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Neetho Step Yaidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Neetho Step Yaidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhinattu Padai Nachinattu Adai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhinattu Padai Nachinattu Adai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Apedhevarinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Apedhevarinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Chinna Lifeye Korukundhi Hifi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Chinna Lifeye Korukundhi Hifi"/>
</div>
<div class="lyrico-lyrics-wrapper">Echedhamle Pada Enka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echedhamle Pada Enka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Tablene Cheripesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Tablene Cheripesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Planning Annadhe Marichesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Planning Annadhe Marichesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Calenderne Chimpesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Calenderne Chimpesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiyaranne Apesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyaranne Apesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupaki Jeans T-Shirt Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupaki Jeans T-Shirt Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumuki Teeyani Swarame Poosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumuki Teeyani Swarame Poosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalathala Tharka Neelake Vasthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalathala Tharka Neelake Vasthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthe Ee Pilla Roo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthe Ee Pilla Roo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimasham Ninda Santhoshale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimasham Ninda Santhoshale"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayam Ninda Vullassale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayam Ninda Vullassale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasurakese Vuchahale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasurakese Vuchahale"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundeloo Nimpindhiroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundeloo Nimpindhiroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Freedom Entha Kothaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freedom Entha Kothaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam Entha Hayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Entha Hayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragamintha Teeyag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragamintha Teeyag"/>
</div>
<div class="lyrico-lyrics-wrapper">Champuthuu Vundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champuthuu Vundagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Shaname Maruvanuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Shaname Maruvanuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Memory Ga Gundelo Dhaachana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Memory Ga Gundelo Dhaachana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakasari Nuvve  Cheyicheyi Kalupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakasari Nuvve  Cheyicheyi Kalupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Chelime Chesindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Chelime Chesindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakasari Nuvve Kallu Kallu Kalupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakasari Nuvve Kallu Kallu Kalupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Kalale Pancheyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Kalale Pancheyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Chinna Smileye Nuvvu Evvu Challe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Chinna Smileye Nuvvu Evvu Challe"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Eka Neee Venakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Eka Neee Venakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Chinna Lifeye Korukunna Lifeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Chinna Lifeye Korukunna Lifeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifeki Life Echedhamle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifeki Life Echedhamle"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellphone Teesi Pakkana Pettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellphone Teesi Pakkana Pettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendstho Kastha Timeye Gadipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendstho Kastha Timeye Gadipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Happinesski Meaning Manamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happinesski Meaning Manamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Chedhamuroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Chedhamuroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalo Chindhulu Chukkalu Thaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalo Chindhulu Chukkalu Thaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalo Allari Ashalu Regi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalo Allari Ashalu Regi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalo Kalale Reppallu Dati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalo Kalale Reppallu Dati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijame Avvaliroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijame Avvaliroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter Entha Kothaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter Entha Kothaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Meter Entha Hayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meter Entha Hayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Quater Vesinattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quater Vesinattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Roottune Marchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roottune Marchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Rooju Weekende
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Rooju Weekende"/>
</div>
<div class="lyrico-lyrics-wrapper">Neverende Feelne Panchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neverende Feelne Panchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakasari Nuvve Mundhadugesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakasari Nuvve Mundhadugesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Neetho Nadicheydha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Neetho Nadicheydha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakasari Nuvve Prematho Pilichey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakasari Nuvve Prematho Pilichey"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Welcome Cheppaidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Welcome Cheppaidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakka Chinna Helpye Nuvvvu Cheye Challe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakka Chinna Helpye Nuvvvu Cheye Challe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Dhigadha Selfile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Dhigadha Selfile"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakka Chinna Lifeye  Love Cheyyi Dhanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakka Chinna Lifeye  Love Cheyyi Dhanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifye Ninnu Love Chesaidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifye Ninnu Love Chesaidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Tablene Cheripesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Tablene Cheripesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Planning Annadhe Marichesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Planning Annadhe Marichesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Calenderne Chimpesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Calenderne Chimpesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooaha Hooaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooaha Hooaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiyaranne Apesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyaranne Apesai"/>
</div>
</pre>
