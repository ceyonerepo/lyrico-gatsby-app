---
title: "naa gundello song lyrics"
album: "Majili"
artist: "Gopi Sunder"
lyricist: "Rambabu Gosala"
director: "Shiva Nirvana"
path: "/albums/majili-lyrics"
song: "Naa Gundello"
image: ../../images/albumart/majili.jpg
date: 2019-04-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mXd32LxPvvc"
type: "happy"
singers:
  - Yazin Nizar
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa gundello undundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundello undundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamgaa jhallumande emayyindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamgaa jhallumande emayyindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oohallo nuvvocchi vaalamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oohallo nuvvocchi vaalamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamgundi emavutundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamgundi emavutundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Madilo medile maatalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madilo medile maatalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedave daachanande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedave daachanande"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedalo egase alajadine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedalo egase alajadine"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagaali mana kathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaali mana kathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O naa gundello undundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O naa gundello undundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamgaa jhallumande emayyindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamgaa jhallumande emayyindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela andinde aakaasam andesinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela andinde aakaasam andesinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela aanandam ponginde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela aanandam ponginde"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela allinde ullaasam allesinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela allinde ullaasam allesinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela vollanta tullinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela vollanta tullinde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inchu minchugaa oopiri aagettundile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchu minchugaa oopiri aagettundile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve choosi choodanattu vellake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve choosi choodanattu vellake"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham konchamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham konchamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maunam karigettundile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maunam karigettundile"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve mantram vesi manase laagite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve mantram vesi manase laagite"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana maate paatagaa maarani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana maate paatagaa maarani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana paate premagaa saaganee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana paate premagaa saaganee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa preme swapnamai satyamai swargamaiponee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa preme swapnamai satyamai swargamaiponee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana kalayikalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana kalayikalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa gundello undundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundello undundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamgaa jhallumande emayyindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamgaa jhallumande emayyindo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manchu puvvanti chinni navvu navveste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchu puvvanti chinni navvu navveste"/>
</div>
<div class="lyrico-lyrics-wrapper">Pancha praanaalanni malli puttele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pancha praanaalanni malli puttele"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchadaaranti teepi oosulaadeste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchadaaranti teepi oosulaadeste"/>
</div>
<div class="lyrico-lyrics-wrapper">Laksha nimishaalainaa itte gadichele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laksha nimishaalainaa itte gadichele"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandramaina chitikelo daatanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandramaina chitikelo daatanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandepoddu jilugulo cherenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandepoddu jilugulo cherenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuram madhuram madhuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuram madhuram madhuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana ee premam sumadhura kaavyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana ee premam sumadhura kaavyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa gundello undundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gundello undundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellamgaa jhallumande emayyindo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellamgaa jhallumande emayyindo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oohallo nuvvocchi vaalamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oohallo nuvvocchi vaalamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamgundi emavutundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamgundi emavutundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Madilo medile maatalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madilo medile maatalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedave daachanande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedave daachanande"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedalo egase alajadine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedalo egase alajadine"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagaali mana kathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaali mana kathe"/>
</div>
</pre>
