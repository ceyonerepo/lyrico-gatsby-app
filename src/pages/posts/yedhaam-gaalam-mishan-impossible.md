---
title: "yedhaam gaalam song lyrics"
album: "Mishan Impossible"
artist: "Mark k Robin"
lyricist: "Hasith Goli"
director: "Swaroop RSJ"
path: "/albums/mishan-impossible-lyrics"
song: "Yedhaam Gaalam"
image: ../../images/albumart/mishan-impossible.jpg
date: 2022-04-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MY_VqWUC4_c"
type: "happy"
singers:
  - Hemachandra Vedala
  - Sreerama Chandra
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedhaam gaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaam gaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seseydhaam gandharagolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seseydhaam gandharagolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Leyseylogaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leyseylogaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeseydhaam raa oorini veylam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeseydhaam raa oorini veylam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelaakolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaakolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi kaadhoyy thingarimelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi kaadhoyy thingarimelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee bhoogolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee bhoogolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammeysainaa theedhaam thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammeysainaa theedhaam thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothaame balai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothaame balai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa plaaney vineyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa plaaney vineyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A, badinammedhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A, badinammedhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">B, seruvappidhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B, seruvappidhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">C, baruvekkinchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="C, baruvekkinchey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kathaseppeydhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kathaseppeydhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vintuu padhi mandhanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintuu padhi mandhanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandho veyyichaaka,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandho veyyichaaka,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hundee nindindhantuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hundee nindindhantuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumpai parigetheydham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumpai parigetheydham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhaam gaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaam gaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seseydhaam gandharagolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seseydhaam gandharagolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetaadainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetaadainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Patteydhaam raa bangaru naanem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patteydhaam raa bangaru naanem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sedhaam beram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhaam beram"/>
</div>
<div class="lyrico-lyrics-wrapper">Geeseydhaam oh aakaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geeseydhaam oh aakaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Roadeydhainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadeydhainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Isthaarey thalakoka manihaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthaarey thalakoka manihaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chillaavvalantey chillaratho pani kaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillaavvalantey chillaratho pani kaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohlley gullainaa rojanthaa thiragaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohlley gullainaa rojanthaa thiragaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaparaalanee hit ayye luck unteyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaparaalanee hit ayye luck unteyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooranthaa manakey dabbichey chuttaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooranthaa manakey dabbichey chuttaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tookigaa vineyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tookigaa vineyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa plaaney koneyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa plaaney koneyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhaam gaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaam gaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seseydhaam gandharagolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seseydhaam gandharagolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moose maani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moose maani"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaley rojuko puli yesham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaley rojuko puli yesham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeyley yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeyley yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorinchey manalo veygam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorinchey manalo veygam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohsaarainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohsaarainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooseylaa seydham eelokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooseylaa seydham eelokam"/>
</div>
</pre>
