---
title: "rasavaachiye song lyrics"
album: "Aranmanai 3"
artist: "C. Sathya"
lyricist: "Mohan Rajan"
director: "Sundar C"
path: "/albums/aranmanai-3-lyrics"
song: "Rasavaachiye"
image: ../../images/albumart/aranmanai-3.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UT3_k2I9qDU"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye Rasa vaaccchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye Rasa vaaccchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un parvaiala ponen koosiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un parvaiala ponen koosiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi saacchiye vizhi saacchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi saacchiye vizhi saacchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesum podhu aawen thoosiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesum podhu aawen thoosiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavada sattayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavada sattayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatha neram yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatha neram yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal aada paala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal aada paala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhumbuwen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhumbuwen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kadicha mitai vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadicha mitai vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan rusicha kaalam yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan rusicha kaalam yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram innum kuda nenaikuren di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram innum kuda nenaikuren di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi saacchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi saacchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaana nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaana nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkapadum un kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkapadum un kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha neram ne potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha neram ne potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un valayal alli serthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un valayal alli serthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhenam kaalaara un kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenam kaalaara un kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu vara nenapeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu vara nenapeney"/>
</div>
<div class="lyrico-lyrics-wrapper">Appoyellam un nezhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appoyellam un nezhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen nezhala thotu pappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen nezhala thotu pappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru ribban pola dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ribban pola dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi kedakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi kedakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mela naney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mela naney"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paaru adhu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paaru adhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhuwen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaazhuwen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye Rasa vaaccchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye Rasa vaaccchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi saacchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi saacchiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavada sattayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavada sattayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paatha neram yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paatha neram yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal aada paala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal aada paala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhumbuwen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhumbuwen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kadicha mitai vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadicha mitai vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan rusicha kaalam yellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan rusicha kaalam yellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram innum kuda nenaikuren di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram innum kuda nenaikuren di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasa vaachchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasa vaachchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi saacchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi saacchiye"/>
</div>
</pre>
