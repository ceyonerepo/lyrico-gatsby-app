---
title: "ohh my love song lyrics"
album: "Chanakya"
artist: "Sricharan Pakala"
lyricist: Ramajogayya Sastry"
director: "Thiru"
path: "/albums/chanakya-lyrics"
song: "Ohh My Love"
image: ../../images/albumart/chanakya.jpg
date: 2019-10-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3DcyYdK1fro"
type: "love"
singers:
  - Chinmayi Sripada
  - Poojan Kohli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ohh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundello Swasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Swasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapallo Aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapallo Aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chalu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chalu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathinityam Paadaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathinityam Paadaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavanchullo Paatagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavanchullo Paatagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chalu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chalu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Allukupoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Allukupoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Allarigaalai Nene Unnaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allarigaalai Nene Unnaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oopirikey Ney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oopirikey Ney"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirinavthaaney Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirinavthaaney Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundello Swasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Swasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapallo Aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapallo Aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chalu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chalu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorinche Ye Swargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorinche Ye Swargam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakem Vaddhu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakem Vaddhu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohallo Nuvvuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohallo Nuvvuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Chaalu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Chaalu Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandham Anteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham Anteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham Nuvvu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham Nuvvu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannaa Neykorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannaa Neykorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamedhi Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamedhi Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janma Needhey Cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janma Needhey Cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Needhey Cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Needhey Cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pancha Praanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pancha Praanala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakshigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakshigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundello Swasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Swasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapallo Aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapallo Aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chalu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chalu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundello Swasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Swasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapallo Aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapallo Aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Chalu Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Chalu Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh My Love"/>
</div>
</pre>
