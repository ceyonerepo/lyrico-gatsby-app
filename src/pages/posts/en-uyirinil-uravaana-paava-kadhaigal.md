---
title: "en uyirinil uruvaana song lyrics"
album: "Paava Kadhaigal"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "En Uyirinil Uruvaana"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UuwCfaQhcJw"
type: "sad"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Uyirilae Uruvaana Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirilae Uruvaana Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Uyirinai Naan Eduththaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Uyirinai Naan Eduththaena"/>
</div>
<div class="lyrico-lyrics-wrapper">En Magalaena Piranthaayae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Magalaena Piranthaayae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pizhai Athu Ondruthaano Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pizhai Athu Ondruthaano Uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraathu Aaraathu Aaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraathu Aaraathu Aaraathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaayamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ponaalum Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ponaalum Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathu En Bhaaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathu En Bhaaramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Uttraarin Thoonduthal Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Uttraarin Thoonduthal Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pirivaalae Purithathu Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pirivaalae Purithathu Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmale Kovam Mudinthathu Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmale Kovam Mudinthathu Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pidivatham Thalarnththu Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pidivatham Thalarnththu Magalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Seitha Thavarinai Manniththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Seitha Thavarinai Manniththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthidu Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthidu Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Seitha Thavarinai Manniththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Seitha Thavarinai Manniththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthidu Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthidu Magalae"/>
</div>
</pre>
