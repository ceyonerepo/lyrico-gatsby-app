---
title: "vadi vadiga song lyrics"
album: "Induvadana"
artist: "Shiva Kakani"
lyricist: "Tirupathi Jaavana"
director: "MSR"
path: "/albums/induvadana-lyrics"
song: "Vadi Vadiga"
image: ../../images/albumart/induvadana.jpg
date: 2022-01-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dxF8GuhVLkg"
type: "love"
singers:
  - Javed Ali
  - Malavika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vadi vadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi vadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudi galiga vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudi galiga vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchi guchi chusthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchi guchi chusthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhale bhale ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhale bhale ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sari tsunaami laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sari tsunaami laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttesaavu Hadavidigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttesaavu Hadavidigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Osina guvvalaa chenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osina guvvalaa chenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodipadda vennelaa vaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodipadda vennelaa vaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodukunna thiyyanee thenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodukunna thiyyanee thenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanane thandhaane thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanane thandhaane thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukonaa muvvalaa gunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukonaa muvvalaa gunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Theluthunna thellani mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theluthunna thellani mainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakathai allaredhainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakathai allaredhainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkincheseinaa menaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkincheseinaa menaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadi vadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi vadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudi galiga vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudi galiga vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchi guchi chusthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchi guchi chusthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhale bhale ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhale bhale ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okka choopuni chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka choopuni chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nellallo thosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nellallo thosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne munchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne munchaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chethitho thaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chethitho thaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothaga malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothaga malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri posavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri posavee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padha padha padhamandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha padha padhamandhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venuke naa hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venuke naa hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhi mandhi edhurainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhi mandhi edhurainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone naa payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone naa payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam ayyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam ayyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa nimushamlo nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa nimushamlo nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham kadhilindhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham kadhilindhee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vente aagave aagave aagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vente aagave aagave aagave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padi padi poyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi padi poyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo pilla ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo pilla ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pancha pranaalistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pancha pranaalistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke pogesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke pogesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pedhavulu thaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pedhavulu thaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenela thepi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenela thepi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa lokam dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa lokam dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee lokaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee lokaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisuku vachindhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisuku vachindhee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mari mari marichedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari mari marichedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhasalu ee samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhasalu ee samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi nanne vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi nanne vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chere"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nimusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nimusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu chusthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu chusthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipisthavu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipisthavu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellipomakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellipomakey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi chudave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi chudave "/>
</div>
<div class="lyrico-lyrics-wrapper">chudave chudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chudave chudave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadi vadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi vadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudi galiga vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudi galiga vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchi guchi chusthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchi guchi chusthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhale bhale ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhale bhale ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sara raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sara raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara sari tsunaami laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sari tsunaami laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttesaavu Hadavidigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttesaavu Hadavidigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Osina guvvalaa chenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osina guvvalaa chenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodipadda vennelaa vaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodipadda vennelaa vaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodukunna thiyyanee thenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodukunna thiyyanee thenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanane thandhaane thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanane thandhaane thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattukunte muvva nenainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukunte muvva nenainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunuthunna thellani mainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunuthunna thellani mainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakathai allari edhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakathai allari edhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkestha nenu menaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkestha nenu menaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadi vadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadi vadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudi galiga vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudi galiga vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchi guchi chusthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchi guchi chusthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhale bhale ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhale bhale ga"/>
</div>
</pre>
