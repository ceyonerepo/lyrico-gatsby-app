---
title: "nenjukulle oru raagam song lyrics"
album: "Saayam"
artist: "Nagha Udhayan"
lyricist: "Malaisamy"
director: "Antony Samy"
path: "/albums/saayam-lyrics"
song: "Nenjukulle Oru Raagam"
image: ../../images/albumart/saayam.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uMtsiTEZ724"
type: "love"
singers:
  - Madhu Balakrishnan
  - Swetha Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nenjukkule puthu raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukkule puthu raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham nitham athu ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham nitham athu ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal solla vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal solla vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvai solli thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvai solli thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai konji konji 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai konji konji "/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kolvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kolvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veetukulle puthu vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetukulle puthu vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">veethi engum vanthu veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veethi engum vanthu veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai uyiril serthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai uyiril serthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnil ennai korthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil ennai korthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai konji konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai konji konji"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kolvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kolvaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">netru nee veru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netru nee veru "/>
</div>
<div class="lyrico-lyrics-wrapper">naan veru endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan veru endru"/>
</div>
<div class="lyrico-lyrics-wrapper">iruntha uravu allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruntha uravu allava"/>
</div>
<div class="lyrico-lyrics-wrapper">indru nee pathi naan pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru nee pathi naan pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">endru endrum vaalvomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru endrum vaalvomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidive illatha iravai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidive illatha iravai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nalum maraipomadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalum maraipomadi"/>
</div>
<div class="lyrico-lyrics-wrapper">janma janmangal aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="janma janmangal aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna jeevan nee thanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna jeevan nee thanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennaye enaku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaye enaku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">arimugam seikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arimugam seikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai vali piranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai vali piranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthai madi valarnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthai madi valarnthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un madi saaivenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madi saaivenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjukkule puthu raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukkule puthu raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham nitham athu ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham nitham athu ketkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iruthi varaikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruthi varaikum "/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaala thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaala thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai thanen adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thanen adi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanneer enai vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneer enai vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">karaikindra neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaikindra neram"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai madi aanayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai madi aanayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaaimai endru oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaimai endru oru"/>
</div>
<div class="lyrico-lyrics-wrapper">nilaiyai koduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilaiyai koduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai uyarvaakinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai uyarvaakinai"/>
</div>
<div class="lyrico-lyrics-wrapper">pirantha payanai nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirantha payanai nan"/>
</div>
<div class="lyrico-lyrics-wrapper">indru adainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru adainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku varamaaginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku varamaaginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavilum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavilum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivilum thaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivilum thaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">man meethu nadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man meethu nadanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">vin meethu paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vin meethu paranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un kootil sernthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kootil sernthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjukkule oru raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukkule oru raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham nitham athu ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham nitham athu ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal solla vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal solla vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvai solli thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvai solli thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai konji konji 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai konji konji "/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kolvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kolvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjukkule puthu raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukkule puthu raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham nitham athu ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham nitham athu ketkum"/>
</div>
</pre>
