---
title: "raththa vettai song lyrics"
album: "Lisaa"
artist: "Santhosh Dhayanidhi"
lyricist: "Mani Amuthavan"
director: "Raju Viswanath"
path: "/albums/lisaa-lyrics"
song: "Raththa Vettai"
image: ../../images/albumart/lisaa.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Lbv4OMSqTlU"
type: "mass"
singers:
  - Deepthi Suresh
  - Ala B Bala
  - Veena Murali
  - Sowmya Ramani Mahadevan
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raththa Raththa Vaettai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa Raththa Vaettai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai Ondru Aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaengai Ondru Aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Ketta Nandri Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Ketta Nandri Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai Kondru Podudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai Kondru Podudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natta Natta Paavam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Natta Paavam Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanju Kondu Thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanju Kondu Thaakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta Vitta Saabam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Vitta Saabam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennjinaththai Kaathudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennjinaththai Kaathudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Haaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Haaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raththa Raththa Vaettai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa Raththa Vaettai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai Ondru Aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaengai Ondru Aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Ketta Nandri Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Ketta Nandri Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai Kondru Podudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai Kondru Podudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta Natta Paavam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Natta Paavam Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanju Kondu Thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanju Kondu Thaakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta Vitta Saabam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Vitta Saabam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennjinaththai Kaathudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennjinaththai Kaathudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Ooo Aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo Aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raththa Raththa Vaettai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa Raththa Vaettai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai Ondru Aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaengai Ondru Aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Ketta Nandri Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Ketta Nandri Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai Kondru Podudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai Kondru Podudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta Natta Paavam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Natta Paavam Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanju Kondu Thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanju Kondu Thaakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta Vitta Saabam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Vitta Saabam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennjinaththai Kaathudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennjinaththai Kaathudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadatheevegajala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadatheevegajala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravaagapaavidhasthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravaagapaavidhasthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalivalayambaya Lambhithaampadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalivalayambaya Lambhithaampadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhunjanga Thungamaaligam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhunjanga Thungamaaligam"/>
</div>
<div class="lyrico-lyrics-wrapper">Damat Damat Dama Dama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damat Damat Dama Dama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninathavadamaruvayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninathavadamaruvayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkara Sandhathaandavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkara Sandhathaandavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanothuna Sivammmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanothuna Sivammmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivam Mmsivam Mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivam Mmsivam Mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavaru Purindha Thalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaru Purindha Thalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarindhu Tharaiyil Uruludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarindhu Tharaiyil Uruludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumam Thirumbi Adikkum Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumam Thirumbi Adikkum Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Meraludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Meraludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavaru Purindha Thalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaru Purindha Thalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarindhu Tharaiyil Uruludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarindhu Tharaiyil Uruludhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharumam Thirumbi Adikkum Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharumam Thirumbi Adikkum Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Meraludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Meraludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urakkam Izhantha Uyirin Valigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakkam Izhantha Uyirin Valigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvam Edukkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam Edukkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkam Izhantha Idhayam Pizhanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkam Izhantha Idhayam Pizhanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram Therikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram Therikkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Annaiyum Thanthaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyum Thanthaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiril Irunthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiril Irunthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulai Kallilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai Kallilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum Ulagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum Ulagamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Karuvilae Sumandhaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvilae Sumandhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Kalanginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Kalanginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavinai Thirandhu Unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavinai Thirandhu Unai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragam Azhaikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragam Azhaikkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raththa Raththa Vaettai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa Raththa Vaettai Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaengai Ondru Aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaengai Ondru Aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta Ketta Nandri Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Ketta Nandri Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai Kondru Podudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai Kondru Podudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta Natta Paavam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta Natta Paavam Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanju Kondu Thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanju Kondu Thaakuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta Vitta Saabam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Vitta Saabam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennjinaththai Kaathudhaea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennjinaththai Kaathudhaea"/>
</div>
</pre>
