---
title: "minnala song lyrics"
album: "Veppam"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Anjana"
path: "/albums/veppam-lyrics"
song: "Minnala"
image: ../../images/albumart/veppam.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eaNcNQJfaJI"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Minnala Pudikira Vayasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala Pudikira Vayasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maegathil Parakira Manasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maegathil Parakira Manasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala Mala Mala Mala Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala Mala Mala Mala Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarathil Yeri Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarathil Yeri Vaa Da Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Asayaai Jeyikira Vayasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asayaai Jeyikira Vayasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavam Pazhagura Manasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavam Pazhagura Manasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Alai Alai Ena Anu Dhinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alai Alai Ena Anu Dhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Seeri Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottamal Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottamal Eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Kootu Thaeni Irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Kootu Thaeni Irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Katterumbum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katterumbum Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vechu Paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vechu Paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaamal Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamal Eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nenja Thooki Nadanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenja Thooki Nadanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Vaazhkai Undhan Kaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vaazhkai Undhan Kaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnala Pudikira Vayasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala Pudikira Vayasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maegathil Parakira Manasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maegathil Parakira Manasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala Mala Mala Mala Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala Mala Mala Mala Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarathil Yeri Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarathil Yeri Vaa Da Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Asayaai Jeyikira Vayasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asayaai Jeyikira Vayasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavam Pazhagura Manasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavam Pazhagura Manasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Alai Alai Ena Anu Dhinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alai Alai Ena Anu Dhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Seeri Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarnthida Nadhi Maruthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarnthida Nadhi Maruthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalinai Thoda Vazhi Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalinai Thoda Vazhi Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthida Ini Thadai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthida Ini Thadai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Vaa Da Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedandhida Nee Kal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedandhida Nee Kal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthida Nee Pul Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthida Nee Pul Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavida Oru Sol Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavida Oru Sol Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Vaa Da Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaelvi Kaetka Yaarum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaelvi Kaetka Yaarum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Puvii Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Puvii Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Sogam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Sogam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila Kaasu Ilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila Kaasu Ilaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaama Siripoam Vellaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama Siripoam Vellaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkayil Ethuvum Thaevaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkayil Ethuvum Thaevaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Nimmathi Pothum Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nimmathi Pothum Ada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnala Pudikira Vayasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala Pudikira Vayasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maegathil Parakira Manasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maegathil Parakira Manasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala Mala Mala Mala Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala Mala Mala Mala Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarathil Yeri Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarathil Yeri Vaa Da Vaa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Asayaai Jeyikira Vayasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asayaai Jeyikira Vayasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavam Pazhagura Manasidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavam Pazhagura Manasidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Alai Alai Ena Anu Dhinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alai Alai Ena Anu Dhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Seeri Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parunthugal Adhu Enaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parunthugal Adhu Enaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallathil Sendru Vaazhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallathil Sendru Vaazhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal Pudhu Malai Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Pudhu Malai Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa Da Vaa Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa Da Vaa Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaigalil Varum Nuraiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalil Varum Nuraiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandugal Adhu Karainthodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandugal Adhu Karainthodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhuthugal Adhu Puthayalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthugal Adhu Puthayalthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa Da Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa Da Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Thoalin Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thoalin Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Pogum Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Pogum Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Thooram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Thooram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Edhuvum Namakkilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Edhuvum Namakkilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaalum Athukku Thavikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaalum Athukku Thavikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkayil Ethuvum Thaeva Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkayil Ethuvum Thaeva Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Nimmathi Pothum Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nimmathi Pothum Ada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Da"/>
</div>
</pre>
