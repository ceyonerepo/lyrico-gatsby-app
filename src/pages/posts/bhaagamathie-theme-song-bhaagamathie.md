---
title: "bhaagamathie theme song song lyrics"
album: "Bhaagamathie"
artist: "S Thaman"
lyricist: "Suchitra"
director: "G Ashok"
path: "/albums/bhaagamathie-lyrics"
song: "Bhaagamathie Theme Song"
image: ../../images/albumart/bhaagamathie-telugu.jpg
date: 2018-01-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fsJbu-B8dFs"
type: "title track"
singers:
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">What Your Eyes See 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Your Eyes See "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Begin The Game 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Begin The Game "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">What Your Eyes See 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Your Eyes See "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">War That Is The Name 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="War That Is The Name "/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyway You Turn 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyway You Turn "/>
</div>
<div class="lyrico-lyrics-wrapper">Around The Freaking 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Around The Freaking "/>
</div>
<div class="lyrico-lyrics-wrapper">Freaking Dangerous
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freaking Dangerous"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t You Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t You Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">The Buddy Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Buddy Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Every Once Explain That
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every Once Explain That"/>
</div>
<div class="lyrico-lyrics-wrapper">Kicking Kicking Ass Ass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kicking Kicking Ass Ass"/>
</div>
<div class="lyrico-lyrics-wrapper">Move It With The Sass Sass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It With The Sass Sass"/>
</div>
<div class="lyrico-lyrics-wrapper">Move Your World
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move Your World"/>
</div>
<div class="lyrico-lyrics-wrapper">Move It Around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It Around"/>
</div>
<div class="lyrico-lyrics-wrapper">Move It Get It Down 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move It Get It Down "/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaagamathie Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie Bhaagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagala Mukhi Raa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagala Mukhi Raa "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagala Mukhi Raa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagala Mukhi Raa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Say Ma Ma Ma Ma Maa Maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say Ma Ma Ma Ma Maa Maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga Ga Gaa Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga Ga Gaa Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ba Ba Ba Ba Bha Bha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ba Ba Ba Ba Bha Bha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagmathi Mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagmathi Mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">What Your Eyes See 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Your Eyes See "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Begin The Game 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Begin The Game "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">What Your Eyes See 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What Your Eyes See "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust "/>
</div>
<div class="lyrico-lyrics-wrapper">Your Own Heart Beat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Own Heart Beat "/>
</div>
<div class="lyrico-lyrics-wrapper">You Cannot Trust Anybody 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Trust Anybody "/>
</div>
<div class="lyrico-lyrics-wrapper">War That Is The Name
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="War That Is The Name"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaaga Bhaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaga Bhaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaaga Bhaaga Bhaagamathie 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaga Bhaaga Bhaagamathie "/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khanditha Sthambitha Bhagalamukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khanditha Sthambitha Bhagalamukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaagamathie Bhaagamathie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaagamathie Bhaagamathie"/>
</div>
</pre>
