---
title: "kabaddi anthem song lyrics"
album: "Seetimaarr"
artist: "Mani Sharma"
lyricist: "Kalyan Chakravarthy"
director: "Sampath Nandi"
path: "/albums/seetimaarr-lyrics"
song: "Kabaddi Anthem"
image: ../../images/albumart/seetimaarr.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/WSWIByyMYq8"
type: "motivational"
singers:
  - Anurag Kulkarni
  - Sai Charan
  - Ramya Behara
  - Sahiti Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nuvva nenaa nenaa nuvvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvva nenaa nenaa nuvvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaa naadhaa naadhaa needa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaa naadhaa naadhaa needa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela meeda rangulanni cherinavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela meeda rangulanni cherinavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethaa pattukoni matti thalli nammakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethaa pattukoni matti thalli nammakame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geetha dhaate seethama thallulera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geetha dhaate seethama thallulera"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranthaa kottha raathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranthaa kottha raathaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Courtulona shivangi angalera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Courtulona shivangi angalera"/>
</div>
<div class="lyrico-lyrics-wrapper">Poradutharu aata kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradutharu aata kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veta antu veeru saagipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veta antu veeru saagipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Potaa poti up ap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potaa poti up ap"/>
</div>
<div class="lyrico-lyrics-wrapper">Telangana punjab kusthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telangana punjab kusthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dharti se kehne do hojaye jeet hamara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharti se kehne do hojaye jeet hamara"/>
</div>
<div class="lyrico-lyrics-wrapper">Janta ko samjha do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta ko samjha do"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann jeet se beet na hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann jeet se beet na hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen latchiyam yen payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen latchiyam yen payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaame vettri thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame vettri thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongisnam ongalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongisnam ongalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari gelisina rangamlone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari gelisina rangamlone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chelaregi pothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelaregi pothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavanchuna vijayam mogelagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavanchuna vijayam mogelagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maidhaname edhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maidhaname edhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelavaali antu unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelavaali antu unnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvva nenaa nenaa nuvvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvva nenaa nenaa nuvvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaa naadhaa naadhaa needa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaa naadhaa naadhaa needa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela meeda rangulanni cherinavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela meeda rangulanni cherinavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethaa pattukoni matti thalli nammakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethaa pattukoni matti thalli nammakame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geetha dhate sethamma thallulera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geetha dhate sethamma thallulera"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeranthaa kettha raathaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeranthaa kettha raathaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Courtulona shivangi angaleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Courtulona shivangi angaleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraduthaaru aata kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraduthaaru aata kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veta antu veru saagipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veta antu veru saagipora"/>
</div>
</pre>
