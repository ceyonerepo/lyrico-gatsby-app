---
title: "cheppake cheppake song lyrics"
album: "Maha Samudram"
artist: "Chaitan Bharadwaj"
lyricist: "Chaitanya Prasad"
director: "Ajay Bhupathi"
path: "/albums/maha-samudram-lyrics"
song: "Cheppake Cheppake"
image: ../../images/albumart/maha-samudram.jpg
date: 2021-10-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/RYjsDrNmFrM"
type: "love"
singers:
  - Deepthi Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheppake cheppake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppake cheppake"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosuponi maatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosuponi maatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaalule velakolam ooruko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaalule velakolam ooruko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerpake nerpake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpake nerpake"/>
</div>
<div class="lyrico-lyrics-wrapper">Leni poni aashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni poni aashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa malli raku vellipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa malli raku vellipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Egase kalale alalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egase kalale alalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhane munchesele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhane munchesele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhile kathalie kadalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile kathalie kadalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppenelle oopesele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppenelle oopesele"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhukee bandhalanni kalapakule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhukee bandhalanni kalapakule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilapakule gentesthanu gentesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilapakule gentesthanu gentesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnika ippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnika ippude"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa kanabadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa kanabadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuruga nilabadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuruga nilabadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Champesthanu champesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champesthanu champesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondhara pedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondhara pedithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Challanaina choopu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challanaina choopu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi gandhapu maata nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi gandhapu maata nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulla kanchelanni thunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla kanchelanni thunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola baatavayyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola baatavayyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Moyaleni haayi nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moyaleni haayi nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne marchina maaya nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne marchina maaya nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu nuvvu velthu unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu nuvvu velthu unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Venta needanayyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venta needanayyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesavi vedilo letha gaalai vachave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesavi vedilo letha gaalai vachave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamathe kurisi manase thadisele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamathe kurisi manase thadisele"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu naa jathaga untey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu naa jathaga untey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathika ne dhairyamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathika ne dhairyamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisen ippude ippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisen ippude ippude"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevithana maadhuryame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevithana maadhuryame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinthaga nanne nenu marachithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinthaga nanne nenu marachithene"/>
</div>
<div class="lyrico-lyrics-wrapper">Murisithine ninna leni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisithine ninna leni "/>
</div>
<div class="lyrico-lyrics-wrapper">monna leni vennela virise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monna leni vennela virise"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhikoka madhi dorikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhikoka madhi dorikey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathala katha mugise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathala katha mugise"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthe leni santhoshana kanthulu kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe leni santhoshana kanthulu kurise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu veru anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu veru anna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaipu asalu chuoodakanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaipu asalu chuoodakanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Donga laga kalle ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donga laga kalle ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongi thongi chusaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongi thongi chusaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paggam esi aaputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paggam esi aaputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme kaadhidhi swardhamanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme kaadhidhi swardhamanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigguleni kaalle nanne mugguloki thosaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigguleni kaalle nanne mugguloki thosaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa madhe midhi premanadhe ayindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa madhe midhi premanadhe ayindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhure marachi varadhai urakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhure marachi varadhai urakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tapane tapamai japamai nilicha neekosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tapane tapamai japamai nilicha neekosame"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadila musire kasire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadila musire kasire"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnapakalni tosesaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnapakalni tosesaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Premake roopam nuvvu Ani telise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premake roopam nuvvu Ani telise"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde theesi dhande chesi rammani piliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde theesi dhande chesi rammani piliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha idhi nilavadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha idhi nilavadhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu ika vadhaladu le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu ika vadhaladu le"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhala mahasandramaayanu manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhala mahasandramaayanu manase"/>
</div>
</pre>
