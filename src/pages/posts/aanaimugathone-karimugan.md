---
title: "aanaimugathone song lyrics"
album: "Karimugan"
artist: "Chella Thangaiah"
lyricist: "unknown"
director: "Chella Thangaiah"
path: "/albums/karimugan-song-lyrics"
song: "Aanaimugathone"
image: ../../images/albumart/karimugan.jpg
date: 2018-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dea5obSO1rI"
type: "devotional"
singers:
  - Senthil Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">munthi munthi vinayagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthi munthi vinayagane"/>
</div>
<div class="lyrico-lyrics-wrapper">mukkannanar than magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkannanar than magane"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarumaiyaa ganapathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarumaiyaa ganapathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu emmai kaarumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu emmai kaarumaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paanai vayithone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanai vayithone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">paanai vayithone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanai vayithone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">bagavane nayagane pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bagavane nayagane pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enga bagavane nayagane pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga bagavane nayagane pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aval pori kadalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval pori kadalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangi varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangi varava"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla arisi kolukataiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla arisi kolukataiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">senju tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senju tharava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aval pori kadalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval pori kadalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangi varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangi varava"/>
</div>
<div class="lyrico-lyrics-wrapper">arisi kolukataiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arisi kolukataiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">senju tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senju tharava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arasa marathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasa marathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">arasa marathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arasa marathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">aatchi kolla venumaiya pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatchi kolla venumaiya pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">engalai aatchi kolla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalai aatchi kolla "/>
</div>
<div class="lyrico-lyrics-wrapper">venumaiya pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venumaiya pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh ganesha maa ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh ganesha maa ganesha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa ganesha varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa ganesha varam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa ganesha"/>
</div>
<div class="lyrico-lyrics-wrapper">eh ganesha maa ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh ganesha maa ganesha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa ganesha varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa ganesha varam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa ganesha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paal panai pacharisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paal panai pacharisi"/>
</div>
<div class="lyrico-lyrics-wrapper">pongal idava unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongal idava unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">paalala abisegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paalala abisegam"/>
</div>
<div class="lyrico-lyrics-wrapper">senju vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senju vidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paal panai pacharisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paal panai pacharisi"/>
</div>
<div class="lyrico-lyrics-wrapper">pongal idava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongal idava"/>
</div>
<div class="lyrico-lyrics-wrapper">paalala abisegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paalala abisegam"/>
</div>
<div class="lyrico-lyrics-wrapper">senju vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senju vidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">velanukku moothavane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velanukku moothavane "/>
</div>
<div class="lyrico-lyrics-wrapper">pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">velanukku moothavane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velanukku moothavane "/>
</div>
<div class="lyrico-lyrics-wrapper">pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum varam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum varam "/>
</div>
<div class="lyrico-lyrics-wrapper">thanthuraiya pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthuraiya pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal vendum varam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal vendum varam "/>
</div>
<div class="lyrico-lyrics-wrapper">thanthuraiya pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthuraiya pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thengai pazham vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thengai pazham vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kumbuda vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumbuda vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu themangu paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu themangu paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">paadum thambiya paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadum thambiya paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan thengai pazham vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thengai pazham vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kumbuda vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumbuda vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu themangu paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu themangu paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">paadum thambiya paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadum thambiya paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moola param porule pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moola param porule pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">moola param porule pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moola param porule pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">munnetram thanthuraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnetram thanthuraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla munnetram thanthuraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla munnetram thanthuraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">aanai mugathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanai mugathone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">iynthu karathone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iynthu karathone pillaiyare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paanai vayithone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanai vayithone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">paanai vayithone pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanai vayithone pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">bagavane nayagane pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bagavane nayagane pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enga bagavane nayagane pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga bagavane nayagane pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">bagavane nayagane pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bagavane nayagane pillaiyare"/>
</div>
<div class="lyrico-lyrics-wrapper">enga bagavane nayagane pillaiyare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga bagavane nayagane pillaiyare"/>
</div>
</pre>
