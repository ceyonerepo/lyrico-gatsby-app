---
title: "en manasukulla song lyrics"
album: "Maniyaar Kudumbam"
artist: "Thambi Ramaiah"
lyricist: "Thambi Ramaiah"
director: "Thambi Ramaiah"
path: "/albums/maniyaar-kudumbam-lyrics"
song: "En Manasukulla"
image: ../../images/albumart/maniyaar-kudumbam.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8wIWapvJCkg"
type: "love"
singers:
  - D Imman
  - Surmukhi Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En manasukulla nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukulla nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugunthu kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugunthu kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">En usurukulla nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usurukulla nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothanjikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothanjikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyira kaiyiraaga nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira kaiyiraaga nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirichi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirichi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Arana kodi pola idupula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arana kodi pola idupula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna nee kalati erinji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna nee kalati erinji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan panni putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan panni putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee kasakki pulunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee kasakki pulunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Juice-ah kuduchu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juice-ah kuduchu putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manasukulla nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukulla nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugunthu kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugunthu kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">En usurukulla nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usurukulla nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothanjikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothanjikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyira kaiyiraaga nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira kaiyiraaga nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirichi putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirichi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Arana kodi pola idupula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arana kodi pola idupula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna nee kalati erinji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna nee kalati erinji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan panni putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan panni putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee kasakki pulunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee kasakki pulunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Juice-ah kuduchu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juice-ah kuduchu putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththaala maliyila thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaala maliyila thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuthadi aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuthadi aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththaalam poda solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaalam poda solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaduthadi kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaduthadi kuruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikalaaam thaluvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikalaaam thaluvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai mel vaaiya vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai mel vaaiya vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavu edukka vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavu edukka vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paai mela enna vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paai mela enna vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam solli thaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam solli thaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukken sooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukken sooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai thaalam pottu pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thaalam pottu pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai valichu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai valichu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei thaalam poda ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei thaalam poda ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram nerungiyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram nerungiyachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada neeyum naanum serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada neeyum naanum serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha paththu maasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha paththu maasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuva kuva kuva kuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuva kuva kuva kuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkaadu illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaadu illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerkaadu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerkaadu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu mootai aakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu mootai aakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thooki poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thooki poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthuguku sugamadi maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthuguku sugamadi maanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nokaadu illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokaadu illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookaadu ullapoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookaadu ullapoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam nadathurennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam nadathurennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonna varen naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonna varen naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikka thudikira thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikka thudikira thaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorai ellaam pokku kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai ellaam pokku kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Orangattalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orangattalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaikulla naamalum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikulla naamalum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Porai nadatha laama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porai nadatha laama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada neeyum naanum serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada neeyum naanum serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha paththu maasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha paththu maasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuva kuva kuva kuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuva kuva kuva kuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi panni puttiyae hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi panni puttiyae hei"/>
</div>
</pre>
