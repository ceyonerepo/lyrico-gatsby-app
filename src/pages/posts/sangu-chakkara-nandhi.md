---
title: "sangu chakkara song lyrics"
album: "Nandhi"
artist: "Bharathwaj"
lyricist: "Muthu Vijayan"
director: "Tamilvannan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Sangu Chakkara"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/m-7Z7puYNOQ"
type: "happy"
singers:
  - Chinna Ponnu
  - Rajamani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sanguchakkara saami vandhu jingu jingunnu aaduchaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanguchakkara saami vandhu jingu jingunnu aaduchaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pudichirundha aasaippeyi longu longunnu Oduchaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudichirundha aasaippeyi longu longunnu Oduchaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanguchakkara saami vandhu jingu jingunnu aaduchaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanguchakkara saami vandhu jingu jingunnu aaduchaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pudichirundha aasaippeyi longu longunnu Oduchaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudichirundha aasaippeyi longu longunnu Oduchaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu vikka sandhaikku poanaa en kannai velaikkettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu vikka sandhaikku poanaa en kannai velaikkettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thenuvikka kadaikku poanaa en udhatta velaikkettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenuvikka kadaikku poanaa en udhatta velaikkettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu vikka sandhaikku poanaa en kannai velaikkettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu vikka sandhaikku poanaa en kannai velaikkettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thenuvikka kadaikku poanaa en udhatta velaikkettaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenuvikka kadaikku poanaa en udhatta velaikkettaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna pannuven naan enna pannuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pannuven naan enna pannuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichukichaang Kichukichaang Kichukichaang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichukichaang Kichukichaang Kichukichaang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kittakkanda paiyan nenju pichukichaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kittakkanda paiyan nenju pichukichaam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichukichaang Kichukichaang Kichukichaang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichukichaang Kichukichaang Kichukichaang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kittakkanda paiyan nenju pichukichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kittakkanda paiyan nenju pichukichaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaa aamaa                                                                    
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa aamaa                                                                    "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irayileri nagaivaanga madhuraikku poanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irayileri nagaivaanga madhuraikku poanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nagaikkadaikkaaran thangamaannu ennai orasippaarthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagaikkadaikkaaran thangamaannu ennai orasippaarthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">AiyO appuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AiyO appuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bus yeri Paramakkudi poanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus yeri Paramakkudi poanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalar padamoannu ennappaarththu kandaktar sonnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalar padamoannu ennappaarththu kandaktar sonnaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Drivar enna sonnaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Drivar enna sonnaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan ennappannuven Naan ennappannuven ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennappannuven Naan ennappannuven ha ha ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah jingiri jingirichaan, jingiri jingirichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah jingiri jingirichaan, jingiri jingirichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jingiri jingidichaan yei jingiri jingirichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jingiri jingidichaan yei jingiri jingirichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siththaalu velaikki sivagangaippoanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siththaalu velaikki sivagangaippoanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru koththanaaru kalavaippoala ennaithaaney kalandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru koththanaaru kalavaippoala ennaithaaney kalandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye nerchi idichichaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye nerchi idichichaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedhiyil koalammaavu virkka oruthan vandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhiyil koalammaavu virkka oruthan vandhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada poattu vacha koalam poala ennaithaaney kalaichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poattu vacha koalam poala ennaithaaney kalaichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalainja koalamey kalakkalaa irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalainja koalamey kalakkalaa irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan ennappannuven naan ennappannuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennappannuven naan ennappannuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichukichaang Kichukichaang Kichukichaang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichukichaang Kichukichaang Kichukichaang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kittakkanda paiyan nenju pichukichaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kittakkanda paiyan nenju pichukichaam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichukichaang Kichukichaang Kichukichaang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichukichaang Kichukichaang Kichukichaang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna kittakkanda paiyan nenju pichukichaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kittakkanda paiyan nenju pichukichaam "/>
</div>
</pre>