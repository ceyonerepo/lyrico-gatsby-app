---
title: "sengathire song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Sengathire"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6iKpWqoXefk"
type: "sad"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaaa aaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengathirae sengathirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengathirae sengathirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thongiyathu yaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai thongiyathu yaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangadamo sanjalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangadamo sanjalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai yethividu kaalalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai yethividu kaalalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengathirae sengathirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengathirae sengathirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thongiyathu yaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai thongiyathu yaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangadamo sanjalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangadamo sanjalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai yethividu kaalalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai yethividu kaalalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir vedhanai tharum vaarthayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vedhanai tharum vaarthayai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravae nee pesuvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravae nee pesuvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyil veetaiyae kudai saithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyil veetaiyae kudai saithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal kaatru veesuvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal kaatru veesuvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyin aatam oyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyin aatam oyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum vilaiyattae vadaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum vilaiyattae vadaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengathirae sengathirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengathirae sengathirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thongiyathu yaaralaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai thongiyathu yaaralaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangadamo sanjalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangadamo sanjalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai yethividu kaalalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai yethividu kaalalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai madi meethu thoongayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai madi meethu thoongayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollaigalum yethadaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollaigalum yethadaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai naamai thaangum velayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai naamai thaangum velayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalilae vaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalilae vaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Theru mannodu naam nadanthaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru mannodu naam nadanthaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhukkillaamalae irunthomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukkillaamalae irunthomada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai kannadiyil siru keeral pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai kannadiyil siru keeral pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala thundaayindru udainthomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala thundaayindru udainthomada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayadhagum pothunaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhagum pothunaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maari pogiromaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maari pogiromaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengathirae sengathirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengathirae sengathirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thongiyathu yaaralaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai thongiyathu yaaralaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangadamo sanjalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangadamo sanjalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai yethividu kaalalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai yethividu kaalalae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottu vidum poovai kaatuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu vidum poovai kaatuvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppoluthum vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppoluthum vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullavarai vazha thevayethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullavarai vazha thevayethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmayilae paasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmayilae paasamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethai sonnalumae thavaragavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai sonnalumae thavaragavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Porul kolvoridam nalam yethada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porul kolvoridam nalam yethada"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravillaamalae oru jeevanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravillaamalae oru jeevanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vazhaathena unarvomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vazhaathena unarvomada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayalodu vaazha naamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayalodu vaazha naamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varappaga maruvomaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varappaga maruvomaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengathirae sengathirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengathirae sengathirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai thongiyathu yaaralaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai thongiyathu yaaralaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangadamo sanjalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangadamo sanjalamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai yethividu kaalalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai yethividu kaalalae ae"/>
</div>
</pre>
