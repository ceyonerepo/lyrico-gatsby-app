---
title: "poovin manam poovil ellai song lyrics"
album: "Narthagi"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "G. Vijayapadma"
path: "/albums/narthagi-lyrics"
song: "Poovin Manam Poovil Ellai"
image: ../../images/albumart/narthagi.jpg
date: 2011-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8oKbDs69JBU"
type: "happy"
singers:
  - Tippu
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin manam poovil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin manam poovil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthendralo thodavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthendralo thodavae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyin gunam theeyil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyin gunam theeyil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum viral sudavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum viral sudavae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin manam poovil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin manam poovil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthendralo thodavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthendralo thodavae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyin gunam theeyil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyin gunam theeyil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum viral sudavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum viral sudavae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo udal ennum padagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo udal ennum padagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneeril aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneeril aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru karaigal naduvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru karaigal naduvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhanthen alainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhanthen alainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Orupuramum malaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orupuramum malaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhaegam maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhaegam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupuramum theeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupuramum theeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagithen erinthen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagithen erinthen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhirumbodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirumbodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saram korkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saram korkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal modha manal serkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal modha manal serkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Idiyin naduvae isai ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiyin naduvae isai ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai naan thotru unai meetkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai naan thotru unai meetkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvathil pei aattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvathil pei aattangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalina thadumaatrangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalina thadumaatrangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaththai urumaatravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaththai urumaatravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaththai adhu maatrumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaththai adhu maatrumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho enakkullae aanmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho enakkullae aanmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee theda vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee theda vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkullae penmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkullae penmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan theda vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan theda vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaeppammarakkilaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaeppammarakkilaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver yedho chedithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver yedho chedithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarvadhai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarvadhai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyai unarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyai unarndhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin manam poovil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin manam poovil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthendralo thodavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthendralo thodavae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyin gunam theeyil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyin gunam theeyil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum viral sudavae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum viral sudavae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalin neerodaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalin neerodaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Meengalai nee ketkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meengalai nee ketkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal puriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal puriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhilgalai nee serkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhilgalai nee serkkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila neram vilakkin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila neram vilakkin"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal eerkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal eerkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakkukkum keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakkukkum keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttondru irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttondru irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulum oliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulum oliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhappathilae naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhappathilae naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaviththen thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaviththen thudithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey eeyyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eeyyy heyy"/>
</div>
</pre>
