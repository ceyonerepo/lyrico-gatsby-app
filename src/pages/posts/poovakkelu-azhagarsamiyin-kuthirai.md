---
title: "poovakkelu song lyrics"
album: "Azhagarsamiyin Kuthirai"
artist: "Ilaiyaraaja"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/azhagarsamiyin-kuthirai-lyrics"
song: "Poovakkelu"
image: ../../images/albumart/azhagarsamiyin-kuthirai.jpg
date: 2011-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kTAm3TltwrE"
type: "love"
singers:
  - Karthik
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maana kelu mayila kelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maana kelu mayila kelu "/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai katta sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai katta sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Therathathe aasai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therathathe aasai "/>
</div>
<div class="lyrico-lyrics-wrapper">verenna nan pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verenna nan pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu nee pathi illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu nee pathi illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illayen nanum bommaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illayen nanum bommaye"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan uyirum neye neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan uyirum neye neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Muthu velli muthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Muthu velli muthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan mudinchu vachen mundhanethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan mudinchu vachen mundhanethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thali katta nenthu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thali katta nenthu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thavichirunthen vazhiya paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thavichirunthen vazhiya paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum vazhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogum vazhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilal nanaagi vizhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilal nanaagi vizhavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongatha vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongatha vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">thunai sernthaaye medhuvaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai sernthaaye medhuvaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">thaazham thatturene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaazham thatturene"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla therriyaama vaaya katturene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla therriyaama vaaya katturene"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga motham kadhal idhu sarithaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga motham kadhal idhu sarithaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maana kelu mayila kelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maana kelu mayila kelu "/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai katta sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai katta sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aeridichu kadhal pithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeridichu kadhal pithu"/>
</div>
<div class="lyrico-lyrics-wrapper">sevanthupochu malli mottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevanthupochu malli mottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa romba muthipochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa romba muthipochu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi parpom jallikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi parpom jallikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaatha thanimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaatha thanimai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai yaarodu unnara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai yaarodu unnara"/>
</div>
<div class="lyrico-lyrics-wrapper">theendatha koduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendatha koduma"/>
</div>
<div class="lyrico-lyrics-wrapper">sudum theeyaagi padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudum theeyaagi padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhi enna sadhi thevayilla maane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhi enna sadhi thevayilla maane"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhaluku theva anbu mattum thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhaluku theva anbu mattum thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga motham kadhal idhu sarithaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga motham kadhal idhu sarithaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maana kelu mayila kelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maana kelu mayila kelu "/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai katta sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai katta sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therathathe aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therathathe aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">verenna nan pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verenna nan pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu nee pathi illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu nee pathi illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illayen nanum bommaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illayen nanum bommaye"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan uyirum neye neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan uyirum neye neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovakkelu Kathakkelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovakkelu Kathakkelu "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathi sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathi sollum"/>
</div>
</pre>
