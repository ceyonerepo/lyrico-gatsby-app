---
title: "kantha karuvizhiyo song lyrics"
album: "Vaanga Vaanga"
artist: "Rajesh Mohan"
lyricist: "unknown"
director: "NP Ismail"
path: "/albums/vaanga-vaanga-lyrics"
song: "Kantha Karuvizhiyo"
image: ../../images/albumart/vaanga-vaanga.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vN46JL5T6Rw"
type: "love"
singers:
  - Rajesh Mohan
  - Durga Vishwanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaantha karuvizhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaantha karuvizhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">aalai mayakiruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai mayakiruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennule aasai mulaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennule aasai mulaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan mogam kothikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan mogam kothikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalana ithayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalana ithayam "/>
</div>
<div class="lyrico-lyrics-wrapper">karaithodi poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaithodi poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">paarvai paartha neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvai paartha neram"/>
</div>
<div class="lyrico-lyrics-wrapper">muthal thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">kaantha karuvizhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaantha karuvizhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">aalai mayakiruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai mayakiruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagaana vathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagaana vathanam"/>
</div>
<div class="lyrico-lyrics-wrapper">athuketha paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuketha paruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">aalai kolgirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai kolgirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai aala ninaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai aala ninaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">soodana ithayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodana ithayam"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam thedum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam thedum tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">athu naala thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu naala thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum pithu aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum pithu aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">unai naanum kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai naanum kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">enai neeyum kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai neeyum kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ododi vantheno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ododi vantheno "/>
</div>
<div class="lyrico-lyrics-wrapper">indru ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru ingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaantha karuvizhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaantha karuvizhiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">aalai mayakiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai mayakiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennule aasai mulaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennule aasai mulaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan mogam kothikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan mogam kothikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbaana ithayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbaana ithayam"/>
</div>
<div class="lyrico-lyrics-wrapper">athai thedum paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai thedum paruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai koduthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai koduthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">unnil naanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil naanum "/>
</div>
<div class="lyrico-lyrics-wrapper">kalanthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanthidava"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu naadum paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu naadum paruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu thaane ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu thaane ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaala thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaala thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum nee aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum nee aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">unakaga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakaga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkaga neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkaga neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">padaithane ninaithane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaithane ninaithane"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaantha karuvizhiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaantha karuvizhiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">aalai mayakiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai mayakiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennule aasai mulaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennule aasai mulaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan mogam kothikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan mogam kothikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalana ithayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalana ithayam "/>
</div>
<div class="lyrico-lyrics-wrapper">karaithodi poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaithodi poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">paarvai paartha neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvai paartha neram"/>
</div>
<div class="lyrico-lyrics-wrapper">muthal thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">kaantha karuvizhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaantha karuvizhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">aalai mayakiruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai mayakiruthe"/>
</div>
</pre>
