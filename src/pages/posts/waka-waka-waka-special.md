---
title: "waka waka waka song lyrics"
album: "Special"
artist: "NVS Manyam"
lyricist: "Krishna Vasudev"
director: "Vastav"
path: "/albums/special-lyrics"
song: "Waka Waka Waka"
image: ../../images/albumart/special.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/YNyo7wybJ0o"
type: "mass"
singers:
  - Spoorthi
  - Aditya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">listen everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listen everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka "/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<div class="lyrico-lyrics-wrapper">waka waka waka waka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waka waka waka waka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okanokanoka mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okanokanoka mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde guttu dochey mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde guttu dochey mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">hey okanokanoka mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey okanokanoka mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde guttu dochey mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde guttu dochey mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">data chori chese mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="data chori chese mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">burra straw tho peelche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burra straw tho peelche"/>
</div>
<div class="lyrico-lyrics-wrapper">mind reader mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mind reader mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">evaru choodani nerputho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaru choodani nerputho"/>
</div>
<div class="lyrico-lyrics-wrapper">edha lothulaneedhe nerputho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edha lothulaneedhe nerputho"/>
</div>
<div class="lyrico-lyrics-wrapper">madhi lopala dhagina rahasyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhi lopala dhagina rahasyame"/>
</div>
<div class="lyrico-lyrics-wrapper">gadhi bayataku laage speedu tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gadhi bayataku laage speedu tho"/>
</div>
<div class="lyrico-lyrics-wrapper">vasthuna mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthuna mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">here is a mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="here is a mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">he is got the skill dear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he is got the skill dear"/>
</div>
<div class="lyrico-lyrics-wrapper">dil dhochy mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil dhochy mind reader"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mayadari mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayadari mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">guttu daganeede mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guttu daganeede mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">mind reader mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mind reader mind reader"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">choodatanikemo chotugadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choodatanikemo chotugadu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadiki cheyyichinavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadiki cheyyichinavante"/>
</div>
<div class="lyrico-lyrics-wrapper">ketugadu evarikemi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketugadu evarikemi"/>
</div>
<div class="lyrico-lyrics-wrapper">cheppakunda dhaachukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppakunda dhaachukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">oosulunnani oka thrachuthone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosulunnani oka thrachuthone"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadu chadhuvuthadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadu chadhuvuthadu "/>
</div>
<div class="lyrico-lyrics-wrapper">thappulo oppulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappulo oppulo "/>
</div>
<div class="lyrico-lyrics-wrapper">dont care ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dont care ante"/>
</div>
<div class="lyrico-lyrics-wrapper">okkasari vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkasari vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku thagalali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku thagalali"/>
</div>
<div class="lyrico-lyrics-wrapper">thappu chesi okkasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu chesi okkasari"/>
</div>
<div class="lyrico-lyrics-wrapper">cheyyichaava ika nee guttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheyyichaava ika nee guttu"/>
</div>
<div class="lyrico-lyrics-wrapper">rattey fattey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rattey fattey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okanokanoka mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okanokanoka mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde guttu dochey mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde guttu dochey mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">data chori chese mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="data chori chese mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">burra straw tho peelche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="burra straw tho peelche"/>
</div>
<div class="lyrico-lyrics-wrapper">mind reader mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mind reader mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mind reader"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cheekati dachina nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekati dachina nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">nadi velugunu choope nerputho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadi velugunu choope nerputho"/>
</div>
<div class="lyrico-lyrics-wrapper">navvu venake dhagina kruramee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvu venake dhagina kruramee"/>
</div>
<div class="lyrico-lyrics-wrapper">thera bayataki laage loukyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thera bayataki laage loukyame"/>
</div>
<div class="lyrico-lyrics-wrapper">chupinche mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chupinche mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">guttu rattu chese mind reader 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guttu rattu chese mind reader "/>
</div>
<div class="lyrico-lyrics-wrapper">dhachanivvademi mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhachanivvademi mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">he is got the skill dear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he is got the skill dear"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okanokanoka mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okanokanoka mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde guttu dochey mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde guttu dochey mind reader"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">here is a mind reader
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="here is a mind reader"/>
</div>
<div class="lyrico-lyrics-wrapper">his special skill dear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="his special skill dear"/>
</div>
<div class="lyrico-lyrics-wrapper">he steel the secrets beware
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he steel the secrets beware"/>
</div>
<div class="lyrico-lyrics-wrapper">coming in june dear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="coming in june dear"/>
</div>
</pre>
