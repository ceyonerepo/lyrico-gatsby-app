---
title: "meesa vecha vetaikkaran song lyrics"
album: "Sandakozhi 2"
artist: "Yuvan Sankar Raja"
lyricist: "Arun Bharathi"
director: "N. Linguswamy"
path: "/albums/sandakozhi-2-lyrics"
song: "Meesa Vecha Vetaikkaran"
image: ../../images/albumart/sandakozhi-2.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/U4kcpv-Bve8"
type: "Mass"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meesa Vecha Vetaikaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Vecha Vetaikaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Karuppan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Karuppan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerathukku Ivana Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathukku Ivana Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illa Oruthan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Oruthan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalli Elumbodu Aduppula Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalli Elumbodu Aduppula Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellaadu Methakkuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaadu Methakkuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Alli Kadikka Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Alli Kadikka Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vayaru Thandora Adikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vayaru Thandora Adikuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Thaan Namma Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thaan Namma Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Yaarukkum Adagantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaarukkum Adagantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madurai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madurai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Thaan Thimiru Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Thaan Thimiru Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Raththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Thimiruna Thirumbume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Thimiruna Thirumbume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Moththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesa Vecha Vettaikkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Vecha Vettaikkaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Karuppan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Karuppan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerathukku Ivan Pola Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathukku Ivan Pola Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruthan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruthan Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyilukku Thaan Vaakka Patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilukku Thaan Vaakka Patta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanangal Ellaam Koothu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanangal Ellaam Koothu Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolasaami Koyilil Koodiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolasaami Koyilil Koodiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Paasatha Thiruneera Poosiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Paasatha Thiruneera Poosiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullai Aathu Thannikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai Aathu Thannikulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mungi Vantha Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mungi Vantha Nenjukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeratha Eppothum Paarthidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeratha Eppothum Paarthidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Veeratha Paasathil Saaichidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Veeratha Paasathil Saaichidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa Vazhum Oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa Vazhum Oorukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagaiyellam Paranil Thookki Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaiyellam Paranil Thookki Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondha Bandham Kootti Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha Bandham Kootti Vechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Vaazhai Elai Poottu Aakki Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Vaazhai Elai Poottu Aakki Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilanthaari Kootamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanthaari Kootamellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu Koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naiyandi Aattam Onnu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naiyandi Aattam Onnu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Theruvellaam Mike Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Theruvellaam Mike Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaru Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gummalam Kondattam Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummalam Kondattam Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Enga Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Enga Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Eh Chella Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Eh Chella Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Enga Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Enga Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chella Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chella Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Veera Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Veera Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Thimiru Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thimiru Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Veera Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Veera Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Thimiru Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thimiru Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vaadipatti Vadugapatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vaadipatti Vadugapatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnuga Thaan Varisai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnuga Thaan Varisai Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthaley Suvarellam Poster Otii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaley Suvarellam Poster Otii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Oorukul Varaverpom Banner Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Oorukul Varaverpom Banner Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththai Magan Pola Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Magan Pola Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkaporu Pannathaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaporu Pannathaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiya Unakkulla Adakki Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya Unakkulla Adakki Vaiyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum Asal Ooru Ponnukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Asal Ooru Ponnukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanakkam Vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam Vaiyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladies Ellaam Athanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladies Ellaam Athanaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaenaaga Inikkura Makkalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenaaga Inikkura Makkalada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Thanda Panni putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thanda Panni putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appothu Unakku Thaan Sikkalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appothu Unakku Thaan Sikkalada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilanthaari Koottamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanthaari Koottamellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu Koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naiyandi Aattam Onnu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naiyandi Aattam Onnu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Theruvellam Mike Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Theruvellam Mike Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaru Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gummalam Kondattam Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummalam Kondattam Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Thaan Namma Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thaan Namma Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Yaarukkum Adagantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaarukkum Adagantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madurai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madurai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Thaan Thimiru Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Thaan Thimiru Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Raththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Thimiruna Thirumbumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Thimiruna Thirumbumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Moththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Thaan Namma Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thaan Namma Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Yaarukkum Adagantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yaarukkum Adagantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madurai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madurai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Thaan Thimiru Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Thaan Thimiru Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Raththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Raththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naama Thimiruna Thirumbumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Thimiruna Thirumbumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Moththam"/>
</div>
</pre>
