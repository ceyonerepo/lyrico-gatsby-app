---
title: "dhethadii song lyrics"
album: "Thipparaa Meesam"
artist: "Suresh Bobbili"
lyricist: "Purnachary"
director: "Krishna Vijay"
path: "/albums/thipparaa-meesam-lyrics"
song: "Dhethadii"
image: ../../images/albumart/thipparaa-meesam.jpg
date: 2019-11-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/iRYmsbg9y0o"
type: "happy"
singers:
  - Suresh Bobbili
  - Naresh Mamindla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Are Maasu Beate Mogindhira Maama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Maasu Beate Mogindhira Maama "/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Steppe Veseyyaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Steppe Veseyyaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Are Jorudaaruga Undhaamuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Jorudaaruga Undhaamuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Jara Joru Thaggithe Bekaaruraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara Joru Thaggithe Bekaaruraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dj Mixu Chesey Remixu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj Mixu Chesey Remixu "/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiranthaa Dharuveyyaliraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiranthaa Dharuveyyaliraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aakathaayi Mana Yavvanaanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakathaayi Mana Yavvanaanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasamlo Egareddhamuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasamlo Egareddhamuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Choti Zindagini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Choti Zindagini "/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Baar Dekho Yaaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Baar Dekho Yaaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Dosthi Chesukoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dosthi Chesukoni "/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuguru Theenu Maaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuguru Theenu Maaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Fate Raasukoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fate Raasukoni "/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Baar Dekho Yaaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Baar Dekho Yaaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy Age Idi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy Age Idi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kottu Guru Theenu Maaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottu Guru Theenu Maaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Repu Antene Mannu Paalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu Antene Mannu Paalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nede Choodaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nede Choodaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagini Bandhuvani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagini Bandhuvani "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Cheyyani Vaaru Leru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Cheyyani Vaaru Leru "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Ledu Thopu Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Ledu Thopu Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Thippu Meesam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thippu Meesam "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Nuvve Kottu Salaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Nuvve Kottu Salaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilla Rethirallaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Rethirallaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Vastaava Pagatilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Vastaava Pagatilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Undu Naatho Tellarekalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu Naatho Tellarekalla "/>
</div>
<div class="lyrico-lyrics-wrapper">Undhi Neetho Muchata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhi Neetho Muchata "/>
</div>
<div class="lyrico-lyrics-wrapper">Mallaa Mallaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallaa Mallaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yey Gelupe Andhukoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yey Gelupe Andhukoni "/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Baar Dekho Yaaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Baar Dekho Yaaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Viluve Penchukoni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluve Penchukoni "/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuguru Theenu Maaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuguru Theenu Maaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Nimisham Niluvadhani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimisham Niluvadhani "/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Baar Dekho Yaaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Baar Dekho Yaaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Aagadhani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Aagadhani "/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuguru Theenu Maaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuguru Theenu Maaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhrushtam Unte Anni Raavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhrushtam Unte Anni Raavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtam Telisinode 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtam Telisinode "/>
</div>
<div class="lyrico-lyrics-wrapper">Dheetugaadu Potugaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheetugaadu Potugaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Classu Maasantu Thedaa Ledhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Classu Maasantu Thedaa Ledhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu Undhante Thopu Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Undhante Thopu Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Thippu Meesam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thippu Meesam "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Nuvve Kottu Salaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Nuvve Kottu Salaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhethadii Thadi Thadi Pochamma Gudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhethadii Thadi Thadi Pochamma Gudi "/>
</div>
</pre>
