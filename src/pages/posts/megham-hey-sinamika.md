---
title: "megham song lyrics"
album: "Hey Sinamika"
artist: "Govind Vasantha"
lyricist: "Madhan Karky"
director: "Brinda"
path: "/albums/hey-sinamika-lyrics"
song: "Megham"
image: ../../images/albumart/hey-sinamika.jpg
date: 2022-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vpAVExa824I"
type: "love"
singers:
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Malayala karaiyin ooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayala karaiyin ooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal ondru veesum neraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal ondru veesum neraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaiyaamai nikkum mottrai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaiyaamai nikkum mottrai "/>
</div>
<div class="lyrico-lyrics-wrapper">poovaai kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovaai kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karum paarai kaatril aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karum paarai kaatril aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalirellaam bayanthe ooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalirellaam bayanthe ooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyaanal nirkkum theeyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyaanal nirkkum theeyai "/>
</div>
<div class="lyrico-lyrics-wrapper">en munn kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en munn kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukil ellaam paaynthe ooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukil ellaam paaynthe ooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram ellaam saaynth aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram ellaam saaynth aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragaaha veezhum enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragaaha veezhum enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam ellaam kaadhal konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam ellaam kaadhal konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megham otti minnal vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham otti minnal vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam otti mettu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam otti mettu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai vittu patchi rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai vittu patchi rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinn mutti kaio katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinn mutti kaio katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti onnum otha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti onnum otha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Karadi rendu thee patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadi rendu thee patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu konda udalin metha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu konda udalin metha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu then chotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu then chotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neron ennai koythu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neron ennai koythu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovellaam kaadhal peythu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovellaam kaadhal peythu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veretho bhoomiyai seithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veretho bhoomiyai seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nattaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nattaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattodu vaazhum meenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattodu vaazhum meenai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu paaya cheythu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu paaya cheythu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnennai vinna cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnennai vinna cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil vittaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil vittaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyodu kaiyum korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodu kaiyum korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu nenjai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithalodu ithalai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithalodu ithalai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin maiyai pulli thottaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin maiyai pulli thottaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megham otti minnal vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham otti minnal vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam otti mettu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam otti mettu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai vittu patchi rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai vittu patchi rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinn mutti kaio katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinn mutti kaio katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti onnum otha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti onnum otha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Karadi rendu thee patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadi rendu thee patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu konda udalin metha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu konda udalin metha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu then chotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu then chotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megham otti minnal vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham otti minnal vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam otti mettu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam otti mettu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai vittu patchi rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai vittu patchi rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinn mutti kaio katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinn mutti kaio katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti onnum otha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti onnum otha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Karadi rendu thee patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadi rendu thee patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu konda udalin metha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu konda udalin metha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu then chotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu then chotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megham otti minnal vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham otti minnal vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam otti mettu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam otti mettu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai vittu patchi rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai vittu patchi rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinn mutti kaio katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinn mutti kaio katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti onnum otha nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti onnum otha nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Karadi rendu thee patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadi rendu thee patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu konda udalin metha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu konda udalin metha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu then chotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu then chotta"/>
</div>
</pre>
