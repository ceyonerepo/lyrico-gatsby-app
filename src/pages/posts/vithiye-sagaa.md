---
title: "vithiye song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Vithiye"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TlNICokGcu8"
type: "love"
singers:
  - Shabir
  - Janani Rajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyae Nenjum Adanga Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Nenjum Adanga Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhava Illai Vizhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhava Illai Vizhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenappo Ennoda Kaivilangachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappo Ennoda Kaivilangachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varava Unna Thozhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varava Unna Thozhava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Vaadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Vaadudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Nogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Nogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alachidu Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alachidu Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithiyae Vithiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiyae Vithiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirai Nee Tharuvayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai Nee Tharuvayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithiyae Vithiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiyae Vithiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirai Nee Tharuvayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai Nee Tharuvayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Uyirkollum Theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Uyirkollum Theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Arivathillaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Arivathillaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruganam Naan Pizhaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruganam Naan Pizhaikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruganam Uyir Thurakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruganam Uyir Thurakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigalai Ingu Sumakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigalai Ingu Sumakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Irulinai Agatrida Vaa Pen Oliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Irulinai Agatrida Vaa Pen Oliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithiyae Vithiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiyae Vithiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirai Nee Tharuvayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai Nee Tharuvayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithiyae Vithiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiyae Vithiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirai Nee Tharuvayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirai Nee Tharuvayae"/>
</div>
</pre>
