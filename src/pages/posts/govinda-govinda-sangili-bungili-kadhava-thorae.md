---
title: "govinda govinda song lyrics"
album: "Sangili Bungili Kadhava Thorae"
artist: "Vishal Chandrasekhar"
lyricist: "Na Muthu Kumar"
director: "Ike"
path: "/albums/sangili-bungili-kadhava-thorae-lyrics"
song: "Govinda Govinda"
image: ../../images/albumart/sangili-bungili-kadhava-thorae.jpg
date: 2017-05-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Shq-eVaO4K4"
type: "happy"
singers:
  -	G V Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Left-il adikithu mister
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left-il adikithu mister"/>
</div>
<div class="lyrico-lyrics-wrapper">Right-il adikithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right-il adikithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha life enna vittu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha life enna vittu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathi adikithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathi adikithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solati adikithu mister
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solati adikithu mister"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratti adikithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratti adikithu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavu ellam trouserathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavu ellam trouserathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalati Adikithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalati Adikithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ara inch-u oru inch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara inch-u oru inch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan adutha nodi aaru inch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan adutha nodi aaru inch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela kedakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela kedakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha vechu idha vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha vechu idha vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondil vaangunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil vaangunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaanal neerila meena thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaanal neerila meena thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nondhu poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondhu poguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi adi peyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adi peyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Da govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">En heartkulla peridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heartkulla peridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Da govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu athanayum meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu athanayum meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan marubadiyum yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan marubadiyum yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven govinda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda "/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthu dandanakka danakunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthu dandanakka danakunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dandanakka nakka nakka nakka hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dandanakka nakka nakka nakka hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Madakki podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madakki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaan erakki kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaan erakki kuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedu vaanga aasaipatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu vaanga aasaipatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangavae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangavae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">En jathagathil sukkuran than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jathagathil sukkuran than"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbavae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbavae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Soru thanni thondaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru thanni thondaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangavae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangavae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sutta vadaiya sutta nariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sutta vadaiya sutta nariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanavae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanavae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi pona sooriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi pona sooriyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Merkil marayidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merkil marayidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En chandhirano megathukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chandhirano megathukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi oliyithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi oliyithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satta kizhiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satta kizhiyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En satta kizhiyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En satta kizhiyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vaazhka oru vattamunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vaazhka oru vattamunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo puriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo puriyuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhalum dhoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhalum dhoorathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham theriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham theriyuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi adi peyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adi peyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Da govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">En heartkulla peridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heartkulla peridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Da govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu athanayum meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu athanayum meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan marubadiyum yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan marubadiyum yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven govinda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda "/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
</pre>
