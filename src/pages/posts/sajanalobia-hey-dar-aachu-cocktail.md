---
title: "sajanalobia song lyrics"
album: "Cocktail"
artist: "Sai Bhaskar"
lyricist: "Gana Joe Papa - Sai Bhaskar"
director: "Ra.Vijaya Murugan"
path: "/albums/cocktail-lyrics"
song: "Sajanalobia - hey dar aachu"
image: ../../images/albumart/cocktail.jpg
date: 2020-07-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ox6O3rG-IfY"
type: "happy"
singers:
  - Gaana Joe Papa
  - Gana Guna
  - Udhay Kannan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Dharariranaa dhara nanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dharariranaa dhara nanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaanu tharariraraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanu tharariraraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyeyeyeyeyeyeyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyeyeyeyeyeyeyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaanu enna dhaanu appadiye valichinnae pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanu enna dhaanu appadiye valichinnae pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakketha maadhiri paadenpaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakketha maadhiri paadenpaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dar aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dar aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damage-u senjaalum salpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damage-u senjaalum salpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavanda kaattadha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavanda kaattadha nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kamatchi meenachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kamatchi meenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollachi balpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollachi balpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vootanda baltha kai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vootanda baltha kai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan alanju thirinjanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan alanju thirinjanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu scene-a poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu scene-a poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En humpty heart-u la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En humpty heart-u la"/>
</div>
<div class="lyrico-lyrics-wrapper">Love tent-a poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love tent-a poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En mummy unakku thaan maamiyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mummy unakku thaan maamiyar"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee right leg eduthu ullava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee right leg eduthu ullava"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu 2020 game-u ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu 2020 game-u ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna jeichu cup eduthu thookava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna jeichu cup eduthu thookava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala aati aati dabaikeethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala aati aati dabaikeethu"/>
</div>
<div class="lyrico-lyrics-wrapper">En titanic-um javanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En titanic-um javanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey peela udala meiyalum thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey peela udala meiyalum thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dar aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dar aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damage-u senjaalum salpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damage-u senjaalum salpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavanda kaattadha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavanda kaattadha nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kamatchi meenachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kamatchi meenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollachi balpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollachi balpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vootanda baltha kai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vootanda baltha kai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thagida thagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thagida thagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thaan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thaan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thagida thagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thagida thagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thaan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thaan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagida thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alagaesa manickku baasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagaesa manickku baasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan local don thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan local don thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Picaasa breaking-u news-a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picaasa breaking-u news-a"/>
</div>
<div class="lyrico-lyrics-wrapper">Un moonu walk thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moonu walk thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu road-u mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu road-u mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappal mela poguthu car-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappal mela poguthu car-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Couple aaga pona jore-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Couple aaga pona jore-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa munthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa munthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kunthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kunthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mitta mela barpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mitta mela barpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi kodutha virumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi kodutha virumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aja uja maja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aja uja maja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkura thirumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkura thirumbi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere sajanalobia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere sajanalobia"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee degree coffee-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee degree coffee-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kutchinae paapia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kutchinae paapia"/>
</div>
<div class="lyrico-lyrics-wrapper">En sajanalobia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sajanalobia"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sajanalobia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sajanalobia"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bun roti naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bun roti naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee halwa theanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee halwa theanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee foreign wine-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee foreign wine-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch aanen doinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch aanen doinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Margo malfi meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margo malfi meni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya konjam kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya konjam kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha ennai meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha ennai meratti"/>
</div>
<div class="lyrico-lyrics-wrapper">Potten avala surutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potten avala surutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi ennai paathu machaanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi ennai paathu machaanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava nenjukula enna thechaangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava nenjukula enna thechaangudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Putchikiren naanum mitaanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putchikiren naanum mitaanguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En pakkathula vanthu utkaarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pakkathula vanthu utkaarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai aati aati dabaikeethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai aati aati dabaikeethu"/>
</div>
<div class="lyrico-lyrics-wrapper">En sajanalobia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sajanalobia"/>
</div>
<div class="lyrico-lyrics-wrapper">En titanic-um javanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En titanic-um javanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey peela udala meiyalum thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey peela udala meiyalum thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey dar aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey dar aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Damage-u senjaalum salpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damage-u senjaalum salpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavanda kaattadha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavanda kaattadha nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kamatchi meenachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kamatchi meenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollachi balpi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollachi balpi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vootanda baltha kai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vootanda baltha kai nee"/>
</div>
</pre>
