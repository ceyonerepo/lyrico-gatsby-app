---
title: "kurumba song lyrics"
album: "Tik Tik Tik"
artist: "D. Imman"
lyricist: "Madhan Karky"
director: "Shakti Soundar Rajan"
path: "/albums/tik-tik-tik-lyrics"
song: "Kurumba"
image: ../../images/albumart/tik-tik-tik.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZBtMymYojsg"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram kuraindhen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram kuraindhen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalil varaindhen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalil varaindhen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil karaindhen unnaalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil karaindhen unnaalae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragaai virindhen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragaai virindhen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil parandhen unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil parandhen unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirangal niraindhen unnaalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirangal niraindhen unnaalae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otrai crayon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai crayon"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendaai udaithu kirukkiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendaai udaithu kirukkiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urulaich cheeval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urulaich cheeval"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyai vedithu norukkiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyai vedithu norukkiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Norukkiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norukkiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae needhaan daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aaa aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aaa aa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinveli meengalil ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinveli meengalil ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhidhaanae paarppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhidhaanae paarppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennila undhan kaalil serppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila undhan kaalil serppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrigal aayiram vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrigal aayiram vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaiyodae yerppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaiyodae yerppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam mattumdhaanae thorppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam mattumdhaanae thorppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattam podumbodhellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam podumbodhellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae azhagaai maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae azhagaai maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettup paadam seidhaaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettup paadam seidhaaloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratha azhutham yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratha azhutham yerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan kurumbu marabanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kurumbu marabanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvazhi kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvazhi kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku theriyaadhaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku theriyaadhaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae needhaan daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ularal mazhigal unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ularal mazhigal unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Cartoon kanavum unnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cartoon kanavum unnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkaai aanaen unnaalae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkaai aanaen unnaalae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erumboderumbaai sila naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumboderumbaai sila naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonai naayaai sila naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonai naayaai sila naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan aanen unnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan aanen unnaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vindhai endru kaiyil vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vindhai endru kaiyil vandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">En manam kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manam kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhai endru pattam thandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhai endru pattam thandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thalai nimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thalai nimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai nimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai nimira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ulagae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ulagae needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae needhaan daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uthiram needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uthiram needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En vidiyal needhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vidiyal needhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaammm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaammm"/>
</div>
</pre>
