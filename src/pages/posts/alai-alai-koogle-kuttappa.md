---
title: "alai alai song lyrics"
album: "Koogle Kuttappa"
artist: "Ghibran"
lyricist: "Madhan Karky"
director: "Sabari – Saravanan"
path: "/albums/koogle-kuttappa-lyrics"
song: "Alai Alai"
image: ../../images/albumart/koogle-kuttappa.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/usZXYtpRz-0"
type: "love"
singers:
  - G V Prakash Kumar
  - Deepthi Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alai Alai Alai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alai Alai "/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyena Unnai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyena Unnai Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Karai Kadarkarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Karai Kadarkarai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Kaadhal Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Kaadhal Konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Tholai Tholai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Tholai Tholai "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivinil Nenjam Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivinil Nenjam Vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Adhai Unadhalaigalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Adhai Unadhalaigalil "/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuril Kuril Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuril Kuril Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthene Thanthani Aalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthene Thanthani Aalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedil Nedil Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedil Nedil Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Aanene Nee En Thunaikaalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanene Nee En Thunaikaalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhage Alaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Alaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirpaara Ooru Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirpaara Ooru Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalgal Izhupathu Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalgal Izhupathu Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinjalile Kilinjal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjalile Kilinjal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Urasidum Osaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Urasidum Osaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Idhayathile Ketkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Idhayathile Ketkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugil Kulungi Mugil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugil Kulungi Mugil "/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungi Veezhndhidum Thooralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungi Veezhndhidum Thooralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaikadale Unadhudalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaikadale Unadhudalil "/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhgiren Veezhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhgiren Veezhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhamaai Unnile Veezhigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhamaai Unnile Veezhigirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Alai Alai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alai Alai "/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyena Paaindhen Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyena Paaindhen Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Kaarai Kadarkarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Kaarai Kadarkarai "/>
</div>
<div class="lyrico-lyrics-wrapper">Unil Saaindhen Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unil Saaindhen Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Tholai Nedun 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Tholai Nedun "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivinil Thondri Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivinil Thondri Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuraiyudan Manal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyudan Manal "/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyinil Ennai Thandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyinil Ennai Thandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thavari Sendrene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thavari Sendrene "/>
</div>
<div class="lyrico-lyrics-wrapper">Thananthani Meenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananthani Meenaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Unai Vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Unai Vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kettene Neeye Vazhi Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettene Neeye Vazhi Aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanave Karaiye Ini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Karaiye Ini "/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvil Vidumuraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvil Vidumuraiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinjalile Kilinjal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjalile Kilinjal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Urasidum Osaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Urasidum Osaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Idhayathile Ketkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Idhayathile Ketkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugil Kulungi Mugil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugil Kulungi Mugil "/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungi Veezhndhidum Thooralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungi Veezhndhidum Thooralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manal Madiyil Oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manal Madiyil Oru "/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyil Veezhgiren Veezhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyil Veezhgiren Veezhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhamaai Unnile Veezhigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhamaai Unnile Veezhigirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo Eezhathin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo Eezhathin "/>
</div>
<div class="lyrico-lyrics-wrapper">Poongaatru Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatru Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Meedhu Modhi Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meedhu Modhi Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Oo Pogindra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Oo Pogindra "/>
</div>
<div class="lyrico-lyrics-wrapper">Podhendhan Marbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhendhan Marbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Perai Theeti Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Perai Theeti Chella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Thamizhai Suvaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Thamizhai Suvaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavam Kidandha Kaadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavam Kidandha Kaadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Tamil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Tamil "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadalaaai Thondrinaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadalaaai Thondrinaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinjalile Kilinjal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjalile Kilinjal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Urasidum Osaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Urasidum Osaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Idhayathile Ketkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Idhayathile Ketkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyanthirangal Naduvinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyanthirangal Naduvinile"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Marandha Podhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Marandha Podhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thunaiyaai Uyir Thunaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thunaiyaai Uyir Thunaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrinaai Thondrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrinaai Thondrinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhanaai Thozhanaai Thondrinaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhanaai Thozhanaai Thondrinaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Alai Alai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alai Alai "/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyena Paaindhen Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyena Paaindhen Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Kaarai Kadarkarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Kaarai Kadarkarai "/>
</div>
<div class="lyrico-lyrics-wrapper">Inil Saaindhen Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inil Saaindhen Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Tholai Nedun 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Tholai Nedun "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivinil Thondri Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivinil Thondri Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuraiyudan Manal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyudan Manal "/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyinil Ennai Thandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyinil Ennai Thandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thavari Sendrene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thavari Sendrene "/>
</div>
<div class="lyrico-lyrics-wrapper">Thananthani Meenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananthani Meenaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Unai Vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Unai Vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kettene Neeye Vazhi Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettene Neeye Vazhi Aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanave Karaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Karaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Eppodhum Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Eppodhum Ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvil Vidumuraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvil Vidumuraiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinjalile Kilinjal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjalile Kilinjal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Urasidum Osaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Urasidum Osaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Idhayathile Ketkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Idhayathile Ketkirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugil Kulungi Mugil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugil Kulungi Mugil "/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungi Veezhndhidum Thooralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungi Veezhndhidum Thooralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manal Madiyil Oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manal Madiyil Oru "/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyil Veezhgiren Veezhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyil Veezhgiren Veezhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhamaai Unnile Veezhigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhamaai Unnile Veezhigirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalaai Thondrinaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaai Thondrinaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalaai Thondrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaai Thondrinaai"/>
</div>
</pre>
