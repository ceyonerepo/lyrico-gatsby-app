---
title: "karadu muradu female version song lyrics"
album: "Bakrid"
artist: "D. Imman"
lyricist: "Gnanakaravel"
director: "Jagadeesan Subu"
path: "/albums/bakrid-lyrics"
song: "Karadu Muradu Female Version"
image: ../../images/albumart/bakrid.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SbYeYOVZgtk"
type: "happy"
singers:
  - Punya Selva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Ulavum Theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ulavum Theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala Ariyaa Seiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavala Ariyaa Seiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Naan Unakku Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naan Unakku Thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla Poththi PaathuVanthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla Poththi PaathuVanthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadu Mala Thaandi Kooti Vanthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu Mala Thaandi Kooti Vanthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamae Unai Naan Baliyaa Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamae Unai Naan Baliyaa Tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Nodi Enna Nadakkumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Nodi Enna Nadakkumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Patharuthu Thikku Thikkunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Patharuthu Thikku Thikkunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellamae Ezhunthu Odivaa Viraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamae Ezhunthu Odivaa Viraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodi Poluthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Poluthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Yugam Ena Neezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yugam Ena Neezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Ulavum Theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ulavum Theevae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noyi Nodi Thaakkumunnu Bayanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noyi Nodi Thaakkumunnu Bayanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooti Vandhen Unaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooti Vandhen Unaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Usuru Pogumunnu Nenacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Usuru Pogumunnu Nenacha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usurum Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usurum Illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Nee Raajavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Nee Raajavada"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Unai Kolvaargalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Unai Kolvaargalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kootilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kootilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boologammum Keezh Vaanamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologammum Keezh Vaanamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saerum Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerum Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Illai Oododi Vaa Pogum Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Illai Oododi Vaa Pogum Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nachchu Naragaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nachchu Naragaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu Vanthuvidu Povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Vanthuvidu Povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kutti Sorgathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kutti Sorgathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaamboochigalaa Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaamboochigalaa Aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyo Sugamoo Koodi Vaazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyo Sugamoo Koodi Vaazhvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Ulavum Theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ulavum Theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala Ariyaa Seiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavala Ariyaa Seiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Naan Unakku Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naan Unakku Thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla Poththi Paathu Vanthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla Poththi Paathu Vanthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadu Mala Thaandi Kooti Vanthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu Mala Thaandi Kooti Vanthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamae Unai Naan Baliyaa Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamae Unai Naan Baliyaa Tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Nodi Enna Nadakkumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Nodi Enna Nadakkumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Patharuthu Thikku Thikkunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Patharuthu Thikku Thikkunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellamae Ezhunthu Odivaa Viraivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamae Ezhunthu Odivaa Viraivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodi Poluthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Poluthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Yugam Ena Neezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yugam Ena Neezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Raaram Thararam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Raaram Thararam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Ra Raa Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Ra Raa Thara"/>
</div>
<div class="lyrico-lyrics-wrapper">ra Raa Thara Ra Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra Raa Thara Ra Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadu Moradu Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadu Moradu Poovae"/>
</div>
</pre>
