---
title: "polladha ulagam song lyrics"
album: "Maaran"
artist: "GV Prakash Kumar"
lyricist: "Vivek"
director: "Karthick Naren"
path: "/albums/maaran-lyrics"
song: "Polladha Ulagam"
image: ../../images/albumart/maaran.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BaGsgjwyHgc"
type: "mass"
singers:
  - Dhanush
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey idhu polladha ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idhu polladha ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee romba sharp aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee romba sharp aah iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum yaar enna koraichal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum yaar enna koraichal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjam mass aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjam mass aah iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan rightunbaan bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan rightunbaan bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thappumbaan bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thappumbaan bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha ellathaiyum kettaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha ellathaiyum kettaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">How will you grow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How will you grow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna kingunbaan bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kingunbaan bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vuttaa godunbaanbro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuttaa godunbaanbro"/>
</div>
<div class="lyrico-lyrics-wrapper">Apram sangootha poraannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apram sangootha poraannu"/>
</div>
<div class="lyrico-lyrics-wrapper">How will you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How will you know"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un rootta nee podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un rootta nee podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un match ah nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un match ah nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aaru ballum sixer adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aaru ballum sixer adi daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey idhu polladha ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idhu polladha ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee romba sharp aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee romba sharp aah iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum yaar enna koraichal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum yaar enna koraichal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjam mass aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjam mass aah iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass aah iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vazhi pudichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhi pudichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa maa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa maa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enna veruthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna veruthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa maa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa maa maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Large aah nee jeichaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Large aah nee jeichaathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Small aah nee aadi ppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Small aah nee aadi ppo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sindhum vervaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sindhum vervaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Poomaalai soodikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomaalai soodikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudicha vaazhka simple naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudicha vaazhka simple naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu thaana bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu thaana bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthu porandu enthiruchaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthu porandu enthiruchaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Worth uh thaana bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worth uh thaana bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae un rootta nee podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae un rootta nee podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un match ah nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un match ah nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aaru baallum sixer adi daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aaru baallum sixer adi daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey…ey idhu polladha ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey…ey idhu polladha ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee romba sharp aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee romba sharp aah iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum yaar enna koraichal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum yaar enna koraichal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjam mass aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjam mass aah iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass aah iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass aah iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan rightunbaan bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan rightunbaan bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thappumbaan bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thappumbaan bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha ellathaiyum kettaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha ellathaiyum kettaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">How will you grow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How will you grow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna kingunbaan bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kingunbaan bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vuttaa godunbaanbro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vuttaa godunbaanbro"/>
</div>
<div class="lyrico-lyrics-wrapper">Apram sangootha poraannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apram sangootha poraannu"/>
</div>
<div class="lyrico-lyrics-wrapper">How will you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How will you know"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When are you will
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When are you will"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna be you right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonna be you right"/>
</div>
<div class="lyrico-lyrics-wrapper">you gonna be the hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you gonna be the hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Never get to that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never get to that"/>
</div>
<div class="lyrico-lyrics-wrapper">You know what i said
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know what i said"/>
</div>
<div class="lyrico-lyrics-wrapper">Savage savage chase bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savage savage chase bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When are you will
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When are you will"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna be you right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonna be you right"/>
</div>
<div class="lyrico-lyrics-wrapper">you gonna be the hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you gonna be the hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Never get to that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never get to that"/>
</div>
<div class="lyrico-lyrics-wrapper">You know what i said
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know what i said"/>
</div>
<div class="lyrico-lyrics-wrapper">Savage savage chase bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savage savage chase bro"/>
</div>
</pre>
