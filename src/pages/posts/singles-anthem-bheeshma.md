---
title: "singles anthem song lyrics"
album: "Bheeshma"
artist: "Mahati Swara Sagar"
lyricist: "Sri Mani"
director: "Venky Kudumula"
path: "/albums/bheeshma-lyrics"
song: "Singles Anthem"
image: ../../images/albumart/bheeshma.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gWI6SH4pqLc"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">High Class Nunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High Class Nunchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Low Class Dhaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Low Class Dhaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Crush-le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Crush-le"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhallo Unnarule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhallo Unnarule"/>
</div>
<div class="lyrico-lyrics-wrapper">Okallu Set Avvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okallu Set Avvale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kissing Kosam Hugging Kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kissing Kosam Hugging Kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Waiting Le Papenake Jogging Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waiting Le Papenake Jogging Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Antha Begging Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Antha Begging Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennallila Ee Ontari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennallila Ee Ontari "/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke Nakila Ooh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke Nakila Ooh ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Boyfriend La Nanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boyfriend La Nanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Marachadhe Ae Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachadhe Ae Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh ooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yem Chesina Na Status 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem Chesina Na Status "/>
</div>
<div class="lyrico-lyrics-wrapper">Single Marala Ooh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Marala Ooh ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Vaipila Chudadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Vaipila Chudadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Cinderella Ha-ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Cinderella Ha-ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Single-y I’aM 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Single-y I’aM "/>
</div>
<div class="lyrico-lyrics-wrapper">Ready To Mingle-y
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready To Mingle-y"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Ki Leve Ranguley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ki Leve Ranguley"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Padava Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Padava Papa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoye Jantale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye Jantale"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kanta Padithe Mental-e
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kanta Padithe Mental-e"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollantha Jealousy Mantale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollantha Jealousy Mantale"/>
</div>
<div class="lyrico-lyrics-wrapper">Challarche Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challarche Papa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Pretty-pretty Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Pretty-pretty Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Na-na Na-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Na-na Na-na"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are So Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are So Beautiful"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Na-na Na-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Na-na Na-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Sassy Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Sassy Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Na-na Na-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Na-na Na-na"/>
</div>
<div class="lyrico-lyrics-wrapper">You Make Ma Life Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Make Ma Life Beautiful"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Pretty-pretty Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Pretty-pretty Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Na-na Na-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Na-na Na-na"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are So Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are So Beautiful"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Na-na Na-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Na-na Na-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma Ma Ma Sassy Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma Ma Ma Sassy Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Na-na Na-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Na-na Na-na"/>
</div>
<div class="lyrico-lyrics-wrapper">You Make Ma Life Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Make Ma Life Beautiful"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendhuko Yemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhuko Yemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontarayi Vunnan Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontarayi Vunnan Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuru Padathemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuru Padathemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Andhala Devatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Andhala Devatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jali Chupena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jali Chupena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalame Napayi Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalame Napayi Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemi Thalaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemi Thalaratho"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kharma Kalindhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kharma Kalindhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayi-Ayi-Yo Single-y
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayi-Ayi-Yo Single-y"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Ready To Mingle-y
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Ready To Mingle-y"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Ki Leve Rangule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ki Leve Rangule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Padava Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Padava Papa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoye Jantale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye Jantale"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Katta Padithe Mental-y
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Katta Padithe Mental-y"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha Jealousy Mantale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha Jealousy Mantale"/>
</div>
<div class="lyrico-lyrics-wrapper">Challarchey Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challarchey Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh"/>
</div>
</pre>
