---
title: "kalaavathi song lyrics"
album: "Sarkaru Vaari Paata"
artist: "S Thaman"
lyricist: "Ananta Sriram"
director: "Parasuram"
path: "/albums/sarkaru-vaari-paata-lyrics"
song: "Kalaavathi"
image: ../../images/albumart/sarkaru-vaari-paata.jpg
date: 2022-05-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Vbu44JdN12s"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maangalyam Thanthunanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangalyam Thanthunanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Jeevana Hethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Jeevana Hethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanthe Bhadhnaami Subhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthe Bhadhnaami Subhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Thvam Jeeva Sharadhaam Shatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvam Jeeva Sharadhaam Shatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandho Oka Veyyo Oka Laksho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandho Oka Veyyo Oka Laksho"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulu Meedhaki Dhookinaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulu Meedhaki Dhookinaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhe Nee Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhe Nee Maaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundho Atupakko Itudhikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundho Atupakko Itudhikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipiga Theegalu Moginaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipiga Theegalu Moginaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyindhe Soya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyindhe Soya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ittanti Vannee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittanti Vannee "/>
</div>
<div class="lyrico-lyrics-wrapper">Alavaate Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavaate Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Attanti Naakee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attanti Naakee "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabaatasa Lendhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabaatasa Lendhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Dhada Gundhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Dhada Gundhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidigundhe Jadisindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidigundhe Jadisindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Jathapadamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Jathapadamani "/>
</div>
<div class="lyrico-lyrics-wrapper">Thega Pilichinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega Pilichinadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Come On Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Come On Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Gathe Nuvve Gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Gathe Nuvve Gathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Come On Come On Kalaavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Come On Kalaavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Lekunte Adho Gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Lekunte Adho Gathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maangalyam Thanthunanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangalyam Thanthunanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Jeevana Hethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Jeevana Hethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kante Bhadhnaami Subhagethvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kante Bhadhnaami Subhagethvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeeva Sharadhaam Shatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeva Sharadhaam Shatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandho Oka Veyyo Oka Laksho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandho Oka Veyyo Oka Laksho"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulu Meedhaki Dhookinaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulu Meedhaki Dhookinaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhe Nee Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhe Nee Maaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anyaayanga Manasuni Kelikaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anyaayanga Manasuni Kelikaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Annam Maanesi Ninne Choosela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annam Maanesi Ninne Choosela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhurmarganga Sogasuni Visiraave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhurmarganga Sogasuni Visiraave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidra Maanesi Ninne Thalachela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidra Maanesi Ninne Thalachela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranga Ghorangaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga Ghorangaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kalalani Kadhipaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kalalani Kadhipaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhongaa Andhangaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhongaa Andhangaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Na Pogaruni Dhochaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Pogaruni Dhochaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinchi Athikinchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinchi Athikinchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Irikinchi Vadhilinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irikinchi Vadhilinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Bathukuni Cheda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Bathukuni Cheda "/>
</div>
<div class="lyrico-lyrics-wrapper">Godithivi Kadhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godithivi Kadhave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Avi Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Avi Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolamaindhe Naa Gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolamaindhe Naa Gathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuroola Avi Kalaavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuroola Avi Kalaavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kullaabodisindi Chaalu Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kullaabodisindi Chaalu Thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Come On Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Come On Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Gathe Nuvve Gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Gathe Nuvve Gathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Come On Come On Kalaavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Come On Kalaavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Lekunte Adho Gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Lekunte Adho Gathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maangalyam Thanthunanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangalyam Thanthunanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Jeevana Hethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Jeevana Hethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanthe Bhadhnaami Subhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthe Bhadhnaami Subhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Thvam Jeeva Sharadhaam Shatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvam Jeeva Sharadhaam Shatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vandho Oka Veyyo Oka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vandho Oka Veyyo Oka"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupulu Meedhaki Dhookinaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupulu Meedhaki Dhookinaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhe Nee Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendhe Nee Maaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundho Atupakko Itudhikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundho Atupakko Itudhikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipiga Theegalu Moginaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipiga Theegalu Moginaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyindhe Soya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyindhe Soya"/>
</div>
</pre>
