---
title: "pookkal methile song lyrics"
album: "Pagadi Aattam"
artist: "Karthik Raja"
lyricist: "Karthik Raja"
director: "Ram K Chandran"
path: "/albums/pagadi-aattam-lyrics"
song: "Pookkal Methile"
image: ../../images/albumart/pagadi-aattam.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wadkxgzSyNI"
type: "love"
singers:
  - Archana Nandakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pookal meethile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal meethile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam thontruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam thontruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai meeriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai meeriye"/>
</div>
<div class="lyrico-lyrics-wrapper">yengum mogame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengum mogame"/>
</div>
<div class="lyrico-lyrics-wrapper">yetho ennai seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetho ennai seithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal valaiyai veesinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal valaiyai veesinai"/>
</div>
<div class="lyrico-lyrics-wrapper">yetho ennai seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetho ennai seithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal valaiyai veesinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal valaiyai veesinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yetho ennai seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetho ennai seithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal valaiyai veesinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal valaiyai veesinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai yetho seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai yetho seithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pookal meethile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal meethile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam thontruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam thontruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai meeriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai meeriye"/>
</div>
<div class="lyrico-lyrics-wrapper">yengum mogame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengum mogame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pookal meethile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal meethile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam thontruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam thontruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai meeriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai meeriye"/>
</div>
<div class="lyrico-lyrics-wrapper">yengum mogame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengum mogame"/>
</div>
</pre>
