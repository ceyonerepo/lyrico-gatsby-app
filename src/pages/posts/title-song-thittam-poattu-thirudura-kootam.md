---
title: "title song song lyrics"
album: "Thittam Poattu Thirudura Kootam"
artist: "Ashwath"
lyricist: "Sudhar"
director: "Sudhar"
path: "/albums/thittam-poattu-thirudura-kootam-lyrics"
song: "Title Song"
image: ../../images/albumart/thittam-poattu-thirudura-kootam.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A2fEAgj18NI"
type: "title song"
singers:
  - Deepak
  - Aparna Narayanan
  - Divya Prasad
  - Lil Phil (Rap)
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">watt up ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="watt up ya"/>
</div>
<div class="lyrico-lyrics-wrapper">we're tptk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we're tptk"/>
</div>
<div class="lyrico-lyrics-wrapper">not gettin' me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="not gettin' me"/>
</div>
<div class="lyrico-lyrics-wrapper">listena what i say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="listena what i say"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thiru thiruvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiru thiruvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">thuru thurvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuru thurvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">thedura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">viru viruvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viru viruvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">oadura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oadura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh ohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh ohho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thagaigal engum irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaigal engum irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai udaithida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai udaithida "/>
</div>
<div class="lyrico-lyrics-wrapper">thunindhidum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunindhidum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">scene potu style ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="scene potu style ah"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaan kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaan kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dare ya darlin'
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dare ya darlin'"/>
</div>
<div class="lyrico-lyrics-wrapper">babe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babe"/>
</div>
<div class="lyrico-lyrics-wrapper">mudunja thotu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudunja thotu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">dear i'm stealin'
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dear i'm stealin'"/>
</div>
<div class="lyrico-lyrics-wrapper">babe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babe"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinjaa uthu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinjaa uthu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">we' re gonna rock it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we' re gonna rock it"/>
</div>
<div class="lyrico-lyrics-wrapper">babe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babe"/>
</div>
<div class="lyrico-lyrics-wrapper">wait n see that far
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wait n see that far"/>
</div>
<div class="lyrico-lyrics-wrapper">we ain't no average
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we ain't no average"/>
</div>
<div class="lyrico-lyrics-wrapper">kinda thieves
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinda thieves"/>
</div>
<div class="lyrico-lyrics-wrapper">can't getta walk n ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="can't getta walk n ya"/>
</div>
<div class="lyrico-lyrics-wrapper">can't getta leave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="can't getta leave"/>
</div>
<div class="lyrico-lyrics-wrapper">we be the masters
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we be the masters"/>
</div>
<div class="lyrico-lyrics-wrapper">of robbery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="of robbery"/>
</div>
<div class="lyrico-lyrics-wrapper">if plan a fail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="if plan a fail"/>
</div>
<div class="lyrico-lyrics-wrapper">we got plan b
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we got plan b"/>
</div>
<div class="lyrico-lyrics-wrapper">its that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its that"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="that"/>
</div>
<div class="lyrico-lyrics-wrapper">tptk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tptk"/>
</div>
<div class="lyrico-lyrics-wrapper">take ya money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="take ya money"/>
</div>
<div class="lyrico-lyrics-wrapper">run away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run away"/>
</div>
<div class="lyrico-lyrics-wrapper">tryin' to stop me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tryin' to stop me"/>
</div>
<div class="lyrico-lyrics-wrapper">there's no way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="there's no way"/>
</div>
<div class="lyrico-lyrics-wrapper">this ain't the 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="this ain't the "/>
</div>
<div class="lyrico-lyrics-wrapper">child's play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="child's play"/>
</div>
<div class="lyrico-lyrics-wrapper">this is the master 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="this is the master "/>
</div>
<div class="lyrico-lyrics-wrapper">mind's game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mind's game"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koodi koodi thittam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi koodi thittam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi thedi kattam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi thedi kattam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhayal maatum nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhayal maatum nammodu"/>
</div>
<div class="lyrico-lyrics-wrapper">pangu pottu kondadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pangu pottu kondadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kullanariya koondodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kullanariya koondodu"/>
</div>
<div class="lyrico-lyrics-wrapper">adichu thooki pandhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichu thooki pandhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">sakkapodu podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakkapodu podura"/>
</div>
<div class="lyrico-lyrics-wrapper">beat um 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beat um "/>
</div>
<div class="lyrico-lyrics-wrapper">pickup odu kelapura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pickup odu kelapura"/>
</div>
<div class="lyrico-lyrics-wrapper">heat um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heat um"/>
</div>
<div class="lyrico-lyrics-wrapper">buildup odu alapara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buildup odu alapara"/>
</div>
<div class="lyrico-lyrics-wrapper">mootum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mootum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thagaigal engum irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaigal engum irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai udaithida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai udaithida "/>
</div>
<div class="lyrico-lyrics-wrapper">thunindhidum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunindhidum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">scene potu style ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="scene potu style ah"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaan kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaan kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kurutu thanam engaruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurutu thanam engaruku"/>
</div>
<div class="lyrico-lyrics-wrapper">thirutu thanam angaruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirutu thanam angaruku"/>
</div>
<div class="lyrico-lyrics-wrapper">kattam katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattam katti"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">kattu katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattu katta"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo thenara uttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo thenara uttu"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga hacking 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga hacking "/>
</div>
<div class="lyrico-lyrics-wrapper">lock picking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lock picking"/>
</div>
<div class="lyrico-lyrics-wrapper">ellathulayum pro's
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellathulayum pro's"/>
</div>
<div class="lyrico-lyrics-wrapper">do it with style
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do it with style"/>
</div>
<div class="lyrico-lyrics-wrapper">and that's how it goes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and that's how it goes"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thiru thiruvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiru thiruvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">thuru thurvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuru thurvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">thedura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">viru viruvendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viru viruvendru"/>
</div>
<div class="lyrico-lyrics-wrapper">oadura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oadura kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh ohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh ohho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thagaigal engum irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagaigal engum irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai udaithida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai udaithida "/>
</div>
<div class="lyrico-lyrics-wrapper">thunindhidum kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunindhidum kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">scene potu style ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="scene potu style ah"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaan kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaan kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">thittam pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittam pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">thirudura kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudura kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dare ya darlin'
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dare ya darlin'"/>
</div>
<div class="lyrico-lyrics-wrapper">babe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babe"/>
</div>
<div class="lyrico-lyrics-wrapper">mudunja thotu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudunja thotu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">dear i'm stealin'
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dear i'm stealin'"/>
</div>
<div class="lyrico-lyrics-wrapper">babe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babe"/>
</div>
<div class="lyrico-lyrics-wrapper">vidinjaa uthu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidinjaa uthu paar"/>
</div>
</pre>
