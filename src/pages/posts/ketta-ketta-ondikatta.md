---
title: "ketta ketta song lyrics"
album: "Ondikatta"
artist: "Bharani"
lyricist: "Bharani"
director: "Bharani"
path: "/albums/ondikatta-lyrics"
song: "Ketta Ketta"
image: ../../images/albumart/ondikatta.jpg
date: 2018-07-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kFyP-TCRtxA"
type: "love"
singers:
  - Prasanna
  - Priya Yamesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ketta ketta kanava varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketta ketta kanava varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham onnu tharatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham onnu tharatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">akkam pakkam aalunga iruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkam pakkam aalunga iruke"/>
</div>
<div class="lyrico-lyrics-wrapper">vetu pakkam varatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetu pakkam varatume"/>
</div>
<div class="lyrico-lyrics-wrapper">che che che venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="che che che venam"/>
</div>
<div class="lyrico-lyrics-wrapper">aparama papom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aparama papom"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum neyum senthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum neyum senthu"/>
</div>
<div class="lyrico-lyrics-wrapper">love kulla kuthipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love kulla kuthipom"/>
</div>
<div class="lyrico-lyrics-wrapper">serama nanum unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serama nanum unna "/>
</div>
<div class="lyrico-lyrics-wrapper">vida maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketta ketta kanava varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketta ketta kanava varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham onnu tharatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham onnu tharatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">akkam pakkam aalunga iruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkam pakkam aalunga iruke"/>
</div>
<div class="lyrico-lyrics-wrapper">vetu pakkam varatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetu pakkam varatume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thulli thulli nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thulli nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu kulla vilunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu kulla vilunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukulla kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukulla kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna enamo enamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna enamo enamo"/>
</div>
<div class="lyrico-lyrics-wrapper">seiyaa sollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seiyaa sollura"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaiyila vilunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyila vilunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aaki vacha virunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaki vacha virunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamathaiyum kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamathaiyum kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">athai unnalam unnalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai unnalam unnalam"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam poru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam poru da"/>
</div>
<div class="lyrico-lyrics-wrapper">kitta vaa kitta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta vaa kitta vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vetka padatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetka padatha "/>
</div>
<div class="lyrico-lyrics-wrapper">kanula kanula sutu pudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanula kanula sutu pudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">varava onnu tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varava onnu tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu nanum enodu neyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu nanum enodu neyum"/>
</div>
<div class="lyrico-lyrics-wrapper">serama nanum unna vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serama nanum unna vida maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketta ketta kanava varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketta ketta kanava varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham onnu tharatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham onnu tharatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">akkam pakkam aalunga iruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkam pakkam aalunga iruke"/>
</div>
<div class="lyrico-lyrics-wrapper">aparama varatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aparama varatume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">motu motu poovu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motu motu poovu"/>
</div>
<div class="lyrico-lyrics-wrapper">meni engum novu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meni engum novu"/>
</div>
<div class="lyrico-lyrics-wrapper">yenguthe navu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenguthe navu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna ipave ipave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna ipave ipave"/>
</div>
<div class="lyrico-lyrics-wrapper">ichu panava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichu panava"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti kutti theevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti kutti theevu"/>
</div>
<div class="lyrico-lyrics-wrapper">koopuduthe aavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koopuduthe aavu"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu kallu maavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu kallu maavu"/>
</div>
<div class="lyrico-lyrics-wrapper">una ipave ipave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una ipave ipave"/>
</div>
<div class="lyrico-lyrics-wrapper">alli thingave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli thingave"/>
</div>
<div class="lyrico-lyrics-wrapper">sollava sollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollava sollava"/>
</div>
<div class="lyrico-lyrics-wrapper">sokki vilaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokki vilaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">allava allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allava allava"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa vidathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa vidathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thodava mella thodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodava mella thodava"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangai thene uchaththil nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangai thene uchaththil nane"/>
</div>
<div class="lyrico-lyrics-wrapper">ammamamma nanum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammamamma nanum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida maten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketta ketta kanava varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketta ketta kanava varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham onnu tharatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham onnu tharatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">akkam pakkam aalunga iruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkam pakkam aalunga iruke"/>
</div>
<div class="lyrico-lyrics-wrapper">vetu pakkam varatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetu pakkam varatume"/>
</div>
<div class="lyrico-lyrics-wrapper">che che che venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="che che che venam"/>
</div>
<div class="lyrico-lyrics-wrapper">aparama papom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aparama papom"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum neyum senthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum neyum senthu"/>
</div>
<div class="lyrico-lyrics-wrapper">love kulla kuthipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love kulla kuthipom"/>
</div>
<div class="lyrico-lyrics-wrapper">serama nanum unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serama nanum unna "/>
</div>
<div class="lyrico-lyrics-wrapper">vida maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida maaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ketta ketta kanava varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketta ketta kanava varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham onnu tharatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham onnu tharatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">akkam pakkam aalunga iruke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkam pakkam aalunga iruke"/>
</div>
<div class="lyrico-lyrics-wrapper">vetu pakkam varatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetu pakkam varatume"/>
</div>
</pre>
