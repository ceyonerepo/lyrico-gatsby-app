---
title: "prema o prema song lyrics"
album: "Bucchinaidu Kandriga"
artist: "Mihiraamsh"
lyricist: "Battu Vijay Kumar"
director: "Krishna Poluru"
path: "/albums/bucchinaidu-kandriga-lyrics"
song: "Prema o Prema"
image: ../../images/albumart/bucchinaidu-kandriga.jpg
date: 2020-08-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rfDNZWNBIpY"
type: "sad"
singers:
  - Adheef Muhamed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Prema O prema prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema O prema prema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mosam chesavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosam chesavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Preme ledantu prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme ledantu prema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pranam theesavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam theesavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maate oka maate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate oka maate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chura kathai yeda kosindey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chura kathai yeda kosindey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paluke aa paluke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paluke aa paluke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanu champe visham ayyinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu champe visham ayyinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa neram ente prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa neram ente prema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Premincha pranam ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premincha pranam ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneeti lokam naku andhinchake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeti lokam naku andhinchake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entho preme chupinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho preme chupinchave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha oo maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha oo maaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anni nene antu naake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni nene antu naake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Droham chesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Droham chesave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa Aa Aa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa Aa Aa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nidure raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidure raani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matthu varamai poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthu varamai poni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallo kuda ninnu marichentha laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallo kuda ninnu marichentha laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dooram poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram poni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam daate poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam daate poni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuve leni lokam cherentha ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuve leni lokam cherentha ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi mosam chesaventey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi mosam chesaventey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idena premante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idena premante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emyna kani vadile ponu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emyna kani vadile ponu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annavu emainde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annavu emainde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entho preme chupinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho preme chupinchave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha oo maaye…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha oo maaye…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anni nene antu naake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni nene antu naake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Droham chesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Droham chesave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maate oka maate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate oka maate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chura kathai yeda kosindey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chura kathai yeda kosindey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paluke aa paluke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paluke aa paluke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanu champe visham ayyinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu champe visham ayyinde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu naade podiche velu naade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu naade podiche velu naade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badha naade niku cheppedela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badha naade niku cheppedela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naade swaaradam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naade swaaradam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naku nuve mukyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naku nuve mukyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lede mosam antha vidhi raatane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lede mosam antha vidhi raatane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kosam ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kosam ey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee dooram ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dooram ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappantha nadega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappantha nadega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee papam antha nade kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee papam antha nade kada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veelunte manninchava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelunte manninchava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entho preme neepai undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho preme neepai undi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheppe veelledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe veelledhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anni nuvai untunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni nuvai untunnanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chere dhaaredi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chere dhaaredi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maate oka maate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate oka maate"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chura kathai yeda kosindey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chura kathai yeda kosindey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paluke aa paluke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paluke aa paluke"/>
</div>
</pre>