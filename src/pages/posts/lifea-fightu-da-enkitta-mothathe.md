---
title: "lifea fightu da song lyrics"
album: "Enkitta Mothathe"
artist: "Natarajan Sankaran"
lyricist: "Yugabharathi"
director: "Ramu Chellappa"
path: "/albums/enkitta-mothathe-lyrics"
song: "Lifea Fightu Da"
image: ../../images/albumart/enkitta-mothathe.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iOj1P6Jn4Zg"
type: "happy"
singers:
  - Natraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life'he Fight'u Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life'he Fight'u Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Ok Right'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok Right'u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Weight'u Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Weight'u Da "/>
</div>
<div class="lyrico-lyrics-wrapper">Aavom Height'u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavom Height'u Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai Tholvigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Tholvigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhndhida Veezhndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhndhida Veezhndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyin Sadugudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Sadugudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Aadida Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadida Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pora Paadha Neru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Paadha Neru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Thaandi Yaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Thaandi Yaru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Porom Peru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Porom Peru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Deva Da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deva Da "/>
</div>
<div class="lyrico-lyrics-wrapper">deva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deva Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maela Maela Yeru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maela Maela Yeru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Romba Joru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Romba Joru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Vena Kelu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Vena Kelu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Aagum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Aagum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Nerma Vazhum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Nerma Vazhum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Serum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Serum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Kaayam Aarum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Kaayam Aarum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorey Pesum Paaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Pesum Paaru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Suthum Peru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Suthum Peru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ella Kota Meeru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella Kota Meeru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Velvom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Tholvigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Tholvigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhndhida Veezhndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhndhida Veezhndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyin Sadugudu Aadida Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Sadugudu Aadida Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are You Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are You Ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pota Vedha Molachidhu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pota Vedha Molachidhu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketadhellam Kedachidum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketadhellam Kedachidum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothiramum Purinjidum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothiramum Purinjidum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Lotteriyo Adichidum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotteriyo Adichidum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovam Porandhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam Porandhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyum Thodaradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyum Thodaradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Irudhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Irudhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Vidhiye Kedaiyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Vidhiye Kedaiyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Tholvigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Tholvigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhndhida Veezhndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhndhida Veezhndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyin Sadugudu Aadida Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Sadugudu Aadida Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pora Paadha Neru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Paadha Neru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Numma Thaandi Yaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Numma Thaandi Yaru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanga Porom Peru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanga Porom Peru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vaa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vaa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maela Maela Yeru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maela Maela Yeru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Romba Joru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Romba Joru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara Vena Kelu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara Vena Kelu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda"/>
</div>
</pre>
