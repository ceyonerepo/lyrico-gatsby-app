---
title: "kasi kasi song lyrics"
album: "4 Idiots"
artist: "Jaya surya"
lyricist: "Jaya surya"
director: "S. Satish Kumar"
path: "/albums/4-idiots-lyrics"
song: "Kasi Kasi Sogusula Kaluva"
image: ../../images/albumart/4-idiots.jpg
date: 2019-11-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6yicFBzgmJ4"
type: "love"
singers:
  - Jaya Surya
  - Sirisha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kasi Kasi Sogasula Kaluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Kasi Sogasula Kaluvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasaniki Ravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasaniki Ravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magasiri Egisina Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magasiri Egisina Madhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Hatthuku Povaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Hatthuku Povaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vurakalu Vese Vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vurakalu Vese Vayasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mudipadaneevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mudipadaneevaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede Ee dheham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede Ee dheham"/>
</div>
<div class="lyrico-lyrics-wrapper">Teercheyi Nuvve Naa Dhaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teercheyi Nuvve Naa Dhaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakattu Kunna Daana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakattu Kunna Daana"/>
</div>
<div class="lyrico-lyrics-wrapper">Loguttu Dhochukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loguttu Dhochukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pattu Pattamandhi Eekatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pattu Pattamandhi Eekatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Andala Chandurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andala Chandurudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa konte Kurravada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa konte Kurravada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kanne Pranam Nee Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kanne Pranam Nee Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasi Kasi Sogasula Kaluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Kasi Sogasula Kaluvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasaniki Ravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasaniki Ravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magasiri Egisina Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magasiri Egisina Madhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Hatthuku Povaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Hatthuku Povaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okatava Tharagathi Sarasamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatava Tharagathi Sarasamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendava Tharagathi Muripamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendava Tharagathi Muripamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodava Tharagathi Virahamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodava Tharagathi Virahamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheddam Shrungaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheddam Shrungaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabadu Adugula Thamakamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabadu Adugula Thamakamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapanalu Tharime Maikamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapanalu Tharime Maikamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvulu Kalise Tharunamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvulu Kalise Tharunamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuddham Aa Swargaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuddham Aa Swargaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddu Muripamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu Muripamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarihaddulu Cheripeeyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarihaddulu Cheripeeyyali"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggu Bidiyale O Mulana Daagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggu Bidiyale O Mulana Daagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhe Kaadha Sogasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhe Kaadha Sogasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Kadha Gadusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Kadha Gadusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Asha Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Asha Telusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Docheshave Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Docheshave Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodesthane Pulusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodesthane Pulusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayase Cheddham Karusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase Cheddham Karusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haaa Haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haaa Haaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haaa Haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haaa Haaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasi Kasi Sogasula Kaluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Kasi Sogasula Kaluvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasaniki Ravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasaniki Ravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magasiri Egisina Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magasiri Egisina Madhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Hatthuku Povaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Hatthuku Povaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigibi parugu Pelichindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigibi parugu Pelichindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavula Madhuram Voolikindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavula Madhuram Voolikindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Polasari Sarasam Kosirindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polasari Sarasam Kosirindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapai nenelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapai nenelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula Varnam Merisindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula Varnam Merisindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaha Thaha Varsham Kurisindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaha Thaha Varsham Kurisindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadupari Dwaram Terichindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadupari Dwaram Terichindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave Javaralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave Javaralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katti Kougililo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katti Kougililo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Andham Alisiponee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Andham Alisiponee"/>
</div>
<div class="lyrico-lyrics-wrapper">konte Vidhullo Eka Ekam Ayiponi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konte Vidhullo Eka Ekam Ayiponi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sare Padha Kaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sare Padha Kaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Raave cheli Vodiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raave cheli Vodiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chustha ruchi koriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chustha ruchi koriki"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasampadhe Geliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasampadhe Geliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Istha padha Thudaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istha padha Thudaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhe kadha Manaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhe kadha Manaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasi Kasi Sogasula Kaluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Kasi Sogasula Kaluvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasaniki Ravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasaniki Ravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magasiri Egisina Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magasiri Egisina Madhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Hatthuku Povaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Hatthuku Povaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vurakalu Vese Vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vurakalu Vese Vayasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mudipadaneevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mudipadaneevaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede Ee dheham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede Ee dheham"/>
</div>
<div class="lyrico-lyrics-wrapper">Teercheyi Nuvve Naa Dhaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teercheyi Nuvve Naa Dhaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakattu Kunna Daana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakattu Kunna Daana"/>
</div>
<div class="lyrico-lyrics-wrapper">Loguttu Dhochukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loguttu Dhochukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Pattu Pattamandhi Eekatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Pattu Pattamandhi Eekatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Andala Chandurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andala Chandurudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa konte Kurravada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa konte Kurravada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kanne Pranam Nee Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kanne Pranam Nee Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasi Kasi Sogasula Kaluvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Kasi Sogasula Kaluvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasaniki Ravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasaniki Ravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magasiri Egisina Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magasiri Egisina Madhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Hattuku Povaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Hattuku Povaa"/>
</div>
</pre>
