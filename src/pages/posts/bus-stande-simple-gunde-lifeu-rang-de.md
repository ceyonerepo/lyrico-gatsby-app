---
title: "bus stande song lyrics"
album: "Rang De"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/rang-de-lyrics"
song: "Bus Stande - Simple gunde life u"
image: ../../images/albumart/rang-de.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rPsPeWTMk2A"
type: "happy"
singers:
  - Sagar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simple gunde life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simple gunde life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Temple run la maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Temple run la maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rangu rangu lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rangu rangu lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekatloki jaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekatloki jaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovely gunde kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovely gunde kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Life ye lenidhi aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life ye lenidhi aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Smiley lanti face ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smiley lanti face ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Smile ye lenidhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smile ye lenidhaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neellu leni bhavilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neellu leni bhavilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappalaga thelipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappalaga thelipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaalaredho gaalamesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalaredho gaalamesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepa laaga dhorikipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepa laaga dhorikipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theesukunna goyyi lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesukunna goyyi lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu kaastha jaaripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu kaastha jaaripoye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond ye abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond ye abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhossham abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhossham abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond ye abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond ye abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhossham abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhossham abscond ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simple gunde life-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simple gunde life-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Temple run la maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Temple run la maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rangu rangu lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee rangu rangu lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatloki jaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatloki jaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sala sala kaagu neetlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala sala kaagu neetlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Velley pettinaanuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velley pettinaanuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaramantukunna chettho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaramantukunna chettho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle nalipinaanuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle nalipinaanuro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evaru leni chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru leni chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaavu kekaindhi life ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaavu kekaindhi life ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend la unde fate ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend la unde fate ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Foot ball aade naathote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foot ball aade naathote"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond ye abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond ye abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhossham abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhossham abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond ye abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond ye abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhossham abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhossham abscond ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Slate ye pakkanuntadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slate ye pakkanuntadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani chalkpiece chikkanantadhe ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani chalkpiece chikkanantadhe ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Plate lo food untadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plate lo food untadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani notiki thaalamuntadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani notiki thaalamuntadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Light swith eyyagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light swith eyyagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulb maadipoyinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulb maadipoyinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Life start avvagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life start avvagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa future puncture ayyene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa future puncture ayyene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Bus stand ye bus stand ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bus stand ye bus stand ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika bathuke bus stande ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika bathuke bus stande ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond ye abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond ye abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhossham abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhossham abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond ye abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond ye abscond ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhossham abscond ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhossham abscond ye"/>
</div>
</pre>
