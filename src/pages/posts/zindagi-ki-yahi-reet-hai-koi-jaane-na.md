---
title: "zindagi ki yahi reet hai song lyrics"
album: "Koi Jaane Na"
artist: "Rochak Kohli"
lyricist: "Manoj Muntashir"
director: "Amin Hajee"
path: "/albums/koi-jaane-na-lyrics"
song: "Zindagi Ki Yahi Reet Hai"
image: ../../images/albumart/koi-jaane-na.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/BqO6EOIiYrk"
type: "happy"
singers:
  - Rochak Kohli
  - Soumitra Dev Burman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thodi baarish ho gayi to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodi baarish ho gayi to"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasmaan dhul gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasmaan dhul gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadalon ke bich koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadalon ke bich koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasta khul gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasta khul gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rone se kabhi darna tu nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rone se kabhi darna tu nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunguna jo tera geet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunguna jo tera geet hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi ki yahi reet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ki yahi reet hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haar ke baad hi jeet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haar ke baad hi jeet hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi ki yehi reet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ki yehi reet hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haar ke baad hi jeet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haar ke baad hi jeet hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh hawa to chal rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh hawa to chal rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Par iske paanv hai kahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par iske paanv hai kahaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho.. Hmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho.. Hmm.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh hawa to chal rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh hawa to chal rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Par iske paanv hai kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par iske paanv hai kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Machliyan ghar ja rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machliyan ghar ja rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inke gaanv hai kahan haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inke gaanv hai kahan haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh jo paheli badi albeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh jo paheli badi albeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo milke suljhayein hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo milke suljhayein hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship woh ship hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship woh ship hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo doobe kabhi na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo doobe kabhi na"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo dost ban jaayein hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo dost ban jaayein hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho sang tere kehele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho sang tere kehele"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo chhode na akele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo chhode na akele"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahin to tera meet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahin to tera meet hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi ki yahi reet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi ki yahi reet hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haar ke baad hi jeet hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haar ke baad hi jeet hai"/>
</div>
</pre>
