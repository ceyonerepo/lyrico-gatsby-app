---
title: "karichaan kuyile song lyrics"
album: "Sarbath"
artist: "Ajesh"
lyricist: "Muthamil"
director: "Prabhakaran"
path: "/albums/sarbath-song-lyrics"
song: "Karichaan Kuyile"
image: ../../images/albumart/sarbath.jpg
date: 2021-04-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-B43sHnsQBA"
type: "Love"
singers:
  - Ajesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karichaan Kuyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karichaan Kuyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Mayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Mayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavunthen Aala Madakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavunthen Aala Madakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Ava Maayamakkurale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Ava Maayamakkurale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vericha Veyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vericha Veyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha Puyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Puyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevanthen Moolaikulla Thedi Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevanthen Moolaikulla Thedi Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thaalam Podurale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thaalam Podurale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalaa Ava Enakkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalaa Ava Enakkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedachale Kanakkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedachale Kanakkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Orakkam Kalaikkum Sogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orakkam Kalaikkum Sogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Thaanamaga Kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Thaanamaga Kodutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nazhuvatha Nangooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhuvatha Nangooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Nenjil Vethachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Nenjil Vethachale"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya Muzhusa Ava Thaan Arasaluraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Muzhusa Ava Thaan Arasaluraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Paarvaiyala Eettiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Paarvaiyala Eettiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Meratturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Meratturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagaatha Bodhai Ootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagaatha Bodhai Ootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Aala Mayakkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Aala Mayakkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kaaramaga Kooru Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kaaramaga Kooru Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kodukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kodukkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aazham Poyi Aarapodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aazham Poyi Aarapodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marundha Inikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marundha Inikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kattadha Kaalai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kattadha Kaalai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Gambeerama Thaan Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Gambeerama Thaan Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannala Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannala Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Saikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Saikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Sollatha Aasai Thoonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Sollatha Aasai Thoonga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sellatha Kaasa Yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sellatha Kaasa Yenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Unnoda Oosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Unnoda Oosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu Kettu Muzhikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Kettu Muzhikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalam Pottu Naanum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalam Pottu Naanum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimaara Paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimaara Paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi Pottu Odachaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi Pottu Odachaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Maalai Thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Maalai Thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kana Neram Thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana Neram Thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalachaye Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalachaye Kanave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thera Pottu Neeyum Theda Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thera Pottu Neeyum Theda Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Oliyiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Oliyiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravaga Naanum Onju Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaga Naanum Onju Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham Kodukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham Kodukkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigara Mulla Pola Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigara Mulla Pola Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatti Sozhatturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatti Sozhatturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadivaalam Maatti Paadhai Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadivaalam Maatti Paadhai Kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Ennai Ennai Azhaikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ennai Ennai Azhaikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Paarvaiyala Eettiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Paarvaiyala Eettiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Meratturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Meratturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagaatha Bodhai Ootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagaatha Bodhai Ootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Aala Mayakkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Aala Mayakkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kaaramaga Kooru Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kaaramaga Kooru Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Kodukkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kodukkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Aazham Poyi Aarapodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aazham Poyi Aarapodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marundha Inikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marundha Inikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye…"/>
</div>
</pre>
