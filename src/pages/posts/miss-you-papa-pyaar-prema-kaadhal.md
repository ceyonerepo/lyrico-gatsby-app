---
title: "miss you papa song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Elan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Miss You Papa"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7maHGCBUeOQ"
type: "sad"
singers:
  - Yuvan Shankar Raja
  - Priya Mali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai kondu selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai kondu selgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tholaivil irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tholaivil irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollada kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollada kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum enna kaadhalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum enna kaadhalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal kalaya kandenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal kalaya kandenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
</pre>
