---
title: "mangalyam lyrics"
album: "Eeswaran"
artist: "Thaman S"
lyricist: "Yugabharathi"
director: "Susienthiran"
path: "/albums/eeswaran-song-lyrics"
song: "Mangalyam"
image: ../../images/albumart/eeswaran.jpg
date: 2021-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r3bngwZsfMI"
type: "Love"
singers:
  - Silambarasan TR
  - Roshini JKV
  - Thaman S
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chellakutty rasathi porathenna soodethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutty rasathi porathenna soodethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne un kadhal kadhava vaikkatha saathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne un kadhal kadhava vaikkatha saathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaikatti nee aathi vekkamennu yemathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaikatti nee aathi vekkamennu yemathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti pogathadi ennai mallathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti pogathadi ennai mallathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan nenjukulla thottil katti vechen kaapathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan nenjukulla thottil katti vechen kaapathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi kotti kedakkuthu azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kotti kedakkuthu azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kooda vanthu kunjom pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kooda vanthu kunjom pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanne ennai karaiyil yethum padagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanne ennai karaiyil yethum padagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai koththa ninaikkuthu kazhugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai koththa ninaikkuthu kazhugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meni engum innum mizhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meni engum innum mizhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaattarayum adakki aalum madhugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaattarayum adakki aalum madhugu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondi veeran naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondi veeran naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakketha aalum thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakketha aalum thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pattu pattu kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pattu pattu kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu muththam veppen paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu muththam veppen paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrivelum naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivelum naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli vesham poda maatendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli vesham poda maatendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aththai aththai peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aththai aththai peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthirai chinnamtha minja yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthirai chinnamtha minja yaaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podra..Podra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podra..Podra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mangalyam thanthunanena mama jeevana hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mangalyam thanthunanena mama jeevana hetuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangalyam thanthunanena mama jeevana hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalyam thanthunanena mama jeevana hetuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangalyam hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalyam hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthunane hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthunane hetuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mangalyam thanthunanena mama jeevana hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mangalyam thanthunanena mama jeevana hetuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellakutty rasathi poga matten soodethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutty rasathi poga matten soodethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnala naanum nadaiya vechene maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala naanum nadaiya vechene maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaikatti nee aathi vedala ponna yemathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaikatti nee aathi vedala ponna yemathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu pogatha un anba kaapathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu pogatha un anba kaapathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuril oonjal katti aada vitten sollama nethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuril oonjal katti aada vitten sollama nethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai alli anaikku viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai alli anaikku viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perai solla mattum thaane kuralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perai solla mattum thaane kuralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kadhal ennum kadavulo arulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kadhal ennum kadavulo arulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thottu thodanguthu pagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thottu thodanguthu pagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechil saaral adikkuthu veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechil saaral adikkuthu veyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu patta kaana pogum puyalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu patta kaana pogum puyalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ondi veeran naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondi veeran naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakketha aalum thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakketha aalum thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pattu pattu kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pattu pattu kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu muththam veppen paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu muththam veppen paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetrivelum naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrivelum naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli vesham poda maatendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli vesham poda maatendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aththai aththai peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aththai aththai peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthirai chinnamtha minja yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthirai chinnamtha minja yaaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podra..Podra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podra..Podra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mangalyam thanthunanena mama jeevana hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mangalyam thanthunanena mama jeevana hetuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangalyam thanthunanena mama jeevana hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalyam thanthunanena mama jeevana hetuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangalyam hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalyam hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthunane hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthunane hetuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mangalyam thanthunanena mama jeevana hetuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mangalyam thanthunanena mama jeevana hetuna"/>
</div>
</pre>
