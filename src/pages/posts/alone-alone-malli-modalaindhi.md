---
title: "alone alone song lyrics"
album: "Malli Modalaindhi"
artist: "Anup Rubens"
lyricist: "Krishna Chaitanya"
director: "TG Keerthi Kumar"
path: "/albums/malli-modalaindhi-lyrics"
song: "Alone Alone"
image: ../../images/albumart/malli-modalaindhi.jpg
date: 2022-02-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/PzeikhzzozE"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanulaku theliyani o kalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaku theliyani o kalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Velipoyaave nuvvu ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velipoyaave nuvvu ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Migila nene o silala Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migila nene o silala Alone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alone Alone Alone Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alone Alone Alone Alone"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m so lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m so lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Alone Alone Alone Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alone Alone Alone Alone"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m so lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m so lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaa yi ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaa yi ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanulaku theliyani o kalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaku theliyani o kalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Velipoyaave nuvvu ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velipoyaave nuvvu ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Migila nene o silala Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migila nene o silala Alone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidi vidi adugulu Padenu ela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi vidi adugulu Padenu ela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavani jantala o kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavani jantala o kadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontari manasulu o vyadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontari manasulu o vyadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Alonee oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alonee oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vadhilellipoke nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vadhilellipoke nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulukolene ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulukolene ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana gathamulone unnaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana gathamulone unnaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">O cheli o cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O cheli o cheli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alone Alone Alone Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alone Alone Alone Alone"/>
</div>
<div class="lyrico-lyrics-wrapper">Alone Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alone Alone"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m lonely"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m so lonely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m so lonely"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalachaava cheli nuvvu ala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalachaava cheli nuvvu ala"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamaarindhe endhukalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamaarindhe endhukalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Verevaroo naakemi ilaa Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verevaroo naakemi ilaa Alone"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvulake sankella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvulake sankella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellipoyaave priyuraala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellipoyaave priyuraala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathame nuvvani maravaala Alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathame nuvvani maravaala Alone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vadhilellipoke nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vadhilellipoke nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulukolene ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulukolene ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana gathamulone unnaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana gathamulone unnaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">O cheli o cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O cheli o cheli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vadhilellipoke nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vadhilellipoke nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhulukolene ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhulukolene ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana gathamulone unnaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana gathamulone unnaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">O cheli o cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O cheli o cheli"/>
</div>
</pre>
