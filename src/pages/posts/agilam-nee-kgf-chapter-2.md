---
title: "agilam nee song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Madhurakavi"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Agilam Nee"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iqa93lGILts"
type: "melody"
singers:
  - Ananya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Agilam nee mugilum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agilam nee mugilum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigaram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigaram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhidum agadhigal sirippu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhidum agadhigal sirippu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamum nee nizhalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamum nee nizhalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulir vidum urimaigal puratchi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulir vidum urimaigal puratchi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varumai nilaththil valarum vidhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumai nilaththil valarum vidhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyum virutchcham unakkul iniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyum virutchcham unakkul iniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkum asura alaigal noorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkum asura alaigal noorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaduppai udaikkum nayagan edhirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppai udaikkum nayagan edhirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarilae naalaiya saridham nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarilae naalaiya saridham nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarilae naalaiya saridham nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarilae naalaiya saridham nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram padaigalum murasudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram padaigalum murasudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhangiye varattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhangiye varattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvanai vellada unakku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvanai vellada unakku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham ulagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayutham ulagilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthaane naane thaaninthathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaane naane thaaninthathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane naane no
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane naane no"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaane naane thaaninthathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaane naane thaaninthathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane naane no
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane naane no"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthaane naane thaaninthathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaane naane thaaninthathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane naane no
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane naane no"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaane naane thaaninthathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaane naane thaaninthathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane naane no
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane naane no"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaa"/>
</div>
</pre>
