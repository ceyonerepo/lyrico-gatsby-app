---
title: "irul vaanam song lyrics"
album: "Irandam Ulagaporin Kadaisi Gundu"
artist: "Tenma"
lyricist: "Umadevi"
director: "Athiyan Athirai"
path: "/albums/irandam-ulagaporin-kadaisi-gundu-lyrics"
song: "Irul Vaanam"
image: ../../images/albumart/irandam-ulagaporin-kadaisi-gundu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UKA0V5ofIEs"
type: "love"
singers:
  - Subiksha Rangarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Irul Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Aagidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Karukkinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Karukkinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaruthu Yugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu Yugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inba Karuppugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inba Karuppugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimayin Mayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimayin Mayamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Emmaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malayurum Nadhiyinai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayurum Nadhiyinai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Adainthaen Kadalai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adainthaen Kadalai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivanin Nizhalinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivanin Nizhalinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarum Kuvalai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarum Kuvalai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Pothuvaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Pothuvaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamai Kaakum Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamai Kaakum Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Manam Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Manam Maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaivaai Varuvaai Kuzhaivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaivaai Varuvaai Kuzhaivaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Mm Hmm Mmm Mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Mm Hmm Mmm Mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Mm Mm Hmm Mmm Mm Mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Mm Mm Hmm Mmm Mm Mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pozhinthidum Nilavinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhinthidum Nilavinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanainthaen Arugil Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanainthaen Arugil Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhindhidum Ozhiyinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhindhidum Ozhiyinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivom Urugi Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivom Urugi Naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Naamai Thaettrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Naamai Thaettrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madai Maatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madai Maatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vaazhkai Puyal Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vaazhkai Puyal Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Maattrum Thunaiyaai Inaiyaai Varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Maattrum Thunaiyaai Inaiyaai Varuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Aagidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravugal Milirgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Milirgirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Uravil Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Uravil Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathavugal Thirakkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavugal Thirakkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inainthaal Idhayam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inainthaal Idhayam Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaedal Niraiverum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedal Niraiverum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Thozhil Malai Sainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Thozhil Malai Sainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaipaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaipaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adalaerae Nee Varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adalaerae Nee Varuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irul Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Kaattidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Kaattidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Aagidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Karukkinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Karukkinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaruthu Yugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu Yugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inba Karuppugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inba Karuppugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimayin Mayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimayin Mayamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Emmaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmaanae"/>
</div>
</pre>
