---
title: "maata ka jaagrata song lyrics"
album: "Laxmii"
artist: "Tanishk Bagchi"
lyricist: "Farhad Samji"
director: "Raghava Lawrence"
path: "/albums/laxmii-lyrics"
song: "Maata Ka Jaagrata"
image: ../../images/albumart/laxmii.jpg
date: 2020-11-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/8tWxKA_Z2-o"
type: "happy"
singers:
  - Farhad Samji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tune Change Hai Mama!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune Change Hai Mama!"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho! Jai Ho!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho! Jai Ho!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Uth Uth Uth Uth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uth Uth Uth Uth"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Uth..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Uth.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uth Jagrata Ki Raat Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uth Jagrata Ki Raat Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mata Ka Saath Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mata Ka Saath Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sherawali Ke Darbaar Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sherawali Ke Darbaar Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo Bhi Jyoti Jalayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Bhi Jyoti Jalayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Mata Di...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Mata Di..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhakti Mein Magan Hokar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakti Mein Magan Hokar"/>
</div>
<div class="lyrico-lyrics-wrapper">Laggn Se Jo Gaaye Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laggn Se Jo Gaaye Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mata Ki Charno Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mata Ki Charno Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo Bhi Shir Jhukaye Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Bhi Shir Jhukaye Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayega... Aayega...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayega... Aayega..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ujala Wo Deti Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ujala Wo Deti Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirala Khel Khelti Hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirala Khel Khelti Hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Dukho Ke Bojh Ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dukho Ke Bojh Ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kinara Wo Karti Hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kinara Wo Karti Hain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyu...?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyu...?"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyu Ki...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyu Ki..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo Mata Ka Vardaan Mange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Mata Ka Vardaan Mange"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaha Se Leke Jaayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaha Se Leke Jaayega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Usska Time Aayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usska Time Aayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Mata Di...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Mata Di..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigdi Banane Wali Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigdi Banane Wali Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali Durga Bhawani Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Durga Bhawani Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Sab Ki Palanhari Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Sab Ki Palanhari Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Janni Jagat Jalalli Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janni Jagat Jalalli Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Tere Dham De Aage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tere Dham De Aage"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Dham Du
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Dham Du"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaha Meri Shridha Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaha Meri Shridha Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri Tu Pooja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri Tu Pooja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kala Kala Kala Kala...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Kala Kala Kala..."/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Kala Kala Chasma Duniya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Kala Kala Chasma Duniya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttar Ke Mathe Charna Ch Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttar Ke Mathe Charna Ch Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kala Kala Chasma Duniya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Kala Chasma Duniya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttar Ke Mathe Charna Ch Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttar Ke Mathe Charna Ch Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Kala Chasma Duniya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Kala Chasma Duniya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttar Ke Mathe Charna Ch Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttar Ke Mathe Charna Ch Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oye Kala Kala Chasma Duniya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Kala Kala Chasma Duniya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttar Te Mathe Charna Ch Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttar Te Mathe Charna Ch Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Hoooo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Hoooo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Jai Mata Jai Jai Mata Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Jai Mata Jai Jai Mata Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main Nikla O Gaddi Le Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Nikla O Gaddi Le Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">O Main Nikla O Gaddi Le Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Main Nikla O Gaddi Le Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Noto Ki Wo Tappi Le Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noto Ki Wo Tappi Le Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Ke Charno Mein De Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Ke Charno Mein De Aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Moh Maya Chod Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Moh Maya Chod Aaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa Ke Charno Mein De Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Ke Charno Mein De Aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Moh Maya Chod Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Moh Maya Chod Aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Mata Di...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Mata Di..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mata Ki Sewa Mein Jo Khoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mata Ki Sewa Mein Jo Khoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Usne Sab Paya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne Sab Paya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Moh Maya Chod Aaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Moh Maya Chod Aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Hoooo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Hoooo..."/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Jai Mata Jai Jai Mata Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Jai Mata Jai Jai Mata Di"/>
</div>
</pre>
