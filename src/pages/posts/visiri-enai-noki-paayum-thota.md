---
title: "visiri song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Visiri"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N5pP7k7qppE"
type: "love"
singers:
  - Sid Sriram
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rappap paa pare paparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappap paa pare paparaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappap paa pare paparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappap paa pare paparaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappap paa pare paparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappap paa pare paparaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rappap paa pare paparaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rappap paa pare paparaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethuvarai pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvarai pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru nee sollavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru nee sollavendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endruthaan vidamal ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endruthaan vidamal ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaen muthangal mattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen muthangal mattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum yendru solvadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum yendru solvadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaamal pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaamal pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar yaaro kanaakkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaaro kanaakkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum nee sendru ulaavukindraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum nee sendru ulaavukindraval"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaanum kanaakkalil varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaanum kanaakkalil varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Or aanendraal naandhaan ennaalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or aanendraal naandhaan ennaalilum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongaatrae nee veesaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatrae nee veesaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongaatrae nee veesaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatrae nee veesaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan dhaan ingae visiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dhaan ingae visiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En veetlil nee nirkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veetlil nee nirkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai nambaamal ennai killikonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai nambaamal ennai killikonden"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottathil nee nirkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathil nee nirkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai poovendru enni koiyyachendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai poovendru enni koiyyachendren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pugazh poomaalaigal thaen solaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazh poomaalaigal thaen solaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanden yen un pin vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanden yen un pin vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum kasolaigal ponmaalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum kasolaigal ponmaalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae nee vendumendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae nee vendumendren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrodu en vegangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrodu en vegangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirutheeyaga maari thoonga kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirutheeyaga maari thoonga kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrodu en kobangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katrodu en kobangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thoosaaga maari pogakanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thoosaaga maari pogakanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai paarkatha naan pesatha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai paarkatha naan pesatha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">En vazhvil nee naanendru naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvil nee naanendru naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam nee vandhathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam nee vandhathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol thanthathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol thanthathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanen naan aannadha pen dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen naan aannadha pen dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho hooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho hooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvarai pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvarai pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru nee solla vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru nee solla vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endruthaan vidamal ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endruthaan vidamal ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaen muthangal mattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen muthangal mattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum endru solvathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum endru solvathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaamal pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaamal pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pondra ilainganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pondra ilainganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam erkamal marupadhae pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam erkamal marupadhae pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanden un alaadhi thooimaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanden un alaadhi thooimaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanpaarthu pesum peraanmaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanpaarthu pesum peraanmaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongaatrae nee veesaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatrae nee veesaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongaatrae nee veesaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongaatrae nee veesaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan dhaan ingae visiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan dhaan ingae visiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ruth thu thu ruth thu ru ruuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruth thu thu ruth thu ru ruuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ruth thu thu ruth thu ru ruuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruth thu thu ruth thu ru ruuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ruth thu thu ruth thu ru ruuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruth thu thu ruth thu ru ruuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ruth thu thu ruth thu ru ruuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ruth thu thu ruth thu ru ruuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee yeeee yeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee yeeee yeee"/>
</div>
</pre>
