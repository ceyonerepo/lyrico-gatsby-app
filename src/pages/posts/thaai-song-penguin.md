---
title: "thaai song lyrics"
album: "penguin"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Eashvar Karthic"
path: "/albums/penguin-lyrics"
song: "Thaai song - Meththayai un vaiyurum"
image: ../../images/albumart/penguin.jpg
date: 2020-06-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0bXWShyblBI"
type: "Amma Song"
singers:
  - Anand Aravindakshan
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meththayai un vaiyurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththayai un vaiyurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththamai un uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamai un uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththamai en uthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththamai en uthiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththamum nee koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththamum nee koduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhiyil thudithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhiyil thudithal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyum kuduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyum kuduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhiyil thudithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhiyil thudithal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyum kuduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyum kuduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Garbamai enai sumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garbamai enai sumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiye unavalanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiye unavalanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Arpamai kidantha ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arpamai kidantha ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirppamai mudithu vaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirppamai mudithu vaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan payila nee sirithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan payila nee sirithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan muyala nee viyarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan muyala nee viyarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thuyala nee isaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thuyala nee isaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan uyara nee vizhithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan uyara nee vizhithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhiyil thudithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhiyil thudithal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyum kuduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyum kuduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottilil naan azhuthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottilil naan azhuthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli vantha kadhalithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli vantha kadhalithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vantha katterumbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vantha katterumbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti vaithu nee udhaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vaithu nee udhaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkul ennai vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul ennai vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan imaiyil kaaval vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan imaiyil kaaval vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaroo kutty pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaroo kutty pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu otti vaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu otti vaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oottri sutta thosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oottri sutta thosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadiyo nei manakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyo nei manakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ootri suttathal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ootri suttathal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir varai athu inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir varai athu inikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhiyil thudithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhiyil thudithal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyum kuduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyum kuduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meththayai un vaiyurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththayai un vaiyurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththamai un uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamai un uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththamai en uthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththamai en uthiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththamum nee koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththamum nee koduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un uyiril paathi vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uyiril paathi vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiril serthu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiril serthu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathu paasam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathu paasam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkai kotti vaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkai kotti vaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam ondru enakku irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam ondru enakku irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha udan kan vizhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha udan kan vizhippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai aaga nee illai endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai aaga nee illai endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayangamal uyir thurappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangamal uyir thurappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yugam pala aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugam pala aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam kaana thavam iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam kaana thavam iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un garbam serum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un garbam serum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakkamal kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkamal kaathiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhiyil thudithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhiyil thudithal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyum kuduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyum kuduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathum koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathum koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vazhiyil thudithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vazhiyil thudithal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un madiyum kuduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un madiyum kuduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meththayai un vaiyurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meththayai un vaiyurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththamai un uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamai un uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththamai en uthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththamai en uthiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththamum nee koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththamum nee koduthai"/>
</div>
</pre>
