---
title: "rajanna ninnapagalara song lyrics"
album: "Yatra"
artist: "K"
lyricist: "Sirivennela Sitaramasastri"
director: "Mahi V Raghav"
path: "/albums/yatra-lyrics"
song: "Rajanna Ninnapagalara"
image: ../../images/albumart/yatra.jpg
date: 2019-02-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xzZCk4G80oQ"
type: "happy"
singers:
  - Vandemataram Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kotalo Koluvayye Redu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotalo Koluvayye Redu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peta Daare Pattinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peta Daare Pattinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha Raathalannee Maripoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha Raathalannee Maripoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasalanno Techinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasalanno Techinadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorle Yeralle Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorle Yeralle Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Horu Horuna Uppongera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horu Horuna Uppongera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorle Yeralle Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorle Yeralle Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Horu Horuna Uppongera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horu Horuna Uppongera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daarle Thaarangavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarle Thaarangavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethulethi Jai Kottero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethulethi Jai Kottero"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarle Thaarangavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarle Thaarangavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethulethi Jai Kottero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethulethi Jai Kottero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandipoye Badugolla Thalupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandipoye Badugolla Thalupai"/>
</div>
<div class="lyrico-lyrics-wrapper">Godugu Needai Vacchadayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godugu Needai Vacchadayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandi Kosame Thaanunnanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandi Kosame Thaanunnanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatiche Maarajayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatiche Maarajayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorle Yeralle Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorle Yeralle Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Horu Horuna Uppongera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horu Horuna Uppongera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorle Yeralle Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorle Yeralle Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Horu Horuna Uppongera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horu Horuna Uppongera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Methukunu Eragani Bathukula Moravini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methukunu Eragani Bathukula Moravini"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeti Mabbula Karigaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti Mabbula Karigaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beetanu Vicchina Beellanu Thadipina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beetanu Vicchina Beellanu Thadipina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaana Jallulaa Kurisaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana Jallulaa Kurisaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usurumantu Nitturpu Seppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurumantu Nitturpu Seppe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosulanni Vinnaduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosulanni Vinnaduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lenodi Gadapake Ainodilagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lenodi Gadapake Ainodilagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaraa Thaanannaadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaraa Thaanannaadura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raithana Senuki Sankrathi Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raithana Senuki Sankrathi Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtala Padavaki Sukkanigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtala Padavaki Sukkanigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Adugu Padithe Maa Vaipila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Adugu Padithe Maa Vaipila"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajanna Ninnapagalara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajanna Ninnapagalara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorle Yeralle Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorle Yeralle Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Horu Horuna Uppongera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horu Horuna Uppongera"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarle Thaarangavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarle Thaarangavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethulethi Jai Kottero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethulethi Jai Kottero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandipoye Badugolla Thalupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandipoye Badugolla Thalupai"/>
</div>
<div class="lyrico-lyrics-wrapper">Godugu Needai Vacchadayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godugu Needai Vacchadayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandi Kosame Thaanunnanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandi Kosame Thaanunnanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatiche Maarajayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatiche Maarajayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorle Yeralle Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorle Yeralle Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Horu Horuna Uppongera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horu Horuna Uppongera"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarle Thaarangavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarle Thaarangavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethulethi Jai Kottero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethulethi Jai Kottero"/>
</div>
</pre>
