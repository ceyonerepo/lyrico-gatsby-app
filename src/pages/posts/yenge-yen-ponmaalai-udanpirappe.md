---
title: "yenge yen ponmaalai song lyrics"
album: "Udanpirappe"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Era. Saravanan"
path: "/albums/udanpirappe-lyrics"
song: "Yenge Yen Ponmaalai"
image: ../../images/albumart/udanpirappe.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Vj5j2Orqupw"
type: "sad"
singers:
  - Arya Dhayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaana sainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaana sainthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholainthu ponathu porul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholainthu ponathu porul"/>
</div>
<div class="lyrico-lyrics-wrapper">endral athai thirumba peruvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endral athai thirumba peruvene"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai vangidum vidhiyinal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai vangidum vidhiyinal "/>
</div>
<div class="lyrico-lyrics-wrapper">nalla uravai ilanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla uravai ilanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavai kalaithathu ore sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavai kalaithathu ore sol"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyai udaithathum ore sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyai udaithathum ore sol"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaiyai valarthathu ore sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaiyai valarthathu ore sol"/>
</div>
<div class="lyrico-lyrics-wrapper">paliyum ethanal sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paliyum ethanal sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaana sainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaana sainthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paasa veru pootha poo naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasa veru pootha poo naan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam sernthe valthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam sernthe valthene"/>
</div>
<div class="lyrico-lyrics-wrapper">en veli kaathan soorai aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veli kaathan soorai aada"/>
</div>
<div class="lyrico-lyrics-wrapper">eeram paithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeram paithene"/>
</div>
<div class="lyrico-lyrics-wrapper">uravenum thari aruntha pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravenum thari aruntha pin"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirin ootamum thenguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin ootamum thenguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyume payam thodarvathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyume payam thodarvathal"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam moolaiyil sainthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam moolaiyil sainthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">manathile vali kooduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathile vali kooduma"/>
</div>
<div class="lyrico-lyrics-wrapper">maraname pathil aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maraname pathil aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">ithe nilai thodaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithe nilai thodaruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi ponene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholainthu ponathu porul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholainthu ponathu porul"/>
</div>
<div class="lyrico-lyrics-wrapper">endral athai thirumba peruvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endral athai thirumba peruvene"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai vangidum vidhiyinal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai vangidum vidhiyinal "/>
</div>
<div class="lyrico-lyrics-wrapper">nalla uravai ilanthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla uravai ilanthene"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavai kalaithathu ore sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavai kalaithathu ore sol"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyai udaithathum ore sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyai udaithathum ore sol"/>
</div>
<div class="lyrico-lyrics-wrapper">pagaiyai valarthathu ore sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaiyai valarthathu ore sol"/>
</div>
<div class="lyrico-lyrics-wrapper">paliyum ethanal sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paliyum ethanal sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">yenge en ponmaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenge en ponmaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaana sainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaana sainthene"/>
</div>
</pre>
