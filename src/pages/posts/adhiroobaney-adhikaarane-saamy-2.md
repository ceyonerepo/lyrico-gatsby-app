---
title: "adhiroobaney adhikaarane song lyrics"
album: "Saamy 2"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/saamy-2-song-lyrics"
song: "Adhiroobaney Adhikaarane"
image: ../../images/albumart/saamy-2.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6-Gh_TTImm4"
type: "Love"
singers:
  - M. M. Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiroobanae…adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae…adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae…adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae…adhikaaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorai kaatrena vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorai kaatrena vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En suthanthiram meendum thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En suthanthiram meendum thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamalaiyaagi nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamalaiyaagi nindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadha yaanai polae vendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadha yaanai polae vendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhiroobanae…adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae…adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae…adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae…adhikaaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paththu viralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paththu viralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayudham…oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayudham…oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarkumbothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarkumbothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam varum…ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam varum…ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan aanin ilakkanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan aanin ilakkanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thunaiyaai nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thunaiyaai nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varume thalaikanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varume thalaikanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiroobanae…adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae…adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae…adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae…adhikaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyai nee vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyai nee vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kudaiyodu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kudaiyodu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyaga nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyaga nindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan veyilaagi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan veyilaagi vanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uraiyaada munbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyaada munbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi yethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi yethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En nizhal kooda indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nizhal kooda indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkum un sollai…ee…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkum un sollai…ee….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaatril yetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaatril yetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanthathu…ooo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanthathu…ooo…."/>
</div>
<div class="lyrico-lyrics-wrapper">En kannil yetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannil yetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhaithathu…oooo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhaithathu…oooo…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnal naanum kuzhambinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal naanum kuzhambinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kuzhappam theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kuzhappam theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudathendrum virumbinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudathendrum virumbinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiroobanae…adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae…adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae…adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae…adhikaaranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathodu yetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathodu yetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Melithana matram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melithana matram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisai maarum nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisai maarum nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thelivaana thottram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thelivaana thottram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirumoolai kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirumoolai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Seyalindri vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyalindri vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thalai keezhai aanen…ae…aennn…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thalai keezhai aanen…ae…aennn…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindral mellinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindral mellinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thottu paarthal vallinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thottu paarthal vallinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivin ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivin ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvil unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvil unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandathu thaanae adhisayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathu thaanae adhisayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiroobanae…adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae…adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiroobanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiroobanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae…adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae…adhikaaranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaaranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaaranae"/>
</div>
</pre>
