---
title: "kotha kotha ga song lyrics"
album: "Mallesham"
artist: "Mark K Robin"
lyricist: "Goreti Venkanna"
director: "Raj R"
path: "/albums/mallesham-lyrics"
song: "Kotha Kotha Ga"
image: ../../images/albumart/mallesham.jpg
date: 2019-06-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wcf4sOt-L-A"
type: "happy"
singers:
  - Sri Krishna
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kotha Kothagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Kothagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathukantha Kothagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathukantha Kothagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Daari Maaripoye Iyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Daari Maaripoye Iyyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vintha Vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintha Vinthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Adugu Vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Adugu Vinthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Lesthu Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Lesthu Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Cheyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Cheyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatanchuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatanchuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaa Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaa Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalundavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalundavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbu Urimina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbu Urimina"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaa Alaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaa Alaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukulundavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukulundavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthentha Nimmalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthentha Nimmalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthentha Nammakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthentha Nammakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtaalu Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtaalu Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulla Baatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla Baatalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthentha Nibbaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthentha Nibbaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkentha Samburam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkentha Samburam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneellaloni Aatupotulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneellaloni Aatupotulo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotha Kothagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Kothagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathukantha Kothagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathukantha Kothagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Daari Maaripoye Iyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Daari Maaripoye Iyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vintha Vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintha Vinthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Adugu Vinthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Adugu Vinthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padi Lesthu Unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padi Lesthu Unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Cheyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Cheyyaala"/>
</div>
</pre>
