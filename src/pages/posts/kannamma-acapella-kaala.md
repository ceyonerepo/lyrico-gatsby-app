---
title: "kannamma acapella song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Uma Devi"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Kannamma Acapella"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ufh_w1AbXaQ"
type: "happy"
singers:
  - Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa aaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa nana naanaana naanana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa nana naanaana naanana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanana nana naanaana naanana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanana nana naanaana naanana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanaa nana naanaa nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa nana naanaa nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaa nee neee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaa nee neee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa nana naanaa nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa nana naanaa nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naananaa nee neee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naananaa nee neee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meettaadha veenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettaadha veenai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharukindra raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharukindra raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekaathu poogaandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekaathu poogaandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottadha thaayin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottadha thaayin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkindra paal pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkindra paal pol"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal kidakkindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal kidakkindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayangal aatrumThalaikkodhi thettrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangal aatrumThalaikkodhi thettrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal kaikooduthae kaikooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal kaikooduthae kaikooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoduvaanam indru neduvaanam aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam indru neduvaanam aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodumneram tholaivaaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodumneram tholaivaaguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilae ennamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae ennamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannammaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam saayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam saayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovaanamethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovaanamethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraama aaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraama aaraama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayangalethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayangalethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma aaa"/>
</div>
</pre>
