---
title: "aaradi singam song lyrics"
album: "Antony"
artist: "Sivatmikha"
lyricist: "Marabin Maindan Muthaiah - Kutti Kumar"
director: "Kutti Kumar"
path: "/albums/antony-lyrics"
song: "Aaradi Singam"
image: ../../images/albumart/antony.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9QpAQVg1P60"
type: "sad"
singers:
  - V.M. Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aaradi singam unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi singam unna"/>
</div>
<div class="lyrico-lyrics-wrapper">appa nu kupiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa nu kupiten"/>
</div>
<div class="lyrico-lyrics-wrapper">aanukku thaaimai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanukku thaaimai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannara paathuten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannara paathuten"/>
</div>
<div class="lyrico-lyrics-wrapper">pullaikaga ella naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullaikaga ella naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadu pattu ulaipaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadu pattu ulaipaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum sinthum viyarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum sinthum viyarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">thane appavin thaai paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane appavin thaai paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum sinthum viyarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum sinthum viyarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">thane appavin thaai paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane appavin thaai paalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanaanaane thanaanaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanaanaane thanaanaane"/>
</div>
<div class="lyrico-lyrics-wrapper">thana naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana naane"/>
</div>
<div class="lyrico-lyrics-wrapper">thanaanaane thanaanaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanaanaane thanaanaane"/>
</div>
<div class="lyrico-lyrics-wrapper">thana naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholil enna thooki kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholil enna thooki kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi varum boomiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi varum boomiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">thevai ellam theethu vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevai ellam theethu vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">appa than saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa than saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naana aakiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naana aakiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">vera ingu yaaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera ingu yaaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thaana paakurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thaana paakurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">appa thane kooramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa thane kooramma"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil theivam neril kandoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil theivam neril kandoma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaval theivam appa thanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaval theivam appa thanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaval theivam appa thanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaval theivam appa thanamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaradi singam unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi singam unna"/>
</div>
<div class="lyrico-lyrics-wrapper">appa nu kupiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa nu kupiten"/>
</div>
<div class="lyrico-lyrics-wrapper">aanukku thaaimai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanukku thaaimai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannara paathuten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannara paathuten"/>
</div>
<div class="lyrico-lyrics-wrapper">pullaikaga ella naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullaikaga ella naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">paadu pattu ulaipaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadu pattu ulaipaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum sinthum viyarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum sinthum viyarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">thane appavin thaai paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane appavin thaai paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum sinthum viyarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum sinthum viyarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">thane appavin thaai paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane appavin thaai paalu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum sinthum viyarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum sinthum viyarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">thane appavin thaai paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane appavin thaai paalu"/>
</div>
</pre>
