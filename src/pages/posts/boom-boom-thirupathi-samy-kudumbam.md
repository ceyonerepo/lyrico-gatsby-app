---
title: "boom boom song lyrics"
album: "Thirupathi Samy Kudumbam"
artist: "Sam D Raj"
lyricist: "Kabilan"
director: "Suresh Shanmugam"
path: "/albums/thirupathi-samy-kudumbam-lyrics"
song: "Boom Boom"
image: ../../images/albumart/thirupathi-samy-kudumbam.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q05RMa9TucI"
type: "happy"
singers:
  - Shruthi
  - Velu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thathi thathi thathi thathi thathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathi thathi thathi thathi thathi"/>
</div>
<div class="lyrico-lyrics-wrapper">nane nane nane nane nane nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane nane nane nane nane nanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom maata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poovu vachu pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovu vachu pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedengum mangalamai anathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedengum mangalamai anathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">elelo padaluku ethamamma ethammama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elelo padaluku ethamamma ethammama"/>
</div>
<div class="lyrico-lyrics-wrapper">ethetho vannam nenjil pootha thamm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethetho vannam nenjil pootha thamm"/>
</div>
<div class="lyrico-lyrics-wrapper">persai ennum sollai pesamal koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="persai ennum sollai pesamal koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">theeratha inbam pola ne thane kidaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratha inbam pola ne thane kidaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom maata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poovu vachu pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovu vachu pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedengum mangalamai anathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedengum mangalamai anathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iraivan nadagathin 2m baagam ithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivan nadagathin 2m baagam ithu "/>
</div>
<div class="lyrico-lyrics-wrapper">inime vaalum vaalvil inimaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime vaalum vaalvil inimaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyil pookal ena kaniyil eekal ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyil pookal ena kaniyil eekal ena"/>
</div>
<div class="lyrico-lyrics-wrapper">kalanthu vaalum oru thotamanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanthu vaalum oru thotamanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom maata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poovu vachu pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovu vachu pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedengum mangalamai anathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedengum mangalamai anathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pavada pattam poochi koothaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavada pattam poochi koothaduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kai veesi santhosamai kathaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai veesi santhosamai kathaduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vai pesa aatukutti vaal aatuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vai pesa aatukutti vaal aatuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thai pola chinna kuyil thalatuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai pola chinna kuyil thalatuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">or noolil sertha manja poomalaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or noolil sertha manja poomalaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">naam vaalum yavum anipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam vaalum yavum anipom"/>
</div>
<div class="lyrico-lyrics-wrapper">naam vaalum vaalvil ini nam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam vaalum vaalvil ini nam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanpathu kaanatha kan katchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanpathu kaanatha kan katchiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom maata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poovu vachu pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovu vachu pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedengum mangalamai anathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedengum mangalamai anathamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">therodum engal pathai engal car oodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therodum engal pathai engal car oodume"/>
</div>
<div class="lyrico-lyrics-wrapper">aanantham kannil vanthu neer aadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanantham kannil vanthu neer aadume"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathadi vannam pola vaalvomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathadi vannam pola vaalvomada"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrodu kai veesi povomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrodu kai veesi povomada"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vaanavillin kolam epothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vaanavillin kolam epothume"/>
</div>
<div class="lyrico-lyrics-wrapper">nam vaasal thedi varume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam vaasal thedi varume"/>
</div>
<div class="lyrico-lyrics-wrapper">naam vaalum vaalkai oru poomalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam vaalum vaalkai oru poomalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">athikalai sangeethame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athikalai sangeethame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom maata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poovu vachu pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovu vachu pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedengum mangalamai anathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedengum mangalamai anathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">persai ennum sollai pesamal koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="persai ennum sollai pesamal koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">theeratha inbam pola ne thane kidaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratha inbam pola ne thane kidaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iraivan nadagathin 2m baagam ithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivan nadagathin 2m baagam ithu "/>
</div>
<div class="lyrico-lyrics-wrapper">inime vaalum vaalvil inimaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime vaalum vaalvil inimaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyil pookal ena kaniyil eekal ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyil pookal ena kaniyil eekal ena"/>
</div>
<div class="lyrico-lyrics-wrapper">kalanthu vaalum oru thotamanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanthu vaalum oru thotamanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom maata pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom maata pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poovu vachu pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovu vachu pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedengum mangalamai anathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedengum mangalamai anathamma"/>
</div>
</pre>
