---
title: "poopol poopol en nenjai song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Poopol Poopol En Nenjai"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Xme4suiErGA"
type: "love"
singers:
  - Karthik
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poopol Poopol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopol Poopol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjai Koithaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Koithaval"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnalaai Minnalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalaai Minnalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paarvai Parithaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvai Parithaval"/>
</div>
</pre>
