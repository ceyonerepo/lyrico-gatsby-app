---
title: "eppa paarthaalum song lyrics"
album: "Aalambana"
artist: "HipHop Tamizha"
lyricist: "Pa Vijay"
director: "Pari K Vijay"
path: "/albums/aalambana-lyrics"
song: "Eppa Paarthaalum"
image: ../../images/albumart/aalambana.jpg
date: 2021-07-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/72hveifkGSM"
type: "love"
singers:
  - Armaan Malik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeppa paarthalum unna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa paarthalum unna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikum manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosikum manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa ketaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa ketaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pathi pesidum othadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pathi pesidum othadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam maranthu uravum maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam maranthu uravum maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanthu kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanthu kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna pinna kaadhal valarnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna pinna kaadhal valarnthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aachudi aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aachudi aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku enna aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku enna aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo chedi poo chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo chedi poo chedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podava kattum poo chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podava kattum poo chedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey innum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey innum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpol yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpol yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum vera illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum vera illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi nenjikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nenjikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpol yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpol yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum vera illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum vera illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi nenjikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nenjikulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayil magale manjil pagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil magale manjil pagale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanin nagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanin nagale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai kurale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai kurale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa mayil magale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa mayil magale"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal pagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal pagale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanin nagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanin nagale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai kurale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai kurale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa paarthalum unna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa paarthalum unna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikum manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosikum manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa ketaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa ketaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pathi pesidum othadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pathi pesidum othadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee eb veetuku vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eb veetuku vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal poonthotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal poonthotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un paatham thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un paatham thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduven kaal metti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduven kaal metti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa nee sollalanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa nee sollalanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippen kai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippen kai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai athu mattum thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai athu mattum thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasura theepetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasura theepetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye patthikichu patthikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye patthikichu patthikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukullaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukullaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatthikuchi vattikuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatthikuchi vattikuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukullaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukullaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kichu kichu kichu kichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kichu kichu kichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seya solliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seya solliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki kichu sikki kichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki kichu sikki kichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha pullaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha pullaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aachudi aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aachudi aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaku enna aachudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaku enna aachudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo chedi poo chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo chedi poo chedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podava kattum poo chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podava kattum poo chedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey innum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey innum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpol yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpol yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum vera illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum vera illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi nenjikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nenjikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Innum enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Innum enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpol yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpol yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum vera illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum vera illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi nenjikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi nenjikulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa paarthalum unna pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa paarthalum unna pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yosikum manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosikum manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa ketaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa ketaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pathi pesidum othadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pathi pesidum othadu"/>
</div>
</pre>
