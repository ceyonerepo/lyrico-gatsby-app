---
title: "kanne kanne song lyrics"
album: "Arjun Suravaram"
artist: "Sam CS"
lyricist: "Sri Mani"
director: "T Santhosh"
path: "/albums/arjun-suravaram-lyrics"
song: "Kanne Kanne"
image: ../../images/albumart/arjun-suravaram.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MCfRAA74vuI"
type: "love"
singers:
  - Anurag Kulkarni
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa Manasila Manasila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manasila Manasila"/>
</div>
<div class="lyrico-lyrics-wrapper">O Manase Korukunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Manase Korukunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manasuke Manasuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manasuke Manasuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Varase Cheppamande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Varase Cheppamande"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Ela Cheppeyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Ela Cheppeyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Teepi Maate Neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Teepi Maate Neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Emo Ela Dateyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emo Ela Dateyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Taguve Takadhimi Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Taguve Takadhimi Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Teliyanide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Teliyanide"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Choodanide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Choodanide"/>
</div>
<div class="lyrico-lyrics-wrapper">Nede Telisinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nede Telisinade"/>
</div>
<div class="lyrico-lyrics-wrapper">Munupennadu Lenidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munupennadu Lenidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Modalouthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modalouthonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jariginade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jariginade"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvedo Perginade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvedo Perginade"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Viriginade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Viriginade"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhave Vippe Vela Ide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhave Vippe Vela Ide"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Reppe Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Reppe Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalaloke Nadichale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalaloke Nadichale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Choosthu Choosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Choosthu Choosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nene Marichane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nene Marichane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Reppe Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Reppe Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalaloke Nadichale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalaloke Nadichale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Choosthu Choosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Choosthu Choosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nene Marichane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nene Marichane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tiyyaga Tiyatiyyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiyyaga Tiyatiyyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Talapulu Panchavela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Talapulu Panchavela"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachuthuu Yemarchuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachuthuu Yemarchuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nuvve Dasthavendukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nuvve Dasthavendukala"/>
</div>
<div class="lyrico-lyrics-wrapper">O Chinuku Kiranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Chinuku Kiranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagalipe Merupe Hariville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagalipe Merupe Hariville"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayam Vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayam Vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Rangulu Neeke Kanapadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Rangulu Neeke Kanapadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellaga Melamellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellaga Melamellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Daarulu Kalisenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Daarulu Kalisenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayilo Ee Haayilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayilo Ee Haayilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Akasale Daatesaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akasale Daatesaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innalla Naa Ontarithaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innalla Naa Ontarithaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherigenu Nee Vallene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherigenu Nee Vallene"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopulatho Kaaka Pedavulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopulatho Kaaka Pedavulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppeyi Ee Maatalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppeyi Ee Maatalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Reppe Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Reppe Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalaloke Nadichale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalaloke Nadichale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Choosthu Choosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Choosthu Choosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nene Marichane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nene Marichane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Reppe Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Reppe Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalaloke Nadichale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalaloke Nadichale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Choosthu Choosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Choosthu Choosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nene Marichane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nene Marichane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalika Toli Kadalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalika Toli Kadalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Nilakada Talapullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nilakada Talapullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadalika Toli Sadalika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadalika Toli Sadalika"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Chuttu Bigisina Sankelalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Chuttu Bigisina Sankelalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kalaham Viraham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kalaham Viraham"/>
</div>
<div class="lyrico-lyrics-wrapper">Tiyyani Tarahagundadu Vidudalela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiyyani Tarahagundadu Vidudalela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinava Cheliyaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinava Cheliyaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchani Pedavula Palukulila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchani Pedavula Palukulila"/>
</div>
<div class="lyrico-lyrics-wrapper">Modalika Tolisariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modalika Tolisariga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Yedalo Alajadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Yedalo Alajadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidurika Karuvavvaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidurika Karuvavvaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Kudure Kudure Chedirenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Kudure Kudure Chedirenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Innella Kaalam Merisenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innella Kaalam Merisenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Kalisina Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Kalisina Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Oohala Vismaya Viswamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Oohala Vismaya Viswamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennela Nee Chirunavve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennela Nee Chirunavve"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Reppe Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Reppe Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalaloke Nadichale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalaloke Nadichale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Choosthu Choosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Choosthu Choosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nene Marichane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nene Marichane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Reppe Vesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Reppe Vesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalaloke Nadichale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalaloke Nadichale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ninne Choosthu Choosthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ninne Choosthu Choosthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Nene Marichane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nene Marichane"/>
</div>
</pre>
