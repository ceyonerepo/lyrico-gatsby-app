---
title: "sanyaas mantra song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Sanyass Mantra"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oojMmtAb5Yk"
type: "melody"
singers:
  - Kamal Haasan
  - Hema Malini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">priya Mythili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="priya Mythili"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam Aayiram Kaalaththu Payir Ena Oru Vachanam Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam Aayiram Kaalaththu Payir Ena Oru Vachanam Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">payir Endraale, Endrenum Aruvadaiyum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payir Endraale, Endrenum Aruvadaiyum Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhramaanandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhramaanandam"/>
</div>
<div class="lyrico-lyrics-wrapper">parama Sukadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parama Sukadham"/>
</div>
<div class="lyrico-lyrics-wrapper">kevalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kevalam"/>
</div>
<div class="lyrico-lyrics-wrapper">gnayana Moorththim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gnayana Moorththim"/>
</div>
<div class="lyrico-lyrics-wrapper">dwanvathitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dwanvathitham"/>
</div>
<div class="lyrico-lyrics-wrapper">trigunarahitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trigunarahitham"/>
</div>
<div class="lyrico-lyrics-wrapper">tatwamasyadhilakshyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tatwamasyadhilakshyam"/>
</div>
<div class="lyrico-lyrics-wrapper">aekam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aekam"/>
</div>
<div class="lyrico-lyrics-wrapper">nithaym
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nithaym"/>
</div>
<div class="lyrico-lyrics-wrapper">vimalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vimalam"/>
</div>
<div class="lyrico-lyrics-wrapper">achalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achalam"/>
</div>
<div class="lyrico-lyrics-wrapper">sarvadhee Sakshi Bootham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarvadhee Sakshi Bootham"/>
</div>
<div class="lyrico-lyrics-wrapper">bhaavaathetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhaavaathetham"/>
</div>
<div class="lyrico-lyrics-wrapper">trigunarahitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trigunarahitham"/>
</div>
<div class="lyrico-lyrics-wrapper">satgurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satgurum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanamaamee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanamaamee"/>
</div>
<div class="lyrico-lyrics-wrapper">om
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="om"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mythili Nalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mythili Nalam"/>
</div>
<div class="lyrico-lyrics-wrapper">azhahaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhahaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nee Enakku Vaaiththathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee Enakku Vaaiththathu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan Seitha Punyam Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan Seitha Punyam Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan Unakku Vaaiththathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan Unakku Vaaiththathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee Endro Seitha Paavam Endrum Kooruvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee Endro Seitha Paavam Endrum Kooruvar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">gamastham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamastham"/>
</div>
<div class="lyrico-lyrics-wrapper">gamanaadhi Shoonyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamanaadhi Shoonyam"/>
</div>
<div class="lyrico-lyrics-wrapper">chidroopa Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidroopa Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">thimiraapahaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimiraapahaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">pashyamithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pashyamithe"/>
</div>
<div class="lyrico-lyrics-wrapper">sarva Janaam Tharastham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarva Janaam Tharastham"/>
</div>
<div class="lyrico-lyrics-wrapper">namaami Hamsam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaami Hamsam"/>
</div>
<div class="lyrico-lyrics-wrapper">paramaathva Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paramaathva Roopam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en Seyalin Kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en Seyalin Kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">koodiya Viravil Unakku Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodiya Viravil Unakku Puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">bharatha Maathavukku Naan Seiyappogum Panividaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bharatha Maathavukku Naan Seiyappogum Panividaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">sontha Bandhangal Idaiyooraagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontha Bandhangal Idaiyooraagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu Karuvil Valarum Kuzhainthaimel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu Karuvil Valarum Kuzhainthaimel"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu Thurgunangalin Nizhal Vizhaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu Thurgunangalin Nizhal Vizhaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">unathu Guna Prakaasham Kaapaatrattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathu Guna Prakaasham Kaapaatrattum"/>
</div>
<div class="lyrico-lyrics-wrapper">en Thaayai Naan Paarththathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en Thaayai Naan Paarththathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">vasantha Akkathaan Enakku Thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasantha Akkathaan Enakku Thaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ippothu Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippothu Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">maama Maami Anaivarin Saabamum Enakkundena Ariven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama Maami Anaivarin Saabamum Enakkundena Ariven"/>
</div>
<div class="lyrico-lyrics-wrapper">avaigalirunthu Meelum Arugathaiyum Ennamum Enakkillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaigalirunthu Meelum Arugathaiyum Ennamum Enakkillai"/>
</div>
<div class="lyrico-lyrics-wrapper">anbilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbilla"/>
</div>
<div class="lyrico-lyrics-wrapper">saaketh Raman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaketh Raman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sarve Vedayathpadamaamadhanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarve Vedayathpadamaamadhanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">thapaamsi Sarvaa Nijayathtvadenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapaamsi Sarvaa Nijayathtvadenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">yadhichandho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yadhichandho"/>
</div>
<div class="lyrico-lyrics-wrapper">bharmachariyam Charanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bharmachariyam Charanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">thaththe Padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaththe Padam"/>
</div>
<div class="lyrico-lyrics-wrapper">sangrahe Nabbravemee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangrahe Nabbravemee"/>
</div>
<div class="lyrico-lyrics-wrapper">om
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="om"/>
</div>
<div class="lyrico-lyrics-wrapper">mithyae Thathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithyae Thathu"/>
</div>
</pre>
