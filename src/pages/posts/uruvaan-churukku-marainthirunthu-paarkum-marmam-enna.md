---
title: "uruvaan churukku song lyrics"
album: "Marainthirunthu Paarkum Marmam Enna"
artist: "Achu Rajamani"
lyricist: "Pa Meenachisundaram"
director: "R. Rahesh"
path: "/albums/marainthirunthu-paarkum-marmam-enna-lyrics"
song: "Uruvaan Churukku"
image: ../../images/albumart/marainthirunthu-paarkum-marmam-enna.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ULL5VxkWjVA"
type: "love"
singers:
  - 	Jithin Raj - Sreelaksmy Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uruvaachu churukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvaachu churukku"/>
</div>
<div class="lyrico-lyrics-wrapper">potta manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potta manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvi thuruvi edukuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvi thuruvi edukuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannala enna koaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannala enna koaka"/>
</div>
<div class="lyrico-lyrics-wrapper">maakaa mathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maakaa mathuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">ariki kirukki aala mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariki kirukki aala mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">azhumbu panni asathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhumbu panni asathuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">un munnadi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un munnadi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">muthada paathu muzhunguriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthada paathu muzhunguriye"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuda iruntha athu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuda iruntha athu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagam romba azhakagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagam romba azhakagum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee senjathellam senju paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee senjathellam senju paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">athuve puthusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuve puthusaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan anjula naala pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan anjula naala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla unna vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla unna vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">panja uruti senja thiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panja uruti senja thiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura kadaiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura kadaiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh "/>
</div>
<div class="lyrico-lyrics-wrapper">un kathula vaasam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kathula vaasam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karanchu naan poren mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karanchu naan poren mela"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla mai pola oti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla mai pola oti"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa kodaiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa kodaiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">viralodu viral pathum serum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralodu viral pathum serum "/>
</div>
<div class="lyrico-lyrics-wrapper">pothu vinmeen koosuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu vinmeen koosuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">athil vetkam katti rekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athil vetkam katti rekka"/>
</div>
<div class="lyrico-lyrics-wrapper">katti kathal vela kaatuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti kathal vela kaatuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa othaiya pona pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa othaiya pona pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">varum enna pakave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum enna pakave illa"/>
</div>
<div class="lyrico-lyrics-wrapper">onnala nan annanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnala nan annanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkum aakaasam aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkum aakaasam aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">naan suthama ena maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan suthama ena maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippadyiye irunthathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippadyiye irunthathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnala naa kandene ennul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnala naa kandene ennul"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai paasam thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai paasam thane"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kodi vaanavil vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kodi vaanavil vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poga megam thooruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga megam thooruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kai regai ellam neeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai regai ellam neeyai"/>
</div>
<div class="lyrico-lyrics-wrapper">maara kathal ooruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maara kathal ooruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uruvaachu churukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvaachu churukku"/>
</div>
<div class="lyrico-lyrics-wrapper">potta manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potta manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvi thuruvi edukuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvi thuruvi edukuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannala enna koaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannala enna koaka"/>
</div>
<div class="lyrico-lyrics-wrapper">maakaa mathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maakaa mathuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">ariki kirukki aala mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariki kirukki aala mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">azhumbu panni asathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhumbu panni asathuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">un munnadi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un munnadi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">muthada paathu muzhunguriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthada paathu muzhunguriye"/>
</div>
</pre>
