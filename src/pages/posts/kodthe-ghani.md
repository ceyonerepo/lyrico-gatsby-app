---
title: "kodthe song lyrics"
album: "Ghani"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Kiran Korrapati"
path: "/albums/ghani-lyrics"
song: "Kodthe"
image: ../../images/albumart/ghani.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Q8KHZg__1vA"
type: "happy"
singers:
  - Harika Narayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ringare Ringa Ringa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringare Ringa Ringa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ringa Ringaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringa Ringaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ring of the Destiny Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ring of the Destiny Ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Raara Singha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raara Singha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil Maange Sport Ye Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Maange Sport Ye Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Boxingaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Boxingaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thu Aaja Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thu Aaja Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Ami Tumi Sannaahanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ami Tumi Sannaahanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidikillai Padhivellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikillai Padhivellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangani Vangani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangani Vangani"/>
</div>
<div class="lyrico-lyrics-wrapper">Varadhalle Adrenaline
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varadhalle Adrenaline"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongani Pongani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongani Pongani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Padhunento Powerento
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Padhunento Powerento"/>
</div>
<div class="lyrico-lyrics-wrapper">Punchullo Kanipinchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punchullo Kanipinchani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodthe Kodthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodthe Kodthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodthe Kodthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodthe Kodthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So-Called Players So Many
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So-Called Players So Many"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvadi Force Enthani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvadi Force Enthani"/>
</div>
<div class="lyrico-lyrics-wrapper">Leg Edithe Neggaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leg Edithe Neggaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvaadali Aatani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvaadali Aatani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakashaala Anchuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashaala Anchuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Meedhunna Anchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Meedhunna Anchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamayye Lekkana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamayye Lekkana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Lagaake Khelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Lagaake Khelona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Opponent Enthatodaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opponent Enthatodaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Usko Knockout Kardena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usko Knockout Kardena"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Hai Raja Range-lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Hai Raja Range-lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum Trophy Lelo Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum Trophy Lelo Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo Jeeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo Jeeta"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Hi Tho Sikander
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Hi Tho Sikander"/>
</div>
<div class="lyrico-lyrics-wrapper">Hota Hai Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hota Hai Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodthe Kodthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodthe Kodthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodthe Kodthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodthe Kodthe"/>
</div>
</pre>
