---
title: "rabb manneya song lyrics"
album: "Koi Jaane Na"
artist: "Rochak Kohli - Vikram Nagi"
lyricist: "Manoj Muntashir - Sham Deewana"
director: "Amin Hajee"
path: "/albums/koi-jaane-na-lyrics"
song: "Rabb Manneya"
image: ../../images/albumart/koi-jaane-na.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/0pv3jn1P4Gc"
type: "Love"
singers:
  - Lakhwinder Wadali
  - Neeti Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tere naina aise kaafir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere naina aise kaafir"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya chhupaun tujhse aakhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya chhupaun tujhse aakhir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere naina aise kaafir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere naina aise kaafir"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya chhupaun tujhse aakhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya chhupaun tujhse aakhir"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu toh jaane meri saari choriyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu toh jaane meri saari choriyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere pichhe tere pichhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere pichhe tere pichhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal dun main aankhein miche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal dun main aankhein miche"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere haathon mein hai meri doriyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere haathon mein hai meri doriyaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu hi chann mera tu hi meri raatan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi chann mera tu hi meri raatan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi dhoop meri tu hi barsaatan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi dhoop meri tu hi barsaatan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main yaara tainu sab manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main yaara tainu sab manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu maane ya na maane dildara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu maane ya na maane dildara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa te tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa te tainu rabb manneya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu maane ya na maane dildara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu maane ya na maane dildara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa te tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa te tainu rabb manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass hor kehda rabb da dwaara ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass hor kehda rabb da dwaara ho.."/>
</div>
<div class="lyrico-lyrics-wrapper">Hor kehda rabb da dwaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hor kehda rabb da dwaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa te tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa te tainu rabb manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu maane ya na maane dildara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu maane ya na maane dildara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa te tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa te tainu rabb manneya"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa te tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa te tainu rabb manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehro pehar tainu takkdi jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehro pehar tainu takkdi jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhpe marke main jee jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhpe marke main jee jaawan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehro pehar tainu takkdi javaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehro pehar tainu takkdi javaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhpe marke main jee jaawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhpe marke main jee jaawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri judayi yun tadpaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri judayi yun tadpaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Saans bharun toh ankh bhar aave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saans bharun toh ankh bhar aave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh tere bina nahiyon dil da ghuzara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh tere bina nahiyon dil da ghuzara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu maane ya na maane dildara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu maane ya na maane dildara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu maane ya na maane dildara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu maane ya na maane dildara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nahi jeena tere baajon main nahi jeena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nahi jeena tere baajon main nahi jeena"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagg chhad jaana nai jeena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagg chhad jaana nai jeena"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tan marr jaana nai jeena ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tan marr jaana nai jeena ho.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai kaafi jogi nu mandar kaafi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai kaafi jogi nu mandar kaafi"/>
</div>
<div class="lyrico-lyrics-wrapper">Taaron ko ambar kaafi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taaron ko ambar kaafi"/>
</div>
<div class="lyrico-lyrics-wrapper">Leharon ko samandar kaafi yaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharon ko samandar kaafi yaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanu kaafi tere ankh da ishaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanu kaafi tere ankh da ishaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanu kaafi tere ankh da ishaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanu kaafi tere ankh da ishaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu maane ya na maane dildara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu maane ya na maane dildara"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
<div class="lyrico-lyrics-wrapper">Assa ne tainu rabb manneya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assa ne tainu rabb manneya"/>
</div>
</pre>
