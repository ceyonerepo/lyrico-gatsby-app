---
title: "manniley eeramundu song lyrics"
album: "Jai Bhim"
artist: "Sean Roldan"
lyricist: "Yugabharathi"
director: "T.J. Gnanavel"
path: "/albums/jai-bhim-lyrics"
song: "Manniley Eeramundu"
image: ../../images/albumart/jai-bhim.jpg
date: 2021-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NJusMZfDslM"
type: "sad"
singers:
  - Vaikom Vijayalakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mannile Eeramundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannile Eeramundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulkaatil Poovum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulkaatil Poovum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambinaal Naalai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambinaal Naalai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Thaanga Jeevan Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thaanga Jeevan Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enge Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponvaanam Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponvaanam Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellai Ingillai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellai Ingillai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Nammodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannile Eeramundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannile Eeramundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulkaatil Poovum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulkaatil Poovum Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulluruthi Kaanbathu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulluruthi Kaanbathu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyilae Un Uyaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyilae Un Uyaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Seyal Aagivittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Seyal Aagivittaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaame Thedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Thedi Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Vazhi Nee Nadanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Vazhi Nee Nadanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Povathu Dhaan Vaazhvin Aram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povathu Dhaan Vaazhvin Aram"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin Kodi Yetri Vaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Kodi Yetri Vaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai Serum Kodi Tharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai Serum Kodi Tharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedal Illaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal Illaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Undo Sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Undo Sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Unnulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Unnulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Thedu Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thedu Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannile Eeramundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannile Eeramundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulkaatil Poovum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulkaatil Poovum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambinaal Naalai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambinaal Naalai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Thaanga Jeevan Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thaanga Jeevan Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enge Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponvaanam Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponvaanam Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellai Ingillai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellai Ingillai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Nammodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannile Eeramundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannile Eeramundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulkaatil Poovum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulkaatil Poovum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambinaal Naalai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambinaal Naalai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Thaanga Jeevan Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thaanga Jeevan Undu"/>
</div>
</pre>
