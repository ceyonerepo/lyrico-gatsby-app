---
title: "venmegam song lyrics"
album: "Dha Dha 87"
artist: "Leander Lee Marty"
lyricist: "Vijay Sri G"
director: "Vijay Sri G"
path: "/albums/dha-dha-87-lyrics"
song: "Venmegam"
image: ../../images/albumart/dha-dha-87.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/61ctRB74Whs"
type: "sad"
singers:
  - Priyanka N K
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">venmegam karuththaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venmegam karuththaale"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulle kanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulle kanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaram pookkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaram pookkama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaykkaama iruppadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaykkaama iruppadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un vidhi thaan vilaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vidhi thaan vilaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai thaan karuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai thaan karuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pizhaithaan sadhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizhaithaan sadhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanneer inippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanneer inippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyoram valaikulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyoram valaikulle"/>
</div>
<div class="lyrico-lyrics-wrapper">saagama thuditheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagama thuditheno"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhiyoram un kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyoram un kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">innum sadhai theduvadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum sadhai theduvadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanam than nilaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanam than nilaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ranam thaan mudivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ranam thaan mudivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvil karaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvil karaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en pirappe irappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pirappe irappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirukku uyiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirukku uyiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagirku pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagirku pudhidhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yen vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yen vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikindra manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikindra manadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaikku vazhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaikku vazhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">enai yen kondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai yen kondraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirukku uyiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirukku uyiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagirku pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagirku pudhidhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yen vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yen vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikindra manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikindra manadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaikku vazhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaikku vazhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">enai yen kondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai yen kondraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venmegam karuththaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venmegam karuththaale"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulle kanneero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulle kanneero"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaram pookkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaram pookkama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaykkaama iruppadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaykkaama iruppadho"/>
</div>
</pre>
