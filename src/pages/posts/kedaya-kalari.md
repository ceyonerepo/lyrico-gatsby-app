---
title: "kedaya song lyrics"
album: "Kalari"
artist: "VV Prasanna"
lyricist: "Muthuvijayan"
director: "Kiran C"
path: "/albums/kalari-lyrics"
song: "Kedaya"
image: ../../images/albumart/kalari.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9d3mpQ0Vkq0"
type: "love"
singers:
  - VV Prassanna
  - Vaishaali 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kedaya kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaya kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjula thala saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjula thala saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaiya nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaiya nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unakkae thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unakkae thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai naan paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeruthu kaichalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeruthu kaichalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannula kaadhalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula kaadhalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu kaal paaichalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu kaal paaichalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada onna nenachaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada onna nenachaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam pandhaa thulladhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam pandhaa thulladhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiya pudichalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiya pudichalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir innum neeladhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir innum neeladhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaya kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaya kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjula thala saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjula thala saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaiya nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaiya nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unakkae thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unakkae thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogura pokkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogura pokkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhaiya yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhaiya yethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesura saakkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesura saakkula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala oothura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala oothura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodhaanguzhal vechu ulnenjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhaanguzhal vechu ulnenjula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu urchaagatha thinuchaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu urchaagatha thinuchaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu dhaan oru kaadhal padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu dhaan oru kaadhal padam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna kanjaadaiyil varanjaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna kanjaadaiyil varanjaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodai mazhai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodai mazhai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaattil dhinam neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaattil dhinam neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endi endi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endi endi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paada paduthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paada paduthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaya kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaya kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjula thala saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjula thala saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaiya nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaiya nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unakkae thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unakkae thunaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalilaa veedhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalilaa veedhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarvamaai paarkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarvamaai paarkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolilaa aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolilaa aasaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervaiyil korkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervaiyil korkkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un veetukkum ada en veetukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veetukkum ada en veetukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla dhooram ingae kuraiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla dhooram ingae kuraiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru un kannamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru un kannamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada en kannamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada en kannamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna muththangalaal niraiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna muththangalaal niraiyaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho edhir paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho edhir paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ooda thenjenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ooda thenjenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadaa kaanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadaa kaanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi mazhaiyaai kudhikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi mazhaiyaai kudhikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaya kedakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaya kedakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjula thala saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjula thala saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaiya nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaiya nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thana na thana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thana na thana naanaa"/>
</div>
</pre>
