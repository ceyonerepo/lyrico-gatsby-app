---
title: "aararo aariraro song lyrics"
album: "Siruthai"
artist: "Vidyasagar"
lyricist: "Arivumathi"
director: "Siva"
path: "/albums/siruthai-lyrics"
song: "Aararo Aariraro"
image: ../../images/albumart/siruthai.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TPtEmPAEKxE"
type: "sentiment"
singers:
  - Srivadhini Thaman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambulikku Naerivaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambulikku Naerivaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiyaana Thaai Ivaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyaana Thaai Ivaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangaratha Thaerivaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangaratha Thaerivaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochupatta Nogumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochupatta Nogumunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochadakki Muththamittaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochadakki Muththamittaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhalupattaa Nogumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalupattaa Nogumunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavadanga Muththamitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavadanga Muththamitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongaa Mani Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaa Mani Vilakkae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongaama Thoongu Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaama Thoongu Kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Agal Vilakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Agal Vilakkae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asaiyaama Thoongu Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaiyaama Thoongu Kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariro Aariraro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariro Aariraro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariro Aariraro Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariro Aariraro Oh"/>
</div>
</pre>
