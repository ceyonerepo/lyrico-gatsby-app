---
title: "muthal idam song lyrics"
album: "Mudhal Idam"
artist: "D. Imman"
lyricist: "Arivumathi"
director: "R. Kumaran"
path: "/albums/mudhal-idam-lyrics"
song: "Muthal Idam"
image: ../../images/albumart/mudhal-idam.jpg
date: 2011-08-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N8RKO04y9AE"
type: "title track"
singers:
  - Feji
  - Ranaina Reddy
  - Sam P. Keerthan
  - Senthil Dass
  - Vasudevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thaga Thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaga Thaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thaga Thaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaga Thaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Nae Thaane Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Nae Thaane Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiyai Thaaiyaai Mathikka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiyai Thaaiyaai Mathikka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarugal Kandaal Mithikka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarugal Kandaal Mithikka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Unnaal Vella Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Unnaal Vella Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaivanum Un Peyar Solla Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaivanum Un Peyar Solla Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanai Tholil Thookka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanai Tholil Thookka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriyai Yeni Aakka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriyai Yeni Aakka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirinil Kudaigal Neetta Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirinil Kudaigal Neetta Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarinil Thunivai Thetra Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarinil Thunivai Thetra Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaal Edhaiyum Thiratta Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaal Edhaiyum Thiratta Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaithaal Udanae Udaikka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaithaal Udanae Udaikka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelvigal Nooru Ketka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal Nooru Ketka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalai Viraivil Meetka Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalai Viraivil Meetka Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhal Idammudhal Idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Idammudhal Idam"/>
</div>
</pre>
