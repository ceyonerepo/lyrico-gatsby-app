---
title: "pelli sandad song lyrics"
album: "Pelli Sanda D"
artist: "M.M.keeravaani"
lyricist: "Chandrabose"
director: "Gowri Ronanki"
path: "/albums/pelli-sandad-lyrics"
song: "Pelli SandaD"
image: ../../images/albumart/pelli-sandad.jpg
date: 2021-05-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/WpdEJsC4xGo"
type: "happy"
singers:
  - Hema Chandra
  - Deepu
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pattu cheeralaa thala thalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu cheeralaa thala thalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattagolusulaa gala galalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattagolusulaa gala galalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu cheeralaa thala thalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu cheeralaa thala thalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattagolusulaa gala galalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattagolusulaa gala galalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poola chokkala reparepalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola chokkala reparepalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silku panchela tapatapalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silku panchela tapatapalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasula perula dhagadhagalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasula perula dhagadhagalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Coffee gaajula bhugabhugalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee gaajula bhugabhugalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamidaakula milamilalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamidaakula milamilalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobbaraakula damadamalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobbaraakula damadamalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gattimelaala damadamalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattimelaala damadamalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantashaalalo ghumaghumalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantashaalalo ghumaghumalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annee annee annee annee anni kalipithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annee annee annee annee anni kalipithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pipi pipi pipipipi pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pipi pipi pipipipi pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dudum dudum dudum dudum pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dudum dudum dudum dudum pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pipi pipi pipipipi pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pipi pipi pipipipi pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dudum dudum dudum dudum pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dudum dudum dudum dudum pelli sandadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pipi pipi pipipipi pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pipi pipi pipipipi pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dudum dudum dudum dudum pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dudum dudum dudum dudum pelli sandadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahilaamanula chintha pikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahilaamanula chintha pikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Punya purushula pekamukkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya purushula pekamukkalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baavamarudhula parihaasaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baavamarudhula parihaasaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatha mithrula palakarimpulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatha mithrula palakarimpulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andarithoti photolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarithoti photolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthyakshari poteelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthyakshari poteelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andarithoti photolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andarithoti photolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthyakshari poteelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthyakshari poteelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atthamaamala aathmeeyathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atthamaamala aathmeeyathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaatha bhaammala aashissulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaatha bhaammala aashissulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaru challe akshinthalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaru challe akshinthalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaa naannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaa naannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma nannala thadi kannulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma nannala thadi kannulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannepillala konte navvulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannepillala konte navvulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurra kannula donga choopulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurra kannula donga choopulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andagatthela chilipi saigalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andagatthela chilipi saigalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodigitthala churuku cheshtalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodigitthala churuku cheshtalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chevulanu oogenu jhookaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevulanu oogenu jhookaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moginchenu madhilo baakaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moginchenu madhilo baakaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukkupudakalo mirumitlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkupudakalo mirumitlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedaverupulu penchenu padhiretlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedaverupulu penchenu padhiretlu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa pachhani oni andaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa pachhani oni andaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhinaayi aa paruvaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhinaayi aa paruvaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokkukunte adhe padivelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokkukunte adhe padivelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaalu yama oholu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaalu yama oholu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evdiki teliyani sangathulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evdiki teliyani sangathulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eragaa visire biscuitulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eragaa visire biscuitulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha pogidinaa me kadhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha pogidinaa me kadhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashalu doshalu appadaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashalu doshalu appadaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chel ree chel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chel ree chel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pelli sandadi pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelli sandadi pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pipi pipi pipipipi pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pipi pipi pipipipi pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dadam dudum dudum dudum pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dadam dudum dudum dudum pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pipi pipi pipipipi pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pipi pipi pipipipi pelli sandadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dadam dudum dudum dudum pelli sandadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dadam dudum dudum dudum pelli sandadi"/>
</div>
</pre>
