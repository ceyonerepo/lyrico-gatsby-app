---
title: "peter beatu yethu song lyrics"
album: "Sarvam Thaala Mayam"
artist: "A. R. Rahman"
lyricist: "Arunraja Kamaraj"
director: "Rajiv Menon"
path: "/albums/sarvam-thaala-mayam-lyrics"
song: "Peter Beatu Yethu"
image: ../../images/albumart/sarvam-thaala-mayam.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/juB6-_LmG74"
type: "happy"
singers:
  - G. V. Prakash Kumar
  - Sathyaprakash
  - Arjun Chandy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Danda Nakkana Dhanananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakkana Dhanananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakkana Dhinananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakkana Dhinananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda Nakkana Dhananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda Nakkana Dhananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhana Na Nana Na Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhana Na Nana Na Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigilu Soundu Therikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigilu Soundu Therikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koralu Kaadha Kizhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koralu Kaadha Kizhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ravusu Ragala Polakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravusu Ragala Polakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Medhakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Medhakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravail Velicham Porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravail Velicham Porakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Unakku Thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Unakku Thudikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangi Kedantha Veeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangi Kedantha Veeramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusa Muzhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Muzhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppothum Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Kavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethukkum Kaattaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukkum Kaattaadhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaalum Manasa Poottaathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Manasa Poottaathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Illai Indrey Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Illai Indrey Kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beatu yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beatu yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beatu yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beatu yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Pottu Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Pottu Thaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Othungu Othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Othungu Othungu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigilu Soundu Therikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigilu Soundu Therikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koralu Kaadha Kizhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koralu Kaadha Kizhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ravusu Ragala Polakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravusu Ragala Polakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Medhakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Medhakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irvail Velicham Porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irvail Velicham Porakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Unakku Thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Unakku Thudikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangi Kedantha Veeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangi Kedantha Veeramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusa Muzhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Muzhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Maa Dhimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Maa Dhimaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ku Nu Kusimey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku Nu Kusimey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mae Ya Keyma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Ya Keyma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ya Yae Yae Yay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Yae Yae Yay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musi Musi Gumaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Musi Gumaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musi Musi Gumaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Musi Gumaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musi Gumaaye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Gumaaye ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zumba Zumba Zumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zumba Zumba Zumba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musk Musk ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musk Musk ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zumba Zumba Zumba Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zumba Zumba Zumba Ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Mani Neramthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Mani Neramthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Paathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paathalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Jenma Santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Jenma Santhosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Thonudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Thonudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Oru Naalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Oru Naalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaaney Ellarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaney Ellarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukulla Un Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Un Pera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaa Poda Porome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaa Poda Porome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pachcha Kuththi Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pachcha Kuththi Kaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Soodam Yethi Aatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Soodam Yethi Aatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Podum Neraththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Podum Neraththula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Pola Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Pola Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Danda Dana Nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Danda Dana Nakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Danakku Danaku Nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Danakku Danaku Nakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Onna Paatha Ellaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Onna Paatha Ellaarukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kick Uh Yerum Pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Uh Yerum Pakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beat Ah yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beat Ah yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beat Ah yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beat Ah yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Pottu Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Pottu Thaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Othungu Othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Othungu Othungu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigilu Sound Uh Therikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigilu Sound Uh Therikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koralu Kaadha Kizhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koralu Kaadha Kizhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ravusu Ragala Polakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravusu Ragala Polakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Medhakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Medhakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravil Velicham Porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Velicham Porakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Unakku Thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Unakku Thudikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangi Kedantha Veeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangi Kedantha Veeramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusa Muzhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Muzhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ya Ma Dhimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Ma Dhimaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ku Nu Kusimey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku Nu Kusimey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mae Ya Keyma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mae Ya Keyma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae Yae Yae Yay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Yae Yae Yay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musi Musi Gumaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Musi Gumaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musi Musi Gumaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Musi Gumaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musi Gumaaye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi Gumaaye ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedhi Vazhi Marichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhi Vazhi Marichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Power Uh Kaatuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Power Uh Kaatuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patrol Eh Vandha Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patrol Eh Vandha Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Scene Ah Poduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Ah Poduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru Pola Theruvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Pola Theruvil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Petti Thookuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Petti Thookuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Engala Morachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Engala Morachaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veluthu Vanguvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthu Vanguvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Ellam Naamakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Ellam Naamakilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondaattam Dhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattam Dhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kaattu Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kaattu Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootamunnu Kaththi Solvomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootamunnu Kaththi Solvomey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beat Ah yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beat Ah yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beat Ah yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beat Ah yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Pottu Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Pottu Thaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Othungu Othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Othungu Othungu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigilu Sound Uh Therikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigilu Sound Uh Therikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koralu Kaadha Kizhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koralu Kaadha Kizhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ravusu Ragala Polakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravusu Ragala Polakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Medhakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Medhakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravil Velicham Porakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Velicham Porakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Unakku Thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Unakku Thudikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangi Kedantha Veeramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangi Kedantha Veeramum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusa Muzhikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Muzhikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppothum Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Kavalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethukkum Kaattaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukkum Kaattaadhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaalum Manasa Poottaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Manasa Poottaadhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Illai Indrey Kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Illai Indrey Kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beat Ah yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beat Ah yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Beat Ah yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Beat Ah yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Peter Pottu Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Peter Pottu Thaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Othungu Othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Othungu Othungu"/>
</div>
</pre>
