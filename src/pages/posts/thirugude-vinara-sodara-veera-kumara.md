---
title: "thirugude song lyrics"
album: "Vinara Sodara Veera Kumara"
artist: "Shravan Bharadwaj"
lyricist: "Laxmi Bhupala"
director: "Sateesh Chandra Nadella"
path: "/albums/vinara-sodara-veera-kumara-lyrics"
song: "Thirugude"
image: ../../images/albumart/vinara-sodara-veera-kumara.jpg
date: 2019-03-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xlziBhJJCu8"
type: "happy"
singers:
  - Shravan Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thellavari Kodikanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellavari Kodikanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Lesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Lesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Gunna Vonti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Gunna Vonti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeda Sabbu Rasi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeda Sabbu Rasi…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillagadu Sigguthoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillagadu Sigguthoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Moggalesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moggalesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi Navvu Navvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Navvu Navvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallajodu Pettinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallajodu Pettinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstar"/>
</div>
<div class="lyrico-lyrics-wrapper">Juttu Kastha Dhuvvinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juttu Kastha Dhuvvinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Stylish Star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stylish Star"/>
</div>
<div class="lyrico-lyrics-wrapper">Powder-Ye Kottinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powder-Ye Kottinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Power Star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power Star"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalone Paddade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalone Paddade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkado Thokkene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkado Thokkene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakka Thoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakka Thoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkani Chukkane Chudagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkani Chukkane Chudagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkuna Lakkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkuna Lakkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadra Vanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadra Vanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkithe Chukkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkithe Chukkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosi Raadaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosi Raadaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotha Kotha Gundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha Kotha Gundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Velle Dhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Velle Dhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthu Matthu Gundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthu Matthu Gundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daati Vachi Gaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daati Vachi Gaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Maaruthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Maaruthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Premathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Premathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Murisi Poye Pillode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisi Poye Pillode"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudanantha Sepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudanantha Sepu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poju Kottinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poju Kottinade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ora Choopu Chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Choopu Chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanuku Puttupoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanuku Puttupoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaggar Avvaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaggar Avvaledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Unda Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Unda Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidura Kuda Polede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidura Kuda Polede"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikkalode Kani Chedda Vadu Kaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkalode Kani Chedda Vadu Kaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekka Cheyadu Inka Potugadu Veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekka Cheyadu Inka Potugadu Veede"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbu Ledhu Kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbu Ledhu Kani"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Cheelchuthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Cheelchuthade"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Pichi Poojare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Pichi Poojare"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-U Gadu Unte Rechipothade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-U Gadu Unte Rechipothade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Dhakkakunte Sachipothade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Dhakkakunte Sachipothade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaali Matti Kuda Dhachukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaali Matti Kuda Dhachukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu Chala Manchode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu Chala Manchode"/>
</div>
<div class="lyrico-lyrics-wrapper">Thellavari Kodikanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellavari Kodikanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Lesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Lesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Gunna Vonti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Gunna Vonti"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeda Sabbu Rasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeda Sabbu Rasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillagadu Sigguthoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillagadu Sigguthoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Moggalesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moggalesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi Navvu Navvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Navvu Navvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallajodu Pettinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallajodu Pettinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Superstar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstar"/>
</div>
<div class="lyrico-lyrics-wrapper">Juttu Kastha Dhuvvinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juttu Kastha Dhuvvinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Stylish Star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stylish Star"/>
</div>
<div class="lyrico-lyrics-wrapper">Powder-Ye Kottinadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Powder-Ye Kottinadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Power Star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power Star"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalone Paddade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalone Paddade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugude Thirugude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugude Thirugude"/>
</div>
</pre>
