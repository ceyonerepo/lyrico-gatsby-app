---
title: "paatu onnu kattu kattu song lyrics"
album: "Jilla"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Nesan"
path: "/albums/jilla-song-lyrics"
song: "Paatu Onnu Kattu Kattu"
image: ../../images/albumart/jilla.jpg
date: 2014-01-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Hr7JFJOG97g"
type: "Mass Intro"
singers:
  - Shankar Mahadevan
  - S. P. Balasubramanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paatu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Kattu Thotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattu Thotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kali Kaiya Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kali Kaiya Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu Jora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu Jora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Kattu Thotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattu Thotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kali Kaiya Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kali Kaiya Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu Jora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu Jora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veli Illaa Kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli Illaa Kaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pole Odu Engum Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pole Odu Engum Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaara Thapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaara Thapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theva Illa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakthiyum Serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakthiyum Serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Massuda Ethirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massuda Ethirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Evanam Thoosu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Evanam Thoosu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Kattu Thotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattu Thotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kali Kaiya Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kali Kaiya Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu Jora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu Jora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorar Kellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorar Kellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Verthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Pol Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Pol Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvan Mannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvan Mannile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadu Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadu Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadu Ellam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu Ellam Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthu Ponaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu Ponaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaivo Nenjile Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaivo Nenjile Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naane Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naane Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Solla Maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Solla Maten"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Endro Oar Naal Pirivaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endro Oar Naal Pirivaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidamaten Thotru Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidamaten Thotru Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaipome Naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaipome Naama"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Boomi Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Boomi Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakthiyum Serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakthiyum Serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Massuda Ethirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massuda Ethirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Evanam Thoosu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Evanam Thoosu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatu Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Kattu Thotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kattu Thotha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kali Kaiya Thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kali Kaiya Thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattu Jora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu Jora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veli Illaa Kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli Illaa Kaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pole Odu Engum Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pole Odu Engum Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaara Thapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaara Thapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theva Illa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakthiyum Serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakthiyum Serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Massuda Hey Ethirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massuda Hey Ethirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Evanam Thoosu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Evanam Thoosu Da"/>
</div>
</pre>
