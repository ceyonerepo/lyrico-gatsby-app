---
title: "koothanamma koothan song lyrics"
album: "Koothan"
artist: "Balz G"
lyricist: "Kabilan"
director: "Venky AL"
path: "/albums/koothan-lyrics"
song: "Koothanamma Koothan"
image: ../../images/albumart/koothan.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WgCBvr3d0P4"
type: "happy"
singers:
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ooru rendu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru rendu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">koothadiku kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadiku kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothadi illaiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadi illaiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukethu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukethu kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooru rendu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru rendu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">koothadiku kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadiku kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">koothadi illaiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothadi illaiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukethu kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukethu kondattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oruthanukul pala pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthanukul pala pera"/>
</div>
<div class="lyrico-lyrics-wrapper">olunju irunthu pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olunju irunthu pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">pathen pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathen pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">pathen pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathen pathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theru koothu raja thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theru koothu raja thane"/>
</div>
<div class="lyrico-lyrics-wrapper">engaloda thathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaloda thathen"/>
</div>
<div class="lyrico-lyrics-wrapper">thathen thathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathen thathen"/>
</div>
<div class="lyrico-lyrics-wrapper">thathen thathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathen thathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadiganukku ootu pota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadiganukku ootu pota"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavagum boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavagum boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">antha mgr ah poei kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha mgr ah poei kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">adengappa saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adengappa saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">naadalum mandarathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadalum mandarathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">nadigaruku naarkaali 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadigaruku naarkaali "/>
</div>
<div class="lyrico-lyrics-wrapper">potanga potanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potanga potanga"/>
</div>
<div class="lyrico-lyrics-wrapper">avar pecha ellam kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avar pecha ellam kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">koduthu periyavanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduthu periyavanga "/>
</div>
<div class="lyrico-lyrics-wrapper">ketanga ketanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketanga ketanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nadiganoda kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadiganoda kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu ninnan munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu ninnan munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">avanoda pinnadi kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanoda pinnadi kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">makkal sernthu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makkal sernthu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">kootama avan paadugira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootama avan paadugira"/>
</div>
<div class="lyrico-lyrics-wrapper">pattuku aadu machan aatama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattuku aadu machan aatama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilambarathula nadigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilambarathula nadigal"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha vikuthada porulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha vikuthada porulu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha nadiganoda mugatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha nadiganoda mugatha"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu vanguranga palaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu vanguranga palaru"/>
</div>
<div class="lyrico-lyrics-wrapper">sivajiyin valiyaaga ketkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivajiyin valiyaaga ketkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">simma kural ketome ketome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simma kural ketome ketome"/>
</div>
<div class="lyrico-lyrics-wrapper">avarale porata thalaivargalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avarale porata thalaivargalin"/>
</div>
<div class="lyrico-lyrics-wrapper">por mugathai paarthome parthome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="por mugathai paarthome parthome"/>
</div>
<div class="lyrico-lyrics-wrapper">nadigar endra ellorume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadigar endra ellorume"/>
</div>
<div class="lyrico-lyrics-wrapper">naadu arinja aaluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadu arinja aaluda"/>
</div>
<div class="lyrico-lyrics-wrapper">naalu pera keluda atha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalu pera keluda atha "/>
</div>
<div class="lyrico-lyrics-wrapper">aelai panakaranukum mela da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelai panakaranukum mela da"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kavalaigalai vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kavalaigalai vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha kathari kathari kola da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha kathari kathari kola da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ovvoru manusanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovvoru manusanum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothanamma koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothanamma koothu"/>
</div>
</pre>
