---
title: "manusha manusha song lyrics"
album: "Kaali"
artist: "Vijay Antony"
lyricist: "Madhan Karky"
director: "Kiruthiga Udhayanidhi"
path: "/albums/kaali-lyrics"
song: "Manusha Manusha"
image: ../../images/albumart/kaali.jpg
date: 2018-05-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/efQcNkUoPrA"
type: "mass"
singers:
  - Jagadeesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappaala yethu sirumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappaala yethu sirumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippaala maarum varumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippaala maarum varumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha ithu un boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha ithu un boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirichu vathachaa athuva saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirichu vathachaa athuva saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini yaarum illa adimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini yaarum illa adimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padippaala maarum nelamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padippaala maarum nelamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhhhhh ohhh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhhhhh ohhh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhiya kettu sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhiya kettu sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha palliya izhuthu moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha palliya izhuthu moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhiya parthu thandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhiya parthu thandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha velaiya thooki poodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha velaiya thooki poodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukku thol niram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku thol niram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ovvoru kuzhanthaiyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ovvoru kuzhanthaiyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam usuraiyum katti anaichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam usuraiyum katti anaichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaa uravaa pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaa uravaa pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanum evalum un mela illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanum evalum un mela illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Evalum evanum un keezha illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalum evanum un keezha illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum ini dhooram illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum ini dhooram illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirndhu ezhundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhu ezhundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha vaanam ellaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha vaanam ellaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhhhhh ohhh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhhhhh ohhh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyula ennam yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyula ennam yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha yethida vaa raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha yethida vaa raasaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttu pooraathaiyum maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttu pooraathaiyum maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vidiyala thaa raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vidiyala thaa raasaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukkum kaathu onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum kaathu onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga yaaraiyum thaangum mannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga yaaraiyum thaangum mannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathula paayura thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathula paayura thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulamumkothiram paakkuma kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulamumkothiram paakkuma kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirichaa kizhiyae nee koani illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirichaa kizhiyae nee koani illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezha aanaa nee kozhai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezha aanaa nee kozhai illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi adichaa adhil artham illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi adichaa adhil artham illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirndhu ezhundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhu ezhundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha vaanam ellaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha vaanam ellaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhhhhh ohhh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhhhhh ohhh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ohhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ohhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusha manusha thoda kanneera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha thoda kanneera"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusha manusha vaa munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusha manusha vaa munnera"/>
</div>
<div class="lyrico-lyrics-wrapper">Porappaala yethu sirumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porappaala yethu sirumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippaala maarum varumai aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippaala maarum varumai aa"/>
</div>
</pre>
