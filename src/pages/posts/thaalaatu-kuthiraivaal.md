---
title: "thaalaatu song lyrics"
album: "Kuthiraivaal"
artist: "Pradeep Kumar"
lyricist: "Sathish Raja Dharmar"
director: "Manoj Leonel Jahson - Shyam Sunder"
path: "/albums/kuthiraivaal-lyrics"
song: "Thaalaatu"
image: ../../images/albumart/kuthiraivaal.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ybK3GHbZinA"
type: "happy"
singers:
  - Lalitha Vijaykumar
  - Priyanka NK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro Aariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro Aariraaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neliyun Kaanalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neliyun Kaanalila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohankaattum Senkuruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohankaattum Senkuruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyellaa Ettu Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyellaa Ettu Vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Etta Pogum Kaanalallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etta Pogum Kaanalallo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valiyaa Uruveduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyaa Uruveduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha Seevan Inga Nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Seevan Inga Nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Usiraa Vona Paakka Uchchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usiraa Vona Paakka Uchchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Nokkanumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Nokkanumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nelatha Kooru Vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelatha Kooru Vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Kurugi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Kurugi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhachen Pozhachenunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhachen Pozhachenunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaatha Indha Saname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaatha Indha Saname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Thalaikku Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Thalaikku Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soppanam Seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soppanam Seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaama Nee Va Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaama Nee Va Saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthinga Saainj Chorangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthinga Saainj Chorangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vanthinga Saainj Chorangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthinga Saainj Chorangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karadellaam Ettu Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karadellaam Ettu Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadellam Neeyodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadellam Neeyodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha Moga Meduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Moga Meduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Selaiya Nee Amainja Dhenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiya Nee Amainja Dhenam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu Vachu Thadamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Vachu Thadamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaamba Poothu Nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaamba Poothu Nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Inganaiyaa Vaanaminnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inganaiyaa Vaanaminnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Megamellaam Vanthiranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megamellaam Vanthiranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkaiya Thovalavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkaiya Thovalavittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Vaanam Alakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Vaanam Alakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerangi Ingana Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerangi Ingana Va"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootharaiyila Nee Orangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootharaiyila Nee Orangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Pootharaiyila Nee Orangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Pootharaiyila Nee Orangu"/>
</div>
</pre>
