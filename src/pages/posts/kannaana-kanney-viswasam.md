---
title: 'kannaana kanney lyrics'
album: 'Viswasam'
artist: 'D Imman'
lyricist: 'Thamarai'
director: 'Siva'
path: '/albums/viswasam-song-lyrics'
song: 'Kannaana Kanney'
image: ../../images/albumart/viswasam.jpg
date: 2019-01-10
lang: tamil
singers:
- Sid Sriram
youtubeLink: "https://www.youtube.com/embed/FysV6XnDlQk"
type: 'parenting'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">En meedhu saaya vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En meedhu saaya vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnaana nenjai
<input type="checkbox" class="lyrico-select-lyric-line" value="Punnaana nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnaana kaiyaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnaana kaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola neeva vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poo pola neeva vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan kaathu nindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kaathu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal dhorum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalangal dhorum"/>
</div>
<div class="lyrico-lyrics-wrapper">En yekkam theerumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En yekkam theerumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan paarthu nindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan paarthu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon vaanam engum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pon vaanam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">En minnal thondrumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En minnal thondrumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaneeraai megam thoorum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaneeraai megam thoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer serum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneer serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karkandaai maarumaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Karkandaai maarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaraariraaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraariraaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaroo raaroo aaraariraaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Raaroo raaroo aaraariraaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaroo raaroo raaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraariraaroo raaroo raaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaroo (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraariraaroo"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">En meedhu saaya vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En meedhu saaya vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnaana nenjai
<input type="checkbox" class="lyrico-select-lyric-line" value="Punnaana nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnaana kaiyaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnaana kaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola neeva vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poo pola neeva vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaa…..aaa….aaa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa…..aaa….aaa….aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa……aaa….aaa….aa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa……aaa….aaa….aa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Alai kadalin naduvae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alai kadalin naduvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alainthidavaa thaniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alainthidavaa thaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padagenavae unaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Padagenavae unaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthen kannae…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthen kannae….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Puthai manalil veezhunthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthai manalil veezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthainthidavae irunthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Puthainthidavae irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuru nagayai erinthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuru nagayai erinthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meettaai ennai
<input type="checkbox" class="lyrico-select-lyric-line" value="Meettaai ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vinnodum mannodum vaadum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinnodum mannodum vaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum oonjal manadhoram
<input type="checkbox" class="lyrico-select-lyric-line" value="Perum oonjal manadhoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanpattu nool vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanpattu nool vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai yedho bayam koodum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enai yedho bayam koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mayil ondrai paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Mayil ondrai paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyaagi aadinen
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhaiyaagi aadinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha urchaagam podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha urchaagam podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaga thondrum idhae vinaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Saaga thondrum idhae vinaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">En meedhu saaya vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En meedhu saaya vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnaana nenjai
<input type="checkbox" class="lyrico-select-lyric-line" value="Punnaana nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnaana kaiyaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponnaana kaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo pola neeva vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poo pola neeva vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee thoongum bothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thoongum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un netri meedhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un netri meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththangal vaikkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththangal vaikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porvaigal porththi
<input type="checkbox" class="lyrico-select-lyric-line" value="Porvaigal porththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaamal thalthi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogaamal thalthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaaval kaakanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kaaval kaakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellorum thoongum neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellorum thoongum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum mounathil pesanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanum neeyum mounathil pesanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaraariraaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraariraaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaroo raaroo aaraariraaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Raaroo raaroo aaraariraaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaroo raaroo raaroo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraariraaroo raaroo raaroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaroo (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaraariraaroo"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaana kannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaana kannae"/>
</div>
</pre>