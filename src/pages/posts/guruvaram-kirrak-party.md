---
title: "guruvaram song lyrics"
album: "Kirrak Party"
artist: "B Ajaneesh Loknath"
lyricist: "Rakendu Mouli"
director: "Sharan Koppisetty"
path: "/albums/kirrak-party-lyrics"
song: "Guruvaram"
image: ../../images/albumart/kirrak-party.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/VByi0lLjZx4"
type: "happy"
singers:
  -	Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Guruvaram sayamkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guruvaram sayamkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisochchindiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisochchindiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhrustam ara metre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhrustam ara metre"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooram lo undhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooram lo undhira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna kanna kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna kanna kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Black n white
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Black n white"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu colour aipoyele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu colour aipoyele"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka chaka samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka chaka samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Breaku lesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breaku lesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku side ichindhi le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku side ichindhi le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalona arerere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalona arerere"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchi alelelele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchi alelelele"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhaadi ayyayyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhaadi ayyayyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi picchi oohalevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi picchi oohalevo"/>
</div>
<div class="lyrico-lyrics-wrapper">Once more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once more"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalona arerere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalona arerere"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchi alelelele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchi alelelele"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhadi ayyayyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhadi ayyayyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi picchi oohalevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi picchi oohalevo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaallo thelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaallo thelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moon ekki oogesaa uyyalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moon ekki oogesaa uyyalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholipremallo of course
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholipremallo of course"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivi maamule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivi maamule"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayo hayo nee kannullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayo hayo nee kannullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho undhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho undhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnattundi thala kindulu ayyaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnattundi thala kindulu ayyaalee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathipoyene athigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathipoyene athigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigindhi nee jathagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigindhi nee jathagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padha padha mantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha padha mantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugu these
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugu these"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapaleni thondara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapaleni thondara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu choodagane ganthulese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choodagane ganthulese"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu chindara vandara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu chindara vandara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalona arerere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalona arerere"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchi alelelele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchi alelelele"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhadi ayyayyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhadi ayyayyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi pichi oohalevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi pichi oohalevo"/>
</div>
<div class="lyrico-lyrics-wrapper">Once more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once more"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalona arerere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalona arerere"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchi alelelele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchi alelelele"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhadi ayyayyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhadi ayyayyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi pichi oohalevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi pichi oohalevo"/>
</div>
</pre>
