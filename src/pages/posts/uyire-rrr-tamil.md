---
title: "uyire song lyrics"
album: "RRR Tamil"
artist: "Maragathamani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli "
path: "/albums/rrr-tamil-lyrics"
song: "Uyire"
image: ../../images/albumart/rrr-tamil.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oSI5jC6l-Hw"
type: "sad"
singers:
  - MM Keeravaani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkagavae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkagavae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keezh vizhum endhan vizhi neerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezh vizhum endhan vizhi neerae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nandri dhaanae uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nandri dhaanae uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">En moochum undhan varalarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En moochum undhan varalarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yetrukol en uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yetrukol en uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meedhu thoovi vananga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meedhu thoovi vananga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sempookkal pol en kurudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sempookkal pol en kurudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan aazhnthurundhu uranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aazhnthurundhu uranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyaagi nalgu amaidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyaagi nalgu amaidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan veezhum bodhum nooraguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan veezhum bodhum nooraguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakkendrum illai irudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakkendrum illai irudhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae ae"/>
</div>
</pre>
