---
title: "lamha tera mera song lyrics"
album: "Pagglait"
artist: "Arijit Singh"
lyricist: "Neelesh Misra"
director: "Umesh Bist"
path: "/albums/pagglait-lyrics"
song: "Lamha Tera Mera"
image: ../../images/albumart/pagglait.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/VXRyfaBluXk"
type: "Mass"
singers:
  - Antara Mitra
  - Arijit Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm.. Lamha tera mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm.. Lamha tera mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoondha kiye thhe kahan kahan na jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoondha kiye thhe kahan kahan na jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamhon ke hain taane baane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamhon ke hain taane baane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil na pehchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil na pehchane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho lamhon ke hain taane baane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho lamhon ke hain taane baane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil na pehchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil na pehchane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhe hum rok lein ya jaane dein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe hum rok lein ya jaane dein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aane de nayi subah ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane de nayi subah ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwaab aakash mein, yun lehrane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaab aakash mein, yun lehrane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadal aane de, baras jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadal aane de, baras jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamha yeh tera, lamha yeh mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamha yeh tera, lamha yeh mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamhon ka phera, dosh tera na mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamhon ka phera, dosh tera na mera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh teri meri duniya hai bas ik lamha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh teri meri duniya hai bas ik lamha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinka hathon se udaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinka hathon se udaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ka bichhadna dil ka milna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ka bichhadna dil ka milna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek lamha isko to roko zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek lamha isko to roko zara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phir na milein dhadkan se bhara yeh lamha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir na milein dhadkan se bhara yeh lamha"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir na milein dusra yeh lamha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir na milein dusra yeh lamha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamhon ko lauta de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamhon ko lauta de"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho phir na mile dusra yeh lamha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho phir na mile dusra yeh lamha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamhon ko lauta de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamhon ko lauta de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhe hum rok lein ya jaane dein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhe hum rok lein ya jaane dein"/>
</div>
<div class="lyrico-lyrics-wrapper">Aane de nayi subah ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane de nayi subah ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khwaab aakash mein, yun lehrane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaab aakash mein, yun lehrane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadal aane de, baras jaane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadal aane de, baras jaane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamha yeh tera, lamha yeh mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamha yeh tera, lamha yeh mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Lamhon ka phera, dosh tera na mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lamhon ka phera, dosh tera na mera"/>
</div>
</pre>
