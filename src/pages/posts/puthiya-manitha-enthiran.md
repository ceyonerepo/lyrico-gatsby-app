---
title: "puthiya manitha song lyrics"
album: "Enthiran"
artist: "A.R. Rahman"
lyricist: "Vairamuthu"
director: "S. Shankar"
path: "/albums/enthiran-lyrics"
song: "Puthiya Manitha"
image: ../../images/albumart/enthiran.jpg
date: 2010-07-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IdveDKCYtI8"
type: "Mass"
singers:
  - S.P.Balasubramaniam
  - A.R.Rahman
  - Khatija Rahman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pudhiya Manidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Manidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eggai Vaarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eggai Vaarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silicon Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silicon Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayarootti Uyirooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayarootti Uyirooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Hardiskil Ninaivootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hardiskil Ninaivootti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhiyaatha Udalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyaatha Udalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadiyaatha Uyirodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadiyaatha Uyirodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraam Arivai Araithu Ootri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraam Arivai Araithu Ootri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaam Arivai Ezhuppum Muyarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaam Arivai Ezhuppum Muyarchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthiya Manithaa Bhoomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Manithaa Bhoomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Manithaa Bhoomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Manithaa Bhoomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiya Manithaa Bhoomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya Manithaa Bhoomikku Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatram Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanai Menmai Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai Menmai Sei"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathu Aatralaal Ulagai Maatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu Aatralaal Ulagai Maatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaa Uyirkkum Namaiyaai Iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaa Uyirkkum Namaiyaai Iru"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Nilaiyilum Unmaiyaai Iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Nilaiyilum Unmaiyaai Iru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhira Endhira Enthira En Enthira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira Endhira Enthira En Enthira"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhira Endhira Enthira En Enthira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira Endhira Enthira En Enthira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kandathu Aararivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kandathu Aararivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kondathu Perarivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondathu Perarivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Katrathu Aaru Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Katrathu Aaru Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Petrathu Nooru Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Petrathu Nooru Mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeral Kanayam Thunbamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeral Kanayam Thunbamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya Kolaaru Yethumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Kolaaru Yethumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthira Manithan Vaazhvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthira Manithan Vaazhvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthiran Veezhvathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthiran Veezhvathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvil Pirantha Ella Marikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvil Pirantha Ella Marikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivil Piranthathu Marippathey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivil Piranthathu Marippathey Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho En Enthiran Ivan Amaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho En Enthiran Ivan Amaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho En Enthiran Ivan Amaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho En Enthiran Ivan Amaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Innoru Naan Mugane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Innoru Naan Mugane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enbavan En Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enbavan En Magane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan Petravan Aan Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan Petravan Aan Magane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aam Un Peyar Enthirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aam Un Peyar Enthirane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enbathu Arivu Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enbathu Arivu Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Enbathu Enathu Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Enbathu Enathu Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan Pondrathu Enathu Veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Pondrathu Enathu Veli"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naalaiya Gnana Oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naalaiya Gnana Oli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kondathu Udal Vadivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondathu Udal Vadivam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kondathu Porul Vadivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kondathu Porul Vadivam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kandathu Oru Piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kandathu Oru Piravi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaanbathu Pala Piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaanbathu Pala Piravi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Robo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pan Mozhigal Katraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pan Mozhigal Katraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thanthai Mozhi Tamil Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thanthai Mozhi Tamil Allava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Robo Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kandam Vendraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kandam Vendraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Karthaavukku Adimai Alla Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Karthaavukku Adimai Alla Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya Manithaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya Manithaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Vaa"/>
</div>
</pre>
