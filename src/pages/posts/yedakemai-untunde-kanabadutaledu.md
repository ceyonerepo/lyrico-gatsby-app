---
title: "yedakemai untunde song lyrics"
album: "Kanabadutaledu"
artist: "Madhu Ponnas"
lyricist: "Purna chary"
director: "Balaraju M"
path: "/albums/kanabadutaledu-lyrics"
song: "Yedakemai Untunde"
image: ../../images/albumart/kanabadutaledu.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/w2DhXugX2H0"
type: "love"
singers:
  - Karthiik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedakemai untundhe yedho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedakemai untundhe yedho "/>
</div>
<div class="lyrico-lyrics-wrapper">chitram jarigindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitram jarigindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepudu ledey maaye needhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudu ledey maaye needhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha maarepoyindhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha maarepoyindhey "/>
</div>
<div class="lyrico-lyrics-wrapper">madhanam modalayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhanam modalayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee alochana entho bagundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee alochana entho bagundhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi prathisaari neevaiposthundhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi prathisaari neevaiposthundhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka nimusham chuseloga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka nimusham chuseloga "/>
</div>
<div class="lyrico-lyrics-wrapper">samayaniki ee parugela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samayaniki ee parugela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika aagey veeley ledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika aagey veeley ledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Urime manasipude Egire yedha layale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urime manasipude Egire yedha layale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate vinadhasale O pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate vinadhasale O pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule kurisenule kalale thadipenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule kurisenule kalale thadipenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanam kudhirinadhey nee valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanam kudhirinadhey nee valla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedakemai untundhe yedho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedakemai untundhe yedho "/>
</div>
<div class="lyrico-lyrics-wrapper">chitram jarigindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitram jarigindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepudu ledey maaye needhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudu ledey maaye needhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venakale nee venakale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakale nee venakale "/>
</div>
<div class="lyrico-lyrics-wrapper">aduge saagidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduge saagidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Munakese thanu munakese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munakese thanu munakese"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oohalle undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oohalle undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaache maate needhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaache maate needhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne dhaati ponani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne dhaati ponani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho unnanante nako varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho unnanante nako varame"/>
</div>
<div class="lyrico-lyrics-wrapper">Piliche nee palakula madhuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piliche nee palakula madhuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Tega murisenu adharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tega murisenu adharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee chinni chinni gnapakalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee chinni chinni gnapakalu "/>
</div>
<div class="lyrico-lyrics-wrapper">gunde lothullo padhilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde lothullo padhilam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urime manasipude Egire yedha layale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urime manasipude Egire yedha layale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate vinadhasale O pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate vinadhasale O pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule kurisenule kalale thadipenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule kurisenule kalale thadipenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanam kudhirinadhey nee valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanam kudhirinadhey nee valla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayase thana tholakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayase thana tholakari "/>
</div>
<div class="lyrico-lyrics-wrapper">alajadi telisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alajadi telisina"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvapu kadalina alaluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvapu kadalina alaluga "/>
</div>
<div class="lyrico-lyrics-wrapper">egisenu mayalona theli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egisenu mayalona theli"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi moyaleni haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi moyaleni haayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru lokam chuse aase 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru lokam chuse aase "/>
</div>
<div class="lyrico-lyrics-wrapper">theere margham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theere margham"/>
</div>
<div class="lyrico-lyrics-wrapper">Unde veladama ippude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unde veladama ippude"/>
</div>
</pre>
