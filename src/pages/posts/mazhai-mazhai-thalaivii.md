---
title: "mazhai mazhai song lyrics"
album: "Thalaivii"
artist: "G.V. Prakash Kumar"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/thalaivii-song-lyrics"
song: "Mazhai Mazhai"
image: ../../images/albumart/thalaivii.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q2s6BHty3XE"
type: "happy"
singers:
  - Saindhavi Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai Mazhai En Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Mazhai En Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nanainthatho Naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nanainthatho Naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Mattum Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Mattum Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thiranthatho Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thiranthatho Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Mazhai En Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Mazhai En Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nanainthatho Naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nanainthatho Naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Mattum Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Mattum Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thiranthatho Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thiranthatho Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanile Odaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanile Odaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerile Medaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerile Medaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Venmegam En Aadaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Venmegam En Aadaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Mazhai En Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Mazhai En Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nanainthatho Naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nanainthatho Naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Mattum Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Mattum Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thiranthatho Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thiranthatho Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadanthu Naan Nadakkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu Naan Nadakkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaasam Kondadum Poo Pookoottame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaasam Kondadum Poo Pookoottame"/>
</div>
<div class="lyrico-lyrics-wrapper">En Meniyai Thaan Theendida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meniyai Thaan Theendida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Mandradum Neerottame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Mandradum Neerottame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha Kaatru Nillamal Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha Kaatru Nillamal Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Penmai Kondadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Penmai Kondadutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindadum Kanni Theanendru Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindadum Kanni Theanendru Enni"/>
</div>
<div class="lyrico-lyrics-wrapper">En Meedhu Vandadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meedhu Vandadutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjamo Paadalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamo Paadalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalo Thedalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalo Thedalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nee Enge En Kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nee Enge En Kadhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Mazhai En Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Mazhai En Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nanainthatho Naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nanainthatho Naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Mattum Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Mattum Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thiranthatho Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thiranthatho Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menkaatrila Thaneerila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menkaatrila Thaneerila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Dhegam Yen Ingu Silirkutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Dhegam Yen Ingu Silirkutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thugilthanai Thulaikave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thugilthanai Thulaikave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorathil Un Kangal Ninaikutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorathil Un Kangal Ninaikutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paadham Killi En Maarbil Thulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadham Killi En Maarbil Thulli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Meengale Neenthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Meengale Neenthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai En Meni Paalendru Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai En Meni Paalendru Enni"/>
</div>
<div class="lyrico-lyrics-wrapper">En Meedhu Thean Sinthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meedhu Thean Sinthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu Naan Eeramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Naan Eeramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu Nee Dhooramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu Nee Dhooramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idham Thaarayo En Kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idham Thaarayo En Kadhala"/>
</div>
</pre>
