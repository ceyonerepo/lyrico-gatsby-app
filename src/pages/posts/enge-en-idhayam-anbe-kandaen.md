---
title: "enge en idhayam anbe song lyrics"
album: "Kandaen"
artist: "Vijay Ebenezer"
lyricist: "Thamarai"
director: "A.C. Mugil"
path: "/albums/kandaen-lyrics"
song: "Enge En Idhayam Anbe"
image: ../../images/albumart/kandaen.jpg
date: 2011-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MNTIR4XYnXo"
type: "love"
singers:
  - Dr. Burn
  - Krish
  - Prashanthini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnai Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorchuvaiye Pon Silaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorchuvaiye Pon Silaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Va Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullunarve Maivizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullunarve Maivizhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Poisonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poisonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Unmai Thervetherve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Unmai Thervetherve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Sevigalil Kavidhaiyai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sevigalil Kavidhaiyai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Sendraai Pudhu Unarvagulaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Sendraai Pudhu Unarvagulaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nandhavana Pookalai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nandhavana Pookalai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam Malarkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Malarkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Theenda Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Theenda Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Vendum Tharuvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Vendum Tharuvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeen Idhayam Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeen Idhayam Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nan Thedi Parppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nan Thedi Parppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Kondu Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Kondu Sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyaayam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaayam Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inge Un Idhayam Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Un Idhayam Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindre En Kadhavai Thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindre En Kadhavai Thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Konde Nirpathai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konde Nirpathai Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnamai Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnamai Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Thozhai Thoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Thozhai Thoora"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyora Velichamum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyora Velichamum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil Sandhegam Kolaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Sandhegam Kolaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaal En Jeeevan Nilaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaal En Jeeevan Nilaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaipozhudhu Un Nenjil Irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaipozhudhu Un Nenjil Irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nani Kadhavena Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nani Kadhavena Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvaya Tharuvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvaya Tharuvaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anbe Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vinai Pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vinai Pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranum Aatralum Unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranum Aatralum Unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarndhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarndhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnarvaaya Unnarvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnarvaaya Unnarvaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">En Anbe Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karirulil Suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karirulil Suriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meralaiyil Thamarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meralaiyil Thamarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhathil Megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhathil Megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Palai Mannil Vaanmazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palai Mannil Vaanmazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayasaithu Parkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasaithu Parkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalil Un Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalil Un Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un-naavil Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un-naavil Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerthamagum Then-thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerthamagum Then-thuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedigaram Kattuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedigaram Kattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Sellum Vegathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Sellum Vegathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalaagi Unnai Nan Adaiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalaagi Unnai Nan Adaiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sellum Vazhi Ellam Maram Aaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sellum Vazhi Ellam Maram Aaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sottu Veyil Kooda Vida Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sottu Veyil Kooda Vida Matten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeen Idhayam Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeen Idhayam Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nan Thedi Parppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nan Thedi Parppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Kondu Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Kondu Sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyaayam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaayam Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inge Un Idhayam Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Un Idhayam Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindre En Kadhavai Thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindre En Kadhavai Thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Konde Nirpathai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konde Nirpathai Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnamai Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnamai Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Haahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorchuvaiye Pon Silaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorchuvaiye Pon Silaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Va Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullunarve Maivizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullunarve Maivizhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Poisonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poisonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Unmai Thervetherve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Unmai Thervetherve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katril Varam Vanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katril Varam Vanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Moongilvali Neenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongilvali Neenginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeethamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeethamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaithaane Paadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaithaane Paadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathirundha Natkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirundha Natkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Aaruthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Aaruthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamal Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamal Sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinbu Thondrum Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinbu Thondrum Vaanavil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pilai Serndha Podhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilai Serndha Podhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurai Nindra Podhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurai Nindra Podhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakagathaan Adhai Seithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakagathaan Adhai Seithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ennai Piriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ennai Piriyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhaal En Uyire Un Sumai-aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhaal En Uyire Un Sumai-aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engeen Idhayam Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeen Idhayam Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Nan Thedi Parppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Nan Thedi Parppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Kondu Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Kondu Sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyaayam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaayam Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Inge Un Idhayam Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Inge Un Idhayam Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindre En Kadhavai Thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindre En Kadhavai Thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Konde Nirpathai Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konde Nirpathai Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnamai Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnamai Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Thozhai Thoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Thozhai Thoora"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichangal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyora Velichamum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyora Velichamum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil Sandhegam Kolaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil Sandhegam Kolaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaal En Jeeevan Nilaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaal En Jeeevan Nilaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kandenekandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandenekandene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thandhene Thandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thandhene Thandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorchuvaiye Pon Silaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorchuvaiye Pon Silaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Va Baby Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Baby Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullunarve Mai Vizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullunarve Mai Vizhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Poisonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poisonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Unmai Therve Therve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Unmai Therve Therve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Sevigalil Kavidhaiyai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sevigalil Kavidhaiyai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Sendraai Pudhu Unarvagulaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Sendraai Pudhu Unarvagulaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nandhavana Pookalai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nandhavana Pookalai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam Malarkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Malarkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Theenda Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Theenda Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam Vendum Tharuvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Vendum Tharuvaaya"/>
</div>
</pre>
