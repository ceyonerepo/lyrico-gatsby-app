---
title: "setthu pochu manasu song lyrics"
album: "Peranbu"
artist: "Yuvan Shankar Raja"
lyricist: "Karunakaran"
director: "Ram"
path: "/albums/peranbu-lyrics"
song: "Setthu Pochu Manasu"
image: ../../images/albumart/peranbu.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ivbvWiN3BJM"
type: "sad"
singers:
  - Madhu Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevidu aachu bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevidu aachu bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurudu aachu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurudu aachu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara naanum kepen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara naanum kepen "/>
</div>
<div class="lyrico-lyrics-wrapper">intha bhoomi panthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha bhoomi panthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena vittu pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena vittu pona "/>
</div>
<div class="lyrico-lyrics-wrapper">neeum engu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeum engu theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul ingu aachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul ingu aachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nila engu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila engu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara naanum kepen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara naanum kepen "/>
</div>
<div class="lyrico-lyrics-wrapper">ada vidhiye sollidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada vidhiye sollidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram aana sediya thozil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram aana sediya thozil"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi sumapathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi sumapathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ninnu uranga koodathan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ninnu uranga koodathan "/>
</div>
<div class="lyrico-lyrics-wrapper">intha mannil idam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha mannil idam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mara kilaiyil thanga ponalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara kilaiyil thanga ponalum "/>
</div>
<div class="lyrico-lyrics-wrapper">ada paravaiyum angu vida villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada paravaiyum angu vida villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kakka kunji pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakka kunji pola "/>
</div>
<div class="lyrico-lyrics-wrapper">karaiyatha kaalam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaiyatha kaalam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Marathu kila nikkum neeum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marathu kila nikkum neeum "/>
</div>
<div class="lyrico-lyrics-wrapper">nimirnthu kooda paka villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirnthu kooda paka villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyathu entru aana pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyathu entru aana pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">naan muyantruthan thorgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan muyantruthan thorgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyathu entru aana pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyathu entru aana pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">naan kizhagaiye pargiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kizhagaiye pargiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irgaiyin theerpil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irgaiyin theerpil "/>
</div>
<div class="lyrico-lyrics-wrapper">naan kutra vaaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kutra vaaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai thiruthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai thiruthi "/>
</div>
<div class="lyrico-lyrics-wrapper">elutha yaarum illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elutha yaarum illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pola jeevan ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pola jeevan ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">othingi kolla idamum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othingi kolla idamum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna vida vaalvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna vida vaalvil "/>
</div>
<div class="lyrico-lyrics-wrapper">enagunu yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enagunu yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">En meethinil mothum kaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethinil mothum kaththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En meethinil mothum kaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meethinil mothum kaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu paavathin iramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu paavathin iramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meethinil saayum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meethinil saayum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">En sapangal theerumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sapangal theerumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudaigalai kandu mazhaium 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaigalai kandu mazhaium "/>
</div>
<div class="lyrico-lyrics-wrapper">vaanil nirgathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanil nirgathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai kandu vaalgaium 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai kandu vaalgaium "/>
</div>
<div class="lyrico-lyrics-wrapper">paathiyil mudiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathiyil mudiyathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Setthu pochchu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setthu pochchu manasu"/>
</div>
</pre>
