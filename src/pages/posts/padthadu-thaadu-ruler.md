---
title: "padthadu thaadu song lyrics"
album: "Ruler"
artist: "Chirantan Bhatt"
lyricist: "Bhaskarabhatla"
director: "K S Ravikumar"
path: "/albums/ruler-lyrics"
song: "Padthadu Thaadu"
image: ../../images/albumart/ruler.jpg
date: 2019-12-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/x-5c8i0LWnY"
type: "love"
singers:
  - Simha
  - Chandni Vijaykumar Shah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Era Erra Era Erra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era Erra Era Erra"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhavulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhavulni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhaadukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhaadukora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira Girra Gira Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Girra Gira Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiragesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiragesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Chuda Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Chuda Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Hay Sara Sara Sarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hay Sara Sara Sarra"/>
</div>
<div class="lyrico-lyrics-wrapper">Tega Nachesevu Kurra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tega Nachesevu Kurra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hay Jara Jara Jarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hay Jara Jara Jarra"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Nadume Jeelakarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Nadume Jeelakarra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kercheefe Asko Jaldi Jaldi Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kercheefe Asko Jaldi Jaldi Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Padthadu Thaadu Thaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padthadu Thaadu Thaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavadu Vaadu Vaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavadu Vaadu Vaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Padthadu Thaadu Thaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padthadu Thaadu Thaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavadu Vaadu Vaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavadu Vaadu Vaadu Avadina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aladdinke Nenu Andani Deepanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aladdinke Nenu Andani Deepanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Vachesa Lookesukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Vachesa Lookesukoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaac Newton Key Dorakani Appleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaac Newton Key Dorakani Appleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Darjaga Dorekesa Pattesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darjaga Dorekesa Pattesuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Kelikithe Kelikithe Itta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Kelikithe Kelikithe Itta"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Vudukuni Dhudukini Chuupistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Vudukuni Dhudukini Chuupistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasapu Sarukula Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasapu Sarukula Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Baruvuni Suluvuga Moosestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Baruvuni Suluvuga Moosestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Misa Misa Merupula Pitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Misa Misa Merupula Pitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Taha Taha Thalupulu Muusestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Taha Taha Thalupulu Muusestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogasari Gadasari Chutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasari Gadasari Chutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Segalanu Pogalanu Vuudestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Segalanu Pogalanu Vuudestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodha Chesko Galla Mulliga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodha Chesko Galla Mulliga"/>
</div>
<div class="lyrico-lyrics-wrapper">Padthadu Thaadu Thaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padthadu Thaadu Thaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavadu Vaadu Vaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavadu Vaadu Vaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Padthadu Thaadu Thaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padthadu Thaadu Thaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavadu Vaadu Vaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavadu Vaadu Vaadu Avadina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Champagne Bottello Sompulni Andhiste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champagne Bottello Sompulni Andhiste"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkulni Chustha Ve Athesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkulni Chustha Ve Athesuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Direct Andhani Vateyadam Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Direct Andhani Vateyadam Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Urgent Panulenti Apesuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urgent Panulenti Apesuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ika Ika Paka Paka Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ika Ika Paka Paka Valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Raka Raka Rakamulu Chuupistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Raka Raka Rakamulu Chuupistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Egaraku Egaraku Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egaraku Egaraku Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Adhigina Vayasunu Pampistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Adhigina Vayasunu Pampistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Niga Niga Navarasa Gulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niga Niga Navarasa Gulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Korakanu Korakanu Minngestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Korakanu Korakanu Minngestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kita Kita Kitukulu Anni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kita Kita Kitukulu Anni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Taka Taka Takataka Lagestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Taka Taka Takataka Lagestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasko Pusko Vundaku Kaaliga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasko Pusko Vundaku Kaaliga"/>
</div>
<div class="lyrico-lyrics-wrapper">Era Erra Era Erra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era Erra Era Erra"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhavulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhavulni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhaadukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhaadukora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gira Girra Gira Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gira Girra Gira Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiragesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiragesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Chuda Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Chuda Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Hay Sara Sara Sarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hay Sara Sara Sarra"/>
</div>
<div class="lyrico-lyrics-wrapper">Tega Nachesevu Kurra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tega Nachesevu Kurra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hay Jara Jara Jarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hay Jara Jara Jarra"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Nadume Jeelakarra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Nadume Jeelakarra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kercheefe Asko Rasko Pusko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kercheefe Asko Rasko Pusko "/>
</div>
<div class="lyrico-lyrics-wrapper">Sodhalanni Chesko Galla Mulliga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodhalanni Chesko Galla Mulliga"/>
</div>
<div class="lyrico-lyrics-wrapper">Padthadu Thaadu Thaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padthadu Thaadu Thaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavadu Vaadu Vaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavadu Vaadu Vaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Padthadu Thaadu Thaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padthadu Thaadu Thaadu Avadina"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavadu Vaadu Vaadu Avadina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavadu Vaadu Vaadu Avadina"/>
</div>
</pre>
