---
title: "aambala item song lyrics"
album: "Bodha"
artist: "Siddharth Vipin"
lyricist: "unknown"
director: "Suresh G"
path: "/albums/bodha-lyrics"
song: "Aambala Item"
image: ../../images/albumart/bodha.jpg
date: 2018-07-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SDPhwsjt2aE"
type: "happy"
singers:
  - Guru
  - Siddharth Vipin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">rich uh figure kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rich uh figure kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">rich uh figure kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rich uh figure kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu utharanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu utharanuma"/>
</div>
<div class="lyrico-lyrics-wrapper">pichu utharanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pichu utharanuma"/>
</div>
<div class="lyrico-lyrics-wrapper">sketch poduthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketch poduthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">sketch poduthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketch poduthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu seiyuthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu seiyuthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai vachu seiyuthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai vachu seiyuthuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalkaiyil aayiram bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiyil aayiram bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasa than thediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasa than thediye"/>
</div>
<div class="lyrico-lyrics-wrapper">maaruthu paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaruthu paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula pathathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula pathathum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaamamum bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaamamum bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">moova suthu moodum kathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moova suthu moodum kathe"/>
</div>
<div class="lyrico-lyrics-wrapper">over ah poduran seenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="over ah poduran seenu"/>
</div>
<div class="lyrico-lyrics-wrapper">dream la aaitaan salman kan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dream la aaitaan salman kan"/>
</div>
<div class="lyrico-lyrics-wrapper">local tea kada ponan nethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="local tea kada ponan nethu"/>
</div>
<div class="lyrico-lyrics-wrapper">cofee day la pandran koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cofee day la pandran koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">panam paduthura paatula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam paduthura paatula"/>
</div>
<div class="lyrico-lyrics-wrapper">iyyo oru thitam theeturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyyo oru thitam theeturan"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyaiyo thinam vattam poduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyaiyo thinam vattam poduran"/>
</div>
<div class="lyrico-lyrics-wrapper">aiiyo ada friendship la 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiiyo ada friendship la "/>
</div>
<div class="lyrico-lyrics-wrapper">ethana peela ada paya pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana peela ada paya pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vidu reel ah oru perusum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidu reel ah oru perusum"/>
</div>
<div class="lyrico-lyrics-wrapper">irukuthu ulla panam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukuthu ulla panam "/>
</div>
<div class="lyrico-lyrics-wrapper">adikuthu callin bell ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikuthu callin bell ah"/>
</div>
<div class="lyrico-lyrics-wrapper">ada life short film illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada life short film illa"/>
</div>
</pre>
