---
title: "yaaradi nee karaoke song lyrics"
album: "Jarugandi"
artist: "Bobo Shashi"
lyricist: "Uma Devi"
director: "AN Pitchumani"
path: "/albums/jarugandi-lyrics"
song: "Yaaradi Nee Karaoke"
image: ../../images/albumart/jarugandi.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KX9VutIDABs"
type: "love"
singers:
  - Bobo Shashi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pudhu pudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaigalai alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalai alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaigalum kulithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaigalum kulithidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravindri valargindra vembil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravindri valargindra vembil"/>
</div>
<div class="lyrico-lyrics-wrapper">Panai vanthu mulaithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panai vanthu mulaithidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suda suda sooriyan kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda suda sooriyan kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamum thulirthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamum thulirthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagargindra megangal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagargindra megangal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal nagarnthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal nagarnthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">yaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaradi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suda suda sooriyan kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda suda sooriyan kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamum thulirthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamum thulirthidumae"/>
</div>
</pre>
