---
title: "satena nenaindhadhu song lyrics"
album: "Kannathil Muthamittal"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kannathil-muthamittal-song-lyrics"
song: "Satena Nenaindhadhu Nenjam"
image: ../../images/albumart/kannathil-muthamittal.jpg
date: 2002-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_7hcdOGd8Tw"
type: "sad"
singers:
  - Minmini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sattena Nanaindhadhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena Nanaindhadhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarkarai Aanadhu Kaneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkarai Aanadhu Kaneer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam Inbam Oru Thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Inbam Oru Thunbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbam Eththanai Perinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam Eththanai Perinbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattena Nanaindhadhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena Nanaindhadhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarkarai Aanadhu Kaneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarkarai Aanadhu Kaneer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam Inbam Oru Thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Inbam Oru Thunbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunbam Eththanai Perinbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam Eththanai Perinbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalukkul Malligai Thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukkul Malligai Thooral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyirukkul Melliya Keeral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirukkul Melliya Keeral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugamaai Sugammaai Kollai Idu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Sugammaai Kollai Idu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyirai Mattum Vittu Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirai Mattum Vittu Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Vaasal Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Vaasal Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Nadandhuvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Nadandhuvarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru Kaathu Kidandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Kaathu Kidandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Vaanil Parandhu Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vaanil Parandhu Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorai Thirandhu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorai Thirandhu Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru Indru Thelindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Indru Thelindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaavi Vandhu Enai Anaitha Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi Vandhu Enai Anaitha Podhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Salli Vergal Arundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Salli Vergal Arundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saavin Ellai Varai Sendru Meendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavin Ellai Varai Sendru Meendu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Rendu Janmam Adaidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Rendu Janmam Adaidhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikkum Udhadu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkum Udhadu Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudaiththidu Vetkaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudaiththidu Vetkaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaippin Aadhikkathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaippin Aadhikkathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyetru Achaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyetru Achaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikkum Udhadu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkum Udhadu Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudaiththidu Vetkaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudaiththidu Vetkaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaippin Aadhikkathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaippin Aadhikkathaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliyetru Achaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyetru Achaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugamaai Sugammaai Kollai Idu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Sugammaai Kollai Idu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyirai Mattum Vittu Vidu…Uuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirai Mattum Vittu Vidu…Uuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugamaai Sugammaai Kollai Idu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Sugammaai Kollai Idu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyirai Mattum Vittu Vidu…Uuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirai Mattum Vittu Vidu…Uuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattena Nanaindhadhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena Nanaindhadhu Nenjam"/>
</div>
</pre>
