---
title: "sandaali song lyrics"
album: "Ispade Rajavum Idhaya Raniyum"
artist: "Sam C. S."
lyricist: "Sam C. S."
director: "Ranjit Jeyakodi"
path: "/albums/idpade-rajavum-idhaya-raniyum-lyrics"
song: "Sandaali"
image: ../../images/albumart/ispade-rajavum-idhaya-raniyum.jpg
date: 2019-03-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rqg7A5r6zng"
type: "sad"
singers:
  - Paul Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan Unna Vittu Velagitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Vittu Velagitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda Pesalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda Pesalana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paakka Varalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakka Varalana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Will You Still Love Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Will You Still Love Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marukathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Maaikkumo Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Maaikkumo Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irappena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kaanaa Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kaanaa Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marukathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Maaikkumo Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Maaikkumo Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irappena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irappena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kaanaa Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kaanaa Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Ennai Neenginaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ennai Neenginaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Ketka Marukkindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ketka Marukkindrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaa Pala Noorugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaa Pala Noorugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ketka Thudikkindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ketka Thudikkindrathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooram Poga Nenaikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram Poga Nenaikkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Irukkenda Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irukkenda Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Iruppenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Iruppenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavume Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavume Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come to Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come to Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelaatho Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaatho Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kooda Vaalatha Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kooda Vaalatha Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraatho Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraatho Meendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaan Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaan Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velagaama Naaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velagaama Naaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaalvil Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaalvil Meendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Saagum Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Saagum Thooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Kaayam Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kaayam Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooram Poyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram Poyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaraladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Maayam Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Maayam Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sandaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sandaali"/>
</div>
</pre>
