---
title: 'nee vaanavilla song lyrics'
album: 'Aadai'
artist: 'Oorka'
lyricist: 'Bharath Sankar'
director: 'Rathnakumar'
path: '/albums/aadai-song-lyrics'
song: 'Nee Vaanavilla'
image: ../../images/albumart/aadai.jpg
date: 2019-07-19
lang: tamil
singers: 
- Shakthishree Gopalan
youtubeLink: "https://www.youtube.com/embed/BcxuxNTy2Fw"
type: 'philosophy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Nee Vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee Vaanavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam thedum vella poovaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vannam thedum vella poovaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee paarkum porulaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee paarkum porulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarpathellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee paarpathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee Vaanavilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee Vaanavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam thedum vella poovaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vannam thedum vella poovaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee paarkum porulaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee paarkum porulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarpathellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee paarpathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sarivaa nenachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sarivaa nenachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriya thudikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Piriya thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangum usurukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalangum usurukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum karaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukkum karaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhikka nenaikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhikka nenaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivu paththala (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Arivu paththala"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey kaalam undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey kaalam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum thaeva
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum thaeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vaanam undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey vaanam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaazhkai thaeva
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vaazhkai thaeva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mmm…mmm…mmm…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mmm…mmm…mmm…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm…mm….mmmm…mm..
<input type="checkbox" class="lyrico-select-lyric-line" value="Mmm…mm….mmmm…mm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmm…mm…mm…mm..
<input type="checkbox" class="lyrico-select-lyric-line" value="Mmmm…mm…mm…mm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm…mmm…mm……..
<input type="checkbox" class="lyrico-select-lyric-line" value="Mmm…mmm…mm…….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kanaa vedhachi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanaa vedhachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaya kadakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Karaya kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varunthum manasukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Varunthum manasukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasiyum periya
<input type="checkbox" class="lyrico-select-lyric-line" value="Kasiyum periya"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiya adakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Valiya adakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivu theriyala (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudivu theriyala"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey kaalam undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey kaalam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum thaeva
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum thaeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vaanam undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey vaanam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyum undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaazhkai thaeva
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vaazhkai thaeva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa.a..aaa…aaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa.a..aaa…aaa…aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa…aa…haaa……
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aa…haaa……"/>
</div>
</pre>