---
title: "kelambu song lyrics"
album: "Goli Soda 2"
artist: "Achu Rajamani"
lyricist: "Mani Amuthavan"
director: "Vijay Milton"
path: "/albums/goli-soda-2-lyrics"
song: "Kelambu"
image: ../../images/albumart/goli-soda-2.jpg
date: 2018-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zRybJK4xaH0"
type: "mass"
singers:
  - Deepak
  - Mahalingam
  - Jithin Raj
  - Sreeraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aathiram thalaikeruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathiram thalaikeruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanal aguthae thaniyamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanal aguthae thaniyamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En veetiyil unakennada vensamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veetiyil unakennada vensamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayutham eduppenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayutham eduppenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirppenada erippenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirppenada erippenada"/>
</div>
<div class="lyrico-lyrics-wrapper">En padhaiyil nee vaipadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En padhaiyil nee vaipadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaal thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaal thadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethai eppodhu enneram seikindrathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai eppodhu enneram seikindrathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniyum iniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyum iniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sellathu nee seiyum rajangamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sellathu nee seiyum rajangamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enna nee enna naattamaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enna nee enna naattamaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En sollai nee pesa naan umaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sollai nee pesa naan umaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyil un kaiyil naan paavaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiyil un kaiyil naan paavaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oododi poo undhan uyir thevaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oododi poo undhan uyir thevaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elai kandu thalai aatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elai kandu thalai aatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan ondrum aadillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan ondrum aadillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Erai kandu adhil maatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erai kandu adhil maatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan ondrum meenillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan ondrum meenillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee koodaram poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee koodaram poottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil kottaram poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil kottaram poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam ingae than illai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam ingae than illai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En veettai veettai nee aalvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veettai veettai nee aalvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee potta koottukul naan valvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee potta koottukul naan valvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen endru ketkaamal naal poovadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen endru ketkaamal naal poovadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vazha nee vazha naan savadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vazha nee vazha naan savadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimelum kuniyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelum kuniyadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada porumaikum undellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada porumaikum undellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai meeri nuzhainthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai meeri nuzhainthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thalai vaangu thappuillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thalai vaangu thappuillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu ennoda koottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ennoda koottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru enkaiyil saattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru enkaiyil saattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thol pinchu thongum munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thol pinchu thongum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambu"/>
</div>
</pre>
