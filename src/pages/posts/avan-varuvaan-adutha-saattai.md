---
title: "avan varuvaan song lyrics"
album: "Adutha Saattai"
artist: "Justin Prabhakaran"
lyricist: "Thenmozhi Das"
director: "M. Anbazhagan"
path: "/albums/adutha-saattai-lyrics"
song: "Avan Varuvaan"
image: ../../images/albumart/adutha-saattai.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Qdd9M1IX3Ns"
type: "melody"
singers:
  - Sathyan Ilanko
  - Aishwarya Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Avan Varuvaan Ena Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Varuvaan Ena Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Puraakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Puraakkal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Vandhaan Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Vandhaan Naan Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilaakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaakkal Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanukkenna Azhagiya Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukkenna Azhagiya Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukkenna Avanadhu Agam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukkenna Avanadhu Agam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarukkenna Avanadhu Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarukkenna Avanadhu Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Vaa Un Devathai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Vaa Un Devathai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaagam Theera Thaabangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theera Thaabangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayathaaga Theriyaa Kaadhalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayathaaga Theriyaa Kaadhalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalil Oorum Kaamangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil Oorum Kaamangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Saaga Theriyaa Deivangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Saaga Theriyaa Deivangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Varuvaal Ena Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Varuvaal Ena Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Puraakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Puraakkal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Vandhaal Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Vandhaal Naan Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilaakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaakkal Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhum Undhan Nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum Undhan Nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinil Suzhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinil Suzhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Purandodiduthe Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Purandodiduthe Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paritchaya Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paritchaya Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pareetchaiyil Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pareetchaiyil Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiperum Varai Vidukadhaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiperum Varai Vidukadhaiyaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomai Baashai Pesidum Vaguppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomai Baashai Pesidum Vaguppil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkum Paadam Pinnani Isaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkum Paadam Pinnani Isaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrin Thondra Kaadhalin Thodaril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondrin Thondra Kaadhalin Thodaril"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai Yaar Thaan Solvar Mudhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai Yaar Thaan Solvar Mudhalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alayaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alayaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadukalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadukalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avizhum Koondhal Salanangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avizhum Koondhal Salanangalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paarvai Engo Hey Eyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarvai Engo Hey Eyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugirathe Namm Ulagariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugirathe Namm Ulagariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaagam Theera Thaabangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theera Thaabangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayathaaga Theriyaa Kaadhalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayathaaga Theriyaa Kaadhalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalil Oorum Kaamangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil Oorum Kaamangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Saaga Theriyaa Deivangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Saaga Theriyaa Deivangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Varuvaal Ena Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Varuvaal Ena Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Puraakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Puraakkal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Puraakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Puraakkal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Vandhaal Naan Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Vandhaal Naan Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilaakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaakkal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennilaakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilaakkal Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalukkenna Azhagiya Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukkenna Azhagiya Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukkenna Avaladhu Agam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukkenna Avaladhu Agam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarukkenna Avaladhu Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarukkenna Avaladhu Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Vaa En Devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Vaa En Devathaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Varuvaan Ena Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Varuvaan Ena Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Puraakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Puraakkal Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Varuvaan Ena Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Varuvaan Ena Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven Puraakkal Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven Puraakkal Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaagam Theera Thaabangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theera Thaabangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayathaaga Theriyaa Kaadhalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayathaaga Theriyaa Kaadhalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalil Oorum Kaamangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil Oorum Kaamangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Saaga Theriyaa Deivangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Saaga Theriyaa Deivangal"/>
</div>
</pre>
