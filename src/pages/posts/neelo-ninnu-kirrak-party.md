---
title: "neelo ninnu song lyrics"
album: "Kirrak Party"
artist: "B Ajaneesh Loknath"
lyricist: "Ramajogayya Sastry"
director: "Sharan Koppisetty"
path: "/albums/kirrak-party-lyrics"
song: "Neelo Ninnu"
image: ../../images/albumart/kirrak-party.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gQM-1Mx1sA4"
type: "love"
singers:
  -	Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nèèlo Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèèlo Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Choosaa Nènu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosaa Nènu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai ètu Kanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai ètu Kanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Paapanu Maralinchanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapanu Maralinchanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Ninnu Daachèsaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Ninnu Daachèsaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porapaatunaa Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porapaatunaa Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">adiginaa Tirigivvanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiginaa Tirigivvanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Mari èmi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Mari èmi "/>
</div>
<div class="lyrico-lyrics-wrapper">Choosindo Adigèlopugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosindo Adigèlopugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Padi Doosukèlutundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Padi Doosukèlutundi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Kalavaipugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Kalavaipugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaga Bhaga Bhaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga Bhaga Bhaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vèlugu Nimpukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vèlugu Nimpukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidaralu Chèruputu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidaralu Chèruputu "/>
</div>
<div class="lyrico-lyrics-wrapper">Tirigè Sooryudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirigè Sooryudaa"/>
</div>
è<div class="lyrico-lyrics-wrapper">da Sadi Tèlupukoga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="da Sadi Tèlupukoga "/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Dorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Dorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nèèlo Prèmikudèkkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèèlo Prèmikudèkkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvantè Naaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvantè Naaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi Prèmaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi Prèmaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatallo Daanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatallo Daanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Chèppalènuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chèppalènuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallallona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallallona "/>
</div>
<div class="lyrico-lyrics-wrapper">Mèrupu Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mèrupu Choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Paatè Adi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Paatè Adi "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadutundiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadutundiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Mari èmi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Mari èmi "/>
</div>
<div class="lyrico-lyrics-wrapper">Choosindo Adigèlopugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosindo Adigèlopugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Padi Doosukèlutundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Padi Doosukèlutundi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Kalavaipugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Kalavaipugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nèè Roopam Nèè 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Roopam Nèè "/>
</div>
<div class="lyrico-lyrics-wrapper">Roopam Apuroopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopam Apuroopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvulèni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvulèni "/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Lopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Lopam"/>
</div>
è<div class="lyrico-lyrics-wrapper">do Chèbutondi èdalotu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do Chèbutondi èdalotu "/>
</div>
<div class="lyrico-lyrics-wrapper">Lotunèè Nè Raanaa Jata Kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotunèè Nè Raanaa Jata Kaanaa"/>
</div>
è<div class="lyrico-lyrics-wrapper">dilaa Manasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilaa Manasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Andinchu Vèèlugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andinchu Vèèlugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prèmagaa Daanipai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prèmagaa Daanipai "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pèrè Raayagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pèrè Raayagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nèèlo Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèèlo Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Choosaa Nènu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosaa Nènu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai ètu Kanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai ètu Kanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Paapanu Maralinchanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapanu Maralinchanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Mari èmi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Mari èmi "/>
</div>
<div class="lyrico-lyrics-wrapper">Choosindo Adigèlopugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosindo Adigèlopugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Padi Doosukèlutundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Padi Doosukèlutundi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Kalavaipugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Kalavaipugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaga Bhaga Bhaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga Bhaga Bhaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vèlugu Nimpukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vèlugu Nimpukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidaralu Chèruputu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidaralu Chèruputu "/>
</div>
<div class="lyrico-lyrics-wrapper">Tirigè Sooryudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirigè Sooryudaa"/>
</div>
è<div class="lyrico-lyrics-wrapper">da Sadi Tèlupukoga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="da Sadi Tèlupukoga "/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Dorakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Dorakadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nèèlo Prèmikudèkkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèèlo Prèmikudèkkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvantè Naaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvantè Naaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Picchi Prèmaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picchi Prèmaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatallo Daanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatallo Daanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Chèppalènuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chèppalènuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kallallona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kallallona "/>
</div>
<div class="lyrico-lyrics-wrapper">Mèrupu Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mèrupu Choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Paatè 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Paatè "/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Paadutundiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Paadutundiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Mari èmi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Mari èmi "/>
</div>
<div class="lyrico-lyrics-wrapper">Choosindo Adigèlopugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosindo Adigèlopugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Padi Doosukèlutundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Padi Doosukèlutundi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nèè Kalavaipugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nèè Kalavaipugaa"/>
</div>
</pre>
