---
title: "evanukku engeyo matcham irukku song lyrics"
album: "Evanukku Engeyo Matcham Irukku"
artist: "Natarajan Sankaran"
lyricist: "Viveka"
director: "A R Mukesh"
path: "/albums/evanukku-engeyo-matcham-irukku-lyrics"
song: "Evanukku Engeyo Matcham Irukku"
image: ../../images/albumart/evanukku-engeyo-matcham-irukku.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/61s8ztFnDrQ"
type: "title song"
singers:
  - Sharanya Gopinath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evanukku engeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahhahahahahahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahhahahahahahaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
420 420 420
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo ooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
420
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
420
<span class="lyrico-song-lyrics-breaker"></span>
420
<div class="lyrico-lyrics-wrapper">Vanga kadal naduvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanga kadal naduvilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki pottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki pottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayil meenai kavvi varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayil meenai kavvi varuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melae keezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melae keezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallai ellam adukki vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai ellam adukki vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Medai katti ninnu sirippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medai katti ninnu sirippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhipathu nari mugamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhipathu nari mugamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan poranthathae sarithiramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan poranthathae sarithiramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
420
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanukku engeyo ooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanukku engeyo ooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Matcham irukkuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matcham irukkuu"/>
</div>
</pre>