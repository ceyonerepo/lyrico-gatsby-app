---
title: "mazhai song lyrics"
album: "Kaalidas"
artist: "Vishal Chandrasekhar"
lyricist: "Thamarai"
director: "Sri Senthil"
path: "/albums/kaalidas-lyrics"
song: "Mazhai"
image: ../../images/albumart/kaalidas.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WmMH8ZXU24A"
type: "love"
singers:
  - Sudha Raghunathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai Nanaiye Vaithathu Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nanaiye Vaithathu Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Mayanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Mayanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Karayai Theendiya Thirunaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Karayai Theendiya Thirunaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kiranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kiranginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Naan Paartha Muthal Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Paartha Muthal Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhnthene Etha Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhnthene Etha Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kan Ennum Siraginal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kan Ennum Siraginal"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Anaithethal Nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Anaithethal Nenjodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Androdu Aasai Koobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Androdu Aasai Koobam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithilum Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithilum Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavesam Vanthal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavesam Vanthal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangum Thee Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangum Thee Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Yaaroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Yaaroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalathi Theroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalathi Theroda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Nanaiye Vaithathu Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nanaiye Vaithathu Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Mayanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Mayanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Karayai Theendiya Thirunaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Karayai Theendiya Thirunaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kiranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kiranginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhil Mugam Kaanamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhil Mugam Kaanamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vendrum Kehlamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vendrum Kehlamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhiliyaai Aanathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhiliyaai Aanathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muruvaalgal Thaaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muruvaalgal Thaaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthamida Vaaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthamida Vaaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Vaithu Pohnathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Vaithu Pohnathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhigal Enathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Enathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Enathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavu Unathallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavu Unathallavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathu Unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathu Unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marathi Unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marathi Unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Enathallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Enathallavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Ula Varaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Ula Varaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Nanaiye Vaithathu Oru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nanaiye Vaithathu Oru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Mayanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Mayanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Karayai Theendiya Thirunaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Karayai Theendiya Thirunaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kiranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kiranginen"/>
</div>
</pre>
