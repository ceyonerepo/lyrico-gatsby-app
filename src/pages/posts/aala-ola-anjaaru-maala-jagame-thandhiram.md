---
title: "aala ola song lyrics"
album: "Jagame Thandhiram"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Karthik Subbaraj"
path: "/albums/jagame-thandhiram-song-lyrics"
song: "Aala Ola Anjaaru Maala"
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2021-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BX9uoVR3SVo"
type: "mass"
singers:
  - Andony Dasan
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ae mapla ale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ae mapla ale"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">ah superu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah superu"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">ae gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ae gaana rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">pottiya oru adiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottiya oru adiya "/>
</div>
<div class="lyrico-lyrics-wrapper">podu maapla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu maapla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">injaara maaplainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="injaara maaplainga"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvamla mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvamla mela"/>
</div>
<div class="lyrico-lyrics-wrapper">thoranaya vandhu ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoranaya vandhu ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">vappomla kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vappomla kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">inganakkula top adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inganakkula top adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">yeldraya than kootuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeldraya than kootuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">oodala poondhu vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodala poondhu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">olattivitu oattuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olattivitu oattuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara vachaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara vachaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara vachaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara vachaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu"/>
</div>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru maala"/>
</div>
<div class="lyrico-lyrics-wrapper">thodha eduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodha eduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thol mela poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thol mela poda"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara "/>
</div>
<div class="lyrico-lyrics-wrapper">thappatta odhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappatta odhara"/>
</div>
<div class="lyrico-lyrics-wrapper">theriya uduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriya uduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nammoda powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammoda powera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madhurai uruththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhurai uruththu"/>
</div>
<div class="lyrico-lyrics-wrapper">sillaattam sillaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillaattam sillaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">odambu valayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu valayum "/>
</div>
<div class="lyrico-lyrics-wrapper">villaattam villaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villaattam villaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">adi yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi yen"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana rathiname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana rathiname"/>
</div>
<div class="lyrico-lyrics-wrapper">adi yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi yen"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana rathiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana rathiname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru maala"/>
</div>
<div class="lyrico-lyrics-wrapper">thodha eduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodha eduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thol mela poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thol mela poda"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara "/>
</div>
<div class="lyrico-lyrics-wrapper">thappatta odhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappatta odhara"/>
</div>
<div class="lyrico-lyrics-wrapper">theriya uduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriya uduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nammoda powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammoda powera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orranda onnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orranda onnaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">orranda rendaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orranda rendaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">orranda izhukkum nallavainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orranda izhukkum nallavainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urumi onnaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urumi onnaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">izhuva rendaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="izhuva rendaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">savva kizhikka vandhavainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savva kizhikka vandhavainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yey sound ah mattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yey sound ah mattum "/>
</div>
<div class="lyrico-lyrics-wrapper">vachu udu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu udu"/>
</div>
<div class="lyrico-lyrics-wrapper">salla yellam pathi udu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salla yellam pathi udu"/>
</div>
<div class="lyrico-lyrics-wrapper">un koottama en koottama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koottama en koottama"/>
</div>
<div class="lyrico-lyrics-wrapper">un kootathula aalirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kootathula aalirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee anuppu paathukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee anuppu paathukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">un aattamaa en aattamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aattamaa en aattamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara vachaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara vachaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara vachaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara vachaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu"/>
</div>
<div class="lyrico-lyrics-wrapper">aala ola anjaaru maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala ola anjaaru maala"/>
</div>
<div class="lyrico-lyrics-wrapper">thodha eduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodha eduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thol mela poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thol mela poda"/>
</div>
<div class="lyrico-lyrics-wrapper">aara thaara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aara thaara "/>
</div>
<div class="lyrico-lyrics-wrapper">thappatta odhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappatta odhara"/>
</div>
<div class="lyrico-lyrics-wrapper">theriya uduyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriya uduyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nammoda powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammoda powera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sillaattam sillaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillaattam sillaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">villaattam villaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villaattam villaattam"/>
</div>
</pre>
