---
title: "vanakkam vanakkamunga song lyrics"
album: "Pariyerum Perumal"
artist: "Santhosh Narayanan"
lyricist: "Perumal Vaathiyar"
director: "Mari Selvaraj"
path: "/albums/pariyerum-perumal-lyrics"
song: "Vanakkam Vanakkamunga"
image: ../../images/albumart/pariyerum-perumal.jpg
date: 2018-09-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Gc_Dy-dD96s"
type: "happy"
singers:
  - Puliyankulam Velmayil
  - Puliyankulam Kannan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aiyaa vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunamulla thaaimaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunamulla thaaimaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunamulla thaaimaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunamulla thaaimaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyya thoothukudi maavattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya thoothukudi maavattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruvaikundam thaalu gramam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruvaikundam thaalu gramam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoothukudi maavattama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoothukudi maavattama"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruvaikundam thaalu gramam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruvaikundam thaalu gramam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumkulam pakkathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumkulam pakkathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagana puliyam kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagana puliyam kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumkulam pakkathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumkulam pakkathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagana puliyam kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagana puliyam kulam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naangal pirantha oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal pirantha oor"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavodu vazhum oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavodu vazhum oor"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal pirantha oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal pirantha oor"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavodu vazhum oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavodu vazhum oor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunamulla thaaimaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunamulla thaaimaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyya yenthar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyya yenthar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthar pariyumga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthar pariyumga"/>
</div>
<div class="lyrico-lyrics-wrapper">En guruvin peru arkaarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En guruvin peru arkaarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthar yenthar pariyumga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthar yenthar pariyumga"/>
</div>
<div class="lyrico-lyrics-wrapper">En guruvin peru arkaarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En guruvin peru arkaarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam pattu segar jegan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam pattu segar jegan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijaya kannanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijaya kannanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam pattu segar jegan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam pattu segar jegan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijaya kannanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijaya kannanunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga ullatha sollarumunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ullatha sollarumunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththu kolla venumunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththu kolla venumunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ullatha sollarumunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ullatha sollarumunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththu kolla venumunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththu kolla venumunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunamulla thaaimaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunamulla thaaimaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa aadugindra aadalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa aadugindra aadalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugindra paadalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugindra paadalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadugindra aadalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadugindra aadalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugindra paadalilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugindra paadalilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollil kovam koodi irukkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollil kovam koodi irukkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollil kovam koodi irukkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollil kovam koodi irukkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla nanbana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla nanbana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga kettu kolla venumunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga kettu kolla venumunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga nanbana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga nanbana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga kettu kolla venumunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga kettu kolla venumunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanakkam vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasamulla sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasamulla sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunamulla thaaimaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunamulla thaaimaarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittom kumbittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittom kumbittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi vanakkamunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi vanakkamunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Appada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appada"/>
</div>
</pre>
