---
title: "nenjamundu nermaiyundu odu raja song lyrics"
album: "Nenjamundu Nermaiyundu Odu Raja"
artist: "Shabir"
lyricist: "Kannadasan"
director: "Karthik Venugopalan"
path: "/albums/nenjamundu-nermaiyundu-odu-raja-lyrics"
song: "Nenjamundu Nermaiyundu Odu Raja"
image: ../../images/albumart/nenjamundu-nermaiyundu-odu-raja.jpg
date: 2019-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Zkq4sVVR1bs"
type: "motivational"
singers:
  - Shabir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjam Undu Nermai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Undu Nermai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Neram Varum Kaathirundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neram Varum Kaathirundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Anji Anji Vaazhndhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anji Anji Vaazhndhathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aatru Vellam Pol Ezhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aatru Vellam Pol Ezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimaiyin Udambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaiyin Udambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam Edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Achappatta Kozhaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Achappatta Kozhaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Illam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illam Edharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adimaiyin Udambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaiyin Udambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam Edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Achappatta Kozhaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Achappatta Kozhaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Illam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illam Edharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodumaiyai Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodumaiyai Kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodumaiyai Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodumaiyai Kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam Edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kondu Vandhathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kondu Vandhathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Murukku Heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Murukku Heyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undu Undu Endru Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu Undu Endru Nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Unnai Vittaal Boomiyethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Unnai Vittaal Boomiyethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undu Undu Endru Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu Undu Endru Nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Unnai Vittaal Boomiyethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Unnai Vittaal Boomiyethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendil Ondru Paarpadharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendil Ondru Paarpadharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Nimirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Nimirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendil Ondru Paarpadharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendil Ondru Paarpadharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Nimirthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Nimirthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Needhi Unnai Thedi Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Needhi Unnai Thedi Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Thoduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Thoduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Undu Nermai Undu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Undu Nermai Undu Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Neram Varum Kaathirundhu Paaru Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neram Varum Kaathirundhu Paaru Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Anji Anji Vaazhndathu Pothum Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anji Anji Vaazhndathu Pothum Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aatru Vellam Pol Ezhunthu Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aatru Vellam Pol Ezhunthu Odu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Undu Nermai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Undu Nermai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Neram Varum Kaathirundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neram Varum Kaathirundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Anji Anji Vaazhndhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anji Anji Vaazhndhathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aatru Vellam Pol Ezhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aatru Vellam Pol Ezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara Rara Rara Rara Rara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara Rara Rara Rara Rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Dham Ram Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Dham Ram Dham"/>
</div>
</pre>
