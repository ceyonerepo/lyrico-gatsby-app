---
title: "yaar ivan song lyrics"
album: "Oomai Sennaai"
artist: "Siva"
lyricist: "Prakash Baskar"
director: "Arjunan Ekalaivan"
path: "/albums/oomai-sennaai-song-lyrics"
song: "Yaar Ivan"
image: ../../images/albumart/oomai-sennaai.jpg
date: 2021-12-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SY7yJAjAHps"
type: "melody"
singers:
  - Shakthisree Gopalan
  - Siva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vizhigal Dhinamum Theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Dhinamum Theduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiyam Kaetkum Yaar Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiyam Kaetkum Yaar Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal Indre Mudiyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Indre Mudiyudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aekkam Konden Yaar Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aekkam Konden Yaar Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinjal Polae Irundhen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjal Polae Irundhen Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattraai Nee Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattraai Nee Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil Pol Naan Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Pol Naan Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna Saaral Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Saaral Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Yaavum Mudiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Yaavum Mudiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaeneer Koappai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeneer Koappai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vidiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vidiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Kaanaal Neerai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kaanaal Neerai Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Neram Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Neram Thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhil Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetkum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetkum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Irandil Nee Nindraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Irandil Nee Nindraaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Aedhum Vazhiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Aedhum Vazhiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Naanum Marandhu Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naanum Marandhu Ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarththa Nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarththa Nodiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalaa Ena Kaetkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaa Ena Kaetkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Unnai Kaattudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Unnai Kaattudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai Naanum Pesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai Naanum Pesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Theduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Yaavum Mudiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Yaavum Mudiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaeneer Koappai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeneer Koappai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Vidiyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Vidiyume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Kaanaal Neerai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kaanaal Neerai Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Neram Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Neram Thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhil Raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetkum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetkum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigal Dhinamum Theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Dhinamum Theduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiyam Kaetkum Yaar Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiyam Kaetkum Yaar Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal Indre Mudiyudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Indre Mudiyudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aekkam Konde Yaar Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aekkam Konde Yaar Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilinjal Polae Irundhen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinjal Polae Irundhen Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattraai Nee Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattraai Nee Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanavil Pol Naan Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Pol Naan Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna Saaral Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna Saaral Nee"/>
</div>
</pre>
