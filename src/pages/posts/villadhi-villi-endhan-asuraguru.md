---
title: "villadhi villi endhan song lyrics"
album: "Asuraguru"
artist: "Ganesh Raghavendra"
lyricist: "Kabilan Vairamuthu"
director: "A. Raajdheep"
path: "/albums/asuraguru-song-lyrics"
song: "Villadhi Villi Endhan"
image: ../../images/albumart/asuraguru.jpg
date: 2020-03-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TIfGq028rAs"
type: "Love"
singers:
  - Naresh Iyer
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Villadhi Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villadhi Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Pakkam Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Pakkam Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoomgaamal Thupparinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoomgaamal Thupparinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanjulla Kallan Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjulla Kallan Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjulla Nallan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjulla Nallan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandarinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandarinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidu Sidu Vena Seenditaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidu Sidu Vena Seenditaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadugudu Ena Aaditaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadugudu Ena Aaditaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Para Paravena Dhinamum Porittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Paravena Dhinamum Porittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh En Thiramai Vaasithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh En Thiramai Vaasithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thanimai Nesithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thanimai Nesithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai Maariya Thendral Pola Nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Maariya Thendral Pola Nindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaduppanai Yedhum Illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaduppanai Yedhum Illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Thodar Naanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Thodar Naanthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraayudhan Unnai Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraayudhan Unnai Kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Satre Nindrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satre Nindrene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaavadi Vaadham Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaavadi Vaadham Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaaladi Penn Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaaladi Penn Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaudin Bootham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaudin Bootham Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaai Poothene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaai Poothene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thavariya Sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thavariya Sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraivinil Athu Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraivinil Athu Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Vizhigal Thoongume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vizhigal Thoongume"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nizhal Ulagam Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nizhal Ulagam Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninai Azhaikkum Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninai Azhaikkum Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulai Vittu Veliye Varuvaai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulai Vittu Veliye Varuvaai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Villadhi Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villadhi Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Pakkam Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Pakkam Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoomgaamal Thupparinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoomgaamal Thupparinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimaigal Yedhum Illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaigal Yedhum Illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunigaram Naanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunigaram Naanthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Manogari Undhan Natpil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manogari Undhan Natpil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavvam Kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavvam Kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavuthal Yedhum Indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavuthal Yedhum Indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaa Varum Aanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaa Varum Aanthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalebara Kanni Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalebara Kanni Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parivil Sarinthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parivil Sarinthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai Maraigira Ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Maraigira Ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurai Ulla Oru Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurai Ulla Oru Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Mudhalaai Maatram Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Mudhalaai Maatram Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thariketta Oru Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thariketta Oru Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai Erangidum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai Erangidum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aravanaikkum Boomi Paadhai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aravanaikkum Boomi Paadhai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Villadhi Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villadhi Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Pakkam Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Pakkam Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoomgaamal Thupparinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoomgaamal Thupparinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi Aanaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanjulla Kallan Endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanjulla Kallan Endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennam Kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennam Kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjulla Nallan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjulla Nallan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandarinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandarinthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegu Naalaai Kuri Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Naalaai Kuri Vaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Thadayangal Sodhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Thadayangal Sodhithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Siraiyida Valigal Theditten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Siraiyida Valigal Theditten"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Un Saridhai Vaasithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Un Saridhai Vaasithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thanimai Nesithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thanimai Nesithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena Sila Kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Sila Kelvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Parisaai Thandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisaai Thandhen"/>
</div>
</pre>
