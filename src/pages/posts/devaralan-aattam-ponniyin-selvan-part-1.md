---
title: devaralan aattam song lyrics
album: Ponniyin Selvan Part - 1
artist: A.R.Rahman
lyricist: Ilango Krishnan
director: Maniratnam
path: /albums/ponniyin-selvan-part-1-lyrics
song: Devaralan Aattam
image: ../../images/albumart/ponniyin-selvan-part-1.jpg
date: 2022-11-25
lang: tamil
youtubeLink: https://www.youtube.com/embed/PaTBeGcDfHg
type: sad
singers:
  - Yogi Sekar
---

<pre class="lyrics-native"></pre>
<pre class="lyrics-english">
   <div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Chek chek chek chekapana</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chekapana</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chek chek</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chekapana</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chek chek chek chekapana</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Patt patt pada patt</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt pada patt</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt pada patt</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Patt patt patt patt patt patt patt</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt patt patt patt patt</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt padavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt padavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt padavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt padavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt padavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patt patt patt padavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Oon patra ketta udalai</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal petru kettalikkave</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oon patra ketta udalai</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal petru kettalikkave</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oon patra ketta udalai</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal petru kettalikkave</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Sudanathu sudanathu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudanathu yutham</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudanathu sudanathu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudanathu ratham</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Poraduthu poraduthu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraduthu sitham</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathathu theerathathu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathathu verisaththam</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Sudanathu sudanathu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudanathu yutham</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudanathu sudanathu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudanathu ratham</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Poraduthu poraduthu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraduthu sitham</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathathu theerathathu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathathu verisaththam</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Kothuparai kothuparai</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kothuparai kottu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathacheru rathacheru</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathacheru vettu</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Kotta pagai kotta pagai</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotta pagai vettu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuttaseyal thuttaseyal</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuttaseyal kattu</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Cheruvettalai pesidudhae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manuketunai odhidudhae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thatchani theeyudane</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai aatridava peyane</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dam dam dam dam dam damare</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Cheruvettalai pesidudhae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manuketunai odhidudhae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thatchani theeyudane</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai aatridava peyane</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Senkurudhi seiyonae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vankodiya velonae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chevalari tholanae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kudiya kapponae</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadampa idumpa muruga</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathirvel kumara marutha</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudivel arasarkarasae</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadivel arulvaai malarvaai</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mamazhai peidhiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manilam ongiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oppukazhi thangiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigalum ongiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Varunda kodada eduda</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvai tharuvaai udane</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sega sega segavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennira kurudhiyai kottu</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Varunda kodada eduda</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvai tharuvaai udane</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sega sega segavena</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennira kurudhiyai kottu</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mamazhai peidhiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manilam ongiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oppukazhi thangiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigalum ongiduma</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Kotupparai kotelundhida</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuttuppagai ketalindhida</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kochakudi pattathoruvanin</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathathinai kotti baliyidu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chutta bali kettaal sankari</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathathinai kotti baliyidu</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Vendan kudi ketaal boodhavi</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathathinai kotti baliyidu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilaiyon thalai ketaal bairavi</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rathathinai kotti baliyidu ((4) times)</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Balikodu balikodu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balikodu balikodu</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Balikodu balikodu…….</div></pre>