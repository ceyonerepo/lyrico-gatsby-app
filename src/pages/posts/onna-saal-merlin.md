---
title: "onna saal song lyrics"
album: "Merlin"
artist: "Ganesh Raghavendra"
lyricist: "Keera"
director: "Keera"
path: "/albums/merlin-song-lyrics"
song: "Onna Saal"
image: ../../images/albumart/merlin.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/B4kS4SShNrg"
type: "melody"
singers:
  - Guru
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manairaya unne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manairaya unne"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadu neraya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadu neraya "/>
</div>
<div class="lyrico-lyrics-wrapper">oothu neer ooranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothu neer ooranum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendan saal rende
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendan saal rende"/>
</div>
<div class="lyrico-lyrics-wrapper">neer oora nilam ooranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer oora nilam ooranum"/>
</div>
<div class="lyrico-lyrics-wrapper">moonanjal moone man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonanjal moone man"/>
</div>
<div class="lyrico-lyrics-wrapper">kolanju payir thulirkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolanju payir thulirkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">naalanjal naale payir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalanjal naale payir"/>
</div>
<div class="lyrico-lyrics-wrapper">thulirka ver vavanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulirka ver vavanum"/>
</div>
<div class="lyrico-lyrics-wrapper">anjanjaal anje ver 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjanjaal anje ver "/>
</div>
<div class="lyrico-lyrics-wrapper">padarnthu poo kaniyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padarnthu poo kaniyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">aaranjaal aare poo kanunju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaranjaal aare poo kanunju"/>
</div>
<div class="lyrico-lyrics-wrapper">kai peruganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai peruganum"/>
</div>
<div class="lyrico-lyrics-wrapper">elanjaal elee kai perugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elanjaal elee kai perugi"/>
</div>
<div class="lyrico-lyrics-wrapper">kulam thalaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulam thalaikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">entanjaal entee kulam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entanjaal entee kulam "/>
</div>
<div class="lyrico-lyrics-wrapper">thalachu oor selikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalachu oor selikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">onbathanjaal onbathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onbathanjaal onbathe"/>
</div>
<div class="lyrico-lyrics-wrapper">oor selichu naadu malaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor selichu naadu malaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">namma naadu malaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma naadu malaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">aama naadu malaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aama naadu malaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthana nane naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthana nane naane"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthana nane naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthana nane naane"/>
</div>
<div class="lyrico-lyrics-wrapper">thane thane thana nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane thane thana nane"/>
</div>
</pre>
