---
title: "oru nila oru kulam song lyrics"
album: "Ilaignan"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Suresh Krishna"
path: "/albums/ilaignan-lyrics"
song: "Oru Nila Oru Kulam"
image: ../../images/albumart/ilaignan.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O4LU2NcTepY"
type: "love"
singers:
  - Karthik
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nedum Pagal Neenda Kanavu Nijamaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedum Pagal Neenda Kanavu Nijamaagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nilaa Oru Kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nilaa Oru Kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mazhai Oru Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mazhai Oru Kudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Poagum Oru Vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Poagum Oru Vizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Manam Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Manam Oru Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Imai Oru Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Imai Oru Kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Poathum Oru Yugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Poathum Oru Yugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kazhal Thiraiyithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kazhal Thiraiyithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nizhal Iru Padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nizhal Iru Padam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Poagum Oru Thavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Poagum Oru Thavam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nilaa Oru Kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nilaa Oru Kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mazhai Oru Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mazhai Oru Kudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Poagum Oru Vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Poagum Oru Vizhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nilaa Oru Kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nilaa Oru Kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mazhai Oru Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mazhai Oru Kudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Poagum Oru Vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Poagum Oru Vizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Ottiya Mun Pani Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Ottiya Mun Pani Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyai Otriya Oli Viral Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyai Otriya Oli Viral Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Minniya Minnal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Minniya Minnal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Thooriya Thaazhai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Thooriya Thaazhai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Santham Konjiya Seiyul Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santham Konjiya Seiyul Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyul Sinthiya Santham Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyul Sinthiya Santham Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam Kavviya Veppam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam Kavviya Veppam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppam Thanigira Nutpam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Thanigira Nutpam Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Koodum Muthal Thanimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Koodum Muthal Thanimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nilaa Oru Kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nilaa Oru Kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mazhai Oru Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mazhai Oru Kudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Poagum Oru Vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Poagum Oru Vizhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Manam Oru Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Manam Oru Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Imai Oru Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Imai Oru Kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Poathum Oru Yugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Poathum Oru Yugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal Konjiya Manmatham Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Konjiya Manmatham Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjal Minjiya Kolmuthal Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjal Minjiya Kolmuthal Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhigal Kenjiya Mounam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhigal Kenjiya Mounam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Malargira Kavithai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Malargira Kavithai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviya Èzhuthum Azhagiyal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviya Èzhuthum Azhagiyal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Varaigira Thøørigai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Varaigira Thøørigai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Neettiya Veezhvisai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Neettiya Veezhvisai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Pøøttiya Ithazhisai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Pøøttiya Ithazhisai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Køathum Puthu Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Køathum Puthu Ulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nilaa Oru Kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nilaa Oru Kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Mazhai Oru Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Mazhai Oru Kudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Pøagum Oru Vizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Pøagum Oru Vizhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Manam Oru Šugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Manam Oru Šugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Imai Oru Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Imai Oru Kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Pøathum Oru Yugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Pøathum Oru Yugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kazhal Thiraiyithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kazhal Thiraiyithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nizhal Iru Padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nizhal Iru Padam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naan Pøagum Oru Thavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naan Pøagum Oru Thavam"/>
</div>
</pre>
