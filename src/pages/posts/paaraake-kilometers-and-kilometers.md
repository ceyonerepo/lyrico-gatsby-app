---
title: "paaraake song lyrics"
album: "Kilometers and Kilometers"
artist: "Sooraj S. Kurup"
lyricist: "Sooraj S. Kurup - Vinayak Sasikumar"
director: "Jeo Baby"
path: "/albums/kilometers-and kilometers-lyrics"
song: "Paaraake"
image: ../../images/albumart/kilometers-and-kilometers.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/a2fu1Mt3U3w"
type: "happy"
singers:
  - Sooraj S. Kurup
  - Ramshi Ahamed
  - Mridul Anil
  - Pavithra Das
  - Pranavya Das
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kisi ne bathaaya mujhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ne bathaaya mujhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nehi jayega re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nehi jayega re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi bhi toh ho jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi bhi toh ho jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Geet Mohabbat hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geet Mohabbat hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisi ne bathaaya mujhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ne bathaaya mujhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nehi jayega re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nehi jayega re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi bhi toh ho jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi bhi toh ho jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Geet Mohabbat hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geet Mohabbat hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisi ne bathaaya mujhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ne bathaaya mujhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nehi jayega re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nehi jayega re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi bhi toh ho jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi bhi toh ho jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Geet Mohabbat hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geet Mohabbat hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisi ne bathaaya mujhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ne bathaaya mujhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nehi jayega re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nehi jayega re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi bhi toh ho jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi bhi toh ho jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Geet Mohabbat hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geet Mohabbat hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aha aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Toorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Toorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaavaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaavaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha aha aha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha aha aha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kana Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana Pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudum thaapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudum thaapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on lets take the road
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on lets take the road"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal thekam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal thekam"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on lets speed it up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on lets speed it up"/>
</div>
<div class="lyrico-lyrics-wrapper">Manal paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manal paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on lets take the road
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on lets take the road"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum podhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum podhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraake Padaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraake Padaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaalm Thudaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaalm Thudaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Irave Nee Pakaraamo Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irave Nee Pakaraamo Nila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraake Padaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraake Padaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaalm Thudaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaalm Thudaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Irave Nee Pakaraamo Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irave Nee Pakaraamo Nila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisi ne bathaaya mujhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ne bathaaya mujhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nehi jayega re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nehi jayega re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi bhi toh ho jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi bhi toh ho jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Geet Mohabbat hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geet Mohabbat hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kisi ne bathaaya mujhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisi ne bathaaya mujhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nehi jayega re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nehi jayega re"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi bhi toh ho jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi bhi toh ho jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Geet Mohabbat hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geet Mohabbat hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum Maargavathintel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Maargavathintel"/>
</div>
<div class="lyrico-lyrics-wrapper">Manassile Kaarum Kolum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manassile Kaarum Kolum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaake Marinjidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaake Marinjidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pular Naalamathinkamanasume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pular Naalamathinkamanasume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarum Thaalivedinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarum Thaalivedinju"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidarnnidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidarnnidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraake Padaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraake Padaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaalm Thudaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaalm Thudaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Irave Nee Pakaraamo Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irave Nee Pakaraamo Nila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnaayi Pala Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaayi Pala Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorangal Palathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorangal Palathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandunnu Raavum Pakalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandunnu Raavum Pakalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanthore Kavaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthore Kavaraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Man Paathayoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man Paathayoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokunnu Nammal Iniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokunnu Nammal Iniyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Thoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Thoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on let's take the road
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on let's take the road"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on let's speed it up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on let's speed it up"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaavaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaavaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on let's take the road
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on let's take the road"/>
</div>
<div class="lyrico-lyrics-wrapper">Kana pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraake Padaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraake Padaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaalm Thudaraame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaalm Thudaraame"/>
</div>
<div class="lyrico-lyrics-wrapper">Irave Nee Pakaraamo Nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irave Nee Pakaraamo Nila"/>
</div>
</pre>
