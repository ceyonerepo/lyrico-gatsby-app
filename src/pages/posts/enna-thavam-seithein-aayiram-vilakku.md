---
title: "enna thavam seithein song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Enna Thavam Seithein"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UfHNh5k7V2Y"
type: "happy"
singers:
  - K.J. Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna thavam seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaippeththa maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaippeththa maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallukkullae eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukkullae eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanacheidha maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanacheidha maganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thavam seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaippeththa maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaippeththa maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallukkullae eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukkullae eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanacheidha maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanacheidha maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththivandha pillai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththivandha pillai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththivandha pillai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththivandha pillai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Petra thandhai naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petra thandhai naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaiyaana pinbu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaiyaana pinbu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum pillayaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum pillayaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thavam seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaippeththa maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaippeththa maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallukkullae eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukkullae eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanacheidha maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanacheidha maganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buththikkul vanmurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththikkul vanmurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarthirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarthirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathil velaanmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathil velaanmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathivandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathivandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalodu velgal rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalodu velgal rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidiththa kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidiththa kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannaththu poochiyai pidiththukkonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannaththu poochiyai pidiththukkonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasathinaalae paaviyin vaazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathinaalae paaviyin vaazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththanai rusigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanai rusigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaththiganaaga vaazhndhavan arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaththiganaaga vaazhndhavan arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iththanai kadavulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanai kadavulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moovar mattum aalgira dhesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moovar mattum aalgira dhesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal vaazhvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal vaazhvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam ondrae dhesiya geedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam ondrae dhesiya geedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae aanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En viral naduvae idaiveli edharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En viral naduvae idaiveli edharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Un viral korththu uravaadathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral korththu uravaadathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarangal marandhu vilaiyaadathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarangal marandhu vilaiyaadathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonthangal poi endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthangal poi endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaiththirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaiththirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorkkathin nagal endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorkkathin nagal endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Therindhukkonden aen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therindhukkonden aen"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhangal baaramendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhangal baaramendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruththirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruththirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Baarangal parisendru purindhukkonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baarangal parisendru purindhukkonden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutrum thurandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutrum thurandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Motcham enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motcham enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivargal sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivargal sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattrum anbum pagirvadhu thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattrum anbum pagirvadhu thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Motcham enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motcham enbadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanavamellaam anbil karaindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavamellaam anbil karaindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha vellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha vellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram aandugal boomiyil vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram aandugal boomiyil vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathirai vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathirai vendumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi kuraindhu arai adiyaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi kuraindhu arai adiyaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanmagam madiyil maganaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmagam madiyil maganaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhkadal migundha malaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhkadal migundha malaiyaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thavam seidhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thavam seidhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaippeththa maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaippeththa maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallukkullae eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallukkullae eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanacheidha maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanacheidha maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththivandha pillai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththivandha pillai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththivandha pillai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththivandha pillai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Petra thandhai naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petra thandhai naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaiyaana pinbu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaiyaana pinbu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum pillayaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum pillayaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa haa haaa aaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa haa haaa aaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaa haaaaahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaa haaaaahaaa"/>
</div>
</pre>
