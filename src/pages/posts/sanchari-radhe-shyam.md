---
title: "sanchari song lyrics"
album: "Radhe Shyam"
artist: "Justin Prabhakaran"
lyricist: "Krishna Kanth"
director: "Radha Krishna Kumar"
path: "/albums/radhe-shyam-lyrics"
song: "Sanchari"
image: ../../images/albumart/radhe-shyam.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OfhRUsaFOso"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oho Oho Oho Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Oho Oho Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho Oho Oho Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Oho Oho Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottha Nelapai Oho Oho Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Nelapai Oho Oho Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali Santhakam Oho Oho Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Santhakam Oho Oho Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondagaalitho Oho Oho Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondagaalitho Oho Oho Oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwaasa Pampakam Oho Oho Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasa Pampakam Oho Oho Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taricha Hridayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taricha Hridayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaduthu Sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaduthu Sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelichaa Prathi Shikharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelichaa Prathi Shikharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Ho OoOo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Ho OoOo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brathuke Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathuke Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadiley Jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadiley Jagadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Panche Manche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Panche Manche"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Neeke Dorakadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Neeke Dorakadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal Chalo Chalo Sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chalo Chalo Sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal Chalo Chalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chalo Chalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chethanaithe Lokamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethanaithe Lokamanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayi Nimpeyy Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayi Nimpeyy Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mailu Raayi Leni Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mailu Raayi Leni Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Ante Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Ante Oo Oo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhe Chinna Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhe Chinna Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Prathikshanam Brathikeyaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Prathikshanam Brathikeyaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripeyy Anchanaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripeyy Anchanaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Vishwam Mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Vishwam Mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Neeke Dhorukuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Neeke Dhorukuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal Chalo Chalo Sanchari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chalo Chalo Sanchari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal Chalo Chalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal Chalo Chalo"/>
</div>
</pre>
