---
title: "kanna nee thoongada song lyrics"
album: "Baahubali 2"
artist: "M.M. Keeravani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli"
path: "/albums/baahubali-2-song-lyrics"
song: "Kanna Nee Thoongada"
image: ../../images/albumart/baahubali-2.jpg
date: 2017-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EDQK8l-VPZs"
type: "Love"
singers:
  - Nayane Nair
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">muraithaanaa mugundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraithaanaa mugundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saridhaana sanandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saridhaana sanandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muraidhaana mugundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraidhaana mugundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saridhanna sanandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saridhanna sanandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">muraidhaana mugundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraidhaana mugundha"/>
</div>
<div class="lyrico-lyrics-wrapper">saridhaanaa sanandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saridhaanaa sanandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poovayar meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovayar meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kan meyvadhu muraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan meyvadhu muraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">paavai en nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavai en nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam theigindra pirayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam theigindra pirayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poathume nee konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poathume nee konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyil kolladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyil kolladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">un viralinil malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un viralinil malai"/>
</div>
<div class="lyrico-lyrics-wrapper">sumanthadhu podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sumanthadhu podhume"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">un ithazhinil kuzhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ithazhinil kuzhal"/>
</div>
<div class="lyrico-lyrics-wrapper">isaithathu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaithathu pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kobiyar kulikkayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kobiyar kulikkayile"/>
</div>
<div class="lyrico-lyrics-wrapper">udaigal thirudi kalaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaigal thirudi kalaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">oyvedu maayavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyvedu maayavane"/>
</div>
<div class="lyrico-lyrics-wrapper">paanayil vennaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanayil vennaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinamum thirudi ilaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinamum thirudi ilaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongidu thooyavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongidu thooyavane"/>
</div>
<div class="lyrico-lyrics-wrapper">saa... manaa, mo... ganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saa... manaa, mo... ganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee seyyum thiruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seyyum thiruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">suzhndhathu iruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhndhathu iruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">maarbil saaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarbil saaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">soolain naduvinela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soolain naduvinela"/>
</div>
<div class="lyrico-lyrics-wrapper">nuzhalainthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuzhalainthan"/>
</div>
<div class="lyrico-lyrics-wrapper">alaindhan tholainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaindhan tholainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unatharuginile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unatharuginile"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanagam naduvinele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanagam naduvinele"/>
</div>
<div class="lyrico-lyrics-wrapper">mayangi erangi kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayangi erangi kidanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unadhazhaginile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unadhazhaginile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maa... dhavaa, yaa...dhavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa... dhavaa, yaa...dhavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neelai seidhi ennai nee kavzhilka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelai seidhi ennai nee kavzhilka"/>
</div>
<div class="lyrico-lyrics-wrapper">paalai moodhi unnaium kavzhilka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paalai moodhi unnaium kavzhilka"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayam ennal kondiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayam ennal kondiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna nee thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">en kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanna nee thoongada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muraithaanaa mugundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraithaanaa mugundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saridhaanaa sanandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saridhaanaa sanandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muraithaanaa mugundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraithaanaa mugundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saridhaanaa sanandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saridhaanaa sanandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">muraithaanaa mugundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraithaanaa mugundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saridhaanaa sanandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saridhaanaa sanandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madana madhusoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madana madhusoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">manohoaraa manimohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manohoaraa manimohana"/>
</div>
<div class="lyrico-lyrics-wrapper">madana madhusoodhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madana madhusoodhana"/>
</div>
<div class="lyrico-lyrics-wrapper">manohoaraa manimohana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manohoaraa manimohana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muraidhaanaa muguntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraidhaanaa muguntha"/>
</div>
<div class="lyrico-lyrics-wrapper">sarithana sananthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarithana sananthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aananda anirudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aananda anirudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aananda anirudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aananda anirudhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna kanna kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kanna kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">krishna radhaa ramanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishna radhaa ramanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">krishnaa, kanna nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishnaa, kanna nee thoongada"/>
</div>
</pre>
