---
title: "superstar song lyrics"
album: "Puppy"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Nattu Dev"
path: "/albums/puppy-lyrics"
song: "Superstar"
image: ../../images/albumart/puppy.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3rWy8crRE7o"
type: "happy"
singers:
  - Anirudh Ravichander
  - MCD
  - Dharan Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pasanga Life-ah Poratti Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Life-ah Poratti Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Problems Thaanda Irukkum Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problems Thaanda Irukkum Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Solutions Kudukka Aalu Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solutions Kudukka Aalu Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Andhu Pochu Dangu Vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Andhu Pochu Dangu Vaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandaiyila Padippu Yerla Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaiyila Padippu Yerla Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Tuition Pona Romba Bore-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tuition Pona Romba Bore-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Medical-ku Neet-ah Pottan Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medical-ku Neet-ah Pottan Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inju Tea-ah Poduraru Engineeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inju Tea-ah Poduraru Engineeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sappa Moonji Kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappa Moonji Kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippovara Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippovara Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Propose Pannathillaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Propose Pannathillaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaya Pola Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaya Pola Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Figure Onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Figure Onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Thedi Odurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Thedi Odurendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Single-unnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Single-unnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Status Onnu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Status Onnu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Scene-ah Kaatturendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Scene-ah Kaatturendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaa Paththu Mani Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa Paththu Mani Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Net Pack-ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Net Pack-ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bit-u Padam Paakurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bit-u Padam Paakurendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasanga Life-a Poratti Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Life-a Poratti Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Problems Thaanda Irukkum Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problems Thaanda Irukkum Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Passion-eh Thedi Poren Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passion-eh Thedi Poren Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaanda Ini Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaanda Ini Superstaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Superstaru Superstaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstaru Superstaru Superstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Superstaru Superstaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstaru Superstaru Superstaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Passion Thedi Poran Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passion Thedi Poran Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel Neethan Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Neethan Superstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Passion Thedi Poran Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passion Thedi Poran Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel Neethan Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Neethan Superstaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Neram Paathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Neram Paathu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poojai Pottakul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poojai Pottakul"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamula Puppy Vaala Aattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamula Puppy Vaala Aattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala Aattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala Aattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Neram Paathu Thaan Poojai Pottakul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Neram Paathu Thaan Poojai Pottakul"/>
</div>
<div class="lyrico-lyrics-wrapper">Gomalam Puppy Vaala Aattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gomalam Puppy Vaala Aattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gents Pair-ukku Hunt-ah Potta Puppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gents Pair-ukku Hunt-ah Potta Puppy"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakka Kuduthu Pasanga Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakka Kuduthu Pasanga Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Yeah Yeah Yeaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah Yeah Yeaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Yrah Yah Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Yrah Yah Yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaiya Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaiya Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Aaga Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Aaga Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Nenjil Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nenjil Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Thaanga Poren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Thaanga Poren Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badam Pisinu Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badam Pisinu Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paathukka Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paathukka Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Minimum Paththu Kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minimum Paththu Kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Peththu Poden Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Peththu Poden Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Peru Puppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Peru Puppy"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Veettukulla Chellakutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veettukulla Chellakutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala Valaikka Paakkuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Valaikka Paakkuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Valaiya Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Valaiya Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta Vechi Poova Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Vechi Poova Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiya Pinni Powder Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiya Pinni Powder Kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Sevathukulla Anupuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Sevathukulla Anupuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Selai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Selai Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Ennai Sooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Ennai Sooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhavu Rendu Mooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavu Rendu Mooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nelama Enga Poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nelama Enga Poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Polappum Light-ah Maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Polappum Light-ah Maaruthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dogs-eh Pootti Vechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dogs-eh Pootti Vechi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Door-eh Thorakka Poren Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Door-eh Thorakka Poren Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Patti Thotti Full-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Patti Thotti Full-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Peru Thaana Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Peru Thaana Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasanga Life-a Poratti Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Life-a Poratti Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Problems Thaanda Irukkum Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problems Thaanda Irukkum Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Passion-eh Thedi Poren Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passion-eh Thedi Poren Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaanda Ini Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaanda Ini Superstaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Superstaru Superstaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstaru Superstaru Superstaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Within 80 Days-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Within 80 Days-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Wait-u Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wait-u Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Porakkum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Porakkum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Puppy Mummy Aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puppy Mummy Aagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambala Thaan Massa Kaattum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambala Thaan Massa Kaattum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Doggu Peru Famous List-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doggu Peru Famous List-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Yera Pothu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yera Pothu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Oorukulla Nambala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Oorukulla Nambala Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Minja Yaaru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minja Yaaru Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Passion Thedi Poran Paaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passion Thedi Poran Paaru Superstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel Neethan Superstaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Neethan Superstaru Superstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Passion Thedi Poran Paaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passion Thedi Poran Paaru Superstaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel Neethan Superstaru Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Neethan Superstaru Superstaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Superstaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstaru"/>
</div>
</pre>
