---
title: "aasaiya aasaiya song lyrics"
album: "Pettikadai"
artist: "Mariya Manohar"
lyricist: "Snehan"
director: "Esakki Karvannan"
path: "/albums/pettikadai-lyrics"
song: "Aasaiya Aasaiya"
image: ../../images/albumart/pettikadai.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AMQq9qCTNEw"
type: "love"
singers:
  - Shreya Ghoshal
  - Satyan Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aasaiya aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiya aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya veanum un tholula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya veanum un tholula"/>
</div>
<div class="lyrico-lyrics-wrapper">eppa nee varuviyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppa nee varuviyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kulantha pola madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulantha pola madiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vearkireyne yeanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vearkireyne yeanada"/>
</div>
<div class="lyrico-lyrics-wrapper">un sparisathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sparisathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thorkireane naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorkireane naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiveli ippo kuraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiveli ippo kuraiyuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini imsai thaaney hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini imsai thaaney hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasaiya aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiya aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya veanum un tholula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya veanum un tholula"/>
</div>
<div class="lyrico-lyrics-wrapper">eppa nee varuviyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppa nee varuviyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kulantha pola madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulantha pola madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasai valarnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai valarnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadai thlarnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadai thlarnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">theham melinthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theham melinthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaa azhake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa azhake"/>
</div>
<div class="lyrico-lyrics-wrapper">vettupatta paambu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettupatta paambu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ushurum ulla thudikithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ushurum ulla thudikithey"/>
</div>
<div class="lyrico-lyrics-wrapper">athai naan unarnthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai naan unarnthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">iravu sooriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu sooriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaanada"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa paniyea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa paniyea"/>
</div>
<div class="lyrico-lyrics-wrapper">nee enakkul karainthuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enakkul karainthuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasaiya aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiya aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya veanum un tholula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya veanum un tholula"/>
</div>
<div class="lyrico-lyrics-wrapper">eppa nee varuviyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppa nee varuviyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kulantha pola madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulantha pola madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoratti paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoratti paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai yizhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai yizhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">muttha mazhaiyil kolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttha mazhaiyil kolladi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa methuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa methuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">settha neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="settha neram"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan maarbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan maarbil"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu vaazhach seiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu vaazhach seiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum udalum unathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum udalum unathey"/>
</div>
<div class="lyrico-lyrics-wrapper">iravu sorriyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravu sorriyane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa arukae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa arukae"/>
</div>
<div class="lyrico-lyrics-wrapper">hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasaiya aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiya aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saaya veanum un tholula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaya veanum un tholula"/>
</div>
<div class="lyrico-lyrics-wrapper">eppa nee varuviyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppa nee varuviyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kulantha pola madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulantha pola madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vearkireyne yea pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vearkireyne yea pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">un sparisathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sparisathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thorkirene yean pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorkirene yean pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiveli ippo kuraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiveli ippo kuraiyuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini imsai thaane hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini imsai thaane hum"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiveli ippo kuraiyuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiveli ippo kuraiyuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini imsai thaane hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini imsai thaane hum"/>
</div>
</pre>
