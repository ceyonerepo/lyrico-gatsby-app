---
title: "varayaadha oviyam song lyrics"
album: "Koorman"
artist: "Tony Britto"
lyricist: "Uma Devi "
director: "Bryan B. George"
path: "/albums/koorman-lyrics"
song: "Varayaadha Oviyam"
image: ../../images/albumart/koorman.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Bs1crwEQhDE"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">varayaadha oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varayaadha oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">pilai aagi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilai aagi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thirai podum vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirai podum vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">irul thaana nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irul thaana nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">malai kaaga yengidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai kaaga yengidum"/>
</div>
<div class="lyrico-lyrics-wrapper">veyil kaayum manam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyil kaayum manam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini mel naam vaalvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini mel naam vaalvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam pol aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam pol aanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">manal mele thudithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manal mele thudithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">anil pole thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anil pole thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinamaga nadanthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinamaga nadanthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal naan vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal naan vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en anbe unnil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en anbe unnil vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">sollal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sonna sollil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonna sollil vaalgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varayaadha oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varayaadha oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">pilai aagi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilai aagi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thirai podum vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirai podum vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">irul thaana nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irul thaana nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">manal mele thudithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manal mele thudithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">anil pole thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anil pole thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinamaga nadanthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinamaga nadanthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en anbe unnil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en anbe unnil vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">sollal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sonna sollil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonna sollil vaalgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">penne ingu unnal thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne ingu unnal thane"/>
</div>
<div class="lyrico-lyrics-wrapper">artham aagum enthan aayul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham aagum enthan aayul"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moochi pothum kathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moochi pothum kathale"/>
</div>
<div class="lyrico-lyrics-wrapper">kannal ennai vellum paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannal ennai vellum paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">kandenadi konjum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandenadi konjum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">mei pola poyanai kathali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei pola poyanai kathali"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivale ninaivale niraigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivale ninaivale niraigindren"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir pogum thinam vendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir pogum thinam vendi"/>
</div>
<div class="lyrico-lyrics-wrapper">manal mele thudithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manal mele thudithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">anil pole thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anil pole thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinamaga nadanthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinamaga nadanthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en anbe unnil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en anbe unnil vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">sollal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sonna sollil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonna sollil vaalgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en thogaiyil vannam serkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thogaiyil vannam serkum"/>
</div>
<div class="lyrico-lyrics-wrapper">un saayalil anbin pookal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un saayalil anbin pookal"/>
</div>
<div class="lyrico-lyrics-wrapper">pen vaasam un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen vaasam un "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kan pesidum manjal poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan pesidum manjal poove"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaanathu minnal theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaanathu minnal theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">un nabaga achil moolgi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nabaga achil moolgi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unaladi vilaiyada ninaithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaladi vilaiyada ninaithene"/>
</div>
<div class="lyrico-lyrics-wrapper">sulal moolgi thavithe than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulal moolgi thavithe than"/>
</div>
<div class="lyrico-lyrics-wrapper">manal mele thudithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manal mele thudithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">anil pole thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anil pole thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinamaga nadanthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinamaga nadanthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en anbe unnil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en anbe unnil vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">sollal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sonna sollil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonna sollil vaalgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varayaadha oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varayaadha oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">pilai aagi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pilai aagi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thirai podum vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirai podum vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">irul thaana nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irul thaana nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">manal mele thudithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manal mele thudithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">anil pole thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anil pole thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinamaga nadanthu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinamaga nadanthu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unnal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en anbe unnil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en anbe unnil vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">sollal vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollal vaalgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee sonna sollil vaalgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee sonna sollil vaalgiren"/>
</div>
</pre>
