---
title: "venditera dora song lyrics"
album: "NTR Mahanayakudu"
artist: "M.M. Keeravani"
lyricist: "M.M. Keeravani"
director: "Krish Jagarlamudi"
path: "/albums/ntr-mahanayakudu-lyrics"
song: "Venditera Dora"
image: ../../images/albumart/ntr-mahanayakudu.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XHgfwB8WCMk"
type: "sad"
singers:
  - M.M. Keeravani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Venditera doraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venditera doraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venditera doraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venditera doraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venditera doraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venditera doraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinava mora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinava mora"/>
</div>
<div class="lyrico-lyrics-wrapper">Venditera doraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venditera doraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinu ma mora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinu ma mora"/>
</div>
<div class="lyrico-lyrics-wrapper">Venditera doraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venditera doraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinu ma mora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinu ma mora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uppene shapamai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppene shapamai "/>
</div>
<div class="lyrico-lyrics-wrapper">kannakalalu kaastamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakalalu kaastamai"/>
</div>
<div class="lyrico-lyrics-wrapper">kattukunna goollu pagili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattukunna goollu pagili "/>
</div>
<div class="lyrico-lyrics-wrapper">thellavaru sariki bathuku chidramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thellavaru sariki bathuku chidramai"/>
</div>
<div class="lyrico-lyrics-wrapper">rudhramai devudaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rudhramai devudaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venditera doraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venditera doraaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinu maa mora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinu maa mora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lekkathelani shavala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lekkathelani shavala "/>
</div>
<div class="lyrico-lyrics-wrapper">padhutelchagaligithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhutelchagaligithey"/>
</div>
<div class="lyrico-lyrics-wrapper">kotidevullake porlu dhandalayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotidevullake porlu dhandalayya"/>
</div>
<div class="lyrico-lyrics-wrapper">bhikkubhikkumantunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhikkubhikkumantunna "/>
</div>
<div class="lyrico-lyrics-wrapper">chavaleka bathikinolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chavaleka bathikinolla"/>
</div>
<div class="lyrico-lyrics-wrapper">kantineeru sandramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kantineeru sandramai"/>
</div>
<div class="lyrico-lyrics-wrapper">uppenayyenayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppenayyenayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli kushalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli kushalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">bidda padhilamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bidda padhilamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thana chirunamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana chirunamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ey diviseema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ey diviseema"/>
</div>
<div class="lyrico-lyrics-wrapper">bhuvana vijayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhuvana vijayama"/>
</div>
<div class="lyrico-lyrics-wrapper">kavula rajyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavula rajyama"/>
</div>
<div class="lyrico-lyrics-wrapper">koruthunnadi pattedannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koruthunnadi pattedannamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venditera doraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venditera doraa"/>
</div>
<div class="lyrico-lyrics-wrapper">vinu maa moraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinu maa moraaa"/>
</div>
</pre>
