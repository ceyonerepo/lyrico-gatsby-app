---
title: "kanney karuvizhie song lyrics"
album: "Market Raja MBBS"
artist: "Simon K. King"
lyricist: "Ku. Karthik"
director: "Saran"
path: "/albums/market-raja-mbbs-lyrics"
song: "Kanney Karuvizhie"
image: ../../images/albumart/market-raja-mbbs.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kEJ391k7-k0"
type: "sad"
singers:
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannae Karuvizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae Karuvizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyaa Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyaa Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhama En Madiyil Vizhuntha Nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhama En Madiyil Vizhuntha Nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivangal Kettaalum Kidaikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivangal Kettaalum Kidaikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Varamae En Varamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Varamae En Varamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae Karuvizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae Karuvizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyaa Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyaa Kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhama En Madiyil Vizhuntha Nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhama En Madiyil Vizhuntha Nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Deivangal Kettaalum Kidaikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deivangal Kettaalum Kidaikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Varamae En Varamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Varamae En Varamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mudhal Muraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mudhal Muraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Naan Sirichenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Naan Sirichenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaala Vanthaayae En Selvamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaala Vanthaayae En Selvamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oor Ulagam Arinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oor Ulagam Arinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Osara Valarnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Osara Valarnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Madiyil Thalaatti Thaangiduvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Madiyil Thalaatti Thaangiduvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarariro Aariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarariro Aariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro Adichathaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro Adichathaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarariro Aariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarariro Aariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro Adichathaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro Adichathaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai Mozhivena Kural Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Mozhivena Kural Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaama Iruppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaama Iruppena"/>
</div>
<div class="lyrico-lyrics-wrapper">En Karuvin Vaasatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Karuvin Vaasatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Marappena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappena"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Boologam Azhinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Boologam Azhinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poongakaathu Erinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongakaathu Erinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kodukkum Paasatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kodukkum Paasatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Korachiduvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korachiduvena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan Raththatha Unavaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Raththatha Unavaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhuthellaam Kanavaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthellaam Kanavaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhinjalae Paasatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhinjalae Paasatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pol Varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pol Varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thaai Illa Ulagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thaai Illa Ulagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Sootti Thanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Sootti Thanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaattu Paattukku Eedaagiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaattu Paattukku Eedaagiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarariro Aariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarariro Aariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro Adichathaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro Adichathaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarariro Aariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarariro Aariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Yaaro Adichathaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaaro Adichathaaro"/>
</div>
</pre>
