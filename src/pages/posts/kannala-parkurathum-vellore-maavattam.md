---
title: "kannala parkurathum song lyrics"
album: "Vellore Maavattam"
artist: "Sundar C Babu"
lyricist: "Viveka"
director: "R.N.R. Manohar"
path: "/albums/vellore-maavattam-lyrics"
song: "Kannala Parkurathum"
image: ../../images/albumart/vellore-maavattam.jpg
date: 2011-10-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WhDzfdpWB4o"
type: "love"
singers:
  - Anitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannaala paarkkuradhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala paarkkuradhum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhaala ketkuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhaala ketkuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayaala pesuradhum poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayaala pesuradhum poi"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyellaam meiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyellaam meiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">meiyellaam poiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meiyellaam poiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">emmela kaiya konjam vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emmela kaiya konjam vai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un iduppa paarthaa enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iduppa paarthaa enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un madippa paartha enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madippa paartha enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un iduppa paarthaa enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un iduppa paarthaa enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un madippa paartha enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madippa paartha enguthamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyillaama Vellure-kku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyillaama Vellure-kku "/>
</div>
<div class="lyrico-lyrics-wrapper">eppadi nee thoadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppadi nee thoadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennazhagathaan paarthupputtu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennazhagathaan paarthupputtu "/>
</div>
<div class="lyrico-lyrics-wrapper">oorey soodaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorey soodaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">pudichadhu konjam nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudichadhu konjam nee "/>
</div>
<div class="lyrico-lyrics-wrapper">thal endru poaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thal endru poaneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Odugira paalaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odugira paalaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">thaanaa veenaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaanaa veenaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">paarvaikku yethura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvaikku yethura "/>
</div>
<div class="lyrico-lyrics-wrapper">aalunga Ottura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalunga Ottura"/>
</div>
<div class="lyrico-lyrics-wrapper">paaraalumandram poagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaraalumandram poagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">ye vervaikku visirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye vervaikku visirena"/>
</div>
<div class="lyrico-lyrics-wrapper">aalunga thool ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalunga thool ena"/>
</div>
<div class="lyrico-lyrics-wrapper">podhukkoottam ondru poadalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhukkoottam ondru poadalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee poagura dhisaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee poagura dhisaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">poo ondru serndhuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo ondru serndhuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mookkumela virala vaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookkumela virala vaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un iduppa paarthaa enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un iduppa paarthaa enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un madippa paartha enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madippa paartha enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un iduppa paarthaa enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un iduppa paarthaa enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un madippa paartha enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madippa paartha enguthamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podi podi pasangalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi podi pasangalum "/>
</div>
<div class="lyrico-lyrics-wrapper">perisunga koottamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perisunga koottamum"/>
</div>
<div class="lyrico-lyrics-wrapper">adikkadi en vaasalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikkadi en vaasalil "/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">pudavainga kaayidhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudavainga kaayidhey "/>
</div>
<div class="lyrico-lyrics-wrapper">kuri enna paarkkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuri enna paarkkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en veettu theruvula ottikkedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en veettu theruvula ottikkedakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paaduren poosura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paaduren poosura "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasanai vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasanai vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">paththu ooru thaandi adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paththu ooru thaandi adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paanaiyum theendunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paanaiyum theendunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nodiyellaam nodiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodiyellaam nodiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">poaraaga maari irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaraaga maari irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un veettil theepidichadhil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veettil theepidichadhil "/>
</div>
<div class="lyrico-lyrics-wrapper">moadhina nilavukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moadhina nilavukku"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu koosudhu yedho aachidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu koosudhu yedho aachidi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un iduppa paarthaa enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iduppa paarthaa enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un madippa paartha enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madippa paartha enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un iduppa paarthaa enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un iduppa paarthaa enguthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un madippa paartha enguthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madippa paartha enguthamaa"/>
</div>
</pre>
