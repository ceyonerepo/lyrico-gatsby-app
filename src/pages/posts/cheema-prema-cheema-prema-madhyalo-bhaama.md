---
title: "cheema prema song lyrics"
album: "Cheema Prema Madhyalo Bhaama"
artist: "Ravi Varma Potedar"
lyricist: "Srikanth “Sri” Appalaraju"
director: "Srikanth “Sri” Appalaraju"
path: "/albums/cheema-prema-madhyalo-bhaama-lyrics"
song: "Cheema Prema"
image: ../../images/albumart/cheema-prema-madhyalo-bhaama.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cY8N-tUZRcI"
type: "title track"
singers:
  - S.P.Balu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">cheema prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheema prema"/>
</div>
<div class="lyrico-lyrics-wrapper">madhyalo bhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhyalo bhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">vishayam vindaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vishayam vindaama"/>
</div>
<div class="lyrico-lyrics-wrapper">viviram kandaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viviram kandaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponchi undi pramaadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponchi undi pramaadam"/>
</div>
<div class="lyrico-lyrics-wrapper">ekkadundi pramodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekkadundi pramodam"/>
</div>
<div class="lyrico-lyrics-wrapper">adugaduguna gandaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugaduguna gandaale"/>
</div>
<div class="lyrico-lyrics-wrapper">anthuleni thantaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthuleni thantaale"/>
</div>
<div class="lyrico-lyrics-wrapper">daarunam daarunam daarunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daarunam daarunam daarunam"/>
</div>
<div class="lyrico-lyrics-wrapper">daanavudee maanavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daanavudee maanavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">dayaa gunam lenodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dayaa gunam lenodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalenno vintunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalenno vintunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">charithalenno kantunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="charithalenno kantunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalenno vintunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalenno vintunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">charithalenno kantunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="charithalenno kantunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">andarini minchinodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andarini minchinodu"/>
</div>
<div class="lyrico-lyrics-wrapper">avanilone monagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanilone monagaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">maanavude mahaneeyudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanavude mahaneeyudu"/>
</div>
<div class="lyrico-lyrics-wrapper">ade nijam antunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ade nijam antunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manishi goppa entanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manishi goppa entanta"/>
</div>
<div class="lyrico-lyrics-wrapper">mundu choopu edanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundu choopu edanta"/>
</div>
<div class="lyrico-lyrics-wrapper">team spirit ledanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="team spirit ledanta"/>
</div>
<div class="lyrico-lyrics-wrapper">economy theliyadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="economy theliyadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">manishi goppa entanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manishi goppa entanta"/>
</div>
<div class="lyrico-lyrics-wrapper">mundu choopu edanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundu choopu edanta"/>
</div>
<div class="lyrico-lyrics-wrapper">team spirit ledanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="team spirit ledanta"/>
</div>
<div class="lyrico-lyrics-wrapper">economy theliyadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="economy theliyadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">paapaala pathithulanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapaala pathithulanta"/>
</div>
<div class="lyrico-lyrics-wrapper">paniki raani yadavalanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniki raani yadavalanta"/>
</div>
<div class="lyrico-lyrics-wrapper">raakaasi hrudayamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raakaasi hrudayamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">raabandhula chandamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raabandhula chandamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">korikokati puttindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korikokati puttindi"/>
</div>
<div class="lyrico-lyrics-wrapper">kotha kothagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotha kothagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">madine melipettindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madine melipettindi"/>
</div>
<div class="lyrico-lyrics-wrapper">meththa meththagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meththa meththagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chiththu thithugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chiththu thithugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manishigaa puttaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manishigaa puttaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu padda cheemaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu padda cheemaro"/>
</div>
<div class="lyrico-lyrics-wrapper">kudurugaa undaleka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudurugaa undaleka"/>
</div>
<div class="lyrico-lyrics-wrapper">kalala kadali munigero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalala kadali munigero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalache napudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalache napudu"/>
</div>
<div class="lyrico-lyrics-wrapper">thandri thaathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandri thaathala"/>
</div>
<div class="lyrico-lyrics-wrapper">thapasu okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapasu okate"/>
</div>
<div class="lyrico-lyrics-wrapper">thoche throvaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoche throvaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siva deeksha saranandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siva deeksha saranandi"/>
</div>
<div class="lyrico-lyrics-wrapper">japa yagnam jaripinithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="japa yagnam jaripinithi"/>
</div>
<div class="lyrico-lyrics-wrapper">mondi pattu pattindee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mondi pattu pattindee"/>
</div>
<div class="lyrico-lyrics-wrapper">mundukadugu vesindee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundukadugu vesindee"/>
</div>
<div class="lyrico-lyrics-wrapper">avasaramaa idi avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avasaramaa idi avasaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ipudidi avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipudidi avasaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">intha risk avasaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha risk avasaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vintha kaaka inkemiti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vintha kaaka inkemiti"/>
</div>
<div class="lyrico-lyrics-wrapper">manche jaruguthundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manche jaruguthundo"/>
</div>
<div class="lyrico-lyrics-wrapper">mari minne viruguthundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari minne viruguthundo"/>
</div>
<div class="lyrico-lyrics-wrapper">maaye jaruguthundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaye jaruguthundo"/>
</div>
<div class="lyrico-lyrics-wrapper">mari emi oruguthundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari emi oruguthundo"/>
</div>
<div class="lyrico-lyrics-wrapper">cheema maare dheera gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheema maare dheera gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maari poye hero gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari poye hero gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">history uliki padindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="history uliki padindi"/>
</div>
<div class="lyrico-lyrics-wrapper">ee story raasukunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee story raasukunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">mystery ika mundundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mystery ika mundundi"/>
</div>
<div class="lyrico-lyrics-wrapper">maastaru mahaa sivudandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maastaru mahaa sivudandi"/>
</div>
<div class="lyrico-lyrics-wrapper">cheema prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheema prema "/>
</div>
<div class="lyrico-lyrics-wrapper">madhyalo bhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhyalo bhaama"/>
</div>
</pre>
