---
title: "mellaga mellaga song lyrics"
album: "Thellavarithe Guruvaram"
artist: "Kaala Bhairava"
lyricist: "Raghuram"
director: "Manikanth Gelli"
path: "/albums/thellavarithe-guruvaram-lyrics"
song: "Mellaga Mellaga Dhaarule"
image: ../../images/albumart/thellavarithe-guruvaram.jpg
date: 2021-03-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6OqG2Vt5Csc"
type: "love"
singers:
  - Kaala Bhairava
  - Sahithi Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mellagaa Mellagaa Dhaarule Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellagaa Mellagaa Dhaarule Maarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthagaa Kotthagaa Payaname Choopenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthagaa Kotthagaa Payaname Choopenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati Aashe Maani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati Aashe Maani"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati Oose Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati Oose Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kshaname Edhuraindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kshaname Edhuraindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammani Pilichindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammani Pilichindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekati Needanu Dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekati Needanu Dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekuva Vaakililoki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekuva Vaakililoki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalatho Egirindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalatho Egirindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalo Manasilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo Manasilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere Unnattundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Unnattundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Challani Gaalai Cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challani Gaalai Cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Sneham Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Sneham Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaare Thagginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaare Thagginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalakshepam Chesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalakshepam Chesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwaasai Maarettugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasai Maarettugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo..! Unnattundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo..! Unnattundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Challani Gaalai Cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challani Gaalai Cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Sneham Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Sneham Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaare Thagginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaare Thagginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalakshepam Chesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalakshepam Chesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwaasai Maarettugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasai Maarettugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mellagaa Mellagaa Dhaarule Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellagaa Mellagaa Dhaarule Maarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthagaa Kotthagaa Payaname Choopenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthagaa Kotthagaa Payaname Choopenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo OoOo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo OoOo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elaa Gathakoma Kshanamulo Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Gathakoma Kshanamulo Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathai Mudipadamannadhi Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathai Mudipadamannadhi Praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathe Malupulu Choopina Vainam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathe Malupulu Choopina Vainam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oke Aluperagani Aaraatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke Aluperagani Aaraatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaa Manasutho Mundhadugeyadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Manasutho Mundhadugeyadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellagaa Mellagaa Dhaarule Maarenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellagaa Mellagaa Dhaarule Maarenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthagaa Kotthagaa Payaname Choopenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthagaa Kotthagaa Payaname Choopenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arere Unnattundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere Unnattundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Challani Gaalai Cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challani Gaalai Cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Sneham Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Sneham Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaare Thagginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaare Thagginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalakshepam Chesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalakshepam Chesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwaasai Maarettugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasai Maarettugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo..! Unnattundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo..! Unnattundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Challani Gaalai Cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challani Gaalai Cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Sneham Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Sneham Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangaare Thagginchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangaare Thagginchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalakshepam Chesindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalakshepam Chesindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Shwaasai Maarettugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shwaasai Maarettugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa… Gathakoma Kshanamulo Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa… Gathakoma Kshanamulo Maayam"/>
</div>
</pre>
