---
title: "toofan song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Madhurakavi"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Toofan"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8JumUZyzXP8"
type: "mass"
singers:
  - Deepak Blue
  - Govind Prasad
  - Yogisekar
  - Mohan Krishna
  - Santhosh Venky
  - Sachin Basrur
  - Ravi Basrur
  - Puneeth Rudranag
  - Vaish
  - Giridhar Kamath
  - Raksha Kamath
  - Sinchana Kamath
  - Nishanth Kini
  - Bharath Bhat
  - Anagha Nayak
  - Avani Bhat
  - Swathi Kamath
  - Shivanand Nayak
  - Keerthana Basrur
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Samandar mein lehar uthi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samandar mein lehar uthi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ziddi ziddi hai toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi ziddi hai toofan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chattaane bhi kaamp rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chattaane bhi kaamp rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ziddi ziddi hai toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi ziddi hai toofan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ziddi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ziddi hai toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi hai toofan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu kya mein kya hath ja hath ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kya mein kya hath ja hath ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gajaveeram kaatum nijakkalinganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gajaveeram kaatum nijakkalinganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadai kobam kottum ozhi tharanganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadai kobam kottum ozhi tharanganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padai deeram naattum jaga jalaganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai deeram naattum jaga jalaganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai yaavum vettum vedi neruppanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai yaavum vettum vedi neruppanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surrunnu erichi thallugira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surrunnu erichi thallugira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee pizhambu ivan kannilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee pizhambu ivan kannilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Virrunnu narukki kollugira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virrunnu narukki kollugira"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera vaal ivan kaigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera vaal ivan kaigalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh rocky oh rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rocky oh rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rocky rocky rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rocky rocky rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rocky oh rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rocky oh rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh rocky rocky rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rocky rocky rocky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey sarrunnu puzhudhi paraparakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sarrunnu puzhudhi paraparakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Singa cheeral ivan moochilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singa cheeral ivan moochilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Darunnu thimiri muttum theri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darunnu thimiri muttum theri"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikattivan paichalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattivan paichalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rock rock rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock rocky rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock rocky rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock rocky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal sindhum kannerilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal sindhum kannerilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal dhaagam theerthiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal dhaagam theerthiduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai meeridum kaattu theeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai meeridum kaattu theeyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Van megam anaithiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Van megam anaithiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udhiram parugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhiram parugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumum koottam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumum koottam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanin veritheeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanin veritheeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey posungi ponarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey posungi ponarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janana marana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janana marana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkeduthu paarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkeduthu paarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha mannoda kadhaikku mannanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha mannoda kadhaikku mannanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaira kaththiyin vagayara ivanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaira kaththiyin vagayara ivanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu kya mein kya hath ja hath ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kya mein kya hath ja hath ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gajaveeram kaatum nijakkalinganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gajaveeram kaatum nijakkalinganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadai kobam kottum ozhi tharangane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadai kobam kottum ozhi tharangane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padai deeram naattum jaga jalaganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai deeram naattum jaga jalaganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai yaavum vettum vedi neruppanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai yaavum vettum vedi neruppanae"/>
</div>
</pre>
