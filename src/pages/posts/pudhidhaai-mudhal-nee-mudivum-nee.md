---
title: "pudhidhaai song lyrics"
album: "Mudhal Nee Mudivum Nee"
artist: "Darbuka Siva"
lyricist: "Keerthi"
director: "Darbuka Siva"
path: "/albums/mudhal-nee-mudivum-nee-lyrics"
song: "Pudhidhaai"
image: ../../images/albumart/mudhal-nee-mudivum-nee.jpg
date: 2022-01-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sXUYrC5WgFI"
type: "happy"
singers:
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen idhu polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen idhu polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En naetrum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naetrum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen enai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen enai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu naanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu naanum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai manadhai indrae thirandhen thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai manadhai indrae thirandhen thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai paadam yaavaiyum marandhen thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai paadam yaavaiyum marandhen thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai veda thol adhu uraindhen thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai veda thol adhu uraindhen thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan idhuva idhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan idhuva idhuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pogum dhisaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pogum dhisaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketta isaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketta isaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu nadandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu nadandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha kanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha kanavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumai kollaadha uravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai kollaadha uravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Polladha azhaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladha azhaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalai izhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalai izhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa endru neeyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa endru neeyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaiyai izhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaiyai izhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi keezh izhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi keezh izhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ennai mel izhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam ennai mel izhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam naan azhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam naan azhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo oo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooimai seiyaadha paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooimai seiyaadha paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral pol veezhum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral pol veezhum kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhiyaai undhan thozhgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhiyaai undhan thozhgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam dhooramena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam dhooramena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadae un paadal ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadae un paadal ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum dhoorathil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum dhoorathil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai endruendru vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai endruendru vaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mattum ketkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mattum ketkka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaalam illaamal vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thaalam illaamal vaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal rasigai naan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal rasigai naan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkaaga vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaaga vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veredhu unadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhu unadhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu ellaamae azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ellaamae azhagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalae idhanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalae idhanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai azhagaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelgindra thiruvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelgindra thiruvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalodu varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalodu varuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal en uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal en uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen idhu polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen idhu polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En naetrum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naetrum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen enai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen enai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu naanum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu naanum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai manadhai indrae thirandhen thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai manadhai indrae thirandhen thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai paadam yaavaiyum marandhen thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai paadam yaavaiyum marandhen thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai veda thol adhu uraindhen thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai veda thol adhu uraindhen thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan idhuva idhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan idhuva idhuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan pogum dhisaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pogum dhisaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketta isaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketta isaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu nadandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu nadandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanadha kanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha kanavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumai kollaadha uravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai kollaadha uravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagae pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagae pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Huu uu uu uu uuu uu haa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huu uu uu uu uuu uu haa aaa"/>
</div>
</pre>
