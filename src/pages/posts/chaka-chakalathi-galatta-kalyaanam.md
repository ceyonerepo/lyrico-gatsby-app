---
title: "chaka chakalathi song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Yugabharathi"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Chaka Chakalathi"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XljAYGnG5Nw"
type: "happy"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi nilave vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi nilave vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikkum vetkam illai therinje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikkum vetkam illai therinje vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi mazhaiye vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi mazhaiye vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogamtha selai kattil mudinje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogamtha selai kattil mudinje vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada thada thadavena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada thada thadavena"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharayila nadanthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharayila nadanthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula mithanthida kaninje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula mithanthida kaninje vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sada sada sadavena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada sada sadavena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaigala alanthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaigala alanthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimaiyil sirichida thuninje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil sirichida thuninje vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru palangi pol uruttida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru palangi pol uruttida vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam urundaiyo porathida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam urundaiyo porathida vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodatha aasai thiri thoonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodatha aasai thiri thoonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman manasa mirattidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman manasa mirattidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalaththi nilave vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalaththi nilave vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikkum vetkam illai therinje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikkum vetkam illai therinje vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi mazhaiye vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi mazhaiye vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogamtha selai kattil mudinje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogamtha selai kattil mudinje vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sengaandhal udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaandhal udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innume innume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innume innume"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengatho thean oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengatho thean oora"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaara valaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaara valaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Satthame satthame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satthame satthame"/>
</div>
<div class="lyrico-lyrics-wrapper">Podaatho eedu era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaatho eedu era"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanathathai kanduvida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanathathai kanduvida"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru kangalum pesuvethetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru kangalum pesuvethetho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamena konjinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamena konjinalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam pola minjinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam pola minjinalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodaatho kaadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodaatho kaadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi kuyile vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi kuyile vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanamtha dhegamthilla marache vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanamtha dhegamthilla marache vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi mayile vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi mayile vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeakkamtha theerthu kolla nenache vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeakkamtha theerthu kolla nenache vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi nilave vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi nilave vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaikkum vetkam illai therinje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikkum vetkam illai therinje vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chaka chakalathi mazhaiye vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chaka chakalathi mazhaiye vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogamtha selai kattil mudinje vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogamtha selai kattil mudinje vaa"/>
</div>
</pre>
