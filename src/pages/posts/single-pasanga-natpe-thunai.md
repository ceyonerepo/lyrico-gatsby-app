---
title: "single pasanga song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Arivu"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Single Pasanga"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ybxt5dBVg5w"
type: "happy"
singers:
  - Ka Ka Balachander
  - Gana Ulagam Dharani
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ponna Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Paakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Stun Avaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stun Avaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Appan Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Appan Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Appittu Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appittu Aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Appurama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Appurama"/>
</div>
<div class="lyrico-lyrics-wrapper">Repeat Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repeat Aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Mingle Aaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Mingle Aaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">Taj Mahal Katta Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taj Mahal Katta Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengal Kodunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengal Kodunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Mingle Aaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Mingle Aaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">Committed Nu Status Maaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Committed Nu Status Maaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Signal Kodunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signal Kodunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Paaththa Manna Paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Paaththa Manna Paakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kanna Paaththa Stun Aavaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Paaththa Stun Aavaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Appan Vantha Appitt Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Appan Vantha Appitt Aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Appurama Repeat Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Appurama Repeat Aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Signal Kodunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signal Kodunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ushar Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushar Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaeko Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaeko Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vaachcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vaachcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Paapom Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Paapom Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ushar Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushar Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaeko Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaeko Ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vaachcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vaachcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Paapom Ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Paapom Ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Color Enna Color Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Color Enna Color Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ujala White Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ujala White Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Avalukkaaga Poduvenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Avalukkaaga Poduvenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maajavaa Fight Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maajavaa Fight Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Ooru Enna Ooru Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Ooru Enna Ooru Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaththu State Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaththu State Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo Yerkanve Aal Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Yerkanve Aal Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchan Nee Late Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchan Nee Late Tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Naanum Love Pannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Naanum Love Pannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inch By Inchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inch By Inchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanga Appankaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga Appankaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodukka Poraan Mookula Punchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodukka Poraan Mookula Punchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Naanum Kooti Povan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Naanum Kooti Povan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marina Beachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marina Beachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga Neenga Sernthu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Neenga Sernthu Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thada Pottaachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Pottaachchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Keralathu Kaanchana Nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Keralathu Kaanchana Nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven di Nee Siruchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven di Nee Siruchana"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Enna Tensiona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Enna Tensiona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Okay Sonna Thakdina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Okay Sonna Thakdina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maathikkuvan Enna Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathikkuvan Enna Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaavaana Persona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaavaana Persona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oththukittu Vara Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oththukittu Vara Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Worku Pannuvan Purusana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Worku Pannuvan Purusana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Mingle Aaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Mingle Aaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">Taj Mahal Katta Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taj Mahal Katta Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengal Kodunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengal Kodunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Mingle Aaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Mingle Aaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">Committed Nu Status Maaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Committed Nu Status Maaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Signal Kodunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Signal Kodunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna Paakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Paakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kanna Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Stun Aavaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stun Aavaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Appan Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Appan Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Appittu Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appittu Aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Appurama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Appurama"/>
</div>
<div class="lyrico-lyrics-wrapper">Repeat Aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repeat Aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhony Jhony
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhony Jhony"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathinu Varraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathinu Varraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanga Appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga Appa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbara Pakkam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbara Pakkam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkum Un Face Su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkum Un Face Su"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbura Ponnu Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbura Ponnu Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilla Neeyum Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilla Neeyum Pesu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaikkura Gapla Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaikkura Gapla Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthu Kalaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthu Kalaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Appan Tha Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Appan Tha Maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Naanthaandi Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Naanthaandi Maasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasula Trending gu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Trending gu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Smilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Smilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Naala Pendingu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Naala Pendingu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda File Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda File Lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Paaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Paaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muduchu Kudu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muduchu Kudu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilinja Heart Ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilinja Heart Ta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachchu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachchu Kudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Friend da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Friend da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kattikidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kattikidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Aeroplana Paathukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aeroplana Paathukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Share Auto La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Share Auto La"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Parachute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Parachute"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyala Thookita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyala Thookita"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nee Mela di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Mela di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa di Jimikki Kammal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa di Jimikki Kammal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaadi Pachcha Signal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaadi Pachcha Signal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Current Kannaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Current Kannaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nolanjan Kolantha Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nolanjan Kolantha Nenjil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siruchcha ilippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruchcha ilippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha Therippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha Therippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruchcha ilippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruchcha ilippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha Therippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha Therippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siruchcha ilippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruchcha ilippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha Therippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha Therippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruchcha ilippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruchcha ilippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha Therippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moracha Therippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Single Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Single Pasanga"/>
</div>
</pre>
