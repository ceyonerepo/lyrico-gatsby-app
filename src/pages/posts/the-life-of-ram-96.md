---
title: "the life of ram song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "The Life of Ram"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6LD30ChPsSs"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karai vantha piragaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai vantha piragaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikuthu kadalaiee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikuthu kadalaiee"/>
</div>
<div class="lyrico-lyrics-wrapper">Narai vantha piragae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narai vantha piragae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyuthu ulagai ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyuthu ulagai ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrin inbangal yaavum koodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrin inbangal yaavum koodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai ippothai artham aakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai ippothai artham aakkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrin ippothin inbam yaavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrin ippothin inbam yaavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai orr artham kaatumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai orr artham kaatumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha en vazhvai vazhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha en vazhvai vazhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazhamal melae pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazhamal melae pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraa ul ootrai theendavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraa ul ootrai theendavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrae ingae meelgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae ingae meelgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrae ingae aazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae ingae aazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey yaarapol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yaarapol"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennai paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum illamalae iyalbaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum illamalae iyalbaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudar pol thelivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudar pol thelivaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanae illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhathil naan vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhathil naan vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiyaai piranthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyaai piranthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaangindra ellamum naan aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaangindra ellamum naan aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru kaalin idayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru kaalin idayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasum poonaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasum poonaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai pothum adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai pothum adada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir kaanum yaavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir kaanum yaavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenda thoondum azhagaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenda thoondum azhagaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanae naanai iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae naanai iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalil pooraai vasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalil pooraai vasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Polae vazhnthae salikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polae vazhnthae salikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvai marukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvai marukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagai vaagai vazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagai vaagai vazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paagai paagai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paagai paagai aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tho kaatrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tho kaatrodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallooru thaan poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallooru thaan poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai illamalae azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai illamalae azhagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhae athuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhae athuvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerin aazhathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerin aazhathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogindra kal polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogindra kal polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosai ellam thuranthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosai ellam thuranthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaangindra katchikkul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaangindra katchikkul "/>
</div>
<div class="lyrico-lyrics-wrapper">naan moozhginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan moozhginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimileri kaalai mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimileri kaalai mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongum kaagamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongum kaagamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi meethu iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi meethu iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvi pogum pokkil kai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvi pogum pokkil kai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum nadappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum nadappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho eagam eluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho eagam eluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha aazham tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha aazham tharuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pol vazhum ganamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pol vazhum ganamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro paaduthae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro paaduthae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarirariro ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarirariro ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaro aarirariro ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaro aarirariro ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai vantha piragaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai vantha piragaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidikuthu kadalai ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidikuthu kadalai ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Narai vantha piragaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narai vantha piragaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyuthu ulagai ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyuthu ulagai ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrin inbangal yaavum koodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrin inbangal yaavum koodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai ippothai artham aakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai ippothai artham aakkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrin ippothin inbam yaavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrin ippothin inbam yaavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai orr artham kaatumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai orr artham kaatumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae naeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae naeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae naeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae naeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae nae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae naeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae naeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae thaanae nanae naeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae thaanae nanae naeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae aa aa"/>
</div>
</pre>
