---
title: "girra girra song lyrics"
album: "F2 Fun and Frustration"
artist: "Devi Sri Prasad"
lyricist: "Balaji"
director: "Anil Ravipudi"
path: "/albums/f2-fun-and-frustration-lyrics"
song: "Girra Girra"
image: ../../images/albumart/f2-fun-and-frustration.jpg
date: 2019-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ZOY993L3kUk"
type: "happy"
singers:
  - Sagar
  - M.M. Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Girra Girra Girra Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Girra Girra Girra Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Tirgutandey Burra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Tirgutandey Burra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maar maar maar maar maarle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maar maar maar maar maarle"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Maar maar maar maar maarle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maar maar maar maar maarle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey jhor jhor jhor jhor jhorle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jhor jhor jhor jhor jhorle"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey jhor jhor jhor jhor jhorle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jhor jhor jhor jhor jhorle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neche Nela paina paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neche Nela paina paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Upar sky kinda kinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upar sky kinda kinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhe kotti Tappay anta dharuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhe kotti Tappay anta dharuley"/>
</div>
<div class="lyrico-lyrics-wrapper">Theda kotti aage peeche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theda kotti aage peeche"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana petti nacho nache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana petti nacho nache"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimme tirigi kanipinchayi starley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimme tirigi kanipinchayi starley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Girra Girra Girra Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Girra Girra Girra Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Tirgutandey Burra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Tirgutandey Burra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey urra urra urra urra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urra urra urra urra"/>
</div>
<div class="lyrico-lyrics-wrapper">Are oodedhama boora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are oodedhama boora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Girra Girra Girra Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Girra Girra Girra Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Tirgutandey Burra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Tirgutandey Burra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey urra urra urra urra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urra urra urra urra"/>
</div>
<div class="lyrico-lyrics-wrapper">Are oodedhama boora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are oodedhama boora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maar maar maar maar maarle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maar maar maar maar maarle"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey jhor jhor jhor jhor jhorle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jhor jhor jhor jhor jhorle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey gaali lona thelinattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey gaali lona thelinattu "/>
</div>
<div class="lyrico-lyrics-wrapper">undi na body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undi na body"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo size zero attagey untundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo size zero attagey untundi "/>
</div>
<div class="lyrico-lyrics-wrapper">O my beautiful lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O my beautiful lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurram ekkinattu vellinattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurram ekkinattu vellinattu "/>
</div>
<div class="lyrico-lyrics-wrapper">undi swarari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undi swarari"/>
</div>
<div class="lyrico-lyrics-wrapper">Are sarrumantu heart centre 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are sarrumantu heart centre "/>
</div>
<div class="lyrico-lyrics-wrapper">lo lub-dub lub-dub lub-dub cheyyi swari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lo lub-dub lub-dub lub-dub cheyyi swari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasaham lo sooridu ratiri em aypoyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasaham lo sooridu ratiri em aypoyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana privacy ke visa ichi USA tour elladu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana privacy ke visa ichi USA tour elladu"/>
</div>
<div class="lyrico-lyrics-wrapper">Window lonchi aa cloud thellaga enduku merisadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Window lonchi aa cloud thellaga enduku merisadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nekey photo testunadey aa indrudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekey photo testunadey aa indrudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Girra Girra Girra Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Girra Girra Girra Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Tirgutandey Burra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Tirgutandey Burra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey urra urra urra urra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urra urra urra urra"/>
</div>
<div class="lyrico-lyrics-wrapper">Are oodedhama boora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are oodedhama boora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Girra Girra Girra Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Girra Girra Girra Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Tirgutandey Burra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Tirgutandey Burra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey urra urra urra urra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urra urra urra urra"/>
</div>
<div class="lyrico-lyrics-wrapper">Are oodedhama boora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are oodedhama boora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roll roll roll avtundi na mata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roll roll roll avtundi na mata"/>
</div>
<div class="lyrico-lyrics-wrapper">Gola gola gola cheseddam rock n roll ee poota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gola gola gola cheseddam rock n roll ee poota"/>
</div>
<div class="lyrico-lyrics-wrapper">OCheers cheers cheers antundi na glassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OCheers cheers cheers antundi na glassu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaaju kalla ninnu chusthunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaaju kalla ninnu chusthunte "/>
</div>
<div class="lyrico-lyrics-wrapper">daniki kuda puttinde romance-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daniki kuda puttinde romance-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Steady ganey ninchunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steady ganey ninchunna"/>
</div>
<div class="lyrico-lyrics-wrapper">left-o right-o potuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="left-o right-o potuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready ganey nenu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready ganey nenu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">lift e istha ra kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lift e istha ra kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Swing avtuna pai paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swing avtuna pai paina"/>
</div>
<div class="lyrico-lyrics-wrapper">thrill avtuna lo lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thrill avtuna lo lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudem chusav inka undi thillana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudem chusav inka undi thillana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Girra Girra Girra Girra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Girra Girra Girra Girra"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Tirgutandey Burra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Tirgutandey Burra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey urra urra urra urra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey urra urra urra urra"/>
</div>
<div class="lyrico-lyrics-wrapper">Are oodedhama boora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are oodedhama boora"/>
</div>
</pre>
