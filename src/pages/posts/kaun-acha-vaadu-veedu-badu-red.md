---
title: "kaun acha song lyrics"
album: "Red"
artist: "Mani Sharma"
lyricist: "Kalyan Chakravarthy"
director: "Kishore Tirumala"
path: "/albums/red-lyrics"
song: "Kaun Acha - Vaadu Veedu Bad U"
image: ../../images/albumart/red.jpg
date: 2021-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/y4SsO6Q8-VY"
type: "mass"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vadu Veedu Bad-U Antu Nuvu Cheppaku Dhobbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadu Veedu Bad-U Antu Nuvu Cheppaku Dhobbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Antu Okati Asaluntega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Antu Okati Asaluntega"/>
</div>
<div class="lyrico-lyrics-wrapper">Nangi Nangi Chuse Gandu Pilli Lanti Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nangi Nangi Chuse Gandu Pilli Lanti Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangulenni Cheppu Whoa! Whoa! Whoa! Swag!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangulenni Cheppu Whoa! Whoa! Whoa! Swag!"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lopala Unna Buchodu Chesetivanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lopala Unna Buchodu Chesetivanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppukovu Neeku Nuvvaina Kya Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppukovu Neeku Nuvvaina Kya Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Veshalinka Chalu Inkaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshalinka Chalu Inkaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchodilaga Nuvvu Ante Ento Cheppena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchodilaga Nuvvu Ante Ento Cheppena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun Hai Acha Kaun Hai Lucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun Hai Acha Kaun Hai Lucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Farake Ledu Chicha Endhukanta Racha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Farake Ledu Chicha Endhukanta Racha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun Hai Acha Kaun Hai Lucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun Hai Acha Kaun Hai Lucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bolo Mere Bacha Idantha Nee Picha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bolo Mere Bacha Idantha Nee Picha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ningi Nela Chusa Neethi Ekkadundhi Cheppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Nela Chusa Neethi Ekkadundhi Cheppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusi Vadham Kastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusi Vadham Kastha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maripodhi Nyayam Notu Retu Mare Kodhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maripodhi Nyayam Notu Retu Mare Kodhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethakamaku Dhanni Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethakamaku Dhanni Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Thelusani Antavu Telisindhedhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Thelusani Antavu Telisindhedhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Kane Kadhu Samjhona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kane Kadhu Samjhona"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-U Fair Kadhu Annavu Odinchukona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-U Fair Kadhu Annavu Odinchukona"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvadu Matichadu Neekaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvadu Matichadu Neekaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun Hai Acha Kaun Hai Lucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun Hai Acha Kaun Hai Lucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Farake Ledu Chicha Endhukanta Racha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Farake Ledu Chicha Endhukanta Racha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun Hai Acha Kaun Hai Lucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun Hai Acha Kaun Hai Lucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bolo Mere Bacha Idhantnha Nee Picha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bolo Mere Bacha Idhantnha Nee Picha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallaloki Chuse Ninnnu Lekka Vesenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaloki Chuse Ninnnu Lekka Vesenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokam Antha Anthe Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokam Antha Anthe Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootakokka Mata Pootakolla Vadi Bata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootakokka Mata Pootakolla Vadi Bata"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Mathram Kada Chethana Ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Mathram Kada Chethana Ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Nammevega Nijalu Neekeppudaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nammevega Nijalu Neekeppudaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Oppukovu Unna Nijanni Haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppukovu Unna Nijanni Haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Natakalu Adu Jagana Nee Adham Mundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natakalu Adu Jagana Nee Adham Mundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Apu Konthaina He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apu Konthaina He"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun Hai Acha Kaun Hai Lucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun Hai Acha Kaun Hai Lucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Farake Ledu Chicha Endhukanta Racha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Farake Ledu Chicha Endhukanta Racha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun Hai Acha Kaun Hai Lucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun Hai Acha Kaun Hai Lucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bolo Mere Bacha Idhantnha Nee Picha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bolo Mere Bacha Idhantnha Nee Picha"/>
</div>
</pre>
