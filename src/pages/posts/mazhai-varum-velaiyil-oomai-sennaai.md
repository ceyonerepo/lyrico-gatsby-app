---
title: "mazhai varum velaiyil song lyrics"
album: "Oomai Sennaai"
artist: "Siva"
lyricist: "Prakash Baskar"
director: "Arjunan Ekalaivan"
path: "/albums/oomai-sennaai-song-lyrics"
song: "Mazhai Varum Velaiyil"
image: ../../images/albumart/oomai-sennaai.jpg
date: 2021-12-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BkQuaCFul8c"
type: "love"
singers:
  - Haricharan
  - Deepika Theagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mazhai varum velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai varum velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">manam unnal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam unnal "/>
</div>
<div class="lyrico-lyrics-wrapper">maayam seiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam seiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam ini vaazhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam ini vaazhave"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasai kooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasai kooduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">padaigalum vilagi oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaigalum vilagi oru"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai uyirum sumakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai uyirum sumakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanilai maatram pol en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanilai maatram pol en"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvum maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvum maaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai meendum unarnthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai meendum unarnthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai sernthathal adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai sernthathal adi"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyuthe en kaalam unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyuthe en kaalam unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">sivanthu pogum vaaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivanthu pogum vaaname"/>
</div>
<div class="lyrico-lyrics-wrapper">en vizhigal paarkum oorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vizhigal paarkum oorame"/>
</div>
<div class="lyrico-lyrics-wrapper">saaram pol nan saagiren anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaram pol nan saagiren anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal engum un vaasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal engum un vaasam "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum kaalam yaavum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum kaalam yaavum un "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal enthan kathil pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal enthan kathil pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal engum un vaasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal engum un vaasam "/>
</div>
<div class="lyrico-lyrics-wrapper">veesum kaalam yaavum un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum kaalam yaavum un "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal enthan kathil pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal enthan kathil pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaarthai nee solidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai nee solidum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasamai unargiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasamai unargiren"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam kondu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam kondu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">veelthum kadhalin poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelthum kadhalin poove"/>
</div>
<div class="lyrico-lyrics-wrapper">thernthidum naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thernthidum naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivila uravum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivila uravum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thanimaiyil thavithu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanimaiyil thavithu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyai vanthaval nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyai vanthaval nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam adithu unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam adithu unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">pathaiyai matri vaipen nanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathaiyai matri vaipen nanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigal pola unnai nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigal pola unnai nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">parthu kolven paradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parthu kolven paradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mazhai varum velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai varum velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">manam unnal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam unnal "/>
</div>
<div class="lyrico-lyrics-wrapper">maayam seiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam seiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam ini vaazhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam ini vaazhave"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasai kooduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasai kooduthe"/>
</div>
</pre>
