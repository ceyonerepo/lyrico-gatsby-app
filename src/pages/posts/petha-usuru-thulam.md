---
title: "petha usuru song lyrics"
album: "Thulam"
artist: "Alex Premnath"
lyricist: "Nadhi Vijaykumar"
director: "Rajanagajothi"
path: "/albums/thulam-lyrics"
song: "Petha Usuru"
image: ../../images/albumart/thulam.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eZQ-AfyAlaE"
type: "sad"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Peththa usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththa usuru ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa usuru ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchai maram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai maram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattai maramaai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattai maramaai aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peththa usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththa usuru ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa usuru ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchai maram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai maram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattai maramaai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattai maramaai aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan ninaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan ninaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukuliyil vaattudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukuliyil vaattudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal azhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal azhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan ninaivil yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan ninaivil yengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkullae unnai veithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkullae unnai veithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthavanthaan naanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthavanthaan naanamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal kazhi kattililae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal kazhi kattililae"/>
</div>
<div class="lyrico-lyrics-wrapper">Povadhengae sollammma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povadhengae sollammma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna ninaithaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna ninaithaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai padaithaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai padaithaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai maranthaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai maranthaaiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thuranthaaiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thuranthaaiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peththa usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththa usuru ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa usuru ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchai maram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai maram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattai maramaai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattai maramaai aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai indri uyirgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai indri uyirgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil yedhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil yedhamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyindri naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyindri naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha koodumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha koodumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottililae thaalatiya thaaiyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottililae thaalatiya thaaiyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattililae kannuranguvathu yen amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattililae kannuranguvathu yen amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattililae kann urangum poovamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattililae kann urangum poovamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu ponadhu thaan nyaayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu ponadhu thaan nyaayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaangalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaangalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirandum thoongalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirandum thoongalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam athu aaralaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam athu aaralaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thaan kaanalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thaan kaanalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir thaan kaanalaiyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir thaan kaanalaiyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peththa usuru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa usuru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththa usuru ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa usuru ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchai maram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai maram thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattai maramaai aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattai maramaai aanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan ninaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan ninaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukuliyil vaattudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukuliyil vaattudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal azhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal azhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan ninaivil yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan ninaivil yengudhae"/>
</div>
</pre>
