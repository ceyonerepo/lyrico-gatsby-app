---
title: "oru vaanam song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Oru Vaanam"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AQe1CjPpr3I"
type: "love"
singers:
  - G.V Prakash Kumar
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Vaanam Thaandiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaanam Thaandiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbey Naan Parakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Naan Parakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Megam Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Megam Polave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbey Naan Mithakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbey Naan Mithakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaal Unnaal Ennul Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Unnaal Ennul Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Saaral Adikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Saaral Adikkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaal Pinnaal Haiyoo Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaal Pinnaal Haiyoo Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaalgal Nadakkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalgal Nadakkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Oru Per Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Oru Per Alai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Thaakki Poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaakki Poguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Indha Kadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Indha Kadhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enna Seivadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Seivadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Veettile Vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veettile Vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanaalume Nee Dhoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalume Nee Dhoorame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Tholile Saaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholile Saaigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">En Vaalibam Paavame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaalibam Paavame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Valaiyal Yengudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Valaiyal Yengudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Sandai Poduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Sandai Poduthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Araigal Thoonguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Araigal Thoonguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Kadhal Pesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kadhal Pesave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnodu Thaan Midhakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnodu Thaan Midhakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Natchathirangalum Nee Thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Natchathirangalum Nee Thaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaanavil Naan Ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaanavil Naan Ada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaanamo Neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanamo Neeyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uraiyaadum Nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyaadum Nerame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadumaari Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Arinthum Naanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Arinthum Naanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Thitti Theerkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Thitti Theerkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaal Unnaal Ennul Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Unnaal Ennul Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Saaral Adikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Saaral Adikkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaal Pinnaal Aiyo Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaal Pinnaal Aiyo Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaalgal Nadakkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalgal Nadakkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Oru Per Alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Oru Per Alai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Thaakki Poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thaakki Poguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Indha Kadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Indha Kadhalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Enna Seivadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Seivadho"/>
</div>
</pre>
