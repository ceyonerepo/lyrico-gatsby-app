---
title: "veesum kaathodadhaan song lyrics"
album: "Pa Paandi"
artist: "Sean Roldan"
lyricist: "Raju Murugan"
director: "Dhanush"
path: "/albums/pa-paandi-lyrics"
song: "Veesum Kaathodadhaan"
image: ../../images/albumart/pa-paandi.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4nA2tItzcws"
type: "happy"
singers:
  -	Sean Roldan
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veesum kaathodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum kaathodathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjaaguthae nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaaguthae nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodu vaasa vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodu vaasa vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadodiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadodiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuputtaa inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuputtaa inbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhooru indhooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhooru indhooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaadu en peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaadu en peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiya nee sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya nee sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga raasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukku vayasilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku vayasilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikku dhisaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikku dhisaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbangal thoolaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbangal thoolaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum dhoosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum dhoosaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthoo thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthoo thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae pogiren pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae pogiren pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthoo thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthoo thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae pogiren pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae pogiren pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eera kaathellaam isaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eera kaathellaam isaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongiponenae eppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongiponenae eppo"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli kondaadum boomikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli kondaadum boomikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Perapulla naan ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perapulla naan ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa uravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa uravaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru marathadi irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru marathadi irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyaa thunaiyaa vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyaa thunaiyaa vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhuvadhum inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvadhum inikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum boomi ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum boomi ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda ooragumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda ooragumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi bethamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi bethamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa nee aagaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa nee aagaasamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhooru indhooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhooru indhooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaadu en peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaadu en peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiya nee sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya nee sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga raasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukku vayasilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukku vayasilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikku dhisaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikku dhisaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbangal thoolaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbangal thoolaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum dhoosaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum dhoosaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthoo thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthoo thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae pogiren pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae pogiren pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo paathaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo paathaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthoo thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poguthoo thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae pogiren pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae pogiren pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesum kaathodathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum kaathodathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjaaguthae nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaaguthae nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodu vaasa vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodu vaasa vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadodiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadodiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuputtaa inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuputtaa inbam"/>
</div>
</pre>
