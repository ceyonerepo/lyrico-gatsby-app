---
title: "kola kuthu song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "Kola Kuthu"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A7mcpiT55oE"
type: "happy"
singers:
  - Vijay Antony
  - Sneha
  - Lock up (Malaysia)
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Ha Ha Ha Ha Ha Ha Ha Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Ha Ha Ha Ha Ha Ha Ha Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Cummon Stop Talk Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Cummon Stop Talk Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijai Antony Cummon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijai Antony Cummon"/>
</div>
<div class="lyrico-lyrics-wrapper">Ring String
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ring String"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jing Jing Jing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Jing Jing"/>
</div>
<div class="lyrico-lyrics-wrapper">Jerry Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jerry Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Ring String
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ring String"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinghu Jikkada Jing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinghu Jikkada Jing"/>
</div>
<div class="lyrico-lyrics-wrapper">Jerry Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jerry Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Ring String
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ring String"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Alebaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alebaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Jerry Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jerry Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Ring String
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ring String"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Alebaba Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alebaba Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaa Kuththu Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaa Kuththu Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaal Vechu Oththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaal Vechu Oththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenja Panju Panjaa Pikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenja Panju Panjaa Pikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekku Thappaa Paakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekku Thappaa Paakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanaamo Kekkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanaamo Kekkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thalaiyil Yerikkittu Suthuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thalaiyil Yerikkittu Suthuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Segappu Colouru Jaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Segappu Colouru Jaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Vechikko Moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Vechikko Moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gondhu Pottu Neeyum Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gondhu Pottu Neeyum Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikkalaam Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikkalaam Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaa Kuththu Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaa Kuththu Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaal Vechu Oththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaal Vechu Oththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenja Panju Panjaa Pikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenja Panju Panjaa Pikkiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana Na Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Na Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana Na Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana Na Naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkarakkara Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkarakkara Giri Giri Giri"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttaalakkadi Vadakkari Kari Kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttaalakkadi Vadakkari Kari Kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghaa Ghaa Ghaa Ghaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghaa Ghaa Ghaa Ghaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkarakkara Giri Giri Giri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkarakkara Giri Giri Giri"/>
</div>
<div class="lyrico-lyrics-wrapper">Uttaalakkadi Vadakkari Kari Kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uttaalakkadi Vadakkari Kari Kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghaa Ghaa Ghaa Ghaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghaa Ghaa Ghaa Ghaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelathi Meenaa Kelathi Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelathi Meenaa Kelathi Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kenda Kaala Kaatturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenda Kaala Kaatturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Poona Pola Poona Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poona Pola Poona Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikka Enna Thoonduriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikka Enna Thoonduriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Settu Paiyaa Dei Settu Paiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settu Paiyaa Dei Settu Paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Adagu Vaanga Nenaikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Adagu Vaanga Nenaikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthatha Nee Vaddiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthatha Nee Vaddiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudingi Edukka Thudikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudingi Edukka Thudikkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ullangai Panju Meththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ullangai Panju Meththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padukka Naan Padukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padukka Naan Padukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannum Rendum Erumbhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannum Rendum Erumbhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadikka Naan Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadikka Naan Thudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesudhadi Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesudhadi Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Duppattaava Saaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Duppattaava Saaththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaa Kuththu Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaa Kuththu Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaal Vechu Oththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaal Vechu Oththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenja Panju Panjaa Pikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenja Panju Panjaa Pikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ku Ku Ku Ku Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku Ku Ku Ku Koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ku Ku Ku Ku Koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ku Ku Ku Ku Koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyayaiyo Aiyayaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayaiyo Aiyayaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayaiyo Aiyo Aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayaiyo Aiyo Aiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayaiyo Aiyayaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayaiyo Aiyayaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayaiyo Aiyo Aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayaiyo Aiyo Aiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattu Vandi Sakkaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Vandi Sakkaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Unna Suthuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Unna Suthuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi Thedhi Sambalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi Thedhi Sambalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambu Romba Yelaikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu Romba Yelaikkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Aana Kutti En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aana Kutti En Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Noola Katti Thookkuradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noola Katti Thookkuradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela Vetti Ellaam Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela Vetti Ellaam Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nenaikka Vekkuradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenaikka Vekkuradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaaru Pulla Peththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaaru Pulla Peththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudukka Nee Kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudukka Nee Kudukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ippodhe Aasappattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ippodhe Aasappattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikka Naan Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikka Naan Thudikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooruthadi Ooththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooruthadi Ooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaatturiye Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaatturiye Koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa Kuththuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaa Kuththu Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaa Kuththu Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaal Vechu Oththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaal Vechu Oththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenja Panju Panjaa Pikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenja Panju Panjaa Pikkiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yekku Thappaa Paakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekku Thappaa Paakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanaamo Kekkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanaamo Kekkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thalaiyil Yerikkittu Suthuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thalaiyil Yerikkittu Suthuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Boxing Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Boxing Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Segappu Colouru Jaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Segappu Colouru Jaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Vechikko Moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Vechikko Moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gondhu Pottu Neeyum Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gondhu Pottu Neeyum Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikkalaam Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikkalaam Vaadi"/>
</div>
</pre>
