---
title: "mrithyunjaya song lyrics"
album: "Zombie Reddy"
artist: "Mark K. Robin"
lyricist: "Shiva Shakthi Datha"
director: "Prasanth Varma"
path: "/albums/zombie-reddy-lyrics"
song: "Mrithyunjaya - Om Thrayambakam"
image: ../../images/albumart/zombie-reddy.jpg
date: 2021-02-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Qh9HZGuNjhU"
type: "devotional"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Om Thrayambakam Yajaamahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om Thrayambakam Yajaamahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugandhim Pushti Vardhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugandhim Pushti Vardhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urvaarukamiva Bandhanaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urvaarukamiva Bandhanaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mrithyor Muksheeya Maamruthaath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mrithyor Muksheeya Maamruthaath"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya Mrithyumjaya Shiva Mrithyumjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Mrithyumjaya Shiva Mrithyumjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhava Mrithyumjaya Trikunjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhava Mrithyumjaya Trikunjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Purandheya Harohara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purandheya Harohara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Harohara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harohara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Harohara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harohara Hara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara Hara Hara Hara Hara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara Hara Hara Hara Hara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pralaya Bhayamkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pralaya Bhayamkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Visha Vilayamkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visha Vilayamkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Vikrutha Vinyaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Vikrutha Vinyaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaranahoma Vishaanala Keelala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaranahoma Vishaanala Keelala"/>
</div>
<div class="lyrico-lyrics-wrapper">MaranamrudhangaDhwaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="MaranamrudhangaDhwaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Mahammaari Laya Thaandavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Mahammaari Laya Thaandavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaagni Dhagdha Jana Kaandavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaagni Dhagdha Jana Kaandavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayambhayamkara Santhula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayambhayamkara Santhula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhwamsamkara Sankata Samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhwamsamkara Sankata Samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanghamaranavinivaarana Tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanghamaranavinivaarana Tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shankaraa Thwameva Sarano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shankaraa Thwameva Sarano"/>
</div>
<div class="lyrico-lyrics-wrapper">Abhyamkaraa Thwameva Sarano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhyamkaraa Thwameva Sarano"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Mrithyumjaya Shiva Mrithyumjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Mrithyumjaya Shiva Mrithyumjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhava Mrithyumjaya Trikunjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhava Mrithyumjaya Trikunjaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sahasra Sahasra Sankhyaa Niyamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sahasra Sahasra Sankhyaa Niyamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthra Pisaacha Samooham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthra Pisaacha Samooham"/>
</div>
<div class="lyrico-lyrics-wrapper">Trinethra Jagathrayeshwara Sahayusweeyanthraaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trinethra Jagathrayeshwara Sahayusweeyanthraaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhushta Samooham Prathighatana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhushta Samooham Prathighatana"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakthi Bhuvamayachha Prayachha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakthi Bhuvamayachha Prayachha"/>
</div>
<div class="lyrico-lyrics-wrapper">Praja Prana Rakshana Dhaksha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praja Prana Rakshana Dhaksha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yukthi Bhuvamayachha Prayachha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yukthi Bhuvamayachha Prayachha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tripuranjaya Samaramjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tripuranjaya Samaramjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Asuramjaya Mrithyunjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asuramjaya Mrithyunjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tripuranjaya Samaramjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tripuranjaya Samaramjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Asuramjaya Mrithyunjaya Puranjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asuramjaya Mrithyunjaya Puranjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Mrithyumjaya Shiva Mrithyumjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Mrithyumjaya Shiva Mrithyumjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhava Mrithyumjaya Trikunjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhava Mrithyumjaya Trikunjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Mrithyumjaya Shiva Mrithyumjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Mrithyumjaya Shiva Mrithyumjaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhava Mrithyumjaya Trikunjaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhava Mrithyumjaya Trikunjaya"/>
</div>
</pre>
