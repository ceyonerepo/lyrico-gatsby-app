---
title: "adi rakkamma rakku song lyrics"
album: "Siruthai"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Siva"
path: "/albums/siruthai-lyrics"
song: "Adi Rakkamma Rakku"
image: ../../images/albumart/siruthai.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/99tjkZ3GwO4"
type: "love"
singers:
  - Ranjith
  - Suchitra
  - Roshan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raakkamma Raakku Raakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkamma Raakku Raakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjikkulla Rockettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikkulla Rockettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkamma Thaakku Thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkamma Thaakku Thaaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattathula Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathula Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakkamma Raakku Raakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkamma Raakku Raakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjikkulla Rockettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikkulla Rockettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkamma Thaakku Thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkamma Thaakku Thaaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattathula Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathula Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaiyaa Solla Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyaa Solla Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavum Thollai Thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Thollai Thara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaadha En Iduppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaadha En Iduppil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Neeyum Killa Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Neeyum Killa Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaa Paam Paam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaa Paam Paam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Paam Paam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Paam Paam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaam Paam Paam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaam Paam Paam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaa Pom Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaa Pom Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Paam Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Paam Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaam Pom Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaam Pom Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakkamma Raakku Raakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkamma Raakku Raakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjikkulla Rockettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikkulla Rockettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkkamma Thaakku Thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkkamma Thaakku Thaaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattathula Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathula Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chi Chi Chinga Maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chi Chi Chinga Maatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Venaa Killu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Venaa Killu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Ee Yengamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Ee Yengamaatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venaa Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venaa Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One Two Threennu Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One Two Threennu Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Moodi Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Moodi Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Un Odhatta Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Un Odhatta Vandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhum Muththa Villu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum Muththa Villu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhanae Thandhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhanae Thandhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhanaane Thandhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhanaane Thandhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gilu Gin Machchaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilu Gin Machchaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One More Time U Say My Name
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One More Time U Say My Name"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Azhagiya Roseu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Azhagiya Roseu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Casso Varainja Pieceu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Casso Varainja Pieceu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Udhattula Un Udhattaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udhattula Un Udhattaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otti Nee Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Nee Pesu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakkamma Raakku Raakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkamma Raakku Raakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Come On Come On Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Come On Come On Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Round Round Roundadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Round Round Roundadichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenju Suththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenju Suththum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sound Sound Soundu Yeththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sound Sound Soundu Yeththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Seiya Kaththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiya Kaththum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind Mind Mindukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Mind Mindukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Bad Thingsu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Bad Thingsu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bind Bind Bindagathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bind Bind Bindagathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaththikkinum Ringsu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaththikkinum Ringsu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honeymoon Pogaththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeymoon Pogaththaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonae Vandhu Ketkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonae Vandhu Ketkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Thaan Naan Kaetten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thaan Naan Kaetten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooraiya Pichchi Kottudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraiya Pichchi Kottudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Manasula Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manasula Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenju Kuthira Ottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenju Kuthira Ottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan Tharikitta Thom Tharikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Tharikitta Thom Tharikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kondaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakkammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Come On Come On
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Come On Come On"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hui Raakkamma Raakku Raakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hui Raakkamma Raakku Raakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjikkulla Rockettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikkulla Rockettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkamma Thaakku Thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkamma Thaakku Thaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattathula Wickettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathula Wickettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaiyaa Solla Varra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyaa Solla Varra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppavum Thollai Tharra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum Thollai Tharra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaatha En Iduppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaatha En Iduppil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Neeyum Killa Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Neeyum Killa Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaa Pom Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaa Pom Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Paam Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Paam Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaam Pom Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaam Pom Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaa Pom Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaa Pom Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappara Paam Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappara Paam Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappaam Pappaam Pom Pom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappaam Pappaam Pom Pom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappara Pappaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappara Pappaam"/>
</div>
</pre>
