---
title: "vaaram song lyrics"
album: "Chal Mohan Ranga"
artist: "S S Thaman"
lyricist: "Kedarnath"
director: "Krishna Chaitanya"
path: "/albums/chal-mohan-ranga-lyrics"
song: "Vaaram"
image: ../../images/albumart/chal-mohan-ranga.jpg
date: 2018-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Fh_48_COKeE"
type: "happy"
singers:
  -	Nakash Aziz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">First Look Somavaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Look Somavaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Kalipe Mangalavaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Kalipe Mangalavaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjigundi Budhavaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjigundi Budhavaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Godavayyindi Guruvaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godavayyindi Guruvaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Godavayyindi Guruvaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godavayyindi Guruvaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Godavayyindi Guruvaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godavayyindi Guruvaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Sorry Andi Sukkaravaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry Andi Sukkaravaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Cencor Cut Sanivaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cencor Cut Sanivaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Rest Ledu Aadivaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rest Ledu Aadivaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Vundi Yevaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Vundi Yevaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Vundi Yevaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Vundi Yevaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Vundi Yevaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Vundi Yevaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram Kaani Vaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram Kaani Vaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Peru Yavvaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Yavvaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Bangaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Bangaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappadu Sokula Sathkaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappadu Sokula Sathkaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuleni Vaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuleni Vaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi Jaagaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi Jaagaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Goda Gadiyaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goda Gadiyaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Mogenu Gundello Alaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogenu Gundello Alaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Roopam Chuste Seghalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Roopam Chuste Seghalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kopam Chuste Digulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kopam Chuste Digulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Ardham Kani Puzzilu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Ardham Kani Puzzilu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele Naa Whistlu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele Naa Whistlu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kallalloni Pogalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kallalloni Pogalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundelloni Ragulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundelloni Ragulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Andani Draksha Pallu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Andani Draksha Pallu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvelle Naa Struggulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvelle Naa Struggulu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Look Somavaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Look Somavaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Kalipe Mangalavaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Kalipe Mangalavaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjigundi Budhavaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjigundi Budhavaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Godavayyindi Guruvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godavayyindi Guruvaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daani Mummylage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daani Mummylage "/>
</div>
<div class="lyrico-lyrics-wrapper">Daanikkuda Vunde Entho Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daanikkuda Vunde Entho Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Chupistunte Sarrantundi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Chupistunte Sarrantundi "/>
</div>
<div class="lyrico-lyrics-wrapper">BP Naade Brotheru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BP Naade Brotheru "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Valle Taage Manduki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Valle Taage Manduki "/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Tidutundi Liveru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Tidutundi Liveru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Neeku Naaku Set Avvadantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Neeku Naaku Set Avvadantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppenu Ooty Wheatheru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppenu Ooty Wheatheru "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram Kaani Vaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram Kaani Vaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Peru Yavvaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Yavvaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Bangaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Bangaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Thappadu Sokula Sathkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappadu Sokula Sathkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Jamuleni Vaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamuleni Vaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi Jaagaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi Jaagaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Goda Gadiyaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goda Gadiyaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Mogenu Gundello Alaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogenu Gundello Alaaram "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Roopam Chuste Seghalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Roopam Chuste Seghalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kopam Chuste Digulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kopam Chuste Digulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Ardham Kani Puzzilu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Ardham Kani Puzzilu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvele Naa Whistlu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvele Naa Whistlu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kallalloni Pogalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kallalloni Pogalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundelloni Ragulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundelloni Ragulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Andani Draksha Pallu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Andani Draksha Pallu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvelle Naa Struggulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvelle Naa Struggulu"/>
</div>
</pre>
