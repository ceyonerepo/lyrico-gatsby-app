---
title: "glassmates song lyrics"
album: "Chitralahari"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Kishore Tirumala"
path: "/albums/chitralahari-lyrics"
song: "Glassmates"
image: ../../images/albumart/chitralahari.jpg
date: 2019-04-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3I7Ka7HT0rI"
type: "happy"
singers:
  - Rahul Sipligunj
  - Penchal Das
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Come On Boys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Boys"/>
</div>
<div class="lyrico-lyrics-wrapper">School Kelle Varakeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School Kelle Varakeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Classmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Classmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Rent Katte Varakeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rent Katte Varakeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Roomates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roomates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey"/>
</div>
<div class="lyrico-lyrics-wrapper">School Kelle Varakeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School Kelle Varakeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Classmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Classmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Rent Katte Varakeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rent Katte Varakeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Roomates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roomates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weekend Vache Varakeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weekend Vache Varakeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Officemates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Officemates"/>
</div>
<div class="lyrico-lyrics-wrapper">Life End Aye Varakeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life End Aye Varakeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Soulmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soulmates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey End Antoo Leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey End Antoo Leni"/>
</div>
<div class="lyrico-lyrics-wrapper">Bend Antoo Kani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bend Antoo Kani"/>
</div>
<div class="lyrico-lyrics-wrapper">Real Relationship Yee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real Relationship Yee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Glassmates Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates Glassmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Glassmates Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates Glassmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappu Rate Perigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappu Rate Perigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peragani Peragani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peragani Peragani"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu Rate Perigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu Rate Perigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peragani Peragani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peragani Peragani"/>
</div>
<div class="lyrico-lyrics-wrapper">Petrol Dhara Thaggithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petrol Dhara Thaggithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaggani Thaggani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaggani Thaggani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Party Odani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Party Odani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neggani Neggani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neggani Neggani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana Snacks Freshgundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Snacks Freshgundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Ice Challagundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Ice Challagundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Munching Manchigundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Munching Manchigundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Glass Fullgundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Glass Fullgundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Muncheddam Daanlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Muncheddam Daanlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Gundeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Gundeni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Glassmates Manam Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates Manam Glassmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Glassmates Manam Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates Manam Glassmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Glassmates Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Glassmates Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Ghutakesthe Bhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Ghutakesthe Bhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Glassmates Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Glassmates Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Ghutakesthe Bhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Ghutakesthe Bhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trump Manaku Visaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trump Manaku Visaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvani Maanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvani Maanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampu Neellu Prathiroju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampu Neellu Prathiroju"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindani Yendani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindani Yendani"/>
</div>
<div class="lyrico-lyrics-wrapper">Buy One Ki Get One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buy One Ki Get One"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammani Apani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammani Apani"/>
</div>
<div class="lyrico-lyrics-wrapper">Iphone Ki New Model
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iphone Ki New Model"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinchani Munchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinchani Munchani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana Beer Ponguthundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Beer Ponguthundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Bar Rushugundhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Bar Rushugundhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sip Saaguthundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sip Saaguthundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kick Vooguthundani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kick Vooguthundani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Okkatunte Lokam Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Okkatunte Lokam Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkem Pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkem Pani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Glassmates Manam Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates Manam Glassmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Glassmates Manam Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates Manam Glassmates"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Gala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Gala"/>
</div>
<div class="lyrico-lyrics-wrapper">Glassmates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glassmates"/>
</div>
</pre>
