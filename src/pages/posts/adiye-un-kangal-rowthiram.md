---
title: "adiye un kangal song lyrics"
album: "Rowthiram"
artist: "Prakash Nikki"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/rowthiram-lyrics"
song: "Adiye Un Kangal"
image: ../../images/albumart/rowthiram.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Mx8qoI12tTc"
type: "love"
singers:
  - Udit Narayan
  - Dominic Adiyiah
  - Harish Raghavendra
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyae Un Kangal Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Un Kangal Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Made In Cuba-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made In Cuba-Vaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvae En Thesam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvae En Thesam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Than Kastro-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Than Kastro-Vaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Nee Vinnil Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee Vinnil Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettrum Nasa Vaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettrum Nasa Vaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyodu Ennai Saaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyodu Ennai Saaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril Pennae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril Pennae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Meiyaa Poiyaa Nee Sothikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Meiyaa Poiyaa Nee Sothikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaichi Koodam Pola Kannai Maatradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaichi Koodam Pola Kannai Maatradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Ondra Rendaa Naan Yosikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Ondra Rendaa Naan Yosikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Aatrinil Mithakiradhaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Aatrinil Mithakiradhaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae Un Kangal Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Un Kangal Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Made In Cuba-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made In Cuba-Vaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvae En Thesam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvae En Thesam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Than Kastro-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Than Kastro-Vaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Nee Vinnil Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee Vinnil Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettrum Nasa Vaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettrum Nasa Vaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyodu Ennai Saaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyodu Ennai Saaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril Pennae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril Pennae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aal Kollum Senai Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Kollum Senai Konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayudhangal Yenthikonda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayudhangal Yenthikonda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarambithaai Yutham Ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambithaai Yutham Ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaal Nidru Thaakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaal Nidru Thaakaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaveera Munnaal Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaveera Munnaal Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam Ennum Theevai Vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Ennum Theevai Vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Aalum Aasai Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Aalum Aasai Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayamaagai Pogaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayamaagai Pogaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraivena Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivena Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Theeyai Thindru Vaazhum Patchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Theeyai Thindru Vaazhum Patchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Chumma Vaththi Kuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Chumma Vaththi Kuchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viduvena Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduvena Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Konji Konji Kollum Katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konji Konji Kollum Katchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Koththum Saiva Pachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Koththum Saiva Pachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Vettaikari Kaadhal Kaatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Vettaikari Kaadhal Kaatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Meelum Vengai Aatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Meelum Vengai Aatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathungaamal En Meethu Paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungaamal En Meethu Paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Pola Aagatha Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Pola Aagatha Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kili Pola Aanenae Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili Pola Aanenae Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yazhum Vaalum Mothaamal Mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yazhum Vaalum Mothaamal Mothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae Un Kangal Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Un Kangal Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Made In Cuba-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made In Cuba-Vaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvae En Thesam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvae En Thesam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Than Kastro-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Than Kastro-Vaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Nee Vinnil Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee Vinnil Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettrum Nasa Vaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettrum Nasa Vaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyodu Ennai Saaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyodu Ennai Saaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril Pennae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril Pennae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nootrandu Kaalam Munbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootrandu Kaalam Munbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgi Pona Kandam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moozhgi Pona Kandam Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Angae Andru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Angae Andru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha Naatkal Paarthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha Naatkal Paarthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yegaandha Theevil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegaandha Theevil Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeval Pola Angel Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeval Pola Angel Ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho Seithu Pogum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Seithu Pogum Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Solla Kettenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Solla Kettenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amazon Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amazon Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Rendae Rendu Pataam Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Rendae Rendu Pataam Poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Paarpom Kannaam Poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Paarpom Kannaam Poochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarovil Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarovil Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Thangakatti Sengal Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Thangakatti Sengal Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangum Aasai Vanthudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangum Aasai Vanthudichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Neela Kannaal Paalam Pottaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neela Kannaal Paalam Pottaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookam Poyae Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam Poyae Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Vilagaamal Unnodu Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Vilagaamal Unnodu Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikaamal Un Thotram Kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikaamal Un Thotram Kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Kooda Un Kaiyil Neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Kooda Un Kaiyil Neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaval Konden Nee Ennai Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaval Konden Nee Ennai Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae Un Kangal Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Un Kangal Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Made In Cuba-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made In Cuba-Vaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvae En Thesam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvae En Thesam Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Than Kastro-Vaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Than Kastro-Vaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Nee Vinnil Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee Vinnil Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettrum Nasa Vaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettrum Nasa Vaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyodu Ennai Saaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyodu Ennai Saaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril Pennae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril Pennae Vaa"/>
</div>
</pre>
