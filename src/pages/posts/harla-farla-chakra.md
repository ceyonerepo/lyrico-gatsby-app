---
title: "harla farla song lyrics"
album: "Chakra"
artist: "Yuvan Shankar Raja"
lyricist: "Madhan Karky"
director: "M.S. Anandhan"
path: "/albums/chakra-song-lyrics"
song: "Harla Farla"
image: ../../images/albumart/chakra.jpg
date: 2021-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hYVlnelgKNI"
type: "Love"
singers:
  - Yuvan Shankar Raja
  - Sanjana Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eah Vaanam Ellaam… Kaadhal Grafitte|
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eah Vaanam Ellaam… Kaadhal Grafitte|"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilukkuthuadee Undhan… Heartily Ulla Gravity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilukkuthuadee Undhan… Heartily Ulla Gravity"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhidavaa Naan Vaan Meale..?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhidavaa Naan Vaan Meale..?"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilundhidavaa Un Nenjulle..?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilundhidavaa Un Nenjulle..?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti Kutti Kannu… Rendum Portal Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Kutti Kannu… Rendum Portal Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Rendum… En Bodhai Bottle Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Rendum… En Bodhai Bottle Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Vali Unil… Vilundhen Total Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Vali Unil… Vilundhen Total Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harla Farla…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harla Farla…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra TTa Ta Tta TaTta… Heartin Rattle Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra TTa Ta Tta TaTta… Heartin Rattle Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gubu Gubuvena… Theeyin Moottalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gubu Gubuvena… Theeyin Moottalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Illai Idhu… Kaadhal Battle Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Illai Idhu… Kaadhal Battle Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Harla Farla…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harla Farla…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Girl… When You Look Into My Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Girl… When You Look Into My Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can Feel My Heart Is… Skippin A Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can Feel My Heart Is… Skippin A Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh You Got The Move So Sweet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh You Got The Move So Sweet"/>
</div>
<div class="lyrico-lyrics-wrapper">You Don’t Know… What You’re Doing To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Don’t Know… What You’re Doing To Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Hello Mister Military…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Hello Mister Military…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Midikku Nadai… Ellam Ennaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Midikku Nadai… Ellam Ennaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Kakki Sattai Kaadhali…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Kakki Sattai Kaadhali…"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kasakki Nasukkidhaan Pottaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kasakki Nasukkidhaan Pottaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaakkai Naattin Sondhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaakkai Naattin Sondhame"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nenjam Undhan Sondhamadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nenjam Undhan Sondhamadee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai Engum Mutkalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Engum Mutkalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Nenjam Endhan Manjamadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Nenjam Endhan Manjamadee"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Muthukodu Muthukaagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Muthukodu Muthukaagavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Mugam Paarthu Alakaagavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Mugam Paarthu Alakaagavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagamo Tholin Pinnaale… Ulagame Endhan Munnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagamo Tholin Pinnaale… Ulagame Endhan Munnaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kanam… Oru Kaaval Kaariyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kanam… Oru Kaaval Kaariyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Kanam… Oru Kaadhal Kariyaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Kanam… Oru Kaadhal Kariyaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Mayakkidum Vedathaariyaai…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Mayakkidum Vedathaariyaai…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harla Farla… Oohumm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harla Farla… Oohumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal Muluvadhum… Yudha Therila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Muluvadhum… Yudha Therila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaviravinil Mutha Thoorala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaviravinil Mutha Thoorala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Naduvile… Ellai Meerala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Naduvile… Ellai Meerala"/>
</div>
<div class="lyrico-lyrics-wrapper">Harla Farla… Aaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harla Farla… Aaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Girl… When You Look Into My Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Girl… When You Look Into My Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can Feel My Heart Is… Skippin A Beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can Feel My Heart Is… Skippin A Beat"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh You Got The Move So Sweet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh You Got The Move So Sweet"/>
</div>
<div class="lyrico-lyrics-wrapper">You Don’t Know… What You’re Doing To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Don’t Know… What You’re Doing To Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Girl… Unna Paathathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Girl… Unna Paathathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mayangi Kiranghidhaan Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mayangi Kiranghidhaan Ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vittu Nee Vilaginaal Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vittu Nee Vilaginaal Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-U Udanchidhaan Povene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-U Udanchidhaan Povene"/>
</div>
</pre>
