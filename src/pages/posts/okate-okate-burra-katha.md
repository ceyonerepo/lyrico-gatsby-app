---
title: "okate okate song lyrics"
album: "Burra Katha"
artist: "Sai Karthik"
lyricist: "Krishna Kanth"
director: "Diamond Ratna Babu"
path: "/albums/burra-katha-lyrics"
song: "Okate Okate"
image: ../../images/albumart/burra-katha.jpg
date: 2019-07-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vTq6aWeJWZs"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">okate okate neethote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate neethote"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate godavaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate godavaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate okasaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate okasaare"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you anave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you anave"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate porapaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate porapaate"/>
</div>
<div class="lyrico-lyrics-wrapper">edalo jarige cherabaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edalo jarige cherabaate"/>
</div>
<div class="lyrico-lyrics-wrapper">pedave padene alavate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedave padene alavate"/>
</div>
<div class="lyrico-lyrics-wrapper">nee perey palile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee perey palile"/>
</div>
<div class="lyrico-lyrics-wrapper">aagaagu kalavaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagaagu kalavaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee ddoram avasaramam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee ddoram avasaramam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kopame to tagginchumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kopame to tagginchumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve ok ante chitikelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve ok ante chitikelo "/>
</div>
<div class="lyrico-lyrics-wrapper">okkatavamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkatavamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okate okate neethote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate neethote"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate godavaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate godavaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate okasaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate okasaare"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you anave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you anave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee kosame kottaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosame kottaya "/>
</div>
<div class="lyrico-lyrics-wrapper">malli puttanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malli puttanana"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosame rojuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosame rojuko"/>
</div>
<div class="lyrico-lyrics-wrapper">peru pettanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peru pettanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosame gundene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosame gundene"/>
</div>
<div class="lyrico-lyrics-wrapper">dhindu cheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhindu cheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve naa priya sakhive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve naa priya sakhive"/>
</div>
<div class="lyrico-lyrics-wrapper">elaaga ayyenu parichayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elaaga ayyenu parichayame"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaaga thenu oka varamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaaga thenu oka varamai"/>
</div>
<div class="lyrico-lyrics-wrapper">elanti nannu maaye chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elanti nannu maaye chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">maarchivesi prema vadilenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarchivesi prema vadilenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okate okate neethote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate neethote"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate godavaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate godavaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate okasaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate okasaare"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you anave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you anave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aagaagu kalavaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagaagu kalavaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ee ddoram avasaramam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee ddoram avasaramam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kopame to tagginchumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kopame to tagginchumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve ok ante chitikelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve ok ante chitikelo "/>
</div>
<div class="lyrico-lyrics-wrapper">okkatavamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkatavamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okate okate neethote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate neethote"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate godavaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate godavaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">okate okate okasaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate okate okasaare"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you anave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you anave"/>
</div>
</pre>
