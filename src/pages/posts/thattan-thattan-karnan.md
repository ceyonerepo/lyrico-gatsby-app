---
title: 'thattan thattan song lyrics'
album: 'Karnan'
artist: 'Santhosh Narayanan'
lyricist: 'Yugabharathi'
director: 'Mari Selvaraj'
path: '/albums/karnan-song-lyrics'
song: 'Thattan Thattan'
image: ../../images/albumart/karnan.jpg
date: 2021-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/llgbxCR2sCw"
type: 'Love'
singers: 
- Dhanush
- Meenakshi Ilayaraja
- Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Thattan Thattan Vandikati,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattan Thattan Vandikati,"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthen Kozhi Thuvattam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthen Kozhi Thuvattam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Sokkapanna Mela Ninna,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sokkapanna Mela Ninna,"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha Sura Kathatam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Sura Kathatam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Motapara Poova Vedichene,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motapara Poova Vedichene,"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchi Thena Vari Kudichene,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi Thena Vari Kudichene,"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kairega Patha Pechi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kairega Patha Pechi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Sonnale Neeye Sachi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Sonnale Neeye Sachi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Pora Vara Pathayila,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Pora Vara Pathayila,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerinji Mulla Othukam Umparva,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerinji Mulla Othukam Umparva,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Thattan Thattan Vandikatti,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thattan Thattan Vandikatti,"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthen Kozhi Thuvattam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthen Kozhi Thuvattam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Sokkapanna Mela Ninna,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sokkapanna Mela Ninna,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Adicha Soora Kathatam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Adicha Soora Kathatam,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthilala Nettatam Kumiyathe Um Vasam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthilala Nettatam Kumiyathe Um Vasam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaiya Nee Paka Soru Pongum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaiya Nee Paka Soru Pongum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvula Ponalum Puzhuthiya Vanthalum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvula Ponalum Puzhuthiya Vanthalum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavani Rasava Matha Sollum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavani Rasava Matha Sollum,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthanala Nenchiruka,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthanala Nenchiruka,"/>
</div>
<div class="lyrico-lyrics-wrapper">Vn Nenappe Thurat Addikkum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vn Nenappe Thurat Addikkum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ur Nezhala Na Iruka En Nesame Neethandi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ur Nezhala Na Iruka En Nesame Neethandi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthatha Thayen Rasathi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthatha Thayen Rasathi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamum Tharen Kaimathi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamum Tharen Kaimathi,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattan Thattan Vandikati,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattan Thattan Vandikati,"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthen Kozhi Thuvattam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthen Kozhi Thuvattam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Sokkapanna Mela Ninnu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sokkapanna Mela Ninnu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha Soora Kathatam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Soora Kathatam,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vzhavan Vayalula Irangi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vzhavan Vayalula Irangi,"/>
</div>
<div class="lyrico-lyrics-wrapper">Koora Nataiyum Pirichu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koora Nataiyum Pirichu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Nelathaiye Kakum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Nelathaiye Kakum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Perungudiyam Uzhakkudiyam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perungudiyam Uzhakkudiyam,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootan Punjaya Tholachan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootan Punjaya Tholachan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Patan Nanjaiya Tholachan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patan Nanjaiya Tholachan,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla Kadavulum Kedaka,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Kadavulum Kedaka,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanom Koolikkudiyanom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanom Koolikkudiyanom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeichidu Kannu Jeichidu Kannu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeichidu Kannu Jeichidu Kannu,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaka Kuruvi Netham Kootampottu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaka Kuruvi Netham Kootampottu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kathaya Pesa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kathaya Pesa,"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Kettu Yenguthe Mazha Oonguthe,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Kettu Yenguthe Mazha Oonguthe,"/>
</div>
<div class="lyrico-lyrics-wrapper">Odampeduthu Theekothu Vyir Eriya Nananche Kedappo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odampeduthu Theekothu Vyir Eriya Nananche Kedappo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattan Thattan Ye Thattan Thattan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattan Thattan Ye Thattan Thattan,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattan Thattan Vandikati,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattan Thattan Vandikati,"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthen Kozhi Thuvattam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthen Kozhi Thuvattam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Sokkapanna Mela Ninna,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sokkapanna Mela Ninna,"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha Soora Kathatam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha Soora Kathatam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora Kathatam Soora Kathatam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora Kathatam Soora Kathatam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattan Thattan Thattan Thattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattan Thattan Thattan Thattan"/>
</div>
</pre>