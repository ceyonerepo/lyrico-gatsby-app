---
title: "dhandam song lyrics"
album: "Amma Rajyam Lo Kadapa Biddalu"
artist: "Ravi Shankar"
lyricist: "Sirasri"
director: "Siddhartha Thatholu"
path: "/albums/amma-rajyam-lo-kadapa-biddalu-lyrics"
song: "Dhandam"
image: ../../images/albumart/amma-rajyam-lo-kadapa-biddalu.jpg
date: 2019-12-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Xj11GkMXDkw"
type: "happy"
singers:
  - Hamsika Iyer
  - Sakshi holkar
  - Harman Nazim
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Baabu Dhandam Salaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baabu Dhandam Salaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Salute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Salute"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippati Raajakeeyamloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippati Raajakeeyamloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutralane Chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutralane Chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharathamlo Shakuni Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharathamlo Shakuni Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock Ayipothadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock Ayipothadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock Ayipothadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock Ayipothadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock Ayipothadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock Ayipothadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Maatalu Cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maatalu Cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">Leaderlani Choosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaderlani Choosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramayanam Mandaru Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramayanam Mandaru Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Ayipothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Ayipothundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Ayipothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Ayipothundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Ayipothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Ayipothundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippati Raajakeeyamloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippati Raajakeeyamloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutralane Chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutralane Chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharathamlo Shakuni Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharathamlo Shakuni Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock Ayipothadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock Ayipothadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Maatalu Cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maatalu Cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">Leaderlani Choosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaderlani Choosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramayanam Mandaru Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramayanam Mandaru Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Ayipothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Ayipothundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootilla Neethiki Addressu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootilla Neethiki Addressu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanakyudaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanakyudaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Karnunni Mosam Chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karnunni Mosam Chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Shalyude Ayinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shalyude Ayinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dungayipotharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dungayipotharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiverayipotharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiverayipotharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mundu Mementhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mundu Mementhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandam Pedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Pedatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandam Pedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Pedatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandam Pedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Pedatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kodatharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhandam Salaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Salaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Salute 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Salute "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Speakani Vaadini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speakani Vaadini"/>
</div>
<div class="lyrico-lyrics-wrapper">Speaker Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speaker Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalu Raanolle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalu Raanolle"/>
</div>
<div class="lyrico-lyrics-wrapper">Speechlu Istharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speechlu Istharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Speakani Vaadini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speakani Vaadini"/>
</div>
<div class="lyrico-lyrics-wrapper">Speaker Antaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speaker Antaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatalu Raanollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatalu Raanollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Speechlu Istharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speechlu Istharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothala Veerulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothala Veerulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethulu Chebuthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethulu Chebuthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothala Raayullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothala Raayullu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijaaladuguthaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijaaladuguthaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naati Bharatham Mayasabhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naati Bharatham Mayasabhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinthalu Viddooraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinthalu Viddooraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neti Bharatham Jana Sabhalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neti Bharatham Jana Sabhalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Arupulu Kekale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arupulu Kekale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandam Pedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Pedatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandam Pedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Pedatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kodatharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya Cinema Herolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya Cinema Herolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero La Waste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero La Waste"/>
</div>
<div class="lyrico-lyrics-wrapper">Comedianla Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comedianla Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Leaderle Best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaderle Best"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Cinema Herolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Cinema Herolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukule Waste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukule Waste"/>
</div>
<div class="lyrico-lyrics-wrapper">Comedianla Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comedianla Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Leaderle Best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaderle Best"/>
</div>
<div class="lyrico-lyrics-wrapper">Pappukemo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappukemo "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizag Development
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizag Development"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani Misskaaniyyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani Misskaaniyyaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Entertainment
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entertainment"/>
</div>
<div class="lyrico-lyrics-wrapper">World Cup Cricketaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World Cup Cricketaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bore Kottochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bore Kottochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Assembly Sessionlu Mathram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assembly Sessionlu Mathram"/>
</div>
<div class="lyrico-lyrics-wrapper">All Time Hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Time Hittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandam Pedatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandam Pedatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Ho Antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Ho Antaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute Kodatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute Kodatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippati Raajakeeyamloni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippati Raajakeeyamloni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutralane Chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutralane Chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharathamlo Shakuni Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharathamlo Shakuni Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock Ayipothadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock Ayipothadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Maatalu Cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maatalu Cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">Leaderlani Choosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaderlani Choosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramayanam Mandaru Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramayanam Mandaru Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake Ayipothundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake Ayipothundi"/>
</div>
</pre>
