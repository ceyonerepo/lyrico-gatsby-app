---
title: "yaar antha oviyathai song lyrics"
album: "Kalathil Santhippom"
artist: "Yuvan Shankar Raja"
lyricist: "Pa. Vijay"
director: "N. Rajasekar"
path: "/albums/kutty-kalathil-santhippom-lyrics"
song: "Yaar Antha Oviyathai"
image: ../../images/albumart/kalathil-santhippom.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jJKnBxJMhz8"
type: "Love"
singers:
  - Vijay Yesudas
  - Nivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar antha oviyaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar antha oviyaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadamada vaithatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamada vaithatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veettil maatti vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veettil maatti vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala naeram vanthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala naeram vanthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi maaligayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi maaligayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan vaithu paarthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan vaithu paarthatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Munne aval nindra podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munne aval nindra podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal koosi ponatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal koosi ponatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulaga azhagi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga azhagi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulavum nilavum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulavum nilavum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga thozhiyaa theriyiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga thozhiyaa theriyiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athira sirippum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athira sirippum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiga sivappum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiga sivappum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagin oviyamaa asaththuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagin oviyamaa asaththuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavidhai pola vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai pola vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu pola vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu pola vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku appadiyae porunthuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku appadiyae porunthuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkunnu irukkuraa ulloor ellora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkunnu irukkuraa ulloor ellora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avathaan un maaman ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avathaan un maaman ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayila meen kannae kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayila meen kannae kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaana jodiyinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaana jodiyinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarthu asantha ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarthu asantha ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennannu naan solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennannu naan solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaguna athanai azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaguna athanai azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andraadam nee mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andraadam nee mella"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you solli pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you solli pazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan paartha devathaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paartha devathaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragillai unmaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragillai unmaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval pola pennai naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval pola pennai naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthathillai anmaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathillai anmaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharai maelae nindra podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai maelae nindra podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhakkindraal menmaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkindraal menmaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangathai oottri oottri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangathai oottri oottri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthu vaitha ponmaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthu vaitha ponmaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Latcham poo parichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham poo parichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Michcham thaen thelichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michcham thaen thelichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha azhagu ava azhagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha azhagu ava azhagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcha paarvayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcha paarvayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchcham kavidhai onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchcham kavidhai onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchil etti vidum adadadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchil etti vidum adadadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachcha theevukkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachcha theevukkoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Machcham vachathu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machcham vachathu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchai pasumai ava paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchai pasumai ava paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagadaa avaladaa asanthu polaamdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagadaa avaladaa asanthu polaamdaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval kangal kavidhai pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval kangal kavidhai pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil kanden vellai vekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil kanden vellai vekkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval vanthu munnae nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval vanthu munnae nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavellam pinnae nirkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavellam pinnae nirkkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththathil aval pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththathil aval pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen intha oorukkul illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen intha oorukkul illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil aval vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil aval vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthiduvaai vaanil mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthiduvaai vaanil mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi silaiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi silaiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnaadi sirichu poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaadi sirichu poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathadi un manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi un manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaadi aakka poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaadi aakka poraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththathil aval pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththathil aval pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen intha oorukkul illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen intha oorukkul illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil aval vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil aval vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthiduvaai vaanil mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthiduvaai vaanil mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa haa aa aa…aa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa haa aa aa…aa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa…..aaa…..aaha…..aahaa…aa…aaa..aa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa…..aaa…..aaha…..aahaa…aa…aaa..aa…"/>
</div>
</pre>
