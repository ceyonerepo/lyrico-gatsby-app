---
title: "anbulla kadhala en uyir kadhala song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Madhan Karky"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Anbulla Kadhala En Uyir Kadhala"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7r-YxYyPd54"
type: "love"
singers:
  - Abhay Jodhpurkar
  - Alisha Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbulla Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbulla Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Kaanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Kaanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Varum Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Varum Varaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mana Thiraiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mana Thiraiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhi Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varainthirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varainthirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaithidum Athirvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaithidum Athirvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathin Naduvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Naduvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigalai Ezhuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalai Ezhuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalin Kadhavai Nanaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalin Kadhavai Nanaikiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhithidum Nodiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithidum Nodiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalgalin Padiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalgalin Padiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Nuni Kondu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Nuni Kondu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Varugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalil Ippadikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil Ippadikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadikku Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadikku Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbulla Kaadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbulla Kaadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Kaanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Kaanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhigalin Kaal Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhigalin Kaal Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthida Vaa Endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthida Vaa Endraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiligalin Mozhi Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiligalin Mozhi Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Keechal Azhaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Keechal Azhaikiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyena Thol Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyena Thol Kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruviyai Pol Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruviyai Pol Aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Mudhal Padham Paaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Mudhal Padham Paaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamaaga Anaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamaaga Anaikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyin Thavarugalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyin Thavarugalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruthida Pirathavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruthida Pirathavane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimidathin Koraikudathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimidathin Koraikudathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraikka Vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraikka Vanthavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathin Seyal Thiranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Seyal Thiranai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arinthida Seithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arinthida Seithavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal Manam Sendru Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Manam Sendru Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Vanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Vanthavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalil Ippadikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil Ippadikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadikku Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadikku Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbulla Kaadhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbulla Kaadhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Kaadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Kaanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Kaanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Varum Varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Varum Varaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mana Thiraiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mana Thiraiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhi Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varainthirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varainthirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaithidum Athirvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaithidum Athirvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathin Naduvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Naduvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigalai Ezhuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalai Ezhuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalin Kadhavai Nanaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalin Kadhavai Nanaikiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhithidum Nodiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithidum Nodiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalgalin Padiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalgalin Padiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Nuni Kondu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Nuni Kondu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Varugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalil Ippadikku Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil Ippadikku Kaadhali"/>
</div>
</pre>
