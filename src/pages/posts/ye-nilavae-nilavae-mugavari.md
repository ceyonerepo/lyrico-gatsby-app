---
title: "yeh nilavae nilavae song lyrics"
album: "Mugavari"
artist: "Deva"
lyricist: "Vairamuthu"
director: "V. Z. Durai"
path: "/albums/mugavari-lyrics"
song: "Yeh Nilavae Nilavae"
image: ../../images/albumart/mugavari.jpg
date: 2000-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mqAllzgtQDU"
type: "motivational"
singers:
  - Unni Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yae Nilavae Yae Nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Nilavae Yae Nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnai Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thoda Unnai Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thoda Unnai Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai Adainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Adainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae Nilavae Yae Nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Nilavae Yae Nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vinnai Vittu Mannai Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vinnai Vittu Mannai Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalukkul Pugunthuvittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalukkul Pugunthuvittai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Mooda Maruthu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Mooda Maruthu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Thoongathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Thoongathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Thaangum Idhayam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Thaangum Idhayam Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Thaangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Thaangathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhi Eerppu Visaiyinilae Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi Eerppu Visaiyinilae Anbae Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vanthu Vizhunthu Vitten Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vanthu Vizhunthu Vitten Anbae Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Jaadai Aamaam Endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Jaadai Aamaam Endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Jaadai Illai Endru Irupathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Jaadai Illai Endru Irupathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonkodi Nijam Ennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonkodi Nijam Ennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Vaazhvaa Saavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Vaazhvaa Saavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethai Nee Tharuvaai Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai Nee Tharuvaai Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae Nilavae Yae Nilavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Nilavae Yae Nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unnai Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thoda Unnai Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thoda Unnai Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai Adainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Adainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaindhu Ninaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaindhu Ninaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Vali Kondathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Vali Kondathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nizhalil Irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nizhalil Irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratham Kasigindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham Kasigindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sol Oru Sol Oru Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sol Oru Sol Oru Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaal Uyirae Oori Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaal Uyirae Oori Vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Adiyae Mudiyaathendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Adiyae Mudiyaathendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Keeri Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Keeri Vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilaaaaa Neeyallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaaaaa Neeyallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theibavan Naan Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theibavan Naan Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam Naan Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam Naan Sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Illamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Illamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatru Nadai Podalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru Nadai Podalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Illamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Illamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal Nadai Poduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Nadai Poduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Mooda Maruthu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Mooda Maruthu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Thoongathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Thoongathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Thaangum Idhayam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Thaangum Idhayam Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Thaangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Thaangathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhi Eerppu Visaiyinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhi Eerppu Visaiyinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Naan Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Naan Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthu Vitten Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthu Vitten Anbae Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Jaadai Aamaam Endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Jaadai Aamaam Endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Jaadai Illai Endru Irupathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Jaadai Illai Endru Irupathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonkodi Nijam Ennadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonkodi Nijam Ennadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Vaazhvaa Saavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Vaazhvaa Saavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethai Nee Tharuvaai Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethai Nee Tharuvaai Pennae"/>
</div>
</pre>
