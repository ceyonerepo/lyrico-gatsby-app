---
title: "avanapathi song lyrics"
album: "Avan Ivan"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bala"
path: "/albums/avan-ivan-lyrics"
song: "Avanapathi"
image: ../../images/albumart/avan-ivan.jpg
date: 2011-06-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q-q8u79EHeQ"
type: "happy"
singers:
  - T.L. Maharajan
  - Sathyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Avanapaththi Naan Paadaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanapaththi Naan Paadaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanappaththi Naan Paadaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanappaththi Naan Paadaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanum Sari Illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanum Sari Illa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanum Thaan Sari Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanum Thaan Sari Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanaththaan Naan Ippo Paadaporaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanaththaan Naan Ippo Paadaporaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora Paththi Naan Paada Poraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Paththi Naan Paada Poraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Orava Paththi Naan Paada Poraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orava Paththi Naan Paada Poraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorum Sari Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorum Sari Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oravum Thaan Sari Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oravum Thaan Sari Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraththaan Naan Ippo Paada Poraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraththaan Naan Ippo Paada Poraen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatil Vazhntha Mirugam Thaan Naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatil Vazhntha Mirugam Thaan Naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Ninapil Atta Thaan Puli Thinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Ninapil Atta Thaan Puli Thinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliya Thaan Kattaana Mithikithunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliya Thaan Kattaana Mithikithunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatu Kugaiyil Naan Vazhntha Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Kugaiyil Naan Vazhntha Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Irundu Theepantham Thaan Aenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Irundu Theepantham Thaan Aenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Thaniyaame Thuraththudunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Thaniyaame Thuraththudunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mansapayalin Raththathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansapayalin Raththathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Nirupathu Unakkenna Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirupathu Unakkenna Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraga Theriyum Neeraa Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraga Theriyum Neeraa Athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraga Oadum Nerupu Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraga Oadum Nerupu Athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram Ullethaan Ullathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram Ullethaan Ullathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhiyirukum Balliyirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhiyirukum Balliyirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyirikum Atha Veta Kumagirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyirikum Atha Veta Kumagirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavirukum Ulagirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavirukum Ulagirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolayirukum Konjam Karunaiyum Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolayirukum Konjam Karunaiyum Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Købaththil sila Naeram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Købaththil sila Naeram "/>
</div>
<div class="lyrico-lyrics-wrapper">saanthaththil sila Naeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saanthaththil sila Naeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaadum Nenjaththin Marumam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaadum Nenjaththin Marumam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">saththangal Aanaalum Mounangal Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saththangal Aanaalum Mounangal Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ellamae Ondraethaan Veraan enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellamae Ondraethaan Veraan enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei ellam Poi Aaga Poi ellam Mei Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei ellam Poi Aaga Poi ellam Mei Aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaaga Mei Poiyin Marumam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaaga Mei Poiyin Marumam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei ellam Mei Illai Poi ellam Poi Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei ellam Mei Illai Poi ellam Poi Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei Mei Mei Poi Poi Poi Meiyaa enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Mei Mei Poi Poi Poi Meiyaa enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Rendil Thinam Thoandrum Kaachiellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Rendil Thinam Thoandrum Kaachiellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Vittu Veliyaerum Marmam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Vittu Veliyaerum Marmam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Rendum Unathillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Rendum Unathillai "/>
</div>
<div class="lyrico-lyrics-wrapper">Athan Kaatchi Unnathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan Kaatchi Unnathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaatha Mada Nenjil Mayakkam Ènna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaatha Mada Nenjil Mayakkam Ènna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edukaatil Thinam Paarkum Thaambal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edukaatil Thinam Paarkum Thaambal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ethayaththil Aeraatha Marmam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethayaththil Aeraatha Marmam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarnthaalum Uthirnthaalum Madi Aenthum Manalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarnthaalum Uthirnthaalum Madi Aenthum Manalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Vaangum Oru Thaedal Kondaal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vaangum Oru Thaedal Kondaal enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirikku Uyirodu Vanthøm enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirikku Uyirodu Vanthøm enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaazhnthu Athai Neeyum Unnarnthal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaazhnthu Athai Neeyum Unnarnthal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaazhkai Nilai Yaamai Marmam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaazhkai Nilai Yaamai Marmam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Yaamai Ninaiyaamal Kondaal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Yaamai Ninaiyaamal Kondaal enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesal Poal Vazhnthaalum esamthaan saernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesal Poal Vazhnthaalum esamthaan saernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoosaagi Thoolaagum Marmam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosaagi Thoolaagum Marmam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">sirithaaga Varainthaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirithaaga Varainthaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Perithaaga Varainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perithaaga Varainthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poojiyaththil Perusellam Mathippaa enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poojiyaththil Perusellam Mathippaa enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanaaga Irunthaalum Ivanaaga Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanaaga Irunthaalum Ivanaaga Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">evanaaga Irunthaalum Iruthi enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanaaga Irunthaalum Iruthi enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pichai Thaan eduththaalum Paerarasar Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichai Thaan eduththaalum Paerarasar Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhuvukku Iraiyaavaan Vaerae enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhuvukku Iraiyaavaan Vaerae enna"/>
</div>
</pre>
