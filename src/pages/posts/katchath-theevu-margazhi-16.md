---
title: "katchath theevu song lyrics"
album: "Margazhi 16"
artist: "E.K. Bobby"
lyricist: "Kalaikumar"
director: "K. Stephen"
path: "/albums/margazhi-16-lyrics"
song: "Katchath Theevu"
image: ../../images/albumart/margazhi-16.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A87HMMsjoFA"
type: "love"
singers:
  - Ranjith
  - Nincy Vincent
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kachchatheevu poagalaamaa penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachchatheevu poagalaamaa penney"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal solli meetkalaamaa anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal solli meetkalaamaa anbey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingum pooppookkumey ini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum pooppookkumey ini "/>
</div>
<div class="lyrico-lyrics-wrapper">youththam ingillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="youththam ingillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum nam vaazhkkaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum nam vaazhkkaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">azhukkaanadhum thoaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhukkaanadhum thoaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaippuyaley anbin kadalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaippuyaley anbin kadalil "/>
</div>
<div class="lyrico-lyrics-wrapper">vaiyam kolgiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaiyam kolgiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru gudhirai vegam kondu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru gudhirai vegam kondu "/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkkai parakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkkai parakkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachchatheevu poagalaamaa penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachchatheevu poagalaamaa penney"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal solli meetkalaamaa anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal solli meetkalaamaa anbey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaanai madham kondu aeri midhithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanai madham kondu aeri midhithaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhalukku vali illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalukku vali illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammai naam thaangum anbai thadukkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai naam thaangum anbai thadukkindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaiye ini illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiye ini illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suya saridhaiyil en mudhal vari neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suya saridhaiyil en mudhal vari neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayammindry solven en mugavari neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayammindry solven en mugavari neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhinai izhuththida vadam pidiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinai izhuththida vadam pidiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley nirandhara idam pidiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley nirandhara idam pidiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai ninaiththaal aayul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ninaiththaal aayul "/>
</div>
<div class="lyrico-lyrics-wrapper">regai neelam aagiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="regai neelam aagiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaippadhai ellaam otrai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaippadhai ellaam otrai "/>
</div>
<div class="lyrico-lyrics-wrapper">nodiyil kaadhal velgiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodiyil kaadhal velgiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachchatheevu poagalaamaa penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachchatheevu poagalaamaa penney"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal solli meetkalaamaa anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal solli meetkalaamaa anbey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhi andhangal edhum illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi andhangal edhum illaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irumanam adangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumanam adangidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koadi sonthangal kottikkodukkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koadi sonthangal kottikkodukkindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravai adhu minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravai adhu minjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nayam oru nodi pin vedi saravedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayam oru nodi pin vedi saravedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagathai padaippin adi adhiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai padaippin adi adhiradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayakkangal aagidum thavittuppodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkangal aagidum thavittuppodi"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisai engum parandhidum kaadhal kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisai engum parandhidum kaadhal kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi munaiyum buththi munaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi munaiyum buththi munaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">inaiyum kaadhaliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaiyum kaadhaliley"/>
</div>
<div class="lyrico-lyrics-wrapper">natpin kanavum karppin kanavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpin kanavum karppin kanavum "/>
</div>
<div class="lyrico-lyrics-wrapper">ulukkum kaadhaliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulukkum kaadhaliley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kachchatheevu poagalaamaa penney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kachchatheevu poagalaamaa penney"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal solli meetkalaamaa anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal solli meetkalaamaa anbey"/>
</div>
</pre>
