---
title: "oh oh cheli song lyrics"
album: "Ninnu Thalachi"
artist: "Yellender mahaveera"
lyricist: "poornachary"
director: "Anil thota"
path: "/albums/ninnu-thalachi-lyrics"
song: "Oh Oh Cheli"
image: ../../images/albumart/ninnu-thalachi.jpg
date: 2019-09-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TuskiEzH7js"
type: "love"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh Oh Cheli  Evare Nuvvu Lalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Cheli  Evare Nuvvu Lalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Alajadi Telusa Nivalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Alajadi Telusa Nivalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Nuvvu Nee Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nuvvu Nee Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Loonu Nee Veenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Loonu Nee Veenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuko E Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuko E Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha Talape Teriche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha Talape Teriche"/>
</div>
<div class="lyrico-lyrics-wrapper">Enduko E Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enduko E Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevaipe Nadiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevaipe Nadiche"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Cheli Evare Lalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Cheli Evare Lalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Alajadi Telusanivalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Alajadi Telusanivalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagithala Rathale Rayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagithala Rathale Rayana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Cheretattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Cheretattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Galilona Bommale Geeyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galilona Bommale Geeyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Arthamay Tattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arthamay Tattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhasakokka Theeru Peru Pettana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhasakokka Theeru Peru Pettana"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasalona Ninnu Dache Vidhamga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasalona Ninnu Dache Vidhamga"/>
</div>
<div class="lyrico-lyrics-wrapper">Putakokkasari Niinnu Tattana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putakokkasari Niinnu Tattana"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Janmalina Thode Vundanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Janmalina Thode Vundanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arare Ni Pilupe Etu Ramani Piliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arare Ni Pilupe Etu Ramani Piliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Gala Gala Mani Kadhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gala Gala Mani Kadhiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yadha Gadhalale Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadha Gadhalale Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Cheli  Evare Nuvvu Lalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Cheli  Evare Nuvvu Lalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Alajadi Telusa Nivalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Alajadi Telusa Nivalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhehamantha Neerula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhehamantha Neerula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvila Alli Anni Vipula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvila Alli Anni Vipula"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooramentha Leduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooramentha Leduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Illu Kattinavuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Illu Kattinavuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Mata Loone Cheppananthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Mata Loone Cheppananthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedu Theeyagaye Okko Kshanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedu Theeyagaye Okko Kshanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnukuda Nuvvu Chuda Nanthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnukuda Nuvvu Chuda Nanthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Rendu Chuse Ninne Elaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Rendu Chuse Ninne Elaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiyadamala Naduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyadamala Naduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Padi Katha Modhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Padi Katha Modhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Erukannula Kalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erukannula Kalala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapade Mari Manala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapade Mari Manala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Cheli  Evare Nuvvu Lalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Cheli  Evare Nuvvu Lalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Alajadi Telusanivalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Alajadi Telusanivalana"/>
</div>
</pre>
