---
title: "aiythaaney song lyrics"
album: "Mudhal Idam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "R. Kumaran"
path: "/albums/mudhal-idam-lyrics"
song: "Aiythaaney"
image: ../../images/albumart/mudhal-idam.jpg
date: 2011-08-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hwbSRMxFGBE"
type: "love"
singers:
  - D. Imman
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aiththaanae Aiththaanae Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththaanae Aiththaanae Kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnullae Unnullae Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullae Unnullae Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Aiththaanae Aiththaanae Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Aiththaanae Aiththaanae Kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnullae Unnullae Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullae Unnullae Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaadhae Idhu Polae Maanae Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaadhae Idhu Polae Maanae Maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yendru Solvenae Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yendru Solvenae Naanae Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamalae Pogadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamalae Pogadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaalumae Kekkaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaalumae Kekkaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Boodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Boodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari Dhaanae Innum Innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Dhaanae Innum Innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiththaanae Aiththaanae Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththaanae Aiththaanae Kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Im Im Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Im Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnullae Unnullae Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullae Unnullae Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Hoeththanaiyo Azhagaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Hoeththanaiyo Azhagaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Nilavu Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Nilavu Irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhayam Naanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhayam Naanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kaaranam Mm Mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kaaranam Mm Mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattazhagil Uruvaagum Kaadhal Vaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattazhagil Uruvaagum Kaadhal Vaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vida Magaraasi Ooril Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vida Magaraasi Ooril Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara Naanum Bayandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Naanum Bayandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Nee Muththam Keppiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Nee Muththam Keppiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Podi Muththam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Podi Muththam Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththam Thevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththam Thevai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiththaanae Aiththaanae Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththaanae Aiththaanae Kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnullae Unnullae Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullae Unnullae Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Kekkaadhae Idhu Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Kekkaadhae Idhu Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanae Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae Maanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Ha Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ha Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endru Solvenae Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endru Solvenae Naanae Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Ha Haa Ennudaiya Ninaivaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ha Haa Ennudaiya Ninaivaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppozhudhum Iruppaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppozhudhum Iruppaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Unna Serndhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Unna Serndhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaripoviyaa Ohh Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaripoviyaa Ohh Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingeedhamae Theriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingeedhamae Theriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechcha Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechcha Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Vittaa Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Vittaa Kidaiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Kaaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Kaaththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayadhaagi Vidum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadhaagi Vidum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nee Thalli Poviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Thalli Poviyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyir Neeyae Thalli Ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Neeyae Thalli Ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu Poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu Poven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkaadhae Idhu Polae Maanae Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaadhae Idhu Polae Maanae Maanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Ha Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ha Ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endru Solvenae Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endru Solvenae Naanae Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh He He
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh He He"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaamalae Pogadhu Dhooram Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamalae Pogadhu Dhooram Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaalum Kekkaadhae Kaadhal Boodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaalum Kekkaadhae Kaadhal Boodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seri Dhaanae Innum Innum Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seri Dhaanae Innum Innum Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiththaanae Aiththaanae Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiththaanae Aiththaanae Kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Im Imoh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Imoh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnullae Unnullae Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnullae Unnullae Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Haa"/>
</div>
</pre>
