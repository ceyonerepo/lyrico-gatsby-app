---
title: "ponapokkil song lyrics"
album: "Adhe Kangal"
artist: "Ghibran"
lyricist: "Parvathy"
director: "Rohin Venkatesan"
path: "/albums/adhe-kangal-lyrics"
song: "Ponapokkil"
image: ../../images/albumart/adhe-kangal.jpg
date: 2017-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0mE19eRtHgs"
type: "love"
singers:
  - Namratha S Aravindan
  - Anudeep Dev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pona pokkil sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona pokkil sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna vaarthayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna vaarthayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillena irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillena irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu unmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu unmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum cheeni sertthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum cheeni sertthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesinaai idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesinaai idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattena asandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena asandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum penmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum penmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanumae naanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanumae naanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai embi embi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai embi embi"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam theendavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam theendavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egamaai engavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egamaai engavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum innum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai thedavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai thedavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thediyae aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediyae aasaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalayaai soodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalayaai soodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalelaam unnaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalelaam unnaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttravaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona pokkil sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona pokkil sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna vaarthayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna vaarthayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillena irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillena irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu unmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu unmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum cheeni sertthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum cheeni sertthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesinaai idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesinaai idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattena asandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena asandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum aanmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum aanmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam koottavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam koottavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli sellavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli sellavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai nirkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai nirkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Killi paarkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killi paarkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli thorkkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli thorkkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyil aayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyil aayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattram varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattram varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru naan indruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru naan indruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu kolgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu kolgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai kooridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai kooridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannin vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannin vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyae kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyae kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saarai saarayaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarai saarayaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarai thaaraiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarai thaaraiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbu thekkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu thekkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam konjamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam konjamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serttha sorkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serttha sorkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhin vaayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhin vaayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu serkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu serkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooralai endhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralai endhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai neettinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai neettinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooralaai mattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralaai mattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovi nindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovi nindradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru en ullamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru en ullamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaikkum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaikkum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Peidhidum vaanmazhaii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peidhidum vaanmazhaii"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona pokkil sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona pokkil sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna vaarthayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna vaarthayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillena irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillena irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu unmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu unmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum cheeni sertthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum cheeni sertthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesinaai idho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesinaai idho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattena asandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena asandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum penmaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum penmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanumae naanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanumae naanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai embi embi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai embi embi"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam theendavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam theendavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egamaai engavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egamaai engavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum innum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai thedavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai thedavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkatthil vandhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkatthil vandhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochilae vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochilae vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiyamai maiyalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyamai maiyalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuthae"/>
</div>
</pre>
