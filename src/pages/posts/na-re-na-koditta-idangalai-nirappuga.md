---
title: "na re na song lyrics"
album: "Koditta Idangalai Nirappuga"
artist: "C Sathya"
lyricist: "Madhan Karky"
director: "Parthiban"
path: "/albums/koditta-idangalai-nirappuga-lyrics"
song: "Na Re Na"
image: ../../images/albumart/koditta-idangalai-nirappuga.jpg
date: 2017-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0rWzC_xWUCw"
type: "love"
singers:
  - Kharesma Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Renaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rigirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Therigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therigirenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Renaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raigirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraigirenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peyaai Oru Peyaai Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyaai Oru Peyaai Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Panikkaattil Uraindhirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panikkaattil Uraindhirundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai Perundh Dheeyaai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai Perundh Dheeyaai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Pattu Uyirthezhundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Pattu Uyirthezhundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Yaarena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yaarena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarkkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Acchchathilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acchchathilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Verkkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Verkkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuruveriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuruveriya"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhazhkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhazhkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurudhuruvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurudhuruvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasindhu Urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasindhu Urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nerungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nerungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi Kirangidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi Kirangidaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Norungidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norungidaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeril Aadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril Aadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neydhu Naan Uduthi Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neydhu Naan Uduthi Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Konde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Konde"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Neekkaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Neekkaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Vaasikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Vaasikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennanjal Naandhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennanjal Naandhaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuchollai Ennaik Ketkaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuchollai Ennaik Ketkaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannai Moodi Thedinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Moodi Thedinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Therivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therivene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thoda Naadinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thoda Naadinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Naa Naa Naa Naa Kirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naa Naa Naa Naa Kirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Naa Naa Naa Rigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Naa Naa Naa Rigirenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Naa Naa Naa Therigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Naa Naa Naa Therigirenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasindhu Urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasindhu Urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nerungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nerungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi Kirangidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi Kirangidaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Norungidaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norungidaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therigirenaa Therigirenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therigirenaa Therigirenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasam Thekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Thekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaitha Poovaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaitha Poovaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Irundhdhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Irundhdhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhdhan Suvaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhdhan Suvaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serkka Vandhaayoa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serkka Vandhaayoa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasal Pootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal Pootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaitha Kaadaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaitha Kaadaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kidandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kidandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saavi Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavi Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Endraayoa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Endraayoa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyam Kondu Nambinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyam Kondu Nambinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Therivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therivene"/>
</div>
<div class="lyrico-lyrics-wrapper">Meyye Endru Naadinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meyye Endru Naadinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivene"/>
</div>
</pre>
