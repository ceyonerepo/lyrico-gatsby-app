---
title: "sonthathukul suzhuchi song lyrics"
album: "Anandham Vilayadum Veedu"
artist: "Siddhu Kumar"
lyricist: "Snehan"
director: "Nandha Periyasamy"
path: "/albums/anandham-vilayadum-veedu-song-lyrics"
song: "Sonthathukul Suzhuchi"
image: ../../images/albumart/anandham-vilayadum-veedu.jpg
date: 2021-12-24
lang: tamil
youtubeLink: 
type: "sad"
singers:
  - Karunguyil Ganesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sonthathukul suzhuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthathukul suzhuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">senjatharu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjatharu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">rathamellam nanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathamellam nanja"/>
</div>
<div class="lyrico-lyrics-wrapper">neram maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonthathukul suzhuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthathukul suzhuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">senjatharu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjatharu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">rathamellam nanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathamellam nanja"/>
</div>
<div class="lyrico-lyrics-wrapper">neram maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjamellam baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjamellam baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal ellam eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal ellam eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">pathagathal paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathagathal paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">thadu maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadu maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theivam vanthu vaaltha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivam vanthu vaaltha"/>
</div>
<div class="lyrico-lyrics-wrapper">veeta theepidika vachatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeta theepidika vachatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">uravukul kalavu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravukul kalavu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangalai paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangalai paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbana katti vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbana katti vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">aagasa pettaya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagasa pettaya than"/>
</div>
<div class="lyrico-lyrics-wrapper">rendaga thundu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendaga thundu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondi vitatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondi vitatharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha then koota than paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha then koota than paru"/>
</div>
<div class="lyrico-lyrics-wrapper">ithil theeya vachathu yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithil theeya vachathu yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">vanjagathal nenju udanchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjagathal nenju udanchu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu katharuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu katharuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">antha baratha por pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha baratha por pola"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kathaiyil sila pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kathaiyil sila pera"/>
</div>
<div class="lyrico-lyrics-wrapper">aada vachu paadam nadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada vachu paadam nadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkai nenaikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkai nenaikuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonthathukul suzhuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthathukul suzhuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">senjatharu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjatharu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">rathamellam nanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathamellam nanja"/>
</div>
<div class="lyrico-lyrics-wrapper">neram maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjamellam baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjamellam baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal ellam eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal ellam eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">pathagathal paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathagathal paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">thadu maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadu maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar valiyai yar inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar valiyai yar inge"/>
</div>
<div class="lyrico-lyrics-wrapper">thangi kolla moorumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangi kolla moorumo"/>
</div>
<div class="lyrico-lyrics-wrapper">yutham thantha kovilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yutham thantha kovilum"/>
</div>
<div class="lyrico-lyrics-wrapper">ratha sontham maarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratha sontham maarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">paasathuku panjam vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasathuku panjam vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu thadu maridumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu thadu maridumo"/>
</div>
<div class="lyrico-lyrics-wrapper">annan thambi uravuku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan thambi uravuku than"/>
</div>
<div class="lyrico-lyrics-wrapper">aayul inge kuraithidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayul inge kuraithidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theivam vanthu vaaltha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theivam vanthu vaaltha"/>
</div>
<div class="lyrico-lyrics-wrapper">veeta theepidika vachatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeta theepidika vachatharu"/>
</div>
<div class="lyrico-lyrics-wrapper">uravukul kalavu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravukul kalavu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">ullangalai paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullangalai paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbana katti vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbana katti vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">aagasa pettaya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagasa pettaya than"/>
</div>
<div class="lyrico-lyrics-wrapper">rendaga thundu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendaga thundu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondi vitatharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondi vitatharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha then koota than paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha then koota than paru"/>
</div>
<div class="lyrico-lyrics-wrapper">ithil theeya vachathu yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithil theeya vachathu yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">vanjagathal nenju udanchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjagathal nenju udanchu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu katharuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu katharuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">antha baratha por pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha baratha por pola"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kathaiyil sila pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kathaiyil sila pera"/>
</div>
<div class="lyrico-lyrics-wrapper">aada vachu paadam nadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada vachu paadam nadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkai nenaikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkai nenaikuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sonthathukul suzhuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthathukul suzhuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">senjatharu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senjatharu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">rathamellam nanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathamellam nanja"/>
</div>
<div class="lyrico-lyrics-wrapper">neram maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram maaruthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjamellam baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjamellam baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal ellam eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal ellam eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">pathagathal paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathagathal paasam"/>
</div>
<div class="lyrico-lyrics-wrapper">thadu maaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadu maaruthe"/>
</div>
</pre>
