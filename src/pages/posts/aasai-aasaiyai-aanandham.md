---
title: "aasai aasaiyai song lyrics"
album: "Aanandham"
artist: "S. A. Rajkumar"
lyricist: "Kalaikumar"
director: "N. Lingusamy"
path: "/albums/aanandham-lyrics"
song: "Aasai Aasaiyai"
image: ../../images/albumart/aanandham.jpg
date: 2001-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RZ6VIEzML-Q"
type: "happy"
singers:
  - K. J. Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aasai Aasaiyaai Irrukiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Aasaiyaai Irrukiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Vaazhndhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Vaazhndhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasa Poo Mazhai Pozhigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasa Poo Mazhai Pozhigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangal Nanaindhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayangal Nanaindhidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammai Kaanugira Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Kaanugira Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammodu Sera Kenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammodu Sera Kenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu Vaazhugira Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu Vaazhugira Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Swargam Thannai Minjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Swargam Thannai Minjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Varamaaghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Varamaaghum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Engal Veedaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Engal Veedaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaai Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Vilaiyaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Vilaiyaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirandhara Aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirandhara Aanandham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Aasaiyaai Irrukiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Aasaiyaai Irrukiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Vaazhndhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Vaazhndhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasa Poo Mazhai Pozhigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasa Poo Mazhai Pozhigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangal Nanaindhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayangal Nanaindhidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Nam Thaayin Mugathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nam Thaayin Mugathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kodi Kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kodi Kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharisanam Naangal Paarthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharisanam Naangal Paarthiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deepangal Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepangal Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Veettil Yetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Veettil Yetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovilai Pola Maatriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovilai Pola Maatriduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaikku Panividai Seidhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaikku Panividai Seidhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jenmangal Vaangi Vandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmangal Vaangi Vandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Jenmangal Maaridum Nerathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Jenmangal Maaridum Nerathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangal Serndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhangal Serndhiruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaivarin Anbil Aayulum Koodidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaivarin Anbil Aayulum Koodidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Aasaiyaai Irrukiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Aasaiyaai Irrukiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Vaazhndhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Vaazhndhidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasa Poo Mazhai Pozhigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasa Poo Mazhai Pozhigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangal Nanaindhidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayangal Nanaindhidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Pala Nooru Vannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Pala Nooru Vannam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam Pola Serndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyam Pola Serndhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaaru Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Perai Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Perai Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvadhai Pola Vaazhndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvadhai Pola Vaazhndhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalukullae Valaindhiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukullae Valaindhiduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanalai Pol Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanalai Pol Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Ottrumai Kaathida Nindriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Ottrumai Kaathida Nindriduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongalai Pol Naamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongalai Pol Naamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai Mazhaiyaaga Peyyum Sandhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Mazhaiyaaga Peyyum Sandhosham"/>
</div>
</pre>
