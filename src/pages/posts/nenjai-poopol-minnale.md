---
title: "nenjai poo pol song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Nenjai Poo pol"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tk4a_YbI6FU"
type: "love"
singers:
  - Harish Raghavendra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjai Poopol Koithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poopol Koithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetho Seithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetho Seithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poopol Koithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poopol Koithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetho Seithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetho Seithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poopol Koithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poopol Koithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetho Seithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetho Seithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai Poopol Koithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Poopol Koithavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetho Seithavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetho Seithavale"/>
</div>
</pre>
