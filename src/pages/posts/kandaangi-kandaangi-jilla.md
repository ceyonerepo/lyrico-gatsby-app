---
title: "kandaangi kandaangi song lyrics"
album: "Jilla"
artist: "D. Imman"
lyricist: "Vairamuthu"
director: "Nesan"
path: "/albums/jilla-song-lyrics"
song: "Kandaangi Kandaangi"
image: ../../images/albumart/jilla.jpg
date: 2014-01-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wJ4i8c1r1Rc"
type: "Love"
singers:
  - Vijay
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kandaangi Kandaangi Katti Vantha Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Kandaangi Katti Vantha Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaale Kirukethum Ganja Vecha Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaale Kirukethum Ganja Vecha Kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi Kandaangi Katti Vantha Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Kandaangi Katti Vantha Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaale Kirukethum Ganja Vecha Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaale Kirukethum Ganja Vecha Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kannukku Anju Latcham Thaaren Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kannukku Anju Latcham Thaaren Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Nenjuku Sothu Ezhuthi Thaarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Nenjuku Sothu Ezhuthi Thaarendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Thariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Thariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi Kandaangi Katti Vantha Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Kandaangi Katti Vantha Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaale Kirukethum Ganja Vecha Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaale Kirukethum Ganja Vecha Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kannukku Anju Latcham Porathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kannukku Anju Latcham Porathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Nenjuku Sothu Ezhuthi Therathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Nenjuku Sothu Ezhuthi Therathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Nillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nillaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Un Veedu Thallakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Un Veedu Thallakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veedu Teppakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veedu Teppakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu Neeru Seratume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerodu Neeru Seratume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagar Malai Koyil Yaana Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagar Malai Koyil Yaana Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Halwa Va Thinbathu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa Va Thinbathu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasa Unna Thinnatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasa Unna Thinnatume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othaikkotha Azhaikkum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaikkotha Azhaikkum Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Pakkam Odhungum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Pakkam Odhungum Pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhikulla Arikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhikulla Arikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethikulla Thudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethikulla Thudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella Muzhi Veliya Theriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella Muzhi Veliya Theriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla Muzhi Muzhikkum Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Muzhi Muzhikkum Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usuru Odunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usuru Odunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera Kola Nadunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eera Kola Nadunguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Chinna Poiyum Pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinna Poiyum Pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillunu Thaan Sudum Yethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillunu Thaan Sudum Yethura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paathaaka Thenna Matta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paathaaka Thenna Matta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanjakka Thegam Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanjakka Thegam Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasangu Venam Sundharare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasangu Venam Sundharare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Theyaatha Naatu Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Theyaatha Naatu Katta"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyama Maatikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyama Maatikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">En Raasi Endrum Manmathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Raasi Endrum Manmathane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla Erangi Erangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla Erangi Erangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla Orangi Orangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Orangi Orangi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usura Parikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usura Parikira"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Seiya Nenaikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seiya Nenaikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambu Vittu Aala Adikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambu Vittu Aala Adikira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbu Vittu Vaala Pudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbu Vittu Vaala Pudikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaali Ilaatha Samsaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali Ilaatha Samsaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai Laa Minsaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Laa Minsaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaketha Vaadi Vennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaketha Vaadi Vennilave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Maarboda Santhaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Maarboda Santhaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaraapu Vaibogame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraapu Vaibogame"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthaada Vaaya Munnirave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthaada Vaaya Munnirave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi Kandaangi Katti Vantha Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Kandaangi Katti Vantha Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaale Kirukethum Ganja Vecha Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaale Kirukethum Ganja Vecha Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kannukku Anju Latcham Porathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kannukku Anju Latcham Porathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Nenjuku Sothu Ezhuthi Therathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Nenjuku Sothu Ezhuthi Therathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Nillaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nillaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandaangi Kandaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaangi Kandaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandaale Kirukethum Ganja Vecha Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandaale Kirukethum Ganja Vecha Kannu"/>
</div>
</pre>
