---
title: "thangamey thangamey sad song lyrics"
album: "Paava Kadhaigal"
artist: "Justin Prabhakaran"
lyricist: "Justin Prabhakaran - Shan Karuppasamy"
director: "Sudha Kongara - Vignesh Shivan - Gautham Vasudev Menon - Vetrimaaran"
path: "/albums/paava-kadhaigal-lyrics"
song: "Thangamey Thangamey Sad"
image: ../../images/albumart/paava-kadhaigal.jpg
date: 2020-12-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gkHN8yaGB5E"
type: "sad"
singers:
  - Justin Prabhakaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engane Magayaasan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engane Magayaasan "/>
</div>
<div class="lyrico-lyrics-wrapper">Marajaaya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marajaaya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Vazhi Paarka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Vazhi Paarka "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Ila Thudichena Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ila Thudichena Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engane Una Thandi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engane Una Thandi "/>
</div>
<div class="lyrico-lyrics-wrapper">Enakaraa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaraa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha Mogam Paarka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha Mogam Paarka "/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Semam Engetha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Semam Engetha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakaaga Karanjayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaaga Karanjayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugaga Erlnjayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhugaga Erlnjayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponaye Etha Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaye Etha Thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Poka Dhoora Desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Poka Dhoora Desam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mana thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mana thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennado thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennado thangamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engane Magayaasan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engane Magayaasan "/>
</div>
<div class="lyrico-lyrics-wrapper">Marajaaya Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marajaaya Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Innu Vazhi Paarka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innu Vazhi Paarka "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Ila Thudichena Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ila Thudichena Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engane Una Thandi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engane Una Thandi "/>
</div>
<div class="lyrico-lyrics-wrapper">Enakaraa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaraa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aantha Mogam Paarka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantha Mogam Paarka "/>
</div>
<div class="lyrico-lyrics-wrapper">Veru Semam Engetha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru Semam Engetha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakaaga Karanjayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakaaga Karanjayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhugaga Erlnjayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhugaga Erlnjayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponaye Etha Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaye Etha Thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Poka Dhoora Desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Poka Dhoora Desam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mana thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mana thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamey Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennado thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennado thangamey"/>
</div>
</pre>
