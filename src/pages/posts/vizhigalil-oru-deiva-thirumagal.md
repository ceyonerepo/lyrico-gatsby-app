---
title: "vizhigalil oru song lyrics"
album: "Deiva Thirumagal"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/deiva-thirumagal-lyrics"
song: "Vizhigalil Oru"
image: ../../images/albumart/deiva-thirumagal.jpg
date: 2011-07-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/osY54GT0T_8"
type: "love"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vizhigalil Oru Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Oru Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigalai Thottu Pesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalai Thottu Pesuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Enna Puthu Vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Enna Puthu Vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Veyil Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Veyil Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnidam Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thaaimugam Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thaaimugam Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodam Thorkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodam Thorkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thorkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thorkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaagumo Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaagumo Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthan Muthalaai Mayangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthan Muthalaai Mayangugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi Pola Thondrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Pola Thondrinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munbu Ennai Kaattinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munbu Ennai Kaattinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa Engum Vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Engum Vinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalil Oru Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Oru Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigalai Thottu Pesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalai Thottu Pesuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Enna Puthu Vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Enna Puthu Vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Veyil Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Veyil Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vanthaai En Vaazhvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthaai En Vaazhvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Poothaai En Verile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Poothaai En Verile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiye Nee Pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiye Nee Pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Niyabagam Nee Aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Niyabagam Nee Aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ther Sendra Pinnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ther Sendra Pinnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Veethi Ennaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veethi Ennaagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Ivan Yaar Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ivan Yaar Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Maayavan Meiyaanavan Anbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Maayavan Meiyaanavan Anbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Ivan Yaar Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Ivan Yaar Ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Nesikkum Kanneer Ivan Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nesikkum Kanneer Ivan Nenjil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inam Puriyaa Uravithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inam Puriyaa Uravithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theevil Pooththa Poovithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theevil Pooththa Poovithu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Vaasam Thoovuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Vaasam Thoovuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Engum Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Engum Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalil Oru Vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Oru Vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigalai Thottu Pesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalai Thottu Pesuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Enna Puthu Vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Enna Puthu Vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Veyil Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Veyil Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unakkaaga Pesinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unakkaaga Pesinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Enakkaaga Pesuvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Enakkaaga Pesuvaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamaai Naan Pesinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaai Naan Pesinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalil Mai Poosinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalil Mai Poosinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vantha Kanavenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vantha Kanavenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Kai Veesinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Kai Veesinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbennum Thoondilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbennum Thoondilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veesinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veesinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenaagiren Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenaagiren Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Munbu Thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Munbu Thaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippothu Naan Pennaagiren Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippothu Naan Pennaagiren Inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayakkangalaal Thinarugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkangalaal Thinarugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillendru Sonna Pothilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillendru Sonna Pothilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaamal Nenjam Oduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaamal Nenjam Oduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho Unthan Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho Unthan Vazhi"/>
</div>
</pre>
