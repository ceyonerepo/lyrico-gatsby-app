---
title: "va va enkooda song lyrics"
album: "Vandi"
artist: "Sooraj S. Kurup"
lyricist: "Snehan"
director: "Rajeesh Bala"
path: "/albums/vandi-song-lyrics"
song: "Va Va Enkooda"
image: ../../images/albumart/vandi.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uCOFOzfDZZs"
type: "happy"
singers:
  - Sithara Krishnakumar
  - Sooraj S Kurup
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Va va enkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va enkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha sollitharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha sollitharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illama vazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illama vazhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thaan sorgathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan sorgathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaikku vaangi irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaikku vaangi irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu thangikko mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu thangikko mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha thanni kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha thanni kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum paththa vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum paththa vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan nenja enna aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan nenja enna aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paartha thala suththikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paartha thala suththikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchan thala modhinaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchan thala modhinaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moola soodu yerikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moola soodu yerikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulusa unna paarthu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulusa unna paarthu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pithu pudikkum yarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithu pudikkum yarukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhka inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru murai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha neeyum kathukkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha neeyum kathukkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrum naalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrum naalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha unmaiyaa othukkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha unmaiyaa othukkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha nimisham thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha nimisham thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sondha nimisham aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sondha nimisham aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kedachatha nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kedachatha nee "/>
</div>
<div class="lyrico-lyrics-wrapper">vittu vidaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu vidaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illa manushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illa manushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu yaar irukka sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu yaar irukka sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu nadantha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu nadantha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku jolly thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku jolly thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va va enkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va enkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha sollitharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha sollitharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illama vazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illama vazhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nattu katta matti kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nattu katta matti kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu kattu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu kattu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Halwa thundu pola irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa thundu pola irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnu puttu puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnu puttu puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellathukkum velai irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellathukkum velai irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesu thuttu thuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesu thuttu thuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirupathikkae kodukalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupathikkae kodukalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya laddu laddu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya laddu laddu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhi rusiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi rusiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolambu vechu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolambu vechu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta sarakka sarakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta sarakka sarakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu yethu naara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu yethu naara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosama vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosama vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba vazhi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba vazhi irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellathaiyum solli tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellathaiyum solli tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar enna sonna pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar enna sonna pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkae thaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkae thaan daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam thunbam rendum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam thunbam rendum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna venum kelu thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna venum kelu thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yega patta aasaigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yega patta aasaigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthu paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthu paarada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamai ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhumai thedum pokkisham daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhumai thedum pokkisham daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai pona thirumbaadhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai pona thirumbaadhu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum bothae nee vazhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum bothae nee vazhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthamana inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthamana inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar irukka kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar irukka kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba unmaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba unmaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhakudathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhakudathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama serntha thaanda joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama serntha thaanda joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil kadavulukkum pangirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil kadavulukkum pangirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam veru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va va enkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va va enkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha sollitharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha sollitharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illama vazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illama vazhaa"/>
</div>
</pre>