---
title: "unnai thedi kath irunthen song lyrics"
album: "Adanga Pasanga"
artist: "AK Alldern"
lyricist: "N Jaigar Devadass"
director: "R. Selvanathan"
path: "/albums/adanga-pasanga-lyrics"
song: "Unnai Thedi Kath Irunthen"
image: ../../images/albumart/adanga-pasanga.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dc8kVH6xID4"
type: "love"
singers:
  - Sujith
  - Anu
  - Ashwini 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">unnai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">mounam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukul malarntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukul malarntha "/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare"/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare panai malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare panai malare"/>
</div>
<div class="lyrico-lyrics-wrapper">alage vaa nilave vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage vaa nilave vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam veesa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam veesa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">mounam aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul viluntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul viluntha "/>
</div>
<div class="lyrico-lyrics-wrapper">pani malar naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malar naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thedi thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">mounam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukul malarntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukul malarntha "/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare"/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare panai malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare panai malare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mayile vaa un manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayile vaa un manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vaalanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vaalanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuyile vaa unthan kuralaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuyile vaa unthan kuralaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan maaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan maaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">inikira nilava enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inikira nilava enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">ulle nee suvaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulle nee suvaikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam ellam enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam ellam enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula nee pesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula nee pesanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nilavai vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavai vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">nimmathi tharuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimmathi tharuva"/>
</div>
<div class="lyrico-lyrics-wrapper">oliyaai vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyaai vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula vasippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula vasippa"/>
</div>
<div class="lyrico-lyrics-wrapper">ennam kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennam kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">urugi pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urugi pova"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam muluthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam muluthum"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaai varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaai varuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thedi thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">mounam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukul malarntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukul malarntha "/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare"/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare panai malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare panai malare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aruvi malaiyila nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruvi malaiyila nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nananju nadakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nananju nadakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kurunji malarin theniya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurunji malarin theniya "/>
</div>
<div class="lyrico-lyrics-wrapper">pola then suvaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola then suvaikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal paarka kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal paarka kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">pesa kannam inikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesa kannam inikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">thendralukkule naam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendralukkule naam "/>
</div>
<div class="lyrico-lyrics-wrapper">thelinavu thaan nadakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelinavu thaan nadakanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megamaaga nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megamaaga nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ennil mithakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennil mithakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">karainthu poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karainthu poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">maalai kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">naam manakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam manakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">soolai malarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soolai malarai "/>
</div>
<div class="lyrico-lyrics-wrapper">nee mathaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mathaganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thedi thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">mounam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukul malarntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukul malarntha "/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare"/>
</div>
<div class="lyrico-lyrics-wrapper">pani malare panai malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malare panai malare"/>
</div>
<div class="lyrico-lyrics-wrapper">alage vaa nilave vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage vaa nilave vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam veesa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam veesa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu irunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu irunthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pesa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pesa "/>
</div>
<div class="lyrico-lyrics-wrapper">mounam aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarnthu vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarnthu vanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">manasukkul viluntha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasukkul viluntha "/>
</div>
<div class="lyrico-lyrics-wrapper">pani malar naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani malar naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thedi thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">kathu irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu irunthen"/>
</div>
</pre>
