---
title: "80's kid song lyrics"
album: "Yennanga Sir Unga Sattam"
artist: "Guna Balasubramanian"
lyricist: "Ranjith VJ"
director: "Prabhu Jeyaram"
path: "/albums/yennanga-sir-unga-sattam-lyrics"
song: "80's Kid"
image: ../../images/albumart/yennanga-sir-unga-sattam.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r-PQRo4xGMI"
type: "love"
singers:
  - Vineeth Sreenivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiye na 80’s kid u theva illa orkut
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye na 80’s kid u theva illa orkut"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan targetu sweet heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan targetu sweet heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Agrahava paal sweet u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agrahava paal sweet u"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagula thaan nee chute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula thaan nee chute"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkula chemistry work out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkula chemistry work out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Butterfly heart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Butterfly heart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada adikkutha en beat u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada adikkutha en beat u"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna follow pannaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna follow pannaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto en flight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto en flight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada petromax u light u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada petromax u light u"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan di romba bright
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan di romba bright"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kolam podum style u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kolam podum style u"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba hot u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba hot u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye na 80’s kid u theva illa orkut
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye na 80’s kid u theva illa orkut"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan targetu sweet heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan targetu sweet heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Agrahava paal sweet u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agrahava paal sweet u"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagula thaan nee chute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula thaan nee chute"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkula chemistry work out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkula chemistry work out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan en white rice u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan en white rice u"/>
</div>
<div class="lyrico-lyrics-wrapper">Pepsi eyes su smile please u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pepsi eyes su smile please u"/>
</div>
<div class="lyrico-lyrics-wrapper">Avendi unakinga boss u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avendi unakinga boss u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en hight breed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en hight breed"/>
</div>
<div class="lyrico-lyrics-wrapper">Adipen di unna sight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adipen di unna sight"/>
</div>
<div class="lyrico-lyrics-wrapper">En life u shy type u boat u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life u shy type u boat u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na mic mohan fan u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na mic mohan fan u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi paaduven romance tune u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi paaduven romance tune u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee takkarana thenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee takkarana thenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan en moon u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan en moon u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan sakthimaanu power u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sakthimaanu power u"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna paaththa yerum sugar u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paaththa yerum sugar u"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna katti thookka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna katti thookka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee flower enooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee flower enooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan en white rice u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan en white rice u"/>
</div>
<div class="lyrico-lyrics-wrapper">Pepsi eyes su smile please u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pepsi eyes su smile please u"/>
</div>
<div class="lyrico-lyrics-wrapper">Avendi unakinga boss u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avendi unakinga boss u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi en hight breed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi en hight breed"/>
</div>
<div class="lyrico-lyrics-wrapper">Adipen di unna sight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adipen di unna sight"/>
</div>
<div class="lyrico-lyrics-wrapper">En life u shy type u boat u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life u shy type u boat u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanthan di 80’kid u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthan di 80’kid u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikuthadi hight volt u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikuthadi hight volt u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gold u my soul u figure u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gold u my soul u figure u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan di sema fire u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan di sema fire u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweat aachu en uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweat aachu en uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mid night u moon light u white u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mid night u moon light u white u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Back ground unnoda sound
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Back ground unnoda sound"/>
</div>
<div class="lyrico-lyrics-wrapper">Feel aguthu ennod mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feel aguthu ennod mind"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan patta poda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan patta poda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennoda land u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennoda land u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovapadatha kuyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadatha kuyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paakkanum unnoda smile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paakkanum unnoda smile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan james bond u aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan james bond u aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi neethan en girl u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi neethan en girl u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanthan di 80’kid u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthan di 80’kid u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikuthadi hight volt u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikuthadi hight volt u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gold u my soul u figure u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gold u my soul u figure u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethan di sema fire u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan di sema fire u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweat aachu en uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweat aachu en uyiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mid night u moon light u white u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mid night u moon light u white u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
80’<div class="lyrico-lyrics-wrapper">s kid
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s kid"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal sweet u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal sweet u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet heart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet heart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Work out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Work out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
80’<div class="lyrico-lyrics-wrapper">s kid
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s kid"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal sweet u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal sweet u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet heart u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet heart u"/>
</div>
<div class="lyrico-lyrics-wrapper">Work out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Work out"/>
</div>
</pre>
