---
title: "en oruthiye song lyrics"
album: "Koditta Idangalai Nirappuga"
artist: "C Sathya"
lyricist: "Kabilan"
director: "Parthiban"
path: "/albums/koditta-idangalai-nirappuga-lyrics"
song: "En Oruthiye"
image: ../../images/albumart/koditta-idangalai-nirappuga.jpg
date: 2017-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GjKuqNdc9Q8"
type: "love"
singers:
  - Sreerama Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaarai kettum pookkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai kettum pookkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaadhal paarvai paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal paarvai paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai yaavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai yaavarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarai kettum pookkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai kettum pookkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaadhal paarvai paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal paarvai paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai yaavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai yaavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En oruthiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En oruthiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">kannakoththi poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakoththi poriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakoththi poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakoththi poriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thorathiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thorathiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottu poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu poriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhugaiyaai monaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhugaiyaai monaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum irundha naal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum irundha naal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kavaidhaipol inikiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavaidhaipol inikiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inithidum idhayathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inithidum idhayathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival vizhi erumbai polathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival vizhi erumbai polathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi kadikiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi kadikiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagiya vizhi yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya vizhi yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya kanavaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya kanavaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagiya vizhi yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagiya vizhi yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya kanavaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya kanavaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kadhavinai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kadhavinai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal mudhal thirandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal mudhal thirandhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin mudhuginil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin mudhuginil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal mudhal parandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal mudhal parandhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai thandhai illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai thandhai illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakkum pillai edhuvndraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkum pillai edhuvndraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vandha pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandha pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalin melae mulaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalin melae mulaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja poochedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja poochedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarai kettum pookkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai kettum pookkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaadhal paarvai paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal paarvai paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai yaavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai yaavarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarai kettum pookkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai kettum pookkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaadhal paarvai paarkkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal paarvai paarkkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai yaavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai yaavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En oruthiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En oruthiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">kannakoththi poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakoththi poriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thorathiyae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thorathiyae "/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottu poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottu poriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakoththi poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakoththi poriyae"/>
</div>
</pre>
