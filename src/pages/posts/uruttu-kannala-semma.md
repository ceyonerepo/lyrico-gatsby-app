---
title: "uruttu kannala song lyrics"
album: "Semma"
artist: "G.V. Prakash Kumar"
lyricist: "Ekadesi"
director: "Vallikanthan"
path: "/albums/semma-lyrics"
song: "Uruttu Kannala"
image: ../../images/albumart/semma.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JoJmHrK4YzI"
type: "happy"
singers:
  - Santosh Hariharan
  - M.M. Monisha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uruttu Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruttu Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasi Porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasi Porale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayaiyaiyoo Nenju Kuzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayaiyaiyoo Nenju Kuzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikichu Thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikichu Thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppu Thandaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Thandaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyum Rendaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyum Rendaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Soki Soki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Soki Soki"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallisukka Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallisukka Aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megatha Katharichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megatha Katharichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Ponnada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Ponnada"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Mayakkari Mandaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Mayakkari Mandaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Varaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Neththi Pottaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Neththi Pottaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Ittu Kittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Ittu Kittale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Micham Veikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Micham Veikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Thinnuputtale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Thinnuputtale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kanna Pinaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Pinaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Konnuporaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Konnuporaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kaana Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kaana Ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Kandaa Sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Kandaa Sollunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhi Saagatuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi Saagatuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Keda Muttu Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keda Muttu Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochil Vegatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochil Vegatumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthu Pechi Munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthu Pechi Munnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keera Kattupola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keera Kattupola"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhal Mudi Pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal Mudi Pinnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Sanjiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Sanjiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiyila Thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyila Thannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Beedi Pattha Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Beedi Pattha Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Pogaya Pola Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pogaya Pola Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Beera Thorandhu Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Beera Thorandhu Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Noriya Pola Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Noriya Pola Varuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Theateruku Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Theateruku Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Heroine-ah Theriva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Heroine-ah Theriva"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Seriala Paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Seriala Paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Marumagala Azhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Marumagala Azhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Idiyoda Mazhai Peiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiyoda Mazhai Peiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Yen Veetil Vandhu Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Yen Veetil Vandhu Ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vadaa Malliya Attam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadaa Malliya Attam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Kita Ninnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Kita Ninnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Soda Thelichu Yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soda Thelichu Yenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookanume Kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookanume Kaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhathanda Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhathanda Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaattamaana Ponnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaattamaana Ponnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vaangathane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vaangathane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Ninnen Munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Ninnen Munnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Sadha Dosa Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sadha Dosa Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nei Oothivecha Roastu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nei Oothivecha Roastu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Poi Solla Villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Poi Solla Villa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Romba Romba Tastu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Romba Romba Tastu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Watermelon Juice
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Watermelon Juice"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Mokkaiyaana Piecu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Mokkaiyaana Piecu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhalidhaan Maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhalidhaan Maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththa Ellarume Thoosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththa Ellarume Thoosu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athi Kaalai Tea Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athi Kaalai Tea Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakama Povadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakama Povadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Uruttu Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Uruttu Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasi Porale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasi Porale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayaiyaiyoo Nenju Kuzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayaiyaiyoo Nenju Kuzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikichu Thannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikichu Thannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppu Thandaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Thandaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyum Rendaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyum Rendaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Soki Soki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Soki Soki"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallisukka Aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallisukka Aanene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megatha Katharichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megatha Katharichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Ponnada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Ponnada"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Mayakkari Mandaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Mayakkari Mandaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Varaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Varaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Neththi Pottaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Neththi Pottaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Ittu Kittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Ittu Kittale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Micham Veikkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Micham Veikkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Thinnuputtale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Thinnuputtale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Kanna Pinaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Pinaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Konnuporaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Konnuporaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kaana Ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kaana Ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Kandaa Sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Kandaa Sollunga"/>
</div>
</pre>
