---
title: "inga vaanthey song lyrics"
album: "Mudhal Idam"
artist: "D. Imman"
lyricist: "Arivumathi"
director: "R. Kumaran"
path: "/albums/mudhal-idam-lyrics"
song: "Inga Vaanthey"
image: ../../images/albumart/mudhal-idam.jpg
date: 2011-08-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7_uoJVuhc7c"
type: "love"
singers:
  - Haricharan
  - Surmukhi Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenthaanae Neenthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenthaanae Neenthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Pethi Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Pethi Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Poga Nee Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Nee Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalum Vellai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalum Vellai Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochchil Pookkum Thaavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochchil Pookkum Thaavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Moondram Paalil Vervidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Moondram Paalil Vervidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thaanae Nee Thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thaanae Nee Thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Pethi Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Pethi Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Poga Nee Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Nee Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalum Vellai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalum Vellai Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Haa Moochil Pookkum Thavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa Moochil Pookkum Thavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Moondram Paalil Vervidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Moondram Paalil Vervidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Seendathae Seendathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Seendathae Seendathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Koondhalil Un Kadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Koondhalil Un Kadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinniyae Pinniyae Parpenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinniyae Pinniyae Parpenada"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naatkalai Un Moochinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naatkalai Un Moochinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhven Naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhven Naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Koothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Koothae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koothae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadum Naathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum Naathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rettai Udal Ottrai Uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai Udal Ottrai Uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Thaan Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Thaan Paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Irul Netri Mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Irul Netri Mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottae Uyir Pookiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottae Uyir Pookiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Ninaithayae Thummi Salithaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ninaithayae Thummi Salithaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharara Naanae Tharara Naanaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharara Naanae Tharara Naanaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharara Naanae Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharara Naanae Naanae Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaipesiyae Vetkapada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaipesiyae Vetkapada"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththamae Sathamaai Thanthaaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththamae Sathamaai Thanthaaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiyae Kaigal Thatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyae Kaigal Thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthom Naamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthom Naamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigal Koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Koosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgal Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mittaai Silai Thotten Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mittaai Silai Thotten Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Guitar Isai Ketkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guitar Isai Ketkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottal Mazhai Vittal Veyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottal Mazhai Vittal Veyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattaryena Thaakuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattaryena Thaakuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Udai Poada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Udai Poada"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Yedai Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Yedai Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Seendathae Seendathae Chella Chella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Vaanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Vaanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Pondhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Pondhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaanae Neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanae Neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Pethi Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Pethi Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Poga Nee Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Nee Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalum Vellai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalum Vellai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendathae Seendathae Chella Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendathae Seendathae Chella Chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh"/>
</div>
</pre>
