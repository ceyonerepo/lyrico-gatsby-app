---
title: "ola veedu nallaala song lyrics"
album: "Pakka"
artist: "C. Sathya"
lyricist: "Yugabharathi"
director: "S.S. Surya"
path: "/albums/pakka-song-lyrics"
song: "Ola Veedu Nallaala"
image: ../../images/albumart/pakka.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ic-G7HscfDI"
type: "happy"
singers:
  - Mathichiyam Bala
  - Palaniammal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kolaaru thangakatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru thangakatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gum iruttu vellakatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gum iruttu vellakatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaana chellakutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaana chellakutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikava uchiuch uch uch kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikava uchiuch uch uch kotti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pangu uchi kotnathu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu uchi kotnathu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatta aarambinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatta aarambinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ola veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga veetta ezhudhi vaangu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga veetta ezhudhi vaangu di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga madiyil vasikka naa ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga madiyil vasikka naa ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ootta cycle nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootta cycle nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Local bus-um nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local bus-um nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Car vangi kodukka sollu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car vangi kodukka sollu di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Audi car nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Audi car nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Benz car nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Benz car nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi nadayaa nadakka naan ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi nadayaa nadakka naan ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna aruvamanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna aruvamanaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aru di aru di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aru di aru di"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli tharuven alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli tharuven alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Porudi porudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porudi porudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna uruvaa surukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna uruvaa surukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudi varudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudi varudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu naduvae mudiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu naduvae mudiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi thirudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga veetta ezhudhi vaangu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga veetta ezhudhi vaangu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga madiyil vasikka naa ready hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga madiyil vasikka naa ready hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavulukaaranlaam veedu vangithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavulukaaranlaam veedu vangithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaltha otta veedukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaltha otta veedukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva en madila padukka ready yam hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva en madila padukka ready yam hmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yow 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yow "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee izhutha izhuppukku lam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee izhutha izhuppukku lam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvennu nenachiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvennu nenachiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En ottu motha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ottu motha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yay seemayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yay seemayila"/>
</div>
<div class="lyrico-lyrics-wrapper">En ottu motha seemayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ottu motha seemayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnavida oorazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnavida oorazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illanuthan solla vanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illanuthan solla vanthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthumani maalayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthumani maalayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnalagu daaladikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnalagu daaladikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottuka naan enna thandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottuka naan enna thandhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulla malli poovazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla malli poovazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhugira un azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhugira un azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollayida pulli vachanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollayida pulli vachanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli nila vinnazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli nila vinnazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinjigira pinnazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinjigira pinnazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Killithara vantha machanaeeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killithara vantha machanaeeyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi selai kalakkura aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi selai kalakkura aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemma naan polikaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemma naan polikaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla nee paala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla nee paala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothikkira vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothikkira vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaatha thara mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaatha thara mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ola veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahaan haahaan haahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaan haahaan haahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ola veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga veetta ezhudhi vaangu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga veetta ezhudhi vaangu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilae "/>
</div>
<div class="lyrico-lyrics-wrapper">Maadi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga madiyil vasikka naa ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga madiyil vasikka naa ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei eppadiyavuthu vangi kudri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei eppadiyavuthu vangi kudri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadiyae sollikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadiyae sollikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Root ah kuduthukuttae irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Root ah kuduthukuttae irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumbothu mayilu vangittu vanthuri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumbothu mayilu vangittu vanthuri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalavu neram aadunathu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalavu neram aadunathu pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilu maamanukkandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilu maamanukkandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum light ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum light ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Itha itha oru kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha itha oru kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey aadudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aadudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa apudi thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa apudi thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayanakaara mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayanakaara mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavilukkaara chitappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavilukkaara chitappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkanthamenikkae irukinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkanthamenikkae irukinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthirichi adinga ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthirichi adinga ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Apudithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apudithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Apudithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apudithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Superu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pacha pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pacha pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">En pallazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pallazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">En pachapattu pallazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pachapattu pallazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaku vetti kannazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaku vetti kannazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathula vantha thevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula vantha thevala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En inji mittai sollazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En inji mittai sollazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eerakola nenjazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerakola nenjazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetti ninnu sollum kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetti ninnu sollum kaadhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kammakara neerazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammakara neerazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai adangaa maarazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai adangaa maarazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathada nee onna kaatammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathada nee onna kaatammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halwa kadai vaayazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halwa kadai vaayazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayaarettu sorazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayaarettu sorazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Allithinna ennangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allithinna ennangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katthaala kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthaala kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadharudhu maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadharudhu maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick ah nee vilayaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick ah nee vilayaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthaala medu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthaala medu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurunguthu roadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurunguthu roadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappama kalavaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappama kalavaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ola…ola veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola…ola veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga veetta ezhudhi vaangu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga veetta ezhudhi vaangu di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga madiyil vasikka naa ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga madiyil vasikka naa ready"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna aruvamanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna aruvamanaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aru di aru di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aru di aru di"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli tharuven alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli tharuven alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Porudi porudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porudi porudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna uruvaa surukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna uruvaa surukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudi varudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudi varudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu naduvae mudiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu naduvae mudiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi thirudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ola ola ola ola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola ola ola ola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola veedu nallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola veedu nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottu veedu nallaalayae di haaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottu veedu nallaalayae di haaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey heyappadithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey heyappadithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey appadi podu sabashu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey appadi podu sabashu"/>
</div>
</pre>
