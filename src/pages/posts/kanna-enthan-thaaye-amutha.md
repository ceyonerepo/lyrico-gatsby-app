---
title: "kanna enthan thaaye song lyrics"
album: "Amutha"
artist: "Arun Gopan"
lyricist: "G. Ra"
director: "P.S. Arjun"
path: "/albums/amutha-lyrics"
song: "Kanna Enthan Thaaye"
image: ../../images/albumart/amutha.jpg
date: 2018-05-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bZRO-mJpQoA"
type: "melody"
singers:
  - KS Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanna Enthan Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Enthan Thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Neeyum Seyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Neeyum Seyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennuyir Unnidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyir Unnidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil Nee Sankamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil Nee Sankamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Kadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Kadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaane Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaane Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Enthan Thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Enthan Thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Neeyum Seaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Neeyum Seaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Kaalam Therum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Kaalam Therum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyul Thudikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyul Thudikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Padapadakkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Padapadakkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiyil Malaraayi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiyil Malaraayi "/>
</div>
<div class="lyrico-lyrics-wrapper">Manathum Thudithudikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathum Thudithudikkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmani Kaadhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmani Kaadhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannimai Thoongumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannimai Thoongumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalin Osaiyayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Osaiyayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathukal Ketkkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathukal Ketkkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Povatho Ulaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povatho Ulaa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruthey Nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruthey Nilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorikai Itho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorikai Itho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam Atho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyam Atho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kankal Theendum Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankal Theendum Theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Alaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Alaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Moozhgum Theeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moozhgum Theeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullilum Thoongidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullilum Thoongidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennilai Sankadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennilai Sankadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Kadhalii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Kadhalii"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaane Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaane Kanaa"/>
</div>
</pre>
