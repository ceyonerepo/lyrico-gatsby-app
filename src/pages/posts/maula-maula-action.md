---
title: "maula maula song lyrics"
album: "Action"
artist: "Hiphop Tamizha"
lyricist: "Pa. Vijay"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Maula Maula"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7LTWDJ16Evw"
type: "happy"
singers:
  - Nikhita Gandhi
  - Kutle Khan
  - Bamba Bakya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Inai Serum Kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai Serum Kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vaazhvil Punnagaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaazhvil Punnagaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inai Serum Kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai Serum Kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vaazhvil Punnagaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaazhvil Punnagaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajah Gulaabi Aajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajah Gulaabi Aajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaamu Solluvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaamu Solluvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabayil Kondadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabayil Kondadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajah Gulaabi Aajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajah Gulaabi Aajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaamu Seivomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaamu Seivomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabayil Kondadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabayil Kondadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darbaril Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darbaril Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaatam Qurbaani Hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaatam Qurbaani Hae"/>
</div>
<div class="lyrico-lyrics-wrapper">Darbaril Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darbaril Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaatam Qurbani Hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaatam Qurbani Hae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aankhon Ka Tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon Ka Tara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein Dil Ka Sitara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein Dil Ka Sitara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon Ka Tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon Ka Tara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein Dil Ka Sitara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein Dil Ka Sitara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aankhon Ka Tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon Ka Tara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein Dil Ka Sitara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein Dil Ka Sitara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon Ka Tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon Ka Tara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein Dil Ka Sitara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein Dil Ka Sitara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkaavil Unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaavil Unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraalae Aaruyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraalae Aaruyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivellaam Neeyavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivellaam Neeyavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraalae Per Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraalae Per Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ifthaaril Unai Mattum Kandaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ifthaaril Unai Mattum Kandaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathai Thanadhaaki Kondaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathai Thanadhaaki Kondaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhodu Thogai Pol Saaindhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhodu Thogai Pol Saaindhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhan Endraalae Kannaalan Endraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan Endraalae Kannaalan Endraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae Haai Aaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae Haai Aaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Uravellaam Unadhaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Uravellaam Unadhaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Nee Uyiraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Nee Uyiraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaai Varuvaai Varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaai Varuvaai Varuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illae De Ask
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illae De Ask"/>
</div>
<div class="lyrico-lyrics-wrapper">Illae Illae Illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illae Illae Illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tesekkur Ederim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tesekkur Ederim"/>
</div>
<div class="lyrico-lyrics-wrapper">Illae Illae Illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illae Illae Illae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inai Serum Kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inai Serum Kanmaniyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajah Gulaabi Aajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajah Gulaabi Aajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaamu Solluvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaamu Solluvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabayil Kondadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabayil Kondadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aajah Gulaabi Aajah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aajah Gulaabi Aajah"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaamu Seivomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaamu Seivomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabayil Kondadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabayil Kondadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Ya Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Ya Maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maula Yaaa Maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula Yaaa Maula"/>
</div>
</pre>
