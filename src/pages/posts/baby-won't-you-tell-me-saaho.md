---
title: "baby won't you tell me song lyrics"
album: "Saaho"
artist: "Shankar–Ehsaan–Loy"
lyricist: "Krishna Kanth"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Baby Won't You Tell Me"
image: ../../images/albumart/saaho-telugu.jpg
date: 2019-08-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/h2JH0vqDcYc"
type: "love"
singers:
  - Shweta Mohan
  - Siddharth Mahadevan
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalisunte Neetho ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisunte Neetho ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaane Thochindhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaane Thochindhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavanchi Aakashamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavanchi Aakashamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilichundha Naa Kosamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilichundha Naa Kosamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karigindha Aa Dhooramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigindha Aa Dhooramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhilellaa Naa Neramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhilellaa Naa Neramey"/>
</div>
<div class="lyrico-lyrics-wrapper">Namminka Nanne Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namminka Nanne Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirustha Nee Prathi Kalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirustha Nee Prathi Kalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neekantoo Sariponanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekantoo Sariponanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukunna Raavaddhanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukunna Raavaddhanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Atupaine Telisindhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atupaine Telisindhiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenundhey Neelo Anii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenundhey Neelo Anii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidadheese Sandhehamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidadheese Sandhehamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhilesthe Santhoshamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhilesthe Santhoshamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaale Kalipayiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaale Kalipayiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopale Kaalaayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopale Kaalaayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthintha Dhooraale Cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthintha Dhooraale Cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthaalu Veedaali Manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthaalu Veedaali Manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamemito Teliyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamemito Teliyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">adhe Kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhe Kshanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niddharalo Lekunnaa Kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddharalo Lekunnaa Kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedocchi Nee Kallu Cherelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedocchi Nee Kallu Cherelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopinchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopinchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Naati Guruthule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Naati Guruthule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho Lekunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Lekunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvaape Kalalannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvaape Kalalannee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Kannaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Chethi Bomme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chethi Bomme"/>
</div>
<div class="lyrico-lyrics-wrapper">Geethalni Dhaati Praanamochche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geethalni Dhaati Praanamochche"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalla Mundhundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalla Mundhundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">O Tell Me O Tell Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Tell Me O Tell Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Won’t You Tell Me Soo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Won’t You Tell Me Soo"/>
</div>
</pre>
