---
title: "damakku damakku dum song lyrics"
album: "Azhagi"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Thangar Bachchan"
path: "/albums/azhagi-lyrics"
song: "Damakku Damakku Dum"
image: ../../images/albumart/azhagi.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VzB8YxiG8r8"
type: "happy"
singers:
  - Bhavatharini
  - Chorus
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu Chittu Kuruvi Andha Vaanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Chittu Kuruvi Andha Vaanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Rakka Virikka Solli Tharanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Rakka Virikka Solli Tharanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu Chittu Kuruvi Andha Vaanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Chittu Kuruvi Andha Vaanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Rakka Virikka Solli Tharanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Rakka Virikka Solli Tharanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Vittu Virinjirukkum Poovukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Vittu Virinjirukkum Poovukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottum Thena Vandugalukku Kaattanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum Thena Vandugalukku Kaattanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothu Kulungudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothu Kulungudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu Chittu Kuruvi Andha Vaanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Chittu Kuruvi Andha Vaanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Rakka Virikka Solli Tharanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Rakka Virikka Solli Tharanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Vittu Virinjirukkum Poovukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Vittu Virinjirukkum Poovukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottum Thena Vandugalukku Kaattanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum Thena Vandugalukku Kaattanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothu Kulungudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothu Kulungudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kava Kaaran Paarkkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kava Kaaran Paarkkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaachu Kedukkum Verkadalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaachu Kedukkum Verkadalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edupoamaa Parippomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edupoamaa Parippomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jujumpumkuchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jujumpumkuchum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saruga Kootti Thee Mootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saruga Kootti Thee Mootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu Kadalai Adhil Vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Kadalai Adhil Vaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppomaa Korippomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppomaa Korippomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jujumpumkuchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jujumpumkuchum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenanjovai Naayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenanjovai Naayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkettida Oodhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkettida Oodhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthirithoppulla Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthirithoppulla Naadagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namm Raathiri Varaiyila Aadanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Raathiri Varaiyila Aadanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kattanum Adhukku Ettu Mundhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kattanum Adhukku Ettu Mundhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakku Paathu Vaangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakku Paathu Vaangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu Chittu Kuruvi Andha Vaanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Chittu Kuruvi Andha Vaanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Rakka Virikka Solli Tharanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Rakka Virikka Solli Tharanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Vittu Virinjirukkum Poovukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Vittu Virinjirukkum Poovukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottum Thena Vandugalukku Kaattanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum Thena Vandugalukku Kaattanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothu Kulungudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothu Kulungudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhathu Vizhundha Panam Pazhatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhathu Vizhundha Panam Pazhatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Eduthu Pakkuvamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Eduthu Pakkuvamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panguvachu Rusippomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panguvachu Rusippomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jujumpumkuchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jujumpumkuchum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoorukooda Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorukooda Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral Pola Eesalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral Pola Eesalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidippomaa Varuppomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidippomaa Varuppomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jujumpumkuchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jujumpumkuchum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyanaar Koyilil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyanaar Koyilil"/>
</div>
<div class="lyrico-lyrics-wrapper">School-u Mudinja Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School-u Mudinja Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Olinju Adalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Olinju Adalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Kandupidikka Thedallaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kandupidikka Thedallaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Yaaru Kaiyilum Maatikkollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Yaaru Kaiyilum Maatikkollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukku Odidallaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukku Odidallaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu Chittu Kuruvi Andha Vaanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Chittu Kuruvi Andha Vaanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Rakka Virikka Solli Tharanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Rakka Virikka Solli Tharanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Vittu Virinjirukkum Poovukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Vittu Virinjirukkum Poovukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottum Thena Vandugalukku Kaattanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum Thena Vandugalukku Kaattanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Jujumpumkuchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jujumpumkuchum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jujumpumkuchum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jujumpumkuchum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkum Manasellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum Manasellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poothu Kulungudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothu Kulungudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu Chittu Kuruvi Andha Vaanathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Chittu Kuruvi Andha Vaanathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Rakka Virikka Solli Tharanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Rakka Virikka Solli Tharanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottu Vittu Virinjirukkum Poovukkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottu Vittu Virinjirukkum Poovukkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottum Thena Vandugalukku Kaattanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum Thena Vandugalukku Kaattanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
<div class="lyrico-lyrics-wrapper">Damakku Damakku Dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku Damakku Dum"/>
</div>
</pre>
