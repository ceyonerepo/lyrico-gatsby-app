---
title: "marandhaye song lyrics"
album: "teddy"
artist: "D Imman"
lyricist: "madhan karky"
director: "shakti soundar rajan"
path: "/albums/teddy-song-lyrics"
song: "marandhaye"
image: ../../images/albumart/teddy.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/X-22J9tsC6M"
type: "love"
singers:
  - Pradeep Kumar
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Marandhaye Marandhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhaye Marandhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Ennai Yen Marandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Ennai Yen Marandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthae Thaan Nadanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthae Thaan Nadanthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Endru Yaen Kadanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Endru Yaen Kadanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Yaavum Neengi Ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Yaavum Neengi Ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Yaar Marathiya Avathiya Sagathiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Yaar Marathiya Avathiya Sagathiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigalnthavai Ellaam Poiyaai Aanaal Nee Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigalnthavai Ellaam Poiyaai Aanaal Nee Yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jananamaa Salanamaa Maranamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananamaa Salanamaa Maranamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaai Naan Vaazhnthaene Vaanaai Nee Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaai Naan Vaazhnthaene Vaanaai Nee Aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unil Yera Paarthene Kaanaamal Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unil Yera Paarthene Kaanaamal Ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradi Yaaradi Naan Ini Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Yaaradi Naan Ini Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ini Vaala Oar Kaaranam Kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ini Vaala Oar Kaaranam Kooradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi Yaaradi Naan Ini Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Yaaradi Naan Ini Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Thuli Niyaabagam Ooruthaa Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Thuli Niyaabagam Ooruthaa Paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhaiye Marandhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhaiye Marandhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Ennai Yen Marandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Ennai Yen Marandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthe Thaan Nadanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthe Thaan Nadanthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Endru Yaen Kadanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Endru Yaen Kadanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugilumillai Puyalumillai Mazhai Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugilumillai Puyalumillai Mazhai Varumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathile Inam Puriyaa Kalavaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathile Inam Puriyaa Kalavaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaiyumillai Uramumillai Maram Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaiyumillai Uramumillai Maram Varumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugalil Kilai Virithaen Sugam Tharumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugalil Kilai Virithaen Sugam Tharumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai Ariyaa Oruvanai Virumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Ariyaa Oruvanai Virumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Idhayam Thudi Thudithidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Idhayam Thudi Thudithidumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivoru Piravi Arupatta Uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivoru Piravi Arupatta Uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Piraviyai Kadanthume Enai Thodarnthidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piraviyai Kadanthume Enai Thodarnthidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam Unmai illai Un Ver Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Unmai illai Un Ver Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Konden Unmel Un Per Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Konden Unmel Un Per Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvellaam Anuvellaam Ninaivena Nirainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvellaam Anuvellaam Ninaivena Nirainthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhaye Marandhaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhaye Marandhaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Ennai Yen Marandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Ennai Yen Marandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirainthaaye Nirainthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirainthaaye Nirainthaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Ellaam Nee Niraindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Ellaam Nee Niraindhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyum Naanum Meendum Ondraai Aanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyum Naanum Meendum Ondraai Aanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi Surungidum Ulagilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi Surungidum Ulagilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Surangaththai Pola Ennul Poga Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surangaththai Pola Ennul Poga Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugidum Perugidum Ninaivile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugidum Perugidum Ninaivile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kaanaa Ulagththil Yethuvum Mei Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaanaa Ulagththil Yethuvum Mei Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagellaam Poi Indha Kadhal Poi Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagellaam Poi Indha Kadhal Poi Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradi Yaaradi Naan Ini Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Yaaradi Naan Ini Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ini Vaala Oar Kaaranam Kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ini Vaala Oar Kaaranam Kooradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi Yaaradi Naan Ini Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Yaaradi Naan Ini Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Thuli Niyabagam Ooruthaa Paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Thuli Niyabagam Ooruthaa Paaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarada Yaaruda Nee Ennul Yarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarada Yaaruda Nee Ennul Yarada"/>
</div>
<div class="lyrico-lyrics-wrapper">Peralai Polenil Paaigiraai Paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peralai Polenil Paaigiraai Paarada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthaaye Maranthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthaaye Maranthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
</pre>
