---
title: "let's make love song lyrics"
album: "Dirty Hari"
artist: "Mark K. Robin"
lyricist: "Krishna Kanth"
director: "M. S. Raju"
path: "/albums/dirty-hari-lyrics"
song: "Let's Make Love"
image: ../../images/albumart/dirty-hari.jpg
date: 2020-12-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ZL81TcSkrUQ"
type: "love"
singers:
  - Manisha Eerabathini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neelo Nene Kalisenthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Nene Kalisenthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Nene Karigenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Nene Karigenthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pedhavanchuna Naa Thanuvunchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedhavanchuna Naa Thanuvunchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Shruthiminchanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Shruthiminchanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alale Modalaayeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alale Modalaayeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileyyaku Vadileyyaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileyyaku Vadileyyaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaku Thudi Ledhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaku Thudi Ledhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileyyaku Vadileyyaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileyyaku Vadileyyaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo Nene Kalisenthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Nene Kalisenthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Nene Karigenthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Nene Karigenthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kougilintha Kreeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilintha Kreeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaguthundhi Eedilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaguthundhi Eedilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veepu Thaake Nela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veepu Thaake Nela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottu Dhuppataayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu Dhuppataayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchu Nunchi Jwaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchu Nunchi Jwaala "/>
</div>
<div class="lyrico-lyrics-wrapper">Puttukochhele Heyy Yeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttukochhele Heyy Yeyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Alale Modalaayeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alale Modalaayeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileyyaku Vadhileyyaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileyyaku Vadhileyyaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaku Thudhi Ledhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaku Thudhi Ledhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileyyaku Vadhileyyaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileyyaku Vadhileyyaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeno Nanne Dhaachenthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeno Nanne Dhaachenthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Nanne Maarchenthagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Nanne Maarchenthagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Meda Ompuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Meda Ompuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thapunchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thapunchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Thapanchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Thapanchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s Make Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s Make Love"/>
</div>
</pre>
