---
title: "theme of awe song lyrics"
album: "AWE"
artist: "Mark k Robin"
lyricist: "Krishna Kanth"
director: "Prasanth Varma"
path: "/albums/awe-lyrics"
song: "Theme of AWE"
image: ../../images/albumart/awe.jpg
date: 2018-02-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NOFq5qiwUsM"
type: "melody"
singers:
  -	Sharon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vishwame Daaginaa Naalonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwame Daaginaa Naalonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudoo Ontaree Nenenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudoo Ontaree Nenenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Choopule Guchchinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopule Guchchinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Adaganainaa Lenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaganainaa Lenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chethule Vesinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethule Vesinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Aapanainaa Lenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapanainaa Lenaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalame Chesenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Chesenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maanani Gaayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanani Gaayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Yanthrame Choopadaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yanthrame Choopadaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gamyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gamyam "/>
</div>
<div class="lyrico-lyrics-wrapper">Andane Andade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andane Andade "/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Avakaasham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Avakaasham "/>
</div>
<div class="lyrico-lyrics-wrapper">Andithe Cheranaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andithe Cheranaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Aakasham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Aakasham "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andaroo Thappanee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaroo Thappanee "/>
</div>
<div class="lyrico-lyrics-wrapper">Choopinaa Vele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopinaa Vele "/>
</div>
<div class="lyrico-lyrics-wrapper">Oohake Andanee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohake Andanee "/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Naadele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Naadele "/>
</div>
<div class="lyrico-lyrics-wrapper">Enthagaa Egirinaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthagaa Egirinaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakuthondee Nele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakuthondee Nele "/>
</div>
<div class="lyrico-lyrics-wrapper">Matthulo Maravanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthulo Maravanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Munuguthunnaa Thele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munuguthunnaa Thele "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Chinnee Gundelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Chinnee Gundelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Vedanaa Modalayyenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Vedanaa Modalayyenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Annee Aashale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Annee Aashale "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayam Maatunaa Migilenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayam Maatunaa Migilenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasilaa Addamai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasilaa Addamai "/>
</div>
<div class="lyrico-lyrics-wrapper">Mukkalayyenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkalayyenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Okkare Vandalaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkare Vandalaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu Moogenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Moogenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Karaganoo Kalavanoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaganoo Kalavanoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Dveshame Vadalanoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dveshame Vadalanoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamune Viduvanoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamune Viduvanoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Maraname Maruvanoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraname Maruvanoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Shatruvai Dehame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shatruvai Dehame "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasutho Kalabade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasutho Kalabade "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekate Veedene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekate Veedene "/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukuke Selavane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukuke Selavane "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadilene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadilene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netitho Baadhale Theerenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netitho Baadhale Theerenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naadane Lokame Cheraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadane Lokame Cheraana"/>
</div>
</pre>
