---
title: "dagaalty naa song lyrics"
album: "Dagaalty"
artist: "Vijay Narain"
lyricist: "Arivu"
director: "Vijay Anand"
path: "/albums/dagaalty-song-lyrics"
song: "Dagaalty Naa"
image: ../../images/albumart/dagaalty.jpg
date: 2020-01-31
lang: tamil
youtubeLink: 
type: "Intro"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nadandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakenna Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakenna Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Therandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Therandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Rendu Moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Rendu Moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alatikkadha Maama Vayasirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alatikkadha Maama Vayasirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Meratti Konjam Poda Manasirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meratti Konjam Poda Manasirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakku Thina Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakku Thina Thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikka Vidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikka Vidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippukku Thaan Panjamillaa Panjamillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippukku Thaan Panjamillaa Panjamillaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suma Summa Kaathula Poova Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suma Summa Kaathula Poova Suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kama Kamma Kaadula Oora Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kama Kamma Kaadula Oora Suthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suma Summa Kaathula Poova Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suma Summa Kaathula Poova Suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kama Kamma Kumma Kumma Kumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kama Kamma Kumma Kumma Kumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatti Sooda Dhoosa Sooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatti Sooda Dhoosa Sooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottikkuthaa Vaapa Mala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottikkuthaa Vaapa Mala"/>
</div>
<div class="lyrico-lyrics-wrapper">Cooffee Kada Aunty Kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cooffee Kada Aunty Kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathikalam Aandaa Kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathikalam Aandaa Kulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aah Haa Iva Settai Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Haa Iva Settai Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah Haa Semma Item Kaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Haa Semma Item Kaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah Haa Vaala Aatti Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah Haa Vaala Aatti Paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Meow Meow Meow Meow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meow Meow Meow Meow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suma Summa Kaathula Poova Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suma Summa Kaathula Poova Suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kama Kamma Kaadula Oora Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kama Kamma Kaadula Oora Suthura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thanna Thaththa Thaana Naanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thanna Thaththa Thaana Naanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagaalty Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thanna Thanna Thanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thanna Thanna Thanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagaalty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty"/>
</div>
</pre>
