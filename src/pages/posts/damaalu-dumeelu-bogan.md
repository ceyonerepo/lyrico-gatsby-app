---
title: "damaalu dumeelu song lyrics"
album: "Bogan"
artist: "D Imman"
lyricist: "Rokesh"
director: "Lakshman"
path: "/albums/bogan-lyrics"
song: "Damaalu Dumeelu"
image: ../../images/albumart/bogan.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PiHs8jVJFw0"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">This song is a tribute to 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This song is a tribute to "/>
</div>
<div class="lyrico-lyrics-wrapper">all the hero’s and villain’s
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the hero’s and villain’s"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madham konda yaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madham konda yaanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna seiyum theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna seiyum theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinam konda singathidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinam konda singathidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotru oodum oodumm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotru oodum oodumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathal maha devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathal maha devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaye marana devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaye marana devi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruvaati mudivu pannitana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvaati mudivu pannitana"/>
</div>
<div class="lyrico-lyrics-wrapper">En pecha naane ketkamaaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pecha naane ketkamaaten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raangu pannaa raaduthaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raangu pannaa raaduthaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellakkottukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellakkottukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokku scene-u taapp-u thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokku scene-u taapp-u thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum oorukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum oorukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu panna vaaya oadachy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu panna vaaya oadachy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattuvendaa palla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattuvendaa palla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen manasukkulla villainuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen manasukkulla villainuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eevu erakkam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eevu erakkam illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulla poochigalukkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla poochigalukkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idea varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idea varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karchal kutchal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karchal kutchal "/>
</div>
<div class="lyrico-lyrics-wrapper">eratchal koduthutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eratchal koduthutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarandhalum mavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarandhalum mavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Ultimate-u aalu thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ultimate-u aalu thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendi paaru ivana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendi paaru ivana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaandaa keendaa aakki utta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaandaa keendaa aakki utta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevallaama sivana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevallaama sivana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen roobathula paathuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen roobathula paathuduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerula nee yemana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerula nee yemana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therikkavidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikkavidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dai en vazhkaila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai en vazhkaila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru nimizhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru nimizhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen ovvoru nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen ovvoru nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana sedhukkunadhu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana sedhukkunadhu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ashok nee irudhuvaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashok nee irudhuvaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">indha rajiniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha rajiniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbanaa thaan pathirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanaa thaan pathirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Inime indha rajiniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inime indha rajiniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Virodhiyaa paakka poraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virodhiyaa paakka poraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Today unnudaya dairy-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Today unnudaya dairy-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurichu vachiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurichu vachiko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeyikkanumnu porandhavan naanthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyikkanumnu porandhavan naanthaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkinga kavala illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkinga kavala illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuthu nikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuthu nikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">yethiringa yellaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yethiringa yellaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Povaanga mannukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povaanga mannukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haai Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haai Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Safety illaadha thangaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Safety illaadha thangaikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavum naanthaanda annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavum naanthaanda annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonjiya muzhusaa maathiduvean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonjiya muzhusaa maathiduvean"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedaa koodam ethanaa panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedaa koodam ethanaa panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukkum thuninja aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukkum thuninja aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu kelu kelu kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu kelu kelu kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothiduvean naanum paalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothiduvean naanum paalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnumilla mani en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnumilla mani en "/>
</div>
<div class="lyrico-lyrics-wrapper">kannu vaenumnu kaetiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu vaenumnu kaetiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi adicha ondre 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi adicha ondre "/>
</div>
<div class="lyrico-lyrics-wrapper">ton weightu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ton weightu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla nenjukkulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla nenjukkulla "/>
</div>
<div class="lyrico-lyrics-wrapper">vechirukkean dhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechirukkean dhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illa yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illa yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna inga vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna inga vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavaththa gaali pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavaththa gaali pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyanaaru pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyanaaru pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengaeriyaala yenna utta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengaeriyaala yenna utta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yellachamy illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellachamy illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am a hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a hero"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a villain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a villain"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivajium naan thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivajium naan thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">MGRum naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="MGRum naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu eppudi irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu eppudi irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammaalu dammaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammaalu dammaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">dummeelu dummeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dummeelu dummeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummeelu dummeelu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummeelu dummeelu "/>
</div>
<div class="lyrico-lyrics-wrapper">dammaalu dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammaalu dammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peesal pilaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peesal pilaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharppu paiyan salaakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharppu paiyan salaakkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fallttu pannaakaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fallttu pannaakaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thookkiduvean aleakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkiduvean aleakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rapapapa papa rapapa papa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa papa rapapa papa pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapapapa papa rapapa papa pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapapapa papa rapapa papa pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am waiting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am waiting"/>
</div>
</pre>
