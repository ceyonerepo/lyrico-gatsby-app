---
title: "madhavi ponmayilaaga song lyrics"
album: "Theal"
artist: "C. Sathya"
lyricist: "Viveka"
director: "Harikumar"
path: "/albums/theal-lyrics"
song: "Madhavi Ponmayilaaga"
image: ../../images/albumart/theal.jpg
date: 2022-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KRm2ZtWJXd4"
type: "happy"
singers:
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">madhavi ponmayilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavi ponmayilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">thogai virithale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thogai virithale "/>
</div>
<div class="lyrico-lyrics-wrapper">kovalan nanjai mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovalan nanjai mella"/>
</div>
<div class="lyrico-lyrics-wrapper">kothi parithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothi parithale"/>
</div>
<div class="lyrico-lyrics-wrapper">kannagi pola kannal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannagi pola kannal"/>
</div>
<div class="lyrico-lyrics-wrapper">oorai erithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorai erithale"/>
</div>
<div class="lyrico-lyrics-wrapper">pandiyaraga aangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandiyaraga aangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">patharida vaithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patharida vaithale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muthu muthana siripale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu muthana siripale"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthu kuthaga asachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthu kuthaga asachale"/>
</div>
<div class="lyrico-lyrics-wrapper">oru manika paral pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru manika paral pole"/>
</div>
<div class="lyrico-lyrics-wrapper">intha man mele nadanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha man mele nadanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kolai seitha sirpam aanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kolai seitha sirpam aanale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mathavi mathavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathavi mathavi"/>
</div>
<div class="lyrico-lyrics-wrapper">madhavi ponmayilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavi ponmayilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">thogai virithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thogai virithale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru pournamai oru vanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pournamai oru vanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">oru maamalai ondraga sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru maamalai ondraga sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvanatho vadivaithai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvanatho vadivaithai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">man meethu paarthayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man meethu paarthayo"/>
</div>
<div class="lyrico-lyrics-wrapper">oru paarvaiyo oru theendalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paarvaiyo oru theendalo"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sindalo urchagamootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sindalo urchagamootum"/>
</div>
<div class="lyrico-lyrics-wrapper">thee mootiye thee kuchiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee mootiye thee kuchiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thegam maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thegam maaratho"/>
</div>
<div class="lyrico-lyrics-wrapper">tharalamai aeralamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharalamai aeralamai"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru poo parthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru poo parthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">pooripile aadathu vandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooripile aadathu vandu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru manthiram kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru manthiram kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thandhiram kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thandhiram kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">en sonthame meniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sonthame meniye"/>
</div>
<div class="lyrico-lyrics-wrapper">kanbikum varalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanbikum varalaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthu thooralum puthu saralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thooralum puthu saralum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendralum ondra sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendralum ondra sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu naal varai puvi mele nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu naal varai puvi mele nee"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum kandayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum kandayo"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam minnalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam minnalum "/>
</div>
<div class="lyrico-lyrics-wrapper">minjum mogamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minjum mogamum"/>
</div>
<div class="lyrico-lyrics-wrapper">pinjum veyilum ondra kudithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinjum veyilum ondra kudithu"/>
</div>
<div class="lyrico-lyrics-wrapper">nila poosiye kulir thegathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nila poosiye kulir thegathai"/>
</div>
<div class="lyrico-lyrics-wrapper">munnale paarkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnale paarkathe"/>
</div>
<div class="lyrico-lyrics-wrapper">poraliyai kai soondinal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraliyai kai soondinal"/>
</div>
<div class="lyrico-lyrics-wrapper">komali aavan oru komaliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komali aavan oru komaliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">kal aakinal porali aavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal aakinal porali aavan"/>
</div>
<div class="lyrico-lyrics-wrapper">entha vattaramum rasikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha vattaramum rasikum"/>
</div>
<div class="lyrico-lyrics-wrapper">udal mittaiyai pol inikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal mittaiyai pol inikum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kattana manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kattana manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">pattasu than vedikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattasu than vedikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madhavi ponmayilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavi ponmayilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">madhavi ponmayilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavi ponmayilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">madhavi ponmayilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavi ponmayilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">thogai virithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thogai virithale"/>
</div>
<div class="lyrico-lyrics-wrapper">madhavi ponmayilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhavi ponmayilaaga"/>
</div>
</pre>
