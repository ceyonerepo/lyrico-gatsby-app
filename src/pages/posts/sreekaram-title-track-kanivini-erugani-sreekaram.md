---
title: "sreekaram title track song lyrics"
album: "Sreekaram"
artist: "Mickey J. Meyer"
lyricist: "Ramajogayya Sastry"
director: "Kishor B"
path: "/albums/sreekaram-lyrics"
song: "Title Track - Kanivini Erugani"
image: ../../images/albumart/sreekaram.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BkwNzhbT_Xk"
type: "happy"
singers:
  - Prudhvi Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanivini erugani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini erugani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalika modhalaindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalika modhalaindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduguloo aduguga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduguloo aduguga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikina velugula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikina velugula"/>
</div>
<div class="lyrico-lyrics-wrapper">Alikidi edhrayyindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alikidi edhrayyindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Niseedhine jayinchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niseedhine jayinchagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sreekaram kottha sankalpaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sreekaram kottha sankalpaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu chiguristhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu chiguristhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham idhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sreekaram kottha adhyayaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sreekaram kottha adhyayaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuku parimalamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuku parimalamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheevisthunnadhi pudami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheevisthunnadhi pudami"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarasulam manamega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarasulam manamega"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnati monnati paddhathiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnati monnati paddhathiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaradhulam manamega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaradhulam manamega"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati maarpulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati maarpulaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s a change revolutin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s a change revolutin"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s a fire revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s a fire revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">Let us all inspire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let us all inspire"/>
</div>
<div class="lyrico-lyrics-wrapper">Revolution, it’s way revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolution, it’s way revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s say revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s say revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">We can make a better future
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We can make a better future"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mande endaku friend avadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mande endaku friend avadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaku thelusuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaku thelusuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavaate ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavaate ika"/>
</div>
<div class="lyrico-lyrics-wrapper">chematathadi panduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chematathadi panduga"/>
</div>
<div class="lyrico-lyrics-wrapper">A.C. gadhulaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A.C. gadhulaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bye bye cheppam alavokaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bye bye cheppam alavokaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam kadhilindhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam kadhilindhilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuku nacchina dhaariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuku nacchina dhaariga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buradhem kaadhidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buradhem kaadhidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakidhi oka sarada sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakidhi oka sarada sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelamma odilo manakika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelamma odilo manakika"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi dhinamoka paatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi dhinamoka paatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Prakruthi pilupidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prakruthi pilupidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaalluga vesina malupidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalluga vesina malupidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaku thalapaga chudadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaku thalapaga chudadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangaram pandidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangaram pandidham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s a change revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s a change revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s a fire revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s a fire revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">Let us all inspire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let us all inspire"/>
</div>
<div class="lyrico-lyrics-wrapper">Revolution, it’s way revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolution, it’s way revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s say revolution
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s say revolution"/>
</div>
<div class="lyrico-lyrics-wrapper">We can make a better future
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We can make a better future"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acchanga manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acchanga manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Computer kaalam yuvakulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Computer kaalam yuvakulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhade indhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhade indhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadhuvu mana saadhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadhuvu mana saadhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhyam kaanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhyam kaanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhantundhi mana yevvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhantundhi mana yevvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasupadi ye pani chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasupadi ye pani chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Suluvuga raanistham manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suluvuga raanistham manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharamula naatidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharamula naatidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana thathalu chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana thathalu chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Krushi idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krushi idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyanidhem kaane kaadhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyanidhem kaane kaadhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakee vyavasayam ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakee vyavasayam ho ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeans ye thodigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeans ye thodigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana genes lo ee kala unnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana genes lo ee kala unnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Padha padha modhalaudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padha padha modhalaudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nede nava yuva karshakulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nede nava yuva karshakulai"/>
</div>
</pre>
