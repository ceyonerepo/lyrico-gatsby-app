---
title: "wi wi wi wifi song lyrics"
album: "Singam III"
artist: "Harris Jayaraj"
lyricist: "Hari - Harris Jayaraj"
director: "Hari Gopalakrishnan"
path: "/albums/singam-3-lyrics"
song: "Wi Wi Wi Wifi"
image: ../../images/albumart/singam-3.jpg
date: 2017-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6DBfyVREJAg"
type: "Love"
singers:
  - Karthik
  - Nikhita Gandhi
  - Christopher Stanley
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maamaayo . maamaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaayo . maamaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey wi wi wi wifi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey wi wi wi wifi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru li li li like-ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru li li li like-ai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sai sai sai saigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sai sai sai saigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal kaattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You better
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You better"/>
</div>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey wi wi wi wifi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey wi wi wi wifi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru li li li like-ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru li li li like-ai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sai sai sai saigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sai sai sai saigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal kaattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girl in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You crazy girl in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You crazy girl in love"/>
</div>
<div class="lyrico-lyrics-wrapper">Girl in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girl in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You crazy girl in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You crazy girl in love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sokiya sokiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokiya sokiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum kadhal jockeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum kadhal jockeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Talkie-ya talkie-ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talkie-ya talkie-ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum romba crazya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum romba crazya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokia mokia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokia mokia"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan heart mafiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan heart mafiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Variya nee vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Variya nee vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey wi wi wi wifi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey wi wi wi wifi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru li li li like-ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru li li li like-ai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sai sai sai saigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sai sai sai saigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal kaattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You better
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You better"/>
</div>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudaya bike-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaya bike-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Yera poren back-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yera poren back-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla mayakkam varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla mayakkam varumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalayila walking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalayila walking"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyila chatting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyila chatting"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dating sugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dating sugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae ennai holidayku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae ennai holidayku"/>
</div>
<div class="lyrico-lyrics-wrapper">Phuket Parris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phuket Parris"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootti poriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootti poriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boatu mela nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boatu mela nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachi mutham thariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachi mutham thariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilandhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilandhiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhandhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey wi wi wi wifi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey wi wi wi wifi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru li li li like-ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru li li li like-ai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sai sai sai saigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sai sai sai saigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal kaattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You better
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You better"/>
</div>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkachakka rifle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachakka rifle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai suthhi life-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai suthhi life-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki mukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki mukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal varumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannivedi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannivedi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil eduppenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil eduppenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodavae bayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodavae bayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kobam meedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kobam meedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam meedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam meedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam pottaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam pottaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thokkam motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkam motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam pottayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam pottayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarndhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarndhiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan edhirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan edhirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaindhiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaindhiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey wi wi wi wifi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey wi wi wi wifi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru li li li like-ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru li li li like-ai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli podura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sai sai sai saigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sai sai sai saigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal kaattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">You better
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You better"/>
</div>
<div class="lyrico-lyrics-wrapper">Fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fall in love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae kilae ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae kilae ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilae kilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilae kilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaayo ((3) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaayo ((3) times)"/>
</div>
</pre>
