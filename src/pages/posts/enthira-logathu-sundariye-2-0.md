---
title: 'enthira logathu sundariye song lyrics'
album: '2.0'
artist: 'A R Rahman'
lyricist: 'Madhan Karky'
director: 'Shankar'
path: '/albums/2.0-song-lyrics'
song: 'Enthira Logathu Sundariye'
image: ../../images/albumart/2-0.jpg
date: 2017-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1r3tYnIiwE4"
type: 'Love'
singers: 
- Shashaa Tirupati
- Sid Sriram
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nee piriyaadhae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nee piriyaadhae.."/>
</div>
<div class="lyrico-lyrics-wrapper">Dhae..dhae..dhae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhae..dhae..dhae…"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyum kuraiyaadhae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyum kuraiyaadhae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhira logathu sundariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira logathu sundariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalil kaadhalai sinthuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalil kaadhalai sinthuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engine ai alli konjuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engine ai alli konjuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey minsaara samsaaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey minsaara samsaaramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratham illa kannam rendil..hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham illa kannam rendil..hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham vaikkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham vaikkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Putham puthu daaba roja…hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham puthu daaba roja…hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookka seiyattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookka seiyattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutham seidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutham seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Data mattum…hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Data mattum…hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootti vidattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootti vidattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey un bus in ..hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un bus in ..hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Conductor naan !!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Conductor naan !!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nee piriyaadhae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nee piriyaadhae.."/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyum kuraiyaadhae…. ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyum kuraiyaadhae…. ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhira logathu sundariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira logathu sundariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalil kaadhalai sinthuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalil kaadhalai sinthuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engine ai alli konjuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engine ai alli konjuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey minsaara samsaaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey minsaara samsaaramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En sensor-uku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sensor-uku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvum unavum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvum unavum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En cable vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En cable vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravum kanavum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravum kanavum nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En visaikkoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En visaikkoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai yettum mayakkam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai yettum mayakkam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn neuron ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn neuron ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirayum nilavum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirayum nilavum nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pogum padhivae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pogum padhivae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadavu sollae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadavu sollae"/>
</div>
<div class="lyrico-lyrics-wrapper">En panimadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En panimadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanini rajini nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanini rajini nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum nilavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum nilavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandum pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandum pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrae urugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraai aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraai aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaaaaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaaaaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaa en avaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaa en avaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Mega omega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mega omega"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love you from
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you from"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero to infinity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero to infinity"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nee piriyaadhae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nee piriyaadhae.."/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyum kuraiyaadhae…. ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyum kuraiyaadhae…. ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhira logathu sundariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhira logathu sundariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalil kaadhalai sinthuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalil kaadhalai sinthuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engine ai alli konjuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engine ai alli konjuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey minsaara samsaaramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey minsaara samsaaramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratham illa kannam rendil..hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham illa kannam rendil..hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham vaikkattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham vaikkattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Putham puthu daaba roja…hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham puthu daaba roja…hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookka seiyattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookka seiyattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutham seidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutham seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Data mattum…hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Data mattum…hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootti vidattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootti vidattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey un bus in ..hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un bus in ..hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Conductor naan !!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Conductor naan !!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nee piriyaadhae..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nee piriyaadhae.."/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirin uyirae batteriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirin uyirae batteriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyum kuraiyaadhae…. ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyum kuraiyaadhae…. ((2) times)"/>
</div>
</pre>