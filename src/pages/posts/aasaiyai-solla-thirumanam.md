---
title: "aasayai solla song lyrics"
album: "Thirumanam"
artist: "Siddharth Vipin"
lyricist: "Yugabharathi"
director: "Cheran"
path: "/albums/thirumanam-lyrics"
song: "Aasayai Solla"
image: ../../images/albumart/thirumanam.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eIsjcKyo9i0"
type: "happy"
singers:
  - Jagadeesh
  - Priyanka
  - Dr. Narayanan
  - Sukanya
  - M. S. Bhaskar
  - Thambi Ramaiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aasaiyai solla ninaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaiyai solla ninaikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthaigal indri thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthaigal indri thavikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalin baasai puriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalin baasai puriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">ka ka tha tha il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ka ka tha tha il"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum naanaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum naanaga "/>
</div>
<div class="lyrico-lyrics-wrapper">engu sendralum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engu sendralum "/>
</div>
<div class="lyrico-lyrics-wrapper">avan thanaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan thanaga "/>
</div>
<div class="lyrico-lyrics-wrapper">senrthane ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senrthane ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">aana maatrangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aana maatrangal "/>
</div>
<div class="lyrico-lyrics-wrapper">aaga sariyanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaga sariyanal"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nannalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nannalai "/>
</div>
<div class="lyrico-lyrics-wrapper">paarthale santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthale santhosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayame en idhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayame en idhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">naal muluthume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal muluthume "/>
</div>
<div class="lyrico-lyrics-wrapper">aval ninaivile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval ninaivile"/>
</div>
<div class="lyrico-lyrics-wrapper">aathirai enai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathirai enai "/>
</div>
<div class="lyrico-lyrics-wrapper">kollai kondale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollai kondale"/>
</div>
<div class="lyrico-lyrics-wrapper">senthamilile en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthamilile en "/>
</div>
<div class="lyrico-lyrics-wrapper">sinthai koithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthai koithale"/>
</div>
<div class="lyrico-lyrics-wrapper">en thunaiyai nam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thunaiyai nam "/>
</div>
<div class="lyrico-lyrics-wrapper">veetil vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetil vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">oru naal parthu solvaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru naal parthu solvaya"/>
</div>
<div class="lyrico-lyrics-wrapper">oh my sweet sister
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh my sweet sister"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakoru pen paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakoru pen paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaithidum munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaithidum munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumana pechai ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumana pechai ne"/>
</div>
<div class="lyrico-lyrics-wrapper">edupathu yen thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edupathu yen thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">avasara pattale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avasara pattale"/>
</div>
<div class="lyrico-lyrics-wrapper">araikurai aagatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="araikurai aagatho"/>
</div>
<div class="lyrico-lyrics-wrapper">porumaiyum illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porumaiyum illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ponane en thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponane en thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyana yogam vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyana yogam vanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">kekame serum ellavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekame serum ellavum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvangi vaala valthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvangi vaala valthu"/>
</div>
<div class="lyrico-lyrics-wrapper">solvene vaayara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solvene vaayara "/>
</div>
<div class="lyrico-lyrics-wrapper">un kadhal thavare illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kadhal thavare illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">love ellam sariya thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love ellam sariya thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">ava thola urichi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava thola urichi "/>
</div>
<div class="lyrico-lyrics-wrapper">podanum road la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podanum road la"/>
</div>
<div class="lyrico-lyrics-wrapper">vilangatha love ah othuki ne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilangatha love ah othuki ne "/>
</div>
<div class="lyrico-lyrics-wrapper">irukanum pappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukanum pappa"/>
</div>
<div class="lyrico-lyrics-wrapper">love marriage ellame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love marriage ellame "/>
</div>
<div class="lyrico-lyrics-wrapper">last la diverse paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="last la diverse paapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">super
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="super"/>
</div>
<div class="lyrico-lyrics-wrapper">porunthuthu pen jathagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porunthuthu pen jathagam"/>
</div>
<div class="lyrico-lyrics-wrapper">sukira thisaiyoda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sukira thisaiyoda "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthale magarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthale magarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">najuvula sikkal illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="najuvula sikkal illa"/>
</div>
<div class="lyrico-lyrics-wrapper">rasiyilum thappu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasiyilum thappu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">vasiyathil pilai illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasiyathil pilai illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">sugathilum kurai illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugathilum kurai illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en thambi paartha pennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thambi paartha pennum"/>
</div>
<div class="lyrico-lyrics-wrapper">en tholiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en tholiye"/>
</div>
<div class="lyrico-lyrics-wrapper">sonthangal kooda paarpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthangal kooda paarpen"/>
</div>
<div class="lyrico-lyrics-wrapper">nal naalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nal naalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayai naanum maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayai naanum maari"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayara veetuku va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayara veetuku va"/>
</div>
<div class="lyrico-lyrics-wrapper">endru solvene kondadiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endru solvene kondadiye"/>
</div>
<div class="lyrico-lyrics-wrapper">aanantha kanner
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanantha kanner"/>
</div>
<div class="lyrico-lyrics-wrapper">sinthum annalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthum annalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanamal ponathena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanamal ponathena"/>
</div>
<div class="lyrico-lyrics-wrapper">en thanthaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thanthaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">aani verai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aani verai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">noorandu kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noorandu kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">ondra thol serum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondra thol serum "/>
</div>
<div class="lyrico-lyrics-wrapper">poo maalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo maalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">shutup
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shutup"/>
</div>
<div class="lyrico-lyrics-wrapper">maname un kai sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maname un kai sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">ellamume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellamume"/>
</div>
<div class="lyrico-lyrics-wrapper">kudumbam thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudumbam thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">varamindri verillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varamindri verillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kari illamal nathi nadanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kari illamal nathi nadanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal serathu ena arivene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal serathu ena arivene"/>
</div>
<div class="lyrico-lyrics-wrapper">alai paayamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai paayamal "/>
</div>
<div class="lyrico-lyrics-wrapper">thisai maaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisai maaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">nadapen naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadapen naane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaai vaasam athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai vaasam athu"/>
</div>
<div class="lyrico-lyrics-wrapper">un anbile veesatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un anbile veesatho"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu en thangaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu en thangaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai ethuvanalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai ethuvanalume"/>
</div>
<div class="lyrico-lyrics-wrapper">erpen erpen naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erpen erpen naane"/>
</div>
<div class="lyrico-lyrics-wrapper">ne kadhal karam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne kadhal karam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee serave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee serave"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapene unai kanorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapene unai kanorame"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum varai un annane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum varai un annane"/>
</div>
<div class="lyrico-lyrics-wrapper">tholan pole thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholan pole thane"/>
</div>
</pre>
