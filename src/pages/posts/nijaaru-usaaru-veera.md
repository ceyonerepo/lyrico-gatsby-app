---
title: "nijaaru usaaru song lyrics"
album: "Veera"
artist: "Leon James"
lyricist: "Na. Muthu Kumar"
director: "Rajaraman"
path: "/albums/veera-lyrics"
song: "Nijaaru Usaaru"
image: ../../images/albumart/veera.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jDv_irIUD-I"
type: "happy"
singers:
  - Lawrence
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eh kootathaan pottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh kootathaan pottalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rottathaan nee podu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rottathaan nee podu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Share auto gap-u kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Share auto gap-u kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocket ah nee seeruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket ah nee seeruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evana irundha unakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana irundha unakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirnthae nada da 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirnthae nada da "/>
</div>
<div class="lyrico-lyrics-wrapper">velakkenna ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velakkenna ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meesa konjam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meesa konjam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Murikki vittuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murikki vittuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Un satta collara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un satta collara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethi vittuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi vittuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchatha dealu la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchatha dealu la"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittukoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittukoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh nijaaru ushaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh nijaaru ushaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illana bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada illana bejaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi nijaaru ushaaru eh hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi nijaaru ushaaru eh hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illana bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada illana bejaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh kootathaan pottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh kootathaan pottalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rottathaan nee podu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rottathaan nee podu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh share auto gap-u kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh share auto gap-u kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocket ah nee seeruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket ah nee seeruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga right-u ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga right-u ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada rightum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada rightum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh wrongu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh wrongu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada wrongum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada wrongum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vera paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu vera paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Route ah maathikoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route ah maathikoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kanna mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanna mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nambidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nambidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kadha kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kadha kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nambidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nambidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenju sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenju sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai kettukoda aah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai kettukoda aah haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polladhindha ulagandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polladhindha ulagandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththukanum kalagamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththukanum kalagamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavathil naa solluranda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavathil naa solluranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaah aaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah aaaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh nijaaru ushaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh nijaaru ushaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illana bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada illana bejaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi nijaaru ushaaru eh hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi nijaaru ushaaru eh hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illana bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada illana bejaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh kootathaan pottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh kootathaan pottalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rottathaan nee podu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rottathaan nee podu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh share auto gap-u kulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh share auto gap-u kulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocket ah nee seeruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket ah nee seeruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evana irundha unakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evana irundha unakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirnthae nada da velakkenna ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirnthae nada da velakkenna ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meesa konjam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meesa konjam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Murikki vittuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murikki vittuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Un satta collara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un satta collara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethi vittuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi vittuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchatha dealu la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchatha dealu la"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittukoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittukoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh nijaaru ushaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh nijaaru ushaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illana bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada illana bejaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi nijaaru ushaaru eh hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi nijaaru ushaaru eh hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada illana bejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada illana bejaaru"/>
</div>
</pre>
