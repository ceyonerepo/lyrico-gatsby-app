---
title: "ippodhu yen indha song lyrics"
album: "Bhaskar Oru Rascal"
artist: "Amresh Ganesh"
lyricist: "Madhan Karky"
director: "Siddique"
path: "/albums/bhaskar-oru-rascal-lyrics"
song: "Ippodhu Yen Indha"
image: ../../images/albumart/bhaskar-oru-rascal.jpg
date: 2018-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3-rKXhebG4E"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Yah yeah yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yah yeah yeah yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Yah yeah yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yah yeah yeah yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippodhu yen indha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu yen indha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil paikiratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil paikiratho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu yen indha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu yen indha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil paikiratho ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil paikiratho ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malargalai maranthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargalai maranthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiyai izhanthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyai izhanthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaigal pookiratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaigal pookiratho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaa nijama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa nijama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melemo melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melemo melemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mele mele melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele mele melemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaa nijama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa nijama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melemo melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melemo melemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mele mele melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele mele melemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Yah yeah yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yah yeah yeah yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaigal katti thantha paalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaigal katti thantha paalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkena neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkena neelumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavin siragenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavin siragenavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada kaigal aarum korthu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kaigal aarum korthu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal ettum parthu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal ettum parthu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil pookum pudhu kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil pookum pudhu kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuva idhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuva idhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum indha mayakkamgal yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum indha mayakkamgal yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu yeno adhu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu yeno adhu yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippodhu yen indha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu yen indha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil paikirathoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil paikirathoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalaai karaindhu uruginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalaai karaindhu uruginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enai nerungida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enai nerungida"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvayil idhayam paruginen anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvayil idhayam paruginen anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo kaanamal pona hormones ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo kaanamal pona hormones ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum ennul paaya seithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum ennul paaya seithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham vaithu motham keduthai nyayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham vaithu motham keduthai nyayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul ennai kandupidithaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul ennai kandupidithaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu yeno adhu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu yeno adhu yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippodhu yen indha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu yen indha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil paikirathooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil paikirathooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu yen indha kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu yen indha kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil paikirathooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil paikirathooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Malargalai maranthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malargalai maranthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamaiyai izhanthumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamaiyai izhanthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithaigal pookirathoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaigal pookirathoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaa nijama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa nijama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melemo melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melemo melemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mele mele melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele mele melemo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaa nijama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa nijama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melemo melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melemo melemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mele mele melemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele mele melemo"/>
</div>
</pre>
