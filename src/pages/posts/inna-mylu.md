---
title: "inna mylu"
album: "Ekaa Entertainment"
artist: "Britto Michael"
lyricist: "Nishanth"
director: "S.Lingesan - G.Ranjith - Sri Manoj Kumar - Raghul Manogaran"
path: "/albums/inna-mylu-song-lyrics"
song: "Inna Mylu"
image: ../../images/albumart/inna-mylu.jpg
date: 2021-04-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tdvwZCbYHh0"
type: "album"
singers:
  - Siva Karthikeyan
  - Poovaiyar
  - Kamala Kannan
  - Rajesh 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thayavu seithu kadhavai mudavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayavu seithu kadhavai mudavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athavadhu english-la sollanum ..na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athavadhu english-la sollanum ..na"/>
</div>
<div class="lyrico-lyrics-wrapper">Please close the door…door…door…door
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please close the door…door…door…door"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mylu ….yaara paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu ….yaara paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-a podu …off aavadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-a podu …off aavadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu orasikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu orasikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru kaila vechikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru kaila vechikina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bangama matniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangama matniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu orasikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu orasikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu olinjikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu olinjikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mylu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ….hey…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ….hey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu orasikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu orasikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu olinjikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu olinjikina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu orasikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu orasikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu olinjikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu olinjikina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Please close the door!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please close the door!!!"/>
</div>
<div class="lyrico-lyrics-wrapper">Please close the door!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please close the door!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei moodra dei!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei moodra dei!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum avanum friendumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum avanum friendumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Road-u- la paathakka time illenba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Road-u- la paathakka time illenba"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss you poduva status-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss you poduva status-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta off late idhu thaan trend-umba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta off late idhu thaan trend-umba"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhi vazhka phonukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhi vazhka phonukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi vazhkai loanukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi vazhkai loanukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podra scene over-ah illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podra scene over-ah illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vangi pottathellam emi-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vangi pottathellam emi-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae.. emi-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae.. emi-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayilae …emi-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae …emi-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mayilae… emi-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae… emi-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Car emi… phone emi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car emi… phone emi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu emi …
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu emi …"/>
</div>
<div class="lyrico-lyrics-wrapper">Jetti kooda endhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jetti kooda endhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana onnuda velaiyuna gunnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana onnuda velaiyuna gunnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi nooru ponnuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi nooru ponnuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman mela kannuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman mela kannuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram friends online-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram friends online-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhavinu koopta oruthanum-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhavinu koopta oruthanum-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottunu varta naan pullingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottunu varta naan pullingala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu kaatlama friendship-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu kaatlama friendship-la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyy…..eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy…..eyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa vaa sonu perla vidu!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa vaa sonu perla vidu!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gili gili gili gujili gujili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili gujili gujili "/>
</div>
<div class="lyrico-lyrics-wrapper">gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili gujili gujili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili gujili gujili "/>
</div>
<div class="lyrico-lyrics-wrapper">gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili gujili gujili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili gujili gujili "/>
</div>
<div class="lyrico-lyrics-wrapper">gili gili gili gujili gujuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gili gili gili gujili gujuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Durrrrrrrrrrrrrrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durrrrrrrrrrrrrrrr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adinga vaaya moodra en vaanaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adinga vaaya moodra en vaanaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee romba gunjaka goonuzhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee romba gunjaka goonuzhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayama pesanumba yaaranalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayama pesanumba yaaranalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei neeyellam nattama -na daara keenjurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei neeyellam nattama -na daara keenjurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyellam nattama -na daara keenjurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyellam nattama -na daara keenjurum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyellam nattama -na daara keenjurum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyellam nattama -na daara keenjurum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei neram ketta nerathula like-a pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei neram ketta nerathula like-a pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee number mattum vangu machi noolavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee number mattum vangu machi noolavittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava nambitana paathikatti thalikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava nambitana paathikatti thalikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye apula yeri ukkandhukko aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye apula yeri ukkandhukko aasaipattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei pakkathootu panjayathu orangkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei pakkathootu panjayathu orangkattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ottukekka nikkathada vetkamkettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ottukekka nikkathada vetkamkettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambadha panjagatha kattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambadha panjagatha kattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo maattikitta sethula thaan kaalavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo maattikitta sethula thaan kaalavittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maman machan rendu perum pechuna puyalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman machan rendu perum pechuna puyalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya vechu vadai suttu ootuvanuva rail-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya vechu vadai suttu ootuvanuva rail-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhumnnu pechedutha parakkudhu analu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhumnnu pechedutha parakkudhu analu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungokka petha pulla rendum tamil-ah fail-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungokka petha pulla rendum tamil-ah fail-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mylu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamil-la fail-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil-la fail-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna mylu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mylu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada tamil-liyae fail-an pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada tamil-liyae fail-an pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan pakkam orasikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan pakkam orasikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu olinjikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu olinjikina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae sirichikuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae sirichikuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan pakkam orasikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan pakkam orasikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara paathu morchikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara paathu morchikina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vandhu olinjikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vandhu olinjikina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inna mayilae…vera level baa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inna mayilae…vera level baa…"/>
</div>
</pre>
