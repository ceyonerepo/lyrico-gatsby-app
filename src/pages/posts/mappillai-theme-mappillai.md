---
title: "mappillai theme song lyrics"
album: "Mappillai"
artist: "Mani Sharma"
lyricist: "Snehan"
director: "Suraj"
path: "/albums/mappillai-lyrics"
song: "Mappillai Theme"
image: ../../images/albumart/mappillai.jpg
date: 2011-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kTntRLLpBlk"
type: "theme"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho Ho come on Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho come on Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mappillai Mappillai Mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mappillai Mappillai Mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarukkum Anjaadha Aanpillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarukkum Anjaadha Aanpillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mappillai Mappillai Mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mappillai Mappillai Mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanaminja MUdiyadhu Vombula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanaminja MUdiyadhu Vombula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mappillai Mappillai Mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mappillai Mappillai Mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yarukkum Anjaadha Aanpillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarukkum Anjaadha Aanpillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mappillai Mappillai Mappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mappillai Mappillai Mappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanaminja MUdiyadhu Vombula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanaminja MUdiyadhu Vombula"/>
</div>
</pre>
