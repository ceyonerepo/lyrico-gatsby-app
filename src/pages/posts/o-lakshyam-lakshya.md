---
title: "o lakshyam song lyrics"
album: "Lakshya"
artist: "Kaala Bhairava"
lyricist: "Rehman"
director: "Dheerendra Santhossh"
path: "/albums/lakshya-lyrics"
song: "O Lakshyam"
image: ../../images/albumart/lakshya.jpg
date: 2021-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/2bXSfWrAJac"
type: "happy"
singers:
  - Hymath Mohammed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arachethillo Daachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arachethillo Daachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliginche Deepam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliginche Deepam Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapalle Kaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapalle Kaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinche Lokam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinche Lokam Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annee Thaanayyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annee Thaanayyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andisthoo Aa Cheyii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andisthoo Aa Cheyii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalane Geliche Sankalpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalane Geliche Sankalpam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve Lekunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Lekunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenantoo Lenantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenantoo Lenantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Odigii Edigey O Lakshyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odigii Edigey O Lakshyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arachethillo Daachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arachethillo Daachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliginche Deepam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliginche Deepam Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapalle Kaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapalle Kaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipinche Lokam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipinche Lokam Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annee Thaanayyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annee Thaanayyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andisthoo Aa Cheyii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andisthoo Aa Cheyii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalane Geliche Sankalpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalane Geliche Sankalpam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve Lekunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Lekunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenantoo Lenantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenantoo Lenantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Odigii Edigey O Lakshyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odigii Edigey O Lakshyam"/>
</div>
</pre>
