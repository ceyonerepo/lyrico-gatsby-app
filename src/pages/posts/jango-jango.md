---
title: "jango song lyrics"
album: "Jango"
artist: "Ghibran"
lyricist: "Muthamizh"
director: "Mano Karthikeyan"
path: "/albums/jango-lyrics"
song: "Jango"
image: ../../images/albumart/jango.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z5-_GMZYOt0"
type: "mass"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One day-il god-ah maari bhramman aagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One day-il god-ah maari bhramman aagitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunday-kul monday pola time-ah maathitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday-kul monday pola time-ah maathitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjagam regai ellam maathi ezhuthitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjagam regai ellam maathi ezhuthitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjatha singamaaga varamum vangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjatha singamaaga varamum vangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikkadha pulli mela kolam poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkadha pulli mela kolam poduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkadha bhoomiya thaan niruthi kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkadha bhoomiya thaan niruthi kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya thaan neettuna kaalamum maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thaan neettuna kaalamum maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam thaan katti naan aalaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam thaan katti naan aalaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladho kettadho indru naal ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladho kettadho indru naal ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththama en vazhi vaazhaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththama en vazhi vaazhaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">JangoJango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="JangoJango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja oru paarvai paatha pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja oru paarvai paatha pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagu kedhellaam thaana odumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagu kedhellaam thaana odumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhagura oru marunodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhagura oru marunodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhidum ini ivan madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhidum ini ivan madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaikadalinil purandu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaikadalinil purandu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam poda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam poda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhigalum ini vizhaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhigalum ini vizhaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhiyinil adi paniyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiyinil adi paniyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivugal ini athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivugal ini athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatram theda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram theda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa aayiram..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa aayiram.."/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam modhidum…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam modhidum…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One day-il god-ah maari bhramman aagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One day-il god-ah maari bhramman aagitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunday-kul monday pola time-ah maathitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday-kul monday pola time-ah maathitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Panjagam regai ellam maathi ezhuthitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjagam regai ellam maathi ezhuthitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjatha singamaaga varamum vangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjatha singamaaga varamum vangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikkadha pulli mela kolam poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkadha pulli mela kolam poduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkadha bhoomiya thaan niruthi kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkadha bhoomiya thaan niruthi kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya thaan neettuna kaalamum maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thaan neettuna kaalamum maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam thaan katti naan aalaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam thaan katti naan aalaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladho kettadho indru naal ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladho kettadho indru naal ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththama en vazhi vaazhaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththama en vazhi vaazhaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
<div class="lyrico-lyrics-wrapper">Jango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jango"/>
</div>
</pre>
