---
title: "vaada thambi song lyrics"
album: "Etharkkum Thunindhavan"
artist: "D. Imman"
lyricist: "Vignesh Shivan"
director: "Pandiraj"
path: "/albums/etharkkum-thunindhavan-lyrics"
song: "Vaada Thambi"
image: ../../images/albumart/etharkkum-thunindhavan.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/B52cNsAIxxU"
type: "mass"
singers:
  - Anirudh Ravichander
  - G.V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Era era mela era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era era mela era"/>
</div>
<div class="lyrico-lyrics-wrapper">Illamai ellame theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illamai ellame theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada thambi mela vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada thambi mela vaada thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maara maara ellam maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara maara ellam maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbale namellam sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbale namellam sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada thambi mela vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada thambi mela vaada thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedikkatha perum parai eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkatha perum parai eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiya marathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiya marathadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyatha viragale eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyatha viragale eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiya pokkathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiya pokkathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkoru padhai avasiyam theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkoru padhai avasiyam theva"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppatha kodutha athu oru potha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppatha kodutha athu oru potha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada thambi mela vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada thambi mela vaada thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada thambi eri vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada thambi eri vaada thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Era era mela era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era era mela era"/>
</div>
<div class="lyrico-lyrics-wrapper">Illamai ellame theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illamai ellame theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Poruthu poruthu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruthu poruthu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Porumaikku irayagi pogatha vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porumaikku irayagi pogatha vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Poruthi pottathum poriyagi veriyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruthi pottathum poriyagi veriyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Venumda thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venumda thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichi udaikka adakki azhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi udaikka adakki azhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedukka nenachaa nee seevanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedukka nenachaa nee seevanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu kodukka ethayum mudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu kodukka ethayum mudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthusa padaikka nee vazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthusa padaikka nee vazhanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eetti pola payuravan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eetti pola payuravan kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottilam ini venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottilam ini venumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theetti vacha koottam undu kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theetti vacha koottam undu kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Etharkum thuninjavan thanamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etharkum thuninjavan thanamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkoru patha avasiyam theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkoru patha avasiyam theva"/>
</div>
<div class="lyrico-lyrics-wrapper">Irupatha kodutha athu oru potha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irupatha kodutha athu oru potha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada thambi mela vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada thambi mela vaada thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada thambi eri vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada thambi eri vaada thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Era era mela era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Era era mela era"/>
</div>
<div class="lyrico-lyrics-wrapper">Illamai ellame theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illamai ellame theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkoru patha avasiyam theva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkoru patha avasiyam theva"/>
</div>
<div class="lyrico-lyrics-wrapper">Irupatha kodutha athu oru potha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irupatha kodutha athu oru potha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada thambi mela vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada thambi mela vaada thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada thambi eri vaada thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada thambi eri vaada thambi"/>
</div>
</pre>
