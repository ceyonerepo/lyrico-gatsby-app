---
title: "engea engea song lyrics"
album: "Sadhurangam"
artist: "Vidyasagar"
lyricist: "Yugabharathi"
director: "Karu Pazhaniappan"
path: "/albums/sadhurangam-lyrics"
song: "Engea Engea"
image: ../../images/albumart/sadhurangam.jpg
date: 2011-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/61KqukiYK4I"
type: "sad"
singers:
  - Karthik
  - Timmy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">engey engey engey en vennilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey engey engey en vennilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingey ingey ingey aen thontharavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingey ingey ingey aen thontharavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engey engey engey en vennilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey engey engey en vennilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingey ingey ingey aen thontharavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingey ingey ingey aen thontharavu"/>
</div>
<div class="lyrico-lyrics-wrapper">veesum thendral undu ennai theendavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum thendral undu ennai theendavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavillum undu aeno vannam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavillum undu aeno vannam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennam ingu undu solla vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennam ingu undu solla vaarthai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aen intha thunbam unnai kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aen intha thunbam unnai kaanavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engey engey engey en vennilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey engey engey en vennilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingey ingey ingey aen thontharavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingey ingey ingey aen thontharavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeyaa inbam neeyae inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyaa inbam neeyae inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyillaamal ethuvum thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyillaamal ethuvum thunbam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathal nummai saetham seythaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathal nummai saetham seythaal"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarai noavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarai noavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyaa unmai neeyae unmai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyaa unmai neeyae unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyillaatha ulagam bhommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyillaatha ulagam bhommai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam nummai noagha seythaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam nummai noagha seythaal"/>
</div>
<div class="lyrico-lyrics-wrapper">poadhum vaazhvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poadhum vaazhvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">uthiraatha njaabagham oru koodi nee thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthiraatha njaabagham oru koodi nee thara"/>
</div>
<div class="lyrico-lyrics-wrapper">varavaana vaedhanai selavaaghum nee vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varavaana vaedhanai selavaaghum nee vara"/>
</div>
<div class="lyrico-lyrics-wrapper">naan mella ninaithaen solla azhaithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan mella ninaithaen solla azhaithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">engu tholaithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engu tholaithaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engey engey engey en vennilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey engey engey en vennilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ingey ingey ingey aen thontharavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingey ingey ingey aen thontharavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalai thanthaay maalai thanthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai thanthaay maalai thanthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathal paesum pozhthum thanthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathal paesum pozhthum thanthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam thanthaay neelam thanthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam thanthaay neelam thanthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thooral thanthaay thookam thanthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooral thanthaay thookam thanthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">thoornthidaatha aekam thanthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoornthidaatha aekam thanthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam thanthaay vaazhvum thanthaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam thanthaay vaazhvum thanthaay"/>
</div>
<div class="lyrico-lyrics-wrapper">swaasam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swaasam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagaana poomugham agalaathu kaathali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagaana poomugham agalaathu kaathali"/>
</div>
<div class="lyrico-lyrics-wrapper">anaiyaathu kaarthikai thunaiyaaghum maarkazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaiyaathu kaarthikai thunaiyaaghum maarkazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan mella ninaithaen solla azhainthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan mella ninaithaen solla azhainthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">engu tholaithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engu tholaithaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aen intha thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aen intha thunbam"/>
</div>
</pre>
