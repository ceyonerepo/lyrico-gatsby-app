---
title: "chaladhe ee pulakintha song lyrics"
album: "Degree College"
artist: "Sunil Kashyap"
lyricist: "Vanamali"
director: "Narasimha Nandi"
path: "/albums/degree-college-lyrics"
song: "Chaladhe Ee Pulakintha"
image: ../../images/albumart/degree-college.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/CrlMsxMfOHA"
type: "love"
singers:
  - Sameera Bharadwaj
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chaaladhe Ee Pulaknitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaaladhe Ee Pulaknitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeluko Naa Thanuvantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeluko Naa Thanuvantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Netho Vunnaagaa Ninnullukoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netho Vunnaagaa Ninnullukoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Motham Neekey Andhsthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Motham Neekey Andhsthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanuvu Mudipadagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvu Mudipadagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vegam Panchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vegam Panchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dhaaham Theerchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dhaaham Theerchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenunna Lokame Marichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenunna Lokame Marichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa swargam Thalupule Therichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa swargam Thalupule Therichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janta Aatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janta Aatalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusale Sukhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusale Sukhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chaaladhe Ee Pulaknitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaaladhe Ee Pulaknitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeluko Naa Thanuvantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeluko Naa Thanuvantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanuvantha Panchainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvantha Panchainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaaganidhee Yathanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaaganidhee Yathanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Em Chesthe Theeruthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Chesthe Theeruthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathisaari Pondhinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathisaari Pondhinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariponi Haayilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariponi Haayilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhileni Thapanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhileni Thapanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenunna Lokame Marichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenunna Lokame Marichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa swargam Thalupule Therichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa swargam Thalupule Therichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janta Aatalo Chusale.. sukhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janta Aatalo Chusale.. sukhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chaaladhe Ee Pulaknitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaaladhe Ee Pulaknitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeluko Naa Thanuvantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeluko Naa Thanuvantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedhavullo Madhurasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavullo Madhurasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhehamlo Paravasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhehamlo Paravasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpindhe Veediponi Viraham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpindhe Veediponi Viraham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamatallo Thadisinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamatallo Thadisinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvella Alasinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvella Alasinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Peruguthondi Dhaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peruguthondi Dhaaham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenunna Lokame Marichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenunna Lokame Marichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa swargam Thalupule Therichaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa swargam Thalupule Therichaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janta Aatalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janta Aatalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusale Sukhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusale Sukhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chaaladhe Ee Pulaknitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaaladhe Ee Pulaknitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeluko Naa Thanuvantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeluko Naa Thanuvantha"/>
</div>
</pre>
