---
title: 'aaha ooho song lyrics'
album: 'Dharala Prabhu'
artist: 'Oorka'
lyricist: 'Bharath Sankar'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Aaha ooho'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
singers: 
- Bharath Sankar
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/1HnFhsCuLUQ'
type: 'happy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Aaha… ooho
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaha… ooho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaa ohooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ahaaa ohooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa…oohoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaa…oohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaaaaa ahaaaaaa ohoooo ahaaaa ohoooo…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aahaaaaa ahaaaaaa ohoooo ahaaaa ohoooo…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adichuruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichuruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah kedachiruchae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ah kedachiruchae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruchae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudichiruchae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah mudinchiruchae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ah mudinchiruchae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallaatamo
<input type="checkbox" class="lyrico-select-lyric-line" value="Nallaatamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaatamo
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallaatamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae inimae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae inimae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotto kottunu kottumo…
<input type="checkbox" class="lyrico-select-lyric-line" value="Kotto kottunu kottumo…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aalaadha aandavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalaadha aandavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arulaadha varam onnaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Arulaadha varam onnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraamaa alliyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraamaa alliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alataamma kodukura thaanae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Alataamma kodukura thaanae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thara rara thara rara thara rara thara rara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thara rara thara rara thara rara thara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara rara thara rara thara rara thara rara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thara rara thara rara thara rara thara rara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suthura ulagam…
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthura ulagam…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pinnaal suthumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pinnaal suthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuva kuri vechaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhuva kuri vechaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna porula kaakkumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponna porula kaakkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyyyy…
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyyy…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sukura nizhalum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Sukura nizhalum…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pinnaal alaiyuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pinnaal alaiyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Selavaaga irupadhellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Selavaaga irupadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varava varava perugudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Varava varava perugudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pudhusu palasu aanalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhusu palasu aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vayasu koraiya pogudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un vayasu koraiya pogudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nenacha podhellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nenacha podhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha bhoomilyil kanako maarudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha bhoomilyil kanako maarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maayam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Maayam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kaayam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey kaayam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaym illa mandhiram illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Maaym illa mandhiram illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal illa kaamam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal illa kaamam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi pangu ketpathukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadhi pangu ketpathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaram ingae yaarum illaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaram ingae yaarum illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathu nadathu nadathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadathu nadathu nadathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja…
<input type="checkbox" class="lyrico-select-lyric-line" value="Raaja…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aalaadha aandavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalaadha aandavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arulaadha varam onnaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Arulaadha varam onnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraamaa alliyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraamaa alliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alataamma kodukura thaanae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Alataamma kodukura thaanae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae….ae….ae…ae….ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae….ae….ae…ae….ae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhaaraala prabhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaaraala prabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thara rara thara rara thara rara thara rara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thara rara thara rara thara rara thara rara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara rara thara rara thara rara thara rara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thara rara thara rara thara rara thara rara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adikuthae …
<input type="checkbox" class="lyrico-select-lyric-line" value="Adikuthae …"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kedaikuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey kedaikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey pudikuthae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey pudikuthae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mudiyuthu mudiyuthu….
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey mudiyuthu mudiyuthu…."/>
</div>
</pre>