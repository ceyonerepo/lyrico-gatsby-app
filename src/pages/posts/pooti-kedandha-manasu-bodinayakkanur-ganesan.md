---
title: "pooti kedandha manasu song lyrics"
album: "Bodinayakkanur Ganesan"
artist: "John Peter"
lyricist: "Nandalala"
director: "O. Gnanam"
path: "/albums/bodinayakkanur-ganesan-lyrics"
song: "Pooti Kedandha Manasu"
image: ../../images/albumart/bodinayakkanur-ganesan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UotaykNMTms"
type: "love"
singers:
  - Karthik
  - Priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poottikkedandha manasu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottikkedandha manasu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thannaaley thorandhiruchi minnal adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannaaley thorandhiruchi minnal adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">kallikkaattu Odai tharisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallikkaattu Odai tharisu"/>
</div>
<div class="lyrico-lyrics-wrapper">ava kannaaley poothiruchi poovaa sirichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava kannaaley poothiruchi poovaa sirichi"/>
</div>
<div class="lyrico-lyrics-wrapper">kallachaavi poadum kannu rendu paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallachaavi poadum kannu rendu paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">kannakkatti poagum kannakkuzhi paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakkatti poagum kannakkuzhi paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjikkul ingey vaanavillaa naan paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjikkul ingey vaanavillaa naan paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottikkedandha manasu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottikkedandha manasu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thannaaley thorandhiruchi minnal adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannaaley thorandhiruchi minnal adichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudaiya pudichi minnal onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaiya pudichi minnal onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu vachu poaradhu poal ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu vachu poaradhu poal ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulathil veesum thavalaikkallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathil veesum thavalaikkallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu thottuthaavum avakkannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu thottuthaavum avakkannu"/>
</div>
<div class="lyrico-lyrics-wrapper">nellikkaai thinna vaaikku thanneervittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nellikkaai thinna vaaikku thanneervittu"/>
</div>
<div class="lyrico-lyrics-wrapper">nesikkum nenjai nanjaai ellaam virippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesikkum nenjai nanjaai ellaam virippu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada raathiri iruttil minmini poochiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada raathiri iruttil minmini poochiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyiren un nenappil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyiren un nenappil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottikkedandha manasu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottikkedandha manasu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thannaaley thorandhiruchi minnal adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannaaley thorandhiruchi minnal adichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruvippoguradhu poala nenjey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvippoguradhu poala nenjey"/>
</div>
<div class="lyrico-lyrics-wrapper">andharangam aasaigalo nenaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharangam aasaigalo nenaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruvi vaayil vaikkoal poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvi vaayil vaikkoal poala"/>
</div>
<div class="lyrico-lyrics-wrapper">emmanasu avaloada parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emmanasu avaloada parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">meen vita moochikkaatru neeril thangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meen vita moochikkaatru neeril thangumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan vita moochellaamey kaatril thangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vita moochellaamey kaatril thangumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">avan vaasalil pozhiyum megamaai nagarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan vaasalil pozhiyum megamaai nagarum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal sugamammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal sugamammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottikkedandha manasu manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottikkedandha manasu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">thannaaley thorandhiruchi minnal adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannaaley thorandhiruchi minnal adichi"/>
</div>
</pre>
