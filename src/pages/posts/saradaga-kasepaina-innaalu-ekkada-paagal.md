---
title: "saradaga kasepaina song lyrics"
album: "Paagal"
artist: "Radhan"
lyricist: "Ananta Sriram "
director: "Naressh Kuppili"
path: "/albums/paagal-lyrics"
song: "Saradaga Kasepaina - Innaalu ekkada"
image: ../../images/albumart/paagal.jpg
date: 2021-08-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sc0QtI6_WWc"
type: "love"
singers:
  - Karthik
  - Purnima
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Innaallu ekkada unnaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu ekkada unnaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvaala evvaru pampare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvaala evvaru pampare"/>
</div>
<div class="lyrico-lyrics-wrapper">Innella cheekati gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innella cheekati gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Varnaala vennela nimpare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varnaala vennela nimpare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaarilo puvvulai veechene aashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaarilo puvvulai veechene aashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandaga chercheney nedu nee chethulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandaga chercheney nedu nee chethulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilo doodhulai oogene oohalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo doodhulai oogene oohalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinduga marchene eedanee maatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinduga marchene eedanee maatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthaga kotthaga puttina inkola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthaga kotthaga puttina inkola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame ammaga kannaadhe nannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame ammaga kannaadhe nannila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga kaasepina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga kaasepina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarijodai neetho unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarijodai neetho unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saripodhaa nakee janmaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripodhaa nakee janmaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvai osarina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvai osarina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurincha lokamlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurincha lokamlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi chaalle ippudu ee kommaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi chaalle ippudu ee kommaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni gnapakale sampaadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gnapakale sampaadana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanchilo poguchesi neekiyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanchilo poguchesi neekiyyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindulese sambaranni eerojuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindulese sambaranni eerojuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchamu daachukoka pancheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchamu daachukoka pancheyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalone santhosham kaliginche oopiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalone santhosham kaliginche oopiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Udayaanne neekosam urikinde ee pari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udayaanne neekosam urikinde ee pari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala nimirey vella kosam verrodinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala nimirey vella kosam verrodinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanalakai nelalaaga vecha mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanalakai nelalaaga vecha mari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhella jeevithaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhella jeevithaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhaala kaanuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhaala kaanuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Andinchinaavu haayiga vaaralalone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andinchinaavu haayiga vaaralalone"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkaanila nuvvekshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkaanila nuvvekshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhundi laagaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhundi laagaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhraani dhaatinanuga theeralalone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhraani dhaatinanuga theeralalone"/>
</div>
<div class="lyrico-lyrics-wrapper">Chenthane chenthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chenthane chenthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnila choosthoone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnila choosthoone"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakasam anchune thaaakane ninchuney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakasam anchune thaaakane ninchuney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saradaga kaasepina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradaga kaasepina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarijodai neetho unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarijodai neetho unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saripodhaa nakee janmaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saripodhaa nakee janmaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvai osarina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvai osarina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurincha lokamlona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurincha lokamlona"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi challe ippudu ee kommaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi challe ippudu ee kommaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinni chinni gnaapakale sampadhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni chinni gnaapakale sampadhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanchilo poguchesi neekiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanchilo poguchesi neekiyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindulese sambaraanni eerojuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindulese sambaraanni eerojuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchamu daachukoka pancheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchamu daachukoka pancheyana"/>
</div>
</pre>
