---
title: "modern muniyamma song lyrics"
album: "Vantha Rajava Than Varuven"
artist: "Hiphop Tamizha"
lyricist: "Arivu"
director: "Sundar C"
path: "/albums/vantha-rajava-than-varuven-lyrics"
song: "Modern Muniyamma"
image: ../../images/albumart/vantha-rajava-than-varuven.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VMHe5SkfAO0"
type: "love"
singers:
  - Anthakudi Ilayaraja
  - Srinisha Jayaseelan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Modern Muniyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Muniyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Vandi Madonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Vandi Madonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavaa Pagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaa Pagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Raasathi Riyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Raasathi Riyaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modern Muniyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Muniyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Vandi Madonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Vandi Madonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavaa Pagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaa Pagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Raasathi Riyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Raasathi Riyaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">En Diva Diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Diva Diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gummaalatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gummaalatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorey Paakkum Diva Live Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Paakkum Diva Live Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">En Diva Diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Diva Diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gummaalatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gummaalatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorey Paakkum Diva Live Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Paakkum Diva Live Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gundumalli Katy Perry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundumalli Katy Perry"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Konjam Paathukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Konjam Paathukkadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkaama Neeyum Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkaama Neeyum Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkathadi Heartu Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkathadi Heartu Vali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">En Diva Diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Diva Diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gummaalatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gummaalatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorey Paakkum Diva Live Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Paakkum Diva Live Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">En Diva Diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Diva Diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gummaalatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gummaalatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorey Paakkum Diva Live Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Paakkum Diva Live Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanakku Marumaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanakku Marumaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaikkum Thirumaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaikkum Thirumaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Vaikka Vanthirukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Vaikka Vanthirukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Teddy Bear Vella Sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teddy Bear Vella Sokka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthi Suthi Vanthiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi Suthi Vanthiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamiyenu Konjiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamiyenu Konjiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi Kettu Ponathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi Kettu Ponathuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saniyanenu Thalliduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saniyanenu Thalliduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkatha Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkatha Vaa Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Britishu Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Britishu Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye Nikkatha Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Nikkatha Vaa Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Britishu Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Britishu Rani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pacharisi Raava Ladde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pacharisi Raava Ladde"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathadi Ennai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathadi Ennai Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pacharisi Raava Ladde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pacharisi Raava Ladde"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathadi Ennai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathadi Ennai Vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallazhagi Pallazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallazhagi Pallazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathoorukku Power Cut-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathoorukku Power Cut-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathoorukku Power Cut-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathoorukku Power Cut-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiye Pathoorukku Power Cut-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Pathoorukku Power Cut-tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chellama Vazhantha Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chellama Vazhantha Kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sari Katti Paayum Puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sari Katti Paayum Puli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaanavillu Vannathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaanavillu Vannathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Killaatha En Kannathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaatha En Kannathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalathaan Naanum Ippadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalathaan Naanum Ippadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Single Aana Singamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Single Aana Singamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Paiyan Nenjamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Paiyan Nenjamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Ennai Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Ennai Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Vittaa Enna Gethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Vittaa Enna Gethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Single Aana Singamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Single Aana Singamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Paiyan Nenjamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Paiyan Nenjamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangame Ennai Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Ennai Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Vittaa Enna Gethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Vittaa Enna Gethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaatha Vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha Vaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Podaatha Rulelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Podaatha Rulelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pollaatha Vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaatha Vaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Podaatha Rule Lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Podaatha Rule Lu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modern Muniyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Muniyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Vandi Madonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Vandi Madonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavaa Pagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaa Pagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Raasathi Riyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Raasathi Riyaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">En Diva Diva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Diva Diva"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gummaalatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gummaalatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorey Paakkum Diva Live Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorey Paakkum Diva Live Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Vaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vaarene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Diva Divaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diva Divaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Diva Divaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diva Divaa"/>
</div>
</pre>
