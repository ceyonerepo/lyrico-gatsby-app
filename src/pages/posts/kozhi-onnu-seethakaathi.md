---
title: "kozhi onnu song lyrics"
album: "Seethakaathi"
artist: "Govind Vasantha"
lyricist: "Yugabharathi"
director: "Balaji Tharaneetharan"
path: "/albums/seethakaathi-lyrics"
song: "Kozhi Onnu"
image: ../../images/albumart/seethakaathi.jpg
date: 2018-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DJ0QSXsK8jY"
type: "happy"
singers:
  - Pushpavanam Kuppusamy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haan kozhi onnu kaal eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan kozhi onnu kaal eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi pandha thorkadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi pandha thorkadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugindra naatiyaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugindra naatiyaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu paarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu paarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paambu puththu thaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu puththu thaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchchi koburatha pola enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchchi koburatha pola enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Podugindra nadagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podugindra nadagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum kelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum kelunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nool arunthu pona pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool arunthu pona pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam etta thaavugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam etta thaavugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedu ketta kevalaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedu ketta kevalaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla venunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla venunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam namma kaalaminnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam namma kaalaminnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavathil pesi nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavathil pesi nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezharaikku egapatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezharaikku egapatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lollu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lollu thaanunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesham potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ara nellikaaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara nellikaaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Apple agaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apple agaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosi koorvaalaga maaridathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi koorvaalaga maaridathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhi onnu kaal eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi onnu kaal eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi pandha thorkadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi pandha thorkadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugindra naatiyaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugindra naatiyaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu paarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu paarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paambu puththu thaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu puththu thaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchchi koburatha pola enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchchi koburatha pola enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Podugindra nadagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podugindra nadagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum kelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum kelunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nool arunthu pona pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool arunthu pona pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam etta thaavugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam etta thaavugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedu ketta kevalaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedu ketta kevalaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla venunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla venunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam namma kaalaminnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam namma kaalaminnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavathil pesi nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavathil pesi nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezharaikku egapatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezharaikku egapatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lollu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lollu thaanunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurukaththi poovum roosavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurukaththi poovum roosavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna ennikolla koodathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna ennikolla koodathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayir illaama ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayir illaama ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnana oonjal aadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana oonjal aadathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podi matta naalum peengaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi matta naalum peengaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhi solla oorum ketkkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhi solla oorum ketkkathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasam illaatha kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasam illaatha kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda moonja kaattathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda moonja kaattathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neer vaththi pona pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer vaththi pona pinnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meen vattam poda ennaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen vattam poda ennaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee mochcha pandam kettaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee mochcha pandam kettaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kooru katti vikkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kooru katti vikkathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varavillai neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavillai neeyum "/>
</div>
<div class="lyrico-lyrics-wrapper">aattam podathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aattam podathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verai vittu ooru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verai vittu ooru "/>
</div>
<div class="lyrico-lyrics-wrapper">naalum neengidathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum neengidathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhi onnu kaal eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi onnu kaal eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi pandha thorkadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi pandha thorkadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugindra naatiyaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugindra naatiyaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu paarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu paarunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paambu puththu thaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu puththu thaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchchi koburatha pola enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchchi koburatha pola enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Podugindra nadagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podugindra nadagaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum kelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum kelunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nool arunthu pona pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool arunthu pona pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam etta thaavugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam etta thaavugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedu ketta kevalaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedu ketta kevalaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla venunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla venunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam namma kaalaminnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam namma kaalaminnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavathil pesi nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavathil pesi nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezharaikku egapatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezharaikku egapatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Lollu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lollu thaanunga"/>
</div>
</pre>
