---
title: "vanji kota song lyrics"
album: "Margandeyan"
artist: "Sundar C Babu"
lyricist: "Kavivarman"
director: "FEFSI Vijayan"
path: "/albums/markandeyan-lyrics"
song: "Vanji Kota"
image: ../../images/albumart/markandeyan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9nGOi2ZmVeI"
type: "happy"
singers:
  - Kavivarman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Azhagu Sathiraadum Vanji Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Sathiraadum Vanji Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesingu Rajanin Senji Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesingu Rajanin Senji Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamae Bayanthaadum Veera Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamae Bayanthaadum Veera Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Paiyan Aerappoagum Vetri Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Paiyan Aerappoagum Vetri Koatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanji Koatta Senji Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanji Koatta Senji Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanji Koatta Senji Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanji Koatta Senji Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera Koatta Vetri Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Koatta Vetri Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Ellam Vaazhavaikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Ellam Vaazhavaikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Koattathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Koattathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu Paadi Poattuthaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadi Poattuthaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Koothukkatti Pozhuthapoakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothukkatti Pozhuthapoakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshatha Rettippaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshatha Rettippaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Vaettaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Vaettaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Thaagam Theerkka Vaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Thaagam Theerkka Vaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Orukaasum Kaetkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orukaasum Kaetkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Osiyila Thanni Thanthathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osiyila Thanni Thanthathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Namma Aerkavilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Namma Aerkavilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyaera Sonnathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyaera Sonnathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadu Thaane Veedu Vanthathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadu Thaane Veedu Vanthathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Koatta Vetri Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Koatta Vetri Koatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanji Koatta Senji Koatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanji Koatta Senji Koatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Ellam Vaazhavaikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Ellam Vaazhavaikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Moongil Koattathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Koattathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu Paadi Poattuthaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadi Poattuthaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Koothukkatti Pozhuthapoakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothukkatti Pozhuthapoakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshatha Rettippaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshatha Rettippaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Vaettaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Vaettaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallamillae Vanjamillae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallamillae Vanjamillae "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Kanna Kolillae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Kanna Kolillae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Vaangum Aasaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Vaangum Aasaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhøømi Thaangum Thøalgal Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhøømi Thaangum Thøalgal Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Thaangum Kaal Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Thaangum Kaal Vendum"/>
</div>
Šø<div class="lyrico-lyrics-wrapper">lli Šølli Vella Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lli Šølli Vella Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkaiyødu Nam Rendukai Inainthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaiyødu Nam Rendukai Inainthaal"/>
</div>
Š<div class="lyrico-lyrics-wrapper">aathikka Mudiyaathaa Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathikka Mudiyaathaa Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Køatta Vetri Køatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Køatta Vetri Køatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanji Køatta Šenji Køatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanji Køatta Šenji Køatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Èllam Vaazhavaikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Èllam Vaazhavaikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Møøngil Køattathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Møøngil Køattathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu Paadi Pøattuthaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadi Pøattuthaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Køøthukkatti Pøzhuthapøakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køøthukkatti Pøzhuthapøakku"/>
</div>
Š<div class="lyrico-lyrics-wrapper">anthøshatha Rettippaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthøshatha Rettippaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Vaettaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Vaettaithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
Šø<div class="lyrico-lyrics-wrapper">lli Vachi Machaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lli Vachi Machaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Pønnu Paakka Pøanaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pønnu Paakka Pøanaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Katha Ènna Aachidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Katha Ènna Aachidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Paarkka Pøna Paappaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Paarkka Pøna Paappaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyø Rettai Peeppaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyø Rettai Peeppaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil Aasa Thallipøachidaa Pøachi Pø
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Aasa Thallipøachidaa Pøachi Pø"/>
</div>
È<div class="lyrico-lyrics-wrapper">era Thuni Kattikkø 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="era Thuni Kattikkø "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavuthadichi Paduthukkø
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavuthadichi Paduthukkø"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal Thøøngattundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal Thøøngattundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Køatta Vetri Køatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Køatta Vetri Køatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanji Køatta Šenji Køatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanji Køatta Šenji Køatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Èllam Vaazhavaikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Èllam Vaazhavaikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Møøngil Køattathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Møøngil Køattathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattu Paadi Pøattuthaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattu Paadi Pøattuthaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Køøthukkatti Pøzhuthapøakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Køøthukkatti Pøzhuthapøakku"/>
</div>
Š<div class="lyrico-lyrics-wrapper">anthøshatha Rettippaakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthøshatha Rettippaakku "/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Vaettaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Vaettaithaan"/>
</div>
</pre>
