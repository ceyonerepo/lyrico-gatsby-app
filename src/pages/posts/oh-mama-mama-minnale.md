---
title: "oh mama mama song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Vaali"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Oh Mama Mama"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/J4XXOyNsV8E"
type: "happy"
singers:
  - Shankar Mahadevan
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Therukkooththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therukkooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rhythm pottu….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rhythm pottu…."/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidu joottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu joottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo maamaa maamaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo maamaa maamaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama maama maamoamiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama maama maamoamiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo sunday..monday..tuesday…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo sunday..monday..tuesday…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu naalum keep it free yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu naalum keep it free yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo maamaa maamaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo maamaa maamaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama maama maamoamiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama maama maamoamiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo sunday..monday..tuesday…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo sunday..monday..tuesday…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu naalum keep it free yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu naalum keep it free yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bismillaa bismillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaa bismillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vaazhvai vaazhvom playful-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vaazhvai vaazhvom playful-aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo laallaa oo laallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo laallaa oo laallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu western gaanaa gopaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu western gaanaa gopaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaththai idhu kalakkidum kalakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaththai idhu kalakkidum kalakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara paadalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara paadalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo maamaa maamaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo maamaa maamaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama maama maamoamiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama maama maamoamiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo sunday..monday..tuesday…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo sunday..monday..tuesday…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu naalum keep it free yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu naalum keep it free yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bismillaa bismillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaa bismillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vaazhvai vaazhvom playful-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vaazhvai vaazhvom playful-aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo laallaa oo laallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo laallaa oo laallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu western gaanaa gopaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu western gaanaa gopaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaththai idhu kalakkidum kalakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaththai idhu kalakkidum kalakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara…. Paadalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara…. Paadalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Therukkooththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therukkooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rhythm pottu….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rhythm pottu…."/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidu joottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidu joottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkukoo…kukkukoo…kukkukoo….koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkukoo…kukkukoo…kukkukoo….koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhadhae maamoolaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhadhae maamoolaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee podu new rule-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee podu new rule-aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life enna naam padikkum high school-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life enna naam padikkum high school-aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yellow linw-aa.. ee.pee.koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yellow linw-aa.. ee.pee.koo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtam pola neeyum go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam pola neeyum go"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbaththukku kodi roottu engengoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbaththukku kodi roottu engengoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizh paattaaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh paattaaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha world-ai pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha world-ai pidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum yetrukkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum yetrukkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamaa naanga ready..eeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa naanga ready..eeeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo maamaa maamaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo maamaa maamaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama maama maamoamiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama maama maamoamiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo sunday..monday..tuesday…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo sunday..monday..tuesday…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu naalum keep it free yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu naalum keep it free yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bismillaa bismillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaa bismillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vaazhvai vaazhvom playful-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vaazhvai vaazhvom playful-aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo laallaa oo laallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo laallaa oo laallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu western gaanaa gopaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu western gaanaa gopaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaththai idhu kalakkidum kalakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaththai idhu kalakkidum kalakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara…. Paadalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara…. Paadalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nana tara tara na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana tara tara na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana tara tara na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana tara tara na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">Na na na na na na na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na na na na na na na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">Na na na na na na na na na….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na na na na na na na na na…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venum venum vae vae venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum venum vae vae venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Venum venum vae vae venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum venum vae vae venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalil telescope varam venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil telescope varam venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgalil rocket speed venum…venumm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgalil rocket speed venum…venumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Film-u kaata jeans venum..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Film-u kaata jeans venum.."/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-a theththa benz venum..venummm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-a theththa benz venum..venummm.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bill gates koodaththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bill gates koodaththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaaththaa aadiththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaaththaa aadiththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Banku kadai baaki ellaam theerpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banku kadai baaki ellaam theerpomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veerappan koodaththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerappan koodaththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Snow balling aadiththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snow balling aadiththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hostages ellaaraiyum meetpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hostages ellaaraiyum meetpomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal vaazhkkaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal vaazhkkaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru full stop illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru full stop illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum irangi yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum irangi yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu bus stop illae…ehhh……
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu bus stop illae…ehhh……"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo maamaa…oo maamaa…oo maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo maamaa…oo maamaa…oo maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo maamaa maamaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo maamaa maamaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama maama maamoamiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama maama maamoamiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo sunday..monday..tuesday…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo sunday..monday..tuesday…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu naalum keep it free yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu naalum keep it free yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bismillaa bismillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bismillaa bismillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vaazhvai vaazhvom playful-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vaazhvai vaazhvom playful-aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo laallaa oo laallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo laallaa oo laallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu western gaanaa gopaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu western gaanaa gopaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaththai idhu kalakkidum kalakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaththai idhu kalakkidum kalakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara…. paadalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara…. paadalaaa"/>
</div>
</pre>
