---
title: "lol lol arasan song lyrics"
album: "Naai Sekar"
artist: "Ajesh - Anirudh Ravichander"
lyricist: "Vivek"
director: "Kishore Rajkumar"
path: "/albums/naai-sekar-lyrics"
song: "Lol Lol Arasan"
image: ../../images/albumart/naai-sekar.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GHkQZ20w_vI"
type: "happy"
singers:
  - Baba Sehgal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru lollu arasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru lollu arasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa lol lol arasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa lol lol arasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paala kudi ball ah pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala kudi ball ah pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalu manusha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalu manusha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeda kelakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeda kelakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan breed ah pudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan breed ah pudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma country paya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma country paya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri ulla friendly miruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri ulla friendly miruga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un network kooda varalaanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un network kooda varalaanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaal varuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaal varuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan htchil vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan htchil vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Good boy illa echil vaippaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good boy illa echil vaippaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thadava bhaasha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thadava bhaasha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Force ah nippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Force ah nippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Superman ah Spiderman ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Superman ah Spiderman ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukkum mela doberman ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukkum mela doberman ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutty kutty poona car bus van ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty kutty poona car bus van ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathidum yeriduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathidum yeriduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundu gundu naaya sudakaattu poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundu gundu naaya sudakaattu poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Therichittu odiruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therichittu odiruvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naayar kada porayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayar kada porayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda vecha vadayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda vecha vadayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaana pudichiruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaana pudichiruvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Smell pannum nose ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smell pannum nose ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Running race ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Running race ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan thaan jayichiruvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan thaan jayichiruvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaradhan jayippaan sollaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaradhan jayippaan sollaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma naaiyagan jaiyippaan pallaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma naaiyagan jaiyippaan pallaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kari soru kuduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari soru kuduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula sorinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula sorinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirayum koduppaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirayum koduppaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aararivellaam peraasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aararivellaam peraasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil onne onnu mattum nee veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil onne onnu mattum nee veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgalukkulla mudhalidam antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgalukkulla mudhalidam antha"/>
</div>
<div class="lyrico-lyrics-wrapper">God ukkum pudicha dogukku thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="God ukkum pudicha dogukku thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru lollu arasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru lollu arasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa lol lol arasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa lol lol arasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paala kudi ball ah pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala kudi ball ah pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalu manusha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalu manusha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeda kelakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeda kelakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan breed ah pudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan breed ah pudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma country paya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma country paya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri ulla friendly mirugan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri ulla friendly mirugan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un network kooda varalaanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un network kooda varalaanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaal varuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaal varuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan htchil vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan htchil vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Good boy illa echil vaippaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good boy illa echil vaippaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thadava bhaasha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thadava bhaasha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Force ah nippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Force ah nippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Superman ah Spiderman ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Superman ah Spiderman ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukkum mela doberman ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukkum mela doberman ah"/>
</div>
</pre>
