---
title: "naanga vera maari song lyrics"
album: "Valimai"
artist: "Yuvan Shankar Raja"
lyricist: "Vignesh Shivan"
director: "H. Vinoth"
path: "/albums/valimai-lyrics"
song: "Naanga Vera Maari"
image: ../../images/albumart/valimai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gjOLk0L830c"
type: "mass"
singers:
  - Yuvan Shankar Raja
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Hey Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Hey Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vera Vera Vera Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vera Vera Vera Vera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yella Naalumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella Naalumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Naalu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Naalu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yella Neramum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella Neramum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Neram Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Neram Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yella Oorumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella Oorumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Ooru Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Ooru Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yella Payalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yella Payalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Paya Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Paya Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Irukkavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Irukkavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Nalla Kathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Nalla Kathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Irukkavana Natppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Irukkavana Natppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Vechikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Vechikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala Vaarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vaarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Mattum Katthukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Mattum Katthukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathoda Neeyum Oda Othukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathoda Neeyum Oda Othukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thaganu Minnalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaganu Minnalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenavetaa Thullalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenavetaa Thullalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Valanu Pesaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Valanu Pesaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiya Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiya Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kada Kadanu Yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kada Kadanu Yeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Muraiya Maathalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Muraiya Maathalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhu Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhu Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thaganu Minnalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaganu Minnalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenavetaa Thullalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenavetaa Thullalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Valanu Pesaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Valanu Pesaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiya Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiya Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kada Kadanu Yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kada Kadanu Yeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Muraiya Maathalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Muraiya Maathalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhu Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhu Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Hey Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Hey Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vera Vera Vera Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vera Vera Vera Vera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa Aa Vaangikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa Aa Vaangikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aai Indha Indha Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aai Indha Indha Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Indha Indha Hey Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Indha Indha Hey Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vera Maari Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Veeta Modha Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Veeta Modha Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Thaanavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thaanavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyagum Un Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyagum Un Ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuthu Solla Naan Gnani Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthu Solla Naan Gnani Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Eduthu Sonaa Endha Thappum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Eduthu Sonaa Endha Thappum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku Nimmadhiya Nee Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku Nimmadhiya Nee Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiku Erangi Sethukidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiku Erangi Sethukidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ennatha Azhaga Nee Amachikkita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ennatha Azhaga Nee Amachikkita"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamey Azhagaagum Seri Yagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamey Azhagaagum Seri Yagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhu Vaazha Vidu Avvalothaan Thathuvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhu Vaazha Vidu Avvalothaan Thathuvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Kaala Vitaa Odachiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Kaala Vitaa Odachiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala Vaarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Vaarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha Mattum Kathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha Mattum Kathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu Pudichitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu Pudichitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thaganu Minnalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaganu Minnalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenavetaa Thullalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenavetaa Thullalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Valanu Pesaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Valanu Pesaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiya Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiya Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kada Kadanu Yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kada Kadanu Yeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Muraiya Maathalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Muraiya Maathalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhu Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhu Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thaganu Minnalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thaganu Minnalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenavetaa Thullalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenavetaa Thullalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala Valanu Pesaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala Valanu Pesaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiya Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiya Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kada Kadanu Yeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kada Kadanu Yeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vara Muraiya Maathalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Muraiya Maathalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhu Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalladhu Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Hey Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Hey Vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vera Vera Vera Vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vera Vera Vera Vera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Vera Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa Aa Vaangikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa Aa Vaangikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Aai Indha Indha Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aai Indha Indha Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Indha Indha Hey Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Indha Indha Hey Indha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Vera Maari Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vera Maari Vera Maari"/>
</div>
</pre>
