---
title: "la la la song lyrics"
album: "Meeku Maathrame Cheptha"
artist: "Sivakumar"
lyricist: "Rakendu Mouli"
director: "Shammeer Sultan"
path: "/albums/meeku-maathrame-cheptha-lyrics"
song: "La La La"
image: ../../images/albumart/meeku-maathrame-cheptha.jpg
date: 2019-11-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SDFRhgSMLBo"
type: "happy"
singers:
  - Vedala Hemachandra
  - Krishnan Ganesan
  - Uma Neha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Why Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Why Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku Maathram Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku Maathram Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Meedha Thappu Thappai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Meedha Thappu Thappai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nake Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nake Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Why Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Why Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyon Yesa Naake Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyon Yesa Naake Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappe Kaani Oppu Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappe Kaani Oppu Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaipoyenila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaipoyenila"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhukani Yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhukani Yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Thappai Yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thappai Yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aindhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aindhilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukani"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Karma Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Karma Kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La Lalala Lalala La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Lalala Lalala La"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La Lalala Lalala La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Lalala Lalala La"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Bhaaraalu Ghoraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Bhaaraalu Ghoraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nake Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nake Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraalu Varjyaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraalu Varjyaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapaadavela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapaadavela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtaalu Saapaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtaalu Saapaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaalantoo Ledhantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalantoo Ledhantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamlo Boledu Mandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamlo Boledu Mandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Yedhavalu Unnaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Yedhavalu Unnaarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaallandharini Vadilesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaallandharini Vadilesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoolanthaa Theerchesindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoolanthaa Theerchesindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Timeye Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timeye Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Saantham Motham Naakinchindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saantham Motham Naakinchindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifeye Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifeye Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La Lalala Lalala La 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Lalala Lalala La "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Why Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Why Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku Maathram Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku Maathram Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Meedha Thappu Thappai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Meedha Thappu Thappai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nake Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nake Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Why Ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Why Ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyon Yesa Naake Ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyon Yesa Naake Ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappe Kaani Oppu Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappe Kaani Oppu Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaipoyenila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaipoyenila"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhukani Yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhukani Yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me "/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Thappai Yaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Thappai Yaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aindhilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aindhilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaen Endhuku Endhukani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaen Endhuku Endhukani"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Karma Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Karma Kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Me"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La Lalala Lalala La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Lalala Lalala La"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La Lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La Lalala"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La Lalala Lalala La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La Lalala Lalala La"/>
</div>
<div class="lyrico-lyrics-wrapper">Chee Deenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chee Deenamma"/>
</div>
</pre>
