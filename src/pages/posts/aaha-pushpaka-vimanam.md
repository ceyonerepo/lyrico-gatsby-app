---
title: "Aaha song lyrics"
album: "Pushpaka Vimanam"
artist: "Ram Miriyala - Sidharth Sadasivuni - Mark K Robin - Amit N Dasani"
lyricist: "Phani Kumar Raghava"
director: "Damodara"
path: "/albums/pushpaka-vimanam-lyrics"
song: "Aaha"
image: ../../images/albumart/pushpaka-vimanam.jpg
date: 2021-11-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BJZ8VpOFjAQ?start=35"
type: "happy"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O tharaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O tharaka"/>
</div>
<div class="lyrico-lyrics-wrapper">O o o tharaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O o o tharaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayalokamlo kanumarugai poyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayalokamlo kanumarugai poyava"/>
</div>
<div class="lyrico-lyrics-wrapper">O o asale modhalukani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O o asale modhalukani"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha kanchi cherenuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha kanchi cherenuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthalone rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalone rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Arerey inthileni o inti vadi katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arerey inthileni o inti vadi katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasinadu brahma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasinadu brahma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinabaduthunda vema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinabaduthunda vema"/>
</div>
<div class="lyrico-lyrics-wrapper">Veediki enti karma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veediki enti karma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikaramaina chupinchaka malichave janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikaramaina chupinchaka malichave janma"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudani bhavasagarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudani bhavasagarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudilo pada dhosena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudilo pada dhosena"/>
</div>
<div class="lyrico-lyrics-wrapper">Tirigina aa chote thippindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tirigina aa chote thippindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi pathi samsarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi pathi samsarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha orey jeevuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha orey jeevuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha telavarera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha telavarera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha modhalettara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha modhalettara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha jara nee natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha jara nee natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha o sundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha o sundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha nee lekkalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha nee lekkalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha thalakindhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha thalakindhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ayipoyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha ayipoyena"/>
</div>
<div class="lyrico-lyrics-wrapper">O tharaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O tharaka"/>
</div>
<div class="lyrico-lyrics-wrapper">O o o tharaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O o o tharaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O ayomayanga marena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ayomayanga marena"/>
</div>
<div class="lyrico-lyrics-wrapper">O prathi gadiya nayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O prathi gadiya nayana"/>
</div>
<div class="lyrico-lyrics-wrapper">Angatlo anni unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angatlo anni unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Alludi notlo shani undha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alludi notlo shani undha"/>
</div>
<div class="lyrico-lyrics-wrapper">Arachethilo vankar geethai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arachethilo vankar geethai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kapurame kulchinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kapurame kulchinda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panakame lekundane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panakame lekundane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudakedho thagilesindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudakedho thagilesindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanindha naluguri nota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanindha naluguri nota"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika nee paruve govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika nee paruve govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha orey jeevuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha orey jeevuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha telavarera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha telavarera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha modhalettara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha modhalettara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha jara nee natana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha jara nee natana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha o sundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha o sundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha nee lekkalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha nee lekkalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha thalakindhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha thalakindhuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ayipoyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha ayipoyena"/>
</div>
<div class="lyrico-lyrics-wrapper">O tharaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O tharaka"/>
</div>
<div class="lyrico-lyrics-wrapper">O o o tharaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O o o tharaka"/>
</div>
</pre>
