---
title: "manchido cheddadho song lyrics"
album: "Middle Class Melodies"
artist: "Sweekar Agasthi"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Vinod Anantoju"
path: "/albums/middle-class-melodies-lyrics"
song: "Manchido Cheddadho"
image: ../../images/albumart/middle-class-melodies.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gGCZgf2Vf98"
type: "sad"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manchido cheddado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchido cheddado"/>
</div>
<div class="lyrico-lyrics-wrapper">Rentiki madyedo anthu chikka ledaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rentiki madyedo anthu chikka ledaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam etuvantidho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam etuvantidho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayyamo nayyamo eppudem cheyyuno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayyamo nayyamo eppudem cheyyuno"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekka thela leda dhaani peru emito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekka thela leda dhaani peru emito"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullu unna maargana nadipeti kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullu unna maargana nadipeti kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi unte raadhaari chupinchadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi unte raadhaari chupinchadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikku prashna veseti telivayna kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku prashna veseti telivayna kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappakunda badhulai raadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappakunda badhulai raadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhiloni chirunavvu janminchagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiloni chirunavvu janminchagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalath epodha kanu moyadaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalath epodha kanu moyadaha"/>
</div>
<div class="lyrico-lyrics-wrapper">nadi reyi dari cheri masi pooyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadi reyi dari cheri masi pooyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluge raada cheripeyyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluge raada cheripeyyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ara chethi rekallo ledanta repu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara chethi rekallo ledanta repu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalni vadilesi raavali choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalni vadilesi raavali choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudoddhu yedhantu odharupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudoddhu yedhantu odharupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachhi poye megaley ee badhalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachhi poye megaley ee badhalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipovu kada dhaka aa ningi laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipovu kada dhaka aa ningi laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthamaithe kaaradhu loloni dhairyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthamaithe kaaradhu loloni dhairyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni vedhale unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni vedhale unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhranni polindhi ee jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhranni polindhi ee jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisi theerali yedhureedhadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisi theerali yedhureedhadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porapaatu kadhanta padipovadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porapaatu kadhanta padipovadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undalo leche gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undalo leche gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Etuvanti aatankam eduraina gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etuvanti aatankam eduraina gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Munumundhukelleti alavatu maani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munumundhukelleti alavatu maani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kerataalu aageti rojedhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerataalu aageti rojedhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganthalnni o naadu theesesi kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganthalnni o naadu theesesi kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthavanni kallara chupinchada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthavanni kallara chupinchada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammukunna bramalanni kaavali maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammukunna bramalanni kaavali maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindha padda thavruvathaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindha padda thavruvathaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannena thannena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannena thannena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannena thannena thane nane nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannena thannena thane nane nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thane nane nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thane nane nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannena thannena thane nane nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannena thannena thane nane nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thane nane nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thane nane nana"/>
</div>
</pre>
