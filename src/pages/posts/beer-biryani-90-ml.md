---
title: "beer biryani song lyrics"
album: "90 ML"
artist: "Silambarasan"
lyricist: "Mirchi Vijay"
director: "Anita Udeep"
path: "/albums/90-ml-lyrics"
song: "Beer Biryani"
image: ../../images/albumart/90-ml.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gngyrUPwNjE"
type: "happy"
singers:
  - Maria
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kumaru Kumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaru Kumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazha Varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazha Varudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akka Reetakka Inna Venumkka?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka Reetakka Inna Venumkka?"/>
</div>
3 <div class="lyrico-lyrics-wrapper">Beer!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beer!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girls Night Outuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girls Night Outuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boys Get Outuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys Get Outuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Party Pana Plan Aa Poten Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Pana Plan Aa Poten Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Stock Vaangi Veyka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stock Vaangi Veyka"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss Aanadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss Aanadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Lock Aagi Poren Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lock Aagi Poren Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippa Kaasu Konjam Extra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Kaasu Konjam Extra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuduthu Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduthu Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Black La Vanga Poren Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Black La Vanga Poren Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mixing Ku Soda Edhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mixing Ku Soda Edhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruku Water Packet Namaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku Water Packet Namaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Use And Throw Va Vangi Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Use And Throw Va Vangi Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">First Cheers Aa Podu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Cheers Aa Podu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patunu Bottle Mandaila Thati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patunu Bottle Mandaila Thati"/>
</div>
<div class="lyrico-lyrics-wrapper">Open Pananum Adhu Dhan Yuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open Pananum Adhu Dhan Yuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illana Moodi Suthidum Suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illana Moodi Suthidum Suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension Namakku Yeanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Namakku Yeanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha Tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Nillu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nillu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Nillu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nillu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka Ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambunga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambunga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Varatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Varatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Enga Route Veruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Enga Route Veruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Vidu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Vidu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha Tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Nillu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nillu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Nillu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Nillu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambunga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambunga Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Varatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Varatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeruh Biryani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeruh Biryani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapida Variya Nee?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida Variya Nee?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightellam Aattam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellam Aattam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Ammuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Ammuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeruh Biryani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeruh Biryani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapida Variya Nee?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida Variya Nee?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightellam Aattam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellam Aattam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Ammuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Ammuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Advice Panna Nee Yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Advice Panna Nee Yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaya Mattum Nee Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaya Mattum Nee Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Vida Ponnukku Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Vida Ponnukku Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Adhigam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Adhigam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeruh Biryani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeruh Biryani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapida Variya Nee?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida Variya Nee?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightellam Aattam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellam Aattam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Ammuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Ammuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeruh Biryani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeruh Biryani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapida Variya Nee?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida Variya Nee?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightellam Aattam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellam Aattam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Ammuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Ammuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti Kadayiladhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Kadayiladhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti Thiruttu Jolly Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti Thiruttu Jolly Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Night ECR Road Fulla Gaali Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night ECR Road Fulla Gaali Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weekend Kaathu Vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weekend Kaathu Vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Beach House Ea Vaanguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beach House Ea Vaanguraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetoda Watchman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetoda Watchman"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly Ea Adhula Thoonguraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Ea Adhula Thoonguraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaagamunnu Vandhuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagamunnu Vandhuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkaru Moor Thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkaru Moor Thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasathodu Soru Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathodu Soru Podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundari Akka Enga Aatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Akka Enga Aatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bridge Keela Tea Kudippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bridge Keela Tea Kudippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension Aana Dhum Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension Aana Dhum Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kealviah Than Evanum Keata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kealviah Than Evanum Keata"/>
</div>
<div class="lyrico-lyrics-wrapper">Keattavana Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keattavana Adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasangala Sight Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasangala Sight Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">College Na Cut Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Na Cut Adippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakkum Sidisheayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakkum Sidisheayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Seandhu Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Seandhu Adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Advice Panna Nee Yaaru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Advice Panna Nee Yaaru?"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaya Mattum Nee Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaya Mattum Nee Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Vida Ponnukku Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Vida Ponnukku Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Adhigam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Adhigam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeruh Biryani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeruh Biryani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapida Variya Nee?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida Variya Nee?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightellam Aattam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellam Aattam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Ammuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Ammuni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeruh Biryani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeruh Biryani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapida Variya Nee?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapida Variya Nee?"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightellam Aattam Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightellam Aattam Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Ammuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Ammuni"/>
</div>
</pre>
