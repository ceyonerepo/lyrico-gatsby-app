---
title: "nee illai naanum song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Kabilan"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Nee Illai Naanum"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iW_SQFuS5r8"
type: "Love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooo nee illa naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo nee illa naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal illa maidhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal illa maidhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen vaazha thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vaazha thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbe, en anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbe, en anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo nee illa naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo nee illa naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal illa maidhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal illa maidhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen vaazha thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vaazha thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol en anbe, en anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol en anbe, en anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paadalin vaasam neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paadalin vaasam neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">En thedalin dhooram neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thedalin dhooram neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigalin regai noolanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigalin regai noolanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidumurai naal aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidumurai naal aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru paravaiyaanen vinnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paravaiyaanen vinnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee parakkave illai ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee parakkave illai ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">En siragugal tholaithena unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En siragugal tholaithena unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne kanneere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne kanneere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhal osai kuyil osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhal osai kuyil osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamal naan puyal osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamal naan puyal osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhal osai kuyil osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhal osai kuyil osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illamal naan puyal osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illamal naan puyal osai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo nee illa naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo nee illa naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal illa maidhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal illa maidhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen vaazha thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vaazha thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol en anbe, en anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol en anbe, en anbe"/>
</div>
</pre>
