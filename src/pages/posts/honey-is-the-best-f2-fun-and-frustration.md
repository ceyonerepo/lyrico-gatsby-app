---
title: "honey is the best song lyrics"
album: "F2 Fun and Frustration"
artist: "Devi Sri Prasad"
lyricist: "Shreemani"
director: "Anil Ravipudi"
path: "/albums/f2-fun-and-frustration-lyrics"
song: "Honey is The Best"
image: ../../images/albumart/f2-fun-and-frustration.jpg
date: 2019-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/r_RifU0GahE"
type: "happy"
singers:
  - Hariharasudhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ayya baboi ayya baboi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya baboi ayya baboi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthakaalam nelapaina ledhoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthakaalam nelapaina ledhoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya baboi ayya baboi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya baboi ayya baboi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthakaalam evaru choodaledhoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthakaalam evaru choodaledhoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarigame nuvvugani paadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigame nuvvugani paadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana kokilaina gaayamavvada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana kokilaina gaayamavvada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakadime nuvvugaani aadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakadime nuvvugaani aadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatya mayuri aina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatya mayuri aina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru vadhali paaripodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru vadhali paaripodha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatakaina paatakina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatakaina paatakina"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Eela kotti vennuthattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eela kotti vennuthattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cricket bat ye nuvvu padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cricket bat ye nuvvu padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Wicket virige aata needhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wicket virige aata needhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Foot ball game ye nuvvu aadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foot ball game ye nuvvu aadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Self goal vesey style needhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Self goal vesey style needhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Running race lo ninnu dimpithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Running race lo ninnu dimpithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Winning number marchuthavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Winning number marchuthavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadaranganike ninnu pampithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadaranganike ninnu pampithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokkudu billa aate anukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkudu billa aate anukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokki thokki thaata theesthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokki thokki thaata theesthave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Game kaina fame kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game kaina fame kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Eela kotti vennuthattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eela kotti vennuthattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvve gani kunche padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve gani kunche padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommalu kanchelu thenchukuntaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommalu kanchelu thenchukuntaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve gani muggu pedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve gani muggu pedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalu nelaku raalipothaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalu nelaku raalipothaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve gani dhanda guchhithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve gani dhanda guchhithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvulu neeku dhandamedathaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvulu neeku dhandamedathaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve gani garita thippithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve gani garita thippithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo unna cook ni choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo unna cook ni choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Cooker ye nee kallu mokkuthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cooker ye nee kallu mokkuthade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Intikaina vantakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intikaina vantakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
<div class="lyrico-lyrics-wrapper">Eela kotti vennuthattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eela kotti vennuthattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Honey is the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honey is the best"/>
</div>
</pre>
