---
title: "sooriyarum sooriyanum ondrai song lyrics"
album: "Sandakozhi 2"
artist: "Yuvan Sankar Raja"
lyricist: "Brinda Sarathy"
director: "N. Linguswamy"
path: "/albums/sandakozhi-2-lyrics"
song: "Sooriyarum Sooriyanum Ondrai"
image: ../../images/albumart/sandakozhi-2.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r527F3DYDZ0"
type: "Mass"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sooriyarum Sooriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyarum Sooriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Sernthu Vaaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu Vaaraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhai Paazhai Ellorkkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai Paazhai Ellorkkum Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanathukkum Veerathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanathukkum Veerathukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvam Pola Vaaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam Pola Vaaraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Potrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Potrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallal Gunam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallal Gunam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Varthai Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Varthai Sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Usura Pola Kaapaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Usura Pola Kaapaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kottah Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kottah Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Oorey Thanda Maattaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Oorey Thanda Maattaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu Nikkum Aiyanaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Nikkum Aiyanaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amasamaaga Rendu Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amasamaaga Rendu Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Thookki Vangaum Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thookki Vangaum Ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippothillai Eppothum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippothillai Eppothum Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suriyarum Suriyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyarum Suriyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Sernthu Vaaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Sernthu Vaaraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhai Paazhai Ellorkkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai Paazhai Ellorkkum Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanathukkum Veerathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanathukkum Veerathukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvam Pola Vaaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam Pola Vaaraga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Potrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Potrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallal Gunam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallal Gunam Thaan"/>
</div>
</pre>
