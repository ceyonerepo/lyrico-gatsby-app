---
title: "penne penne song lyrics"
album: "Sabhaapathy"
artist: "Sam C.S"
lyricist: "Sam C.S"
director: "R. Srinivasa Rao"
path: "/albums/sabhaapathy-lyrics"
song: "Penne Penne"
image: ../../images/albumart/sabhaapathy.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ClRq3OabeVY"
type: "sad"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Penne Penne Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Penne Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollama Kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollama Kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Nee Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Nee Illama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Penne Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Penne Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollama Kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollama Kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Nee Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Nee Illama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thaan Sollama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thaan Sollama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonthe Pone Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonthe Pone Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ennoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ennoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Maari Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Maari Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Illama Ennaaven Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Illama Ennaaven Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kaalam Kadhal Serthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kaalam Kadhal Serthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kai Kooda Aasai Korthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kai Kooda Aasai Korthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Sollama Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Sollama Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavichene Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavichene Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Ennulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Ennulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasichen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasichen Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnil Thaan Sikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Thaan Sikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethanjene Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethanjene Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Podhunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Podhunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alainjene Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alainjene Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Penne Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Penne Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollama Kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollama Kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Nee Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Nee Illama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thaan Sollama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thaan Sollama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonthe Pone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonthe Pone"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Vaa Ennoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Vaa Ennoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ver Yaarodum Thonatha Or
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Yaarodum Thonatha Or"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmela Yen Thonudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmela Yen Thonudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vehnumnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vehnumnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Kodi Undanaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Kodi Undanaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anba Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anba Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollida Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollida Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthaigal Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthaigal Illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ullamtha Kollamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullamtha Kollamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panthadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthadura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Kannadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Kannadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naan Paarkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naan Paarkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarvai Paarkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarvai Paarkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal Veesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal Veesura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enna Nizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enna Nizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Thonavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Thonavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Jenmam Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jenmam Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Inga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne Penne Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Penne Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollama Kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollama Kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Nee Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Nee Illama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Kanne Enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Kanne Enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thaan Sollama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thaan Sollama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonthe Pone Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonthe Pone Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ennoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ennoda"/>
</div>
</pre>
