---
title: 'nagarodi song lyrics'
album: 'Jail'
artist: 'G V Prakash Kumar'
lyricist: 'Arivu'
director: 'G Vasanthabalan'
path: '/albums/jail-song-lyrics'
song: 'Nagarodi'
image: ../../images/albumart/jail.jpg
date: 2021-12-09
lang: tamil
singers: 
- G.V. Prakash Kumar
- Arivu
- Ananya Bhat
youtubeLink: "https://www.youtube.com/embed/amXG0RgtGIc"
type: 'mass'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Oora vittu veera vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora vittu veera vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram pogurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram pogurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru yaaro aagurom bhoomiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru yaaro aagurom bhoomiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathoyathu niyaayam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathoyathu niyaayam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thangurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thangurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pooraa engurom saamiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pooraa engurom saamiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koora melaa kaakam sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koora melaa kaakam sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram enga cheri vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram enga cheri vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora vittu oraam pooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora vittu oraam pooka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooram ingu naadu naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooram ingu naadu naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu nindru pochu manne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu nindru pochu manne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarodi naanga nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarodi naanga nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegudhooram pona nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegudhooram pona nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakanthome ooru edam thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakanthome ooru edam thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarodi naanga nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarodi naanga nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyora kara nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyora kara nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthome naanga kadaveedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthome naanga kadaveedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu meedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondha manne nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha manne nee "/>
</div>
<div class="lyrico-lyrics-wrapper">illai endru solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai endru solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoora thalla oorai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora thalla oorai "/>
</div>
<div class="lyrico-lyrics-wrapper">veettu oram sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veettu oram sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaithu vaithathaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaithu vaithathaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">nammai indha jailile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammai indha jailile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulaithezhunthu vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulaithezhunthu vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">unmaiyai urakka solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmaiyai urakka solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilathai ezhathu pulam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilathai ezhathu pulam "/>
</div>
<div class="lyrico-lyrics-wrapper">peyarntha sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyarntha sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaimuraikkum irukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimuraikkum irukkum "/>
</div>
<div class="lyrico-lyrics-wrapper">velikodutha ranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velikodutha ranam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu karattai udaithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu karattai udaithu "/>
</div>
<div class="lyrico-lyrics-wrapper">nagar amaitha inaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nagar amaitha inaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathi viratta uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathi viratta uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">thavikka dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikka dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikkoodam pooka annaadam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikkoodam pooka annaadam "/>
</div>
<div class="lyrico-lyrics-wrapper">moochu vaanguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu vaanguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru poyoi sera ennoda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru poyoi sera ennoda "/>
</div>
<div class="lyrico-lyrics-wrapper">moochu enguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu enguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda thundamaaga en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda thundamaaga en "/>
</div>
<div class="lyrico-lyrics-wrapper">veraai kooru poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraai kooru poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Veandhavargal vaazha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veandhavargal vaazha "/>
</div>
<div class="lyrico-lyrics-wrapper">nammai vandheri aakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammai vandheri aakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarodi naanga nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarodi naanga nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyora kaara nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyora kaara nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthome naanga kadaveedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthome naanga kadaveedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu meedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiveesamma kaiveesu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiveesamma kaiveesu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyendha thaan vechaane naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyendha thaan vechaane naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettaakkani thaan ezhaikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaakkani thaan ezhaikku "/>
</div>
<div class="lyrico-lyrics-wrapper">needhi thappaakki nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhi thappaakki nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sertha pinna sorgatha seraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sertha pinna sorgatha seraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukkittom mannittu vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukkittom mannittu vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethaalume vittuthaan tharaatha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethaalume vittuthaan tharaatha nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottai ellaam un vervaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottai ellaam un vervaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapidalaa nee moonu vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapidalaa nee moonu vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Retham motham urinjukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retham motham urinjukkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppaiyena veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppaiyena veesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irakkathaan urakkuthaan edhuvume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkathaan urakkuthaan edhuvume"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai illai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai illai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakkithaan odukkittan odachttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakkithaan odukkittan odachttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirai illai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirai illai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthai neruthaal bhalathai thirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthai neruthaal bhalathai thirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udamai parithaal urimai muzhakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udamai parithaal urimai muzhakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttil adaithaal velidam kilappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttil adaithaal velidam kilappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum idam thaan namakku uyire moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum idam thaan namakku uyire moochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora vittu veera vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora vittu veera vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram pogurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram pogurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeru yaaro aagurom bhoomiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeru yaaro aagurom bhoomiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathoyathu niyaayam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathoyathu niyaayam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thangurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thangurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pooraa engurom saamiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pooraa engurom saamiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarodi naanga nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarodi naanga nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyora kara nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyora kara nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthome naanga kadaveedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthome naanga kadaveedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu meedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarodi naanga nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarodi naanga nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegudhooram pona nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegudhooram pona nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakanthome ooru edam thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakanthome ooru edam thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarodi naanga nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarodi naanga nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyora kaara nagarodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyora kaara nagarodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthome naanga kadaveedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthome naanga kadaveedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethu meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethu meedhi"/>
</div>
</pre>