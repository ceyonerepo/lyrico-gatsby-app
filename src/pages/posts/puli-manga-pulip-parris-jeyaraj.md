---
title: "puli manga pulip song lyrics"
album: "Parris Jeyaraj"
artist: "Santhosh Narayanan"
lyricist: "Rokesh - Asal Kolar"
director: "Johnson K"
path: "/albums/parris-jeyaraj-lyrics"
song: "Puli Manga Pulip"
image: ../../images/albumart/parris-jeyaraj.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RgxiR0_LXUs"
type: "Celebration"
singers:
  - Gaana Muthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyaiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyaiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maame valipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daavu daavu daguluthan maavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavu daavu daguluthan maavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouser darra kizhiyura novu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouser darra kizhiyura novu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarum thaadi aavathu shave-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarum thaadi aavathu shave-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Vena enakku kaadhal enum novu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena enakku kaadhal enum novu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyyayo!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyayo!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kikkiri mikkiri velai kamchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kikkiri mikkiri velai kamchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aava nenachom un husband-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aava nenachom un husband-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Husband-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Husband-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kavithaiya darra kizhchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kavithaiya darra kizhchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki erinjittada dustbin-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki erinjittada dustbin-ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dustbin-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dustbin-ku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Passport-ah thaan naan unnai putchinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Passport-ah thaan naan unnai putchinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kadhal oorukku tour povom thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kadhal oorukku tour povom thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jackpot-ah unnai naan kizhichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jackpot-ah unnai naan kizhichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha lottery chit-eh tholachitten ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha lottery chit-eh tholachitten ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fast-ah paranthutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fast-ah paranthutten"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nasthava maranthutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nasthava maranthutten"/>
</div>
<div class="lyrico-lyrics-wrapper">Daavadichi kavuthutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daavadichi kavuthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaavala kavunthutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaavala kavunthutten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chichi kavuchi chikkama aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chichi kavuchi chikkama aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorthen maama love-ula jaichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorthen maama love-ula jaichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachi nichi navunthutha chechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachi nichi navunthutha chechi"/>
</div>
<div class="lyrico-lyrics-wrapper">Steel-u body slim-aayi pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steel-u body slim-aayi pochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenga venumna…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenga venumna…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennamaram yerunom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennamaram yerunom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kochinu pona…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kochinu pona…"/>
</div>
<div class="lyrico-lyrics-wrapper">Convince pannanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Convince pannanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhulama theriyamaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhulama theriyamaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Relationship start pannen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Relationship start pannen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu naala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu naala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuthunu irunthen raajavadaasana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthunu irunthen raajavadaasana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kathara vuttalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kathara vuttalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai azhuva vuttalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai azhuva vuttalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya ozhuva vuttalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya ozhuva vuttalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniya olara vuttalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya olara vuttalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya bongu aayitalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya bongu aayitalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai saavadichalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai saavadichalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parris Jeyaraj heart patient aaittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parris Jeyaraj heart patient aaittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapatha baby ma kitta vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapatha baby ma kitta vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart ellaam ondinjuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart ellaam ondinjuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththunu vaadi fevi stick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththunu vaadi fevi stick"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthturen naan pei pudchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthturen naan pei pudchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumttu podi manthiruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumttu podi manthiruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal thaan di darr aaitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal thaan di darr aaitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizha vizhnthu mannaitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizha vizhnthu mannaitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darr aaitten… darr aaitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darr aaitten… darr aaitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Barr aaitten.. barr aitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barr aaitten.. barr aitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Darr aaitten… darr aaitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darr aaitten… darr aaitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Barr aaitten.. barr aitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barr aaitten.. barr aitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyaiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyaiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi maame valip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maame valip"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli manga pulip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli manga pulip"/>
</div>
<div class="lyrico-lyrics-wrapper">Podraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaa…haiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaa…haiyooo"/>
</div>
</pre>
