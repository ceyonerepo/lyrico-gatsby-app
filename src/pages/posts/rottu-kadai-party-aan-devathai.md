---
title: "rottu kadai party song lyrics"
album: "Aan Devathai"
artist: "Ghibran"
lyricist: "Viveka"
director: "Thamira"
path: "/albums/aan-devathai-lyrics"
song: "Rottu Kadai Party"
image: ../../images/albumart/aan-devathai.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YElRB7-lpak"
type: "happy"
singers:
  - Gold Devaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ah rottu kadai party idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah rottu kadai party idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu thunna Gethu gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu thunna Gethu gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varutha kari treatu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varutha kari treatu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai valikka Kathu kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai valikka Kathu kathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kalakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kalakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathi matum lighta pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi matum lighta pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Louda oru songa pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Louda oru songa pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poradiya kalakurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradiya kalakurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarty illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarty illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rottanda chair ah pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rottanda chair ah pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu mela kaalu pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu mela kaalu pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaulathaa order pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaulathaa order pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karandi ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karandi ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorandi eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorandi eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila soththa parimaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila soththa parimaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Piece-eh illa briyaaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piece-eh illa briyaaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhka iruntha sema bore-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka iruntha sema bore-uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah rottu kadai party idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah rottu kadai party idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu thunna Gethu gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu thunna Gethu gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varutha kari treatu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varutha kari treatu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai valikka Kathu kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai valikka Kathu kathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idly kattiyacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idly kattiyacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavanda vadai sutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavanda vadai sutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilavikku kai thattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilavikku kai thattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bike-ah thaan otta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike-ah thaan otta"/>
</div>
<div class="lyrico-lyrics-wrapper">Murugan thaan nam settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugan thaan nam settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaka kadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaka kadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichu thunnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichu thunnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaru kattu murukku ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaru kattu murukku ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakai adakkum aalu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakai adakkum aalu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda inam illa thoora ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda inam illa thoora ottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathi matum lighta pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi matum lighta pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Louda oru songa pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Louda oru songa pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poradiya kalakurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradiya kalakurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarty illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarty illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rottanda chair ah pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rottanda chair ah pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu mela kaalu pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu mela kaalu pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaulathaa order pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaulathaa order pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karandi ethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karandi ethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorandi eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorandi eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila soththa parimaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila soththa parimaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Piece-eh illa briyaaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piece-eh illa briyaaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhka iruntha sema bore-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhka iruntha sema bore-uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah rottu kadai party idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah rottu kadai party idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu thunna Gethu gethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu thunna Gethu gethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varutha kari treatu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varutha kari treatu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai valikka Kathu kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai valikka Kathu kathu"/>
</div>
</pre>
