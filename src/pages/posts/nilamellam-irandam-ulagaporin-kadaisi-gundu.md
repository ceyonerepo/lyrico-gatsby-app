---
title: "nilamellam song lyrics"
album: "Irandam Ulagaporin Kadaisi Gundu"
artist: "Tenma"
lyricist: "Umadevi"
director: "Athiyan Athirai"
path: "/albums/irandam-ulagaporin-kadaisi-gundu-lyrics"
song: "Nilamellam"
image: ../../images/albumart/irandam-ulagaporin-kadaisi-gundu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9ffHtJ_Sv7k"
type: "happy"
singers:
  - K. Chitrasenan
  - Arivu
  - Gana Muthu
  - Ezhumalai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amma Aelanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Aelanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Aelanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Aelanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethiyadieeeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethiyadieeeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Ezhai Janaaaaaaaaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Ezhai Janaaaaaaaaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Ezhai Janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Ezhai Janam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Boomiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Boomiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhuthadi Vaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhuthadi Vaduthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaanaaaimmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaanaaaimmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaanaaaimmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaanaaaimmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamellaam Engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamellaam Engal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervayil Mulaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervayil Mulaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamellaam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamellaam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethanai Valikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethanai Valikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam Engal Vaasalil Palikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Engal Vaasalil Palikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Pazhagaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Pazhagaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaanaaaimmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaanaaaimmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalellam Ungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalellam Ungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhuppinil Uraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhuppinil Uraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalellaam Uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalellaam Uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppinil Karaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppinil Karaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Samamellaam Engal Kooliyil Nigazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samamellaam Engal Kooliyil Nigazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Pazhagaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Pazhagaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaanaaaimmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaanaaaimmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Endral Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Endral Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennada Kidaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada Kidaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edu Endral Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Endral Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Matrangal Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matrangal Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkatum Oru Puratchiyae Vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkatum Oru Puratchiyae Vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Thayangaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Thayangaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaanaaaimmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaanaaaimmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhalil Vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhalil Vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyin Thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin Thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippin Palan Izhanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippin Palan Izhanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathungi Viraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungi Viraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanathin Pulipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanathin Pulipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimai Kalam Adaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimai Kalam Adaivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamellaam Engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamellaam Engal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervayil Mulaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervayil Mulaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamellaam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamellaam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethanai Valikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethanai Valikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam Engal Vaasalil Palikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Engal Vaasalil Palikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Pazhagaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Pazhagaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangalae Neendiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangalae Neendiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnulagam Thondriduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnulagam Thondriduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhargalin Thagangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhargalin Thagangalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Senneeril Theernthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senneeril Theernthiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Inge Endha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Inge Endha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaththukum Pothuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaththukum Pothuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Ena Oru Uyirukku Kuraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ena Oru Uyirukku Kuraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippilae Ingu Sivakkatum Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippilae Ingu Sivakkatum Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagorae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malayin Vazhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayin Vazhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaiyum Nadhipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaiyum Nadhipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal Pala Vendrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal Pala Vendrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraiyil Adaiyum Nathiyin Nilaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraiyil Adaiyum Nathiyin Nilaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyilae Nindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyilae Nindrom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Barangalae Neelumendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barangalae Neelumendral"/>
</div>
<div class="lyrico-lyrics-wrapper">Boogambam Nernthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boogambam Nernthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathilae Paambu Vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathilae Paambu Vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi Kooda Seeridumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi Kooda Seeridumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaiyellam Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyellam Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyatum Malaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyatum Malaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimayil Enna Kidaithidum Perithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimayil Enna Kidaithidum Perithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyatum Nathi Kadaludan Thunivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyatum Nathi Kadaludan Thunivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyorae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhalil Vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhalil Vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyin Thuliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin Thuliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippin Palan Izhanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippin Palan Izhanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathungi Virayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungi Virayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanathin Pulipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanathin Pulipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimai Kalam Adaivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimai Kalam Adaivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathirukkum Ayuthangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirukkum Ayuthangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkalena Poothiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalena Poothiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaiyilae Ranuvangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaiyilae Ranuvangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Samarangal Veesiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarangal Veesiduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thath Thathath Tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thath Thathath Tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thana Naa Thana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thana Naa Thana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaanaaaimmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaanaaaimmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelviyidum Senchenayidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelviyidum Senchenayidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanayidum Aatchi Koottam Thorkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanayidum Aatchi Koottam Thorkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhi Mara Maligaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhi Mara Maligaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhan Kudi Yeriyathaai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhan Kudi Yeriyathaai Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamellaam Engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamellaam Engal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervayil Mulaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervayil Mulaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamellaam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamellaam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethanai Valikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethanai Valikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam Engal Vaasalil Palikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Engal Vaasalil Palikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Pazhagaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Pazhagaatho"/>
</div>
</pre>
