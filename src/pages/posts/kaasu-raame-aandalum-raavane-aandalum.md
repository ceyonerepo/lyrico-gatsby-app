---
title: "kaasu song lyrics"
album: "Raame Aandalum Raavane Aandalum"
artist: "Krish"
lyricist: "Ve. Madhankumar"
director: "Arisil Moorthy"
path: "/albums/raame-aandalum-raavane-aandalum-lyrics"
song: "Kaasu"
image: ../../images/albumart/raame-aandalum-raavane-aandalum.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zQ9uMQJxHGc"
type: "sad"
singers:
  - Bamba Bakya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaasu aalura naadaachu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu aalura naadaachu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam karaiyudhu mudiva kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam karaiyudhu mudiva kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu marundhukku maladaachu bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu marundhukku maladaachu bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vazhivarum usura seami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vazhivarum usura seami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu patharudhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu patharudhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Panamae maathuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panamae maathuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Marathiya pazhagikada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marathiya pazhagikada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam varadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam varadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manusa vanmamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusa vanmamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaradhu vazhkaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaradhu vazhkaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Porupa pozhachukada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porupa pozhachukada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu nillathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu nillathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu aalura naadaachu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu aalura naadaachu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam karaiyudhu mudiva kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam karaiyudhu mudiva kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu marundhukku maladaachu bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu marundhukku maladaachu bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vazhivarum usura seami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vazhivarum usura seami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaavi karuppathaan arasiyala senjanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavi karuppathaan arasiyala senjanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meda pottuthaan llamanasa keduthanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meda pottuthaan llamanasa keduthanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum puriyama thenarithaan ninnanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum puriyama thenarithaan ninnanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodagam idhayum vechu kaasakithaan thinnanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodagam idhayum vechu kaasakithaan thinnanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppakki ravaigalukku usura vittaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppakki ravaigalukku usura vittaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuliyum bayapadala naanga ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuliyum bayapadala naanga ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju nelakolanju irukudhu thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju nelakolanju irukudhu thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelama maarum vara porukanum thaanae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelama maarum vara porukanum thaanae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chairu pottu aatchi nadathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chairu pottu aatchi nadathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu yaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu yaaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga seru sagathi aana road ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga seru sagathi aana road ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatha paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatha paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaya pesaya kolanju pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaya pesaya kolanju pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Searu naangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Searu naangada"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga thasaya kilichu kasakki pizhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga thasaya kilichu kasakki pizhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga yaaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga yaaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mala kazhivedukka manasana vittomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala kazhivedukka manasana vittomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavula edampidkka perum paadu pattomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavula edampidkka perum paadu pattomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Attapuzhuva angam muzhukka uriyura arasiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attapuzhuva angam muzhukka uriyura arasiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Anala suttuthaan erichudu theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anala suttuthaan erichudu theva illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu aalura naadaachu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu aalura naadaachu saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam karaiyudhu mudiva kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam karaiyudhu mudiva kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu marundhukku maladaachu bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu marundhukku maladaachu bhoomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vazhivarum usura seami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vazhivarum usura seami"/>
</div>
</pre>
