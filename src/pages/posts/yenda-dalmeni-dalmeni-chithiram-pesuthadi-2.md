---
title: "yenda dalmeni dalmeni song lyrics"
album: "Chithiram Pesuthadi 2"
artist: "Sajan Madhav"
lyricist: "Pandiselvam"
director: "Rajan Madhav"
path: "/albums/chithiram-pesuthadi-2-lyrics"
song: "Yenda Dalmeni Dalmeni"
image: ../../images/albumart/chithiram-pesuthadi-2.jpg
date: 2019-02-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FAkDDAWMuK8"
type: "happy"
singers:
  - Gaana Bala
  - Naven Madhav
  - feat. Dwayne Bravo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aasai Kudi Sarakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Kudi Sarakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhil Mayakkam Konjam Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhil Mayakkam Konjam Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam Konjam Iruppathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkam Konjam Iruppathaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veen Vaathangal Pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veen Vaathangal Pirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Peyara Kedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Peyara Kedukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Koranga Pola Nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Koranga Pola Nadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhuthu Vidinji Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthu Vidinji Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Botha Thelinji Vidum Onakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Botha Thelinji Vidum Onakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dalmeni Dalmeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dalmeni Dalmeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Kaattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Kaattudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Dullaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Dullaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-Ukkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-Ukkum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Mattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Mattudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Yethudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Yethudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammala Pinnadi Alaiyavida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammala Pinnadi Alaiyavida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkal Naattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkal Naattudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koovamum Aarudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovamum Aarudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulichida Mudiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulichida Mudiyumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum Bodhathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalum Bodhathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku Pol Aagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku Pol Aagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhayil Naan Paduththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhayil Naan Paduththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhimara Nyanamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhimara Nyanamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tubelight-U Tubelight-Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tubelight-U Tubelight-Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyum Theriyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyum Theriyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dalmeni Dalmeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dalmeni Dalmeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu Kaattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu Kaattudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Dullaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Dullaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-Ukkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-Ukkum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Mattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Mattudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Yethudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Yethudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammala Pinnadi Alaiyavida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammala Pinnadi Alaiyavida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkal Naattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkal Naattudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothys-La Podava Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothys-La Podava Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poththi Kaadhal Valarthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poththi Kaadhal Valarthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsung Nokia-Nu Phone-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsung Nokia-Nu Phone-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi Koduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Koduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inox Sathiyamnu Onnaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inox Sathiyamnu Onnaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love You Solli Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You Solli Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattipidichi Urundaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattipidichi Urundaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnukellaam Kaadhal Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnukellaam Kaadhal Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Paas-U Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Paas-U Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkaama Poittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkaama Poittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Life-U Pass Thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Life-U Pass Thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kaadhala Paththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kaadhala Paththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaa Sonnaa Pichiduven Pichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaa Sonnaa Pichiduven Pichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Dhevadhaiya Paathirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Dhevadhaiya Paathirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Puriyum Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Puriyum Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-U Onnum Pannidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-U Onnum Pannidaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Empty-Aana Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Empty-Aana Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kuppa Serum Thottiyinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kuppa Serum Thottiyinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhandhaikkumthaan Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhandhaikkumthaan Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Pada Pattaampoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Pada Pattaampoochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukulla Parakkanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla Parakkanumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham Rendum Boomiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham Rendum Boomiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaamalae Nadakkanumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaamalae Nadakkanumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anushkaa Pola Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anushkaa Pola Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu Onnu Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu Onnu Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Hanumaaraa Irundhuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hanumaaraa Irundhuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-U Enga Thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-U Enga Thonum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarakku Bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku Bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerndhu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerndhu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Pola Varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Pola Varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kadhalaala Sadhikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kadhalaala Sadhikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-U Pannidu Maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-U Pannidu Maamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Coffee Day-Il Thodaraum Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee Day-Il Thodaraum Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tasmac-Ula Mudiyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tasmac-Ula Mudiyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasirundha Mattum Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasirundha Mattum Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Kanna Thorakkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Kanna Thorakkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anushkaavae Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anushkaavae Vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku Onnum Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Onnum Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Half Adichu Paduthukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Half Adichu Paduthukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Aruvaa Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Aruvaa Thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavulaava Varuvaa Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavulaava Varuvaa Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yen Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelavaanam Neelavaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelavaanam Neelavaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Vaanam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Vaanam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Thaan Da Saamithandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Thaan Da Saamithandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevadhaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevadhaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraa Vaaraam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraa Vaaraam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu Naalum Odippogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Naalum Odippogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Boomyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Boomyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Mattum Nilayaai Nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Mattum Nilayaai Nikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-A Pola Tasmac-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-A Pola Tasmac-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppavumae Mass-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppavumae Mass-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothuvaa Das Solren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuvaa Das Solren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukonga Boss-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukonga Boss-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendaiyum Kattruthandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendaiyum Kattruthandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Dhevadaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Dhevadaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukku Munnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukku Munnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Dummy Piece
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Dummy Piece"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Yen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Yen Da"/>
</div>
</pre>
