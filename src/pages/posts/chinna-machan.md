---
title: "chinna machan song lyrics"
album: "charlie chaplin 2"
artist: "amrish"
lyricist: "chella thangaiah"
director: "Sakthi chithambaram"
path: "/albums/charlie-chaplin-2-song-lyrics"
song: "chinna machan"
image: ../../images/albumart/charlie-chaplin-2.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mCCW6m0Cczk"
type: "love"
singers:
  - senthil ganesh
  - rajalakshmi
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eh Eh Eh Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Eh Eh Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevatha Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevatha Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sevatha Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sevatha Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Sollu Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Eh Eh Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Eh Eh Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevaththa Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevaththa Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sevaththa Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sevaththa Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sollu Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukulla Ungala Yesuraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Ungala Yesuraanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Onnu Rendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Onnu Rendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Pesuraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Pesuraanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla Ungala Yesuraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla Ungala Yesuraanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Onnu Rendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Onnu Rendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Pesuraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Pesuraanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh Eh Eh Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh Eh Eh Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevaththa Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevaththa Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Sevatha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Sevatha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sollu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sollu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Yaaru Enna Pesunaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Yaaru Enna Pesunaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Ennavellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ennavellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Yesunaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Yesunaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Yaaru Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Yaaru Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pesunaangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pesunaangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Ennavellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ennavellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Yesunaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Yesunaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Ah Ah Ahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Ah Ah Ahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Azhagu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Azhagu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Solu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandipatti Anitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandipatti Anitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Adhu Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Adhu Pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Pechu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Pechu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Ah Ah Ahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Ah Ah Ahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Azhagu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Azhagu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Solu Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solu Machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandipatti Anitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandipatti Anitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Anjaan Claasu Padikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Anjaan Claasu Padikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Nalla Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nalla Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellphonula Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellphonula Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Siriki Mava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Siriki Mava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiya Yaaru Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiya Yaaru Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaata Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaata Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuttha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuttha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Karuththapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Karuththapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyama Solluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama Solluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Sathiya Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Sathiya Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Thozhan Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Thozhan Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesa Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesa Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasa Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasa Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aasai Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasai Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Sollu Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Sollu Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongayilum Devi Nu Sollureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongayilum Devi Nu Sollureenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Yengavechchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Yengavechchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Machaan Kollureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Machaan Kollureenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sollu Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sollu Machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumaari Dheviyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumaari Dheviyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachiruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachiruken"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kalyaana Thedhiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kalyaana Thedhiyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurichiruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurichiruken"/>
</div>
</pre>
