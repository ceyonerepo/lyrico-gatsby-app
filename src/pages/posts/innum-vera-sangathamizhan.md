---
title: "innum vera song lyrics"
album: "Sangathamizhan"
artist: "	Vivek - Mervin"
lyricist: "Viveka"
director: "Vijay Chandar"
path: "/albums/sangathamizhan-lyrics"
song: "Innum Vera"
image: ../../images/albumart/sangathamizhan.jpg
date: 2019-11-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/W_xllu6nqG4"
type: "happy"
singers:
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vera Yedhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Yedhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theva Illaiyae Yae Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illaiyae Yae Yae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Enga Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Enga Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku Eedu Yethum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku Eedu Yethum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Kaainthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Kaainthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Saainthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Saainthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaana Nesam Theivathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaana Nesam Theivathila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Vera Enna Venumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vera Enna Venumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Indha Vaazhka Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Indha Vaazhka Podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Vera Enna Venumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vera Enna Venumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Vera Enna Venumae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vera Enna Venumae Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Indha Vaazhka Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Indha Vaazhka Podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamthorumae Ingu Thiruvizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamthorumae Ingu Thiruvizhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiththikkuthae Thaen Thuligalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiththikkuthae Thaen Thuligalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh Saainthida Pala Uravugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh Saainthida Pala Uravugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayil Thogaiyaai Pala Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayil Thogaiyaai Pala Kanavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Ulagae Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ulagae Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirthaal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirthaal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkkum Veeramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkkum Veeramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Uyirai Kooda Thaanam Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Uyirai Kooda Thaanam Thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkum Vamsamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum Vamsamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajaali Pola Nee Pogum Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaali Pola Nee Pogum Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaadha Kannum Paarkkumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaadha Kannum Paarkkumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajathi Raajan Nee Pesum Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajathi Raajan Nee Pesum Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukkae Vedham Aagumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkae Vedham Aagumaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Vera Enna Venumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vera Enna Venumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Thanthu Kaakkum Vamsamae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Thanthu Kaakkum Vamsamae Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaa Maganum Aiyaa Amsamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa Maganum Aiyaa Amsamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Vera Enna Venumae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vera Enna Venumae Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Indha Vaazhka Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Indha Vaazhka Podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli Thanthu Kaakkum Vamsamae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Thanthu Kaakkum Vamsamae Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaa Maganum Aiyaa Amsamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa Maganum Aiyaa Amsamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Vera Enna Venumae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Vera Enna Venumae Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Indha Vaazhka Podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Indha Vaazhka Podhumae"/>
</div>
</pre>
