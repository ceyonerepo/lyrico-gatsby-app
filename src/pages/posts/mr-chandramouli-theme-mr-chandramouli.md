---
title: "mr chandramouli theme song lyrics"
album: "Mr Chandramouli"
artist: "Sam CS"
lyricist: "Vithya Damodharan"
director: "Thiru"
path: "/albums/mr-chandramouli-lyrics"
song: "Mr Chandramouli Theme"
image: ../../images/albumart/mr-chandramouli.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZrCmfGbJECA"
type: "theme"
singers:
  - Sam CS
  - Brindha Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Siru naali podhaathingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru naali podhaathingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaai ulava podhaathingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaai ulava podhaathingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru aazhi podhaathingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru aazhi podhaathingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivai nirappa podhaathingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivai nirappa podhaathingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhu dhu dhuu dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhu dhu dhuu dhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhu dhu dhuu dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhu dhu dhuu dhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhu dhu dhuu dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhu dhu dhuu dhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaigira nodigalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigira nodigalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam neela yedho seiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam neela yedho seiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Purinthidaa uravaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinthidaa uravaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neettithida paalam seiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neettithida paalam seiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naraigal mulaikkum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraigal mulaikkum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal suzhalum vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal suzhalum vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum kalainthu poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum kalainthu poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijangalin uravum meela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijangalin uravum meela"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna solvadho indha uravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solvadho indha uravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenbadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenbadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhu dhu dhuu dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhu dhu dhuu dhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhuththu thuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhuththu thuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhu dhu dhuu dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhu dhu dhuu dhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuu dhu dhu dhuu dhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuu dhu dhu dhuu dhu"/>
</div>
</pre>
