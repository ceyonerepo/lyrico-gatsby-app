---
title: "it's over song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Elan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "It's Over"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kay9Y-5vkQ0"
type: "sad"
singers:
  - Shweta Pandit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaenae kaanal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenae kaanal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen therinthu marainthu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen therinthu marainthu ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae kaadhal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae kaadhal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen kalainthu karainthu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen kalainthu karainthu ponaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala vaanavil theetiya pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vaanavil theetiya pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila vannangal theernthathu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila vannangal theernthathu yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala vinmeen minniya naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala vinmeen minniya naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila megangal soozhnthathu yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila megangal soozhnthathu yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaenae kaanal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenae kaanal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen therinthu marainthu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen therinthu marainthu ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae kaadhal nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae kaadhal nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yen kalainthu karainthu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yen kalainthu karainthu ponaai"/>
</div>
</pre>
