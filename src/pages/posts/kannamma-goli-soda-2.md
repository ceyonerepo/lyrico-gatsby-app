---
title: "kannamma song lyrics"
album: "Goli Soda 2"
artist: "Achu Rajamani"
lyricist: "Viveka"
director: "Vijay Milton"
path: "/albums/goli-soda-2-lyrics"
song: "Kannamma"
image: ../../images/albumart/goli-soda-2.jpg
date: 2018-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6g5nKB1esSA"
type: "melody"
singers:
  - Prativa
  - Rohan Prakash
  - Khadhijah Sahreef
  - Nishitha Menon
  - Arrya Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae yaaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan paarvai thotta udan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan paarvai thotta udan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigal karaindhu pogudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal karaindhu pogudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae yaaradi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae yaaradi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kuralai ketkayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kuralai ketkayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhil urudhi kooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil urudhi kooduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethaiyum thanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaiyum thanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaai irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaai irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marundhai irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marundhai irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugae irundhaal anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugae irundhaal anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayangal aatruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayangal aatruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Udainthu ponavan orunginaiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udainthu ponavan orunginaiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai pasiyil nenjin isaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai pasiyil nenjin isaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal visaiyil anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal visaiyil anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum ennai kaavuvenye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum ennai kaavuvenye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan jeevanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan jeevanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan oruyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan oruyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae podhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En garvam naan kalaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En garvam naan kalaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu kaalil veezhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu kaalil veezhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae…vendumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae…vendumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee muththam ondru ida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee muththam ondru ida"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugi kuzhandhai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugi kuzhandhai aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaindhu ponadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhu ponadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidandha ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidandha ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil eduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil eduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir nee koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir nee koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayai kidaithai anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayai kidaithai anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un anbai kooravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anbai kooravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadandhae kaalangal maraiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadandhae kaalangal maraiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigal kuraiya manamo karaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal kuraiya manamo karaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil nee niraiya anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil nee niraiya anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum thevai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum thevai illaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae mayiliragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae mayiliragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai soodi kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai soodi kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyara uyara uyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyara uyara uyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam yengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae maatruyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae maatruyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum patrikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum patrikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhen parandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhen parandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham neengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham neengudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silurthu pogiren thooralgalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silurthu pogiren thooralgalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ezhanthu melae ezhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam ezhanthu melae ezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasiyum viliyil anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasiyum viliyil anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi nandri kooruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi nandri kooruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu tholvigal pagirindhavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu tholvigal pagirindhavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan vetrigal muzhudhum unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan vetrigal muzhudhum unadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyum kadappen anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyum kadappen anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thandha vaanamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thandha vaanamadi"/>
</div>
</pre>
