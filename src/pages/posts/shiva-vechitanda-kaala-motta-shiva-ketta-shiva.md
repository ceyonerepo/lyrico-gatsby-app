---
title: "shiva vechitanda kaala song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Shiva Vechitanda Kaala"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uuunRdP_jCI"
type: "mass"
singers:
  -	Shankar Mahadevan
  - Amresh Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Shiva vachittaandaa kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva vachittaandaa kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">City parakka podhu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City parakka podhu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattaadha nee vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattaadha nee vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku oothiduvaan paala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku oothiduvaan paala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Immaamperiya naattula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaamperiya naattula"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunai evanum kaattula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunai evanum kaattula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyaa sonnaa God-u daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyaa sonnaa God-u daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari illaangaatti rod-u daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari illaangaatti rod-u daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daangiri dongiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daangiri dongiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta vandhu ottikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta vandhu ottikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuttuthaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuttuthaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei oola udaar uttukkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei oola udaar uttukkinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaikkaagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkaagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oottulaiyae kundhikkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oottulaiyae kundhikkinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalu varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalu varaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madras city poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madras city poora"/>
</div>
<div class="lyrico-lyrics-wrapper">En atrocity thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En atrocity thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara paathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan konnuduven smoothaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan konnuduven smoothaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madras city poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madras city poora"/>
</div>
<div class="lyrico-lyrics-wrapper">En atrocity thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En atrocity thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara paathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan konnuduven smoothaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan konnuduven smoothaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gola kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gola kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge ge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge ge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge ge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge ge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge sema geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge sema geththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhirukkum annen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukkum annen"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumai nira kannen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumai nira kannen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga kurai theerkka vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kurai theerkka vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannan indha mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannan indha mannan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roottu pudhu roottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roottu pudhu roottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Joottu vilaiyaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joottu vilaiyaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli adipen allikodupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli adipen allikodupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaichadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaichadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Senju mudipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senju mudipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaikellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaikellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu nee taataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu nee taataa"/>
</div>
<div class="lyrico-lyrics-wrapper">Single paatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single paatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda kottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda kottaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Late-u pannaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Late-u pannaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthukko notta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthukko notta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illannu sollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illannu sollaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaraachum keattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaraachum keattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjavaraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjavaraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthupazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthupazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduthaathaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthaathaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gola kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gola kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge ge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge ge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge ge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge ge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge sema geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge sema geththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daangiri dongiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daangiri dongiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta vandhu ottikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta vandhu ottikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuttuthaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuttuthaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei oola udaar uttukkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei oola udaar uttukkinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaikkaagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkaagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oottulaiyae kundhikkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oottulaiyae kundhikkinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalu varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalu varaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhirukkum annen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukkum annen"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumai nira kannen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumai nira kannen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga kurai theerkka vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kurai theerkka vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannan indha mannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannan indha mannan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaara thappatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaara thappatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei kizhinji thonganundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kizhinji thonganundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyila kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyila kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padakkoodaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padakkoodaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi navuthanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi navuthanumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhai panama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhai panama"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaathu panamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaathu panamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Smellu vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smellu vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kandu pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kandu pidipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkar kanakkil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkar kanakkil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedulla aalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedulla aalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkivandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkivandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththathaan tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththathaan tharuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaalithaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalithaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae namakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae namakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Gujjaal thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujjaal thaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daangiri dongiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daangiri dongiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta vandhu ottikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta vandhu ottikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuttuthaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuttuthaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei oola udaar uttukkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei oola udaar uttukkinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaikkaagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaikkaagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oottulaiyae kundhikkinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oottulaiyae kundhikkinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalu varaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalu varaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madras city poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madras city poora"/>
</div>
<div class="lyrico-lyrics-wrapper">En atrocity thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En atrocity thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara paathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan konnuduven smoothaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan konnuduven smoothaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madras city poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madras city poora"/>
</div>
<div class="lyrico-lyrics-wrapper">En atrocity thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En atrocity thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara paathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan konnuduven smoothaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan konnuduven smoothaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaa erangi kuthu saamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa erangi kuthu saamiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gu gu gu gu gola kuththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gu gu gu gu gola kuththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge ge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge ge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge ge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge ge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ge ge ge ge sema geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ge ge ge ge sema geththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi kuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi kuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shiva romba gethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shiva romba gethudaa"/>
</div>
</pre>
