---
title: "kannamma song lyrics"
album: "Ispade Rajavum Idhaya Raniyum"
artist: "Sam C. S."
lyricist: "Sam C. S."
director: "Ranjit Jeyakodi"
path: "/albums/idpade-rajavum-idhaya-raniyum-lyrics"
song: "Kannamma"
image: ../../images/albumart/ispade-rajavum-idhaya-raniyum.jpg
date: 2019-03-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_fTN72GPgIU"
type: "love"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannamma Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva Paaradi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Paaradi Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Pesina Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pesina Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva Paaradi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Paaradi Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Pesina Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pesina Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkulla Puthithaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkulla Puthithaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kadhal Nee Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kadhal Nee Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaagum Vali Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaagum Vali Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamthaane Nee Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamthaane Nee Sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokkaatha Sokkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkaatha Sokkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Paathum Sikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Paathum Sikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Yen Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Yen Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Thikkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Thikkaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ara Paarva Nee Paaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara Paarva Nee Paaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Nenja Kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nenja Kollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalkooda Nadakkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalkooda Nadakkindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamkooda Nee Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamkooda Nee Thantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva Paaradi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Paaradi Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Pesina Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pesina Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva Paaradi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Paaradi Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Pesina Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pesina Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mounam Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mounam Pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhikooda Azhagadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhikooda Azhagadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayul Neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayul Neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Pothum Varudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Pothum Varudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Uthadin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Uthadin"/>
</div>
<div class="lyrico-lyrics-wrapper">Orangal Maraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orangal Maraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Udaitheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Udaitheri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enthan Nilavadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enthan Nilavadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Vaanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vaanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraikkindra Azhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraikkindra Azhagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Uyirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Uyirai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swasam Thoduthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swasam Thoduthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooradi Vanthu Kooradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooradi Vanthu Kooradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilave Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilave Malare"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaviye Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaviye Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiya Oliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiya Oliye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilave Malare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilave Malare"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaviye Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaviye Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Nee Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamma Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasil Ninaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasil Ninaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarva Paaradi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Paaradi Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Pesina Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Pesina Kanne"/>
</div>
</pre>
