---
title: "yaayum song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Yaayum"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UH0haOwkf3Q"
type: "love"
singers:
  - Naresh Iyer
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaayum Gnayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaayum Gnayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaraagiyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaraagiyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthaiyum Nunthaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthaiyum Nunthaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemmurai Kezhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmurai Kezhir"/>
</div>
<div class="lyrico-lyrics-wrapper">Chempula Peyalneer Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chempula Peyalneer Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjamtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanthanavae Kalanthanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanthanavae Kalanthanavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Lae Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Le Lae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Le Lae Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakadha Nerathil Paakkuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakadha Nerathil Paakkuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulungi Kulungi Sirikiradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulungi Kulungi Sirikiradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Munnadi Pesuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Munnadi Pesuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vasapatta Arikuriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vasapatta Arikuriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Lae Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Le Lae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Le Lae Ae"/>
</div>
 & <div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Lae Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Le Lae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Le Lae Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Turutu Turutu Tuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turutu Turutu Tuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaayum Gnayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaayum Gnayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaraagiyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaraagiyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthaiyum Nunthaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthaiyum Nunthaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemmurai Kezhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmurai Kezhir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Chempula Peyalneer Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chempula Peyalneer Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjamtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanthanavae Kalanthanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanthanavae Kalanthanavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmmmmm"/>
</div>
 & <div class="lyrico-lyrics-wrapper">Hmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadha Theriyama Nadakkuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadha Theriyama Nadakkuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragae Illama Parakkuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragae Illama Parakkuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Nenapil Irukkuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Nenapil Irukkuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vasapatta Arikuriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vasapatta Arikuriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Lae Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Le Lae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Le Lae Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Raathookkam Illama Vizhikiradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathookkam Illama Vizhikiradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Purandu Purandu Padukkuradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purandu Purandu Padukkuradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Kalanji Muzhikiradhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Kalanji Muzhikiradhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vasapatta Arikuriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vasapatta Arikuriyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Lae Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Lae Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Le Le Le Le Le Lae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Le Le Le Lae Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaayum Gnayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaayum Gnayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaraagiyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaraagiyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthaiyum Nunthaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthaiyum Nunthaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemmurai Kezhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemmurai Kezhir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Chempula Peyalneer Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chempula Peyalneer Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjamtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanthanavae Kalanthanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanthanavae Kalanthanavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 & <div class="lyrico-lyrics-wrapper">Hmm Ahaahmm Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Ahaahmm Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Ahaahmm Ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Ahaahmm Ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Ahaahmm Ahaahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Ahaahmm Ahaahaaa"/>
</div>
</pre>
