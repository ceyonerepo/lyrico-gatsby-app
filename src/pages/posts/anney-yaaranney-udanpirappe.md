---
title: "anney yaaranney song lyrics"
album: "Udanpirappe"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Era. Saravanan"
path: "/albums/udanpirappe-lyrics"
song: "Anney Yaaranney"
image: ../../images/albumart/udanpirappe.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rmgjG_pqMuQ"
type: "affection"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anne Yaranne Mannula Onattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Yaranne Mannula Onattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Kandale Nanjila Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kandale Nanjila Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anne Yaranne Anne Yaranne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Yaranne Anne Yaranne"/>
</div>
<div class="lyrico-lyrics-wrapper">Anne Yaranne Mannula Onattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Yaranne Mannula Onattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Kandale Nanjila Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kandale Nanjila Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Kothidum Un Pasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kothidum Un Pasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kula Samiya Minchatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kula Samiya Minchatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Vadura Pothellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vadura Pothellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Neerena Thoovatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Neerena Thoovatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Azham Unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Azham Unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Endru Sollida Sollida Ullamum Pongatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Endru Sollida Sollida Ullamum Pongatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Suththum Bhoomi Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suththum Bhoomi Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikku Samy Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikku Samy Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Suththum Bhoomi Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suththum Bhoomi Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikku Samy Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikku Samy Enga Annan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anne Yaranne Mannula Onattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Yaranne Mannula Onattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Kandale Nanjila Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kandale Nanjila Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan Enum Varththai Nangezhuththu Vetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Enum Varththai Nangezhuththu Vetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullavarai Nanum Solla Athu Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullavarai Nanum Solla Athu Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththe Nee Parka Ullirukkum Sogam Odathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththe Nee Parka Ullirukkum Sogam Odathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seththe Ponalum Un Kuralil Vazhve Neelatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththe Ponalum Un Kuralil Vazhve Neelatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnayum Kasaiyum Virumbum Bhoomiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnayum Kasaiyum Virumbum Bhoomiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanin Moochu Than Thangaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanin Moochu Than Thangaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollivida Senruvidum Sanjalangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollivida Senruvidum Sanjalangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Suththum Bhoomi Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suththum Bhoomi Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikku Samy Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikku Samy Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Suththum Bhoomi Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suththum Bhoomi Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikku Samy Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikku Samy Enga Annan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Kothidum Un Pasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Kothidum Un Pasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kula Samiya Minchatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kula Samiya Minchatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Vadura Pothellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Vadura Pothellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Neerena Thoovatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Neerena Thoovatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Azham Unthan Anbe Endru Sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Azham Unthan Anbe Endru Sollida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollida Ullamum Pongatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollida Ullamum Pongatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Suththum Bhoomi Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suththum Bhoomi Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikku Samy Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikku Samy Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Suththum Bhoomi Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Suththum Bhoomi Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu Thikku Samy Enga Annan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu Thikku Samy Enga Annan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anne Yaranne Mannula Onattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Yaranne Mannula Onattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Kandale Nanjila Kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kandale Nanjila Kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anne Yaranne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anne Yaranne"/>
</div>
</pre>
