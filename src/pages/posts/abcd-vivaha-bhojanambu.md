---
title: "abcd song lyrics"
album: "Vivaha Bhojanambu"
artist: "AniVee"
lyricist: "Kittu Vissapragada"
director: "Ram Abbaraju"
path: "/albums/vivaha-bhojanambu-lyrics"
song: "ABCD"
image: ../../images/albumart/vivaha-bhojanambu.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/d_9yAlVZlK0"
type: "happy"
singers:
  - Inno Genga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Artharatri vela nidramatthulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artharatri vela nidramatthulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahma devudochi engilshulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahma devudochi engilshulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovestory okati inku pennutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovestory okati inku pennutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasinaduro raasinaduro...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasinaduro raasinaduro..."/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti gundelona flash mob la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti gundelona flash mob la"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujji brainulona traffic jamula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujji brainulona traffic jamula"/>
</div>
<div class="lyrico-lyrics-wrapper">Present tensulona tensiona vachhera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Present tensulona tensiona vachhera"/>
</div>
<div class="lyrico-lyrics-wrapper">Love kottera love puttera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love kottera love puttera"/>
</div>
<div class="lyrico-lyrics-wrapper">O pilla chhudaradhe jimmik jimmik chesthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O pilla chhudaradhe jimmik jimmik chesthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kosam wait chesi tumbuku tumbuku ayithunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kosam wait chesi tumbuku tumbuku ayithunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu piluvu chalu ilta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu piluvu chalu ilta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu hajaru ayitha gitla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu hajaru ayitha gitla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee meeda esina dasthini clear cheyi rasthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee meeda esina dasthini clear cheyi rasthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey kuch hoya you and me match hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey kuch hoya you and me match hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Beauty chusi chachipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beauty chusi chachipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Something something pichi hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Something something pichi hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vastha chengu chenga my heart vaibavanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vastha chengu chenga my heart vaibavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chusthu sambaranga ayitha naa samiranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chusthu sambaranga ayitha naa samiranga"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD nuvu naa jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD nuvu naa jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD your my jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD your my jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD nuvu naa jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD nuvu naa jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD your my jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD your my jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Confidence leni antenna pai kaki nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confidence leni antenna pai kaki nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Coloru fullugunna Tv lona bomma nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coloru fullugunna Tv lona bomma nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tune chesukunte duradarshan thagilinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune chesukunte duradarshan thagilinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudirinadhi jattu kudirinadhi jattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudirinadhi jattu kudirinadhi jattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha mandhi mundu pakka pakka nilabadettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha mandhi mundu pakka pakka nilabadettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallalona kottha flood light veliginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona kottha flood light veliginattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gukka thippakunda love story cheppukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gukka thippakunda love story cheppukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruguthanu ottu dachalenu guttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruguthanu ottu dachalenu guttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets go uppu kappurambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets go uppu kappurambu"/>
</div>
<div class="lyrico-lyrics-wrapper">padyame maripoyinattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padyame maripoyinattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalu chalu antu biginchu currentu kottha rhymes cheppana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalu chalu antu biginchu currentu kottha rhymes cheppana"/>
</div>
<div class="lyrico-lyrics-wrapper">Twinkle twinkle little star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Twinkle twinkle little star"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu pippudu super star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu pippudu super star"/>
</div>
<div class="lyrico-lyrics-wrapper">I am the light your my moon light
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am the light your my moon light"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi pichigundhi rachalethundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi pichigundhi rachalethundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu musukunte nenala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu musukunte nenala"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchi guchi nannu nidra leputhandi mid night lona nee kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchi guchi nannu nidra leputhandi mid night lona nee kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Brandedsitu heart beatu nannu drill master laaga cheppaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brandedsitu heart beatu nannu drill master laaga cheppaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chess board laga free bosslaga combinationa ettugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chess board laga free bosslaga combinationa ettugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD nuvu naa jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD nuvu naa jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD your my jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD your my jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD nuvu naa jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD nuvu naa jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">ABCD your my jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ABCD your my jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey baby naathone raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey baby naathone raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu leka i will go crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu leka i will go crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey baby naathone raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey baby naathone raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu leka i will go crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu leka i will go crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey baby naathone raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey baby naathone raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu leka i will go crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu leka i will go crazy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey baby naathone raave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey baby naathone raave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu leka i will go crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu leka i will go crazy"/>
</div>
</pre>
