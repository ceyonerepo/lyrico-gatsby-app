---
title: "mattu mattu nee song lyrics"
album: "Thamizhan"
artist: "D. Imman"
lyricist: "Vairamuthu"
director: "Majith"
path: "/albums/thamizhan-lyrics"
song: "Mattu Mattu Nee"
image: ../../images/albumart/thamizhan.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nU6QMmjrXGI"
type: "love"
singers:
  - D. Imman
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maatu Maatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu Maatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maattennu Sollapora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maattennu Sollapora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattava Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattava Maattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottu Pootu Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu Pootu Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottukaetha Saavi Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottukaetha Saavi Naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottave Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottave Poottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalil Nee Mudichu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalil Nee Mudichu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piragu Nee Aattam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piragu Nee Aattam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pazhutha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pazhutha Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sulukki Kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulukki Kidakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Thottu Sulukku Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Thottu Sulukku Edu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatu Maatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu Maatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maattennu Sollapora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maattennu Sollapora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattava Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattava Maattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatu Maatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu Maatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maattennu Sollapora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maattennu Sollapora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattava Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattava Maattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottu Pootu Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu Pootu Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottukaetha Saavi Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottukaetha Saavi Naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottave Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottave Poottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalil Nee Mudichu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalil Nee Mudichu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piragu Nee Aattam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piragu Nee Aattam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pazhutha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pazhutha Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sulukki Kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulukki Kidakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Thottu Sulukku Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Thottu Sulukku Edu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thanna Thanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thanna Thanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Thaanaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Thaanaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaa Un Kannu Pattu Poovaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Un Kannu Pattu Poovaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottu En Nethi Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu En Nethi Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattu Un Nenjil Pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Un Nenjil Pattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otto-nnu Ottum Madhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otto-nnu Ottum Madhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thanna Thanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thanna Thanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Thaanaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Thaanaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulnaakkil Kothum Kili Needhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulnaakkil Kothum Kili Needhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Mazhaiyadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Mazhaiyadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Thee Thee Pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Thee Thee Pidikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaikaari Kanna Vettina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikaari Kanna Vettina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Manmadhanin Raajiyathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manmadhanin Raajiyathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannu Thinnu Valarndhavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu Thinnu Valarndhavana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Puvi Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Puvi Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thimira Kaattirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thimira Kaattirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Yetha Varanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Yetha Varanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Indhirana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Indhirana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Methai Mela Vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Methai Mela Vithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaattum Vithagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattum Vithagana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatu Maatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu Maatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maattennu Sollapora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maattennu Sollapora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattava Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattava Maattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottu Pootu Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu Pootu Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottukaetha Saavi Naanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottukaetha Saavi Naanthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottave Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottave Poottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thanna Thanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thanna Thanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Thaanaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Thaanaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Ul Ulang Kaiyil Thaenaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Ul Ulang Kaiyil Thaenaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munna Kai Thottadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna Kai Thottadhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manna Un Kaiyil Sikkina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna Un Kaiyil Sikkina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thanna Thanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thanna Thanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Thaanaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Thaanaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Popol Kuthum Mulla Needhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Popol Kuthum Mulla Needhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epper Un Nirathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epper Un Nirathukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epper Un Tharathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epper Un Tharathukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pithaagi Puthi Kettenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithaagi Puthi Kettenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sikkanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sikkanathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutham Tharum Laganathil Porandhavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Tharum Laganathil Porandhavana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mutham Onnudhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Onnudhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mogam Theerka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mogam Theerka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Mutham Podum Machine ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mutham Podum Machine ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaama Vaithiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Vaithiyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maedu Pallam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maedu Pallam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodu Podum Oviyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodu Podum Oviyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatu Maatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatu Maatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Maatu Maatu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Maatu Maatu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Maatu Maatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Maatu Maatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maattennu Sollapora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maattennu Sollapora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattava Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattava Maattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottu Pootu Indha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu Pootu Indha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottukaetha Saavi Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottukaetha Saavi Neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poottaiyaa Poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottaiyaa Poottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalil Nee Mudichu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalil Nee Mudichu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piragu Nee Aattam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piragu Nee Aattam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pazhutha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pazhutha Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sulukki Kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulukki Kidakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Thottu Sulukku Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Thottu Sulukku Edu"/>
</div>
</pre>
