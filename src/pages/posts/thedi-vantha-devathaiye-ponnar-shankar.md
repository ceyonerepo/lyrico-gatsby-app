---
title: "thedi vantha devathaiye song lyrics"
album: "Ponnar Shankar"
artist: "Ilaiyaraaja"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/ponnar-shankar-lyrics"
song: "Thedi Vantha Devathaiye"
image: ../../images/albumart/ponnar-shankar.jpg
date: 2011-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HUS_XQ1wC7Y"
type: "love"
singers:
  - Shreya Ghoshal
  - Kunal Ganjawala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adi thaedi vandha dhevadhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaedi vandha dhevadhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan rasikkum poongodhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan rasikkum poongodhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga thaanae kaathirukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga thaanae kaathirukku "/>
</div>
<div class="lyrico-lyrics-wrapper">endhan kaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan kaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi poova pola kitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi poova pola kitta "/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi aiyae aiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi aiyae aiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka pattu pogaadhadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka pattu pogaadhadi "/>
</div>
<div class="lyrico-lyrics-wrapper">paiya paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paiya paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjukkulla nee thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjukkulla nee thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">rekka neiya neiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka neiya neiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On tholil saanju kattikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On tholil saanju kattikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara maela muttikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara maela muttikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalap paathu vekka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalap paathu vekka "/>
</div>
<div class="lyrico-lyrics-wrapper">patten vekka patten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patten vekka patten"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan aalillaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aalillaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">neram paathu kattikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram paathu kattikkitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayasu pulla nee yendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu pulla nee yendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Em manasukkulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em manasukkulla "/>
</div>
<div class="lyrico-lyrics-wrapper">theriyaama kaiya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyaama kaiya vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On vekkatha nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On vekkatha nee "/>
</div>
<div class="lyrico-lyrics-wrapper">engaeyo vuttu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaeyo vuttu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae vittadhanaal nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae vittadhanaal nee "/>
</div>
<div class="lyrico-lyrics-wrapper">enna nashta patta aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nashta patta aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannatha neeyae kavanil vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannatha neeyae kavanil vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichu vecha kalavaaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichu vecha kalavaaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parigaaram ennaannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parigaaram ennaannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee badhil onnu sollendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee badhil onnu sollendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellaandi amman koyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaandi amman koyil "/>
</div>
<div class="lyrico-lyrics-wrapper">poyi neeyae kaelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyi neeyae kaelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enna thandhaa ponna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enna thandhaa ponna "/>
</div>
<div class="lyrico-lyrics-wrapper">pola vaangi kollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola vaangi kollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi thaedi vandha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaedi vandha "/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaedi vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaedi vandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On tholil saanju kattikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On tholil saanju kattikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara maela muttikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara maela muttikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalap paathu vekka patten vekka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalap paathu vekka patten vekka "/>
</div>
<div class="lyrico-lyrics-wrapper">pattenNaan aalillaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattenNaan aalillaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">neram paathu kattikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram paathu kattikkitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaleduthu poraada pogum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaleduthu poraada pogum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaela pola un kanna kanden kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaela pola un kanna kanden kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukkellaam nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkellaam nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval thandhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval thandhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku mattum nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku mattum nee "/>
</div>
<div class="lyrico-lyrics-wrapper">yendaa kaadhal thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendaa kaadhal thandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pulli maanu kanni vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanu kanni vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkidumaa singam singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkidumaa singam singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattukkum naattukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattukkum naattukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaavae naan thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaavae naan thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae yaana puli singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae yaana puli singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan solla kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan solla kekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aanaa indha kanni manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aanaa indha kanni manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maala kekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maala kekkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi thaedi vandha dhevadhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thaedi vandha dhevadhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan rasikkum poongodhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan rasikkum poongodhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga thaanae kaathirukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga thaanae kaathirukku "/>
</div>
<div class="lyrico-lyrics-wrapper">endhan kaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan kaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi poova pola kitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi poova pola kitta "/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi aiyae aiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi aiyae aiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka pattu pogaadhadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka pattu pogaadhadi "/>
</div>
<div class="lyrico-lyrics-wrapper">paiya paiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paiya paiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjukkulla nee thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjukkulla nee thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">rekka neiya neiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rekka neiya neiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On tholil saanju kattikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On tholil saanju kattikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Paara maela muttikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paara maela muttikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalap paathu vekka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalap paathu vekka "/>
</div>
<div class="lyrico-lyrics-wrapper">patten vekka patten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patten vekka patten"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan aalillaadha neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aalillaadha neram "/>
</div>
<div class="lyrico-lyrics-wrapper">paathu kattikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu kattikkitten"/>
</div>
</pre>
