import React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import {Link} from 'gatsby';

const NotFoundPage = () => (
  <Layout isHomePage={true}>
   <Seo title="404: Not found" />
      <div className="lyrico-404-contents">
        <h1>The page you are looking for is not available</h1>
        <Link to="/"> Tack me back to home </Link>
      </div>
  </Layout>
)

export default NotFoundPage
