import React from "react"
import Home from '../components/Home';
import Layout from '../components/layout';
import Seo from '../components/seo';

const IndexPage = (props) => (
  <Layout>
   <Seo title='Movie song lyrics' 
         keywords={['Song lyrics', 'Tamil movie song lyrics', 'Tamil song lyrics', 'Tamil movie lyrics', 'Movie song lyrics']}
    ></Seo>
    <Home />  
  </Layout>
)



export default IndexPage;
