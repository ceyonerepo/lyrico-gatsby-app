import React from 'react';

function wrapText(context, text, x, y, maxWidth, lineHeight) {
    var words = text.split(' ');
    var line = '';

    for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
        }
        else {
            line = testLine;
        }
    }
    context.fillText(line, x, y);
}

function createStatusImage() {
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    const image = document.getElementById('imageElem');
    image.addEventListener('load', e => {
        ctx.drawImage(image, 0, 0, ctx.canvas.width, ctx.canvas.height);
        let imgData = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
        let pixels = imgData.data;
        for (var i = 0; i < pixels.length; i += 4) {

            let lightness = parseInt((pixels[i] + pixels[i + 1] + pixels[i + 2]) / 10);

            pixels[i] = lightness;
            pixels[i + 1] = lightness;
            pixels[i + 2] = lightness;
        }
        ctx.putImageData(imgData, 0, 0);
        ctx.font = "30px Comic Sans MS";
        ctx.fillStyle = "white";
        ctx.textAlign = "center";
        var text = `Neeratum Naerathil Yen Annai Aaginraai
                Vaalatum Naerathil Yen Pillai Aaginrai
                `
        ctx.font = "18px Comic Sans MS"
        ctx.fillText('https://lyrico.net', canvas.width - 100, canvas.height - 20);

        ctx.font = "15px Comic Sans MS"
        wrapText(ctx, text, canvas.width / 2, canvas.height / 10, 370, 40)
        var updatedImage = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream")
        window.location.href = updatedImage;
    });
}

export default createStatusImage;