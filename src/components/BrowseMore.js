import * as React from "react"
import { Link } from "gatsby"
import { Button } from "reactstrap"

export default function BrowseMore({ totalCount, lang }) {
  function renderBrowserButtons() {
    return (
      <div className="load-more-browser">
        <div className="load-more-content">
          <div>
            <span className="song-count">{totalCount}</span>
            <br />
          </div>
          <div className="load-more-songs">
            More <span>{lang}</span> Songs
          </div>
        </div>
        <div>
          <Link to={"/language/" + lang}>
            <Button className="browse-more-btn outline">Browse All</Button>
          </Link>
        </div>
      </div>
    )
  }

  return <>{renderBrowserButtons()}</>
}
