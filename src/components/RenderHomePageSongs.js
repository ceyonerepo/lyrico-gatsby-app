import React from "react"
import { Badge } from "reactstrap"
import { Link } from "gatsby"
import slugify from "react-slugify"
import { getImage, GatsbyImage } from "gatsby-plugin-image"

const RenderSong = ({ node, index }) => {
  
  var slug = "/songs/" + slugify(node.frontmatter.title)
  var singers = node.frontmatter.singers.map(singer => {
     return (
       <Badge color="lyrico text-white lyrico-song-artist-badge" key={singer}>
         {singer}
       </Badge>
     )
   })
  return (
    <div className="home-page-song">
      <Link to={slug}>
        <div className="wrapper">
          <div className="img-container">
            <GatsbyImage
              image={getImage(node.frontmatter.image)}
              alt={node.frontmatter.title}
            ></GatsbyImage>
          </div>
          <div className="lyrico-artists">
            <div className="lyrico-song-title">
              Song : {node.frontmatter.song}
            </div>
            <div className="lyrico-song-movie">
              Movie : {node.frontmatter.album}
            </div>
            <div className="lyrico-singers">Singers : {singers}</div>
          </div>
        </div>
      </Link>
    </div>
  )
}

export default RenderSong
