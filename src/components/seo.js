/**
 *Seocomponent that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"
import { getSrc } from "gatsby-plugin-image"

function getCapitalized(str) {
  var strList = str.split(" ")
  var updatedStr = ""
  strList.forEach(el => {
    updatedStr += " "
    updatedStr += el.slice(0, 1).toUpperCase() + el.slice(1)
  })

  return updatedStr
}

function SEO({ description, title, image, url, keywords }) {
  console.log(getSrc(image));
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
            logo
            url
          }
        }
      }
    `
  )
  const metaDescription = description || site.siteMetadata.description
  const metaImage =
    (image && "https://lyrico.net" + getSrc(image)) || site.siteMetadata.logo
  const metaURL = url || site.siteMetadata.url
  const metaTitle =
    getCapitalized(title) || getCapitalized(site.siteMetadata.title)

  return (
    <Helmet>
      {/* OpenGraph tags */}
      <meta property="og:url" content={metaURL} />
      <meta property="og:type" content="article" />
      <meta property="og:title" content={metaTitle} />
      <meta property="og:description" content={metaDescription} />
      <meta property="og:image" content={metaImage} />
      <meta property="og:site_name" content="Lyrico" />

      {/* Google adsense account information*/}
      <script
        async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
      ></script>
      {/* General tags */}
      <meta name="description" content={metaDescription} />
      <meta name="image" content={metaImage} />
      <meta name="keywords" content={keywords} />
      <title>{metaTitle}</title>

      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={metaTitle} />
      <meta name="twitter:description" content={metaDescription} />
      <meta name="twitter:image" content={metaImage} />
    </Helmet>
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
}

export default SEO
