import React from "react"
import { Row, Col } from "reactstrap"
import TypeSongs from "./TypeSongs"
import AdSense from "react-adsense"
import { isMobile } from "react-device-detect"

function renderLeftSidebar() {
  return (
    <Col xs="12" md="3" className="lyrico-sidebar lyrico-sidebar-left">
      <div className="lyrico-google-adds">
        <AdSense.Google
          client="ca-pub-7536246717624121"
          slot="8887877565"
          style={{ display: "block" }}
          format="auto"
          responsive="true"
        />
      </div>
      <div className="lyrico-google-adds">
        <AdSense.Google
          client="ca-pub-7536246717624121"
          slot="8887877565"
          style={{ display: "block" }}
          format="auto"
          responsive="true"
        />
      </div>
      <div className="lyrico-google-adds">
        <AdSense.Google
          client="ca-pub-7536246717624121"
          slot="8887877565"
          style={{ display: "block" }}
          format="auto"
          responsive="true"
        />
      </div>
    </Col>
  )
}

const SongTab = props => {
  return (
    <div>
      <Row className="lyrico-songpage-details-container">
        <Col xs="12" md="7" className="lyrico-song-lyrics">
          <div
            dangerouslySetInnerHTML={{
              __html: props.lyrics.english,
            }}
          ></div>
        </Col>
        <Col xs="12" md="4" className="lyrico-sidebar">
          <TypeSongs type={props.typeData}></TypeSongs>
          <div className="lyrico-google-adds">
            <AdSense.Google
              client="ca-pub-7536246717624121"
              slot="8887877565"
              style={{ display: "block" }}
              format="auto"
              responsive="true"
            />
            <div className="lyrico-google-adds">
              <AdSense.Google
                client="ca-pub-7536246717624121"
                slot="8887877565"
                style={{ display: "block" }}
                format="auto"
                responsive="true"
              />
            </div>
            <div className="lyrico-google-adds">
              <AdSense.Google
                client="ca-pub-7536246717624121"
                slot="8887877565"
                style={{ display: "block" }}
                format="auto"
                responsive="true"
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default SongTab
