import React from "react"
import PropTypes from "prop-types"
import "./layout.css"
import "../../src/styles/index.scss"
import Navbar from "../components/Navbar"
import FooterSection from "../components/FooterPage"

const Layout = ({ children }) => {
  return (
    <>
      <Navbar />
      <main>{children}</main>
      <FooterSection />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
