import React from 'react';
import { Badge } from 'reactstrap';
import {Link} from 'gatsby';
import slugify from 'react-slugify';

const RenderSong = (props) => {
  var slug = '/songs/' + slugify(props.data.node.frontmatter.title);
  var singers = props.data.node.frontmatter.singers.map(singer => {
      return <Badge color="lyrico text-white lyrico-song-artist-badge" key={singer}>{singer}</Badge>
  });
  return (
    <div className="lyrico-song-detail-wrapper">
      <Link to={slug}>{props.data.node.frontmatter.title}</Link>
      <div className="lyrico-song-artists-details">
        <div className="lyrico-artists">
          <div>
            <span>Lyricist : </span><Badge color="lyrico text-white lyrico-song-lyricis-badge">{props.data.node.frontmatter.lyricist}</Badge>
          </div>
          <div>
            <span>singers : </span>{singers}
          </div>
        </div>
      </div>
    </div>
  );
};

export default RenderSong;