import React from 'react';
import {Link} from 'gatsby';

function FooterPage() {
    return (
        <footer>
            <div className="lyrico-footer-container">
               <div className="lyrico-copyright-content">
                   Copyright © 2020 Lyrico.net
               </div>
               <Link to="/privacy-policy">
                   <div>Privacy Policy</div>
               </Link>
            </div>
        </footer>
    )
}


export default FooterPage;