import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { FaFacebookSquare } from 'react-icons/fa';
import { FaTwitterSquare } from 'react-icons/fa';
import { FaWhatsappSquare } from 'react-icons/fa';
import { FaPrint } from 'react-icons/fa';
// import { trackCustomEvent } from 'gatsby-plugin-google-analytics';
import {
    FacebookShareButton,
    TwitterShareButton,
    WhatsappShareButton
} from 'react-share';

function Share(props) {
    const { site } = useStaticQuery(
        graphql`
            query {
                site {
                    siteMetadata {
                        title
                        description
                        author
                        url
                    }
                }
            }`
    );

    var { url, title } = props.data;
    var updatedUrl = site.siteMetadata.url + url;
    var totalWords = title.split(' ');
    var updatedWords = totalWords.map((word) => {
        return word.slice(0, 1).toUpperCase() + word.slice(1);
    })

    updatedWords = updatedWords.join(' ');

    function showPrintPreview() {
        window.print();
        // trackCustomEvent({
        //     category: 'Print',
        //     action: 'Click',
        //     label: 'Lyrico Analytics Print button'
        // })
    }

    return (
        <div className="lyrico-social-share-button-container">
            <FacebookShareButton url={updatedUrl} className="lyrico-social-share-icons button is-outlined is-rounded facebook" onClick={e => {
                // trackCustomEvent({
                //     category: 'Facebook Share',
                //     action: 'Click',
                //     label: 'Lyrico Analytics FB share button'
                // })
            }}>
                <span className="icon">
                    <FaFacebookSquare color="#4267B2" size="35px" />
                </span>
            </FacebookShareButton>

            <TwitterShareButton url={updatedUrl} className="lyrico-social-share-icons button is-outlined is-rounded facebook" onClick={e => {
                // trackCustomEvent({
                //     category: 'Twitter Share',
                //     action: 'Click',
                //     label: 'Lyrico Analytics Twitter share button'
                // })
            }}>
                <span className="icon">
                    <FaTwitterSquare color="#1DA1F2" size="35px" />
                </span>
            </TwitterShareButton>

            <WhatsappShareButton url={updatedUrl} title={updatedWords} separator=' | ' className="lyrico-social-share-icons button is-outlined is-rounded facebook" onClick={e => {
                // trackCustomEvent({
                //     category: 'Whatsapp Share',
                //     action: 'Click',
                //     label: 'Lyrico Analytics Whatsapp share button'
                // })
            }}>
                <span className="icon">
                    <FaWhatsappSquare color="#4AC959" size="35px" />
                </span>
            </WhatsappShareButton>

            <span className="icon" onClick={showPrintPreview}>
                <FaPrint color="#4267B2" size="30px" />
            </span>
        </div >
    );
}

export default Share;