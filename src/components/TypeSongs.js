import React from 'react';
import slugify from 'react-slugify';
import {Link} from 'gatsby';


function renderTypeSongs(data) {
    return data.edges.map((data, index) => {
        var songSlug = '/songs/' + slugify(data.node.frontmatter.title);
        return (
            <div className="latest-post-container" key={index}>
                <div className="latest-post-details">
                     <Link to={songSlug}>
                        <div className="latest-post-title">
                            {data.node.frontmatter.song} - {data.node.frontmatter.album}
                        </div>
                    </Link>
                </div>
            </div>
        )
    });
}

function TypeSongs( props ) {

    return(
        <div className="lyrico-typesongs-container">
            <div className="lyrico-show-recents-header">More {props.type.edges[0].node.fields.type} Songs</div>
           {renderTypeSongs(props.type)}
        </div>
    );
}


export default TypeSongs;