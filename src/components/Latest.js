import React from 'react';
import { graphql, StaticQuery, Link } from 'gatsby'
import slugify from 'react-slugify';


function renderLatestPosts(data) {
    return data.allMarkdownRemark.edges.map((data, index) => {
        var songSlug = '/songs/' + slugify(data.node.frontmatter.title);
        return (
            <div className="latest-post-container" key={index}>
                <div className="latest-post-details">
                    <div className="latest-post-title">
                        <Link to={songSlug}>{data.node.frontmatter.title}</Link>
                    </div>
                </div>
            </div>
        )
    });
}

function LatestPosts(props) {
    return (
        <div className="lyrico-recents-container">
            <StaticQuery query={latest} render={renderLatestPosts} />
        </div>
    );
}

const latest = graphql`query LatestPosts {
  allMarkdownRemark(sort: {frontmatter: {date: DESC}}, limit: 5) {
    edges {
      node {
        frontmatter {
          path
          song
          title
          date
          album
          image {
            childImageSharp {
              gatsbyImageData(layout: FULL_WIDTH)
            }
          }
        }
      }
    }
    totalCount
  }
}
`


export default LatestPosts;