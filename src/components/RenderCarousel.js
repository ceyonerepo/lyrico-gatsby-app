import * as React from 'react';
import {
  CarouselProvider,
  Slider,
  Slide
} from "pure-react-carousel"
import "pure-react-carousel/dist/react-carousel.es.css"
import { isMobileOnly, isTablet, isBrowser } from "react-device-detect"
import HomepageSongs from "../components/RenderHomePageSongs"

export default function RenderCarousel({ node, isBackWord }) {
    var index = 0;
    var playDirection = isBackWord ? 'backword' : 'forward'
    return (
      <CarouselProvider
        naturalSlideWidth={300}
        naturalSlideHeight={isBrowser ? 250 : 300}
        visibleSlides={isMobileOnly ? 1 : isTablet ? 2 : 3}
        totalSlides={6}
        isPlaying={true}
        interval={2000}
        playDirection={playDirection}
        infinite
      >
        <Slider>
          {node.map(node => {
            return (
              <Slide index={index++}>
                <HomepageSongs key={node.frontmatter.title} node={node} />
              </Slide>
            )
          })}
        </Slider>
      </CarouselProvider>
    )

}