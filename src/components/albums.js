import React from 'react';
import { graphql, StaticQuery } from 'gatsby';
import RenderAlbums from '../components/RenderAlbums';

function renderAlbums(data) {
  var addedAlbums = [];

  var albumData = data.allMarkdownRemark.edges.map((node, index) => {
        if (addedAlbums.indexOf(node.node.frontmatter.album) === -1) {
          addedAlbums.push(node.node.frontmatter.album);
          return <RenderAlbums key={index} data={node}/>
        }
    });

  return albumData;
}

function Movies( props ) {

  var data = props && props.albumDetails ? renderAlbums(props.albumDetails) : <StaticQuery query={AlbumsQuery} render= {renderAlbums}/>;
    return(
        <div className="lyrico-albums-container">
            {data}
        </div>
    )
}

const AlbumsQuery = graphql`query AlbumsQuery {
  allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
    edges {
      node {
        frontmatter {
          artist
          director
          path
          title
          lyricist
          album
          image {
            childImageSharp {
              gatsbyImageData(layout: FULL_WIDTH)
            }
          }
        }
        fields {
          slug
        }
      }
    }
  }
}
`

export default Movies;