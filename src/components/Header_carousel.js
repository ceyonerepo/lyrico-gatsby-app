import React from 'react';
import {graphql, StaticQuery} from 'gatsby';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { getImage, GatsbyImage } from "gatsby-plugin-image"

// const getConfigurableProps = () => ({
//     showArrows: boolean('showArrows', false, tooglesGroupId),
//     showStatus: boolean('showStatus', false, tooglesGroupId),
//     showIndicators: boolean('showIndicators', false, tooglesGroupId),
//     infiniteLoop: boolean('infiniteLoop', true, tooglesGroupId),
//     showThumbs: boolean('showThumbs', false, tooglesGroupId),
//     useKeyboardArrows: boolean('useKeyboardArrows', false, tooglesGroupId),
//     autoPlay: boolean('autoPlay', true, tooglesGroupId),
//     stopOnHover: boolean('stopOnHover', true, tooglesGroupId),
//     swipeable: boolean('swipeable', true, tooglesGroupId),
//     dynamicHeight: boolean('dynamicHeight', false, tooglesGroupId),
//     emulateTouch: boolean('emulateTouch', true, tooglesGroupId),
//     thumbWidth: number('thumbWidth', 100, {}, valuesGroupId),
//     selectedItem: number('selectedItem', 0, {}, valuesGroupId),
//     interval: number('interval', 3000, {}, valuesGroupId),
//     transitionTime: number('transitionTime', 150, {}, valuesGroupId),
//     swipeScrollTolerance: number('swipeScrollTolerance', 5, {}, valuesGroupId),
// });

function getData(props) {
    var carouselData = props.allMarkdownRemark.edges.map((data, index) => {
        var imageSrc = getImage(data.node.frontmatter.image);
      
        return (
          <div key={index}>
            <GatsbyImage
              image={imageSrc}
              alt={data.node.frontmatter.title}
            ></GatsbyImage>
            <p className="legend">{data.node.frontmatter.title}</p>
          </div>
        )
    });

    return (
      <Carousel
        centerSlidePercentage={70}
        centerMode={true}
        infiniteLoop={true}
        showThumbs={false}
        showArrows={true}
        interval={2500}
        showIndicators={true}
        autoPlay={true}
        stopOnHover={false}
        showStatus={true}
        width='100%'
      >
        {carouselData}
      </Carousel>
    )
}

function ImageCarousel() {
    return(
        <StaticQuery query={carouselImages} render= {getData}/>
    )
};

const carouselImages = graphql`
  query carouselImages {
    allMarkdownRemark(
      limit: 10
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      edges {
        node {
          frontmatter {
            path
            song
            title
            date
            album
            image {
              childImageSharp {
                gatsbyImageData(layout: FULL_WIDTH)
              }
            }
          }
        }
      }
      totalCount
    }
  }
`


export default ImageCarousel;