import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import HomepageSongs from "../components/RenderHomePageSongs"
import Yearslist from "../components/YearList"
import BrowseMore from "../components/BrowseMore"

function Home() {
  var { tamilSongs, hindiSongs, teluguSongs, malayalamSongs } = useStaticQuery(
    graphql`
      {
        tamilSongs: allMarkdownRemark(
          filter: { frontmatter: { lang: { eq: "tamil" } } }
          limit: 5
          sort: {frontmatter: {date: DESC}}
        ) {
          totalCount
          nodes {
            frontmatter {
              artist
              director
              path
              title
              lyricist
              album
              song
              singers
              image {
                childImageSharp {
                  gatsbyImageData(layout: FIXED, width: 330, height: 200)
                }
              }
            }
            fields {
              slug
            }
          }
        }
        hindiSongs: allMarkdownRemark(
          filter: { frontmatter: { lang: { eq: "hindi" } } }
          limit: 5
          sort: {frontmatter: {date: DESC}}
        ) {
          totalCount
          nodes {
            frontmatter {
              artist
              director
              path
              title
              lyricist
              album
              song
              singers
              image {
                childImageSharp {
                  gatsbyImageData(layout: FIXED, width: 330, height: 200)
                }
              }
            }
            fields {
              slug
            }
          }
        }
        teluguSongs: allMarkdownRemark(
          filter: { frontmatter: { lang: { eq: "telugu" } } }
          limit: 5
          sort: {frontmatter: {date: DESC}}
        ) {
          nodes {
            frontmatter {
              artist
              director
              path
              title
              lyricist
              album
              song
              singers
              image {
                childImageSharp {
                  gatsbyImageData(layout: FIXED, width: 330, height: 200)
                }
              }
            }
            fields {
              slug
            }
          }
          totalCount
        }
        malayalamSongs: allMarkdownRemark(
          filter: { frontmatter: { lang: { eq: "malayalam" } } }
          limit: 5
          sort: {frontmatter: {date: DESC}}
        ) {
          nodes {
            frontmatter {
              artist
              director
              path
              title
              lyricist
              album
              song
              singers
              image {
                childImageSharp {
                  gatsbyImageData(layout: FIXED, width: 330, height: 200)
                }
              }
            }
            fields {
              slug
            }
          }
          totalCount
        }
      }
    `
  )

  return (
    <>
      <div className="title-wrapper">
        <h3 className="lang-title">Latest Songs in Tamil</h3>
        <div className="title-ruler"></div>
      </div>
      <div className="home-songs-list">
        {tamilSongs.nodes.map(node => {
          return <HomepageSongs key={node.frontmatter.title} node={node} />
        })}
        <BrowseMore
          totalCount={tamilSongs.totalCount}
          lang="tamil"
        ></BrowseMore>
      </div>
      <div className="title-wrapper">
        <h3 className="lang-title">Latest Songs in Telugu</h3>
        <div className="title-ruler"></div>
      </div>
      <div className="home-songs-list">
        {teluguSongs.nodes.map(node => {
          return <HomepageSongs key={node.frontmatter.title} node={node} />
        })}
        <BrowseMore
          totalCount={teluguSongs.totalCount}
          lang="telugu"
        ></BrowseMore>
      </div>
      <div className="title-wrapper">
        <h3 className="lang-title">Latest Songs in Hindi</h3>
        <div className="title-ruler"></div>
      </div>
      <div className="home-songs-list">
        {hindiSongs.nodes.map(node => {
          return <HomepageSongs key={node.frontmatter.title} node={node} />
        })}
        <BrowseMore
          totalCount={hindiSongs.totalCount}
          lang="hindi"
        ></BrowseMore>
      </div>
      <div className="title-wrapper">
        <h3 className="lang-title">Latest Songs in Malayalam</h3>
        <div className="title-ruler"></div>
      </div>
      <div className="home-songs-list">
        {malayalamSongs.nodes.map(node => {
          return <HomepageSongs key={node.frontmatter.title} node={node} />
        })}
        <BrowseMore
          totalCount={malayalamSongs.totalCount}
          lang="malayalam"
        ></BrowseMore>
      </div>
      <div className="title-wrapper">
        <h3 className="lang-title">Movie Songs By Year</h3>
        <div className="title-ruler"></div>
      </div>
      <div className="yearslist">
        <Yearslist></Yearslist>
      </div>
    </>
  )
}

export default Home