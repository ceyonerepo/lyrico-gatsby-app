import React from 'react';
import {Link} from 'gatsby';
// import Search from "./SearchPage"
import { StaticQuery, graphql } from "gatsby"

const NavigationBar = (props) => {

  return (
    <div className="lyrico-navbar-container">
      <Link to="/">
        <span className="lyrico-home-text">Lyrico</span>
      </Link>
      {/* <StaticQuery
        query={graphql`
          query SearchIndexQuery {
            siteSearchIndex {
              index
            }
          }
        `}
        render={data => <Search searchIndex={data.siteSearchIndex.index} />}
      /> */}
    </div>
  )
}

export default NavigationBar;