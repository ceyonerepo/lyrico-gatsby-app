import React, { Component } from "react"
import { Index } from "elasticlunr";
import { Link } from 'gatsby';

// Search component
export default class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      query: ``,
      results: [],
    }
  }


  render() {
    return (
      <div className="search-area">
        <div className="search-container">
          <input
            className="search-item"
            type="text"
            value={this.state.query}
            onChange={this.search}
            placeholder='Search Album or Song'
          />
          <span className="search-icon"></span>
        </div>
        <ul className="search-wrapper">
          {this.state.results.map(page => (
            <li key={page.id}>
              <div className="search-album-wrapper search-detail-wrapper">
                <div className="search-album-title">Album : </div>{" "}
                <Link
                  to={"/albums/" + page.album.toLowerCase().replace(/\s/g, "-")}
                >
                  <div className="search-album-name">{page.album}</div>
                </Link>
              </div>
              <div className="search-song-wrapper search-detail-wrapper">
                <div className="search-song-title">Song : </div>
                <Link
                  to={"/songs/" + page.title.toLowerCase().replace(/\s/g, "-")}
                >
                  <div className="search-song-name">{page.song}</div>
                </Link>
              </div>
            </li>
          ))}
        </ul>
      </div>
    )
  }
  getOrCreateIndex = () =>
    this.index
      ? this.index
      : // Create an elastic lunr index and hydrate with graphql query results
        Index.load(this.props.searchIndex)

  search = evt => {
    const query = evt.target.value
    this.index = this.getOrCreateIndex()
    this.setState({
      query,
      // Query the index with search string to get an [] of IDs
      results: this.index
        .search(query, {expand: true})
        // Map over each ID and return the full document
        .map(({ ref }) => this.index.documentStore.getDoc(ref)),
    })
  }
}
