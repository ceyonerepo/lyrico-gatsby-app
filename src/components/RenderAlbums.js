import React from "react"
import { Link } from "gatsby"
import slugify from "react-slugify"
import { getImage, GatsbyImage } from "gatsby-plugin-image"

const RenderSong = ({ data }) => {
  var slug = "/albums/" + slugify(data.node.frontmatter.album.toLowerCase())
  return (
    <div className="home-page-song">
      <Link to={slug}>
        <div className="wrapper">
          <div className="img-container">
            <GatsbyImage
              image={getImage(data.node.frontmatter.image)}
              alt={data.node.frontmatter.album}
            ></GatsbyImage>
          </div>
          <div className="lyrico-artists">
            <div className="lyrico-song-title">
              Album : {data.node.frontmatter.album}
            </div>
            <div className="lyrico-song-movie">
              Music : {data.node.frontmatter.artist}
            </div>
          </div>
        </div>
      </Link>
    </div>
  )
}

export default RenderSong
