import React from "react"

function SongInfo(props) {
  return (
    <>
      <div className="lyrico-song-info-container">
        <p>
          <span className="song-info-song-title">
            <b>{props.data.title}</b>
          </span>{" "}
          is written by {props.data.lyricist}. This song is composed by{" "}
          <b>{props.data.artist}</b> for the movie <b>{props.data.album}</b>
        </p>
      </div>
    </>
  )
}

export default SongInfo
