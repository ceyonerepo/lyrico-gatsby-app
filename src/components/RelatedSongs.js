import React from 'react';
import { Card, CardFooter} from 'reactstrap';

import {Link} from 'gatsby';
import { GatsbyImage } from "gatsby-plugin-image";
import slugify from 'react-slugify';

const RelatedSongs = (props) => {
  var slug = '/songs/' + slugify(props.album.node.frontmatter.title);
  return (
    <div className='lyrico-card-container'>
      <Card>
        <Link to={slug}>
          <GatsbyImage
            image={props.album.node.frontmatter.image.childImageSharp.gatsbyImageData}
            className="card-image-top"
            width="100%"
            alt={props.album.node.frontmatter.title} />
        </Link>
        <CardFooter><Link to={slug}>{props.album.node.frontmatter.song}</Link></CardFooter>
      </Card>
    </div>
  );
};

export default RelatedSongs;