import React from "react"
import LatestPosts from "./Latest"

function SideBar(props) {
  return (
    <>
      <div className="lyrico-show-recents-header">More {props.type} Songs</div>
      <LatestPosts />
    </>
  )
}

export default SideBar
