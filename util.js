function _getLowerCase(ele) {
    return $(ele).text().toLowerCase()
}

module.exports = {
    removeSingerAndMusicBy: function (data, $) {
       var songData =  $(data).filter(function (i, el) {
            if ($(el).text().toLowerCase().indexOf('singers') < 0 && $(el).text().toLowerCase().indexOf('music by') < 0) {
                return $(el).html($(el).html()); //adding new line after a line
            }
       })
        
        return $(songData).text();
    },
}