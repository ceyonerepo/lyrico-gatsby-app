let {render} = require('mustache');
let fs = require('fs');

// var title = '24 hours',
var album = 'Yuvan yuvathi',
    artist = 'yuvan shankar raja',
    lyricist = 'Yugabharathi',
    director = 'Maniratnam',
    // path = 'test',
    song = 'Oru Kal Oru kannadi',
    // image = 'test',
    date = '2011-08-26',
    lang = 'tamil',
    youtubeLink = "https://www.youtube.com/embed/FJaHSEt7Lyw",
    type = 'Love',
    singers = 'Udit narayanan',
    updatedAlbumName = '',
    updatedSongName = '';


if(album.indexOf(' ') >= 0) {
    var splittedAlbumName = album.split(' ');

    splittedAlbumName.forEach((name, index) => {
        if(index == splittedAlbumName.length -1) {
            updatedAlbumName += name
        } else {
            updatedAlbumName +=  name + '-'
        }
    })
}

if(song.indexOf(' ') >= 0) {
    var splittedSongName = song.split(' ');

    splittedSongName.forEach((name, index) => {
        if(index == splittedSongName.length -1) {
            updatedSongName += name
        } else {
            updatedSongName +=  name + '-'
        }
    })
} 


let songData = {
    title : song.toLowerCase() + ' song lyrics',
    album,
    artist,
    lyricist,
    director,
    path : updatedAlbumName + '-lyrics',
    song,
    image : updatedAlbumName,
    date,
    lang,
    youtubeLink : youtubeLink.split('embed/')[1],
    type,
    singers,
};

var template = fs.readFileSync('./sonfile_template.md').toString();
var output = render(template, songData);
fs.writeFileSync(`./${updatedSongName.toLowerCase() + '-' + updatedAlbumName.toLowerCase()}.md`, output);