const path = require('path');

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    if (stage === "build-html") {
        actions.setWebpackConfig({
            module: {
                rules: [
                    {
                        test: /bad-module/,
                        use: loaders.null(),
                    },
                ],
            },
        })
    }
}

exports.onCreateNode = ({ node, actions }) => {

    const { createNodeField } = actions;

    if (node.internal.type === 'MarkdownRemark') {
        var alubum = node.frontmatter.album? node.frontmatter.album.toLowerCase():"";
        const slugFromAlbum = '/albums/' + alubum.replace(/\s/g, '-');

        var songTitle = node.frontmatter.title?node.frontmatter.title.toLowerCase():"";
        const Songslug = '/songs/' + songTitle.replace(/\s/g, '-');

        var albumName = node.frontmatter.album;
        var songType = node.frontmatter.type;

        createNodeField({
            node,
            name: 'slug',
            value: slugFromAlbum
        });

        createNodeField({
            node,
            name: 'songslug',
            value: Songslug
        });

        createNodeField({
            node,
            name: 'albumName',
            value: albumName
        });

        createNodeField({
            node,
            name: 'type',
            value: songType
        });
    }
}

exports.createPages = ({ actions, graphql }) => {
    const { createPage } = actions;
    const albumSongListTemplate = path.resolve('src/templates/single-album-page.js');
    const songPageTemplate = path.resolve('src/templates/song-page.js');
    const YearAlbums = path.resolve('src/templates/year-albums.js');
    const languagePage = path.resolve('src/templates/language-page.js');

    return graphql(`
    {
        allMarkdownRemark {
            edges {
                node {
                    id
                    frontmatter {
                        title
                        album
                        artist
                        path
                        song
                        date
                        lang
                    }
                    fields {
                        slug
                        songslug
                        albumName
                        type
                    }
                }
            }
        }
    }`).then(res => {
        if (res.errors) return Promise.reject(res.errors);

        const albumSongs = res.data.allMarkdownRemark.edges;
        const songs = res.data.allMarkdownRemark.edges;

        var langs = res.data.allMarkdownRemark.edges.map(({ node }) => {
            return node.frontmatter.lang;
        })

        langs = [...new Set(langs)];

         
        var years = res.data.allMarkdownRemark.edges.map(({node}) => {
            return node.frontmatter.date? node.frontmatter.date.split('-')[0]: "";
        });

        years = [...new Set(years)];

        console.table('------------------YEARS---------------- ', years)

        years.forEach((year) => {
            createPage({
                path: "/year/" + year,
                component: YearAlbums,
                context: {
                    year: '/' + (year || '2023') + '/'
                },
            })
        });

        console.table('------------------langs---------------- ', langs)

        langs.forEach((lang) => {
            if(lang) {
                createPage({
                  path: "/language/" + lang,
                  component: languagePage,
                  context: {
                    lang: lang,
                  },
                })
            }
        })
       
        albumSongs.forEach(({ node }) => {
            if(node.fields.albumName) {
                createPage({
                    path: node.fields.slug,
                    component: albumSongListTemplate,
                    context: {
                        slug: node.fields.slug,
                        albumName: node.fields.albumName
                    }
                })
            }
        });

        songs.forEach(({ node }) => {
            if(node.fields.type) {
                createPage({
                    path: node.fields.songslug,
                    component: songPageTemplate,
                    context: {
                        albumName: node.fields.albumName,
                        songslug: node.fields.songslug,
                        type: node.fields.type
                    }
                })
            }
        });
    })

}