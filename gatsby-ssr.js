// Import React so that you can use JSX in HeadComponents
const React = require("react")

const HeadComponents = [
  <script
    async
    src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
  ></script>,
]

exports.onRenderBody = ({ setPostBodyComponents }) => {
  setPostBodyComponents(HeadComponents)
}
