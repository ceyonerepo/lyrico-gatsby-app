---
title: {{title}}
album: {{album}}
artist: {{artist}}
lyricist: {{lyricist}}
director: {{director}}
path: /albums/{{path}}
song: {{song}}
image: ../../images/albumart/{{image}}.jpg
date: {{date}}
lang: {{lang}}
youtubeLink: https://www.youtube.com/embed/{{youtubeLink}}
type: {{type}}
singers:
  - {{singers}}
---

{{{songLyrics}}}