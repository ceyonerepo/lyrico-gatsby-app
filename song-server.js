const express = require('express')
const path = require('path');
const app = express()
let { render } = require('mustache');
let fs = require('fs');
var bodyParser = require('body-parser')
const fsPromises = require('fs').promises;
const request = require('request');
const cheerio = require('cheerio')
const fetch = require('fetch')
const util = require('./util');

var siteConfigs = [
  {
    site: 'tamil2lyrics',
    song: '#English p',
    artist: '.lyrics-title > h3',
    songName: '.lyrics-title .pull-left h1',
    album: '.lyrics-title .pull-left a'
  }
]


app.get('/new', function (req, res) {
  res.sendFile(path.join(__dirname, './new-song-formatter.html'));
})

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, './songFormatter.html'));
})

app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.post('/uploadFile', async function (req, res) {
  var { file, fileName, ext } = req.body;
  var oldPath = 'C:\\Users\\manoc\\Documents\\Lyrico Albumarts\\' + file 
  var newpath = 'D:\\gatsby-sample\\Lyrico_Latest\\src\\images\\albumart\\' + fileName + '.' + ext;
  try {
    await fsPromises.rename(oldPath, newpath);
    res.status(200).send('File created Successfully');
  } catch(e) {
    res.status(300).send(e);
  }

});

app.post('/createSongFile', function (req, res) {
  var template = fs.readFileSync('./sonfile_template.md').toString();
  var output = render(template, req.body);

  fs.writeFileSync(`./src/pages/posts/${req.body.fileName}.md`, output);

  res.send('File create Successfully')
});


app.post('/getLyricsFromURL', function (req, res) {
  var curRes = res;

  fetch.fetchUrl(req.body.siteURL, function (err, meta, body) {
    var currentConfig = siteConfigs[0];
    const $ = cheerio.load(body);

    var response = {
      lyrics: util.removeSingerAndMusicBy(currentConfig.song, $),
      song: $(currentConfig.songName).text(),
      album: $(currentConfig.album).text(),
      artist: $(currentConfig.artist).text()
    }
    curRes.send(response)
  })
})


app.listen(3000, '', () => {
  console.error('Server started')
})